﻿var chqDat = "";
var globalBalance = 0;
var globalEmpPrevBalance = 0;
var globalJentryAmount = 0;
$tableItemCounter = 0;
var $addedProductIDs = [];




$('#btnSave').click(function () {
    var empID = +$('#AccountID option:selected').val() || null;
    var jentryID = +$('#JEntryID_DD option:selected').val() || null;
    var voucherDate = $('#VoucherDate').val();

    var tableLength = $('#tblProduct tbody tr').length || 0;
    var isChecked = $('#directCashPay').is(":checked");



    if (empID == null || empID <= 0) {
        swal("Employee", "Please Select Employee!", "error");
    }
    else if (!document.getElementById("Description").value) {
        swal("Description", "Please Enter Description!", "error");
    }
    else if (voucherDate == "") {
        swal("Date", "Please Enter Date!", "error");
    }

    //else if (!isChecked && ((jentryID <= 0 || jentryID == null) || tableLength <= 0)) {

    //    swal("Employee Amount/Data", "Please select employee amount/Insert Data", "error");
    //}
    else {
        $('#btnSave').prop('disabled', true);
        uiBlock();
            InsertEmployeePurchases();
    }
});

var invoiceTotal = 0;

$(document).ready(function () {
   
    $("#Amount").val(0);
    $("#Amount").prop('readonly', true);

    document.getElementById('VoucherDate').valueAsDate = new Date();

   
})

calEmpBalance = () => {
    var bal = +globalEmpPrevBalance;
    var amount = +$("#Amount").val();
    if (bal > 0 && amount > 0 && (bal - amount) >= 0) {
        $("#empPreviousBal").text('Bal: Rs ' + parseFloat(bal - amount).toFixed(0));
        $("#hdnEmpPrevBal").val(parseFloat(bal - amount).toFixed(0));

    }
    else if (bal < amount && (bal - amount) <= 0) {
        $("#empPreviousBal").text('Bal: Rs ' + parseFloat(globalEmpPrevBalance).toFixed(0));
        $("#hdnEmpPrevBal").val(parseFloat(globalEmpPrevBalance).toFixed(0));
        $("#Amount").val('');
    }
    else {
        $("#empPreviousBal").text('Bal: Rs ' + parseFloat(globalEmpPrevBalance).toFixed(0));
        $("#hdnEmpPrevBal").val(parseFloat(globalEmpPrevBalance).toFixed(0));
        $("#Amount").val('');

    }
}

calBalance = () => {
    var bal = globalBalance;
    var empBal = globalEmpPrevBalance;
    var amount = +$("#Amount").val();
    if (bal > 0 && amount > 0 && (bal - amount) > 0) {
        $("#Balance").val(bal - amount);

    }
    else if (bal < amount && (bal - amount) <= 0) {
        $("#Balance").val(globalBalance);
        $("#Amount").val('');
    }
    else {
        $("#Balance").val(globalBalance);
        $("#Amount").val('');

    }
}

calTotalAmount = () => {
    var total = 0;

    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);

        var subTotal = parseFloat($row.find('#ProductSubTotal').text());

        //var qty = $row.find("#ProductQty").text() || 0;

        total += subTotal;
    });

    var jentryAmount = globalJentryAmount - total;

    if (jentryAmount > 0) {
        $('#EmployeeSettledAmount').val(parseFloat(jentryAmount).toFixed(1));
    }
    else {
        $('#EmployeeSettledAmount').val(0);
    }

    $('#Amount').val(parseFloat(total).toFixed(2));

    //calBalance();

}

$('#Qty').on('input', function (e) {
    calcSubTotal();
});

$('#UnitPrice').on('input', function (e) {
    calcSubTotal();
});

function calcSubTotal() {
    var qty = +$('#Qty').val() || 0;


    var rate = parseFloat($('#UnitPrice').val());

    if (($.isNumeric(rate)) && ($.isNumeric(qty))) {


        var amount = qty * rate;
        //console.log(amount + " amount");
        $('#SubTotal').val(parseFloat(amount).toFixed(2));
    }
    else {
        $('#SubTotal').val("");
    }
}

$("#directCashPay").click(function () {
    var isChecked = $('#directCashPay').is(":checked");
    $("#Amount").val(0);
    $("#Balance").val(globalBalance);


    if (isChecked) {
        $("#productSecionDiv").hide();
        $("#Amount").prop('readonly', false);
    }
    else {
        getJEntryDataEmployeeWise();
        $("#tblProduct>tbody>tr").remove();
        $addedProductIDs = [];
        $tableItemCounter = 0;
        $("#productSecionDiv").show();
        $("#Amount").prop('readonly', true);

    }
})

$("#AccountID").change(function () {
    getJEntryDataEmployeeWise();
});
getJEntryDataEmployeeWise = () => {
    var empID = +$("#AccountID").val() || null;

    if (empID != null && empID > 0) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Payment/GetJentryEmployeeWiseForBalanceOnEmployeePurchaseCreate?EmpID=' + empID,
            async: true,
            success: function (data) {
                console.log(data);
                if (data.length) {
                    GetDropdown1('JEntryID_DD', data[0].jentryList, true);
                    //if (parseFloat(data[0].prevBal).toFixed(0) > 0) {
                        $("#empPreviousBal").text('Bal: Rs ' + parseFloat(data[0].prevBal).toFixed(0));
                        globalEmpPrevBalance = parseFloat(data[0].prevBal).toFixed(0);
                        $("#hdnEmpPrevBal").val(parseFloat(data[0].prevBal).toFixed(0));

                        if (data[0].jentryIds.length > 0) {
                            JEntryIDs = data[0].jentryIds
                        }
                        else {
                            JEntryIDs = [];
                        }
                    //}
                    //else {
                    //    $("#empPreviousBal").text('Bal: Rs ' + parseFloat(0).toFixed(0));
                    //    globalEmpPrevBalance = 0;
                    //    $("#hdnEmpPrevBal").val(0);

                    //    if (data[0] ?.jentryIds ?.length > 0) {
                    //        JEntryIDs = data[0].jentryIds
                    //    }
                    //    else {
                    //        JEntryIDs = [];
                    //    }
                    //}
                }
                else {
                    $("#empPreviousBal").text('Bal: Rs ' + parseFloat(0).toFixed(0));
                    globalEmpPrevBalance = 0;
                    $("#hdnEmpPrevBal").val(0);


                    JEntryIDs = [];

                }

            },
            error: function (err) {
                console.log(err);
            }
        });
    }
}
//getJEntryDataEmployeeWise = () => {
//    var empID = +$("#AccountID").val() || null;

//    if (empID != null && empID > 0) {
//        $.ajax({
//            type: "POST",
//            contentType: "application/json; charset=utf-8",
//            url: '/Payment/GetJentryEmployeeWise?EmpID=' + empID,
//            async: true,
//            success: function (data) {
//                GetDropdown1('JEntryID_DD', data, true);
//            },
//            error: function (err) {
//                console.log(err);
//            }
//        });
//    }
//}

$("#JEntryID_DD").change(function () {
    console.log("in");
    var Id = +$("#JEntryID_DD").val() || 0;
    console.log(Id);

    if (Id > 0) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Payment/GetJentryAmountIdWise?JEntryID=' + Id,
            async: true,
            success: function (data) {
                if (data > 0) {
                    globalJentryAmount = data;
                    $("#EmployeeSettledAmount").val(data);

                }
                else {
                    $("#EmployeeSettledAmount").val(0);
                    swal("Error", "No Data/Unable To Fetch", "error");
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    }
})

$("#ProdDesc").on('input', function () {
    if (document.getElementById("ProdDesc").value) {
        $("#ddlPartNumber").val(null).trigger('change.select2');
        $("#ddlPartNumber").prop('disabled', true);
    }
    else {
        $("#ddlPartNumber").prop('disabled', false);
    }
})

$("#ddlPartNumber").change(function () {
    if (+$("#ddlPartNumber").val() > 0) {
        $("#ProdDesc").val('');
        $("#ProdDesc").prop('disabled', true);
        $("#ExpenseID").val(null).trigger('change.select2');
        $("#ExpenseID").prop('disabled', true);
    }
    else {
        $("#ExpenseID").prop('disabled', false);
        $("#ProdDesc").prop('disabled', false);
    }
})
$("#addRow").click(function () {
    var ProductID = +$("#ddlPartNumber").val() || 0; // hidden
    var PartNO = $("#ddlPartNumber :selected").text();
    var Qty = $("#Qty").val();
    var ProdDesc = $("#ProdDesc").val() || "-";
    var UnitPrice = $("#UnitPrice").val();
    var ExpenseID = +$("#ExpenseID").val() || null;
    var ExpenseName = $("#ExpenseID option:selected").text() || "-";

    if (ProductID <= 0) {
        PartNO = ProdDesc;
    }

    if (ProductID > 0) {
        ExpenseID = null;
        ExpenseName = "-";
    }

    var SubTotal = +$("#SubTotal").val(); // 8
    var Total = +$("#Amount").val(); // 8
    var empBal = +globalEmpPrevBalance; // 8

    //if ((Total+SubTotal) > empBal) { swal("Error", "Purchase total amount must be less than/equals to employee balance", "error"); }
     if ((ExpenseID <= 0 || ExpenseID == null) && document.getElementById("ProdDesc").value) { swal("Error", "Please Select Expense!", "error"); }
    else if (ExpenseID > 0 && !document.getElementById("ProdDesc").value) { swal("Error", "Please Enter Product Description!", "error"); }

    else if (ProductID >= 0 && (parseFloat(Qty)) > 0 && Number(parseFloat(UnitPrice)) && SubTotal > 0) {
        if (ProductID > 0) {
            var index = $.inArray(+ProductID, $addedProductIDs);
            if (index >= 0) {
                swal("Error", "Product Already Added!", "error");
            } else {
                var markup = "<tr><td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deleteRow(this)'/></td><td id='productID' hidden>" + ProductID + "</td><td  id='ProductDescription'>" + PartNO + "</td><td hidden id='hdnExpId'>" + ExpenseID + "</td><td  id='expenseName'>" + ExpenseName + "</td><td id='ProductQty'>" + Qty + "</td><td id='ProductCostPrice'>" + parseFloat(UnitPrice).toFixed(2) + "</td><td id='ProductSubTotal'>" + parseFloat(SubTotal).toFixed(2) + "</td></tr>";
                $("#tblProduct tbody").append(markup);
                $tableItemCounter++;
                $addedProductIDs.push(+ProductID);

                calTotalAmount();
                ClearFields();
                // $('#BarCode').focus();
            }
        }
        else {
            var markup = "<tr><td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deleteRow(this)'/></td><td id='productID' hidden>" + ProductID + "</td><td id='ProductDescription'>" + PartNO + "</td><td hidden id='hdnExpId'>" + ExpenseID + "</td><td  id='expenseName'>" + ExpenseName + "</td><td id='ProductQty'>" + Qty + "</td><td id='ProductCostPrice'>" + parseFloat(UnitPrice).toFixed(2) + "</td><td id='ProductSubTotal'>" + parseFloat(SubTotal).toFixed(2) + "</td></tr>";
            $("#tblProduct tbody").append(markup);
            $tableItemCounter++;

            calTotalAmount();
            ClearFields();
        }

    }
    else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
    else if (UnitPrice == "" || UnitPrice == "undefined" || !Number(parseFloat(UnitPrice))) { swal("Error", "Please enter Unit Price!", "error"); }
    else { swal("Error", "Check Entries!", "error"); }

});


function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tblProduct").deleteRow(i);
    var row = $(r).closest("tr");
    var productID = parseInt(row.find('#productID').text());
    var index = $.inArray(+productID, $addedProductIDs);

    if (index >= 0) { $addedProductIDs.splice(index, 1); }
    calTotalAmount();
}


function ClearFields() {
    //console.log("call stack")
    //$('#ddlVehCode').val(null).change();
    $("#ddlPartNumber").val(null).trigger('change.select2');
    $("#ddlPartNumber").prop('disabled', false);
    // $('#StoreStatus').val(null).change();
    // $('#DeliveryStatus').val(null).change();
    // $('#OrderMachine').val(null).change();
    $("#ProdDesc").prop('disabled', false);
    $("#ExpenseID").val(null).trigger('change.select2');
    $("#ExpenseID").prop('disabled', false);
    $("#Qty").val("");
    $("#ProdDesc").val("");
    $("#UnitPrice").val("");
    $("#SubTotal").val("");
}

////////////

function InsertEmployeePurchases() {
    var rows = [];
    var payLog = [];
    var JEntryLog = [];
    var totalAmount = 0;
    var orderId = 0;
    var empID = +$('#AccountID option:selected').val() || null;
    var JEntryID = +$('#JEntryID_DD option:selected').val() || null;
    var desc = $('#Description').val();

    var voucherDate = $('#VoucherDate').val();

    var amount = parseFloat($('#Amount').val());

    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('#ProductSubTotal').text());

        var prodID = $row.find("#productID").text() || 0;
        var qty = $row.find("#ProductQty").text() || 0;
        var Desc = $row.find("#ProductDescription").text() || 0;
        var price = $row.find("#ProductCostPrice").text() || 0;
        var ExpenseID = +$row.find("#hdnExpId").text() || null;

        rows.push({
            Description: Desc,
            ProductID: prodID,
            Qty: qty,
            UnitPrice: price,
            Total: subTotal,
            IsProduct: prodID > 0 ? true : false,
            ExpenseID: ExpenseID
        });

    });


    var data = {
        'Description': desc,
        'Amount': amount,
        'AccountID': empID,
        'VoucherDate': voucherDate,
        'PaymentTypeID': 1,
        'JEntryID': JEntryID

    };

    JEntryLog.push({
        Amount: amount,
        OrderTypeID: 15,
        OrderID: 0,
        EntryTypeID: 43,
        isReversed: false
    });

    var json = JSON.stringify({ 'model': data, 'detailList': rows, 'jentryLog': JEntryLog, 'isChecked': false });
    ajaxCall("POST", json, "application/json; charset=utf-8", "/Payment/SaveEmployeePurchase", "json", onSuccess, onFailure);
    function onSuccess(data) {
        $('#btnSave').prop('disabled', false);
        console.log(data)
        uiUnBlock();

        if (data > 0) {
            swal("Success", "Received Purchase Note from Employee!", "success");

            setTimeout(function () { window.location.reload(); }, 2000);
        }
        else {
            swal("Error", data, "error");
        }
    }
    function onFailure(error) {
        if (error.statusText == "OK") {
            window.location.reload();
        }
    }
}
