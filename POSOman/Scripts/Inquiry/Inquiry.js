﻿

$("#Sources").select2();
$("#StatusID").select2();
$("#SalePersonID").select2();
$("#AccountID").select2();
$("#ChancesPercent").select2();
$("#FollowUp").select2();

document.getElementById('InquiryDate').valueAsDate = new Date();
//document.getElementById('FollowUpDate').valueAsDate = new Date();


function GetLatestInquiryNo() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Inquiry/LatestInquiryNo',
        async: true,
        success: function (data) {
            var value = "Inq-" + data;
            $("#InquiryNumber").val(value);
            $("#Code").val(data);
        },
        error: function (err) { console.log(err); }
    });
}

function GetCustomerDetail() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Customer/GetCustomerDetail?accID=' + $("#AccountID").val(),
        async: true,
        success: function (data) {
            $("#CustomerID").val(data.CustomerID);
            $("#CompanyName").val(data.Name);
            $("#ContactPerson").val(data.NameUr);
            $("#PhoneNo").val(data.Cell);
            $("#Email").val(data.Email);
        },
        error: function (err) { console.log(err); }
    });
}

$(document).ready(function () {

    $("#AccountID").change(function () {
        GetCustomerDetail();
    });

    
});
