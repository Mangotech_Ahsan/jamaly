﻿




$("#StatusID").select2();

$("#btnSearch").on("click", function () {
    var accountID = $('#hdnAccountID').val();
    var fromDate = $('#fromDate').val();
    var toDate = $('#toDate').val();
    var branchID = $('#Branch').val();
    var Branch = $("#Branch option:selected").text();
    var StatusID = $("#StatusID").val();

    if (fromDate != "" && toDate != "") { html += "<h4>From:    " + fromDate + "            TO:    " + toDate + " </h4>"; }
    $.ajax({
        url: "@Url.Action('Index', 'Inquiry')",
        data: {
            btn: true,
            AccountID: accountID,
            fromDate: $('#fromDate').val(),
            toDate: $('#toDate').val(),
            StatusID: StatusID
        },
        success: function (data) {
            $("#SearchResult").html(data);

            var col = [1, 2, 3, 4, 5, 6]
            $('#tblData').dataTable({
                dom: 'Bfrtip',
                order: [[0, "desc"]],
                buttons: [
                    {
                        extend: 'print',
                        title: '',
                        messageTop: function () {
                            return $("#PrintHeader").html();
                        },
                        messageBottom: $("#PrintBottom").html(),
                        exportOptions: {
                            columns: col
                        }
                    },
                    {
                        extend: 'excel',
                        title: '',
                        messageTop: '@ViewBag.Title List',
                        messageBottom: '',
                        exportOptions: {
                            columns: col
                        }
                    },
                    {
                        extend: 'pdf',
                        title: '',
                        messageTop: '@ViewBag.Title List',
                        messageBottom: '',
                        exportOptions: {
                            columns: col
                        }
                    }
                ]
            });

        }
    });
});


function UpdateStatus() {
    var inquiryid = $("#inquiryid").val();
    var StatusID = $("#StatusID").val();

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Inquiry/UpdateStatus?InquiryID=' + inquiryid + "&StatusID=" + StatusID,
        async: true,
        success: function (data) {
            location.reload();
        },
        error: function (err) { console.log(err); }
    });
}

