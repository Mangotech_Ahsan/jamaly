﻿$(function () {

    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function () {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    $(document).ready(function () {
        $(':file').on('fileselect', function (event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }

        });
    });

});
$(document).ready(function () {
    //var vehCodeID = $('#ddlVehCode').val();
    var vehCodeID = null;
    //The url we will send our get request to
    var valueUrl = '@Url.Action("GetValues", "Product")';
    // if (vehCodeID > 0) {
    $('#ddlProduct').select2(
      {
          placeholder: 'Enter Product',
          //Does the user have to enter any data before sending the ajax request
          minimumInputLength: 0,
          allowClear: true,
          ajax: {
              //How long the user has to pause their typing before sending the next request
              delay: 200,
              //The url of the json service
              url: '/Product/GetValues',
              dataType: 'json',
              async: true,
              //Our search term and what page we are on
              data: function (params) {
                  return {
                      pageSize: 500,
                      pageNum: params.page || 1,
                      searchTerm: params.term,
                      //Value from client side.
                      countyId: null
                  };
              },
              processResults: function (data, params) {
                  params.page = params.page || 1;
                  return {
                      results: $.map(data.Results, function (obj) {
                          return { id: obj.ProductID, text: obj.PartNo };
                      }),
                      pagination: {
                          more: (params.page * 500) <= data.Total
                      }
                  };
              }
          }
      });
});
function Upload() {
    //Get reference of FileUpload.
    var fileUpload = document.getElementById("img");

    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpeg|.jpg|.png|.gif)$");
    if (regex.test(fileUpload.value.toLowerCase())) {

        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();

                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;

                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    //if (height > 200 || width > 175) {
                    //    $(".errorProfile").text("Height and Width must not exceed 175x200.")
                    //    //alert("Height and Width must not exceed 200x175.");
                    //    return false;
                    //}
                    $(".errorProfile").text("")
                    //alert("Uploaded image has valid Height and Width.");
                    return true;
                };

            }
        } else {
            $(".errorProfile").text("This browser does not support HTML5.")
            //alert("This browser does not support HTML5.");
            return false;
        }
    } else {
        $(".errorProfile").text("Please select a valid Image file.")
        //alert("Please select a valid Image file.");
        return false;
    }
}
$('#btnSubmit').click(function () {
    
    var productID = $('#ddlProduct option:selected').val();
    
    var cashRet = parseFloat($('#cashReturned').val());
    
    if (productID == "" || typeof productID == undefined || productID == 0) {
        isValid = false;
        swal("Product", "Please Select Product!", "error");
    } else if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Sales Date!", "error");
        //console.log(PurchaseDate + "date");
    } else if (rowCount == 0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");
        //console.log(PurchaseDate + "date");
    } else if (paymentStatus == 0) {
        isValid = false;
        swal("Payment Status", "Please Select Payment Status!", "error");
    }    
    else if (isValid == true) {
        $('#btnSubmit').prop('disabled', true);
        insert();
    }
});
function insert() {
    var productID = $('#ddlProduct option:selected').val();
    var formData = new FormData();
    var totalFiles = document.getElementById("imageUploadForm").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("imageUploadForm").files[i];
        formData.append("imageUploadForm", file);
    } 
    ajaxCall("POST", formData, "application/json; charset=utf-8", "/SalesOrder/SaveOrder", "json", onSuccess, onFailure);

    function onSuccess(Result) {
        if (Result > 0) {
            uiUnBlock();
            //window.location.href = '/Quotation'; 
            if (isPOS == 0) {
                window.open('/SalesOrder/printOrder?id=' + parseInt(Result) + '');
                //window.open('/SalesOrder/printOrder?id=' + parseInt(Result) + '');
                //console.log("OK");
                window.location.href = '/SalesOrder?IsNew=true';
            }
            else if (isPOS == 1) {
                window.open('/SalesOrder/printOrder?id=' + parseInt(Result) + '');
                window.location.href = '/SalesOrder/POS';
            }
            else if (isPOS == 2) { // online orders
                window.open('/OnlineOrders/printOrder?id=' + parseInt(Result) + '');
                window.location.href = '/OnlineOrders';
            }

        } else {
            uiUnBlock();
            $('#btnSubmit').prop('disabled', false);
            // $('#btnHold').prop('disabled', false);

            alert("Some error Ocurred! Please Check Your Entries");
        }
    }

    function onFailure(error) {
        if (error.statusText == "OK") {
            console.log("OK");
        } else {
            swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
        }
    }
}