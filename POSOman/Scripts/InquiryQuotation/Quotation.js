﻿
function ajaxCall(requestType, parameters, contentType, url, dataType, success, error, complete, beforeSend) {
    $.ajax({
        type: requestType,
        contentType: contentType,
        url: url,
        async: false,
        data: parameters,
        dataType: dataType,
        beforeSend: beforeSend,
        success: success,
        error: error,
        complete: complete
    });
}


function GetInquiryInfo() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Product/GetProductsList?TypeID=' + vehCodeID,
        async: true,
        success: function (data) {
            GetDropdown1("ddlPartNumber", data, true);
        },
        error: function (err) { console.log(err); }
    });
}


function Validate() {
    var isValid = true;

    var len = $("#tblProduct tbody tr").length;
    if ($("#InquiryID").val() == "") {
        isValid = false;
        swal("Select Inquiry!", "Please Select Inquiry!", "error");
    }
    else if ($("#OfferFor").val() == "") {
        isValid = false;
        swal("Offer For!", "Please Enter Offer For!", "error");
    }
    else if ($("#ProductType").val() == "") {
        isValid = false;
        swal("Quotation Type!", "Please Select Quotation Type!", "error");
    }
    else if (!(len > 0)) {
        isValid = false;
        swal("Data Enter!", "Please Enter Data First!", "error");
    }
    else if ($("#TaxPercnt").val() == "") {
        isValid = false;
        swal("Tax!", "Please Select Tax!", "error");
    }
    else if ($("#AdvaneAmount").val() == "" || $("#MidWayAmount").val() == "" || $("#CompletionAmount").val() == "") {
        isValid = false;
        swal("Payment Terms!", "Please Enter Payment Terms!", "error");
    }
    //else if ($("#ProductType").val() == 1 && $("#deliverydate").val() == "") {
    //    swal("Delivery Time!", "Please Select Delivery Time!", "error");
    //}
    //else if ($("#ProductType").val() == 2 && $("#completedate").val() == "") {
    //    swal("Completion Time!", "Please Select Completion Time!", "error");
    //}
    else {
        isValid = true;
    }

    return isValid;
}


//Attach input width
$(function () {
    $('#btnSubmit').click(function () {
        if (Validate()) {
            $('#btnSubmit').prop('disabled', true);
            uiBlock();
            Insert();
        }
    });

    $('#btnEdit').click(function () {
        if (Validate()) {
            $('#btnSubmit').prop('disabled', true);
            uiBlock();
            Edit();
        }
    });
});

function Insert() {

    var json = JSON.stringify({ 'model': GetSubmitData() });
    ajaxCall("POST", json, "application/json; charset=utf-8", "/InquiryQuotation/SaveQuotation", "json", onSuccess, onFailure);
}


function Edit() {

    var json = JSON.stringify({ 'model': GetSubmitData() });
    ajaxCall("POST", json, "application/json; charset=utf-8", "/InquiryQuotation/UpdateQuotation", "json", onSuccess, onFailure);
}


function GetSubmitData() {

    var rows = [];
    $('#tblProduct tbody tr').each(function (i, n) {

        var $row = $(n);
        var ProductID = parseInt($row.find("#ProductID").text());
        var Description = $row.find("#Description").text();
        var UnitCode = $row.find("#UnitCode").text();
        var UnitPrice = parseFloat($row.find("#UnitPrice").text());
        var Qty = parseFloat($row.find("#Qty").text());
        var Total = parseFloat($row.find("#Total").text());

        rows.push({
            QuotationID: $("#QuotationID").val(),
            ProductID: ProductID,
            Description: Description,
            Unit: UnitCode,
            Qty: Qty,
            UnitPrice: UnitPrice,
            Total: Total
        });
    });

    var taxType = $("#TaxPercnt option:selected").text();
    taxType = taxType.split('|')[0];


    var data = {
        'QuotationID': $("#QuotationID").val(),
        'Code': $('#Code').val(),
        'StrCode': $('#StrCode').val(),
        'QuotationDate': $("#QuotationDate").val(),
        'InquiryID': $('#InquiryID').val(),
        'Amount': $('#subAmount').val(),
        'TaxPercnt': $('#TaxPercnt').val() || 0,
        'TaxAmount': $('#TaxAmount').val() || 0,
        'GSTPercnt': $('#GSTPercnt').val() || 0,
        'GSTAmount': $('#GSTAmount').val() || 0,
        'DiscPercnt': 0,
        'DiscAmount': 0,
        'TotalAmount': $('#TotalAmount').val(),
        'DeliveryDate': $("#deliverydate").val(),
        'CompleteDate': $("#completedate").val(),
        'AdvaneAmount': $("#AdvaneAmount").val(),
        'MidWayAmount': $("#MidWayAmount").val(),
        'CompletionAmount': $("#CompletionAmount").val(),
        'OfferFor': $("#OfferFor").val(),
        'QuotationType': $("#ProductType option:selected").text(),
        'TaxType': taxType,
        'OnAdvance': $("#OnAdvance").val(),
        'OnDelivery': $("#OnDelivery").val(),
        'OnCompletion': $("#OnCompletion").val(),
        'ValidUpto': $("#ValidUpto").val(),
        'tbl_InquiryQuotationDetails': rows
    };

    return data;

}


function onSuccess(Result) {
    if (Result == "Success") {
        uiUnBlock();
        window.location.href = '/InquiryQuotation/Index';
    }
    else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
    }
}

function onFailure(error) {
    uiUnBlock();
    $('#btnSubmit').prop('disabled', false);
    if (error.statusText == "OK") {
        swal("Bad Request!", "Some error Ocurred! Please Check Your Entries!", "error");
    }
    else {
        swal("Bad Request!", "Some error Ocurred! Please Check Your Entries!", "error");
    }
}
