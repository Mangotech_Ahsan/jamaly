﻿

$("#InquiryID").change(function () {
    ChangeInquiry();
});


function ChangeInquiry() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Inquiry/InquiryDetail?id=' + $("#InquiryID").val(),
        async: true,
        //data: JSON.stringify(json),
        success: function (data) {
            console.log(data);
            $("#CompanyName").val(data.CompanyName);
            $("#PhoneNo").val(data.PhoneNo);
            $("#Email").val(data.Email);
        },
        error: function (err) { console.log(err); }
    });
}


$("#ProductType").change(function () {
    var vehCodeID = $('#ProductType').val();
    $(".gstDiv").css("display", "none");

    if (vehCodeID == 1) {
        $("#TaxPercnt").val("17.00").change();
    }
    else if (vehCodeID == 2) {
        $("#TaxPercnt").val("13.00").change();
    }
    else if (vehCodeID == 3) {
        $(".gstDiv").css("display", "block");
        $("#TaxPercnt").val("17.00").change();
        $("#GSTPercnt").val("13.00").change();
        $("#TaxPercnt").prop("disabled", true);
        $("#GSTPercnt").prop("disabled", true);
    }

    getProducts(vehCodeID);
});


function getProducts(vehCodeID) {
    if (vehCodeID == "") {
        vehCodeID = -1;
    }
    var json = { "TypeID": vehCodeID };
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Product/GetProductsList?TypeID=' + vehCodeID,
        async: true,
        //data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("ddlPartNumber", data, true);
            $("#hdnProductTypeID").empty();
            $.each(data, function (key, value) {
                $("#hdnProductTypeID").append($("<option></option>").val(value.Value).html(value.Type));
            });
            
        },
        error: function (err) { console.log(err); }
    });
}

$('.Calc').on('input', function (e) {
    var Qty = parseFloat($('#Qty').val());
    var UnitPrice = parseFloat($('#UnitPrice').val());
    var units = parseFloat(Qty * UnitPrice).toFixed(2);
    $('#Total').val(units);
});


function ClearFields() {
    $('#ddlPartNumber').val('').change();
    $('#Description').val('');
    $('#unitCode').val('').change();
    $('#Qty').val('');
    $('#UnitPrice').val('');
    $('#Total').val('');
}


function GenerateTable(ProductID, Product, Description, unitCode, Qty, UnitPrice, Total) {
    if (ProductID > 0 && Description != "" && (parseFloat(Qty)) > 0 && Number(parseFloat(UnitPrice)) && Total > 0) {

        var markup = "<tr>\
            <td> <input type='checkbox' name='record'> </td> \
            <td id='hdnProductTypeID' hidden> " + $("#hdnProductTypeID option:selected").text() + "</td> \
            <td id='ProductID' hidden> " + ProductID + "</td> \
            <td id='Product'> " + Product + "</td> \
            <td id='Description' contenteditable=true> " + Description + "</td> \
            <td id='UnitCode' contenteditable=true> " + unitCode + " </td> \
            <td id='UnitPrice' contenteditable=true> " + UnitPrice + " </td> \
            <td id='Qty' contenteditable=true> " + Qty + " </td> \
            <td id='Total'>"+ Total + "</td> \
        </tr>";

        $("#tblProduct tbody").append(markup);

        ClearFields();
        CalcTotal();
    }
    else if (ProductID == "" || ProductID == "undefined" || ProductID == 0) { swal("Error", "Please Select Product!", "error"); }
    else if (Description == "" || Description == "undefined") { swal("Error", "Please Enter Description!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
    else if (UnitPrice == "" || UnitPrice == "undefined" || !Number(parseFloat(UnitPrice))) { swal("Error", "Please enter Unit Price!", "error"); }
    else { swal("Error", "Check Entries!", "error"); }
}

function BindData() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/InquiryQuotation/GetQuotationDetails?Qid=' + $("#QuotationID").val(),
        async: true,
        //data: JSON.stringify(json),
        success: function (data) {
            $.each(data, function (key, value) {
                GenerateTable(value.ProductID, value.Product, value.Description, value.unitCode, value.Qty, value.UnitPrice, value.Total);
            });

        },
        error: function (err) { console.log(err); }
    });
}


// Add Product in Table 
$("#addRow").click(function () {
    
    var ProductID = $("#ddlPartNumber").val();
    var Product = $("#ddlPartNumber option:selected").text();
    var Description = $("#Description").val();
    var unitCode = $("#unitCode").val();
    var Qty = $("#Qty").val();
    var UnitPrice = $("#UnitPrice").val();
    var Total = $("#Total").val();

    GenerateTable(ProductID, Product, Description, unitCode, Qty, UnitPrice, Total);

});

function CalcTotal() {
    var total = 0;
    var supplytotal = 0;
    var servicetotal = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = 0; //parseFloat($row.find('#Total').text());
        var ProductTypeID = $row.find('#hdnProductTypeID').text();
        if (ProductTypeID == " 1") {
            subTotal = parseFloat($row.find('#Total').text());
            supplytotal += subTotal;
        }
        else {
            subTotal = parseFloat($row.find('#Total').text());
            servicetotal += subTotal;
        }

        total += subTotal;
    });

    total = parseFloat(total).toFixed(2);
    $("#subAmount").val(total);

    var TaxPercnt = $("#TaxPercnt").val() || 0;
    var NewAm = parseFloat(TaxPercnt / 100) * total;
    $("#TaxAmount").val(parseFloat(NewAm).toFixed(2));

    var TaxPercnt1 = $("#GSTPercnt").val() || 0;
    var NewAm1 = parseFloat(TaxPercnt1 / 100) * total;
    $("#GSTAmount").val(parseFloat(NewAm1).toFixed(2));

    if ($("#ProductType").val() == 3) {
        NewAm = parseFloat(TaxPercnt / 100) * supplytotal;
        $("#TaxAmount").val(parseFloat(NewAm).toFixed(2));
        
        var NewAm1 = parseFloat(TaxPercnt1 / 100) * servicetotal;
        $("#GSTAmount").val(parseFloat(NewAm1).toFixed(2));
    }

    var sstamount = parseFloat($("#TaxAmount").val()) || 0;
    var gstamount = parseFloat($("#GSTAmount").val()) || 0;

    var finalamt = parseFloat(parseFloat(sstamount) + parseFloat(gstamount) + parseFloat(total)).toFixed(2);
    $("#TotalAmount").val(finalamt);
}

$('#TaxAmount').on('input', function (e) {
    var SubTotal = $("#subAmount").val() || 0;
    var TaxAmount = $("#TaxAmount").val() || 0;

    var NewAm = (parseFloat(TaxAmount) * 100) / SubTotal;
    $("#TaxPercnt").val(parseFloat(NewAm).toFixed(2));
    CalcTotal();
});

$('#TaxPercnt').on('input', function (e) {
    var TaxPercnt = $("#TaxPercnt").val() || 0;
    var SubTotal = $("#subAmount").val() || 0;

    var NewAm = parseFloat(TaxPercnt / 100) * SubTotal;
    $("#TaxAmount").val(parseFloat(NewAm).toFixed(2));
    CalcTotal();
});

$('#GSTAmount').on('input', function (e) {
    var SubTotal = $("#subAmount").val() || 0;
    var TaxAmount = $("#GSTAmount").val() || 0;

    var NewAm = (parseFloat(TaxAmount) * 100) / SubTotal;
    $("#GSTPercnt").val(parseFloat(NewAm).toFixed(2));
    CalcTotal();
});

$('#GSTPercnt').on('input', function (e) {
    var TaxPercnt = $("#GSTPercnt").val() || 0;
    var SubTotal = $("#subAmount").val() || 0;

    var NewAm = parseFloat(TaxPercnt / 100) * SubTotal;
    $("#GSTAmount").val(parseFloat(NewAm).toFixed(2));
    CalcTotal();
});


// Remove Selected Products 
function remove(input) {
    $("#tblProduct tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {
            $(this).parents("tr").remove();
            var row = $(this).closest("tr");
            CalcTotal();
        }
    });
}

// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

var validate = function (e) {
    var t = e.value;
    e.value = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 3)) : t;
}


$('#tblProduct').keyup(function (e) {
    $row = $(e.target).closest("tr");
    CalcTableData($row);
});

function CalcTableData($row) {
    var Description = $row.find("#Description").text();
    var UnitCode = $row.find("#UnitCode").text();
    var UnitPrice = parseFloat($row.find("#UnitPrice").text() || 0);
    var Qty = parseFloat($row.find("#Qty").text() || 0);

    if (($.isNumeric(Qty)) && ($.isNumeric(UnitPrice))) {
        var total = parseFloat(UnitPrice * Qty).toFixed(2) || 0;
        $row.find("#Total").text(total);
        CalcTotal();
    }
}

