﻿

var $addedProductIDs = [];
var GlobalProductSP = 0;
var GlobalProductQty = 0;
var GlobalProductSheets = 0;


function convertJsonDate(d) {
    var m = d.match(/\/Date\((\d+)\)\//);
    return m ? (new Date(+m[1])).toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: 'numeric' }) : d;
}


$("#ddlVehCode").change(function () {
    var vehCodeID = $('#ddlVehCode').val();
    GetProducts(vehCodeID);
});

function GetProducts(vehCodeID) {

    if (vehCodeID == "") {
        vehCodeID = -1;
    }
    var json = { "vehCodeId": vehCodeID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getProducts',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("ddlPartNumber", data, true);
        },
        error: function (err) { console.log(err); }
    });
}


$("#ddlPartNumber").change(function () {

    var pId = +$('#ddlPartNumber').val() || 0;
    console.log(pId)
    if (pId != "" || pId > 0) {
        GetDetail(pId);
    }
});


function GetDetail(pId) {
    var BranchId = $('#BranchID option:selected').val();
    var accID = $('#hdnAccountID').val();
    var json = { "productId": pId, "BranchID": BranchId };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            console.log(data)
            GlobalProductQty = data[0].qry.QtyPerUnit;
            GlobalProductSP = data[0].qry.SalePrice;
            GlobalProductSheets = data[0].qry.Sheets;
            $('#Stock').val(data[0].stock);
            $('#UnitPrice').val(data[0].qry.SalePrice);
            $("#Qty").val(data[0].qry.QtyPerUnit);
            $("#Sheets").val(data[0].qry.Sheets);

            CalcProductTotal();
        },
        error: function (err) { console.log(err); }
    });
}

$(".Calc").keyup(function () {
    CalcProductTotal();
});

$("#SalePrice").on('input', function () {
    CalcProductTotal();
})

function CalcProductTotal() {
    var qty = parseFloat($("#Qty").val() || 0);
    var sheets = +$("#Sheets").val() || 0;
    var SP = +$("#SalePrice").val() || 0;

    let total_sp = qty * SP;
    //$("#Total").val(total_sp);
    if (GlobalProductQty > 0) {
        qty = qty / GlobalProductQty;
        if (GlobalProductSheets > 0) {
            sheets = GlobalProductSheets * qty;
            $("#Sheets").val(sheets.toFixed(2))

        }
    }

    var saleprice = parseFloat($("#SalePrice").val() || 0);
    //var total = parseFloat(qty * saleprice).toFixed(2);

    $("#Total").val(total_sp);
}


$("#addRow").click(function () {
    var CategoryID = $("#ddlVehCode").val();
    var CategoryName = $("#ddlVehCode :selected").text();
    var ProductID = $("#ddlPartNumber").val();
    var ProductName = $("#ddlPartNumber :selected").text();
    var StoreStatus = $("#StoreStatus :selected").text();
    var StoreStatusID = $("#StoreStatus").val();
    var DeliveryStatus = $("#DeliveryStatus :selected").text();
    var OrderMachineID = $("#OrderMachine").val();
    var OrderMachine = $("#OrderMachine :selected").text();
    var Impression = $("#Impression").val();
    var DeliveryStatusID = $("#DeliveryStatus").val();
    var UnitCode = $("#unitCode").val();
    var Qty = parseFloat($("#Qty").val()) || 0;
    var Sheets = parseFloat($("#Sheets").val()) || 0;
    var UnitPrice = parseFloat($("#UnitPrice").val()) || 0;
    var SalePrice = +$("#SalePrice").val() || 0;
    var SubTotal = parseFloat($("#Total").val()) || 0;


    GenerateProductTable(CategoryID, CategoryName, ProductID, ProductName, UnitCode, Qty, UnitPrice, SalePrice, SubTotal, StoreStatusID, StoreStatus, DeliveryStatusID, DeliveryStatus, Sheets, OrderMachineID, OrderMachine, Impression);

});

$("#OrderMachine").change(() => {
    let OrderMachine = +$("#OrderMachine").val() || 0;
    if (OrderMachine > 0) {
        //  var json = { "OrderMachineID": OrderMachine };
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/OrderMachine/getMachineDetails?OrderMachineID=' + OrderMachine,
            async: true,
            // data: JSON.stringify(json),
            success: function (data) {
                console.log(data)

                $("#Impression").val(data.Impressions);

            },
            error: function (err) { console.log(err); }
        });
    }
})

$("#Qty").change(() => {
    let Qty = +$("#Qty").val() || 0;
    let GProductQty = +GlobalProductQty || 0;
    //let GProductSP = +GlobalProductSP || 0;
    let GProductSP = +$("#SalePrice").val() || 0;

    let totalSP = Qty * GProductSP;
    // $("#Total").val(totalSP.toFixed(2))

});

function GenerateProductTable(CategoryID, CategoryName, ProductID, ProductName, UnitCode, Qty, UnitPrice, SalePrice, Total, StkStatusID, StkStatus, DeliveryStatusID, DeliveryStatus, Sheets, OrderMachineID, OrderMachine, Impression) {

    if (ProductID > 0 && Qty > 0 && Total > 0 && UnitCode != "") {
        var index = $.inArray(ProductID, $addedProductIDs);
        if (index >= 0) {
            swal("Error", "Product Already Added!", "error");
        }
        else {
            let GeneralValue = 0;


            var markup = "<tr>\
                        <td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deleteRow(this)'/></td>\
                        <td id='VehicleCodeID' hidden>" + CategoryID + "</td>\
                        <td hidden> " + CategoryName + "</td>\
                        <td id='ProductID' hidden>" + ProductID + "</td>\
                        <td id='PartNo'>" + ProductName + "</td>\
                        <td id='hdnStoreStatusID' hidden>" + StkStatusID + "</td>\
                        <td >" + StkStatus + "</td>\
                        <td id='hdnDeliveryStatusID' hidden>" + DeliveryStatusID + "</td>\
                        <td >" + DeliveryStatus + "</td>\
                        <td id='hdnOrderMachineID' hidden>" + OrderMachineID + "</td>\
                        <td hidden>" + OrderMachine + "</td>\
                        <td hidden id='impression' hidden>" + Impression + "</td>\
                        <td id='UnitCode'>" + UnitCode + "</td>\
                        <td id='Qty' contenteditable='false'>" + Qty + "</td>\
                        <td id='totalsheets' contenteditable='false' hidden>" + Sheets + "</td>\
                        <td id='UnitPrice' contenteditable='false'>" + UnitPrice + "</td>\
                        <td id='UnitPrice' contenteditable='false'>" + SalePrice + "</td>\
                        <td id='SubTotal'>" + Total + "</td>\
                        <td hidden id='sheets'>" + GlobalProductSheets + "</td>\
                         <td  hidden id='jobSize' contenteditable='false'>" + GeneralValue + "</td>\
                         <td hidden  id='ups' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='gsm' contenteditable='false'>" + GeneralValue + "</td>\
                         <td hidden  id='rawSize' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='splitSheets' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='needWastageJobSize' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='rawSheetRqdQty' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='sheetRcvd' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='extaWastageSheet' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='extraQty' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='deliveredQty' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='balancedQty' contenteditable='false'>" + GeneralValue + "</td>\
                         </tr>";

            $("#tblProduct tbody").prepend(markup);
            $addedProductIDs.push(ProductID);
            ClearFields();
            CalcTotal();
        }
    }

    else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
    else if (UnitCode == "" || ProductID == "undefined") { swal("Error", "Please Select UnitCode!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
    else if (Total == "" || Total == "undefined") { swal("Error", "Please enter Amount!", "error"); }

}

function hideTableColumns() {
    //$('th:nth-child(2), tr td:nth-child(2)').hide();
    //$('table#tblProduct td:nth-child(2)').hide();
    //$('table#tblProduct th:nth-child(2)').hide();
    for (var i = 20; i <= 31; i++) {
        $('#tblProduct td:nth-child(' + i + ')').hide();
        $('#tblProduct th:nth-child(' + i + ')').hide();
    }

    //$('#tblProduct tbody tr').each(function (i, n) {
    //    var $row = $(n);
    //   // var pId = parseInt($row.find('#ProductID').text());

    //    console.log(n)

    //});

}

function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tblProduct").deleteRow(i);
    var row = $(r).closest("tr");
    var productID = row.find('#ProductID').text(); // find hidden id 
    var index = $.inArray(productID, $addedProductIDs);
    if (index >= 0) { $addedProductIDs.splice(index, 1); }
    CalcTotal();
}


function ClearFields() {
    $('#ddlVehCode').val(null).change();
    $('#ddlPartNumber').val(null).change();
    $('#StoreStatus').val(null).change();
    $('#DeliveryStatus').val(null).change();
    $('#OrderMachine').val(null).change();
    // $("#unitCode").val("").change();
    $("#Qty").val("");
    $("#Impression").val("");
    $("#Sheets").val("");
    $("#UnitPrice").val("");
    $("#Total").val("");
}

$('#tblProduct').keyup(function (e) {

    var $row = $(e.target).closest("tr");
    var Qty = parseFloat($row.find('#Qty').text());
    var UnitPrice = parseFloat($row.find('#UnitPrice').text());

    if (($.isNumeric(UnitPrice)) && ($.isNumeric(Qty))) {
        var amount = parseFloat(Qty * UnitPrice).toFixed(2);
        $row.find('#SubTotal').text(amount)
        CalcTotal();
    }
});


function CalcTotal() {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('#SubTotal').text());
        total += parseFloat(subTotal);
    });

    $("#subAmount").val(parseFloat(total).toFixed(2));
    var TaxAmount = parseFloat($("#TaxAmount").val()) || 0;
    var DiscAmount = parseFloat($("#DiscountAmount").val()) || 0;

    var NetTotal = parseFloat(total + TaxAmount - DiscAmount).toFixed(2);
    $("#totalAmount").val(NetTotal);
}

