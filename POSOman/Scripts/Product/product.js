﻿

var $addedProductIDs = [];
var GlobalProductSP = 0;
var GlobalProductQty = 0;

$("#ddlVehCode").change(function () {
    var vehCodeID = $('#ddlVehCode').val();
    GetProducts(vehCodeID);
});

function GetProducts(vehCodeID) {

    if (vehCodeID == "") {
        vehCodeID = -1;
    }
    var json = { "vehCodeId": vehCodeID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getProducts',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("ddlPartNumber", data, true);
        },
        error: function (err) { console.log(err); }
    });
}


$("#ddlPartNumber").change(function () {
   
    var pId = +$('#ddlPartNumber').val()||0;
    console.log(pId)
    if (pId != "" || pId > 0) {
        GetDetail(pId);
    }
});


function GetDetail(pId) {
    var BranchId = $('#BranchID option:selected').val();
    var accID = $('#hdnAccountID').val();
    var json = { "productId": pId, "BranchID": BranchId };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            console.log(data)
            GlobalProductQty = data[0].qry.QtyPerUnit;
            GlobalProductSP = data[0].qry.SalePrice;
            $('#Stock').val(data[0].stock);
            $('#UnitPrice').val(data[0].qry.SalePrice);
            $("#Qty").val(data[0].qry.QtyPerUnit);
            
            CalcProductTotal();
        },
        error: function (err) { console.log(err); }
    });
}

$(".Calc").keyup(function () {
    CalcProductTotal();
});

function CalcProductTotal() {
    var qty = parseFloat($("#Qty").val() || 0);
    var saleprice = parseFloat($("#UnitPrice").val() || 0);
    var total = parseFloat(qty * saleprice).toFixed(2);

    $("#Total").val(total);
}


$("#addRow").click(function () {
    var CategoryID = $("#ddlVehCode").val();
    var CategoryName = $("#ddlVehCode :selected").text();
    var ProductID = $("#ddlPartNumber").val();
    var ProductName = $("#ddlPartNumber :selected").text();
    var UnitCode = $("#unitCode").val();
    var Qty = parseFloat($("#Qty").val()) || 0;
    var UnitPrice = parseFloat($("#UnitPrice").val()) || 0;
    var SubTotal = parseFloat($("#Total").val()) || 0;


    GenerateProductTable(CategoryID, CategoryName, ProductID, ProductName, UnitCode, Qty, UnitPrice, SubTotal);

});

$("#Qty").change(() => {
    let Qty = +$("#Qty").val() || 0;
    let GProductQty = +GlobalProductQty || 1;
    let GProductSP = +GlobalProductSP || 0;

    let totalSP = (Qty / GProductQty) * GProductSP;
    $("#Total").val(totalSP.toFixed(2))

});

function GenerateProductTable(CategoryID, CategoryName, ProductID, ProductName, UnitCode, Qty, UnitPrice, Total) {

    if (ProductID > 0 && Qty > 0 && Total > 0 && UnitCode != "") {
        var index = $.inArray(ProductID, $addedProductIDs);
        if (index >= 0) {
            swal("Error", "Product Already Added!", "error");
        }
        else {

            var markup = "<tr>\
                        <td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deleteRow(this)'/></td>\
                        <td id='VehicleCodeID' hidden>" + CategoryID + "</td>\
                        <td hidden> " + CategoryName + "</td>\
                        <td id='ProductID' hidden>" + ProductID + "</td>\
                        <td id='PartNo'>" + ProductName + "</td>\
                        <td id='UnitCode'>" + UnitCode + "</td>\
                        <td id='Qty' contenteditable='true'>" + Qty + "</td>\
                        <td id='UnitPrice' contenteditable='true'>" + UnitPrice + "</td>\
                        <td id='SubTotal'>" + Total + "</td>\
                    </tr>";

            $("#tblProduct tbody").prepend(markup);
            $addedProductIDs.push(ProductID);
            ClearFields();
            CalcTotal();
        }
    }
    
    else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
    else if (UnitCode == "" || ProductID == "undefined") { swal("Error", "Please Select UnitCode!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
    else if (Total == "" || Total == "undefined") { swal("Error", "Please enter Amount!", "error"); }

}


function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tblProduct").deleteRow(i);
    var row = $(r).closest("tr");
    var productID = row.find('#ProductID').text(); // find hidden id 
    var index = $.inArray(productID, $addedProductIDs);
    if (index >= 0) { $addedProductIDs.splice(index, 1); }
    CalcTotal();
}


function ClearFields() {
    $('#ddlVehCode').val(null).change();
    $('#ddlPartNumber').val(null).change();
    $("#unitCode").val("").change();
    $("#Qty").val("");
    $("#UnitPrice").val("");
    $("#Total").val("");    
}

$('#tblProduct').keyup(function (e) {

    var $row = $(e.target).closest("tr");
    var Qty = parseFloat($row.find('#Qty').text());
    var UnitPrice = parseFloat($row.find('#UnitPrice').text());

    if (($.isNumeric(UnitPrice)) && ($.isNumeric(Qty))) {
        var amount = parseFloat(Qty * UnitPrice).toFixed(2);
        $row.find('#SubTotal').text(amount)
        CalcTotal();
    }
});

