﻿

var $addedProductIDs = [];
//var $addedRawProductIDs = [];
var itemWiseProducts = [];
var GlobalProductSP = 0;
var GlobalProductQty = 0;
var GlobalProductSheets = 0;


function convertJsonDate(d) {
    var m = d.match(/\/Date\((\d+)\)\//);
    return m ? (new Date(+m[1])).toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: 'numeric' }) : d;
}


$("#ddlVehCode").change(function () {
    var vehCodeID = $('#ddlVehCode').val();
    GetProducts(vehCodeID);
});

function GetProducts(vehCodeID) {

    if (vehCodeID == "") {
        vehCodeID = -1;
    }
    var json = { "vehCodeId": vehCodeID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getProducts',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("ddlPartNumber", data, true);
        },
        error: function (err) { console.log(err); }
    });
}


$("#ddlPartNumber").on('change',function (e) {
    e.preventDefault();
    var pId = +$('#ddlPartNumber').val() || 0;
    //console.log(pId)
    if (pId != "" || pId > 0) {
        $("#hdnItemIDForData").val(pId);

        $("#OMachineID").prop('disabled', false);
        GetDetail(pId);
    }
    else {
     //  console.log("in else")
        $("#hdnItemIDForData").val(null);
        //GetDropdown1('OMachineID', null, true);
        //$("#OMachineID").prop('disabled', true);
        ClearFields();
    }
});


//($("#ddlPartNumber").change(function (e) {
//    // The following condition 
//    // is the base case.
//    if (!x) {
//        return;
//    }
//    a(--x);
//}))(10);


function GetDetail(pId) {
    var BranchId = $('#BranchID option:selected').val();
    var json = { "productId": pId, "BranchID": BranchId };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
           // console.log(data)
            GlobalProductQty = data[0].qry.QtyPerUnit;
            GlobalProductSP = data[0].qry.SalePrice;
            GlobalProductSheets = data[0].qry.Sheets;
            $('#Stock').val(data[0].stock);
            $('#UnitPrice').val(data[0].qry.SalePrice);
            $('#SalePrice').val(data[0].SalePrice);
            $("#Qty").val(data[0].qry.QtyPerUnit);
            $("#Sheets").val(data[0].qry.TotalSheets);
            $("#unitCode").val(data[0].qry.UnitCodeID).trigger('change');
             GetDropdown1('OMachineID', data[0].orderMachine, true);

            CalcProductTotal();
        },
        error: function (err) { console.log(err); }
    });
}

$(".Calc").keyup(function () {
    CalcProductTotal();
});

$("#SalePrice").on('input', function () {
    CalcProductTotal();
})

function CalcProductTotal() {
    var qty = parseFloat($("#Qty").val() || 0);
    var sheets = +$("#Sheets").val() || 0;
    var SP = +$("#SalePrice").val() || 0;

    let total_sp = qty * SP;
    //$("#Total").val(total_sp);
    if (GlobalProductQty > 0) {
        qty = qty / GlobalProductQty;
        if (GlobalProductSheets > 0) {
            sheets = GlobalProductSheets * qty;
            $("#Sheets").val(sheets.toFixed(2))
        }
    }

    var saleprice = parseFloat($("#SalePrice").val() || 0);
    //var total = parseFloat(qty * saleprice).toFixed(2);

    $("#Total").val(total_sp);
}


$("#addRow").click(function () {
    var CategoryID = $("#ddlVehCode").val();
    var CategoryName = $("#ddlVehCode :selected").text();
    var ProductID = $("#ddlPartNumber").val();
    var ProductName = $("#ddlPartNumber :selected").text();
    var StoreStatus = $("#StoreStatus :selected").text();
    var StoreStatusID = $("#StoreStatus").val();
    var DeliveryStatus = $("#DeliveryStatus :selected").text();
    var OrderMachineID = $("#OMachineID").val()|| null;
    var OrderMachine = $("#OMachineID :selected").text();
    var Impression = $("#Impression").val();
    var DeliveryStatusID = $("#DeliveryStatus").val();
    var UnitCode = $("#unitCode option:selected").text()||"";
    var UnitCodeID = $("#unitCode").val();
    var Qty = parseFloat($("#Qty").val()) || 0;
    var Sheets = parseFloat($("#Sheets").val()) || 0;
    var UnitPrice = parseFloat($("#UnitPrice").val()) || 0;
    var SalePrice = +$("#SalePrice").val() || 0;
    var SubTotal = parseFloat($("#Total").val()) || 0;


    GenerateProductTable(CategoryID, CategoryName, ProductID, ProductName, UnitCode, UnitCodeID, Qty, UnitPrice, SalePrice, SubTotal, StoreStatusID, StoreStatus, DeliveryStatusID, DeliveryStatus, Sheets, OrderMachineID, OrderMachine, Impression);

});

$("#OrderMachine").change(() => {
    let OrderMachine = +$("#OrderMachine").val() || 0;
    if (OrderMachine > 0) {
        //  var json = { "OrderMachineID": OrderMachine };
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/OrderMachine/getMachineDetails?OrderMachineID=' + OrderMachine,
            async: true,
            // data: JSON.stringify(json),
            success: function (data) {
                console.log(data)

                $("#Impression").val(data.Impressions);

            },
            error: function (err) { console.log(err); }
        });
    }
})

$("#Qty").change(() => {
    let Qty = +$("#Qty").val() || 0;
    let GProductQty = +GlobalProductQty || 0;
    //let GProductSP = +GlobalProductSP || 0;
    let GProductSP = +$("#SalePrice").val() || 0;
   
    let totalSP = Qty * GProductSP;
   // $("#Total").val(totalSP.toFixed(2))

});

function GenerateProductTable(CategoryID, CategoryName, ProductID, ProductName, UnitCode, UnitCodeID, Qty, UnitPrice, SalePrice, Total, StkStatusID, StkStatus, DeliveryStatusID, DeliveryStatus, Sheets, OrderMachineID, OrderMachine, Impression) {

    ProductName = ProductName.replace(/[^\w\s]/gi, '');
    console.log(ProductName);
    if (ProductID > 0 && Qty > 0 && Total > 0 && UnitCode != "") {
        var index = $.inArray(ProductID, $addedProductIDs);
        if (index >= 0) {
            swal("Error", "Product Already Added!", "error");
        }
        else {
            let GeneralValue = 0;


            var markup = "<tr>\
                        <td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deleteRow(this)'/></td>\
                        <td id='VehicleCodeID' hidden>" + CategoryID + "</td>\
                        <td hidden> " + CategoryName + "</td>\
                        <td id='ProductID' hidden>" + ProductID + "</td>\
                        <td id='PartNo'>" + ProductName + "</td>\
                        <td id='hdnStoreStatusID' hidden>" + StkStatusID + "</td>\
                        <td hidden>" + StkStatus + "</td>\
                        <td id='hdnDeliveryStatusID' hidden>" + DeliveryStatusID + "</td>\
                        <td hidden>" + DeliveryStatus + "</td>\
                        <td id='hdnOrderMachineID' hidden>" + OrderMachineID + "</td>\
                        <td >" + OrderMachine + "</td>\
                        <td hidden id='impression'>" + Impression + "</td>\
                        <td id='UnitCode'>" + UnitCode + "</td>\
                        <td id='UnitCodeID' hidden>" + UnitCodeID + "</td>\
                        <td id='Qty' contenteditable='false'>" + Qty + "</td>\
                        <td id='totalsheets' contenteditable='false' hidden>" + Sheets + "</td>\
                        <td id='UnitPrice' contenteditable='false'>" + UnitPrice + "</td>\
                        <td id='SalePrice' contenteditable='false'>" + SalePrice + "</td>\
                        <td id='SubTotal'>" + Total + "</td>\
                        <td id='btnAddPackages'><a style='color:#1D9293' id='addPackageHref' data-toggle='modal' data-target='#exampleModal' onclick='getPackagesModal("+ ProductID + ",\"" + ProductName + "\"," + Qty + ")' >Add Packages</a> | <text>same as item qty</text> <input type='checkbox' id='chkbx' onchange = 'sameAsPackageQty(" + ProductID + ",\"" + ProductName + "\"," + Qty +",event)'  /></td>\
                        <td hidden id='sheets'>" + GlobalProductSheets + "</td>\
                         <td  hidden id='jobSize' contenteditable='false'>" + GeneralValue + "</td>\
                         <td hidden  id='ups' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='gsm' contenteditable='false'>" + GeneralValue + "</td>\
                         <td hidden  id='rawSize' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='splitSheets' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='needWastageJobSize' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='rawSheetRqdQty' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='sheetRcvd' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='extaWastageSheet' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='extraQty' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='deliveredQty' contenteditable='false'>" + GeneralValue + "</td>\
                         <td  hidden id='balancedQty' contenteditable='false'>" + GeneralValue + "</td>\
                         </tr>";

            $("#tblProduct tbody").prepend(markup);
            $addedProductIDs.push(ProductID);
            ClearFields();
            CalcTotal();
        }
    }

    else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
    else if (UnitCode == "" || ProductID == "undefined") { swal("Error", "Please Select UnitCode!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
    else if (Total == "" || Total == "undefined") { swal("Error", "Please enter Amount!", "error"); }

}

sameAsPackageQty = (productId,name,qty,event) => {
    console.log(name)
    console.log(qty)
    console.log(productId)
    console.log(event)
}


function getPackagesModal(ProductID, ProductName, Qty) {
    
    $("#PkgQty").val(0);
    $("#BalQty").val(0);
    //$("#ItemQty").val(0);
   //console.log(Qty)
    $("#rawProductQty").val(null);
    $("#rawProductID").val(null).trigger('change');
 //   console.log("in function->" + ProductID)
    let pID = +ProductID || 0;
    $("#fileInput").val("");
    if (pID > 0) {

        $("#lbl_itemID").text(ProductName);
        $("#ItemQty").val(Qty);
        $("#hdnItemID").val(pID);
        if (itemWiseProducts.length) {
            const found = itemWiseProducts.some(el => el.ItemID === pID);
            console.log('found array->');
           
            if (!found) {
                console.log(found)
                console.log(itemWiseProducts)
                $("#addPackagesTable tbody").empty();
                //swal('', 'not found', 'error');
                console.log('not found')
            }
            else {
                console.log(found)
                console.log(itemWiseProducts)
                var newArr = itemWiseProducts.filter(x => x.ItemID === pID);
                console.log('pID->' + pID);
              //  console.log(newArr)
                if (newArr.length) {
                    $("#addPackagesTable tbody").empty();
                    for (var i = 0; i < newArr.length; i++) {
                        var markup = "<tr>\
                             <td hidden  id='rowID'> " + newArr[i].RowID  + " </td> \
                             <td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deletePackageRow(this,"+ newArr[i].ItemID + "," + newArr[i].RowID +")'/></td>\
                   <td  id='hdnRawProduct_td'> "+ newArr[i].Description + " </td> \
                   <td id='hdnRawProductQty_td'>  "+ newArr[i].Qty + " </td> \
                    </tr>";

                        $("#addPackagesTable tbody").append(markup);
                    }
                }
              
            }
           
        }
        
    }
    else {
        $("#lbl_itemID").val(null);
        $("#hdnItemID").val(null);

    }

   
}

function calcTotalItemQty() {
    var itemQty = +$("#ItemQty").val() || 0;
    var pkgQty = 0;
    var balQty = 0;
    $('#addPackagesTable tbody tr').each(function (i, n) {
        var $row = $(n);
        var pkg_qty = parseInt($row.find('#hdnRawProductQty_td').text()) || 0;
        if (pkg_qty <= itemQty) {
            pkgQty += pkg_qty;
        }

    });

    balQty = itemQty - pkgQty;
    $("#PkgQty").val(pkgQty);
    $("#BalQty").val(balQty);

}
function populateFromExcel(data) {
    let itemID = +$("#hdnItemID").val() || 0;
    if (itemID > 0) {
        itemWiseProducts = itemWiseProducts.filter(obj => obj.ItemID !== itemID);
        for (const i of data) {
            itemWiseProducts.push({ RowID: i.RowID, ItemID: itemID, Description: i.Description, Qty: i.Qty });
        }

        if (itemWiseProducts.length) {
            
           
                var newArr = itemWiseProducts.filter(x => x.ItemID === itemID);

                if (newArr.length) {
                    $("#addPackagesTable tbody").empty();

                    // console.log(newArr)
                    for (var i = 0; i < newArr.length; i++) {

                        // console.log(itemWiseProducts[i].ItemID);
                        var markup = "<tr>\
                     <td hidden id='rowID'> " + newArr[i].RowID + " </td> \
                     <td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deletePackageRow(this,"+ newArr[i].ItemID + "," + newArr[i].RowID + ")'/></td>\
                     <td  id='hdnRawProduct_td'> "+ newArr[i].Description + " </td> \
                   <td id='hdnRawProductQty_td'>  "+ newArr[i].Qty + " </td> \
                    </tr>";

                        $("#addPackagesTable tbody").append(markup);
                    }
                    calcTotalItemQty();


                }
            
        }
    }
}

$("#addPackageRowBtn").click(function () {
   // let rawPID = +$("#rawProductID").val() || 0;
    let rawProduct = $("#rawProductID").val() || '-';
    let itemID = +$("#hdnItemID").val() || 0;
    let rawProductQty = +$("#rawProductQty").val() || 0;
    if ( itemID > 0) {
        //const found = itemWiseProducts.some(el =>  el.ItemID === itemID);
        //if (!found) {

        var id = 1;
        if (itemWiseProducts.length) {
            //id = itemWiseProducts.length + 1;
            while (itemWiseProducts.some(o => o.RowID === id)) {
                id = itemWiseProducts.length + 1;
            }

            
        }
        console.log("****test 1****itemWiseProducts before pushing****");
        console.log(itemWiseProducts);
        itemWiseProducts.push({ RowID: id, ItemID: itemID, Description: rawProduct, Qty: rawProductQty });

        if (itemWiseProducts.length) {

            var chk = overrideTotalItemQty();
            console.log("chk->" + chk);
            if (chk >= 0) {

                if (chk == 1) {
                    //swal("Package Qty", "package qty is less than item qty", "warning");
                }
                var newArr = itemWiseProducts.filter(x => x.ItemID === itemID);
                console.log("****test 1****itemWiseProducts after pushing and creating new array itemID wise****");
                console.log(itemID)
                console.log(newArr);
                if (newArr.length) {
                    $("#addPackagesTable tbody").empty();

                    // console.log(newArr)
                    for (var i = 0; i < newArr.length; i++) {

                        // console.log(itemWiseProducts[i].ItemID);
                        var markup = "<tr>\
                     <td hidden id='rowID'> " + newArr[i].RowID + " </td> \
                     <td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deletePackageRow(this,"+ newArr[i].ItemID + "," + newArr[i].RowID + ")'/></td>\
                     <td  id='hdnRawProduct_td'> "+ newArr[i].Description + " </td> \
                   <td id='hdnRawProductQty_td'>  "+ newArr[i].Qty + " </td> \
                    </tr>";

                        $("#addPackagesTable tbody").append(markup);
                    }
                    calcTotalItemQty();


                }
            }
            else {
                itemWiseProducts = itemWiseProducts.filter(obj => obj.RowID !== id);

                swal("Package Qty", "qty must be equal to item qty", "error");
            }
               
            }
        //}
        //else
        //    swal("Error", "product already exists", "error");
    }
    else {
        swal("Error", "nothing selected", "error");
    }
    
})

//function SumTotalQty(key) {
//    return this.reduce((a, b) => a + (b[key] || 0), 0);
//}

function overrideTotalItemQty() {

    var totalQty = 0;
    var boolChk = 0;

    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = parseInt($row.find('#ProductID').text());
        var Qty = +$row.find('#Qty').text()||0;

        console.log(pId)
        console.log("Qty->"+Qty)
        if (pId > 0) {
            console.log("in table")
            
            var arr = itemWiseProducts.filter(x => x.ItemID === pId);
            console.log(arr)
            if (arr.length) {
                console.log("arr exist")
                
                totalQty = arr.reduce((accum, item) => accum + item.Qty, 0);
                console.log("totalQty->"+totalQty)
                if (+totalQty > +Qty) {
                    console.log("in-if");

                    boolChk = -1;
                }
                else if (+totalQty < +Qty) {
                    boolChk = 1;
                }

                
                //$row.find('#Qty').text(totalQty) // change item qty
            }

         

        }
        
       

    });
    return boolChk;
}

function hideTableColumns() {
    //$('th:nth-child(2), tr td:nth-child(2)').hide();
    //$('table#tblProduct td:nth-child(2)').hide();
    //$('table#tblProduct th:nth-child(2)').hide();
    for (var i = 21; i <= 33; i++) {
        $('#tblProduct td:nth-child('+i+')').hide();
        $('#tblProduct th:nth-child(' + i +')').hide();
    }
   
    //$('#tblProduct tbody tr').each(function (i, n) {
    //    var $row = $(n);
    //   // var pId = parseInt($row.find('#ProductID').text());
       
    //    console.log(n)

    //});
   
}

function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tblProduct").deleteRow(i);
    var row = $(r).closest("tr");
    var productID = row.find('#ProductID').text(); // find hidden id 
    var index = $.inArray(productID, $addedProductIDs);
    if (index >= 0) { $addedProductIDs.splice(index, 1); }
    CalcTotal();
}


function deletePackageRow(r,ItemID,RowID) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("addPackagesTable").deleteRow(i);
    var row = $(r).closest("tr");

   
    var desc = row.find('#hdnRawProduct_td').text(); // find hidden id
   // var rowID = row.find('#RowID').text()||null; // find hidden id

   // console.log(rowID)
    console.log(ItemID)
    console.log("before deleting...")
    console.log(itemWiseProducts)
    //itemWiseProducts = itemWiseProducts.filter(obj => obj.ItemID !== ItemID && obj.Description != desc);
   // console.log(String(desc));

    var data = itemWiseProducts.filter(obj => obj.RowID === RowID);
    itemWiseProducts = itemWiseProducts.filter(obj => obj.RowID !== RowID);

    console.log("deleting object...")
    console.log(data)
    console.log("after deleting...")
    console.log(itemWiseProducts)
    overrideTotalItemQty();

    calcTotalItemQty();
   
}

function ClearFields() {
    //console.log("call stack")
    //$('#ddlVehCode').val(null).change();
    $('#ddlPartNumber').val(null);
   // $('#StoreStatus').val(null).change();
   // $('#DeliveryStatus').val(null).change();
   // $('#OrderMachine').val(null).change();
    $('#unitCode').val(null);
    $("#Qty").val("");
    $("#SalePrice").val("");
    $("#Impression").val("");
    $("#Sheets").val("");
    $("#UnitPrice").val("");
    $("#Total").val("");
}

$('#tblProduct').keyup(function (e) {

    var $row = $(e.target).closest("tr");
    var Qty = parseFloat($row.find('#Qty').text());
    var UnitPrice = parseFloat($row.find('#UnitPrice').text());

    if (($.isNumeric(UnitPrice)) && ($.isNumeric(Qty))) {
        var amount = parseFloat(Qty * UnitPrice).toFixed(2);
        $row.find('#SubTotal').text(amount)
        CalcTotal();
    }
});


function CalcTotal() {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('#SubTotal').text());
        total += parseFloat(subTotal);
    });

    $("#subAmount").val(parseFloat(total).toFixed(2));
    var TaxAmount = parseFloat($("#TaxAmount").val()) || 0;
    var DiscAmount = parseFloat($("#DiscountAmount").val()) || 0;

    var NetTotal = parseFloat(total + TaxAmount - DiscAmount).toFixed(2);
    $("#totalAmount").val(NetTotal);
}

