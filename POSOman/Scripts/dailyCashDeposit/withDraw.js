﻿var balance = "";
$(function () {
    $('#btnSave').click(function () {
        var FromBank_ID = 109;
        var ToBank_ID = 1;//$('#ToBank option:selected').val();
        var voucherDate = $('#VoucherDate').val();
        
        var amount = $('#Amount').val();
        var BranchID = $('#BranchID option:selected').val();
        if (BranchID == "" || BranchID == 0 || BranchID == undefined) {
            
            swal("Branch", "Please Select Branch!", "error");
            //alert("Please Select Vendor");
        }
       else if (FromBank_ID == "" || FromBank_ID == 0) {
            swal("Account", "Please Select From Account!", "error");
        }
        
        else if (ToBank_ID == "" || ToBank_ID == 0) {
            swal("Account", "Please Select To Account!", "error");
        }
        else if (FromBank_ID == ToBank_ID) {
            swal("Same Account", "Please Select different Account!", "error");
        }
        
        else if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if (amount == "" || amount == 0) {
            swal("Amount", "Please Enter Amount!", "error");
            //console.log(PurchaseDate + "date");
        }
        
        else {
        $('#btnSave').prop('disabled', true);
        uiBlock();
            insert();
        }
    });

});

function insert() {    
    var payLog = "";
    var totalAmount = 0;          
    var FromBank_ID = 109;
    var ToBank_ID = 1;
    var BranchID = $('#BranchID option:selected').val();
    var desc = $('#Description').val();
    var referenceNo = $('#ReferenceNo').val();
    var voucherDate = $('#VoucherDate').val();        
    var amount = parseFloat($('#Amount').val());  
            var data = {
                'BranchID': BranchID,
                'Description': desc,
                'Amount': amount,
                'FromBankID': FromBank_ID,
                'ToBankID': ToBank_ID,
                'UserReferenceID':referenceNo,
                'VoucherDate': voucherDate,                                
            };            
            var json = JSON.stringify({ 'model': data });
            //console.log(json);            
            ajaxCall("POST", json, "application/json; charset=utf-8", "/DailyPOSCashDeposit/SaveWithDrawEntry", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result > 0) {                    
                    window.location.href = '/DailyPOSCashDeposit/WithDrawList';
                }
                else {
                uiUnBlock();
                $('#btnSave').prop('disabled', false);        
                    swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                }
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    window.location.reload();
            }
        
    
    
}
