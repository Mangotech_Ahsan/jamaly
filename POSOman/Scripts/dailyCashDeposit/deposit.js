﻿
var balance = "";
$(function () {
    $('#btnSave').click(function () {
        var FromBank_ID = 1;  // From Cash 
        var ToBank_ID = 109;  // To POS Cash 
        var voucherDate = $('#VoucherDate').val();
        var amount = $('#Amount').val();
        var BranchID = $('#BranchID option:selected').val();
        
        if (BranchID == "" || BranchID == 0 || BranchID == undefined) {
            console.log(BranchID + "dw");
            swal("Branch", "Please Select Branch!", "error");
        }
       else if (FromBank_ID == "" || FromBank_ID == 0) {
            swal("From Bank", "Please Select From Bank!", "error");
        }
        else if (ToBank_ID == "" || ToBank_ID == 0) {
            swal("To Bank", "Please Select To Bank!", "error");
        }           
        else if (FromBank_ID == ToBank_ID) {
            swal("Same Bank", "Please Select different Bank!", "error");
        }

        else if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if (amount == "" || amount == 0) {
            swal("Amount", "Please Enter Amount!", "error");
            //console.log(PurchaseDate + "date");
        }

        else {
            $('#btnSave').prop('disabled', true);
            uiBlock();
            insert();
        }
    });

});

function insert() {
    var payLog = "";
    var totalAmount = 0;
    var FromBank_ID = 1;
    var ToBank_ID = 109;
    var referenceAccountID = $('#referenceAccount option:selected').val();
    var BranchID = $('#BranchID option:selected').val();
    var desc = $('#Description').val();
    var referenceNo = $('#ReferenceNo').val();
    var voucherDate = $('#VoucherDate').val();
    var amount = parseFloat($('#Amount').val());
    var data = {
        'BranchID': BranchID,
        'Description': desc,
        'Amount': amount,
        'FromBankID': FromBank_ID,
        'ToBankID': ToBank_ID,
        'UserReferenceID': referenceNo,
        'VoucherDate': voucherDate,
    };
    var json = JSON.stringify({ 'model': data });
    //console.log(json);            
    ajaxCall("POST", json, "application/json; charset=utf-8", "/DailyPOSCashDeposit/SaveDepositEntry", "json", onSuccess, onFailure);
    function onSuccess(Result) {
        if (Result > 0) {
            window.location.href = '/DailyPOSCashDeposit/DepositList';
        }
       
        else {
            uiUnBlock();
            $('#btnSave').prop('disabled', false);
            swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
        }
    }
    function onFailure(error) {
        if (error.statusText == "OK")
            //alert("success");
            //console.log(error);
            window.location.reload();
    }



}
