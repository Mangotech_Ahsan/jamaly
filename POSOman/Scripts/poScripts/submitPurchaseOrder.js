﻿var controlId = 0;
var vehControlId = 0;
var tmpOrderID = 0;
var tOrderID = 0;
var price = 0;
var perPieceExp = 0;
var perProductExp = 0;
var expensePercentage = 0;
var exUnitPrice = 0;
$("#Bank").prop("disabled", true);

// Bind Unsaved Order Data , Payment and Vendor Details if Exist
//function bindTempData(data) {
//    vendorAccountID = data.qry[0].AccountID;
//    $('#AccountID').val(vendorAccountID).trigger('change.select2');

//    document.getElementById('TotalAmount').value = data.qry[0].TotalAmount;
//    document.getElementById('BranchID').value = data.qry[0].BranchID;
//    $("#Currency option:contains(" + data.qry[0].Currency + ")").attr("selected", true);
//}
function bindTempData(data) {
    $('#InvoiceNo').val(data.qry[0].InvoiceNo);
    vendorAccountID = data.qry[0].AccountID;
    $('#AccountID').val(vendorAccountID).trigger('change.select2');
    document.getElementById('TotalAmount').value = data.qry[0].TotalAmount;
    if (data.qry[0].VAT > 0)
    { document.getElementById('vatInput').value = data.qry[0].VAT; }
    document.getElementById('PaymentType').value = data.qry[0].PaymentTypeID;
    $("#Currency option:contains(" + data.qry[0].Currency + ")").attr("selected", true);    
    if (data.qry[0].PaymentStatus != "") {
        $("#PaymentStatus option:contains(" + data.qry[0].PaymentStatus + ")").attr("selected", true);
    }
}

// Get Unsaved order and its details
function getPurchaseOrderDetails() {
    tOrderID = $('#tOrderID').val();
    //console.log("torderid" + tOrderID);
    ajaxCall("GET", { 'tOrderID': tOrderID }, "application/json; charset=utf-8", "/Purchase/getPurchaseOrder",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        $('#tblProduct').find('tbody').empty();

        if (ien > 0) {
            bindTempData(data);
            $("#exRate").val(data.qry[0].CurrencyRate);
           // console.log(data.qry[0].CurrencyRate + " Curr Exch Rate");
            var currencyID = 1;//$("#Currency").val();
            for (var i = 0; i < ien; i++) {
                var row = '';
                var description = data.qry[i].Description;
                var substNo =  (data.qry[i].SubstituteNo);
                var unitCode = (data.qry[i].UnitCode); 
                var unitPerCarton = parseInt(data.qry[i].UnitPerCarton) || 1;
                console.log();
                var vehCode = (data.qry[i].VehicleCode);
                var prodID = data.qry[i].ProductID;
                var PartNO = data.qry[i].PartNo;
                var Qty = data.qry[i].Qty;;
                var levelID = data.qry[i].LevelID;
                var isPacket = data.qry[i].IsPack;
                var SalePrice = data.qry[i].SalePrice;
                var SubTotal = $("#SubTotal").val(); // 8
                var isMinor = '<input type="hidden" id="isMinor" value="' + $("#isMinor").val() + '"/>';
                var levelIDField = '<input type="hidden" id="LevelID" value="' + levelID + '"/>';
                var isPack = '<input type="hidden" id="isPacket" value="' + isPacket + '"/>';
                var totalQty = parseFloat((Qty * unitPerCarton));
                
                var totalWeight = totalQty;
                var UnitPrice = data.qry[i].UnitPrice;
                var SubTotal = data.qry[i].Total; 
                var pid = '<input type="hidden" id="productID" value="' + prodID + '"/>';
                var unitCTN = '<input type="hidden" id="unitPerCTN" value="' + unitPerCarton + '"/>';
                var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + isPack + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td contenteditable='true' id=Location hidden>" + Location + "</td><td id='unitCode'>" + unitCode + "</td><td contenteditable='true' id='ProductQty'>" + Qty + unitCTN + "</td><td contenteditable='true' id='ProductCostPrice'>" + levelIDField + parseFloat(UnitPrice).toFixed(2) + "</td><td id='exchangePrice' hidden>" + parseFloat(exUnitPrice).toFixed(2) + "</td><td>" + isMinor + parseFloat(totalWeight).toFixed(2) + "</td><td contenteditable='true' id='ProductSalePrice'>" + SalePrice + "</td><td id='ProductSubTotal'>" + parseFloat(SubTotal).toFixed(2) + "</td></tr>";

                $("#tblProduct tbody").append(markup);
                $tableItemCounter++;
                $addedProductIDs.push(prodID);
                
            }
            calcTotal();
        }
    }
    function onFailure(err) {
        console.log(err);
    }
}
function checkNull(value) {
    return JSON.stringify(value).replace(/null/g, "\"\"");
}

$(function () {
    getPurchaseOrderDetails();   
})

function updatePurchaseOrderStatus() {

    var json = { "tOrderID": tOrderID, "invoiceNo": $('#InvoiceNo').val() };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/changePOStatus',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            console.log("Status changed successfully!");
        },
        error: function (err) { console.log(err); }
    });
}
