﻿$tableItemCounter = 0;
var $addedProductIDs = [];
var QtyinPack = 0;
var PackLength = 0;
var PrUntLnth = 0;
var OrderId = $('#InvOrderId').val();
var Count = 0;
var DatE = null;
$("#ddlVehCode").prop('disabled', true);
$("#ddlPartNumber").prop('disabled', true);
$("#ddlproductname").prop('disabled', true);
$("#addRow").prop('disabled', true);
$("#BranchID").prop('disabled', true);
$("#btnRemove").prop('disabled', false);

GetEditVendors(OrderId);
//Edit Vendor

function GetEditVendors(OrderId) {

    if (OrderId == "")
    { OrderId = -1; }
    var json = { "OrderId": OrderId };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/GetEditInvoiceVendor',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            var num = (data[0].PurchaseDate).match(/\d+/g);
            var date = new Date(parseFloat(num));
            if (data[0].chequeDate != null) {
                 var num1 = (data[0].chequeDate).match(/\d+/g);
            var date1 = new Date(parseFloat(num));
                document.getElementById('chqDate').valueAsDate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate(),date.getUTCHours()+5,date.getMinutes());
            }
            
            document.getElementById('PurchaseDate').valueAsDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(),date.getUTCHours()+5,date.getMinutes());
            $('#AccountID').val(data[0].Qry.Value).trigger('change.select2');
            $('#BranchID').val(data[0].Qry1.Value).trigger('change.select2');
            var vendorID = $('#AccountID').val();
            $('#hdnAccountID').val(vendorID);               
            getVendorDetail(vendorID);
            $('#PaymentStatus').val(data[0].PayStatus).trigger('change.select2');
            //console.log(data[0].PayStatus);
            if (data[0].PayStatus == 3) {
                $("#PaymentType").prop('disabled', true);
            }
            $("#Bank").val(data[0].Bank.Value).trigger('change.select2');
            DatE = data[0].ChqDate;


            // $('#chqDate').val(DatE.dateFormat("dd/MM/yy"));
            $('#chqNumber').val(data[0].Cheque);
            $('#amountPaid').val(data[0].AmountPaid);
            $('#vatAmount').val(data[0].Tax);
            //$('#finalAmountWithVAT').val(data[0].FinalAmount);
            $('#TotalAmount').val(data[0].TotalAmount);
            $('#subAmount').val(data[0].SubAmount);
            $('#discInput').val(data[0].Discount);
            $('#InvoiceNo').val(data[0].InvNo);
            $('#expenseInput').val(data[0].Expenses);
            $('#ExpenseDesc').val(data[0].Description);
            $('#RefNo').val(data[0].ReferenceNo);

            // console.log(DatE + "ch20Date");

            ////////////populate product edit table/////////////
            data[0].ProductsList.forEach(ListProduct);
           
            if (data[0].Paytype == "" || data[0].Paytype == null) {
            }
            else {
                $('#PaymentType').val(data[0].Paytype.Value).trigger('change.select2');
                if (data[0].Paytype.Value == 1) {
                    $("#Bank").prop("disabled", true);
                    $("#chqNumber").prop("disabled", true);
                    $("#chqDate").prop("disabled", true);
                }
                else if (data[0].Paytype.Value == 2) {
                    $("#Bank").prop("disabled", false);
                    $("#chqNumber").prop("disabled", true);
                    $("#chqDate").prop("disabled", true);
                }
                else {
                    $("#Bank").prop("disabled", false);
                    $("#chqNumber").prop("disabled", false);
                    $("#chqDate").prop("disabled", false);
                }
            }
        },
        error: function (err) { console.log(err); }
    });
}
//function for edit product list
////////////
function ListProduct(item) {
    var unitPerCarton = parseInt(item.UnitPerCarton);
    var vehCode = item.Cat;    
    var ProductID = item.ProductID; // hidden
    var PartNO = item.PartNo;  // 1
    var UnitCode = item.UnitCode; // 3
    var Qty = item.Qty; // 5
    var UnitPrice = item.UnitPrice;
    
    var SalePrice = item.SalePrice;
    var SubTotal = item.PTotal;
    var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';
    var isMinor = '<input type="hidden" id="isMinor" value="' + item.IsMinor + '"/>';
    var levelIDField = '<input type="hidden" id="LevelID" value="' + item.LevelID + '"/>';
    var isPack = '<input type="hidden" id="isPacket" value="' + item.IsPack + '"/>';   
    var packet = item.Packet;
    var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + isPack + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td contenteditable='true' id=Location hidden>" + Location + "</td><td id='unitCode' hidden>" + UnitCode + "</td><td contenteditable='true' id='ProductQty'>" + Qty + "</td><td id='Packet' hidden>" + packet + "</td><td contenteditable='true' id='ProductCostPrice'>" + parseFloat(UnitPrice).toFixed(2) + "</td><td id='exchangePrice' hidden>" + parseFloat(0).toFixed(2) + "</td><td contenteditable='true' id='ProductSalePrice'>" + SalePrice + "</td><td id='ProductSubTotal'>" + parseFloat(SubTotal).toFixed(2) + "</td></tr>";
    
    $("#tblProduct tbody").append(markup);
    $tableItemCounter++;
    $addedProductIDs.push(item.ProductID);
    proIdEdit = item.ProductID;
    getNewTotal();
}
function GetEditProductDetail(OrderId, ProductID, BranchID) {

    var json = { "OrderId": OrderId, "ProductID": ProductID, "BranchID": BranchID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/GetEditProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {                       
            var prodID = parseInt(data[0].ProductsList.ProductID);
            $('#ddlPartNumber').val(prodID).trigger('change.select2');;
            $('#ddlVehCode').val(data[0].ProductsList.CatID).trigger('change.select2');
            $('#SubTotal').val(data[0].ProductsList.PTotal);
            $('#SalePrice').val(data[0].ProductsList.SalePrice);
            var item = data[0].ProductsList;
            var qtyBox = parseFloat(data[0].ProductsList.Qty);
            var qty = qtyBox;
            var UnitPrice = parseFloat(data[0].ProductsList.unitPrice);
            if (data[0].ProductsList.IsPack == true) {
                $('#Packet').val(data[0].ProductsList.Packet);
                $("#Packet").prop('disabled', false);
            }
            else {
                $('#Packet').val();
                $("#Packet").prop('disabled', true);
            }
            
            $('#Qty').val(qty);
            $('#UnitPrice').val(UnitPrice);
            getDetail(data[0].ProductsList.ProductID)
            var VatInput = data;
            $("#addRow").prop('disabled', false);
            $("#btnRemove").prop('disabled', true);
        },
        error: function (err) { console.log(err); }
    });
}
function EditProduct(input) {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {

            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            var productID = row.find('input[id*="productID"]').val(); // find hidden id 

            var index = $.inArray(productID, $addedProductIDs);
            if (index >= 0) { $addedProductIDs.splice(index, 1); }
            var BranchID = $("#BranchID").val();
            GetEditProductDetail(OrderId, productID, BranchID);
            //console.log($tableItemCounter + "EditCounter");
            Count = $tableItemCounter;
            calcTotal();
        }
    });
}
// Remove Selected Products 
function remove(input) {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {
            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            calcTotal();
            var productID = row.find('input[id*="productID"]').val(); // find hidden id 
            var index = $.inArray(productID, $addedProductIDs);
            if (index >= 0) { $addedProductIDs.splice(index, 1); }
            Count = $tableItemCounter;
        }
    });
}
$('#Qty').on('input', function (e) {
    var qty = $('#Qty').val();
    var unitPerCarton = $('#unitPerCarton').val();
    var units = parseFloat(qty * unitPerCarton).toFixed(0);
    $('#Units').val(units);
    calcSubTotal();
    calcTotalWeight();
});
$('#UnitPrice').on('input', function (e) {
    calcSubTotal();
});
getPOID();
// Get Total Weight
function calcTotalWeight() {
    
    var unitPerCarton = parseInt($('#unitPerCarton').val());    
    var ctnQty =parseFloat($('#Qty').val() || 0);     
    var totalQty = parseFloat((ctnQty * unitPerCarton));        
    var isMinor = $('#isMinor').val();
    var isPack = $('#IsPacketID').val();
    var levelID = $('#LevelID').val();
    var qty = totalQty;
       
    $('#totalWeight').val(parseFloat(qty).toFixed(2));
}
function getTotalWeight(unitPerCarton,Qty) {
    unitPerCarton = parseInt(unitPerCarton);
    var totalQty = parseFloat((Qty * unitPerCarton));
    return totalQty;
}
function getPOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getNewPOID',
        async: true,
        success: function (data) {
            {
                $("#POID").val(data);
                POID = data;
            }
        },
        error: function (err) { console.log(err); }
    });
}
$("#ddlVehCode").change(function () {
    var vehCodeID = $('#ddlVehCode').val();    
    //getProducts(vehCodeID);
});
function getDetail(pId) {
    $("#hdnProductID").val(pId);
    // get product description
    var json = { "productId": pId };
    //ajaxCall("GET", json, "application/json; charset=utf-8", "/Purchase/getProductDetail", "json", onSuccess, onFailure);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            //console.log($('#hdnProductID').val());
            //if (data) { document.getElementById('btnEdit').style.visibility = 'visible'; }
            if (data[0].qry.Description) { $('#DescriptionTitle').text("Desc: "); $('#Description').text(data[0].qry.Description); }
            //if (data.SubstituteNo) { $('#SubsTitle').text("Su No.: "); $('#SubstituteNo').text(data.SubstituteNo); }
            if (data[0].qry.GroupName) { $('#GroupTitle').text("Group: "); $('#Group').text(data[0].qry.GroupName); }
            if (data[0].qry.VehicleName) { $('#VehModelTitle').text("Model: "); $('#VehModel').text(data[0].qry.VehicleName); }
            //if (data.Location) { $('#LocationTitle').text("Location: "); $('#Location').text(data.Location); }
            if (data[0].qry.UnitCode) {
                $('#UnitCodeTitle').text("Un Code: "); $('#UnitCode').text(data[0].qry.UnitCode);
                $('#unitCode').val(data[0].qry.UnitCode);
            }
            if (data[0].qry.VehicleCode) {
                var vc = data[0].qry.VehicleCode;
                $("#ddlVehCode option:contains(" + data[0].qry.VehicleCode + ")").attr("selected", true);
                //$('#ddlVehCode').val(vc).trigger('change.select2');
            }
            if (data[0].qry.UnitPerCtn) {
                $('#unitPerCarton').val(data[0].qry.UnitPerCtn);
                $('#UnitPerCtnTitle').text("Unit/Ctn: "); $('#UnitPerCtn').text(data[0].qry.UnitPerCtn);
            }
            if (data[0].qry.QtyPerUnit) {
                $('#QtyPerUnitTitle').text("Qty/Unit: "); $('#QtyPerUnit').text(data[0].qry.QtyPerUnit);
                $('#qtyPerUnit').val(data[0].qry.QtyPerUnit);
            }
            if (data[0].qry.LevelID) {
                $('#LevelID').val(data[0].qry.LevelID);
            }
            if (data[0].qry.IsMinor) {
                $('#isMinor').val(data[0].qry.IsMinor);
            }
            else {
                $('#isMinor').val('false');
            }
            var isPacket = data[0].qry.IsPacket;
            if (data[0].qry.IsPacket) {
                $('#isPacket').val(data[0].qry.IsPacket);
            }
            else {
                $('#isPacket').val('false');
            }
            if (data[0].qry.MinorDivisor) {
                $('#minorDivisor').val(data[0].qry.MinorDivisor);
            }
            if (isPacket == true) {
                document.getElementById("Packet").readOnly = false;
            }
            else {
                document.getElementById("Packet").readOnly = true;
            }
        },
        error: function (err) { console.log(err); }
    });
    var json = { "productId": pId };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getLastUnitPrice',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#UnitPrice').attr('placeholder', data);
        },
        error: function (err) { console.log(err); }
    });
}
// If currency is AED then perform actions
$("#Currency").on("change", function () {
    var currencyID = $("#Currency").val();
    if (currencyID == 1) {
        onCurrChange();
    }
    if (currencyID == 2) {
        //console.log("Currency event called " + currencyID);
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/Purchase/getCurrency',
            async: true,
            success: function (data) {
                //console.log("ajax"+data);
                $("#exRate").val(data);
                calcTotal();
                onCurrChange();
            },
            error: function (err) { console.log(err); }
        });
    }
});
// if currency changed
function onCurrChange() {
    //console.log("OnCurrCalled");
    $('#tblProduct  tbody tr').each(function (i, row) {
        // reference all the stuff you need first
        var $row = $(row);
        //var qty = $row.find('input[id*="ProductQty"]').val();                 
        //console.log($row + "  qty = " + qty);
        var qty = $row.find("#ProductQty").text();
        var price = $row.find("#ProductCostPrice").text();
        var currency = $('#Currency').val();
        // console.log("Currency " + currency);
        if (currency == 2) {
            var convRate = $('#exRate').val();
            // console.log("convRate=" + convRate);
            price = price * convRate;
            $row.find("#exchangePrice").text(parseFloat(price).toFixed(2));
        }
        else {
            $row.find("#exchangePrice").text("");
        }
        if (($.isNumeric(price)) && ($.isNumeric(qty))) {
            var amount = (qty * price);
            $row.find("#ProductSubTotal").text(parseFloat(amount).toFixed(2)
            );


            Total();
        }
        else {
            $row.find("#ProductSubTotal").text('');
        }
    });
}
function calcSubTotal() {
    var qty = $('#Qty').val();
    var rate = $('#UnitPrice').val();
    var currency = $('#Currency').val();
    if (currency == 2) {
        var convRate = $('#exRate').val();
        rate = rate * convRate;
        $('#exUnitPrice').val(rate);
        //console.log("rate = " + rate);
        //console.log("convRate = " + convRate);
    }
    if (($.isNumeric(rate)) && ($.isNumeric(qty))) {
        var amount = (qty * rate);
        //  console.log(amount + " amount");
        $('#SubTotal').val(parseFloat(amount).toFixed(2));
    }
    else {
        $('#SubTotal').val("");
    }
}

// On part No Selection
$("#ddlPartNumber").change(function () {
    var pId = $('#ddlPartNumber').val();
    getDetail(pId);
});
// Add Product to Purchase List 
$("#addRow").click(function () {
    var vehCode = $("#ddlVehCode :selected").text();  // 1    
    var ProductID = $("#ddlPartNumber").val(); // hidden
    var PartNO = $("#ddlPartNumber :selected").text();
    var UnitCode = $("#UnitCode").text(); // 3   
    var Qty = $("#Qty").val();
    var packet = $("#Packet").val();
    var UnitPrice = $("#UnitPrice").val();
    var totalWeight = $("#totalWeight").val();
    var exUnitPrice = $("#exUnitPrice").val(); // 7 
    if (!(exUnitPrice > 0)) { exUnitPrice = 0; }
    if (typeof exUnitPrice == 'undefined') { exUnitPrice = UnitPrice; }
    var SalePrice = $("#SalePrice").val(); // 7
    var SubTotal = $("#SubTotal").val(); // 8
    var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';
    var isPack = '<input type="hidden" id="isPacket" value="' + $("#isPacket").val() + '"/>';   

    if (ProductID > 0 && (parseFloat(Qty)) > 0 && Number(parseFloat(UnitPrice)) && SubTotal > 0) {
        var index = $.inArray(ProductID, $addedProductIDs);
        if (index >= 0) {
            swal("Error", "Product Already Added!", "error");
        } else {

            var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + isPack + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td contenteditable='true' id=Location hidden>" + Location + "</td><td id='unitCode' hidden>" + UnitCode + "</td><td contenteditable='true' id='ProductQty'>" + Qty + "</td><td id='Packet' hidden>" + packet + "</td><td  id='ProductCostPrice'>" + parseFloat(UnitPrice).toFixed(2) + "</td><td id='exchangePrice' hidden>" + parseFloat(exUnitPrice).toFixed(2) + "</td><td contenteditable='true' id='ProductSalePrice'>" + SalePrice + "</td><td id='ProductSubTotal'>" + parseFloat(SubTotal).toFixed(2) + "</td></tr>";
            $("#tblProduct tbody").append(markup);
            $tableItemCounter++;
            $addedProductIDs.push(ProductID);
           
            clearFields();
            clearLabels();
            calcTotal();
            $('#BarCode').focus();
$("#btnRemove").prop('disabled', false);
        }
    }
    else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
    else if (UnitPrice == "" || UnitPrice == "undefined" || !Number(parseFloat(UnitPrice))) { swal("Error", "Please enter Unit Price!", "error"); }
    else { swal("Error", "Check Entries!", "error"); }
    getNewTotal();
});
function clearLabels() {
    $('#DescriptionTitle').text(''); $('#Description').text('');
    $('#SubsTitle').text(''); $('#SubstituteNo').text('');
    $('#GroupTitle').text(''); $('#Group').text('');
    $('#VehModelTitle').text(''); $('#VehModel').text('');
    $('#LocationTitle').text(''); $('#Location').text('');
    $('#UnitCodeTitle').text(''); $('#UnitCode').text('');
    $('#QtyPerUnitTitle').text(''); $('#QtyPerUnit').text('');
    $('#UnitPerCartonTitle').text(''); $('#UnitPerCarton').text('');
}
// Clear product fields when added 
function clearFields() {
    $("#isMinor").val("");
    $("#Packet").val("");
    $("#LevelID").val("");
    $("#isPack").val("");
    $("#Description").text("");
    $("#unitCode").val("");
    $("#Location").text("");
    $("#Qty").val("");
    $("#UnitPrice").val("");
    $("#SalePrice").val("");
    $("#exUnitPrice").val("");
    $("#BarCode").val("");
    $("#SubTotal").val("");
    $("#ddlVehCode").focus();
    $('#ddlVehCode').val(null).trigger('change');
    $('#ddlPartNumber').val(null).trigger('change.select2');
    $("#btnRemove").prop('disabled', false);
}
$("#AccountID").change(function () {
    var vendorID = $('#AccountID').val();
    $('#hdnAccountID').val(vendorID);      
    getVendorDetail(vendorID);
});

// Get Vendor Balance and Credit Limit 
function getVendorDetail(accountID) {
    if (accountID == "")
    { accountID = -1; }
    var json = {
        "accountID": accountID
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Vendor/getDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {

            $('#OpeningBal').val(data.Balance);
        },
        error: function (err) {
            console.log(err);
        }
    });
}
// Only allow int 
function isNumberKey(evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
var validate = function (e) {
    var t = e.value;
    e.value = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 3)) : t;
}
function calcTotal() {

    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        //  console.log("in tbl product body");
        var $row = $(n);
        var subTotal = parseFloat($row.find('#ProductSubTotal').text());
        total += parseFloat(subTotal);
    });
    $('#subAmount').val(parseFloat(total).toFixed(2));
    $('#TotalAmount').val(parseFloat(total).toFixed(2));
    $('#amountPaid').val('');
}



