﻿var controlId = 0;
var vehControlId = 0;
var perPieceExp = 0;
var tmpOrderID = 0;
var tOrderID = 0;
var price = 0;
$(document).ready(function () {
    // Self Generated PurchaseCode  
        getPurchaseCode();        
        function getPurchaseCode() {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: '/Purchase/getLastPOCode',
                async: true,
                success: function (data) {                    
                    {
                        var value = data;
                        var newValue = value.split('-');
                        var increase = newValue[1];
                        increase++;
                        value = "PO-" + increase;                        
                        $("#PurchaseCode").val(value);
                    }
                },
                error: function (err) { console.log(err); }
            });
        }
});

    // Get Unsaved order and its details

    function checkNull(value) {
        return JSON.stringify(value).replace(/null/g, "\"\"");
    }
    //Populate Dropdowns
    function GetDropdown(ddl, method, _data, pID) {

        $("#" + ddl).empty();
        $("#" + ddl).select2();
        ajaxCall("POST", _data, "application/json; charset=utf-8", method, "json", onSuccess);


        function onSuccess(Result) {

            var data = Result;// JSON.parse(Result.d);
            $.each(data, function (key, value) {
                $("#" + ddl).append($("<option></option>").val(value.Value).html(value.Name));
            });
            $("#" + ddl).val(pID);
        }
        // });

    }
    
    $(function () {       
            
        $('#btnCreate').click(function () {                       
            var isValid = true;
            var accID = $('#AccountID option:selected').val();
            var Date = $('#Date').val();
            if (accID == "" || accID == 0) {
                isValid = false;
                swal("Vendor", "Please Select Vendor! ", "error");
                //alert("Please Select Vendor");
            }            
            else if (isValid == true) {
                $('#btnCreate').prop('disabled', true);
                uiBlock();
                insert();
            }
        });       
    });
    //Add New Row For Product
    
    //function getProducts(pro) {
    //    var tr = $(pro).closest('tr');

    //    var vehCodeID = $(tr).find('td:eq(0) :selected').val();
    //    var controlID = $(pro).closest('tr').index();
    //    var json = { "vehCodeId": vehCodeID };
    //    $.ajax({
    //        type: "POST",
    //        contentType: "application/json; charset=utf-8",
    //        url: '/Product/getProducts',
    //        async: true,
    //        data: JSON.stringify(json),
    //        success: function (data) {
    //            GetDropdown1("ddlPartNumber" + controlID, data, true);
    //        },
    //        error: function (err) { console.log(err); }
    //    });
    //}
    
    // Get Data from Table  and Send it to Controller
    function insert() {
        var currencyID = $("#Currency").val();
        var exchangeRate = 0;
        var rows = [];
        var total = 0;
        var isValid = false;
        totalInvoice = document.getElementById("TotalAmount").value;        
        $('#tblProduct tbody tr').each(function (i, n) {
            var $row = $(n);
            var pId = $row.find('input[id*="productID"]').val();
            // console.log(pId +" pId");
            var partNumber = $row.find("td").eq(2).text();
           // var pLocation = $row.find("#Location").text();
            var qtyCTN = parseFloat($row.find("#ProductQty").text());
            var price = 0;
            if (currencyID == 1) {
                price = $row.find("#ProductCostPrice").text();
            }
            else if (currencyID == 2) {
                price = $row.find("#exchangePrice").text();
            }
            //var newRate = $row.find('td:eq(9) input[type="Number"]').val();  // add new field to Database if required
            var salePrice = $row.find("#ProductSalePrice").text();
            var subTotal = $row.find("#ProductSubTotal").text();
            exchangeRate = $row.find("#exchangePrice").text();
            var unitPerCTN = $row.find('input[id*="unitPerCTN"]').val();
            var unitCode = $row.find("#unitCode").text();
            var isPack = $row.find('input[id*="isPacket"]').val();
            var isMinor = $row.find('input[id*="isMinor"]').val();
            var levelID = $row.find('input[id*="LevelID"]').val();
            var minorDivisor = 1000;
            
            rows.push({
                PartNo: partNumber,
                ProductId: pId,
                Qty: qtyCTN,
                UnitPerCarton: unitPerCTN,
                UnitPrice: price,
                UnitCode: unitCode,
                IsPack: isPack,
                IsMinor: isMinor,
                LevelID: levelID,
                MinorDivisor: minorDivisor,
                IsReturned: false,
                ReturnedQty: 0,
                SalePrice: salePrice,
                ExchangeRate: exchangeRate,
                Total: subTotal
                
            });
        });
        if (rows.length) {            
            var data = {
                'IsPO'     : true,
                'AccountID': $('#AccountID option:selected').val(),                
                'PurchaseDate': $('#PurchaseDate').val(),
                'PurchaseCode': $('#PurchaseCode').val(),
                'IsPurchased' : false,
                'Currency': $('#Currency option:selected').text(),
                'ExchangeRate': $('#exRate').val(),
                'TotalAmount': document.getElementById("TotalAmount").value,
                'BranchID'   : $('#BranchID option:selected').val(),
                'tmp_OrderDetails': rows
            };
            var json = JSON.stringify({ 'model': data });
            //console.log(json);
            ajaxCall("POST", json, "application/json; charset=utf-8", "/Purchase/SavePurchaseOrder", "json", onSuccess, onFailure);
            function onSuccess(Result) {                
                if (Result == "success") {
                    uiUnBlock();
                    window.location.href = '/Purchase/PurchaseOrderIndex?isPO='+true;
                    //console.log(Result);
                }
                else {
                    uiUnBlock();
                    $('#btnCreate').prop('disabled', false);
                    swal("Error!", "Please Check Your Entries! ", "error");
                    //alert("Some error Ocurred! Please Check Your Entries");
                }                
            }
            function onFailure(error) {
                if (error.statusText == "OK") {
                    window.location.reload();
                    console.log(error.status);                    
                }
            }
        }
        else
        {
            uiUnBlock();
            $('#btnCreate').prop('disabled', false);            
        }
    }
