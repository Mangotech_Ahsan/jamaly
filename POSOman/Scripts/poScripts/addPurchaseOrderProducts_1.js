﻿$tableItemCounter = 0;
var $addedProductIDs = [];
$('#Qty').on('input', function (e) {
    var qty = $('#Qty').val();
    var unitPerCarton = $('#unitPerCarton').val();
    var units = parseFloat(qty * unitPerCarton).toFixed(0);
    $('#Units').val(units);
    calcSubTotal();
    calcTotalWeight();
});
$('#UnitPrice').on('input', function (e) {
    calcSubTotal();
});


$('#PrDisAmount').on('input', function (e) {

    var subTotal = $("#SubTotal").val() || 0;
    var PrDisAmount = $("#PrDisAmount").val() || 0;
    var rate = $('#UnitPrice').val();
    if (subTotal > 0 ) {
        //for converting to %
        var NewAm = (PrDisAmount * 100) / rate;       
        $("#PrDisPercent").val(parseFloat(NewAm).toFixed(2));
        calcSubTotal();
    }
    else {
        $("#PrDisAmount").val(0);
        $("#PrDisPercent").val(0);
        calcSubTotal();
    }   
});
$('#PrDisPercent').on('input', function (e) {

    var subTotal = $("#SubTotal").val() || 0;
    var PrDisPercent = $("#PrDisPercent").val() || 0;

    var rate = $('#UnitPrice').val();
    //var qty = $('#Qty').val();


    if (subTotal > 0) {

        //for converting to %


        var NewAm = (PrDisPercent / 100) * rate;
        $("#PrDisAmount").val(parseFloat(NewAm).toFixed(2));


        /////////////////////


        calcSubTotal();

    }
    else {
        $("#PrDisAmount").val(0);
        $("#PrDisPercent").val(0);
        calcSubTotal();
    }

});
$('#PrGSTAmount').on('input', function (e) {
    var subTotal = $("#SubTotal").val() || 0;
    var PrGSTAmount = $("#PrGSTAmount").val() || 0;
    var rate = $('#UnitPrice').val();
  //  console.log("PrGSTAmount=" + PrGSTAmount);
    if (subTotal > 0) {
        if (parseFloat(PrGSTAmount) > 0){
            var NewAm = (parseFloat(PrGSTAmount) * 100) / rate;
            $("#PrGSTPercent").val(parseFloat(NewAm).toFixed(2));
           // $("#PrGSTAmount").val(parseFloat(PrGSTAmount).toFixed(2));
            calcSubTotal();
        }
        else {
            
            $("#PrGSTAmount").val(0);
            $("#PrGSTPercent").val(0);
            calcSubTotal();
        }    
    }
    else {
        $("#PrGSTAmount").val(0);
        $("#PrGSTPercent").val(0);
        calcSubTotal();
    }
   
});

$('#PrGSTPercent').on('input', function (e) {


    var subTotal = $("#SubTotal").val() || 0;
    var PrGSTAmount = $("#PrGSTPercent").val() || 0;
    var rate = $('#UnitPrice').val();



    if (subTotal > 0) {

        if ($.isNumeric(PrGSTAmount)) {

            var NewAm = parseFloat(PrGSTAmount / 100) * rate;
            $("#PrGSTAmount").val(parseFloat(NewAm).toFixed(2));
            calcSubTotal();

        }
        else {
            $("#PrGSTAmount").val(0);
            $("#PrGSTPercent").val(0);
            calcSubTotal();
        }


       

    }
    else {
        $("#PrGSTAmount").val(0);
        $("#PrGSTPercent").val(0);
        calcSubTotal();
    }

});


getPOID();
// Get Total Weight
function calcTotalWeight() {
    
    var unitPerCarton = parseInt($('#unitPerCarton').val());    
    var ctnQty =parseFloat($('#Qty').val() || 0);     
    var totalQty = parseFloat((ctnQty * unitPerCarton));        
    var isMinor = $('#isMinor').val();
    var isPack = $('#IsPacketID').val();
    var levelID = $('#LevelID').val();
    var qty = totalQty;
       
    $('#totalWeight').val(parseFloat(qty).toFixed(2));
}
function getPOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getNewPOID',
        async: true,
        success: function (data) {
            {
                $("#POID").val(data);
                POID = data;
            }
        },
        error: function (err) { console.log(err); }
    });
}
$("#ddlVehCode").change(function () {
    var vehCodeID = $('#ddlVehCode').val();
    
    getProducts(vehCodeID);
});
function getProducts(vehCodeID) {    
    if (vehCodeID == "")
    { vehCodeID = -1; }
    var json = { "vehCodeId": vehCodeID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getProducts',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("ddlPartNumber", data, true);
        },
        error: function (err) { console.log(err); }
    });
}
$('#BarCode').on('change', function (e) {
    var BarCode = $("#BarCode").val();
   //console.log(BarCode);
    getDetailByBarCode(BarCode);
});
// Get Details By BAr code 
function getDetailByBarCode(barCode) {
    var pId = 0;
    // get product description
    var json = { "barCode": barCode };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetailByBarCode',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            
            
            if (data[0].qry.Description) { $('#DescriptionTitle').text("Desc: "); $('#Description').text(data[0].qry.Description); }
            //if (data.SubstituteNo) { $('#SubsTitle').text("Su No.: "); $('#SubstituteNo').text(data.SubstituteNo); }
            if (data[0].qry.GroupName) { $('#GroupTitle').text("Group: "); $('#Group').text(data[0].qry.GroupName); }
            if (data[0].qry.VehicleName) { $('#VehModelTitle').text("Model: "); $('#VehModel').text(data[0].qry.VehicleName); }
            //if (data.Location) { $('#LocationTitle').text("Location: "); $('#Location').text(data.Location); }
            if (data[0].qry.UnitCode) {
                $('#UnitCodeTitle').text("Un Code: "); $('#UnitCode').text(data[0].qry.UnitCode);
                $('#unitCode').val(data[0].qry.UnitCode);
            }
            if (data[0].qry.VehicleCode) {
                var vc = data[0].qry.VehicleCode;
                $("#ddlVehCode option:contains(" + data[0].qry.VehicleCode + ")").attr("selected", true);
                //$('#ddlVehCode').val(vc).trigger('change.select2');
            }            
            
            pId = data[0].qry.ProductID;
            //console.log("PID="+pId);
            $("#hdnProductID").val(pId);
            $('#ddlPartNumber').val(parseInt(pId)).trigger('change.select2');
        },
        error: function (err) { console.log(err); }
    });
   
}
// Get Details of Selected Product
function getDetail(pId) {
    $("#hdnProductID").val(pId);
    // get product description
    var json = { "productId": pId };
    //ajaxCall("GET", json, "application/json; charset=utf-8", "/Purchase/getProductDetail", "json", onSuccess, onFailure);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            //console.log($('#hdnProductID').val());
            //if (data) { document.getElementById('btnEdit').style.visibility = 'visible'; }
            if (data[0].qry.Description) { $('#DescriptionTitle').text("Desc: "); $('#Description').text(data[0].qry.Description); }
            //if (data.SubstituteNo) { $('#SubsTitle').text("Su No.: "); $('#SubstituteNo').text(data.SubstituteNo); }
            if (data[0].qry.GroupName) { $('#GroupTitle').text("Group: "); $('#Group').text(data[0].qry.GroupName); }
            if (data[0].qry.VehicleName) { $('#VehModelTitle').text("Model: "); $('#VehModel').text(data[0].qry.VehicleName); }
            //if (data.Location) { $('#LocationTitle').text("Location: "); $('#Location').text(data.Location); }
            if (data[0].qry.UnitCode) {
                $('#UnitCodeTitle').text("Un Code: "); $('#UnitCode').text(data[0].qry.UnitCode);
                $('#unitCode').val(data[0].qry.UnitCode);
            }
            if (data[0].qry.VehicleCode) {
                var vc = data[0].qry.VehicleCode;
                $("#ddlVehCode option:contains(" + data[0].qry.VehicleCode + ")").attr("selected", true);
                //$('#ddlVehCode').val(vc).trigger('change.select2');
            }
            if (data[0].qry.UnitPerCtn) {
                $('#unitPerCarton').val(data[0].qry.UnitPerCtn);
                $('#UnitPerCtnTitle').text("Unit/Ctn: "); $('#UnitPerCtn').text(data[0].qry.UnitPerCtn);
            }
            if (data[0].qry.QtyPerUnit) {
                $('#QtyPerUnitTitle').text("Qty/Unit: "); $('#QtyPerUnit').text(data[0].qry.QtyPerUnit);
                $('#qtyPerUnit').val(data[0].qry.QtyPerUnit);
            }
            if (data[0].qry.LevelID) {
                $('#LevelID').val(data[0].qry.LevelID);
            }
            if (data[0].qry.IsMinor) {
                $('#isMinor').val(data[0].qry.IsMinor);
            }
            else {
                $('#isMinor').val('false');
            }
            var isPacket = data[0].qry.IsPacket;
            if (data[0].qry.IsPacket) {
                $('#isPacket').val(data[0].qry.IsPacket);
            }
            else {
                $('#isPacket').val('false');
            }
            if (data[0].qry.MinorDivisor) {
                $('#minorDivisor').val(data[0].qry.MinorDivisor);
            }
            if (isPacket == true) {
                document.getElementById("Packet").readOnly = false;
            }
            else{
                document.getElementById("Packet").readOnly = true;
            }
        },
        error: function (err) { console.log(err); }
    });
    var json = { "productId": pId };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getLastUnitPrice',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#UnitPrice').attr('placeholder', data);
        },
        error: function (err) { console.log(err); }
    });
}
// If currency is AED then perform actions
$("#Currency").on("change", function () {
    var currencyID = $("#Currency").val();
    if (currencyID == 1) {
        onCurrChange();
    }
    if (currencyID == 2) {
        //console.log("Currency event called " + currencyID);
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/Purchase/getCurrency',
            async: true,
            success: function (data) {
                //console.log("ajax"+data);
                $("#exRate").val(data);
                calcTotal();
                onCurrChange();
            },
            error: function (err) { console.log(err); }
        });
    }
});
$('#getPurchaseOrder').click(function () {
    var poID = $('#purchaseOrderID').val();
    
    var orderID = 0;
    if (poID != "") {
        var json = { "POID": poID };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Purchase/getPOOrderID',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                if (data > 0) {
                    orderID = data;
                    //console.log(orderID);
                    window.location.href = '/Purchase/GoToNewOrder?tOrderID=' + orderID;
                }
                else {
                    swal("Error", "An Error Occured please check PO No.!", "error");
                }
            },
            error: function (err) { console.log(err); }
        });
    }
    else {
        swal("PO No", "Enter PoNo!", "error");
    }


});
// if currency changed
function onCurrChange() {
    //console.log("OnCurrCalled");
    $('#tblProduct  tbody tr').each(function (i, row) {
        // reference all the stuff you need first
        var $row = $(row);
        //var qty = $row.find('input[id*="ProductQty"]').val();                 
        //console.log($row + "  qty = " + qty);
        var qty = $row.find("#ProductQty").text();
        var price = $row.find("#ProductCostPrice").text();
        var currency = $('#Currency').val();
       // console.log("Currency " + currency);
        if (currency == 2) {
            var convRate = $('#exRate').val();
           // console.log("convRate=" + convRate);
            price = price * convRate;
            $row.find("#exchangePrice").text(parseFloat(price).toFixed(2));
        }
        else {
            $row.find("#exchangePrice").text("");
        }
        if (($.isNumeric(price)) && ($.isNumeric(qty))) {
            var amount = (qty * price);
            $row.find("#ProductSubTotal").text(parseFloat(amount).toFixed(2)
            );
            calcTotal();
        }
        else {
            $row.find("#ProductSubTotal").text('');
        }
    });
}
function calcSubTotal() {
    var qty = $('#Qty').val()||0;
    var PrDisAmount = parseFloat($('#PrDisAmount').val()) ||0 ;
    var PrGSTAmount1 = parseFloat($('#PrGSTAmount').val()) ||0 ;
    //var CalcGST = 0;
    var CalDis = 0;

    var rate = parseFloat($('#UnitPrice').val());
    var currency = $('#Currency').val();
    if (currency == 2) {
        var convRate = $('#exRate').val();
        rate = rate * convRate;
        $('#exUnitPrice').val(rate);        
        //console.log("rate = " + rate);
        //console.log("convRate = " + convRate);
    }
    if (($.isNumeric(rate)) && ($.isNumeric(qty))) {

        var CalcGST = parseFloat(rate + PrGSTAmount1).toFixed(2);
        //console.log(CalcGST + " CalcGST");

        CalDis = CalcGST - PrDisAmount;
       //console.log(CalDis + " CalDis");
        var amount = qty * CalDis;
       //console.log(amount + " amount");
        $('#SubTotal').val(parseFloat(amount).toFixed(2));        
    }
    else {
        $('#SubTotal').val("");
    }           
}
// When changed made in product table
$("#tblProduct").focusout(function () {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);                              
        var qty = $row.find("#ProductQty").text();
        var rate = $row.find("#ProductCostPrice").text();
        ///////////////Changes by ahsan //////////////////////////
        var DiscAm = $row.find("#ProductDiscount").text()||0;
        var DiscAmPer = $row.find("#ProductDiscountPercent").text();
        var GST = $row.find("#ProductGST").text()||0;
        var GSTPer = $row.find("#ProductGSTPercent").text();
        /////////////////////////////////////////////////////////
        var currency = $('#Currency').val();
        if (currency == 2) {
            var convRate = $('#exRate').val();
            rate = rate * convRate;
            $row.find("#exchangePrice").text(parseFloat(rate).toFixed(2));
        }
        else {
            $row.find("#exchangePrice").text("");
        }
        if (($.isNumeric(rate)) && ($.isNumeric(qty))) {
           // var amount = 0;
            //if ((($.isNumeric(DiscAm)) && ($.isNumeric(GST)))) {
           //console.log(DiscAm + "Discount");
           // console.log(GST + "GST");
           // console.log(rate + "rate");

            var amountWithGST = (parseFloat(rate) + parseFloat(GST));
            var amountWithGSTAndDisc = amountWithGST - parseFloat(DiscAm);
            var amount = parseInt(qty) * amountWithGSTAndDisc;

            //console.log(amountWithGST + "amountWithGST");
            //console.log(amountWithGSTAndDisc + "amountWithGSTAndDisc");
            //console.log(amount + "amount");
         //   }

           
            $row.find("#ProductSubTotal").text(parseFloat(amount).toFixed(2));
            calcTotal();
        }
    });
});
// On part No Selection
$("#ddlPartNumber").change(function () {
    var pId = $('#ddlPartNumber').val();
    getDetail(pId);
});
// Add Product to Purchase List 
$("#addRow").click(function () {    
    var vehCode = $("#ddlVehCode :selected").text();  // 1    
    var ProductID = $("#ddlPartNumber").val(); // hidden
    var PartNO = $("#ddlPartNumber :selected").text();  
    var UnitCode = $("#unitCode").val(); // 3
    var Qty = $("#Qty").val();
    var packet = $("#Packet").val();
    var UnitPrice = $("#UnitPrice").val();
    ////////////////////////////
    var PrDisPercent = $("#PrDisPercent").val();
    var PrDisAmount = $("#PrDisAmount").val();
    var PrGSTPercent = $("#PrGSTPercent").val();
    var PrGSTAmount = $("#PrGSTAmount").val();
    ////////////////////////////
    var totalWeight = $("#totalWeight").val();
    var exUnitPrice = $("#exUnitPrice").val(); // 7 
    if (!(exUnitPrice > 0)) { exUnitPrice = 0; }
    if (typeof exUnitPrice == 'undefined') {  exUnitPrice = UnitPrice; }
    var SalePrice = $("#SalePrice").val(); // 7
    var SubTotal = $("#SubTotal").val(); // 8
    var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';  
    var expDate = $("#expDate").val();
    var totalGST = parseFloat(PrGSTAmount * Qty).toFixed(2);
    var totalDisc = parseFloat(PrDisAmount * Qty).toFixed(2);
    //console.log(expDate);
    var isPack = '<input type="hidden" id="isPacket" value="' + $("#isPacket").val() + '"/>';
    if (ProductID > 0 && (parseFloat(Qty)) > 0 && Number(parseFloat(UnitPrice)) && SubTotal > 0) {
        var index = $.inArray(ProductID, $addedProductIDs);
        if (index >= 0) {
            swal("Error", "Product Already Added!", "error");
        } else {            
            var markup = "<tr><td><input type='checkbox' name='record'></td><td hidden>" + isPack + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td contenteditable='true' id=Location hidden>" + Location + "</td><td id='unitCode' >" + UnitCode + "</td><td id='expDate' hidden>" + expDate + "</td><td contenteditable='true' id='ProductQty'>" + Qty + "</td><td id='Packet' hidden>" + packet + "</td><td contenteditable='true' id='ProductCostPrice'>" + parseFloat(UnitPrice).toFixed(2) + "</td><td id='exchangePrice' hidden>" + parseFloat(exUnitPrice).toFixed(2) + "</td><td contenteditable='true' id='ProductSalePrice' hidden>" + SalePrice + "</td><td contenteditable='false' id='ProductDiscountPercent' >" + PrDisPercent + "</td><td contenteditable='false' id='ProductDiscount' >" + PrDisAmount + "</td><td >" + totalDisc + "</td><td contenteditable='false' id='ProductGSTPercent' >" + PrGSTPercent + "</td><td contenteditable='false' id='ProductGST' >" + PrGSTAmount + "</td><td >" + totalGST  + "</td><td id='ProductSubTotal'>" + parseFloat(SubTotal).toFixed(2) + "</td></tr>";
            $("#tblProduct tbody").append(markup);
            $tableItemCounter++;
            $addedProductIDs.push(ProductID);
            if (tmpOrderID > 0) {
                insertTempData();
            }
            clearFields();
            clearLabels();
            calcTotal();
            $('#BarCode').focus();
        }
    }
    else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
    else if (UnitPrice == "" || UnitPrice == "undefined" || !Number(parseFloat(UnitPrice))) { swal("Error", "Please enter Unit Price!", "error"); }
    else { swal("Error", "Check Entries!", "error"); }
});
function clearLabels() {
    $('#DescriptionTitle').text(''); $('#Description').text('');
    $('#SubsTitle').text(''); $('#SubstituteNo').text('');
    $('#GroupTitle').text(''); $('#Group').text('');
    $('#VehModelTitle').text(''); $('#VehModel').text('');
    $('#LocationTitle').text(''); $('#Location').text('');
    $('#UnitCodeTitle').text(''); $('#UnitCode').text('');
    $('#QtyPerUnitTitle').text(''); $('#QtyPerUnit').text('');
    $('#UnitPerCartonTitle').text(''); $('#UnitPerCarton').text('');
}
// Clear product fields when added 
function clearFields() {
    $("#isMinor").val("");
    $("#Packet").val("");

    $("#PrDisAmount").val(""); $("#PrDisPercent").val("");
    $("#PrGSTAmount").val(""); $("#PrGSTPercent").val("");

    $("#LevelID").val("");
    $("#isPack").val("");
    $("#Description").text(""); 
    $("#unitCode").val(""); 
    $("#Location").text(""); 
    $("#Qty").val(""); 
    $("#UnitPrice").val("");
    $("#SalePrice").val("");
    $("#exUnitPrice").val("");
    $("#BarCode").val("");
    $("#SubTotal").val("");
    $("#ddlVehCode").focus();
    //$('#ddlVehCode').val(null).trigger('change');
    //$('#ddlPartNumber').val(null).trigger('change.select2');
}
// Remove Selected Products 
function remove(input) {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {
            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            calcTotal();
            var productID = row.find('input[id*="productID"]').val(); // find hidden id 
            var index = $.inArray(productID, $addedProductIDs);
            if (index >= 0) { $addedProductIDs.splice(index, 1); }
        }
    });
}
// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
var validate = function (e) {
    //var t = e.value;
    //e.value = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 3)) : t;
}
function calcTotal() {
    var total = 0;
    var GST = 0;
    var Disc = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('#ProductSubTotal').text());
        var SubDiscAm = $row.find("#ProductDiscount").text() || 0;
        var SubGST = $row.find("#ProductGST").text() || 0;

        var qty = $row.find("#ProductQty").text()||0;

        GST += (SubGST*qty);
        Disc += (SubDiscAm * qty);
        total += subTotal;
    });

    var SubTot = parseFloat(total + Disc - GST).toFixed(2);
    console.log(SubTot);
    $('#subAmount').val(parseFloat(SubTot).toFixed(2));
    $('#TotalAmount').val(parseFloat(total).toFixed(2));

    $('#discountAmount').val(parseFloat(Disc).toFixed(2));
    $('#vatAmount').val(parseFloat(GST).toFixed(2));
}
