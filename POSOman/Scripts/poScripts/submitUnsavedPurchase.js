﻿var controlId = 0;
var vehControlId = 0;
var totalQty = 0;
var perPieceExp = 0;
var perProductExp = 0;
var tmpOrderID = 0;
var POID = 0;
var expensePercentage = 0;
var tOrderID = 0;
var price = 0;
$(function () {
    getTempOrderDetails();
})
// Bind Unsaved Order Data , Payment and Vendor Details if Exist
function bindTempData(data) {
    $("#Bank").prop("disabled", true);

    $('#InvoiceNo').val(data.qry[0].InvoiceNo);
    //$('#Currency').text(data.qry[0].Currency);        
    var vendorAccountID = data.qry[0].AccountID;
    $('#AccountID').val(vendorAccountID).trigger('change.select2');
    var BranchID = data.qry[0].BranchID;
    $('#BranchID').val(BranchID).trigger('change.select2');
    document.getElementById('vatAmount').value = data.qry[0].VAT;//previously vat input in cshtml
    document.getElementById('discountAmount').value = data.qry[0].DiscountAmount;
    document.getElementById('PaymentType').value = data.qry[0].PaymentTypeID;
    document.getElementById('expenseInput').value = data.qry[0].Expenses;
    document.getElementById('amountPaid').value = data.qry[0].AmountPaid;
    document.getElementById('TotalAmount').value = data.qry[0].TotalAmount
    if (data.qry[0].PaymentStatus != "") {
        $("#PaymentStatus option:contains(" + data.qry[0].PaymentStatus + ")").attr("selected", true);
    }
    $("#Currency option:contains(" + data.qry[0].Currency + ")").attr("selected", true);

}

// Get Unsaved order and its details
function getTempOrderDetails() {
    var oldtmpOrderID = $('#oldtmpOrderID').val();
    tOrderID = $('#tOrderID').val();
    ajaxCall("GET", { "tOrderID": tOrderID, "tmpOrderID": oldtmpOrderID }, "application/json; charset=utf-8", "/TempPO/getInvoiceDetails",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        //console.log(ien);
        $('#tblProduct').find('tbody').empty();

        if (ien > 0) {
            bindTempData(data);
            
            for (var i = 0; i < ien; i++) {
                var row = '';
                var packet = 1;
                var expDate;
                if (data.qry[i].ExpiryDate) {
                    var num = (data.qry[i].ExpiryDate).match(/\d+/g);
                    var date = new Date(parseFloat(num));
                    //console.log("date" +date);
                    var month = parseInt(date.getMonth()) + 1;
                    var dateString = date.getFullYear() + "-" + month + "-" + date.getDate();
                    expDate = dateString;
                }

                var exUnitPrice = 0;
                var vehCode = (data.qry[i].VehicleCode);
                var ProductID = data.qry[i].ProductID;
                var PartNO = data.qry[i].PartNo;
                var UnitCode = ""; // 3   
                var Qty = data.qry[i].Qty;
                var UnitPrice = data.qry[i].UnitPrice;
                var SalePrice = (data.qry[i].SalePrice);

                var PrDisAmount = data.qry[i].PrDiscAm||0;
                var PrDisPercent = data.qry[i].PrDiscPer || 0;
                var PrGSTPercent = data.qry[i].PrGSTPer || 0;
                var PrGSTAmount = data.qry[i].PrGSTAm || 0;

                var SubTotal = data.qry[i].Total;//$("#SubTotal").val(); // 8

                var newTotal = SubTotal + (Qty * PrGSTAmount) - (Qty * PrDisAmount);


                var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';

                var SubTotal = data.qry[i].Total;
                var isPack = '<input type="hidden" id="isPacket" value="' + $("#isPacket").val() + '"/>';

                var markup = "<tr><td><input type='checkbox' name='record'></td><td hidden>" + isPack + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td contenteditable='true' id=Location hidden>" + Location + "</td><td id='unitCode' hidden>" + UnitCode + "</td><td id='expDate'>" + expDate + "</td><td contenteditable='true' id='ProductQty'>" + Qty + "</td><td id='Packet' hidden>" + packet + "</td><td contenteditable='true' id='ProductCostPrice'>" + parseFloat(UnitPrice).toFixed(2) + "</td><td id='exchangePrice' hidden>" + parseFloat(exUnitPrice).toFixed(2) + "</td><td contenteditable='true' id='ProductSalePrice'>" + SalePrice + "</td><td contenteditable='false' id='ProductDiscountPercent'>" + PrDisPercent + "</td><td contenteditable='false' id='ProductDiscount'>" + PrDisAmount + "</td><td contenteditable='false' id='ProductGSTPercent'>" + PrGSTPercent + "</td><td contenteditable='false' id='ProductGST'>" + PrGSTAmount +"</td><td id='ProductSubTotal'>" + parseFloat(newTotal).toFixed(2) + "</td></tr>";
                $("#tblProduct tbody").append(markup);
                $tableItemCounter++;
                $addedProductIDs.push(ProductID);

            }
            calcTotal();

        }
    }
    function onFailure(err) {
        console.log(err);
    }
}
function checkNull(value) {
    return JSON.stringify(value).replace(/null/g, "\"\"");
}
function hideTempOrder(tmpOrderID, tOrderID) {

    var json = { "tmpOrderID": tmpOrderID, "tOrderID": tOrderID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/TempPO/falseTempOrder',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            //console.log("IsDeleted flag true successfully!");
        },
        error: function (err) { console.log(err); }
    });
}
