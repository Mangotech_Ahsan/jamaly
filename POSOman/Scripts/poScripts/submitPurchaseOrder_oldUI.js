﻿var controlId = 0;
var vehControlId = 0;
var tmpOrderID = 0;
var tOrderID = 0;
var price = 0;
var perPieceExp = 0;
var perProductExp = 0;
var expensePercentage = 0;
getPOID();
$("#Bank").prop("disabled", true);
//  Get New POID 
function getPOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getNewPOID',
        async: true,
        success: function (data) {
            {
                $("#POID").val(data);
                POID = data;
                console.log(POID+" POID" );
            }
        },
        error: function (err) { console.log(err); }
    });
}
// Bind Unsaved Order Data , Payment and Vendor Details if Exist
function bindTempData(data) {       
    vendorAccountID = data.qry[0].AccountID;
    $('#AccountID').val(vendorAccountID).trigger('change.select2');
       
    document.getElementById('TotalAmount').value = data.qry[0].TotalAmount;
    document.getElementById('BranchID').value = data.qry[0].BranchID;
    $("#Currency option:contains(" + data.qry[0].Currency + ")").attr("selected", true);
}
// Get Unsaved order and its details
function getPurchaseOrderDetails() {    
    tOrderID = $('#tOrderID').val();
    ajaxCall("GET", { "tOrderID": tOrderID }, "application/json; charset=utf-8", "/Purchase/getPurchaseOrder",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;        
        $('#tbl').find('tbody').empty();

        if (ien > 0) {
            bindTempData(data);
            for (var i = 0; i < ien; i++) {
                var row = '';
                var prodID = data.qry[i].ProductID;
                var vehCodeID = (data.qry[i].VehicleCodeID);
                var convRate = data.qry[i].ExchangeRate;
                var description = data.qry[i].Description;
                var substNo = checkNull(data.qry[i].SubstituteNo);
                var vehModel = checkNull(data.qry[i].VehicleName);
                var vehGroup = checkNull(data.qry[i].GroupName);
                var location = checkNull(data.qry[i].Location);
                var unitCode = checkNull(data.qry[i].UnitCode);
                var salePrice = checkNull(data.qry[i].SalePrice);
                if (convRate > 0) {
                    price = price * convRate;
                }
                row += '<tr>';
                row += '<td ">';
                row += '<select id="ddlVehCode' + vehControlId + '"  name="ddlVehCode' + vehControlId + '" onchange="getProducts(this)" class="select2 form-control"></select>';
                row += '<input type="hidden" id="hdnVId' + vehControlId + '" />';
                row += '</td>';
                row += '<td ">';
                row += '<select id="ddlPartNumber' + controlId + '" name="ddlPartNumber' + controlId + '" value=' + data.qry[i].ProductID + ' onchange="getDetail(this)" class="select2 form-control" style="width: 150px" ></select>';
                row += '<input type="hidden" id="hdnPId' + controlId + '" />';
                row += '</td>';
                row += '<td ">';
                row += '<button type="button" value="edit" id="btnEdit" name="btnEdit" class="btn btn-danger btn-sm" onclick="editProduct(this)" >Edit</button>';
                row += '</td>';
                row += '<td><input type="text" value=' + description + ' class="form-control input-sm" style="width: 130px" name="" id="txtDescription" disabled /></td>';
                row += '<td><input type="text" value=' + substNo + ' class="form-control input-sm" name="" id="txtVNumber" disabled/></td>';
                row += '<td><input type="text" value=' + vehModel + ' class="form-control input-sm" name="" id="txtVmodel" disabled/></td>';
                row += '<td><input type="text" class="form-control" value=' + vehGroup + ' name="" id="txtGroup" disabled/></td>';
                row += '<td><input type="text" class="form-control input-sm" value=' + location + ' name="" id="txtLocation" disabled/></td>';
                row += '<td><input type="text" class="form-control input-sm" value=' + unitCode + ' name="" id="txtUnCode" disabled/></td>';
                row += '<td><input type="Number" class="form-control" style="width: 70px" value=' + data.qry[i].Qty + ' name="qty[]" id="txtQty" min="1" /></td>';
                row += '<td><input type="Number" class="form-control"  value=' + data.qry[i].UnitPrice + ' name="price[]" id="txtPrice" min="1" />' + '</td>';
                row += '<td><input type="Number" class="form-control" name="newRate[]" value=' + price + ' id="txtERate" disabled/>' + '</td>';
                row += '<td><input type="Number" class="form-control"  value=' + salePrice + ' name="salePrice[]" id="txtSalePrice"  /></td>';
                row += '<td><input type="Number" class="form-control" style="width: 100px" value=' + data.qry[i].Total + ' name="sub" style="width: 70px" id="txtSub" disabled />' + '</td>';
                row += '<td><button type="button" value="Remove" id="btnRemove" name="btnRemove" class="btn btn-danger btn-sm" onclick="remove(this)" tabindex="0">Remove</button></td>';
                row += '</tr>';
                $("#tbl tbody").append(row);
                GetDropdown("ddlVehCode" + controlId, "/VehicleCode/getProduct_VehCode", "{}", vehCodeID);
                GetDropdown("ddlPartNumber" + vehControlId, "/Purchase/getProduct_PartNumbers", "{}", prodID);
                controlId += 1;
                vehControlId += 1;
            }
            calcTotal();
        }
    }
    function onFailure(err) {
        console.log(err);
    }
}
function checkNull(value) {
    return JSON.stringify(value).replace(/null/g, "\"\"");
}
//Populate Dropdowns
function GetDropdown(ddl, method, _data, pID) {

    $("#" + ddl).empty();
    $("#" + ddl).select2();
    ajaxCall("POST", _data, "application/json; charset=utf-8", method, "json", onSuccess);
    function onSuccess(Result) {
        var data = Result;// JSON.parse(Result.d);
        $.each(data, function (key, value) {
            $("#" + ddl).append($("<option></option>").val(value.Value).html(value.Name));
        });
        $("#" + ddl).val(pID);
    }
}
$(function () {
    getPurchaseOrderDetails();
    $('#btnAddRow').click(function () {
        //insertTempData();
        addRow();
    })
    $('#btnSubmit').click(function () {
        // Confirm required fields from Maaz bhai     
        var isValid = true;
        var accID = $('#AccountID option:selected').val();
        var InvoiceNo = $('#InvoiceNo').val();
        var PurchaseDate = $('#PurchaseDate').val();
        var paymentStatus = $('#PaymentStatus option:selected').val();
        //    var PaymentStatus = $('#PaymentStatus').val();
        var PaymentTypeID = $('#PaymentType option:selected').val();
        var BranchID = $('#BranchID option:selected').val()
        //if (BranchID == "" || BranchID == 0 || BranchID == undefined) {
        //    isValid = false;
        //    swal("Branch", "Please Select Branch!", "error");
        //    //alert("Please Select Branch");
        //}
         if (accID == "" || accID == 0 || accID == undefined) {
            isValid = false;
            swal("Vendor", "Please Select Vendor!", "error");
            //alert("Please Select Vendor");
        }
        else if (PurchaseDate == "") {
            isValid = false;
            swal("Date", "Please Enter Purchase Date!", "error");
            //alert("Please Enter Purchase Date");
            //console.log(PurchaseDate + "date");
        }
        else if (InvoiceNo == "") {
            isValid = false;
            swal("Invoice No.", "Please Enter Invoice No!", "error");
            //alert("Please Enter Invoice No");
            //console.log(PurchaseDate+"date");
        }

        else if (paymentStatus == 0) {
            isValid = false;
            swal("Payment Status.", "Please Select Payment Status!", "error");
            //alert("Please Select Payment Status");
        }
        else if ((paymentStatus == 1 || paymentStatus == 2) && (PaymentTypeID == "" || PaymentTypeID == undefined)) {

            isValid = false;
            swal("Payment Type.", "Please Select Payment Type!", "error");
            //alert("Please Select Payment Type");                     
        }
        else if (isValid == true) {
            insert();
        }
    });
    $('#tbl').keyup(function (e) {
        var qty = $(e.target).closest("tr").find('input[id*="txtQty"]').val();
        var price = $(e.target).closest("tr").find('input[id*="txtPrice"]').val();
        var currency = $('#Currency').val();
        if (currency == 2) {
            var convRate = $('#exRate').val();
            price = price * convRate;
            $(e.target).closest("tr").find('input[id*="txtERate"]').val(price);
        }
        if (($.isNumeric(price)) && ($.isNumeric(qty))) {
            var amount = (qty * price);
            $(e.target).closest('tr').find('input[id*="txtSub"]').val(parseFloat(amount).toFixed(2)
            );
            calcTotal();
        }
        else {
            $(e.target).closest('tr').find('input[id*="txtSub"]').val(''
            );
        }
    });
})
// expense and VAT calculation
function calcVAT() {
    var vat = parseFloat($('#vatInput').val());
    if (vat == "" || isNaN(vat)) {
        vat = 0; $('#vatInput').val(0)
        $('#vatAmount').val(parseFloat(0));
    }
    else {
        var totalInvoice = parseFloat($('#tbl tfoot tr').find('input[id*="txtTotal"]').val());
        var vatAmount = parseFloat((vat * totalInvoice) / 100);
        $('#vatAmount').val(parseFloat(vatAmount).toFixed(2));
    }
}
$("#expenseInput").on("change", function () {
    getNewTotal();
    calcVatExpAmount();
});
$("#vatInput").on("change", function () {
    calcVAT();
    getNewTotal();
    calcVatExpAmount();
});
function calcVatExpAmount() {
    var vat = parseFloat($('#vatAmount').val());
    var exp = parseFloat($('#expenseInput').val());
    var productTotal = $('#tbl tfoot tr').find('input[id*="txtTotal"]').val();
    if (vat == "" || isNaN(vat)) {
        vat = 0;
        $('#vatInput').val(0);
    }
    if (exp == "" || isNaN(exp)) {
        exp = 0; $('#expenseInput').val(0);
    }
    //calcTotalQty();
    if (exp > 0 || vat > 0) {
        var totalParts = $('#tbl tbody tr').length;
        var totalVATExp = parseFloat(exp) + parseFloat(vat);
        expensePercentage = parseFloat(totalVATExp) / parseFloat(productTotal);
        //console.log(expensePercentage + " expensePercentage ");
        //perProductExp = parseFloat(totalVATExp) / parseFloat(totalParts);
        //console.log(totalParts + " Rows");
        //var total = parseFloat($('#tbl tfoot tr').find('input[id*="txtTotal"]').val()).toFixed(2);
        //var totalVATExp = parseFloat(exp) + parseFloat(vat);
        //console.log(totalVATExp + " totalVATExp");
        //var percent = parseFloat((totalVATExp / total) * 100);
        //perPieceExp = parseFloat((percent * total) / 100);
        //totalExpAmount = (parseFloat(percent) * parseFloat(total)) / 100;
        //console.log(totalExpAmount + " totalExpAmount");
        //perPieceExp = parseFloat(totalExpAmount) / totalParts;
        //console.log(perPieceExp + " perPieceExp");
    }
}
// if any of amount, expense , vat is changed call this to make effect every where 
function getNewTotal() {
    var gTotal = $('#tbl tfoot tr').find('input[id*="txtTotal"]').val();
    var Expense = $('#expenseInput').val();
    var VAT = $('#vatAmount').val();
    if (Expense == "" || isNaN(Expense)) { Expense = 0; }
    if (VAT == "" || isNaN(VAT)) { VAT = 0; $('#vatAmount').val(0) }
    var newTotal = parseFloat(gTotal) + parseFloat(Expense) + parseFloat(VAT);
    var amountPaid = parseFloat(gTotal) + parseFloat(VAT);
    $('#TotalAmount').val(parseFloat(newTotal).toFixed(2));
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(amountPaid).toFixed(2);
    }

}
//Add New Row For Product
function addRow() {
    var row = '';
    row += '<tr>';
    row += '<td ">';
    row += '<select id="ddlVehCode' + vehControlId + '" name="ddlVehCode' + vehControlId + '" onchange="getProducts(this)" class="select2 form-control"></select>';
    row += '<input type="hidden" id="hdnVId' + vehControlId + '" />';
    row += '</td>';
    row += '<td ">';
    row += '<select id="ddlPartNumber' + controlId + '" name="ddlPartNumber' + controlId + '" onchange="getDetail(this)" class="select2 form-control" style="width: 150px" ></select>';
    row += '<input type="hidden" id="hdnPId' + controlId + '" />';
    row += '</td>';
    row += '<td ">';
    row += '<button type="button" value="edit" id="btnEdit" name="btnEdit" class="btn btn-danger btn-sm" onclick="editProduct(this)" >Edit</button>';
    row += '</td>';
    row += '<td><input type="text" class="form-control input-sm" style="width: 150px" name="" id="txtDescription" disabled /></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtVNumber" disabled/></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtVmodel" disabled/></td>';
    row += '<td><input type="text" class="form-control" name="" id="txtGroup" disabled/></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtLocation" disabled/></td>';
    //row += '<td><input type="text" class="form-control input-sm" name="" id="txtVehCode" disabled/></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtUnCode" disabled/></td>';
    row += '<td><input type="Number" class="form-control input-sm" style="width: 80px" name="qty[]" id="txtQty" min="1" /></td>';
    row += '<td><input type="Number" class="form-control input-sm" style="width: 80px" name="price[]" id="txtPrice" min="1" />' + '</td>';
    row += '<td><input type="Number" class="form-control input-sm" name="newRate[]" id="txtERate" disabled/>' + '</td>';
    //row += '<td><input type="Number" class="form-control input-sm" name="discPercent[]" id="txtDiscount" />' + '</td>';
    row += '<td><input type="Number" class="form-control input-sm" style="width: 80px" name="salePrice[]" id="txtSalePrice"  /></td>';
    row += '<td><input type="Number" class="form-control input-sm" style="width: 120px" name="sub" style="width: 70px" id="txtSub" disabled />' + '</td>';
    row += '<td><button type="button" value="Remove" id="btnRemove" name="btnRemove" class="btn btn-danger btn-sm" onclick="remove(this)" tabindex="0">Remove</button></td>';
    row += '</tr>';

    $("#tbl tbody").append(row);
    GetDropdown("ddlVehCode" + vehControlId, "/VehicleCode/getProduct_VehCode", "{}", true);
    //GetDropdown("ddlPartNumber" + controlId, "/Purchase/getProduct_PartNumbers", "{}", true);
    controlId += 1;
    vehControlId += 1;
    //table.setAttribute("id", "myId");
    //console.log(document.getElementById("tbl").classList);

}
function getProducts(pro) {
    var tr = $(pro).closest('tr');

    var vehCodeID = $(tr).find('td:eq(0) :selected').val();
    var controlID = $(pro).closest('tr').index();
    var json = { "vehCodeId": vehCodeID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getProducts',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("ddlPartNumber" + controlID, data, true);
        },
        error: function (err) { console.log(err); }
    });
}
function getDetail(pro) {
    var currency = $('#Currency').val();
    var exchangeRate = $('#exRate').val();
    //console.log("rate"+exchangeRate);
    var accountID = $('#AccountID option:selected').val();
    if (currency == 0)
    { alert("Please Select Currency First !"); }
    else
    {
        var tr = $(pro).closest('tr');
        var pId = $(tr).find('td:eq(1) :selected').val();
        $(tr).find('input[id*="hdnPId"]').val(pId);
        //productDuplicate(pId);
        // get product description
        var json = { "productId": pId };
        //ajaxCall("GET", json, "application/json; charset=utf-8", "/Purchase/getProductDetail", "json", onSuccess, onFailure);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Purchase/getProductDetail',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                $(tr).find('input[id*="txtDescription"]').val(data.Description);
                $(tr).find('input[id*="txtVNumber"]').val(data.SubstituteNo);
                $(tr).find('input[id*="txtGroup"]').val(data.GroupName);
                $(tr).find('input[id*="txtVmodel"]').val(data.VehicleName);
                $(tr).find('input[id*="txtLocation"]').val(data.Location);
                $(tr).find('input[id*="txtVehCode"]').val(data.VehicleCode);
                $(tr).find('input[id*="txtUnCode"]').val(data.UnitCode);
            },
            error: function (err) { console.log(err); }
        });
        var json = { "productId": pId };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Purchase/getLastUnitPrice',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                $(tr).find('input[id*="txtPrice"]').attr('placeholder', data);
            },
            error: function (err) { console.log(err); }
        });
    }
}
function editProduct(input) {
    var tr = $(input).closest('tr');
    var pId = $(tr).find('td:eq(1) :selected').val();
    event.preventDefault();

    var url = '/PartialProduct/Edit/?id=' + pId;
    $.get(url, function (data) {
        $('#editProductContainer').html(data);
        $('#editProductModal').modal('show');
        //debugger;
    });
}
function remove(input) {

    var totalRowCount = $("#tbl tbody tr").length;
    if (totalRowCount > 1) {
        $(input).parent().parent().remove();
        calcTotal();        
        controlId--;
        vehControlId--;  
    }
    else {
        alert("Cannot remove all rows");
    }

}
function onCurrChange() {
    //console.log("OnCurrCalled");
    $('#tbl  tbody tr').each(function (i, row) {
        // reference all the stuff you need first
        var $row = $(row);
        //var qty = $row.find('input[id*="txtQty"]').val();                 
        //console.log($row + "  qty = " + qty);
        var qty = $row.find('input[id*="txtQty"]').val();
        var price = $row.find('input[id*="txtPrice"]').val();
        var currency = $('#Currency').val();
        if (currency == 2) {
            var convRate = $('#exRate').val();
            price = price * convRate;
            $row.find('input[id*="txtERate"]').val(price);
        }
        else {
            $row.find('input[id*="txtERate"]').val("");
        }
        if (($.isNumeric(price)) && ($.isNumeric(qty))) {
            var amount = (qty * price);
            $row.find('input[id*="txtSub"]').val(parseFloat(amount).toFixed(2)
            );
            calcTotal();
        }
        else {
            $row.find('input[id*="txtSub"]').val(''
            );
        }
    });
}
function calcTotal() {
    var total = 0;
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        //var subTotal = parseFloat($row.find('td:eq(13) input[id*="txtSub"]').val());  
        var subTotal = parseFloat($row.find('input[id*="txtSub"]').val());
        total += parseFloat(subTotal);
    });

    $('#tbl tfoot tr').find('input[id*="txtTotal"]').val(parseFloat(total).toFixed(2));
    //console.log(total);
    $('#TotalAmount').val(parseFloat(total).toFixed(2));
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(total).toFixed(2);
    }
}
// Calculate Price and Sub Amount on qty and price input
$('#tbl').keyup(function (e) {
    var qty = $(e.target).closest("tr").find('input[id*="txtQty"]').val();
    var price = $(e.target).closest("tr").find('input[id*="txtPrice"]').val();
    var currency = $('#Currency').val();
    if (currency == 2) {
        var convRate = $('#exRate').val();
        price = price * convRate;
        $(e.target).closest("tr").find('input[id*="txtERate"]').val(price);
        //console.log("convRate = " + convRate);
        //console.log("newPrice = " + price);
    }
    if (($.isNumeric(price)) && ($.isNumeric(qty))) {
        var amount = (qty * price);
        $(e.target).closest('tr').find('input[id*="txtSub"]').val(parseFloat(amount).toFixed(2)
        );
        calcTotal();
    }
    else {
        $(e.target).closest('tr').find('input[id*="txtSub"]').val(''
        );
    }
});

// If currency is AED then perform actions
$("#Currency").on("change", function () {
    var currencyID = $("#Currency").val();
    if (currencyID == 1) {
        onCurrChange();                       
    }
    if (currencyID == 2) {        
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/Purchase/getCurrency',
            async: true,
            success: function (data) {                
                $("#exRate").val(data);
                calcTotal();
                onCurrChange();
            },
            error: function (err) { console.log(err); }
        });
    }
});
// Payment Status DropDOwn, Action according to Payment Status Selection
$("#PaymentStatus").on("change", function () {
    var payStatusSelection = $("#PaymentStatus").val();
    var totalAmount = $("#TotalAmount").val();
    // PAID
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = totalAmount;
        $("#PaymentType").prop("disabled", false);
        //document.getElementById("expenseInput").readOnly = false;
        //document.getElementById("vatInput").readOnly = false;
    }
        //Partial PAID
    else if (payStatusSelection == 2) {
        document.getElementById("amountPaid").readOnly = false;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("amountPaid").value = 0;
        //document.getElementById("expenseInput").readOnly = false;
        //document.getElementById("vatInput").readOnly = false;
    }
        //Unpaid
    else if (payStatusSelection == 3) {
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        $("#PaymentType").prop("disabled", true);
        //document.getElementById("expenseInput").readOnly = true;
        //document.getElementById("vatInput").readOnly = true;
    }


});
// Payment Type Change 
$("#PaymentType").on("change", function () {
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //Cash
    if (PaymentTypeID == 1) {
        //$("#bankName").prop("disabled", true);
        $("#Bank").prop("disabled", true);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);
    }
        // Bank
    else if (PaymentTypeID == 2) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);

    }
        // Cheque
    else if (PaymentTypeID == 3) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", false);
        $("#chqDate").prop("disabled", false);
    }
});
function insert() {
    var exchangeRate = $('#exRate').val();
    var rows = [];
    var stockLog = [];
    var total = 0;
    var qtyVE = 0;
    var isValid = false;
    totalInvoice = $('#TotalAmount').val();
    //if (totalInvoice == "" || totalInvoice == 0) {
    var paidAmount = $('#amountPaid').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    //console.log(totalInvoice);
    var vat = parseFloat($('#vatAmount').val());
    var exp = parseFloat($('#expenseInput').val());
    if (vat == "" || isNaN(vat)) {
        vat = 0;
        $('#vatInput').val(0);
    }
    if (exp == "" || isNaN(exp)) {
        exp = 0; $('#expenseInput').val(0);
    }
    calcVatExpAmount();

    // 
    $('#tbl tbody tr').each(function (i, n) {

        var $row = $(n);
        var pId = $row.find('td:eq(1) :selected').val();
        var partNumber = $row.find('td:eq(1) option:selected').text();
        var pLocation = $row.find('input[id*="txtLocation"]').val();
        var qty = $row.find('td:eq(9) input[type="Number"]').val();
        var price = $row.find('td:eq(10) input[type="Number"]').val();
        //var newRate = $row.find('td:eq(9) input[type="Number"]').val();  // add new field to Database if required
        var salePrice = $row.find('td:eq(12) input[type="Number"]').val();
        var subTotal = $row.find('td:eq(13) input[type="Number"]').val();
        exchangeRate = $row.find('input[id*="txtERate"]').val();
        if (pId != "" && qty != "" && qty != 0 && price != "" && price != 0) {
            isValid = true;
            rows.push({
                PartNo: partNumber,
                ProductId: pId,
                Location: pLocation,
                Qty: qty,
                UnitPrice: price,
                IsReturned: false,
                ReturnedQty: 0,
                SalePrice: salePrice,
                ExchangeRate: exchangeRate,
                Total: subTotal,
                BranchID: $('#BranchID option:selected').val()
            });
            var total1 = parseFloat(expensePercentage) * parseFloat(subTotal);
            var costing = parseFloat(total1) / parseInt(qty);
            //perPieceExp = perProductExp / qty;
            var newprice = parseFloat(price) + parseFloat(costing);
            stockLog.push({
                AccountID: $('#AccountID option:selected').val(),
                ProductId: pId,
                StockIN: qty,
                CostPrice: parseFloat(newprice).toFixed(2),
                SalePrice: salePrice,
                Location: pLocation,
                InReference: 'Purchase',
                OrderTypeID: 1,
                UserReferenceID: $('#InvoiceNo').val(),
                BranchID: $('#BranchID option:selected').val(),
                InvoiceDate: $('#PurchaseDate').val()
            });
        }
        else if (pId == "") {
            isValid = false;
            swal("Product", "Please Select Product!", "error");
            //alert("Please Select Product");
        }
        else if (qty == "" || qty == 0) {
            isValid = false;
            swal("Quantity", "Please Enter Quantity!", "error");
            //alert("Please Enter Quantity");
        }
        else if (price == "" || price == 0) {
            isValid = false;
            swal("Unit Cost", "Please Enter Unit Cost!", "error");
            //alert("Please Enter Unit Cost");
        }
        else {
            swal("Error", "Some error occured!", "error");
            //alert("Error!!!");
        }
    });

    if (rows.length && isValid == true) {

        total = $('#tbl tfoot tr').find('input[id*="txtTotal"]').val();
        //total = $('#Total').val()
        var paymentStatus = 0;
        if ($('#PaymentStatus option:selected').val() != 0) {
            paymentStatus = $('#PaymentStatus option:selected').text()
        }
        var invoiceAmount = parseFloat(total + vat).toFixed(2);
        var data = {
            'AccountID': $('#AccountID option:selected').val(),
            'BranchID': $('#BranchID option:selected').val(),
            'POID': $("#POID").val(),
            'InvoiceNo': $('#InvoiceNo').val(),
            'PaymentStatus': $('#PaymentStatus').val(),
            'PurchaseDate': $('#PurchaseDate').val(),
            'PaymentStatus': paymentStatus,
            'PaymentTypeID': $('#PaymentType option:selected').val(),
            'VAT': vat,   // confirm from Maaz Bhai wither we should put % or Amount
            'Expenses': $('#expenseInput').val(),
            'PurchaseCode': $('#PurchaseCode').val(),
            'Currency': $('#Currency option:selected').text(),
            'TotalAmount': invoiceAmount,
            'ChequeDate': $("#chqDate").val(),
            'ChequeNo': $("#chqNumber").val(),
            'BankName': bank,
            'AmountPaid': $('#amountPaid').val(),
            'tbl_PODetails': rows
        };
        var json = JSON.stringify({ 'model': data, 'modelStockLog': stockLog, 'bankAccId': bankAccountId });

        ajaxCall("POST", json, "application/json; charset=utf-8", "/Purchase/SaveOrder", "json", onSuccess, onFailure);
        //debugger;
        function onSuccess(Result) {
            if (Result == "success") {
                updatePurchaseOrderStatus();
                window.location.href = '/Purchase';
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                //alert("Some error Ocurred! Please Check Your Entries");
            }
            //location.reload();
            //window.location.href = 'Index';
            //alert("success");
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                //location.reload();
                //window.location.href = 'Index';
                //deleteTempOrder(tmpOrderID);
                //console.log(error.status);
                //alert(error.status);
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }

}
function updatePurchaseOrderStatus() {

    var json = { "tOrderID": tOrderID, "invoiceNo": $('#InvoiceNo').val() };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/changePOStatus',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            console.log("Status changed successfully!");
        },
        error: function (err) { console.log(err); }
    });
}
