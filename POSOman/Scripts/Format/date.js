﻿
/// This function is use for Show Date in TextBox when date is comes from datebase ///
function SetDateToTextField(RecDate, TextBoxID) {
    var date = new Date(RecDate);
    date = date.setDate(date.getDate() + 1);
    document.getElementById(TextBoxID).valueAsDate = new Date(date);
}
