﻿

var $addedProductIDs = [];
var $tableItemCounter = 0;

$("#ddlVehCode").change(function () {
    var vehCodeID = $('#ddlVehCode').val();
    GetProducts(vehCodeID);
});

function GetProducts(vehCodeID) {

    if (vehCodeID == "") {
        vehCodeID = -1;
    }
    var json = { "vehCodeId": vehCodeID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getProducts',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("ddlPartNumber", data, true);
        },
        error: function (err) { console.log(err); }
    });
}


$("#ddlPartNumber").change(function () {
    var pId = $('#ddlPartNumber').val();
    if (pId != "" || pId > 0) {
        GetDetail(pId);
    }
});

$("#AccountID").change(function () {
    var accID = +$('#AccountID').val() || 0;
    $("#tblProduct tbody").empty();
    $(".ChallanAppend").empty();
    SDchallanIDs = [];

    if (accID > 0) {
        var json = { "AccID": accID };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Challan/getChallansCustomerWise',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                $("#InvType").val(data.InvoiceType);
                GetDropdown1('ChallanID', data.Challans);
                
                $("#ChallanID").prop('disabled', false);
                $("#hs_Code").val(data.HSCode);
                console.log(data)
            },
            error: function (err) { console.log(err); }
        });
 
    }
    else {
        $("#ChallanID").val(null).trigger('change');
        $("#ChallanID").prop('disabled', true);
    }
});


function GetDetail(pId) {
    var BranchId = $('#BranchID option:selected').val();
    var accID = $('#hdnAccountID').val();
    var json = { "productId": pId, "BranchID": BranchId };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#Stock').val(data[0].stock);
            $('#productStock').val(data[0].stock);
            $('#SalePrice').val(data[0].SalePrice);
            $("#Qty").val(1);
            $("#prodSize").val(data[0].qry.prodSize);
            CalcProductTotal();
        },
        error: function (err) { console.log(err); }
    });
}

$(".Calc").keyup(function () {
    CalcProductTotal();
});

function CalcProductTotal() {
    var qty = parseFloat($("#Qty").val() || 0);
    var saleprice = parseFloat($("#UnitPrice").val() || 0);
    var total = parseFloat(qty * saleprice).toFixed(2);

    $("#Total").val(total);
}


$("#addRow").click(function () {
    var CategoryID = $("#ddlVehCode").val();
    var CategoryName = $("#ddlVehCode :selected").text();
    var ProductID = $("#ddlPartNumber").val();
    var ProductName = $("#ddlPartNumber :selected").text();
    var UnitCode = $("#unitCode").val();
    var Qty = parseFloat($("#Qty").val()) || 0;
    var Stk = +$("#productStock").val() || 0;
    var UnitPrice = parseFloat($("#UnitPrice").val()) || 1;
    var SubTotal = parseFloat($("#Total").val()) || 1;

    console.log(UnitCode)
    GenerateProductTable(null, CategoryID, CategoryName, ProductID, ProductName, UnitCode, Qty, UnitPrice, SubTotal,Stk);

});

function GenerateProductTable(ChallanID, CategoryID, CategoryName, ProductID, ProductName, UnitCode, Qty, UnitPrice, Total,Stk) {

    if (ProductID > 0 && Qty > 0 && Total > 0 && UnitCode != "") {
        var index = $.inArray(+ProductID, $addedProductIDs);
        if (index >= 0) {
            swal("Error", "Product Already Added!", "error");
        }
        else {

            var markup = "<tr>\
                        <td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deleteRow(this)'/></td>\
                        <td id='ChallanID' hidden>" + ChallanID + "</td>\
                        <td id='VehicleCodeID' hidden>" + CategoryID + "</td>\
                        <td> " + CategoryName + "</td>\
                        <td id='ProductID' hidden>" + ProductID + "</td>\
                        <td id='PartNo'>" + ProductName + "</td>\
                        <td id='UnitCode'>" + UnitCode + "</td>\
                        <td id='Qty' contenteditable='true'>" + Qty + "</td>\
                        <td id='UnitPrice' contenteditable='true' >" + UnitPrice + "</td>\
                        <td id='SubTotal' >" + Total + "</td>\
                    </tr>";

            $("#tblProduct tbody").prepend(markup);
            $addedProductIDs.push(+ProductID);
            ClearFields();
            CalcTotal();
        }
    }

    else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
    else if (Stk <= 0 || Qty > Stk) { swal("Error", "Stock Not Available!", "error"); }
    else if (UnitCode == "" || ProductID == "undefined") { swal("Error", "Please Select UnitCode!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
    else if (Total == "" || Total == "undefined") { swal("Error", "Please enter Amount!", "error"); }

}



function deleteRow(r) {

    var i = r.parentNode.parentNode.rowIndex;
    console.log(r);
    document.getElementById("tblProduct").deleteRow(i);
    var row = $(r).closest("tr");
    var productID = row.find('#ProductID').text(); // find hidden id 
    var index = $.inArray(productID, $addedProductIDs);
    if (index >= 0) { $addedProductIDs.splice(index, 1); }
    CalcTotal();
}

function findRows() {
    $('#tblProduct tr').each(function () {
        var customerId = $(this).find("#ProductID").html();
        console.log(customerID);
    });
}


function ClearFields() {
    $('#ddlVehCode').val(null).change();
    $('#ddlPartNumber').val(null).change();
    $("#unitCode").val("").change();
    $("#Qty").val("");
    $("#productStock").val("");
    $("#UnitPrice").val("");
    $("#Total").val("");
    $("#prodSize").val("");
}

$('#tblProduct').keyup(function (e) {

    var $row = $(e.target).closest("tr");
    var Qty = parseFloat($row.find('#Qty').text());
    var UnitPrice = parseFloat($row.find('#UnitPrice').text());

    if (($.isNumeric(UnitPrice)) && ($.isNumeric(Qty))) {
        var amount = parseFloat(Qty * UnitPrice).toFixed(2);
        $row.find('#SubTotal').text(amount)
        CalcTotal();
    }
});

