﻿
let SDchallanIDs = [];

//$(".ChallanAppend").on("click", ".searchterm", function (event) {
//    console.log('clicked');
//});


//$('ul.ChallanAppend li').click(function (e) {
    
//});

$("#SalePersonAccID").change(function () {
    let AccID = +$("#SalePersonAccID").val() || 0;
    if (AccID > 0) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/Sales1/SalePersonCommissionDetails?AccID=' + AccID,
            async: true,
            success: function (data) {
                console.log(data);
                $("#commissionPercent").val(data);
           },
            error: function (err) { console.log(err); }
        });
    }
})

$("#totalAmount").change(function () {
    let totalAmount = +$("#totalAmount").val() || 0;
    if (totalAmount <= 0) {
        $("#commissionPercent").prop('disabled', true);
        $("#commissionAmount").prop('disabled', true);
    }
    else {
        $("#commissionPercent").prop('disabled', false);
        $("#commissionAmount").prop('disabled', false);

    }
});

$("#commissionPercent").change(function () {
    let commPercent = +$("#commissionPercent").val() || 0;
    let totalAmount = +$("#totalAmount").val() || 0;
    if (totalAmount > 0) {
        let commAm = totalAmount * (commPercent / 100);
        $("#commissionAmount").val(parseFloat(commAm).toFixed(2));
    }
    else {
        $("#commissionAmount").val(0)
    }

})

$("#commissionAmount").change(function () {
    let totalAmount = +$("#totalAmount").val() || 0;
    let commAm = +$("#commissionAmount").val() || 0;
    if (totalAmount > 0) {
        let commPer = (commAm * 100) / totalAmount;
        $("#commissionPercent").val(parseFloat(commPer).toFixed(2));
    }
    else {
        $("#commissionPercent").val(0)
    }
})

function getNewTotal() {
    applyDiscount();
    // applyLoyaltyDiscount();
    calcVAT();
    calcAmountPaid();
    var subTotal = $("#subAmount").val();
    var VAT = $('#vatAmount').val();
    var discount = $('#discInput').val();
    var loyaltyDiscount = $('#redeemPoints').val();
    if (VAT == "" || isNaN(VAT)) {
        VAT = 0;
        applyZeroVAT();
    }
    if (discount == "" || isNaN(discount)) {
        discount = 0;
        applyDiscount();
    }
   
    var newTotal = parseFloat(subTotal) + parseFloat(VAT) - parseFloat(discount);
   
    $('#finalAmountWithVAT').val(parseFloat(newTotal).toFixed(2));
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(newTotal).toFixed(2);
    }

}

function applyZeroVAT() {
    var vat = parseFloat($('#vatInput').val());
    //var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        //$('#vatInput').val(0);
        vat = 0;
        $('#vatAmount').val(0);
        $('#finalAmountWithVAT').val($("#totalAmount").val());
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat($("#totalAmount").val()).toFixed(2);
        }
    }
}

// in case of vat entered please do calculations
function calcVAT() {
    var vat = parseFloat($('#vatInput').val());
    //var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        applyZeroVAT();
    } else {
        var totalInvoice = $("#totalAmount").val();
        var vatAmount = parseFloat((vat * totalInvoice) / 100);

        $('#vatAmount').val(parseFloat(vatAmount).toFixed(2));
        var finalAmount = parseFloat(vatAmount) + parseFloat(totalInvoice);
        $('#finalAmountWithVAT').val(finalAmount);
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat(finalAmount).toFixed(2);
        }
    }
}

// calculate balance if amountPaid entered and make changes every where needed
function calcAmountPaid() {
    isLimitExceed = false;
    var amountPaid = $("#amountPaid").val();
    var finalAmount = $('#finalAmountWithVAT').val();
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 2) {
        if (parseFloat(amountPaid) >= parseFloat(finalAmount)) {
            toastr.warning('Partial ! AmountPaid should be less than total amount ')
            $("#amountPaid").val(0)
        } else {
            var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
            $("#balanceAmount").val(parseFloat(balance).toFixed(2));
        }
    } else {

        var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
        $("#balanceAmount").val(parseFloat(balance).toFixed(2));
    }
}

function applyDiscount() {
    var discount = $('#discInput').val();
    var subTotal = $("#subAmount").val();
    if (discount == "" || isNaN(discount)) {
        //$('#discInput').val(0);
        discount = 0;
        $('#totalAmount').val(subTotal);
    } else if (parseFloat(discount) >= parseFloat(subTotal)) {
        toastr.warning('Discount must less than total!')
        $('#discInput').val(0);
        $('#totalAmount').val(subTotal);
    } else {
        var totalAmount = parseFloat(subTotal) - parseFloat(discount);
        $('#totalAmount').val(parseFloat(totalAmount).toFixed(2));

    }
    cash();

}

function cash() {
    var paymentStatus = $('#PaymentStatus option:selected').val();
    if (paymentStatus != 3) {
        var cashRec = parseFloat($('#cashReceived').val());
        var invoiceAmount = parseFloat($('#totalAmount').val());
        if (cashRec > 0 && cashRec >= invoiceAmount) {
            var retAmount = parseFloat(cashRec) - parseFloat(invoiceAmount);
            $('#cashReturned').val(parseFloat(retAmount).toFixed(0));
        }
        else if (cashRec < invoiceAmount) {
            swal("Short", "Recd cash must greater than total!", "error");
        }
    }

}

function disableBank() {
    $("#Bank").prop("disabled", true);
    $("#chqNumber").prop("disabled", true);
    $("#chqDate").prop("disabled", true);
}
function arrayRemove(arr, value) {

    return arr.filter(function (ele) {
        return ele != value;
    });
}
function getID(id) {
    console.log('id: ' + id);

    var dataList;
    $('#tblProduct tr').each(function () {
        var tds = $(this).find('td');
        if (tds.length > 0) {


            var index = $.inArray(id, SDchallanIDs);
            if (index >= 0) {
                console.log("ChID-IfExist->" + id);
                SDchallanIDs = arrayRemove(SDchallanIDs, +id);


                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url: '/Challan/ChallanDetail?id=' + id,
                    async: true,
                    success: function (data) {
                        console.log(data);
                        dataList = data;

                    },
                    error: function (err) { console.log(err); }
                });

                console.log("PID->" + tds.eq(4).text());
            }

            setTimeout(function () {
                if (dataList.length > 0) {
                    $.each(dataList, function (key, value) {
                        console.log('Qty: ' + value.Qty + ' | PID: ' + value.ProductID);
                        if (+tds.eq(4).text() === +value.ProductID) {
                            let updatedQty = +(+tds.eq(7).text() - value.Qty);
                            tds.eq(7).text(updatedQty);

                            if (+tds.eq(1).text() === id) {
                                console.log("deleting");
                                $(tds).parent().remove();

                            }

                            var liID = '#li-' + id;
                            $(liID).remove();
                            console.log(liID);

                            //CalcTotal();
                            calculateEditiedTax();
                        }
                    }

                    );
                }
            }, 1200)

        }

    });


}

$("#btnSearch").click(function () {
    if ($("#ChallanID").val() != "") {
        var ChallanID = +$("#ChallanID").val() || 0;
        var index = $.inArray(+ChallanID, SDchallanIDs);
        if (index >= 0) {
            swal('Challan', 'Challan Already Inserted!', 'error');
        }
        else {
            SDchallanIDs.push(ChallanID);
            console.log("After Search->");
            console.log(SDchallanIDs)
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: '/Challan/ChallanDetail?id=' + $("#ChallanID").val(),
                async: true,
                success: function (data) {
                    //console.log(data)
                   // $("#AccountID").val(data[0].AccountID).change();
                    $("#PONo").val(data[0].CustomerPO);
                    $("#PaymentTerm").val(data[0].PaymentTerm);
                    $("#commissionPercent").val(data[0].CommissionPercent);
                    $("#SalePersonAccID").val(data[0].SalePersonAccID).trigger('change.select2');
                    console.log("PO->" + data[0].CustomerPO)

                    $.each(data, function (index, value) {
                        var ChallanID = +$("#ChallanID").val();//value.ChallanID;
                        var Challan = $("#ChallanID option:selected").text();//value.ChallanID;
                        var CategoryID = value.CategoryID;
                        var CategoryName = value.CategoryName;
                        var ProductID = value.ProductID;
                        var ProductName = value.ProductName;
                        var UnitCode = value.UnitCode;
                        var Qty = parseFloat(value.Qty) || 0;
                        var UnitPrice = parseFloat(value.UnitPrice).toFixed(2) || 0;
                        var SubTotal = parseFloat(value.Total).toFixed(2) || 0;

                        //alert("in")
                        GenerateProductTableForSaleOrder(ChallanID, Challan, CategoryID, CategoryName, ProductID, ProductName, UnitCode, Qty, UnitPrice, SubTotal);
                    });
                    var x = $("#ChallanID option:selected").text();
                    if ([...$(".ChallanAppend li")].some(el => $(el).text() === $("#ChallanID option:selected").text()) == false) {
                        $('.ChallanAppend').append('<li style="display: inline-block;border: 5px solid #FFFFFF;border-radius: 50px;background-color: #04FF00;width:90px;text-align:center;" id="li-' + ChallanID + '" data-val=' + ChallanID + ' onclick = getID($(this).data().val)>' + $("#ChallanID option:selected").text() + '</li>');

                    }
                },
                error: function (err) { console.log(err); }
            });
        }
        
    }
    else {
        swal("Challan", "Please Select Challan!", "error")
    }
});

function GenerateProductTableForSaleOrder(ChallanID,Challan, CategoryID, CategoryName, ProductID, ProductName, UnitCode, Qty, UnitPrice, Total) {


        if (ProductID > 0 && Qty > 0) {

            var index = $.inArray(ProductID, $addedProductIDs);

            //if (index >= 0) {

            //    $("#tblProduct").find('tr').each(function (i, el) {
            //        var tds = $(this).find('td');
            //        if (tds.length > 0) {
            //            if (tds.eq(4).text() == ProductID) {
            //                tds.eq(7).text(+tds.eq(7).text() + Qty);
            //            }
            //        }

            //    });
            //}
            //else {
                Total = Qty * UnitPrice;
                var markup = "<tr>\
                        <td hidden><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deleteRow(this)'/></td>\
                        <td id='ChallanID' hidden>" + ChallanID + "</td>\
                        <td id='VehicleCodeID' hidden>" + CategoryID + "</td>\
                        <td> " + CategoryName + "</td>\
                        <td id='ProductID' hidden>" + ProductID + "</td>\
                        <td >" + Challan + "</td>\
                        <td id='PartNo'>" + ProductName + "</td>\
                        <td id='UnitCode'>" + UnitCode + "</td>\
                        <td id='Qty' contenteditable='false'>" + Qty + "</td>\
                        <td id='UnitPrice' contenteditable='true' >" + UnitPrice + "</td>\
                        <td id='SubTotal' >" + parseFloat(Total).toFixed(2) + "</td>\
                    </tr>";

                $("#tblProduct tbody").prepend(markup);
                $addedProductIDs.push(ProductID);
                ClearFields();
            CalcTotal();
            calculateEditiedTax();
          //  }
        }

        else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
        else if (UnitCode == "" || ProductID == "undefined") { swal("Error", "Please Select UnitCode!", "error"); }
        else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
        else if (Total == "" || Total == "undefined") { swal("Error", "Please enter Amount!", "error"); }
    }

//$("#TaxPercent").change(function () {
//    var SubTotal = parseFloat($("#subAmount").val()) || 0;
//    var TaxPercent = parseFloat($("#TaxPercent").val()) || 0;
//    var TaxAmt = parseFloat((TaxPercent / 100) * SubTotal).toFixed(2);

//    $("#TaxAmount").val(TaxAmt);
//    CalcTotal();
//});
function calculateEditiedTax() {
    var SubTotal = parseFloat($("#subAmount").val()) || 0;
    var TaxPercent = parseFloat($("#TaxPercent").val()) || 0;
    var TaxAmt = parseFloat((TaxPercent / 100) * SubTotal).toFixed(2);

    $("#TaxAmount").val(TaxAmt);
    CalcTotal();
}
$("#TaxPercent").change(function () {
    calculateEditiedTax();
});

$("#DiscountPercent").keyup(function () {
    var SubTotal = parseFloat($("#subAmount").val()) || 0;
    var DiscPercent = parseFloat($("#DiscountPercent").val()) || 0;
    var DiscAmt = parseFloat((DiscPercent / 100) * SubTotal).toFixed(2);

    $("#DiscountAmount").val(DiscAmt);
    CalcTotal();
});

function CalcTotal() {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('#SubTotal').text());
        total += parseFloat(subTotal);
    });

    $("#subAmount").val(parseFloat(total).toFixed(2));

    var commPercent = +$("#commissionPercent").val() || 0;

    if (commPercent > 0 && total > 0) {

        let commAm = +total * (+commPercent / 100);

        $("#commissionAmount").val(+commAm.toFixed(2));
    }
    else {
        $("#commissionAmount").val(0);
    }

    var TaxAmount = parseFloat($("#TaxAmount").val()) || 0;
    var DiscAmount = parseFloat($("#DiscountAmount").val()) || 0;

    var NetTotal = parseFloat(total + TaxAmount - DiscAmount).toFixed(2);
    $("#totalAmount").val(NetTotal);
    $("#amountPaid").val(NetTotal);
}





