﻿

function isValid() {

    var isValid = true;
    accID = $('#AccountID').val();
    inquiryID = $('#InquiryID').val();
    var PONo = $('#PONO').val();
    var SalesDate = $('#SaleDate').val();
    var paymentStatus = $('#PaymentStatus option:selected').val();
    var rowCount = $('#tblProduct tbody tr').length;
    var PaymentTypeID = $('#PaymentType option:selected').val();
    var BranchID = $('#BranchID option:selected').val();
    var amountPaid = $("#amountPaid").val();
    var balance = $("#balanceAmount").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    var bankAccountId = $('#Bank option:selected').val();
    var cashRec = parseFloat($('#cashReceived').val());
    var cashRet = parseFloat($('#cashReturned').val());
    var invoiceAmount = parseFloat($('#totalAmount').val());

    if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Customer", "Please Select Customer!", "error");
    }
    else if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Sales Date!", "error");
    }
    else if (rowCount == 0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");
    }
    else if (paymentStatus == 0) {
        isValid = false;
        swal("Payment Status", "Please Select Payment Status!", "error");
    }
    else if ((paymentStatus == 1 || paymentStatus == 2) && (PaymentTypeID == "" || PaymentTypeID == undefined)) {
        isValid = false;
        swal("Payment Type", "Please Select Payment Type!", "error");
    }
    else if ((paymentStatus == 2) && (amountPaid == "" || amountPaid == 0)) {
        isValid = false;
        swal("Amount Paid!", "Please Enter Amount!", "error");
    }
    else if ((PaymentTypeID == 3 || PaymentTypeID == 2) && (bankAccountId == "" || bankAccountId == undefined)) {
        isValid = false;
        swal("Bank!", "Please Select  Bank!", "error");
    }

    return isValid;

}



$('#btnSubmit').click(function () {
    var SaleDate = $('#SaleDate').val();

    
    if (isValid()) {

        if (SaleDate == null || SaleDate == '' || SaleDate == undefined) {
            swal("Sales Date", "Please Enter Date", "error");
        }
        else {
            $('#btnSubmit').prop('disabled', true);
            $('#btnHold').prop('disabled', true);
            uiBlock();
            insert();
        }
        
    }
});

var stockLog = [];
function GetFinalData() {

    var saleDetails = [];
    var BranchId = $('#BranchID option:selected').val();
    var SalePersonAccID = $('#SalePersonAccID').val() || null;
    var SalePersonCommissionAmount = $('#commissionAmount').val() || null;
    var hs_code = $("#hs_Code").val() || null;


    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = parseInt($row.find('#ProductID').text());
        var partNumber = $row.find("#PartNo").text();
        var UnitCode = $row.find("#UnitCode").text();
        var UnitPrice = parseFloat($row.find('#UnitPrice').text()) || 0;
        var Qty = parseFloat($row.find('#Qty').text());
        var SubTotal = (UnitPrice * Qty) || 0;//parseFloat($row.find("#SubTotal").text()) || 0;

        saleDetails.push({
            PartNo: partNumber,
            ProductId: pId,
            UnitPrice: UnitPrice,
            UnitCode: UnitCode,
            Qty: Qty,
            SalePrice: UnitPrice,
            Total: SubTotal,
            BranchID: BranchId
        });

        stockLog.push({
            AccountID: $('#AccountID').val(),
            ProductId: pId,
            StockOut: Qty,
            SalePrice: UnitPrice,
            CostPrice: 0,
            OutReference: 'Sales',
            OrderTypeID: 3,
            BranchID: BranchId,
            InvoiceDate: $('#SaleDate').val(),
            UnitCode: UnitCode
        });
    });

    if (saleDetails.length) {
        var Tax = +$("#TaxPercent").val() || 0;
        //var TaxPercent = null;
        //if (TaxID > 0) {
        //    TaxPercent = parseInt($("#TaxPercent option:selected").text());
        //}
        var data = {
            'OrderID': $('#hdnOrderID').val(),
            'AccountID': $('#AccountID').val(),
            'SOID': $("#SOID").val(),
            'BranchID': BranchId,
            'PaymentStatus': $('#PaymentStatus option:selected').text(),
            'SalesDate': $('#SaleDate').val(),
            'PaymentTypeID': $('#PaymentType option:selected').val(),
            'VAT': parseFloat($("#TaxAmount").val()) || 0,
            //'TAXID': +$("#TaxPercent").val || null,
            'TotalAmount': parseFloat($("#totalAmount").val()) || 0,
            'CashReceived': 0,
            'SalePersonAccID': SalePersonAccID,
            'SalePersonCommissionAmount': SalePersonCommissionAmount,
            'CashReturned': 0,
            'HSCode': hs_code,
            'PONo': $("#PONo").val(),
            'InvoiceNo': $("#InvoiceNo").val(),
            'Input': $("#STInvoiceNo").val(),
            'DiscountPercent': parseFloat($('#DiscountPercent').val()) || 0,
            'DiscountAmount': parseFloat($('#DiscountAmount').val()) || 0,
            'AmountPaid': parseFloat($('#amountPaid').val()) || 0,
            'TotalPaid': parseFloat($('#amountPaid').val()) || 0,
            'ChequeDate': $("#chqDate").val(),
            'ChequeNo': $("#chqNumber").val(),
            'BankName': $("#Bank option:selected").text(),
            'TaxPercent': Tax || 0,
            'TaxAmount': parseFloat($('#TaxAmount').val()) || 0,
            'COGS': 0,
            'ChallanID': $("#ChallanID").val(),
            'ChallanIDs': SDchallanIDs,
            'tbl_SaleDetails': saleDetails
        };

        return data;
    }

    return null;
}


function insert() {
    var data = GetFinalData();
    if (data != null) {
        var fsc_Cert = $("#FSC_Cert").is(":checked");
        var grs_Cert = $("#GRS_Cert").is(":checked");
        var oeko_tex_Cert = $("#OEKO_TEX_Cert").is(":checked");


        var json = JSON.stringify({
            'model': data,
            'modelStockLog': stockLog,
            'isQuote': false,
            'QuoteOrderID': 0,
            'bankAccId': $('#Bank option:selected').val(),
            'OrderTypeID': 0,
            'fsc_Cert': fsc_Cert,
            'grs_Cert': grs_Cert,
            'oeko_tex_Cert': oeko_tex_Cert

        });

        ajaxCall("POST", json, "application/json; charset=utf-8", "/Sales1/SaveEditedSaleInvoiceOrder", "json", onSuccess, onFailure);
    }
    else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("Check Entries", "Some error Ocurred! Please Check your Entries!", "error");
    }
}

function onSuccess(Result) {
    if (Result.OrderID > 0 && Result.InvoiceType > 0) {
        uiUnBlock();
        window.open('/Sales1/Export/' + Result.OrderID + '?InvoiceType=' + Result.InvoiceType + '&ReportType=PDF', 'Sales Invoice');
        window.location.href = '/Sales1';


    }
    else {

        uiUnBlock();
        if (Result == -100) {
            swal("Stock Error", "Some Products Are Out Of Stock Kindly Check Stock!", "error");
        }
        else {
            swal("Critical Error", "Something went Wrong! Please try Again!", "error");
        }
        $('#btnSubmit').prop('disabled', false);

    }
}

function onFailure(error) {
    if (error.statusText == "OK") {
        console.log("OK");
    } else {
        swal("Critical Error", "Something went Wrong! Please try Again!", "error");
    }
}

// Payment Type Change 
$("#PaymentType").on("change", function () {
    var PaymentTypeID = $('#PaymentType option:selected').val();

    if (PaymentTypeID == 1) {      //Cash
        $("#Bank").prop("disabled", true);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);
    }
    else if (PaymentTypeID == 2) {    // Bank
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);
    }

    else if (PaymentTypeID == 3) {   // Cheque
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", false);
        $("#chqDate").prop("disabled", false);
    }
    else if (PaymentTypeID == 5) {   // Credit Card        
        var invoiceAmount = parseFloat($('#totalAmount').val());
        $('#cashReceived').val(invoiceAmount);
    }
});


