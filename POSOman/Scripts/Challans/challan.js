﻿


$('#AccountID').select2();
$('#ddlVehCode').select2();
$('#ddlPartNumber').select2();
$('#InquiryID').select2();
$('#TaxPercent').select2();

//document.getElementById('chqDate').valueAsDate = new Date();
document.getElementById('SaleDate').valueAsDate = new Date();



function isValid() {

    var isValid = true;
    accID = $('#AccountID').val();
    inquiryID = $('#InquiryID').val();
    var PONo = $('#PONO').val();
    var SalesDate = $('#SaleDate').val();
    var paymentStatus = $('#PaymentStatus option:selected').val();
    var rowCount = $('#tblProduct tbody tr').length;
    var PaymentTypeID = $('#PaymentType option:selected').val();
    var BranchID = $('#BranchID option:selected').val();
    var amountPaid = $("#amountPaid").val();
    var balance = $("#balanceAmount").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    var bankAccountId = $('#Bank option:selected').val();
    var cashRec = parseFloat($('#cashReceived').val());
    var cashRet = parseFloat($('#cashReturned').val());
    var invoiceAmount = parseFloat($('#totalAmount').val());

    if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Customer", "Please Select Customer!", "error");
    }
    else if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Sales Date!", "error");
    }
    else if (rowCount == 0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");
    }
    else if (paymentStatus == 0) {
        isValid = false;
        swal("Payment Status", "Please Select Payment Status!", "error");
    }
    else if ((paymentStatus == 1 || paymentStatus == 2) && (PaymentTypeID == "" || PaymentTypeID == undefined)) {
        isValid = false;
        swal("Payment Type", "Please Select Payment Type!", "error");
    }
    else if ((paymentStatus == 2) && (amountPaid == "" || amountPaid == 0)) {
        isValid = false;
        swal("Amount Paid!", "Please Enter Amount!", "error");
    }
    else if ((PaymentTypeID == 3 || PaymentTypeID == 2) && (bankAccountId == "" || bankAccountId == undefined)) {
        isValid = false;
        swal("Bank!", "Please Select  Bank!", "error");
    }

    return isValid;

}


$('#btnSubmit').click(function () {

    if (isValid()) {
        $('#btnSubmit').prop('disabled', true);
        $('#btnHold').prop('disabled', true);
        uiBlock();
        insert();
    }
});

function GetFinalData() {

    var saleDetails = [];
    var BranchId = $('#BranchID option:selected').val();


    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = parseInt($row.find('#ProductID').text());
        var partNumber = $row.find("#PartNo").text();
        var UnitCode = $row.find("#UnitCode").text();
        var UnitPrice = parseFloat($row.find('#UnitPrice').text()) || 0;
        var SPrice = parseFloat($row.find('#SPrice').text()) || 0;
        var Qty = parseFloat($row.find('#Qty').text());
        var SubTotal = parseFloat($row.find("#SubTotal").text()) || 0;


        ///// Sale Details ////
        saleDetails.push({
            ProductID: pId,
            UnitCode: UnitCode,
            Qty: Qty,
            UnitPrice: UnitPrice,
            SalePrice: SPrice,
            Total: SubTotal,
            BranchID: BranchId,
        });

    });

    if (saleDetails.length) {

        var data = {
            'AccountID': $('#AccountID').val(),
            'SOID': $("#SOID").val(),
            'BranchID': BranchId,
            'PaymentStatus': "UnPaid",
            'SalesDate': $('#SaleDate').val(),
            'PaymentTypeID': 2,
            'TotalAmount': parseFloat($("#totalAmount").val()),
            'DiscountPercent': parseFloat($('#DiscountPercent').val()) || 0,
            'DiscountAmount': parseFloat($('#DiscountAmount').val()) || 0,
            'AmountPaid': $('#amountPaid').val() || 0,
            'TotalPaid': $('#amountPaid').val() || 0,
            'SaleOrderID': $("#SaleOrderID").val(),
            'TaxPercent': $("#TaxPercent").val(),
            'TaxAmount': $("#TaxAmount").val(),
            'tbl_ChallanDetails': saleDetails
        };

        return data;
    }

    return null;

}


// Add data to array and send it to controller for order creation
function insert() {

    var data = GetFinalData();
    if (data != null) {
        var json = JSON.stringify({
            'model': data,
        });

        ajaxCall("POST", json, "application/json; charset=utf-8", "/Challan/SaveOrder", "json", onSuccess, onFailure);
    }
    else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("Check Entries", "Some error Ocurred! Please Check your Entries!", "error");
    }
}


function onSuccess(Result) {
    if (Result == 200) {
        uiUnBlock();
        window.location.href = '/Challan';
    }
    else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("Critical Error", "Something went Wrong! Please try Again!", "error");
    }
}

function onFailure(error) {
    if (error.statusText == "OK") {
        console.log("OK");
    } else {
        swal("Critical Error", "Something went Wrong! Please try Again!", "error");
    }
}

