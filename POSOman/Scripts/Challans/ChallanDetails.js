﻿

$("#btnSearch").click(function () {
    if ($("#SaleOrderID").val() != "") {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/Sales/SaleOrderDetail?id=' + $("#SaleOrderID").val(),
            async: true,
            success: function (data) {
                console.log(data)
                $("#AccountID").val(data[0].AccountID).change();

                $.each(data, function (index, value) {
                    var CategoryID = value.CategoryID;
                    var CategoryName = value.CategoryName;
                    var ProductID = value.ProductID;
                    var ProductName = value.ProductName;
                    var UnitCode = value.UnitCode;
                    var Qty = parseFloat(value.Qty) || 0;
                    var UnitPrice = parseFloat(value.UnitPrice) || 0;
                    var SubTotal = parseFloat(value.Total) || 0;


                    GenerateProductTable(CategoryID, CategoryName, ProductID, ProductName, UnitCode, Qty, UnitPrice, SubTotal);
                })
            },
            error: function (err) { console.log(err); }
        });
    }
    else {
        swal("Quotation", "Please Select Quotation!", "error")
    }
});



$("#TaxPercent").change(function () {
    var SubTotal = parseFloat($("#subAmount").val()) || 0;
    var TaxPercent = parseFloat($("#TaxPercent").val()) || 0;
    var TaxAmt = parseFloat((TaxPercent / 100) * SubTotal).toFixed(2);

    $("#TaxAmount").val(TaxAmt);
    CalcTotal();
});

$("#DiscountPercent").keyup(function () {
    var SubTotal = parseFloat($("#subAmount").val()) || 0;
    var DiscPercent = parseFloat($("#DiscountPercent").val()) || 0;
    var DiscAmt = parseFloat((DiscPercent / 100) * SubTotal).toFixed(2);

    $("#DiscountAmount").val(DiscAmt);
    CalcTotal();
});

function CalcTotal() {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('#SubTotal').text());
        total += parseFloat(subTotal);
    });

    $("#subAmount").val(parseFloat(total).toFixed(2));
    var TaxAmount = parseFloat($("#TaxAmount").val()) || 0;
    var DiscAmount = parseFloat($("#DiscountAmount").val()) || 0;

    var NetTotal = parseFloat(total + TaxAmount - DiscAmount).toFixed(2);
    $("#totalAmount").val(NetTotal);
}





