﻿var customerID = 0;
var accID = 0;
var isPONOExist = false;
var isQuote = false;
var SOID = 0;
var DatE = null;

var isLimitExceed = false;
getHoldID();
getSOID();
$("#Bank").prop("disabled", true);
//  Get New SOID 
function ToJSDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
}
// Calculate Discount Amount from Discount Percentage 
function calcDiscount() {
    var disc = parseFloat($('#customerDiscount').val());
    var subTotal = $("#subAmount").val();
    if (disc == "" || isNaN(disc)) { applyDiscount(); }
    else if (parseFloat(subTotal) > 0) {
        var discAmount = parseFloat((disc * subTotal) / 100);
        $('#discInput').val(parseFloat(discAmount).toFixed(2));
        getNewTotal();
    }
}
function getSOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/getNewSOID',
        async: true,
        success: function (data) {
            {
                $("#hdnSOID").val(data);
                SOID = data;
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function getHoldID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Quotation/getLastHoldID',
        async: true,
        success: function (data) {
            {
                var value = data;
                var newValue = value.split('-');
                var increase = newValue[1];
                increase++;
                value = "H-" + increase;
                $("#hdnHoldID").val(value);
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}
$("#PONO").on("change", function () {
    var accID = $('#hdnAccountID').val();
    if (accID == "" || accID == 0) {
        swal("Customer", "Please Select Customer First!", "error");
    } else {
        var PONO = $("#PONO").val();
        checkPONO(PONO, accID);

    }
});

function checkPONO(PONO, accID) {

    var json = {
        "accountID": accID,
        "PONO": PONO
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/checkPONO',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            if (data == true) {
                toastr.warning('PONo already Exists for selected Customer!')
                $("#PONO").val("");
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

$('#getQuotation').click(function () {
    var qouteID = $('#QID').val();
    var orderID = 0;
    if (qouteID != "") {
        var json = {
            "QuoteID": qouteID
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Quotation/getQuotationOrderID',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                if (data > 0) {
                    orderID = data;
                    //console.log(orderID);
                    window.location.href = '/Quotation/SubmitOrders?OrderID=' + orderID;
                } else {
                    swal("Submitted", "Quotation Already Submitted!", "error");
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    } else {
        swal("QuotationID", "Enter QuotationID!", "error");
    }


});


//$('#btnSubmit').click(function () {

//   // if (isValid()) {
//        $('#btnSubmit').prop('disabled', true);
//      //  $('#btnHold').prop('disabled', true);
//        uiBlock();
//        insert();
//   // }
//});
// Payment Status DropDOwn, Action according to Payment Status Selection
$("#PaymentStatus").on("change", function () {
    var payStatusSelection = $("#PaymentStatus").val();
    var totalAmount = $("#finalAmountWithVAT").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    //Paid
    if (payStatusSelection == 1) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        getNewTotal();
        //document.getElementById("amountPaid").value = totalAmount;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("balanceAmount").value = 0;
        //document.getElementById("expenseInput").readOnly = false;
        //document.getElementById("vatInput").readOnly = false;
    }
    // Partial PAid
    else if (payStatusSelection == 2) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = false;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
    }
    //Unpaid
    else if ((payStatusSelection == 3) && (parseFloat(totalAmount) > parseFloat(creditLimit - customerBalance))) { // Remove Nested if Condition 
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        $("#PaymentType").prop("disabled", true);
        disableBank();
        swal("Credit Limit", "Credit Limit Exceeds! ", "error");
    }
    //else if ((payStatusSelection == 3) && (parseFloat(totalAmount) <= parseFloat(creditLimit))) {                      // Remove Nested if Condition 

    //    isLimitExceed = false;//isLimitExceed = true;
    //    document.getElementById("amountPaid").readOnly = true;
    //    document.getElementById("amountPaid").value = 0;
    //    document.getElementById("balanceAmount").value = totalAmount;
    //    $("#PaymentType").prop("disabled", true);
    //    disableBank();
    //}
    else if (payStatusSelection == 3 && (creditLimit == "")) {
        isLimitExceed = false;
        swal("Credit Limit", "Credit Limit not available! ", "error");
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        $("#PaymentType").prop("disabled", true);
        disableBank();
    } else if (payStatusSelection == 3) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        document.getElementById("PaymentType").selectedIndex = "0";
        $("#PaymentType").prop("disabled", true);
        disableBank();
    } else {
        isLimitExceed = false;

    }

});

// Check credit limit when Unpaid Selected 
function checkCreditLimit() {
    var creditLimit = $('#customerCrLimit').val();
    var balance = $('#customerBalance').val();
    if ((parseFloat(totalAmount) > parseFloat(creditLimit))) {
        isLimitExceed = false;
        swal("Credit Limit", "Credit Limit not available! ", "error");
    } else {
        isLimitExceed = false;
    }
}

function disableBank() {
    $("#Bank").prop("disabled", true);
    $("#chqNumber").prop("disabled", true);
    $("#chqDate").prop("disabled", true);
}
// Payment Type Change 
$("#PaymentType").on("change", function () {
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //Cash
    if (PaymentTypeID == 1) {
        //$("#bankName").prop("disabled", true);
        $("#Bank").prop("disabled", true);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);
    }
    // Bank
    else if (PaymentTypeID == 2) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);

    }
    // Cheque
    else if (PaymentTypeID == 3) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", false);
        $("#chqDate").prop("disabled", false);
    }
});

function getNewTotal() {
    applyDiscount();
    calcVAT();
    calcAmountPaid();
    var subTotal = $("#subAmount").val();
    var VAT = $('#vatAmount').val();
    var discount = $('#discInput').val();
    if (VAT == "" || isNaN(VAT)) {
        VAT = 0;
        applyZeroVAT();
    }
    if (discount == "" || isNaN(discount)) {
        discount = 0;
        applyDiscount();
    }
    var newTotal = parseFloat(subTotal) + parseFloat(VAT) - parseFloat(discount);
    // var amountPaid = parseFloat(subTotal) + parseFloat(VAT);
    $('#finalAmountWithVAT').val(parseFloat(newTotal).toFixed(3));
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(newTotal).toFixed(3);
    }
}

function applyZeroVAT() {
    //var vat = parseFloat($('#vatInput').val());
    var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        //$('#vatInput').val(0);
        vat = 0;
        $('#vatAmount').val(0);
        $('#finalAmountWithVAT').val($("#totalAmount").val());
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat($("#totalAmount").val()).toFixed(3);
        }
    }
}


function applyDiscount() {
    var discount = $('#discInput').val();
    var subTotal = $("#subAmount").val();
    if (discount == "" || isNaN(discount)) {
        //$('#discInput').val(0);
        discount = 0;
        $('#totalAmount').val(subTotal);
    } else if (parseFloat(discount) >= parseFloat(subTotal)) {
        // toastr.warning('Discount must less than total!')
        $('#discInput').val(0);
        $('#totalAmount').val(subTotal);
    } else {
        var totalAmount = parseFloat(subTotal) - parseFloat(discount);
        $('#totalAmount').val(parseFloat(totalAmount).toFixed(3));
    }
}
$('#vatAmount').on('input', function (e) {
    getNewTotal();
});
// Get Customer Balance and Credit Limit 
function getCustomerDetail(accountID) {
    if (accountID == "") {
        accountID = -1;
    }
    var json = {
        "accountID": accountID
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Customer/getDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#customerCrLimit').val(data.creditLimit);
            $('#customerBalance').val(data.Balance);
            $('#customerDiscount').val(data.Discount);
        },
        error: function (err) {
            console.log(err);
        }
    });
}
// calculate balance if amountPaid entered and make changes every where needed
function calcAmountPaid() {
    isLimitExceed = false;
    var amountPaid = $("#amountPaid").val();
    var finalAmount = $('#finalAmountWithVAT').val();
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 2) {
        if (parseFloat(amountPaid) >= parseFloat(finalAmount)) {
            toastr.warning('Partial ! AmountPaid should be less than total amount ')
            $("#amountPaid").val(0)
        } else {
            var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
            $("#balanceAmount").val(parseFloat(balance).toFixed(3));
        }
    } else {

        var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
        $("#balanceAmount").val(parseFloat(balance).toFixed(3));
    }
}
// in case of vat entered please do calculations
function calcVAT() {
    //var vat = parseFloat($('#vatInput').val());
    var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        applyZeroVAT();
    } else {
        var totalInvoice = $("#totalAmount").val();
        //var vatAmount = parseFloat((vat * totalInvoice) / 100);
        var vatAmount = parseFloat(vat);
        // $('#vatAmount').val(parseFloat(vatAmount).toFixed(3));
        var finalAmount = parseFloat(vatAmount) + parseFloat(totalInvoice);
        $('#finalAmountWithVAT').val(finalAmount);
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat(finalAmount).toFixed(3);
        }
    }
}
// if Vat then calculate and make effects on total amount
$('#vatInput').on('input', function (e) {
    getNewTotal();
});
$('#vatAmount').on('input', function (e) {
    getNewTotal();
});
// if Discount then calculate and make effects on total amount
$('#discInput').on('input', function (e) {
    getNewTotal();
});
$("#cashReceived").on("change", function () {
    cash();
});
$("#amountPaid").on("change", function () {
    calcAmountPaid();
});
function cash() {
    var cashRec = parseFloat($('#cashReceived').val());
    var invoiceAmount = parseFloat($('#totalAmount').val());
    if (cashRec >= invoiceAmount) {
        var retAmount = parseFloat(cashRec) - parseFloat(invoiceAmount);
        $('#cashReturned').val(retAmount);
    }
    else {
        swal("Short", "Recd cash must greater than total!", "error");
    }
}

$('#btnSubmit').click(function () {

    //if (isValid()) {
        $('#btnSubmit').prop('disabled', true);
        $('#btnHold').prop('disabled', true);
        uiBlock();
        insert();
    //}
});
var stockLog = [];
function GetFinalData() {

    var detail = [];
    var model ;
    var OrderID = +$('#OrderID').val() || 0;
    var Date = $('#SalesDate').val() || null;
    var ItemID = +$('#ItemID').val() || 0;
    var DepartmentID = +$('#DepartmentID').val() || null;
    var OrderMachineID = +$('#OrderMachineID').val() || null;
    var note = $('#description').val() || null;
    
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = parseInt($row.find('#ProductID').text());

        var Qty = parseFloat($row.find('#Qty').text()).toFixed(2) || 0;
      
        detail.push({
            ProductID: pId,
            CurrentIssuedQty: Qty
        });

    });

    if (detail.length) {

        model = {
            'ItemID': ItemID,
            'AddedOn':Date,
            'OrderID': OrderID,
            'DepartmentID': DepartmentID,
            'OrderMachineID': OrderMachineID,
            'Note': note,
            'tbl_StockIssuanceDetails': detail
        };


        return model;
    }

    return null;
}


function insert() {

    var data = GetFinalData();
    if (data != null) {
        var json = JSON.stringify({
            'model': data,
           
        });

        ajaxCall("POST", json, "application/json; charset=utf-8", "/StockIssuance/Save", "json", onSuccess, onFailure);
    }
    else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("Check Entries", "Some error Ocurred! Please Check your Entries!", "error");
    }
}


function onSuccess(Result) {
    if (Result >0) {
        uiUnBlock();
        window.location.href = '/StockIssuance';
    }
    else if (Result == -2) {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("Duplicate Record", "Item With Same Order Already Exist!", "error");
    }
    else if (Result == -3) {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("", "Stock not exist!", "error");
    }
    else if (Result == -4) {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("", "Please select department!", "error");
    }
    else if (Result == -5) {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("", "Please select Date!", "error");
    }
    else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("Critical Error", "Something went Wrong! Please try Again!", "error");
    }
}

function onFailure(error) {
    uiUnBlock();
    if (error.statusText == "OK") {
        console.log("OK");
    } else {
        swal("Critical Error", "Something went Wrong! Please try Again!", "error");
    }
}

