﻿
$tableItemCounter = 0;
$addedProductIDs = [];
$selCustID = 0;
var QtyinPack = 0;
var vc = '';
var PrUntLnth = 0;
var Count = 0;
var OrderId = $('#InvOrderId').val();
var deptID = $('#hdnDeptID').val();
//var index = null;
var proIdEdit = 0;
//console.log(OrderId + "OID");
$("#ddlVehCode").prop('disabled', true);
//$("#ddlPartNumber").prop('disabled', true);
$("#ddlproductname").prop('disabled', true);
$("#addRow").prop('disabled', true);
$("#BranchID").prop('disabled', true);
$("#btnRemove").prop('disabled', false);

$("#ItemID").prop('disabled', true);

$("#DepartmentID").change(function () {
    let deptID = +$("#DepartmentID").val() || 0;

    var json = { "DeptID": deptID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Sales/GetDeptartmentWiseMachineList',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            // console.log(data)
            if (data.length > 0) {

                GetDropdown1('OrderMachineID', data, true);
            }
        },
        error: function (err) { console.log(err); }
    });

});



//$("#DepartmentID").change(() => {

//    let DepID = +$("#DepartmentID").val() || 0;
//    if (DepID > 0) {

       
//        $("#OrderID").prop('disabled', false);

        
        
//    }
//    else {

      
//        $("#OrderID").prop('disabled', true);
//        $("#ItemID").prop('disabled', true);

//        $('#OrderID').val(null).trigger('change.select2');
//        $('#ItemID').val(null).trigger('change.select2');
//    }

//});

$("#OrderID").change(() => {
    $('#DepartmentID').val(null).trigger('change.select2');
    let OrderID = +$("#OrderID").val() || 0;
    if (OrderID > 0) {
        $("#ItemID").prop('disabled', false);
        $("#DepartmentID").prop('disabled', true);
        
        var json = { "OrderID": OrderID };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/StockIssuance/getItemsOrderIDWise',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                GetDropdown1("ItemID", data, true);
            },
            error: function (err) { console.log(err); }
        });

    }
    else {

        $('#tblProduct tbody td').remove();
        $("#ItemID").prop('disabled', true);
        $("#DepartmentID").prop('disabled', false);
        $('#ItemID').val(null).trigger('change.select2');
    }

});

$("#ItemID").change(() => {
    $('#DepartmentID').val(null).trigger('change.select2');
    let itemID = +$("#ItemID").val() || 0;
    if (itemID <= 0) {
        $('#tblProduct tbody td').remove();
    }

    else if (itemID > 0) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/StockIssuance/getDepartmentIDItemsIDWise?ItemID='+itemID,
            async: true,
            //data: JSON.stringify(json),
            success: function (data) {
                $('#DepartmentID').val(+data).trigger('change.select2');
            },
            error: function (err) { console.log(err); }
        });
    }
})

//$("#btnSearch").click(() => {

//    let ItemID = +$("#ItemID").val() || 0;
//    let OrderID = +$("#OrderID").val() || 0;
//    let DepartmentID = +$("#DepartmentID").val() || 0;

//    if (OrderID <= 0) {
//        swal("", "Please Select Job Order", "error");
//    }
//    //else if (DepartmentID <= 0) {
//    //    swal("", "Please Select Department", "error");
//    //}
//    else if (ItemID <= 0) {
//        swal("", "Please Select Item", "error");
//    }
//    else {
//        if (OrderID > 0 && ItemID > 0 ) {

//            var json = { "OrderID": OrderID, "ItemID": ItemID};
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: '/StockIssuance/getRawMaterialItemIDWise',
//                async: true,
//                data: JSON.stringify(json),
//                success: function (data) {
//                    console.log(data);
//                    $('#tblProduct tbody td').remove();
//                    data.forEach(ListProduct);
//                },
//                error: function (err) { console.log(err); }
//            });

//        }

//    }

//});


//$("#DepartmentID").change(() => {
//    let DeptID = +$("#DepartmentID").val() || 0;

//    if (DeptID > 0) {
//      $.ajax({
//                type: "GET",
//                contentType: "application/json; charset=utf-8",
//                url: '/StockIssuance/getRawMaterialDepartmentIDWise?DepartmentID=' + DeptID,
//                async: true,
//                success: function (data) {
//                    console.log(data);
//                    $("#ddlPartNumber").prop('disabled', false);
//                    GetDropdown1("ddlPartNumber", data, true);
                 
//                },
//                error: function (err) { console.log(err); }
//            });
//    }
//})

$("#tblProduct").focusout(() => {
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = parseInt($row.find('#ProductID').text());
        var OrderID = parseInt($row.find('#OrderID').text()) || 0;
        var Qty = parseInt($row.find('#Qty').text()) || 0;
        var StkQty = parseInt($row.find('#StkQty').text()) || 0;

        //if (Qty > StkQty) {
        //    toastr.warning("Qty must be less than/equal to stk qty");
        //    $row.find('#Qty').text(0)
        //}


    });
})


function GetMachineImpressionForDD(id, Pid) {
    if (+id > 0) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/OrderMachine/getMachineDetails?OrderMachineID=' + id + '&ProdID=' + Pid,
            async: true,
            // data: JSON.stringify(json),
            success: function (data) {
                console.log(data)
                $('#tblProduct tbody tr').each(function (i, n) {
                    var $row = $(n);
                    var productID = +$row.find('#ProductID').text() || 0;

                    if (productID === Pid) {
                        $row.find('#impression').text(data.Impressions);
                    }
                });


            },
            error: function (err) { console.log(err); }
        });
    }

}
//function for edit product list
////////////
function ListProduct(item) {
    var Qty = 0;
   
    var ProductID = +item.ProductID;
    var OrderID = +item.OrderID;
    var DepartmentID = +item.DepartmentID;
    var ProductName = item.Product;
    var Department = item.Department;
    var StkQty = +item.StockQty || 0;
    var TotalIssuedQty = +item.TotalIssuedQty||0;
    

    var markup = "<tr>\
                        <td id='ProductID' hidden>" + ProductID + "</td>\
                        <td id='OrderID' hidden>" + OrderID + "</td>\
                        <td id='DepartmentID' hidden>" + DepartmentID + "</td>\
                        <td id='Department'>" + Department + "</td>\
                        <td id='ProductName'>" + ProductName + "</td>\
                        <td id='StkQty' contenteditable='false'>" + StkQty + "</td>\
                        <td id='Qty' contenteditable='true'>" + Qty + "</td>\
                        <td id='TotalIssuedQty' contenteditable='false'>" + TotalIssuedQty + "</td>\
                        </tr>";

    $("#tblProduct tbody").append(markup);
    $tableItemCounter++;
    $addedProductIDs.push(item.ProductID);
   // proIdEdit = item.ProductID;
    //getNewTotal();
    //  CalcTotal();
}


////////////
//get other edit details

$('#Qty').on('input', function (e) {
    var qty = $('#Qty').val();
    var unitPerCarton = $('#unitPerCarton').val();
    calcSubTotal();
    calcTotalWeight();
});
$('#BOX').on('input', function (e) {
    calcSubTotal();
    calcTotalWeight();
});
$('#SalePrice').on('input', function (e) {
    calcSubTotal();
});
$('#SalePrice').change(function () {
    var price = parseFloat($('#SalePrice').val()) || 0;
    var costPrice = parseFloat($('#hdnCostPrice').val()) || 0;
    if (price < costPrice) {
        swal("Error", "The sale price is below cost price of this item! " + costPrice + "PKR", "error");
        // $("#SalePrice").val('');
    }
});
function calcSubTotal() {
    //console.log("unitPerCarton=" + unitPerCarton);
    var ctnQty = parseFloat($('#Qty').val() || 0);
    var ctnPrice = parseFloat($('#SalePrice').val());
    var qty = ctnQty;
    var price = ctnPrice;
    if (($.isNumeric(price)) && ($.isNumeric(qty))) {
        var amount = parseFloat(qty * price);
        $('#SubTotal').val(parseFloat(amount).toFixed(2));
        calcTotal();
    }
    else {
        $('#subAmount').val(parseFloat(amount).toFixed(2));
    }
}

function calcTotalWeight() {
    //console.log("Total Wright ");
    var unitPerCarton = parseInt($('#unitPerCarton').val());
    var ctnQty = parseInt($('#Qty').val() || 0);
    var boxQty = parseInt($('#BOX').val() || 0);
    var totalQty = parseInt((ctnQty * unitPerCarton));
    var isMinor = $('#isMinor').val();
    var isPack = $('#UOMID').val();
    var levelID = $('#LevelID').val();
    var qty = totalQty;

    if (isPack == 0) {
        qty = totalQty * 1000;
    }
    else if (isPack == 1) {
        qty = boxQty * 1000;
    }
    else if (isPack == 2) {
        qty = boxQty;
    }
    else {
        qty = totalQty;
    }
    $('#totalWeight').val(parseInt(qty));
}
$("#UOMID").change(function () {
    var isPack = $('#UOMID').val();
    var isMinor = $('#IsMinor').val();
    var unitPerCarton = parseInt($('#unitPerCarton').val());

    if (isPack == 0) {
        //var price = parseFloat(salePrice / unitPerCarton);
        //$('#SalePrice').val(price);
        document.getElementById("Qty").readOnly = false;
        document.getElementById("BOX").readOnly = true;
    }
    else if (isPack == 1 || isPack == 2) {// && isMinor == 0)  { // open 
        //var price = parseFloat(salePrice / unitPerCarton / 1000);
        //$('#SalePrice').val(price);
        document.getElementById("BOX").readOnly = false;
        document.getElementById("Qty").readOnly = true;
    }
    var unitPerCarton = parseInt($('#unitPerCarton').val());
    var ctnPrice = parseFloat($('#hdnSalePrice').val());
    // console.log('ctnPrice=' + ctnPrice);
    var boxPrice = parseFloat(ctnPrice / unitPerCarton);
    var levelID = $('#LevelID').val();
    if (isPack == 0) {
        boxPrice = parseFloat(ctnPrice * unitPerCarton * 1000);

    }
    else if (isPack == 1) {
        boxPrice = parseFloat(ctnPrice * 1000);
    }
    else if (isPack == 2) {
        boxPrice = parseFloat(ctnPrice);
    }
    var price = boxPrice;
    $('#SalePrice').val(price)
});

////////////////////////
$("#BranchID").change(function () {
    // console.log('on branch change');
    var branchids = $('#BranchID option:selected').val();
    // console.log("branch id" + branchids);
    if (branchids == null || branchids == 'undefiend' || branchids == 0) {
        $("#ddlVehCode").prop('disabled', true);
        $("#ddlPartNumber").prop('disabled', true);
    }
    else {

        $("#ddlVehCode").prop('disabled', false);
        $("#ddlPartNumber").prop('disabled', false);
    }
});

$("#AccountID").change(function () {
    var customerID = $('#AccountID').val();
    $('#hdnAccountID').val(customerID);

    getCustomerDetail(customerID);
});
$("#CustomerCode").change(function () {
    var customerID = $('#CustomerCode').val();
    $('#hdnAccountID').val(customerID);
    $('#AccountID').val(customerID).trigger('change.select2');
    $('#CustomerPhone').val(customerID).trigger('change.select2');
    getCustomerDetail(customerID);
});
$("#CustomerPhone").change(function () {
    var customerID = $('#CustomerPhone').val();
    $('#hdnAccountID').val(customerID);
    $('#AccountID').val(customerID).trigger('change.select2');
    $('#CustomerCode').val(customerID).trigger('change.select2');
    getCustomerDetail(customerID);
});
$("#ddlPartNumber").change(function () {
    var pId = $('#ddlPartNumber').val();
    $("#addRow").prop('disabled', false);
    if (pId != "" || pId > 0) { getDetail(pId); }
});
var validate = function (e) {
    var t = e.value;
    e.value = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 4)) : t;
}




function calcTotal() {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('#ProductSubTotal').text());
        total += parseFloat(subTotal);
    });
    $("#subAmount").val(parseFloat(total).toFixed(2));
    $("#totalAmount").val(parseFloat(total).toFixed(2));
    $("#finalAmountWithVAT").val(parseFloat(total).toFixed(2));
    calcDiscount();
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(total).toFixed(2);
    }
    //var customerBalance = $('#customerBalance').val();
    var creditLimit = parseFloat($('#customerCrLimit').val()) - parseFloat($('#customerBalance').val());
    if (creditLimit != "" && total > creditLimit) { swal("Credit Limit", "You can not sale more than Credit Limit! ", "error"); }

}

$("#addRow").click(function () {
    var ProductID = +$("#ddlPartNumber").val() || 0; // hidden
    var ProductName = $("#ddlPartNumber :selected").text();  // 1
    var Description = $("#Description").text(); // 2
    var UnitCode = $("#UnitCode").text();
    var Qty = +$("#Qty").val()||0;

    if (ProductID > 0 && Qty > 0) {
        var index = $.inArray(+ProductID, $addedProductIDs);
        if (index >= 0) {
            swal("Error", "Product Already Added!", "error");
        } else {
           
            var markup = "<tr>\
                <td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deleteRow(this)'/></td>\
                        <td id='ProductID' hidden>" + ProductID + "</td>\
                        <td id='ProductName'>" + ProductName + "</td>\
                        <td id='Qty' contenteditable='true'>" + Qty + "</td>\
                        </tr>";

            $("#tblProduct tbody").append(markup);
            $tableItemCounter++;
            $addedProductIDs.push(+ProductID);
            clearFields();
           
        }
    }
    else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
    //else if (parseInt(Qty) > stock) { swal("Error", "Stock Not Available!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
});


function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tblProduct").deleteRow(i);
    var row = $(r).closest("tr");
    var productID = row.find('#ProductID').text(); // find hidden id 
    var index = $.inArray(+productID, $addedProductIDs);
    if (index >= 0) { $addedProductIDs.splice(index, 1); }
    
}

function clearFields() {
    $("#isPack").val("");
    $("#Description").text("");
    $("#productUnitCode").val("");
    $("#Packet").val("");
    $("#StkQty").val("");
    $("#Qty").val("");
    $("#SalePrice").val("");
    $("#productDepartment").val("");
   // $("#ddlVehCode").focus();
    $('#ddlVehCode').val(null);
    $('#ddlPartNumber').val('').trigger('change');
}
function GetEditProductDetail(OrderId, ProductID, BranchID) {
    var json = { "OrderId": OrderId, "ProductID": ProductID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/GetEditProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#ddlPartNumber').val(data[0].ProductsList.ProductID).trigger('change.select2');
            $('#ddlVehCode').val(data[0].ProductsList.CatID).trigger('change.select2');
            $('#SubTotal').val(data[0].ProductsList.PTotal);

            var qtyBox = parseFloat(data[0].ProductsList.Qty);
            var unitPerCTN = 1;
            var qty = 0;
            var UnitCode = data[0].ProductsList.UnitCode;
            var SalePrice = data[0].ProductsList.SalePrice;
            var SubTotal = data[0].ProductsList.PTotal;
            var unitID = data[0].ProductsList.UnitID;
            var levelID = data[0].ProductsList.LevelID;
            var Box = 0;
            Qty = qtyBox;
            $('#Qty').val(Qty);
            if (data[0].ProductsList.IsPack == true) {
                $('#Packet').val(data[0].ProductsList.Packet);
                $("#Packet").prop('disabled', false);
            }
            else {
                $('#Packet').val();
                $("#Packet").prop('disabled', true);
            }
            $('#SalePrice').val(SalePrice);
            $('#UnitCode').val(UnitCode);

            $('#unitPerCarton').val(data[0].ProductsList.UnitPerCarton);
            var VatInput = data
            getDetail(data[0].ProductsList.ProductID);

            $("#addRow").prop('disabled', false);
            $("#btnRemove").prop('disabled', true);
        },
        error: function (err) { console.log(err); }
    });
}

function EditProduct(input) {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {

            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            var productID = row.find('input[id*="productID"]').val(); // find hidden id 
            //console.log(productID + "IN Edit Function");

            var index = $.inArray(productID, $addedProductIDs);
            if (index >= 0) { $addedProductIDs.splice(index, 1); }
            var BranchID = "";
            GetEditProductDetail(OrderId, productID, BranchID);

            //  calcTotal();
        }
    });
}
// Remove Selected Products 
function remove(input) {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {
            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            // calcTotal();
            var productID = row.find('input[id*="productID"]').val(); // find hidden id 
            var index = $.inArray(productID, $addedProductIDs);
            if (index >= 0) { $addedProductIDs.splice(index, 1); }
            Count = $tableItemCounter;
        }
    });
}
// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function clearLabels() {
    $('#DescriptionTitle').text(''); $('#Description').text('');
    $('#SubsTitle').text(''); $('#SubstituteNo').text('');
    $('#GroupTitle').text(''); $('#Group').text('');
    $('#VehModelTitle').text(''); $('#VehModel').text('');
    $('#LocationTitle').text(''); $('#Location').text('');
    $('#UnitCodeTitle').text(''); $('#UnitCode').text('');
    $('#MinorUnitCodeTitle').text(''); $('#MinorUnitCode').text('');
    $('#LeastUnitCodeTitle').text(''); $('#LeastUnitCode').text('');
    $('#QtyPerUnitTitle').text(''); $('#QtyPerUnit').text('');
    $('#UnitPerCartonTitle').text(''); $('#UnitPerCarton').text('');
}

function getDetail(pId) {
    clearLabels();
    var accID = $('#hdnAccountID').val();
    // get product description
    var json = { "productId": pId };
    //ajaxCall("GET", json, "application/json; charset=utf-8", "/Purchase/getProductDetail", "json", onSuccess, onFailure);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            // console.log('data=' + data[0].qry.LeastUnitCode);
            // console.log('C.p=' + data[0].costPrice);
            var unitPerCarton = 1;
            var level = 0;
            if (data[0].qry.Description) { $('#DescriptionTitle').text("Desc: "); $('#Description').text(data[0].qry.Description); }
            if (data[0].qry.SubstituteNo) { $('#SubsTitle').text("Su No.: "); $('#SubstituteNo').text(data[0].qry.SubstituteNo); }
            if (data[0].qry.GroupName) { $('#GroupTitle').text("Group: "); $('#Group').text(data[0].qry.GroupName); }
            if (data[0].qry.VehicleName) { $('#VehModelTitle').text("Model: "); $('#VehModel').text(data[0].qry.VehicleName); }
            if (data[0].qry.Location) { $('#LocationTitle').text("Location: "); $('#Location').text(data[0].qry.Location); }
            if (data[0].qry.UnitCode) { $('#UnitCodeTitle').text("Pack Unit: "); $('#UnitCode').text(data[0].qry.UnitCode); }
            if (data[0].qry.OpenUnitCode) { $('#MinorUnitCodeTitle').text("Minor Unit: "); $('#MinorUnitCode').text(data[0].qry.OpenUnitCode); }
            if (data[0].qry.LeastUnitCode) { $('#LeastUnitCodeTitle').text("Least Unit: "); $('#LeastUnitCode').text(data[0].qry.LeastUnitCode); }

            $("#productUnitCode").val(data[0].qry.UnitCode)
            $("#productDepartment").val(data[0].qry.Department)
            if (data[0].qry.VehicleCode) {
                var vc = data.VehicleCode;
                $("#ddlVehCode option:contains(" + data[0].qry.VehicleCode + ")").attr("selected", true);
            }
            if (data[0].qry.UnitPerCtn) {
                unitPerCarton = data[0].qry.UnitPerCtn;
                $('#unitPerCarton').val(unitPerCarton);
                $('#UnitPerCtnTitle').text("Unit/Ctn: "); $('#UnitPerCtn').text(unitPerCarton);
            }
            if (data[0].qry.QtyPerUnit) {
                $('#QtyPerUnitTitle').text("Qty/Unit: "); $('#QtyPerUnit').text(data[0].qry.QtyPerUnit);
                $('#qtyPerUnit').val(data[0].qry.QtyPerUnit);
            }
            if (data[0].qry.IsMinor) {
                $('#isMinor').val(data[0].qry.IsMinor);
            }
            else {
                $('#isMinor').val('false');
            }
            if (data[0].qry.LevelID) {
                level = data[0].qry.LevelID;
                $('#LevelID').val(level);
            }
            if (data[0].qry.IsPacket == "1") {
                $('#isPacket').val(data[0].qry.IsPacket);
               // document.getElementById("Packet").readOnly = false;
            }
            else if (data[0].qry.IsPacket == "0") {
                //document.getElementById("Packet").readOnly = true;
                $('#isPacket').val('false');
            }
            if (data[0].qry.UnitCode) {

                $('#unitCode').val(data[0].qry.UnitCode);
            }

            var isPack = $('#isPacket').val();
            var qty = data[0].stock;
            $('#StkQty').val(qty);

            $('#hdnCostPrice').val(data[0].costPrice);
            var salePrice = data[0].SalePrice;
            $('#hdnSalePrice').val(salePrice);
            $('#SalePrice').val(salePrice);

        },
        error: function (err) { console.log(err); }
    });
}
