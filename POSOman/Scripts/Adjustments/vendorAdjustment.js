﻿$(function () {
    
    $("#Bank").prop("disabled", true);
    document.getElementById("VoucherDate").readOnly = true;
    $('#btnSave').click(function () {
        var vendorID = $('#AccountID option:selected').val();
        var voucherDate = $('#VoucherDate').val();        
        if (vendorID == "" || vendorID == 0) {
            swal("Vendor", "Please Select Vendor!", "error");
        }
        else if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
        else {
            $('#btnSave').prop('disabled', true);
            uiBlock();
            insert();
        }
    });

});
var invoiceTotal = 0;
function getVendorDetails(accountId) {
    //console.log(accountId);

    var controlPaying = '<input type = "Number" class="form-control">';

    ajaxCall("GET", { "accountId": accountId }, "application/json; charset=utf-8", "/VendorAdjustment/getVendorDetail",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        $('#tbl').find('tbody').empty();
        if (ien > 0) {

            var controlPaying = '<input type = "Number"  value=0>';
            var html = '';

            for (var i = 0; i < ien; i++) {
                var num = (data.qry[i].PurchaseDate).match(/\d+/g);
                var date = new Date(parseFloat(num));
                //console.log("date" +date);
                var month = parseInt(date.getMonth()) + 1;
                var dateString = date.getDate() + "/" + month + "/" + date.getFullYear();
                //console.log(dateString +"dstring");
                var totalAmount = parseFloat(data.qry[i].TotalAmount);
                var returnAmount = parseFloat(data.qry[i].ReturnAmount || 0);
                var VAT = parseFloat(data.qry[i].VAT || 0);
                var invoiceTotal = parseFloat(totalAmount + VAT).toFixed(3);
                var balance = parseFloat(invoiceTotal - data.qry[i].TotalPaid - returnAmount).toFixed(3);
                var status = data.qry[i].PaymentStatus;
                if (status == null || status == "") {
                    status = "";
                }


                html += '<tr>';
                //html+='<td>' + <input type=checkbox >+ '</td>';
                html += '<td><input type="hidden" value="' + data.qry[i].OrderID + '">' + data.qry[i].POID + '</td>';
                html += '<td>' + data.qry[i].InvoiceNo + '</td>';
                html += '<td><input type="hidden" value="' + data.qry[i].AccountID + '">' + data.qry[i].AccountName + '</td>';
                html += '<td>' + dateString + '</td>';
                html += '<td>' + invoiceTotal + '</td>';
                html += '<td>' + data.qry[i].TotalPaid + '</td>';
                html += '<td>' + status + '</td>';
                html += '<td>' + balance + '</td>';
                html += '<td><input type = "Number"  min="0" max=' + balance + ' step="0" class="form-control"></td> <span id="Error"></span>';

                html += '</tr>';
            }
            $('#tbl tbody').append(html);
        }
    }
    function onFailure(err) {
        console.log("ERROR");
    }

}

function calcTotal() {
    var total = 0;
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('td:eq(8) input[type="Number"]').val());
        $field = $row.find('td:eq(8) input[type="Number"]')
        //console.log("subTotal : " + subTotal);
        if (subTotal > Number($field.attr("max"))) {
            $field.val($field.attr("max"));
            toastr.warning('Adjustment Amount must be equal or less than balance!')
            subTotal = parseFloat($row.find('td:eq(8) input[type="Number"]').val());
        }
        if ($.isNumeric(subTotal)) {
            total += parseFloat(subTotal);
            $('#Amount').val(parseFloat(total).toFixed(3));
        }

        //console.log(subTotal);
        //console.log(total);
    });
    $('#tbl tfoot tr').find('td:eq(8) input[type="text"]').val(total);
    //$('#TotalPaying').val(total);    
}
$('#tbl').keyup(function (e) {
    calcTotal();
});
function insert() {
    var rows = [];
    var payLog = [];
    var JEntryLog = [];
    var totalAmount = 0;
    var totalPaying = $('#tbl tfoot tr').find('td:eq(8) input[type="text"]').val();    
    var orderId = 0;
    var vendorID = $('#AccountID option:selected').val();
    var desc = $('#Description').val();
    var voucherDate = $('#VoucherDate').val();
    //var chqDate = $('#ChequeDate').val();
    var amount = parseFloat($('#Amount').val());

    if (totalPaying == amount) {
        $('#tbl tbody tr').each(function (i, n) {

            var $row = $(n);
            orderId = $row.find('td:eq(0) input[type="hidden"]').val();
            var accountId = $row.find('td:eq(2) input[type="hidden"]').val();
            var balance = $row.find('td:eq(7)').html();
            var paying = $row.find('td:eq(8) input[type="Number"]').val();

            if (orderId != "" && balance != "" && paying != "") {

                totalAmount += parseFloat(paying);
                balance = parseFloat(balance) - parseFloat(paying);                

                rows.push({
                    OrderID: orderId,
                    Paying: paying,
                    Balance: balance
                });
                JEntryLog.push({
                    //BranchID : BranchID,
                    Amount: paying,                    
                    EntryTypeID: 13,
                    OrderTypeID: 1,
                    OrderID: orderId,
                    isReversed: false
                });
            }
        });
        if (rows.length) {

            var data = {
                'Description': desc,
                'Amount': amount,
                'AccountID': vendorID,
                'VoucherDate': voucherDate,                                
                'Details': rows
            };

            var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog});
            //console.log(json);            
            ajaxCall("POST", json, "application/json; charset=utf-8", "/VendorAdjustment/SavePayment", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result > 0) {
                    window.location.href = '/VendorPayment/Details?isNew=true&id=' + vendorID;
                }
                else {
                    swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                }
                //location.reload();
                //window.location.href = 'Index';
                //alert("success");
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    location.reload();
                //console.log(error);
                //location.reload();
            }
        }       
    }
    else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);

        swal("Total", "Total Paying Must be Equal to Total Amount of Payment/Cheque!", "error");
    }
}

