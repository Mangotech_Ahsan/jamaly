﻿
var AccountID = 0;
var soPaymentTypeID = 0;
$(function () {
    
    document.getElementById("VoucherDate").readOnly = true;    
    $("#btnSearch").on("click", function () {
        //AccountID = $('#AccountID option:selected').val();
        AccountID = $('#hdnAccountID').val();
        getCustomerDetails(AccountID);       
    });

    $('#btnSave').click(function () {
        //var customerID = $('#AccountID option:selected').val();
        var customerID = $('#hdnAccountID').val();
        var voucherDate = $('#VoucherDate').val();        
        var BranchID = $('#BranchID option:selected').val();
       
        if (customerID == "" || customerID == 0) {
            swal("Vendor", "Please Select Vendor! ", "error");
            //alert("Please Select Vendor");
        }
        else if (voucherDate == "") {
            swal("Date", "Please Enter Payment Date! ", "error");
            //alert("Please Enter Payment Date");
            //console.log(PurchaseDate + "date");
        }
        else {
            $('#btnSave').prop('disabled', true);
        uiBlock();
            insert();
        }
    });

});
var invoiceTotal = 0;
function getCustomerDetails(accountId) {
    $('#customerID').val(accountId).trigger('change.select2');
    $("#customerID").prop("disabled", true);    
    document.getElementById('VoucherDate').valueAsDate = new Date();
    var controlPaying = '<input type = "Number" class="form-control">';
    ajaxCall("GET", { "accountId": accountId }, "application/json; charset=utf-8", "/AdjustAccount/getCustomerDetail",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        $("#tbl").dataTable().fnDestroy();
        $('#tbl').find('tbody').empty();        
        if (ien > 0) {
            var html = '';
            for (var i = 0; i < ien; i++) {
                var num = (data.qry[i].SalesDate).match(/\d+/g);
                var date = new Date(parseFloat(num));
                //console.log("date" +date);
                var month = parseInt(date.getMonth()) + 1;
                var dateString = date.getDate() + "/" + month + "/" + date.getFullYear();
                //console.log(dateString +"dstring");
                var totalAmount = parseFloat(data.qry[i].TotalAmount);
                var VAT = parseFloat(data.qry[i].VAT || 0);
                var invoiceTotal = parseFloat(totalAmount + VAT).toFixed(3);
                var totalPaid = parseFloat(data.qry[i].TotalPaid)
                var returnAmount = parseFloat(data.qry[i].ReturnAmount || 0)
                var Balance = parseFloat(invoiceTotal - totalPaid - returnAmount);
                var status = data.qry[i].PaymentStatus;
                soPaymentTypeID = data.qry[i].PaymentTypeID;
                if (status == null || status == "") {
                    status = "";
                }
                html += '<tr>';
                //html+='<td>' + <input type=checkbox >+ '</td>';
                html += '<td id=OrderID><input type="hidden" value="' + data.qry[i].OrderID + '">' + data.qry[i].SOID + '</td>';
                html += '<td id=PONo>' + data.qry[i].PONo + '</td>';
                html += '<td><input id="AccountID" type="hidden" value="' + data.qry[i].AccountID + '">' + data.qry[i].AccountName + '</td>';
                html += '<td>' + dateString + '</td>';
                html += '<td>' + invoiceTotal + '</td>';
                html += '<td>' + data.qry[i].TotalPaid + '</td>';
                html += '<td>' + status + '</td>';
                html += '<td id=Balance>' + parseFloat(Balance).toFixed(3) + '</td>';
                html += '<td><input id="ReceivingAmount" type = "Number"  min="0" max=' + Balance + ' step="0" class="form-control"></td> <span id="Error"></span>';

                html += '</tr>';
            }
            $('#tbl tbody').append(html);
            
        }
        
        
    }
    function onFailure(err) {
        console.log(err);
    }

}

function calcTotal() {
    
    var total = 0;
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('td:eq(8) input[type="Number"]').val());
        $field = $row.find('td:eq(8) input[type="Number"]')
        //console.log("subTotal : " + subTotal);
        if (subTotal > Number($field.attr("max"))) {
            $field.val($field.attr("max"));
            toastr.warning('Receiving Amount must be equal or less than balance!')
            subTotal = parseFloat($row.find('td:eq(8) input[type="Number"]').val());
        }
        if ($.isNumeric(subTotal)) {
            total += parseFloat(subTotal);
            $('#Amount').val(total);
        }
    });
    $('#tbl tfoot tr').find('input[id*=txtTotal]').val(total);
    $('#txtTotal').val(total);
}
$('#tbl').keyup(function (e) {
    calcTotal();
});
function insert() {
    var rows = [];
    var JEntryLog = [];
    var totalAmount = 0;    
    var totalPaying = $('#tbl tfoot tr').find('td:eq(8) input[type="text"]').val();
    //var totalPaying = $('#TotalPaying').val();
    //console.log("Total Paying"+totalPaying);

    //var AccountID = $('#AccountID option:selected').val();
    var desc = $('#Description').val();    
    var voucherDate = $('#VoucherDate').val();        
    var amount = parseFloat($('#Amount').val());

    if (totalPaying == amount) {
        $('#tbl tbody tr').each(function (i, n) {

            var $row = $(n);
            //var orderID = $row.find('input[id*="OrderID"]').val();
            var OrderID = $row.find('td:eq(0) input[type="hidden"]').val();
            var PONO = $row.find("#PONO").text();
            //var orderId = $row.find('td:eq(0)').html();
            var AccountID = $row.find('input[id*="AccountID"]').val();
            var Balance = $row.find("#Balance").text();
            var Receiving = $row.find('input[id*="ReceivingAmount"]').val();
            if (OrderID != "" && Balance != "" && Receiving != "") {

                totalAmount += parseFloat(Receiving);
                Balance = parseFloat(Balance) - parseFloat(Receiving);

                rows.push({
                    OrderID: OrderID,
                    Receiving: Receiving,
                    Balance: Balance
                });
                JEntryLog.push({
                    //BranchID : BranchID,
                    Amount: Receiving,
                    OrderTypeID: 3,
                    EntryTypeID: 14,
                    OrderID: OrderID,
                    isReversed: false
                });                
            }
        });
        if (rows.length) {

            var data = {
                'Description': desc,
                'Amount': amount,
                'AccountID': AccountID,
                'VoucherDate': voucherDate,                                
                'Details': rows
            };           
            var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog});
            //console.log(json);
            ajaxCall("POST", json, "application/json; charset=utf-8", "/AdjustAccount/SaveCustomerAdjustment", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result > 0) {                    
                    window.location.href = '/CustomerPayment/Details?isNew=true&id=' + Result;
                }
                else {
                    $('#btnSave').prop('disabled', false);
                    uiUnBlock();
                    swal("Error!", "Please Check Your Entries! ", "error");
                    //alert("Some error Ocurred! Please Check Your Entries");
                }
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    location.reload();
            }
        }      
    }
    else {
        uiUnBlock();
        swal("Total!", "Total Receiving Must be Equal to Total Amount of Receiving/Cheque! ", "error");
        //alert("Total Receiving Must be Equal to Total Amount of Receiving/Cheque");
    }
}

