﻿var controlId = 0;
var vehControlId = 0;
var totalQty = 0;
var perPieceExp = 0;
var perProductExp = 0;
var expensePercentage = 0;
var tmpOrderID = 0;
var POID = 0;
getPOID();
$("#Bank").prop("disabled", true);
//Attach input width
$(function () {
    //window.onbeforeunload = function(e) {
    //return 'You have unsaved changes. If you leave the page these changes will be lost.';
    //};
    tmpOrderID = getTmpOrderID();    
    
    //addTable();
    addRow();
    //dummyData();
    $('#btnAddRow').click(function () {
        //insertTempData();
        addRow();
    })        
    $('#btnSubmit').click(function () {
        // Confirm required fields from Maaz bhai     
var isValid = true;      
  var accID = $('#AccountID option:selected').val();
        var InvoiceNo= $('#InvoiceNo').val();
        var PurchaseDate = $('#PurchaseDate').val();
        var paymentStatus = $('#PaymentStatus option:selected').val();
        //    var PaymentStatus = $('#PaymentStatus').val();
        var PaymentTypeID = $('#PaymentType option:selected').val();        
        //var BranchID = $('#BranchID option:selected').val();
        var BranchID = $('#hdnBranchId').val();
        if (BranchID == "" || BranchID == 0 || BranchID == undefined) {
            isValid = false;
            swal("Branch", "Please Select Branch!", "error");
            //alert("Please Select Branch");
        }
            else if(accID == "" || accID==0 || accID==undefined)
            {               
                isValid = false;
                swal("Vendor", "Please Select Vendor!", "error");
                 //alert("Please Select Vendor");
            }
        else if (PurchaseDate == "") {
            isValid = false;
            swal("Date", "Please Enter Purchase Date!", "error");
                //alert("Please Enter Purchase Date");
                //console.log(PurchaseDate + "date");
            }
        else if (InvoiceNo == "") {
            isValid = false;
            swal("Invoice No.", "Please Enter Invoice No!", "error");
                //alert("Please Enter Invoice No");
                //console.log(PurchaseDate+"date");
            }
            
        else if (paymentStatus == 0) {
            isValid = false;
            swal("Payment Status.", "Please Select Payment Status!", "error");
               //alert("Please Select Payment Status");
           }
            else if ((paymentStatus == 1 || paymentStatus == 2) && (PaymentTypeID == "" || PaymentTypeID == undefined)) {           
            
                isValid = false;
                swal("Payment Type.", "Please Select Payment Type!", "error");
                //alert("Please Select Payment Type");                     
               }                
           else if (isValid == true) {    
               $('#btnSubmit').prop('disabled', true);
               uiBlock();
                   insert();               
            }
    });
    $('#tbl').keyup(function (e) {
        var qty = $(e.target).closest("tr").find('input[id*="txtQty"]').val();
        var price = $(e.target).closest("tr").find('input[id*="txtPrice"]').val();       
        var currency = $('#Currency').val();
        if (currency == 2) {
            var convRate = $('#exRate').val();
            price = price * convRate; 
            $(e.target).closest("tr").find('input[id*="txtERate"]').val(price);
            //console.log("convRate = " + convRate);
            //console.log("newPrice = " + price);
        }
        if (($.isNumeric(price)) && ($.isNumeric(qty))) {
            var amount = (qty * price);
            $(e.target).closest('tr').find('input[id*="txtSub"]').val(parseFloat(amount).toFixed(3)
            );
            calcTotal();
        }
        else {
            $(e.target).closest('tr').find('input[id*="txtSub"]').val(''
            );
        }
    });
})
function getTmpOrderID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/TempPO/getTmpOrderID',
        async: true,
        success: function (data) {
            //console.log("tmpOrderID"+data);
            $("#tmpOrderID").val(data);
            tmpOrderID = data;
        },
        error: function (err) { console.log(err); }
    });
}
function calcVAT()
{
    var vat = parseFloat($('#vatInput').val());
    if (vat == "" || isNaN(vat)) {
        vat = 0; $('#vatInput').val(0)
        $('#vatAmount').val(parseFloat(0));
    }
    else {
        var totalInvoice = parseFloat($('#tbl tfoot tr').find('input[id*="txtTotal"]').val());
    var vatAmount = parseFloat((vat * totalInvoice) / 100);
    $('#vatAmount').val(parseFloat(vatAmount).toFixed(3));
    }
}
$("#vatInput").on("change", function () {
    calcVAT();
    getNewTotal();
    calcVatExpAmount();
});
function addRow() {
    insertTempData();
    var row = '';
    row += '<tr>';
    row += '<td ">';
    row += '<select id="ddlVehCode' + vehControlId + '" name="ddlVehCode' + vehControlId + '" onchange="getProducts(this)" class="select2 form-control"></select>';
    row += '<input type="hidden" id="hdnVId' + vehControlId + '" />';
    row += '</td>';
    row += '<td ">';
    row += '<select id="ddlPartNumber' + controlId + '" name="ddlPartNumber' + controlId + '" onchange="getDetail(this)" class="select2 form-control" style="width: 150px" ></select>';
    row += '<input type="hidden" id="hdnPId' + controlId + '" />';
    row += '</td>';
    row += '<td ">';
    row += '<button type="button" value="edit" id="btnEdit" name="btnEdit" class="btn btn-danger btn-sm" onclick="editProduct(this)" >Edit</button>';
    row += '</td>';    
    row += '<td><input type="text" class="form-control input-sm" style="width: 150px" name="" id="txtDescription" disabled /></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtVNumber" disabled/></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtVmodel" disabled/></td>';
    row += '<td><input type="text" class="form-control" name="" id="txtGroup" disabled/></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtLocation" disabled/></td>';
    //row += '<td><input type="text" class="form-control input-sm" name="" id="txtVehCode" disabled/></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtUnCode" disabled/></td>';
    row += '<td><input type="Number" class="form-control input-sm" style="width: 80px" name="qty[]" id="txtQty" min="1" /></td>';
    row += '<td><input type="Number" class="form-control input-sm" style="width: 80px" name="price[]" id="txtPrice" min="1" />' + '</td>';
    row += '<td><input type="Number" class="form-control input-sm" name="newRate[]" id="txtERate" disabled/>' + '</td>';
    //row += '<td><input type="Number" class="form-control input-sm" name="discPercent[]" id="txtDiscount" />' + '</td>';
    row += '<td><input type="Number" class="form-control input-sm" style="width: 80px" name="salePrice[]" id="txtSalePrice"  /></td>';
    row += '<td><input type="Number" class="form-control input-sm" style="width: 120px" name="sub" style="width: 70px" id="txtSub" disabled />' + '</td>';
    row += '<td><button type="button" value="Remove" id="btnRemove" name="btnRemove" class="btn btn-danger btn-sm" onclick="remove(this)" tabindex="0">Remove</button></td>';
    row += '</tr>';    
    $("#tbl tbody").append(row);
    GetDropdown("ddlVehCode" + vehControlId, "/VehicleCode/getProduct_VehCode", "{}", true);
    //GetDropdown("ddlPartNumber" + controlId, "/Purchase/getProduct_PartNumbers", "{}", true);
    controlId += 1;
    vehControlId += 1;    
    //table.setAttribute("id", "myId");
    //console.log(document.getElementById("tbl").classList);    
}
function getProducts(pro) {
    var tr = $(pro).closest('tr');    
    var vehCodeID = $(tr).find('td:eq(0) :selected').val();
    var controlID = $(pro).closest('tr').index();    
    var json = { "vehCodeId": vehCodeID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getProducts',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {            
            GetDropdown1("ddlPartNumber" + controlID,data, true);
        },
        error: function (err) { console.log(err); }
    });    
}
function getDetail(pro) {
    //CheckDuplicate();
    var currency = $('#Currency').val();
    var exchangeRate = $('#exRate').val();
    //console.log("rate"+exchangeRate);
    var accountID = $('#AccountID option:selected').val();
    if (currency == 0)
    {
        swal("Currency", "Please Select Currency First!", "error");
        //alert("Please Select Currency First !");
    }
    else
    {        
        var tr = $(pro).closest('tr');
        var pId = $(tr).find('td:eq(1) :selected').val();
        $(tr).find('input[id*="hdnPId"]').val(pId);
        //productDuplicate(pId);
        // get product description
        var json = { "productId": pId };
        //ajaxCall("GET", json, "application/json; charset=utf-8", "/Purchase/getProductDetail", "json", onSuccess, onFailure);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Purchase/getProductDetail',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {                
                $(tr).find('input[id*="txtDescription"]').val(data.Description);                
                $(tr).find('input[id*="txtVNumber"]').val(data.SubstituteNo);
                $(tr).find('input[id*="txtGroup"]').val(data.GroupName);
                $(tr).find('input[id*="txtVmodel"]').val(data.VehicleName);
                $(tr).find('input[id*="txtLocation"]').val(data.Location);
                $(tr).find('input[id*="txtVehCode"]').val(data.VehicleCode);
                $(tr).find('input[id*="txtUnCode"]').val(data.UnitCode);
            },
            error: function (err) { console.log(err); }
        });
        var json = { "productId": pId };       
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Purchase/getLastUnitPrice',            
            async: true,
            data: JSON.stringify(json),
            success: function (data) {                
                $(tr).find('input[id*="txtPrice"]').attr('placeholder', data);
            },
            error: function (err) { console.log(err); }
        });       
    }
}
//  Get New POID 
function getPOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getNewPOID',
        async: true,
        success: function (data) {
            {                               
                $("#POID").val(data);
                POID = data;
            }
        },
        error: function (err) { console.log(err); }
    });
}
function remove(input) {

    var totalRowCount = $("#tbl tbody tr").length;
    if (totalRowCount > 1) {
        $(input).parent().parent().remove();        
        calcTotal();
        controlId--;
        vehControlId--;        
        //reIndextbl();
    }
    else {
        swal("Rows", "Cannot remove all rows!", "error");
        //alert("Cannot remove all rows");
    }

}
function onCurrChange()
{
    //console.log("OnCurrCalled");
    $('#tbl  tbody tr').each(function (i, row) {
        // reference all the stuff you need first
        var $row = $(row);
            //var qty = $row.find('input[id*="txtQty"]').val();                 
            //console.log($row + "  qty = " + qty);
            var qty = $row.find('input[id*="txtQty"]').val();
            var price = $row.find('input[id*="txtPrice"]').val();
            var currency = $('#Currency').val();
            if (currency == 2) {
                var convRate = $('#exRate').val();
                price = price * convRate;
                $row.find('input[id*="txtERate"]').val(price);
            }
            else {
                $row.find('input[id*="txtERate"]').val("");
            }
            if (($.isNumeric(price)) && ($.isNumeric(qty))) {
                var amount = (qty * price);
                $row.find('input[id*="txtSub"]').val(parseFloat(amount).toFixed(3)
                );
                calcTotal();
            }
            else {
                $row.find('input[id*="txtSub"]').val('');
            }
    });
}
function calcTotal() {
    var total = 0;
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);        
        //var subTotal = parseFloat($row.find('td:eq(13) input[id*="txtSub"]').val());  
        var subTotal = parseFloat($row.find('input[id*="txtSub"]').val());
        total += parseFloat(subTotal);        
    });

    $('#tbl tfoot tr').find('input[id*="txtTotal"]').val(parseFloat(total).toFixed(3));    
    $('#TotalAmount').val(parseFloat(total).toFixed(3));
    var gTotal = $('#TotalAmount').val();    
    getNewTotal();
    calcVAT();
}
function calcTotalQty() {
   
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        //var subQty = parseFloat($row.find('td:eq(9) input[id*="txtQty"]').val());
        var subQty = parseFloat($row.find('input[id*="txtQty"]').val());
        totalQty += parseFloat(subQty);            
    });
}
// if any of amount, expense , vat is changed call this to make effect every where 
function getNewTotal() {
    var gTotal = $('#tbl tfoot tr').find('input[id*="txtTotal"]').val();
    var Expense = $('#expenseInput').val();
    var VAT = $('#vatAmount').val();
    if (Expense == "" || isNaN(Expense)) { Expense = 0; }
    if (VAT == "" || isNaN(VAT)) { VAT = 0; $('#inputVAT').val(0) }
    var newTotal = parseFloat(gTotal) + parseFloat(Expense) + parseFloat(VAT);
    var amountPaid = parseFloat(gTotal) + parseFloat(VAT);
    $('#TotalAmount').val(parseFloat(newTotal).toFixed(3));
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(amountPaid).toFixed(3);
    }    
    
}

$("#expenseInput").on("change", function () {
    getNewTotal();
    calcVatExpAmount();
});

    // If currency is AED then perform actions
    $("#Currency").on("change", function () {
        var currencyID = $("#Currency").val();
        if (currencyID == 1)
        {
            onCurrChange();                            
        }
        if (currencyID == 2) {
            //console.log("Currency event called " + currencyID);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: '/Purchase/getCurrency',
                async: true,
                success: function (data) {
                    //console.log("ajax"+data);
                    $("#exRate").val(data);
                    calcTotal();
                    onCurrChange();
                },
                error: function (err) { console.log(err); }
            });
        }
    });
// Payment Status DropDOwn, Action according to Payment Status Selection
$("#PaymentStatus").on("change", function () {
    var payStatusSelection = $("#PaymentStatus").val();
    var totalAmount = $("#TotalAmount").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").readOnly = true;
        getNewTotal();
        //document.getElementById("amountPaid").value = totalAmount;
        $("#PaymentType").prop("disabled", false);
        //document.getElementById("expenseInput").readOnly = false;
        //document.getElementById("vatInput").readOnly = false;
    }
    else if (payStatusSelection == 2) {
        document.getElementById("amountPaid").readOnly = false;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("amountPaid").value = 0;
        //document.getElementById("expenseInput").readOnly = false;
        //document.getElementById("vatInput").readOnly = false;
    }
    else if (payStatusSelection == 3) {
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        $("#PaymentType").prop("disabled", true);
        //document.getElementById("expenseInput").readOnly = true;
        //document.getElementById("vatInput").readOnly = true;
    }
});
// 
// Payment Type Change 
$("#PaymentType").on("change", function () {
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //Cash
    if (PaymentTypeID == 1) {
        //$("#bankName").prop("disabled", true);
        $("#Bank").prop("disabled", true);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);
    }
        // Bank
    else if (PaymentTypeID == 2) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);

    }
        // Cheque
    else if (PaymentTypeID == 3) {
        //$("#bankName").prop("disabled", false);
        $("#bankName").prop("disabled", false);
        $("#chqNumber").prop("disabled", false);
        $("#chqDate").prop("disabled", false);
    }
});
function calcVatExpAmount() {
    var vat = parseFloat($('#vatAmount').val());
    var exp = parseFloat($('#expenseInput').val());
    var productTotal = $('#tbl tfoot tr').find('input[id*="txtTotal"]').val();
    if (vat == "" || isNaN(vat)) {
        vat = 0;
        $('#vatInput').val(0);
    }
    if (exp == "" || isNaN(exp)) {
        exp = 0; $('#expenseInput').val(0);
    }
    //calcTotalQty();
    if (exp > 0 || vat > 0) {
        var totalParts = $('#tbl tbody tr').length;
        var totalVATExp = parseFloat(exp) + parseFloat(vat);
        expensePercentage = parseFloat(totalVATExp) / parseFloat(productTotal);
        //console.log(expensePercentage + " expensePercentage ");
        //perProductExp = parseFloat(totalVATExp) / parseFloat(totalParts);
        //console.log(totalParts + " Rows");
        //var total = parseFloat($('#tbl tfoot tr').find('input[id*="txtTotal"]').val()).toFixed(2);
        //var totalVATExp = parseFloat(exp) + parseFloat(vat);
        //console.log(totalVATExp + " totalVATExp");
        //var percent = parseFloat((totalVATExp / total) * 100);
        //perPieceExp = parseFloat((percent * total) / 100);
        //totalExpAmount = (parseFloat(percent) * parseFloat(total)) / 100;
        //console.log(totalExpAmount + " totalExpAmount");
        //perPieceExp = parseFloat(totalExpAmount) / totalParts;
       //console.log(perPieceExp + " perPieceExp");
    }
}
function insert() {
    var currencyID = $("#Currency").val();    
    var exchangeRate = $('#exRate').val();
    var rows = [];
    var stockLog = [];
    var total = 0;
    var qtyVE = 0;
    var isValid = false;    
    totalInvoice = $('#TotalAmount').val();
    //if (totalInvoice == "" || totalInvoice == 0) {
    var paidAmount = $('#amountPaid').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    //console.log(totalInvoice);
    var BranchID = $('#hdnBranchId').val();
    var vat = parseFloat($('#vatAmount').val());
    var exp = parseFloat($('#expenseInput').val());
    if (vat == "" || isNaN(vat)) {
        vat = 0;
        $('#vatInput').val(0);
     }
    if (exp == "" || isNaN(exp)) {
            exp = 0; $('#expenseInput').val(0);
    }
    calcVatExpAmount();

        // 
        $('#tbl tbody tr').each(function (i, n) {
            var $row = $(n);
            var pId = $row.find('td:eq(1) :selected').val();
            var partNumber = $row.find('td:eq(1) option:selected').text();
            var pLocation = $row.find('input[id*="txtLocation"]').val();            
            var qty = $row.find('td:eq(9) input[type="Number"]').val();
            var price = 0;
            if (currencyID == 1) {
                price = $row.find('td:eq(10) input[type="Number"]').val();
            }
           else if (currencyID == 2) {
                price = $row.find('td:eq(11) input[type="Number"]').val();
            }           
            //var newRate = $row.find('td:eq(9) input[type="Number"]').val();  // add new field to Database if required
            var salePrice = $row.find('td:eq(12) input[type="Number"]').val();
            var subTotal = $row.find('td:eq(13) input[type="Number"]').val();
            exchangeRate = $row.find('input[id*="txtERate"]').val();
            if (pId != "" && qty != "" && qty != 0 && price != "" && price != 0) {
                isValid = true;
                rows.push({
                    PartNo: partNumber,
                    ProductId: pId,
                    Location: pLocation,
                    Qty: qty,
                    UnitPrice: price,
                    IsReturned: false,
                    ReturnedQty: 0,
                    SalePrice: salePrice,
                    ExchangeRate: exchangeRate,
                    Total: subTotal,
                    BranchID: BranchID
                });
                var total1 = parseFloat(expensePercentage) * parseFloat(subTotal);
                var costing = parseFloat(total1) / parseInt(qty);
                //perPieceExp = perProductExp / qty;
                var newprice = parseFloat(price) + parseFloat(costing);
                //console.log(newprice + "  newprice");
                stockLog.push({      
                    AccountID:$('#AccountID option:selected').val(),
                    ProductId: pId,
                    StockIN: qty,
                    InvoiceDate: $('#PurchaseDate').val(),
                    CostPrice: parseFloat(newprice).toFixed(3),
                    SalePrice: salePrice,
                    Location : pLocation,
                    InReference: 'Purchase',
                    OrderTypeID: 1,
                    UserReferenceID: $('#InvoiceNo').val(),
                    BranchID: BranchID
                });
            }
            else if (pId == "") {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                isValid = false;
                swal("Product", "Please Select Product!", "error");
                //alert("Please Select Product");
            }
            else if (qty == "" || qty == 0) {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                isValid = false;
                swal("Quantity", "Please Enter Quantity!", "error");
                //alert("Please Enter Quantity");
            }
            else if (price == "" || price == 0) {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                isValid = false;
                swal("Unit Cost", "Please Enter Unit Cost!", "error");
                //alert("Please Enter Unit Cost");
            }
            else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                swal("Error", "Some error occured!", "error");
                //alert("Error!!!");
            }
        });

        if (rows.length && isValid == true) {

            total = $('#tbl tfoot tr').find('input[id*="txtTotal"]').val();            
            //total = $('#Total').val()
            var paymentStatus = 0;
            if ($('#PaymentStatus option:selected').val() != 0) {
                paymentStatus = $('#PaymentStatus option:selected').text()
            }
            var invoiceAmount = parseFloat(total + vat).toFixed(3);
            var data = {                
                'AccountID': $('#AccountID option:selected').val(),
                'BranchID': BranchID,
                'POID' :  $("#POID").val(),
                'InvoiceNo': $('#InvoiceNo').val(),
                'PaymentStatus': $('#PaymentStatus').val(),
                'PurchaseDate': $('#PurchaseDate').val(),
                'PaymentStatus': paymentStatus,
                'PaymentTypeID': $('#PaymentType option:selected').val(),
                'VAT': vat,   
                'Expenses': $('#expenseInput').val(),
                'PurchaseCode': $('#PurchaseCode').val(),
                'Currency': $('#Currency option:selected').text(),
                'TotalAmount': invoiceAmount,
                'ChequeDate': $("#chqDate").val(),
                'ChequeNo': $("#chqNumber").val(),
                'BankName': bank,
                'AmountPaid': $('#amountPaid').val(),
                'tbl_PODetails': rows
            };
            var json = JSON.stringify({ 'model': data, 'modelStockLog': stockLog, 'bankAccId': bankAccountId });
            //console.log(json);
            ajaxCall("POST", json, "application/json; charset=utf-8", "/Purchase/SaveOrder", "json", onSuccess, onFailure);
            //debugger;
            function onSuccess(Result) {                
                if (Result == "success") {
                    window.location.href = 'Index';
                    deleteTempOrder(tmpOrderID);
                    
                }
                else {
                    uiUnBlock();
                    $('#btnSubmit').prop('disabled', false);
                    swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                    //alert("Some error Ocurred! Please Check Your Entries");
                }
                //location.reload();
                //window.location.href = 'Index';
                //alert("success");
            }
            function onFailure(error) {

                if (error.statusText == "OK") {
                    uiUnBlock();
                    $('#btnSubmit').prop('disabled', false);
                    //location.reload();
                    //window.location.href = 'Index';
                    //deleteTempOrder(tmpOrderID);
                    //console.log(error.status);
                    //alert(error.status);
                }
                else {
                    swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                }
            }
        }
    
}
function insertTempData() {
    var currencyID = $("#Currency").val();
    var exchangeRate = $('#exRate').val();
    var tempDetails = [];
    var total = 0;
    var qtyVE = 0;
    totalInvoice = $('#TotalAmount').val();
    var BranchID = $('#hdnBranchId').val();
    $('#tbl tbody tr').each(function (i, n) {

        var $row = $(n);
        var pId = $row.find('td:eq(1) :selected').val();
        var partNumber = $row.find('td:eq(1) option:selected').text();
        var pLocation = $row.find('td').eq(5).find('input').val();
        var qty = $row.find('td:eq(9) input[type="Number"]').val();
        var price = 0;
        if (currencyID == 1) {
            price = $row.find('td:eq(10) input[type="Number"]').val();
        }
        else if (currencyID == 2) {
            price = $row.find('td:eq(11) input[type="Number"]').val();
        }
        //var newRate = $row.find('td:eq(9) input[type="Number"]').val();  // add new field to Database if required
        var salePrice = $row.find('td:eq(12) input[type="Number"]').val();
        var subTotal = $row.find('td:eq(13) input[type="Number"]').val();
        if (pId != "" && qty != "" && qty != 0 && price != "" && price != 0) {
            isValid = true;
            tempDetails.push({
                PartNo: partNumber,
                ProductId: pId,
                tmpID: tmpOrderID,
                Location: pLocation,
                Qty: qty,
                UnitPrice: price,
                IsReturned: false,
                ReturnedQty: 0,
                SalePrice: salePrice,
                ExchangeRate: exchangeRate,
                Total: subTotal,
                BranchID: BranchID
            });
        }
    });
    if (tempDetails.length) {

        total = $('#tbl tfoot tr').find('input[id="txtTotal"]').val();
       // console.log(total);
        //total = $('#Total').val()
        var paymentStatus = 0;
        if ($('#PaymentStatus option:selected').val() != 0) {
            paymentStatus = $('#PaymentStatus option:selected').text()
        }
        var tmp_Order = {
            'tmpID': tmpOrderID,
            'AccountID': $('#AccountID option:selected').val(),
            'BranchID': BranchID,
            'InvoiceNo': $('#InvoiceNo').val(),
            'PaymentStatus': $('#PaymentStatus').val(),
            'PurchaseDate': $('#PurchaseDate').val(),
            'PaymentStatus': paymentStatus,
            'PaymentTypeID': $('#PaymentType option:selected').val(),
            'VAT': $('#vatInput').val(),
            'Expenses': $('#expenseInput').val(),
            'PurchaseCode': $('#PurchaseCode').val(),
            'Currency': $('#Currency option:selected').text(),
            'TotalAmount': document.getElementById("TotalAmount").value,
            'DiscountPercent': $('#DiscountPercent').val(),
            'DiscountAmount': $('#DiscountAmount').val(),
            'AmountPaid': $('#amountPaid').val(),
            'tmp_OrderDetails': tempDetails

        };

        var json = JSON.stringify({ 'model': tmp_Order });
        //console.log(json);

        ajaxCall("POST", json, "application/json; charset=utf-8", "/TempPO/tmpSaveOrder", "json", onSuccess, onFailure);        
        function onSuccess(Result) {
            if (Result == "success") {
                console.log("success");
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }            
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }

    }
}
function deleteTempOrder(tmpOrderID) {

    var json = { "tmpOrderID": tmpOrderID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/TempPO/deleteTempOrder',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
        },
        error: function (err) { console.log(err); }
    });
}
