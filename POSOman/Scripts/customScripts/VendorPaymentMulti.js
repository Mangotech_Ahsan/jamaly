﻿var chqDat = "";
var C = 1;
var Accs;

var boolCheck = false;
$tableItemCounter = 0;
$addedIDs = [];
$(function () {
    //document.getElementById("Bank").readOnly = true;
    $("#Bank").prop("disabled", true);
    //document.getElementById("VoucherDate").readOnly = true;
    document.getElementById("ChequeDate").readOnly = true;
    document.getElementById("ChequeNumber").readOnly = true;
    // document.getElementById("ChequeDate").readOnly = true;
    $('#btnSave').click(function () {
        var vendorID = $('#AccountID option:selected').val();
        var voucherDate = $('#VoucherDate').val();
        chqDate = $('#ChequeDate').val();
        var PaymentTypeID = $('#PayTypeID option:selected').val();
        var bankAccountId = $('#Bank option:selected').val();
        if (chqDate == "") {
            //alert("Please cheque date");
            chqDat = "";
            //console.log(PurchaseDate+"date");
        }
        if (vendorID == "" || vendorID == 0) {
            swal("Vendor", "Please Select Vendor!", "error");
        }
        else if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if ((PaymentTypeID == 3 || PaymentTypeID == 2) && (bankAccountId == "" || bankAccountId == undefined)) {
            isValid = false;
            swal("Bank!", "Please Select  Bank!", "error");
        }
        else {
            $('#btnSave').prop('disabled', true);
            uiBlock();
            insert();
        }
    });

});
var invoiceTotal = 0;

$('#btnSearch').click(function () {

    var AccountID = $("#AccountID").val();
    var BranchID = $("#BranchID").val();

    if (AccountID <= 0 || AccountID == null || AccountID == '') {
        swal("Account", "Please select vendor", "error");
    }
    else if (BranchID <= 0 || BranchID == null || BranchID == '') {
        swal("Project", "Please select Project", "error");
    }

    else {
        getVendorDetails(AccountID, BranchID);
    }
});

////////////////////////////
function remove(input) {
    var Amount = $("#Amount").val() || 0;
    // console.log($addedIDs);

    $("#payTbl tbody").find('input[name="record"]').each(function () {
        //  alert("am");
        if ($(this).is(":checked")) {

            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            var ID = row.find('input[id*="ID"]').val(); // find hidden id 
            var index = $.inArray(ID, $addedIDs);
            if (index >= 0) { $addedIDs.splice(index, 1); }

            calcRemBal();

            // console.log($tableItemCounter);
            if ($tableItemCounter <= 0) {
                //  alert("coun");
                $("#RemBal").val(Amount);
            }

            //  calcTotal();
        }
    });
}

function checkNullValueInAmount() {
    $('#payTbl tbody tr').each(function (i, n) {

        var $row = $(n);
        var Amount = parseInt($row.find('#paymentAmount').text());/*parseInt($row.find('td:eq(5) input[type="Number"]').val());*/

        // console.log(Amount);
        if (Amount == null || Amount == '' || !$.isNumeric(Amount) || Amount <= 0) {
            boolCheck = true;
        }
        else {
            boolCheck = false;
        }

    });
}



$("#btnAdd").click(function () {
    var Amount = $("#PayAmount").val();
    var Bal = $("#RemBal").val();
    var PayTypeID = $("#PayTypeID :selected").val();



    var PayType = $("#PayTypeID :selected").text();
    var Bank = $("#Bank :selected").text() || "-";
    var BankID = $("#Bank :selected").val();  // 1

    if (Bank == "Select") {
        Bank = "-";
        BankID = null;
    }
    var chNo = $("#ChequeNumber").val() || "-"; // 3
    var chDate = $("#ChequeDate").val() || "-";  // 4
    var PayAmount = $("#PayAmount").val(); // 2
    var PayID = '<input type="hidden" id="PayTypeID" value="' + PayTypeID + '"/>';

    var BnkID = '<input type="hidden" id="BankID" value="' + BankID + '"/>';

    checkNullValueInAmount();
    if (PayTypeID <= 0 || PayTypeID == null || PayTypeID == "") {
        swal("PayType", "Please Select PayType! ", "error");
    }
    else if (Amount <= 0 || Amount == null || Amount == "") {
        swal("Amount", "Please Enter Amount! ", "error");
    }

    else if ((Bal - Amount) < 0) {
        swal("Error", "Cannot Enter Amount greater than balance", "error");
        // $("#RemBal").val(RemainBal);
    }
    else if (Bal <= 0) {
        swal("Remaining Balance", "You cannot add further pay options as balance amount is setteled! ", "error");
    }
    else if (boolCheck == true) {
        swal("Amount", "Invalid amount entered! ", "error");


    }

    else if (PayTypeID == 2 && (BankID == null || BankID == "" || BankID <= 0)) {
        swal("Bank", "Please Select Bank! ", "error");


    }


    else if (PayTypeID == 4 && ((chDate == null || chDate == "" || chDate == "-") || (chNo == null || chNo == "" || chNo == "-"))) {
        swal("Chq in hand", "invalid chq in hand entry ! Check Cheque Date / Number ", "error");


    }

    else if ((PayTypeID == 3 || PayTypeID == 5) && ((chDate == null || chDate == "" || chDate == "-") || (chNo == null || chNo == "" || chNo == "-") || (BankID == null || BankID == "" || BankID <= 0))) {
        swal("Cheque/Others", "invalid  entry ! Check Cheque Date / Number / Bank ", "error");


    }




    //else if (subTot <= 0 || subTot == null || subTot == undefined || subTot == "") {
    //    swal("Product", "Please Enter Qty and saleprice! ", "error");
    //}
    else {
        var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + PayID + "" + PayType + "</td><td>" + BnkID + "" + Bank + "</td><td id='chqDate'>" + chDate + "</td><td id='chqNumber'>" + chNo + "</td><td id='paymentAmount'>" + Amount + "</td></tr>";

        $("#payTbl tbody").append(markup);
        ///////////////Old Scenario////////////////////
        // getAccounts();
        //var html = '';
        //var id = '<input type="hidden" id="ID" value="' + C + '"/>';
        //var BnkSelect = '<select id="BnkAccID' + C + '" name="BID" class="form-control" style="width:200px;" ></select>';

        //// console.log(C);
        ////var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + pid + "" + PartNO + "</td><td>" + Description + "</td><td>" + UnitCode + "</td><td>" + Location + "</td><td>" + Qty + "</td><td>" + UnitPrice + "</td><td>" + SalePrice + "</td><td>" + SubTotal + "</td></tr>";

        //html += '<tr>';
        //html += '<td><input type="checkbox" name="record"></td>';
        //html += '<td id="GetPayTypeID" ><select id ="payType" name="payType" class = "select2 form-control" onchange="getDropDownID(this)"><option value = "1">Cash</option><option value = "2">Bank</option><option value = "3">Cheque</option><option value = "4">chq in hand</option><option value = "5">Others</option>  </select>' + id + '</td>';
        // html += '<td id="GetBnkID" name="BID"><input type="hidden" id="hdnControlID" value="' + C + '">' + BnkSelect + '</td>';

        //html += '<td><input  type="Date" id="chqDate" class="form-control"  /></td>';
        //html += '<td><input  type="text"  class="form-control" style="width: 150px; " id="chqNumber"  /></td>';
        //html += '<td><input  type="number"  class="form-control" style="width: 150px; " id="chqAmount"  /></td>';
        //html += '<td><input type="hidden" id="hdnBnkcolID" value="' + C + '"></td>';

        //html += '</tr>';
        //$("#payTbl tbody").append(html);


        ///////////////Old Scenario////////////////////


        $tableItemCounter++;
        //$addedIDs.push(C);
        //C++;

        $("#RemBal").val(Bal - Amount);
        boolCheck = false;
        // $('#tbl tbody').append(html);
        // GetBankDropdown();


        // var markup = "<tr><td><input type='checkbox' name='record'></td><td><select id ='payType' class = 'select2 form-control'><option value = '2'>Bank</option><option value = '3'>Cheque</option>  </select>" + id + "</td><td><select id ='BnkAccID' class = 'select2 form-control'></select></td><td ><input  type='Date' required id='chqDate' class='form-control'  /></td><td ><input  type='text'  class='form-control' style='width:150px;' id='chqNumber'  /></td><td><input  type='number'  class='form-control' style='width:150px;' id='chqAmount'  /></td></tr>";

        // var BranchSelect = '<select id="BnkID' + C + '" class="form-control"></select>';

        // $('#payTbl').find('tbody').empty();
        // console.log("empty");
        //var s = '<option value="-1">Please Select Bank</option>';
        //for (var i = 0; i < data.length; i++) {
        //    s += '<option value="' + data[i].Value + '">' + data[i].Name + '</option>';
        //}
        //$("#payTbl tbody").append(markup);
        //$("#BnkAccID").html(s);
        //console.log(data+"dasd");

        //   }
        //  });





        //  console.log($addedIDs);
        clearFields();
        // calcNewInstallmentTotal();



    }

});


function clearFields() {
    $('#PayTypeID').val(0).trigger('change.select2');
    $('#Bank').val(0).trigger('change.select2');
    $("#Bank").prop("disabled", true);
    $('#ChequeDate').val("");
    $('#PayAmount').val("");
    $('#ChequeNumber').val("");
    document.getElementById("ChequeDate").readOnly = true;
    document.getElementById("ChequeNumber").readOnly = true;
    //$('#ddlVehCode').val(-1).trigger('change.select2');
    //document.getElementById('selectIdHere').selectedIndex = 0;
    // or with jQuery
    // $("#ddlVehCode").prop("selectedIndex", 0);
}
//$("#btnAdd").click(function () {
//    var Amount = $("#Amount").val();
//    var Bal = $("#RemBal").val();
//    // var subTot = $("#SubTotal").val();
//    checkNullValueInAmount();
//    if (Amount <= 0 || Amount == null || Amount == "") {
//        swal("Amount", "Please Enter Amount! ", "error");
//    }
//    else if (Bal <= 0) {
//        swal("Remaining Balance", "You cannot add further pay options as balance amount is setteled! ", "error");
//    }
//    else if (boolCheck == true) {
//        swal("Amount", "Invalid amount entered! ", "error");


//    }
//    //else if (subTot <= 0 || subTot == null || subTot == undefined || subTot == "") {
//    //    swal("Product", "Please Enter Qty and saleprice! ", "error");
//    //}
//    else {


//        //$.ajax({
//        //    type: "GET",
//        //    url: "/InstallmentPlan/getBankAccounts",
//        //    data: "{}",
//        //    success: function (data) {
//        getAccounts();
//        var html = '';
//        var id = '<input type="hidden" id="ID" value="' + C + '"/>';
//        var BnkSelect = '<select id="BnkAccID' + C + '" name="BID" class="form-control" style="width:200px;"></select>';

//        // console.log(C);
//        //var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + pid + "" + PartNO + "</td><td>" + Description + "</td><td>" + UnitCode + "</td><td>" + Location + "</td><td>" + Qty + "</td><td>" + UnitPrice + "</td><td>" + SalePrice + "</td><td>" + SubTotal + "</td></tr>";

//        html += '<tr>';
//        html += '<td><input type="checkbox" name="record"></td>';
//        html += '<td id="GetPayTypeID" ><select id ="payType" name="payType" class = "select2 form-control"><option value = "1">Cash</option><option value = "2">Bank</option><option value = "3">Cheque</option><option value = "4">chq in hand</option><option value = "5">Others</option>  </select>' + id + '</td>';
//        //  html += '<td><select id ="BnkAccID" class = "select2 form-control"></select></td>';
//        html += '<td id="GetBnkID" name="BID"><input type="hidden" id="hdnControlID" value="' + C + '">' + BnkSelect + '</td>';

//        html += '<td><input  type="Date" id="chqDate" class="form-control"  /></td>';
//        html += '<td><input  type="text"  class="form-control" style="width: 150px; " id="chqNumber"  /></td>';
//        html += '<td><input  type="number"  class="form-control" style="width: 150px; " id="chqAmount"  /></td>';
//        html += '<td><input type="hidden" id="hdnBnkcolID" value="' + C + '"></td>';

//        html += '</tr>';
//        $("#payTbl tbody").append(html);
//        $tableItemCounter++;
//        $addedIDs.push(C);
//        C++;
//        // $('#tbl tbody').append(html);
//        GetBankDropdown();


//        // var markup = "<tr><td><input type='checkbox' name='record'></td><td><select id ='payType' class = 'select2 form-control'><option value = '2'>Bank</option><option value = '3'>Cheque</option>  </select>" + id + "</td><td><select id ='BnkAccID' class = 'select2 form-control'></select></td><td ><input  type='Date' required id='chqDate' class='form-control'  /></td><td ><input  type='text'  class='form-control' style='width:150px;' id='chqNumber'  /></td><td><input  type='number'  class='form-control' style='width:150px;' id='chqAmount'  /></td></tr>";

//        // var BranchSelect = '<select id="BnkID' + C + '" class="form-control"></select>';

//        // $('#payTbl').find('tbody').empty();
//        // console.log("empty");
//        //var s = '<option value="-1">Please Select Bank</option>';
//        //for (var i = 0; i < data.length; i++) {
//        //    s += '<option value="' + data[i].Value + '">' + data[i].Name + '</option>';
//        //}
//        //$("#payTbl tbody").append(markup);
//        //$("#BnkAccID").html(s);
//        //console.log(data+"dasd");

//        //   }
//        //  });





//        //  console.log($addedIDs);
//        //  clearFields();
//        // calcNewInstallmentTotal();



//    }

//});
//function remove(input) {
//    $("table tbody").find('input[name="record"]').each(function () {
//        if ($(this).is(":checked")) {
//            $(this).parents("tr").remove();
//            $tableItemCounter--;
//            var row = $(this).closest("tr");
//            var Ids = row.find('input[id*="hdnControlID"]').val(); // find hidden id 
//            var index = $.inArray(Ids, $addedIDs);
//            if (index >= 0) { $addedIDs.splice(index, 1); }
//           // calcTotal();
//        }
//    });
//}

function getAccounts() {
    ajaxCall("POST", {}, "application/json; charset=utf-8", "/InstallmentPlan/getBankAccounts", "json", onSuccess);
    function onSuccess(data) {
        Accs = data;
    }
    function onFailure(err) {
        console.log(err);
    }
}


function GetBankDropdown() {

    $('#payTbl tbody tr').each(function (i, n) {

        var $row = $(n);
        var bnkColID = $row.find('input[id*="hdnBnkcolID"]').val();
        //console.log(bnkColID+"BID");

        //var pId = $row.find('input[id*="hdnPId"]').val(pId);
        var controlId = $row.find('input[id*="hdnControlID"]').val();
        //console.log(controlId+"CID");
        var ddl = "#BnkAccID" + controlId;
        $(ddl).empty();
        $(ddl).select2();
        $.each(Accs, function (key, value) {
            $(ddl).append($("<option></option>").val(value.Value).html(value.Name));
            //$("#" + ddl + "option[value='++']").remove();
            $(ddl).find('option[value=' + bnkColID + ']').remove();
        });
    });
}

// Find and remove selected table rows   

$('#payTbl').keyup(function (e) {
    calcRemBal();
});

function calcRemBal() {
    var TotAmount = parseInt($("#Amount").val()) || 0;
    // var Bal = 0;
    var RemBal = parseInt($("#Amount").val()) || 0;
    $('#payTbl tbody tr').each(function (i, n) {
        var $row = $(n);
        var subAmount = parseInt($row.find('#paymentAmount').text()) || 0;// parseInt($row.find('td:eq(5) input[type="Number"]').val()) || 0;

        if (subAmount > RemBal && subAmount > 0) {


            if ($tableItemCounter <= 0) {

                $("#RemBal").val(TotAmount);
                //  $row.find('td:eq(5) input[type="Number"]').val(TotAmount);
            }
            else {

                $("#RemBal").val(RemBal);
                $row.find('td:eq(5) input[type="Number"]').val('');
            }

        }
        else if (subAmount > 0 && subAmount <= RemBal) {

            if ($.isNumeric(subAmount)) {

                RemBal -= subAmount;
                $("#RemBal").val(RemBal);

            }
        }
        else {

            //if (subAmount > RemBal && subAmount > 0) {

            //  //  $row.find('td:eq(5) input[type="Number"]').val('');
            //    $("#RemBal").val(RemBal);
            //}
            //else {

            //  $row.find('td:eq(5) input[type="Number"]').val('');
            $("#RemBal").val(RemBal);
            //  }

        }

    });
}

//function calcRemBal() {
//    var TotAmount = parseInt($("#Amount").val()) || 0;
//    // var Bal = 0;
//    var RemBal = parseInt($("#Amount").val()) || 0;
//    $('#payTbl tbody tr').each(function (i, n) {
//        var $row = $(n);
//        var subAmount = parseInt($row.find('td:eq(5) input[type="Number"]').val()) || 0;
//        //  var subSurcharge = parseFloat($row.find('td:eq(10) input[type="Number"]').val());

//        //   console.log(subTotal);
//        //  console.log(subSurcharge);

//        //  $field = $row.find('td:eq(9) input[type="Number"]');
//        //console.log("subTotal : " + subTotal);
//        if (subAmount > RemBal && subAmount > 0) {

//            // alert("1");
//            if ($tableItemCounter <= 0) {
//                //   alert("1.1");
//                // alert("table greater" + $tableItemCounter);
//                $("#RemBal").val(TotAmount);
//                $row.find('td:eq(5) input[type="Number"]').val(TotAmount);
//            }
//            else {
//                //  alert("1.2");
//                // alert("table small" + $tableItemCounter);
//                $("#RemBal").val(RemBal);
//                $row.find('td:eq(5) input[type="Number"]').val('');
//            }

//            //$field.val($field.attr("max"));
//            //  toastr.warning('Receiving Amount must be equal or less than balance!')
//            //  subTotal = parseFloat($row.find('td:eq(9) input[type="Number"]').val());
//        }
//        else if (subAmount > 0 && subAmount <= RemBal) {
//            //  alert("2");
//            if ($.isNumeric(subAmount)) {
//                //   alert("2.1");
//                RemBal -= subAmount;
//                $("#RemBal").val(RemBal);

//                // total += parseFloat(subTotal);
//                //  $('#Amount').val(total + surcharge);
//            }
//        }
//        else {
//            // alert("3");

//            if (subAmount > RemBal && subAmount > 0) {
//                //  alert("3.1");
//                $row.find('td:eq(5) input[type="Number"]').val('');
//                $("#RemBal").val(RemBal);
//            }
//            else {
//                //  alert("3.2");
//                // alert("else");
//                $row.find('td:eq(5) input[type="Number"]').val('');
//                $("#RemBal").val(RemBal);
//            }

//        }


//        //else if ($.isNumeric(subAmount)) {
//        //    RemBal -= subAmount;
//        //    $("#RemBal").val(RemBal);

//        //   // total += parseFloat(subTotal);
//        //  //  $('#Amount').val(total + surcharge);
//        //}
//        //if ($.isNumeric(subSurcharge)) {
//        //    surcharge += parseFloat(subSurcharge);
//        //    $('#Amount').val(total + surcharge);
//        //}

//    });
//}






/////////////////////////


function getVendorDetails(accountId, branchID) {
    //console.log(accountId);

    var controlPaying = '<input type = "Number" class="form-control">';

    ajaxCall("GET", { "accountId": accountId, "BranchID": branchID }, "application/json; charset=utf-8", "/VendorPayment/getVendorDetail",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        $('#tbl').find('tbody').empty();
        if (ien > 0) {

            var controlPaying = '<input type = "Number"  value=0>';
            var html = '';

            for (var i = 0; i < ien; i++) {
                var num = (data.qry[i].PurchaseDate).match(/\d+/g);
                var date = new Date(parseFloat(num));
                //console.log("date" +date);
                var month = parseInt(date.getMonth()) + 1;
                var dateString = date.getDate() + "/" + month + "/" + date.getFullYear();
                //console.log(dateString +"dstring");
                var totalAmount = parseFloat(data.qry[i].TotalAmount);
                var expense = parseFloat(data.qry[i].Expenses || 0);
                var returnAmount = parseFloat(data.qry[i].ReturnAmount || 0);
                var VAT = parseFloat(data.qry[i].VAT || 0);
                var invoiceTotal = parseFloat(totalAmount + VAT).toFixed(3);
                var balance = parseFloat(invoiceTotal - expense - data.qry[i].TotalPaid - returnAmount).toFixed(3);
                var status = data.qry[i].PaymentStatus;
                if (status == null || status == "") {
                    status = "";
                }


                html += '<tr>';
                //html+='<td>' + <input type=checkbox >+ '</td>';
                html += '<td><input type="hidden" value="' + data.qry[i].OrderID + '">' + data.qry[i].POID + '</td>';
                html += '<td>' + data.qry[i].InvoiceNo + '</td>';
                html += '<td><input type="hidden" value="' + data.qry[i].AccountID + '">' + data.qry[i].AccountName + '</td>';
                html += '<td>' + dateString + '</td>';
                html += '<td>' + invoiceTotal + '</td>';
                html += '<td>' + data.qry[i].TotalPaid + '</td>';
                html += '<td>' + status + '</td>';
                html += '<td>' + balance + '</td>';
                html += '<td><input type = "Number"  min="0" max=' + balance + ' step="0" class="form-control"></td> <span id="Error"></span>';

                html += '</tr>';
            }
            $('#tbl tbody').append(html);
        }
    }
    function onFailure(err) {
        console.log("ERROR");
    }

}

$("#PayTypeID").on("change", function () {
    var payStatusSelection = $("#PayTypeID").val();

    // console.log(payStatusSelection);

    var totalAmount = $("#TotalAmount").val();
    if (payStatusSelection == 1) {
        //document.getElementById("Bank").readOnly = true;
        $('#Bank').val(0).trigger('change.select2');
        $("#Bank").prop("disabled", true);
        $('#ChequeDate').val("");
        $('#ChequeNumber').val("");
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;
    }
    else if (payStatusSelection == 2) {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        $('#ChequeDate').val("");
        $('#ChequeNumber').val("");
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;
    }
    else if (payStatusSelection == 3) {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeDate").readOnly = false;
        document.getElementById("ChequeNumber").readOnly = false;
    }

    else if (payStatusSelection == 4)// chq in hand
    {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", true);
        $('#Bank').val(0).trigger('change.select2');
        document.getElementById("ChequeDate").readOnly = false;
        document.getElementById("ChequeNumber").readOnly = false;
    }

    else if (payStatusSelection == 5)// others
    {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeDate").readOnly = false;
        document.getElementById("ChequeNumber").readOnly = false;
    }
});
//$("#PayTypeID").on("change", function () {
//    var payStatusSelection = $("#PayTypeID").val();
//    var totalAmount = $("#TotalAmount").val();
//    if (payStatusSelection == 1) {
//        //document.getElementById("Bank").readOnly = true;
//        $("#Bank").prop("disabled", true);
//        document.getElementById("ChequeDate").readOnly = true;
//        document.getElementById("ChequeNumber").readOnly = true;        
//    }    
//    else if (payStatusSelection == 2) {
//        $("#Bank").prop("disabled", false);
//        //document.getElementById("Bank").readOnly = false;
//        document.getElementById("ChequeDate").readOnly = true;
//        document.getElementById("ChequeNumber").readOnly = true;
//    }
//    else if (payStatusSelection == 3) {
//        //document.getElementById("Bank").readOnly = false;
//        $("#Bank").prop("disabled", false);
//        document.getElementById("ChequeDate").readOnly = false;
//        document.getElementById("ChequeNumber").readOnly = false;
//    }

//});
function calcTotal() {
    var total = 0;
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('td:eq(8) input[type="Number"]').val());
        $field = $row.find('td:eq(8) input[type="Number"]')
        //console.log("subTotal : " + subTotal);
        if (subTotal > Number($field.attr("max"))) {
            $field.val($field.attr("max"));
            toastr.warning('Paying Amount must be equal or less than balance!')
            subTotal = parseFloat($row.find('td:eq(9) input[type="Number"]').val());
        }
        if ($.isNumeric(subTotal)) {
            total += parseFloat(subTotal);
            $('#Amount').val(parseFloat(total).toFixed(3));
            $("#RemBal").val(total);
        }

        //console.log(subTotal);
        //console.log(total);
    });
    // alert(total + "in calctotal");
    $('#tbl tfoot tr').find('td:eq(9) input[type="text"]').val(total);
    //$('#TotalPaying').val(total);    
}
$('#tbl').keyup(function (e) {
    calcTotal();
});
function insert() {
    var MultiPayments = [];
    var rows = [];
    var payLog = [];
    var JEntryLog = [];
    var totalAmount = 0;
    var BranchID = $("#BranchID").val();
    var totalPaying = $('#tbl tfoot tr').find('td:eq(9) input[type="text"]').val();
    //var totalPaying = $('#TotalPaying').val();
    //console.log("Total Paying"+totalPaying);
    var orderId = 0;
    var vendorID = $('#AccountID option:selected').val();
    var desc = $('#Description').val();
    var payType = $('#PayTypeID option:selected').val()
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var voucherDate = $('#VoucherDate').val();
    //var chqDate = $('#ChequeDate').val();
    var chqNumber = $('#ChequeNumber').val();
    var amount = parseFloat($('#Amount').val());
    //alert(totalPaying);
    //alert(amount);
    if (totalPaying == amount) {
        $('#tbl tbody tr').each(function (i, n) {

            var $row = $(n);
            orderId = $row.find('td:eq(0) input[type="hidden"]').val();
            var accountId = $row.find('td:eq(2) input[type="hidden"]').val();
            var balance = $row.find('td:eq(7)').html();
            var paying = $row.find('td:eq(8) input[type="Number"]').val();

            if (orderId != "" && balance != "" && paying != "") {

                totalAmount += parseFloat(paying);
                balance = parseFloat(balance) - parseFloat(paying);
                //console.log('orderId:' + orderId + ', accountId: ' + accountId + ', balance:' + balance + ', paying: ' + paying);
                //console.log('total: ' + totalAmount);

                rows.push({
                    OrderID: orderId,
                    Paying: paying,
                    Balance: balance
                });
                JEntryLog.push({
                    //BranchID : BranchID,
                    Amount: paying,
                    OrderTypeID: 1,
                    EntryTypeID: 1,
                    OrderID: orderId,
                    isReversed: false
                });
            }
        });

        //////////////////////
        $('#payTbl tbody tr').each(function (i, n) {

            var $row = $(n);
            //var orderID = $row.find('input[id*="OrderID"]').val();
            var PayTypeID = $row.find('input[id*="PayTypeID"]').val();
            var ID = $row.find('td:eq(1) input[type="hidden"]').val();
            //  var PayTypeID = $row.find('select[name=payType] option:selected').val();
            //var orderId = $row.find('td:eq(0)').html();
            //  var BankID = $row.find('input[id*="AccountID"]').val();
            var BankID = $row.find('input[id*="BankID"]').val();
            var chqDate = $row.find("#chqDate").text();
            var chqNumber = $row.find("#chqNumber").text();
            var Amount = parseFloat($row.find("#paymentAmount").text());


            //console.log(ID);
            //console.log(PayTypeID);
            //console.log(BankID);
            //console.log(chqDate);
            //console.log(chqNumber);
            //console.log(Amount);


            if (ID != "" && PayTypeID != "" && Amount != "" && Amount > 0) {

                if (PayTypeID == 1) {
                    BankID = null;
                    chqDate = null;
                    chqNumber = null;
                }
                else if (PayTypeID > 1) {
                    if (PayTypeID == 2) {
                        chqDate = null;
                    }


                }

                MultiPayments.push({
                    ID: orderId,
                    PayTypeID: PayTypeID,
                    BankID: BankID,
                    chqDate: chqDate,
                    chqNumber: chqNumber,
                    Amount: Amount


                });


            }
        });

        /////////////////////
        ////////////////////////
        //$('#payTbl tbody tr').each(function (i, n) {

        //    var $row = $(n);
        //    //var orderID = $row.find('input[id*="OrderID"]').val();
        //    var ID = $row.find('td:eq(1) input[type="hidden"]').val();
        //    var PayTypeID = $row.find('select[name=payType] option:selected').val();
        //    //var orderId = $row.find('td:eq(0)').html();
        //    //  var BankID = $row.find('input[id*="AccountID"]').val();
        //    var BankID = $row.find('select[name=BID] option:selected').val();
        //    var chqDate = $row.find('input[id*="chqDate"]').val();
        //    var chqNumber = $row.find('input[id*="chqNumber"]').val();
        //    var Amount = parseFloat($row.find('td:eq(5) input[type="Number"]').val());


        //    //console.log(ID);
        //    //console.log(PayTypeID);
        //    //console.log(BankID);
        //    //console.log(chqDate);
        //    //console.log(chqNumber);
        //    //console.log(Amount);


        //    if (ID != "" && PayTypeID != "" && Amount != "" && Amount > 0) {

        //        if (PayTypeID == 1) {
        //            BankID = null;
        //            chqDate = null;
        //            chqNumber = null;
        //        }
        //        else if (PayTypeID > 1) {
        //            if (PayTypeID == 2) {
        //                chqDate = null;
        //            }


        //        }

        //        MultiPayments.push({
        //            ID: 0,
        //            PayTypeID: PayTypeID,
        //            BankID: BankID,
        //            chqDate: chqDate,
        //            chqNumber: chqNumber,
        //            Amount: Amount


        //        });


        //    }
        //});

        ///////////////////////
        if (rows.length && MultiPayments.length) {

            var data = {

                //'VoucherName': 'Payment',
                'Description': desc,
                'Amount': amount,
                'BranchID': BranchID,
                'AccountID': vendorID,
                'VoucherDate': voucherDate,
                'PaymentTypeID': payType,
                'BankName': bank,
                'ChequeDate': chqDate,
                'ChequeNumber': chqNumber,
                'Details': rows
            };

            var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog, 'bankAccId': 0, 'MultiPayments': MultiPayments });
            //console.log(json);            
            ajaxCall("POST", json, "application/json; charset=utf-8", "/VendorPayment/SavePaymentMulti", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result == "success") {
                    //  window.location.href = '/VendorPayment/Details?isNew=true&id=' + vendorID;
                    window.location.href = '/VendorPayment/GetVendorPaymentReportFilterWise';
                }
                else {
                    if (Result == "false") {
                        $('#btnSave').prop('disabled', false);
                        uiUnBlock();
                        swal("Error!", "Please Check Amount! ", "error");
                    }
                    else if (Result == "chqDate") {
                        $('#btnSave').prop('disabled', false);
                        uiUnBlock();
                        swal("Error!", "Please Check Cheque Dates! ", "error");
                    }
                    else if (Result == "chqNumber") {
                        $('#btnSave').prop('disabled', false);
                        uiUnBlock();
                        swal("Error!", "Please Check Cheque Numbers! ", "error");
                    }
                    else if (Result == "chqInHand") {
                        $('#btnSave').prop('disabled', false);
                        uiUnBlock();
                        swal("Error!", "Please Check Cheque Numbers and Chq Dates! ", "error");
                    }

                    else if (Result == "bank") {
                        $('#btnSave').prop('disabled', false);
                        uiUnBlock();
                        swal("Error!", "Please Check Bank Accounts! ", "error");
                    }



                    else {
                        $('#btnSave').prop('disabled', false);
                        uiUnBlock();
                        swal("Error!", "Please Check Your Entries! ", "error");
                    }

                    //swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                }
                //location.reload();
                //window.location.href = 'Index';
                //alert("success");
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    location.reload();
                //console.log(error);
                //location.reload();
            }
        }
        else {
            swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            //var data = {

            //    //'VoucherName': 'Payment',
            //    'Description': desc,
            //    'Amount': amount,
            //    'AccountID': vendorID,
            //    'VoucherDate': voucherDate,
            //    'PaymentTypeID': payType,
            //    'BankName': bank,
            //    'ChequeDate': chqDate,
            //    'ChequeNumber': chqNumber,
            //};
            //JEntryLog.push({
            //        //BranchID : BranchID,
            //        Amount: paying,
            //        OrderTypeID: 1,
            //        OrderID: orderId,
            //        EntryTypeID: 1,
            //        isReversed: false
            //    });
            //var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog, 'bankAccId': bankAccountId });
            ////console.log(json);            
            //ajaxCall("POST", json, "application/json; charset=utf-8", "/VendorPayment/SavePayment", "json", onSuccess, onFailure);
            //function onSuccess(Result) {
            //    if (Result == "success") {
            //        uiUnBlock();
            //        //window.location.href = '/VendorPayment/VendorStatement?AccountID=' + vendorID;
            //        //window.location.href = '/VendorPayment/Details?isNew=true&id=' + vendorID;
            //        window.location.href = '/VendorPayment';
            //    }
            //    else {
            //        uiUnBlock();
            //        $('#btnSave').prop('disabled', false);
            //        swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            //    }
            //    //location.reload();
            //    //window.location.href = 'Index';
            //    //alert("success");
            //}
            //function onFailure(error) {
            //    if (error.statusText == "OK") {
            //        //alert("success");
            //        //console.log(error);
            //        window.location.reload();                    
            //    }
            //}
        }
    }
    else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);

        swal("Total", "Total Paying Must be Equal to Total Amount of Payment/Cheque!", "error");
    }
}

