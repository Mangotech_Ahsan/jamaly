﻿var controlId = 0;
var products;
var pID = 0;
$(document).ready(function () {
        var vehCodeID = $('#ddlVehCode').val();
        
        //The url we will send our get request to
        var valueUrl = '@Url.Action("GetValuesStkMove", "Product")';
        // if (vehCodeID > 0) {
        $('#ddlPartNo').select2(
          {
              placeholder: 'Enter Product',
              //Does the user have to enter any data before sending the ajax request
              minimumInputLength: 0,
              allowClear: true,
              ajax: {
                  //How long the user has to pause their typing before sending the next request
                  delay: 200,
                  //The url of the json service
                  url: '/Product/GetValuesStkMove',
                  dataType: 'json',
                  async: true,
                  //Our search term and what page we are on
                  data: function (params) {
                      return {
                          pageSize: 1500,
                          pageNum: params.page || 1,
                          searchTerm: params.term,
                          //Value from client side.
                          countyId: $('#ddlVehCode').val()
                      };
                  },
                  processResults: function (data, params) {
                      params.page = params.page || 1;
                      return {
                          results: $.map(data.Results, function (obj) {
                              return { id: obj.ProductID, text: obj.PartNo };
                          }),
                          pagination: {
                              more: (params.page * 1500) <= data.Total
                          }
                      };
                  }
              }
          });
    });
$(function () {
    $('#btnSave').click(function () {        
        insert();
    })
})
$("#ddlVehCode").change(function () {    
    var vehCodeID = $('#ddlVehCode').val();    
    //if (vehCodeID == "")
    //{ vehCodeID = -1; }
    //var json = { "vehCodeId": vehCodeID };
    //$.ajax({
    //    type: "POST",
    //    contentType: "application/json; charset=utf-8",
    //    url: '/Product/getProducts',
    //    async: true,
    //    data: JSON.stringify(json),
    //    success: function (data) {            
    //        GetDropdown1("ddlPartNo", data, true);
    //    },
    //    error: function (err) { console.log(err); }
    //});
});
 
// Get  Stock in All Branches by selected Product
function getProductStock(productID) {

    ajaxCall("GET", { "iProductID": productID }, "application/json; charset=utf-8", "/Stock/getStockForUpdatePrice",
        "json", onSuccess, onFailure);    
    function onSuccess(data) {
        getProducts();
        var total = 0;
        var ien = data.qry.length;
        $('#tbl').find('tbody').empty();
        if (ien > 0) {            
            var html = '';           
            for (var i = 0; i < ien; i++) {
                var BranchSelect = '<select id="ddlBranch' + controlId + '" class="form-control"></select>';
                var prodID = data.qry[i].ProductID;
                var stockID = data.qry[i].StockID;
                var vehCode = (data.qry[i].VehicleCode); 
                var partNo = (data.qry[i].PartNo);
                var branchID = (data.qry[i].BranchID);
                var costPrice = (data.qry[i].CostPrice);
                var salePrice = (data.qry[i].SalePrice);
                html += '<tr>';                
                html += '<td><input type="hidden" id="hdnstockID" value="' + stockID + '">' + vehCode + '</td>';
                html += '<td><input type="hidden" id="hdnpID" value="' + prodID + '">' + partNo + '</td>';             
                html += '<td>' + qty + '</td>';
                html += '<td>' + costPrice + '</td>';
                html += '<td><input id="hdnSPrice" type = "Number" value ="' + salePrice + '" min="0""></td>';
                html += '</tr>';               
                controlId += 1;           
            }
            $('#tbl tbody').append(html);
            //GetBranchDropdown(popDdl, products, branchID);
            GetBranchDropdown();
            
        }
    }
    function onFailure(err) {
        console.log(err);
    }
}
// Get Branch Lists for dropdown
function getProducts(){           
    ajaxCall("POST", {}, "application/json; charset=utf-8", "/Stock/getBranches", "json", onSuccess);
    function onSuccess(data) {   
        products = data;        
    }    
    function onFailure(err) {
        console.log(err);
    }    
}

function checkNull(value) {
    return JSON.stringify(value).replace(/null/g, "\"\"");
}
// Prevent user to enter quantity more than avaialable qty 
//$('#tbl').keyup(function (e) {
//    $field = $(e.target).closest("tr").find('input[id*="movingQty"]');
//    var movingQty = $(e.target).closest("tr").find('input[id*="movingQty"]').val();    
//    if (movingQty > Number($field.attr("max"))) {
//        var max = $field.attr("max");
//        $field.val(0);
//        toastr.warning('Moving quantity must be equal or less than Balance Qty!')
//    }
//    $('#btnSave').prop('disabled', false);
//});

// Move Stock
function insert() {
    var updatePrice = [];    
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        var stockID = $row.find('td:eq(0)').html();        
        var newPrice = $row.find('input[id*="hdnSPrice"]').val();
        if (newPrice != "" && newPrice > 0) {        
        pID = $row.find('input[id*="hdnpID"]').val();   // can also select from main ddl                
        updatePrice.push({
                ProductId: pID,
                SrockID : location,                
                SalePrice: newPrice
            });
        }
    });
    if (stockMoving.length) {        
        var json = JSON.stringify({ 'modelStockMoving': stockMoving});
        //console.log(json);            
        ajaxCall("POST", json, "application/json; charset=utf-8", "/Stock/MoveStock", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result == "success") {
                getProductStock(pID);
                //window.location.href = '/Stock/StockMovement';
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
            //location.reload();
            //window.location.href = 'Index';
            //alert("success");
        }
        function onFailure(error) {
            if (error.statusText == "OK")               
            console.log(error);
            //location.reload();
        }
    }   
}