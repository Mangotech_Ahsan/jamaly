﻿var controlId = 0;
var products;
var pID = 0;
$(document).ready(function () {
        var vehCodeID = $('#ddlVehCode').val();
        
        //The url we will send our get request to
        var valueUrl = '@Url.Action("GetValues", "Product")';
        // if (vehCodeID > 0) {
        $('#ddlPartNo').select2(
          {
              placeholder: 'Enter Product',
              //Does the user have to enter any data before sending the ajax request
              minimumInputLength: 0,
              allowClear: true,
              ajax: {
                  //How long the user has to pause their typing before sending the next request
                  delay: 200,
                  //The url of the json service
                  url: '/Product/GetValues',
                  dataType: 'json',
                  async: true,
                  //Our search term and what page we are on
                  data: function (params) {
                      return {
                          pageSize: 1500,
                          pageNum: params.page || 1,
                          searchTerm: params.term,
                          //Value from client side.
                          countyId: $('#ddlVehCode').val()
                      };
                  },
                  processResults: function (data, params) {
                      params.page = params.page || 1;
                      return {
                          results: $.map(data.Results, function (obj) {
                              return { id: obj.ProductID, text: obj.PartNo };
                          }),
                          pagination: {
                              more: (params.page * 1500) <= data.Total
                          }
                      };
                  }
              }
          });
    });
$(function () {
    $('#btnSave').click(function () {
        insert();
    })
})
$("#ddlVehCode").change(function () {    
    var vehCodeID = $('#ddlVehCode').val();     
});
// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
// Get  Stock in All Branches by selected Product
function getProductStock(productID) {
   // console.log(productID, location);
    ajaxCall("GET", { "iProductID": productID }, "application/json; charset=utf-8", "/StockManagement/getStockDetail",
        "json", onSuccess, onFailure);    
    function onSuccess(data) {
        getProducts();
        var total = 0;
        var ien = data.qry.length;
        $("#tblStock").dataTable().fnDestroy();
        $('#tblStock').find('tbody').empty();
        if (ien > 0) {            
            var html = '';           
            for (var i = 0; i < ien; i++) {                
                var prodID = data.qry[i].ProductID;
                var onMove = 0;
                if (data.qry[i].OnMove > 0)
                {
                    onMove = data.qry[i].OnMove;
                }
                var BalanceQty = parseFloat(data.qry[i].Qty - data.qry[i].OnMove);
                var vehCode = (data.qry[i].VehicleCode);                
                var location = (data.qry[i].Location);                
                //var vehModel = checkNull(data.qry[i].VehicleName);
                var partNo = (data.qry[i].PartNo);           
                var description = checkNull(data.qry[i].Description);
                var branch = (data.qry[i].BranchName);
                var branchID = (data.qry[i].BranchID);
                var qty = (data.qry[i].Qty);
                var costPrice = (data.qry[i].CostPrice);
                var salePrice = (data.qry[i].SalePrice);
                var levelID = data.qry[i].LevelID;
                var openUnitCode = data.qry[i].OpenUnitCode;
                var leastUnitCode = data.qry[i].LeastUnitCode;
                var unitPerCarton = data.qry[i].UnitPerCarton;
                html += '<tr>';
                html += '<td><input type="hidden" id="hdnCPrice" value="' + costPrice + '">' + vehCode + '</td>';
                html += '<td><input type="hidden" id="hdnpID" value="' + prodID + '">' + partNo + '</td>';
                html += '<td><input type="hidden" id="hdnBranchID" value="' + branchID + '">' + branch + '</td>';
                html += '<td><input type="hidden" id="hdnSPrice" value="' + salePrice + '">' + qty + '</td>';
                html += '<td>' + onMove + '</td>';
                html += '<td><input type="hidden" id="hdnLocation" value="' + location + '">' + BalanceQty + '</td>';
                html += '<td hidden>' + location + '</td>';                             
                html += '<td><input type="hidden" id="hdnLevelID" value="' + levelID + '"><input id="movingQty" name="movingQty" onkeypress="return isNumberKey(event)" type = "Number"  min="0" step="0" class="form-control"></td> <span id="Error"></span>';
                                
                html += '<td><input id="excessQty" name="excessQty" onkeypress="return isNumberKey(event)" type = "Number"  min="0"  step="0" class="form-control"></td> <span id="Error"></span>';
                html += '<td hidden><input id="newLocation" name="newLocation" type = "text"  class="form-control"></td>';
                html += '<td><input type="hidden" id="hdnUnitPerCtn" value="' + unitPerCarton + '"><input id="reason" name="reason" type = "text"  class="form-control"></td> <span id="Error"></span>';
                html += '</tr>';
                var subQty = qty;
                total += parseInt(subQty);
                $('#totalQty').val(total);
            }
            $('#tblStock tbody').append(html);
            $('#tblStock').dataTable({
                dom: 'Bt',
                paging: false,
                bFilter: false,
                ordering: false,
                searching: false,
                buttons: [
                    {
                        extend: 'print',
                        messageTop: function () {                           
                            return 'Stock Adjustment Report';                           
                        },
                        messageBottom: 'Powered By: Mangotech Solutions',
                        exportOptions: {
                            columns: [0,1,2 ,3, 4, 5, 6, 7,8,9,10]
                        }
                    }
                ]
            });
        }
    }
    function onFailure(err) {
        console.log(err);
    }
}
// Get Branch Lists for dropdown
function getProducts(){           
    ajaxCall("POST", {}, "application/json; charset=utf-8", "/Stock/getBranches", "json", onSuccess);
    function onSuccess(data) {   
        products = data;        
    }    
    function onFailure(err) {
        console.log(err);
    }    
}

function checkNull(value) {
    return JSON.stringify(value).replace(/null/g, "\"\"");
}
// Prevent user to enter quantity more than avaialable qty 
$('#tblStock').keyup(function (e) {
    //$field = $(e.target).closest("tr").find('input[id*="movingQty"]');
    //$fieldExcess = $(e.target).closest("tr").find('input[id*="movingQty"]');
    //var movingQty = parseInt($(e.target).closest("tr").find('input[id*="movingQty"]').val());    
    //if (movingQty > Number($field.attr("max"))) {
    //    var max = $field.attr("max");
    //    $field.val(0);
    //    toastr.warning('Adjustment quantity must be equal or less than Balance Qty!')
    //}
    //var excessQty = parseInt($(e.target).closest("tr").find('input[id*="excessQty"]').val());
    //if (movingQty > 0) {        
    //    $field.val(0);
    //    $fieldExcess.val(0);
    //    toastr.warning('You must enter either Short or Excess!')
    //}
    $('#btnSave').prop('disabled', false);
});

// Move Stock
function insert() {    
    var stockLogOut = [];
    var isValid = false;
    $('#tblStock tbody tr').each(function (i, n) {        
        var $row = $(n);        
        var movingQty = parseInt($row.find('input[id*="movingQty"]').val()) || 0;
        var excessQty = parseInt($row.find('input[id*="excessQty"]').val()) || 0;
        var costPrice = $row.find('input[id*="hdnCPrice"]').val();
        var levelID = $row.find('input[id*="hdnLevelID"]').val();
        var unitPerCTN = $row.find('input[id*="hdnUnitPerCtn"]').val();
        var orderTypeID = 0;
        var outReference = "";
        var inReference = "";
        var minorDivisor = 1000;
        var totalQty = parseInt((movingQty));
        //console.log("leastQty=" + leastQty);
       
        var reason = $row.find('input[id*="reason"]').val();
        if (totalQty != "" && totalQty > 0) {
            //console.log("movingQty");
            isValid = true;
            orderTypeID = 11;
            outReference = 'Short Stock Adjustment';
            inReference = "";
        }
        if (excessQty != "" && excessQty > 0) {
            console.log("excessQty");
            isValid = true;
            orderTypeID = 12;
            outReference = '';
            inReference = 'Excess Stock Adjustment';
        }
       //// console.log("totalQty=" + totalQty);
        if ((movingQty != "" && movingQty > 0) || (excessQty != "" && excessQty > 0)) {
        //else{
            isValid == true;
        var branchID = $row.find('input[id*="hdnBranchID"]').val(); // Moving From 
        pID = $row.find('input[id*="hdnpID"]').val();   // can also select from main ddl        
        var costPrice = $row.find('input[id*="hdnCPrice"]').val();
        var salePrice = $row.find('input[id*="hdnSPrice"]').val();        
        var location = $row.find('input[id*="hdnLocation"]').val();
             
        stockLogOut.push({
            ProductId: pID,
            StockOUT: totalQty,
            StockIN: excessQty,
            CostPrice: parseFloat(costPrice).toFixed(3),
            SalePrice: salePrice,
            OutReference: outReference,
            InReference: inReference,
            OrderTypeID: orderTypeID,    // let check from tbl_OrderType Adjustment =6
            BranchID: branchID,
            Location: location,
            Detail: reason
        });
        }
    });
 //   console.log("isValid ="+isValid);
    if (stockLogOut.length && isValid == true) {
        // console.log("Callled");
        var json = JSON.stringify({ 'modelStockLogOut': stockLogOut });
        //console.log(json);            
        ajaxCall("POST", json, "application/json; charset=utf-8", "/StockManagement/SaveStockAdjustment", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result == "success") {
                //getProductStock(pID);
                window.location.href = '/StockManagement';
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
            //location.reload();
            //window.location.href = 'Index';
            //alert("success");
        }
        function onFailure(error) {
            if (error.statusText == "OK")
                console.log(error);
            //location.reload();
        }
    }
    else {
        swal("Error", "Please Check your entries!", "error");
    }
}