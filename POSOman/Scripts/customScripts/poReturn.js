﻿
$(function () {

    $('#btnSave').click(function () {
    $('#btnSave').prop('disabled', true);
        uiBlock();
        insert();
    });

});
var invoiceTotal = 0;
function getPurchaseDetails(orderID) {
    //console.log(accountId);

    var controlPaying = '<input type = "Number" class="form-control">';

    ajaxCall("GET", { "orderId": orderId }, "application/json; charset=utf-8", "/PurchaseReturn/getOrderDetails",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        $('#tbl').find('tbody').empty();
        if (ien > 0) {
            var controlPaying = '<input type = "Number" class="form-control" value=0>';
            var html = '';

            for (var i = 0; i < ien; i++) {      

                html += '<tr>';
                //html+='<td>' + <input type=checkbox >+ '</td>';
                html += '<td><input type="hidden" value="' + data.qry[i].OrderID + '">' + data.qry[i].POID + '</td>';
                html += '<td>' + data.qry[i].InvoiceNo + '</td>';
                html += '<td><input type="hidden" value="' + data.qry[i].AccountID + '">' + data.qry[i].AccountName + '</td>';
                html += '<td>' + data.qry[i].PurchaseDate + '</td>';
                html += '<td>' + data.qry[i].TotalAmount + '</td>';                
                html += '<td>' + data.qry[i].Status + '</td>';                
                html += '</tr>';

            }

            $('#tbl tbody').append(html);

        }
    }
    function onFailure(err) {
        debugger;
    }

}
function calcTotal() {
    var total = 0;
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('td:eq(8) input[type="Number"]').val());
        if ($.isNumeric(subTotal)) {
            total += parseFloat(subTotal);
        }
        //console.log(subTotal);
        //console.log(total);
    });
    $('#TotalPaying').val(total);
}
$('#tbl').keyup(function (e) {
    calcTotal();
});
function insert() {
    var rows = [];
    var totalAmount = 0;
    var totalPaying = $('#TotalPaying').val();
    //console.log("Total Paying"+totalPaying);

    var vendorID = $('#AccountID option:selected').val();
    var desc = $('#Description').val();
    var payType = $('#PayTypeID option:selected').val()
    var bank = $('#Bank').val();
    var voucherDate = $('#VoucherDate').val();
    var chqDate = $('#ChequeDate').val();
    var chqNumber = $('#ChequeNumber').val();
    var amount = parseFloat($('#Amount').val());
    if (totalPaying == amount) {
        $('#tbl tbody tr').each(function (i, n) {

            var $row = $(n);
            //var orderId = $row.find('td:eq(0)').html();
            var orderId = $row.find('td:eq(0) input[type="hidden"]').val();
            var accountId = $row.find('td:eq(2) input[type="hidden"]').val();
            var balance = $row.find('td:eq(7)').html();
            var paying = $row.find('td:eq(8) input[type="Number"]').val();

            if (orderId != "" && balance != "" && paying != "") {

                totalAmount += parseFloat(paying);
                balance = parseFloat(balance) - parseFloat(paying);
                //console.log('orderId:' + orderId + ', accountId: ' + accountId + ', balance:' + balance + ', paying: ' + paying);
                //console.log('total: ' + totalAmount);

                rows.push({
                    OrderID: orderId,
                    Paying: paying,
                    Balance: balance
                });
                //price = parseFloat(price) + parseFloat(perPieceExp);

            }
            //else if (paying == "") {
            //    isValid = false;
            //    alert("Please enter paying amount");
            //}        
            //else {
            //    alert("Error!!!");
            //}
        });
        if (rows.length) {

            var data = {

                //'VoucherName': 'Payment',
                'Description': desc,
                'Amount': amount,
                'AccountID': vendorID,
                'VoucherDate': voucherDate,
                'PaymentTypeID': payType,
                'BankName': bank,
                'ChequeDate': chqDate,
                'ChequeNumber': chqNumber,
                'Details': rows
            };

            var json = JSON.stringify({ 'model': data });
            //console.log(json);
            console.log(json);
            ajaxCall("POST", json, "application/json; charset=utf-8", "/VendorPayment/SavePayment", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                //console.log("Success");                
                uiUnBlock();
                //alert("success");
                swal("Succesful", "Paid successfully!", "error");
            }
            function onFailure(error) {
                uiUnBlock();
                $('#btnSave').prop('disabled', false);
                if (error.statusText == "OK")
                    alert("success");
                //console.log(error);
                //location.reload();
            }

        }
        else {
            var data = {

                //'VoucherName': 'Payment',
                'Description': desc,
                'Amount': amount,
                'AccountID': vendorID,
                'VoucherDate': voucherDate,
                'PaymentTypeID': payType,
                'BankName': bank,
                'ChequeDate': chqDate,
                'ChequeNumber': chqNumber,
            };

            var json = JSON.stringify({ 'model': data });
            //console.log(json);
            console.log(json);
            ajaxCall("POST", json, "application/json; charset=utf-8", "/VendorPayment/SavePayment", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                //console.log("Success");
                alert("success");
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    alert("success");
                //console.log(error);
                //location.reload();
            }
        }
    }
    else {
        alert("Total Paying Must be Equal to Total Amount of Payment/Cheque");
    }
}

