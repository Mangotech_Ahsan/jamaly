﻿var chqDat = "";
var AccountID = 0;
var soPaymentTypeID = 0;
var globalSettleAmount = 0;
$(function () {
    $("#Bank").prop("disabled", true);
    $("#SettlementAmounts").prop("disabled", true);    

    document.getElementById("ChequeNumber").readOnly = true;
    document.getElementById("ChequeDate").readOnly = true;
    $("#btnSearch").on("click", function () {
        //AccountID = $('#AccountID option:selected').val();
        AccountID = $('#hdnAccountID').val();

        getCustomerDetails(AccountID);       
    });

    

    $('#btnSave').click(function () {
        //var customerID = $('#AccountID option:selected').val();
        var customerID = $('#hdnAccountID').val();
        var voucherDate = $('#VoucherDate').val();        
        var BranchID = $('#BranchID option:selected').val();
        chqDate = $('#ChequeDate').val();

        if (chqDate == "") {
            //alert("Please cheque date");
            chqDat = "";
            //console.log(PurchaseDate+"date");
        }
        if (customerID == "" || customerID == 0) {
            swal("Vendor", "Please Select Customer! ", "error");
            //alert("Please Select Vendor");
        }
        else if (voucherDate == "") {
            swal("Date", "Please Enter Payment Date! ", "error");
            //alert("Please Enter Payment Date");
            //console.log(PurchaseDate + "date");
        }
        else {
            $('#btnSave').prop('disabled', true);
        uiBlock();
            insert();
        }
    });

});
$("#SettlementAmounts").change(function () {

    var amount = +$("#SettlementAmounts option:selected").text() || 0;
    if (amount > 0) {
        $("#SettleAmount").val(amount);
        globalSettleAmount = amount;
    }
    else {
        $("#SettleAmount").val(0);
        globalSettleAmount = 0;
    }

    $("#btnSearch").click();
})
function getSettlementAmountCustomerIDWise(cusID) {
    console.log(cusID);
    if (cusID != '' && cusID != null && cusID > 0) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/CustomerPayment/GetSettlementAmountIDWise/?AccountID=' + cusID+"&type=customer",
            async: true,
            success: function (data) {
                {
                    console.log(data);
                    if (data != null && data.length > 0) {
                        $("#SettlementAmounts").prop("disabled", false);

                        GetDropdown1("SettlementAmounts", data, true)
                    }
                    else {
                        $("#SettlementAmounts").prop("disabled", true);    

                    }
                    $("#SettleAmount").val(0);

                }
            },
            error: function (err) {
                console.log(err);
                $("#SettlementAmounts").prop("disabled", true); 
                $("#SettleAmount").val(0);
            }
        });
    }
    else{
        $("#SettlementAmounts").prop("disabled", true);
        $('#SettlementAmounts').val('').trigger('change.select2');
        $('#SettleAmount').val(0);
    }
}

var invoiceTotal = 0;
function getCustomerDetails(accountId) {
    $('#customerID').val(accountId).trigger('change.select2');
    $("#customerID").prop("disabled", true);    
    document.getElementById('ChequeDate').valueAsDate = new Date();
    document.getElementById('VoucherDate').valueAsDate = new Date();
    var controlPaying = '<input type = "Number" class="form-control">';
    ajaxCall("GET", { "accountId": accountId }, "application/json; charset=utf-8", "/CustomerPayment/getCustomerDetail",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        $("#tbl").dataTable().fnDestroy();
        $('#tbl').find('tbody').empty();        
        if (ien > 0) {
            var html = '';
            for (var i = 0; i < ien; i++) {
                var num = (data.qry[i].SalesDate).match(/\d+/g);
                var date = new Date(parseFloat(num));
                //console.log("date" +date);
                var month = parseInt(date.getMonth()) + 1;
                var dateString = date.getDate() + "/" + month + "/" + date.getFullYear();
                //console.log(dateString +"dstring");
                var totalAmount = parseFloat(data.qry[i].TotalAmount);
                var VAT = parseFloat(data.qry[i].VAT || 0);
                //var invoiceTotal = parseFloat(totalAmount + VAT).toFixed(3);
                var invoiceTotal = parseFloat(totalAmount ).toFixed(2);
                var totalPaid = parseFloat(data.qry[i].TotalPaid)
                var returnAmount = parseFloat(data.qry[i].ReturnAmount || 0)
                var Balance = parseFloat(invoiceTotal - totalPaid - returnAmount).toFixed(2);
                var status = data.qry[i].PaymentStatus;
                soPaymentTypeID = data.qry[i].PaymentTypeID;
                ////////////////////////
                var discount = parseFloat(data.qry[i].DiscountAmount);
                //var expense = parseFloat(data.qry[i].Expenses || 0);
                var adjustment = parseFloat(data.qry[i].Adjustment);
                var WHT = parseFloat(data.qry[i].Adjustment);
                var adjustmentDesc = data.qry[i].AdjustmentDescription;
                var vatType = data.qry[i].VATType || null;
                var vatPercent = data.qry[i].TaxPercent;
                ///////////////////////



                if (status == null || status == "") {
                    status = "";
                }
                html += '<tr>';
                //html+='<td>' + <input type=checkbox >+ '</td>';
                html += '<td id=OrderID><input type="checkbox" id="chkboxForCopyAmount" onclick=copy_data(this)><input type="hidden" value="' + data.qry[i].OrderID + '">' + data.qry[i].SOID + '</td>';
                html += '<td id=PONo hidden>' + data.qry[i].PONo + '</td>';
                html += '<td><input id="AccountID" type="hidden" value="' + data.qry[i].AccountID + '">' + data.qry[i].AccountName + '</td>';
                html += '<td>' + dateString + '</td>';
                html += '<td>' + invoiceTotal + '</td>';
                html += '<td>' + data.qry[i].TotalPaid + '</td>';
                html += '<td>' + returnAmount + '</td>';
                html += '<td>' + status + '</td>';
                html += '<td id=Balance>' + parseFloat(Balance).toFixed(2) + '</td>';
                if (discount > 0) {
                    html += '<td>' + discount + '</td>';

                }
                else {
                    html += '<td><input readonly type = "Number" value="0" min="0" max=' + Balance + ' step="0" class="form-control" style="width:90px"></td>';

                }
                if (adjustment > 0) {
                    html += '<td>' + adjustment + '</td>';
                }
                else {
                    html += '<td><input readonly type = "Number" value="0" min="0" max=' + Balance + ' step="0" class="form-control" style="width:90px"></td>';
                }
                if (WHT > 0) {
                    html += '<td>' + WHT + '</td>';
                }
                else {
                    html += '<td><input type = "Number" value="0" min="0" max=' + Balance + ' step="0" class="form-control" style="width:90px"></td>';
                }
                if (adjustmentDesc !== "noDesc") {
                    html += '<td>' + adjustmentDesc + '</td>';
                }
                else {
                    html += '<td><input readonly type = "text" class="form-control"></td>';
                }

                if (vatType !== "noType") {
                    html += '<td><select><option selected value = ' + vatType + '>' + vatType + '</option></select></td>';
                }
                else {

                    //html += '<td><select><option value="GST">GST</option><option value="WHT">WHT</option></select></td>';
                    html += '<td><select><option value="GST">GST</option></select></td>';
                }

                if (VAT > 0) {
                    html += '<td>' + vatPercent + '</td>';
                }
                else {
                    html += '<td><input type = "Number" value="0" min="0" max=' + Balance + ' step="0" class="form-control" style="width:90px"></td>';
                }

                html += '<td><input id="balanceAm" type = "Number" value=' + Balance + ' readonly step="0" class="form-control" style="width:150px"><button hidden type="button" onclick="copy_data(this)" class="btn btn-primary btn-sm btn-round copy_data">copy</button></td>';
                html += '<td><input id="ReceivingAmount" type = "Number"  min="0" max=' + Balance + ' step="0" class="form-control"></td> <span id="Error"></span>';

                html += '</tr>';
            }
            $('#tbl tbody').append(html);
            
        }
        
        
    }
    function onFailure(err) {
        console.log(err);
    }

}

function copy_data(data) {
    
    //console.log("inside")
    var $row = $(data).closest("tr");    // Find the row
    //console.log(data);
    //console.log($row);
    var isChecked = $row.find('input[id*="chkboxForCopyAmount"]').is(":checked");
    if (isChecked) {
        var BalAm = +$row.find('input[id*="balanceAm"]').val(); // Find the text
        //var $BalAm = $row.find("#balanceAm").text(); // Find the text
        if (BalAm > 0) {
            $row.find('input[id*="ReceivingAmount"]').val(BalAm);
        }
        else {
            $row.find('input[id*="ReceivingAmount"]').val(0);
        }
        
    }
    else {
        $row.find('input[id*="ReceivingAmount"]').val(0);
    }
    calcTotal();
    console.log(isChecked);
    
    // Let's test it out
    //console.log(BalAm);
};

$("#PayTypeID").on("change", function () {
    var payStatusSelection = $("#PayTypeID").val();
    var totalAmount = $("#TotalAmount").val();
    if (payStatusSelection == 1) {
        //document.getElementById("Bank").readOnly = true;
        $("#Bank").prop("disabled", true);
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;        
    }
    else if (payStatusSelection == 2) {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;        
    }
    else if (payStatusSelection == 3) {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeDate").readOnly = false;
        document.getElementById("ChequeNumber").readOnly = false;
    }
});

function calcTotal() {
    
    var SettleEntryAmount = +$("#SettleAmount").val() || 0;
    var SettleEntryID = +$("#SettlementAmounts").val() || 0;

    if (SettleEntryAmount >= 0 && SettleEntryID) {
        var total = 0;
        $('#tbl tbody tr').each(function (i, n) {
            
            var $row = $(n);
            //var subTotal = parseFloat($row.find('td:eq(15) input[type="Number"]').val());
            var subTotal = +$row.find('td:eq(15) input[type="Number"]').val() || 0;
            var balance = +$row.find('td:eq(8)').text() || 0;
            var pay = +$row.find('td:eq(16) input[type="Number"]').val() || 0;
            
            //$field = $row.find('td:eq(9) input[type="Number"]')
           
            //$field = $row.find('td:eq(8)').text();

            //if (pay > SettleEntryAmount) {
            //    +$row.find('td:eq(16) input[type="Number"]').val(0);

            //    toastr.warning('Receiving Amount must be equal or less than settle amount!');
            //    //subTotal = parseFloat($row.find('td:eq(15)').text());
            //    $('#SettleAmount').val(parseFloat(SettleEntryAmount).toFixed(2));

            //}

            if (subTotal < pay) {
                +$row.find('td:eq(16) input[type="Number"]').val(0);

                toastr.warning('Receiving Amount must be equal or less than balance!');
                subTotal = parseFloat($row.find('td:eq(15)').text());
            }
            if ($.isNumeric(subTotal)) {
                if (globalSettleAmount < (total+pay)) {
                    +$row.find('td:eq(16) input[type="Number"]').val(0);
                    $('#SettleAmount').val(parseFloat(SettleEntryAmount).toFixed(2));
                }
                var discount = +$row.find('td:eq(9) input[type="Number"]').val() || 0;
                var adjustments = +$row.find('td:eq(10) input[type="Number"]').val() || 0;
                var WHT = +$row.find('td:eq(11) input[type="Number"]').val() || 0;
                var adjustmentDesc = $row.find('td:eq(12) input[type="text"]').val() || 0;
                var taxPercent = +$row.find('td:eq(14) input[type="Number"]').val() || 0;
                var paying = +$row.find('td:eq(16) input[type="Number"]').val() || 0;
                var taxAmount = taxPercent / 100 * (balance);
                var totBal = parseFloat(balance - (discount + WHT + taxAmount) + adjustments).toFixed(2);
                $row.find('td:eq(15) input[type="Number"]').val(totBal);
                
                total += parseFloat(paying);
                $('#Amount').val(parseFloat(total).toFixed(2));

                $('#SettleAmount').val(parseFloat(globalSettleAmount - total).toFixed(2));
            }
        });
        $('#tbl tfoot tr').find('td:eq(16) input[type="text"]').val(parseFloat(total).toFixed(2));
        $('#txtTotal').val(parseFloat(total).toFixed(2));
    }
    else {
        var total = 0;
        $('#tbl tbody tr').each(function (i, n) {
            var $row = $(n);
            //var subTotal = parseFloat($row.find('td:eq(15) input[type="Number"]').val());
            var subTotal = +$row.find('td:eq(15) input[type="Number"]').val() || 0;
            var balance = +$row.find('td:eq(8)').text() || 0;
            var pay = +$row.find('td:eq(16) input[type="Number"]').val() || 0;
            console.log($row.find("td").eq(0).text())
            console.log($row.find("td").eq(1).text())
            console.log($row.find("td").eq(2).text())
            console.log($row.find("td").eq(3).text())
            console.log($row.find("td").eq(4).text())
            console.log($row.find("td").eq(5).text())
            console.log($row.find("td").eq(6).text())
            console.log($row.find("td").eq(7).text())
            console.log($row.find("td").eq(8).text())
            console.log($row.find('td:eq(9) input[type="Number"]').val())
            console.log($row.find('td:eq(10) input[type="Number"]').val())
            console.log($row.find('td:eq(11) input[type="Number"]').val())
            console.log($row.find('td:eq(13) :selected').text())
            //console.log($row.find("td").eq(10).val())
            //console.log($row.find("td").eq(11).val())
            //console.log($row.find("td").eq(12).text())
            console.log($row.find("td").eq(14).text());
            console.log("Payinggggggg->", +$row.find('td:eq(15) input[type="Number"]').val())
            console.log(+$row.find('td:eq(16) input[type="Number"]').val())
            //console.log($row.find("td").eq(14).text())
            //console.log($row.find("td").eq(15).val())
            $field = $row.find('td:eq(9) input[type="Number"]')
            //console.log("subTotal : " + subTotal);
            //if (subTotal > Number($field.attr("max"))) {
            //    $field.val($field.attr("max"));
            //    toastr.warning('Receiving Amount must be equal or less than balance!')
            //    subTotal = parseFloat($row.find('td:eq(9) input[type="Number"]').val());
            //}
            $field = $row.find('td:eq(8)').text();

            // if (subTotal > Number($field.attr("max"))) {
            //if (subTotal > Number($field)) {
            //    //$field.val($field.attr("max"));
            //    $field.val(Number($field));
            //    toastr.warning('Receiving Amount must be equal or less than balance!')
            //    subTotal = parseFloat($row.find('td:eq(8)').text());
            //}

            if (subTotal < pay) {
                //$field.val($field.attr("max"));
                +$row.find('td:eq(16) input[type="Number"]').val(0);

                toastr.warning('Receiving Amount must be equal or less than balance!')
                subTotal = parseFloat($row.find('td:eq(15)').text());
            }
            console.log('subtotal-->' + subTotal);
            if ($.isNumeric(subTotal)) {
                console.log("inside ")
                var discount = +$row.find('td:eq(9) input[type="Number"]').val() || 0;
                var adjustments = +$row.find('td:eq(10) input[type="Number"]').val() || 0;
                var WHT = +$row.find('td:eq(11) input[type="Number"]').val() || 0;
                var adjustmentDesc = $row.find('td:eq(12) input[type="text"]').val() || 0;
                var taxPercent = +$row.find('td:eq(14) input[type="Number"]').val() || 0;
                var paying = +$row.find('td:eq(16) input[type="Number"]').val() || 0;
                var taxAmount = taxPercent / 100 * (balance);
                var totBal = parseFloat(balance - (discount + WHT + taxAmount) + adjustments).toFixed(2);
                console.log("paying->" + paying);
                $row.find('td:eq(15) input[type="Number"]').val(totBal);
                console.log("paying->" + paying);
                //total += parseFloat(subTotal);
                total += parseFloat(paying);
                $('#Amount').val(parseFloat(total).toFixed(2));
            }
        });
        //$('#tbl tfoot tr').find('input[id*=txtTotal]').val(total);
        $('#tbl tfoot tr').find('td:eq(16) input[type="text"]').val(parseFloat(total).toFixed(2));
        $('#txtTotal').val(parseFloat(total).toFixed(2));
    }
    
}

$('#tbl').keyup(function (e) {
    calcTotal();
});

function insert() {
    var rows = [];
    var JEntryLog = [];
    var totalAmount = 0;    
    //var totalPaying = $('#tbl tfoot tr').find('td:eq(9) input[type="text"]').val();
    var totalPaying = $('#tbl tfoot tr').find('td:eq(16) input[type="text"]').val();
    //var totalPaying = $('#TotalPaying').val();
    //console.log("Total Paying"+totalPaying);

    //var AccountID = $('#AccountID option:selected').val();
    var desc = $('#Description').val();
    var payType = $('#PayTypeID option:selected').val()
    //var bank = $('#Bank').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var voucherDate = $('#VoucherDate').val();
    //var chqDate = $('#ChequeDate').val();
    var chqNumber = $('#ChequeNumber').val();
    var amount = parseFloat($('#Amount').val());

    if (totalPaying == amount) {
        $('#tbl tbody tr').each(function (i, n) {

            var $row = $(n);
            //var orderID = $row.find('input[id*="OrderID"]').val();
            var OrderID = $row.find('td:eq(0) input[type="hidden"]').val();
            var PONO = $row.find("#PONO").text();
            //var orderId = $row.find('td:eq(0)').html();
            var AccountID = $row.find('input[id*="AccountID"]').val();
            //var Balance = $row.find("#Balance").text();
            var Receiving = $row.find('input[id*="ReceivingAmount"]').val();
            ///

            var discount = parseFloat($row.find('td:eq(9) input[type="Number"]').val()).toFixed(2) || 0;
            var totBal = parseFloat(+$row.find('td:eq(15) input[type="Number"]').val()).toFixed(2) || 0;
            var adjustments = +$row.find('td:eq(10) input[type="Number"]').val() || 0;
            var WHT = +$row.find('td:eq(11) input[type="Number"]').val() || 0;
            var adjustmentDesc = $row.find('td:eq(12) input[type="text"]').val() || 0;
            var taxPercent = parseFloat($row.find('td:eq(14) input[type="Number"]').val()).toFixed(2) || 0;
            var taxType = $row.find('td:eq(13) :selected').text();
            //var paying = +$row.find('td:eq(15) input[type="Number"]').val() || 0;

            var taxAmount = parseFloat(taxPercent / 100 * (totBal)).toFixed(2);
            console.log("tax Amount->" + taxAmount)
            //if (OrderID != "" && Balance != "" && Receiving != "" && Receiving > 0) {
            if (OrderID != "" && totBal != "" && Receiving != "" && Receiving > 0) {

                totalAmount += parseFloat(Receiving);
                Balance = parseFloat(totBal) - parseFloat(Receiving);

                rows.push({
                    OrderID: OrderID,
                    Receiving: Receiving,
                    Balance: totBal,
                    Discount: discount === "NaN" ? 0 : discount,
                    Tax: taxAmount === "NaN" ? 0 : taxAmount,
                    TaxType: taxType,
                    TaxPercent: taxPercent === "NaN" ? 0 : taxPercent,
                    Adjustment: adjustments,
                    WHT: WHT,
                    AdjustmentDescription: adjustmentDesc
                });
                JEntryLog.push({
                    //BranchID : BranchID,
                    Amount: Receiving,
                    OrderTypeID: 3,
                    EntryTypeID: 2,
                    OrderID: OrderID,
                    isReversed: false
                });
                
            }
        });
        if (rows.length) {

            var data = {

                //'VoucherName': 'Payment',
                'Description': desc,
                'Amount': amount,
                'AccountID': AccountID,
                'VoucherDate': voucherDate,
                'PaymentTypeID': payType,
                'BankName': bank,
                'ChequeDate': chqDate,
                'ChequeNumber': chqNumber,
                'JEntryID': +$("#SettlementAmounts").val()||null,
                'Details': rows
            };           

            var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog, 'bankAccId': bankAccountId });
            //console.log(json);
            ajaxCall("POST", json, "application/json; charset=utf-8", "/CustomerPayment/SavePayment", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result > 0) {                    
                    window.location.href = '/CustomerPayment/Voucher?isNew=true&id=' + Result;
                }
                else {
                    $('#btnSave').prop('disabled', false);
                    uiUnBlock();
                    swal("Error!", "Please Check Your Entries! " + Result.toString(), "error");
                    //alert("Some error Ocurred! Please Check Your Entries");
                }
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    location.reload();
            }
        }
        else {
            var data = {
                //'VoucherName': 'Payment',
                'Description': desc,
                'Amount': amount,
                'AccountID': AccountID,
                'VoucherDate': voucherDate,
                'PaymentTypeID': payType,
                'BankName': bank,
                'ChequeDate': chqDate,
                'ChequeNumber': chqNumber,
            };
            JEntryLog.push({
                //BranchID : BranchID,
                Amount: amount,
                OrderTypeID: 3,
                EntryTypeID: 2,
                OrderID: orderId,
                isReversed: false
            });
            var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog, 'bankAccId': bankAccountId });
            //console.log(json);            
            ajaxCall("POST", json, "application/json; charset=utf-8", "/CustomerPayment/SavePayment", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result == "success") {                    
                    window.location.href = '/CustomerPayment/Details?isNew=true&id=' + AccountID;
                }
                else {
                    uiUnBlock();
                    $('#btnSave').prop('disabled', false);                    
                    swal("Error!", "Please Check Your Entries! ", "error");
                }
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    window.location.reload();
            }
        }
    }
    else {
        uiUnBlock();
        swal("Total!", "Total Receiving Must be Equal to Total Amount of Receiving/Cheque! ", "error");
        //alert("Total Receiving Must be Equal to Total Amount of Receiving/Cheque");
    }
}

