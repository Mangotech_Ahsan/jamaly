﻿$("#btnPrint").click(function (e) {
    e.preventDefault();
    accID = $('#hdnAccountID').val();
    var isValid = true;
    var PONo = $('#PONO').val();
    var SalesDate = $('#SaleDate').val();
    var paymentStatus = $('#PaymentStatus option:selected').val();
    var rowCount = $('#tblProduct tbody tr').length;
    var PaymentTypeID = $('#PaymentType option:selected').val();
    var BranchID = $('#BranchID option:selected').val();
    var amountPaid = $("#amountPaid").val();    
    if (PONo != "") {
        // check duplication
    }
    if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Customer", "Please Select Customer!", "error");
    }
    else if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Sales Date!", "error");
        //console.log(PurchaseDate + "date");
    }
    else if (rowCount == 0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");
        //console.log(PurchaseDate + "date");
    }
    else if (paymentStatus == 0) {
        isValid = false;
        swal("Payment Status", "Please Select Payment Status!", "error");
    }
    else if ((paymentStatus == 1 || paymentStatus == 2) && (PaymentTypeID == "" || PaymentTypeID == undefined)) {
        isValid = false;
        swal("Payment Type", "Please Select Payment Type!", "error");
    }
    else if ((paymentStatus == 2) && (amountPaid == "" || amountPaid == 0)) {
        isValid = false;
        swal("Amount Paid!", "Please Enter Amount!", "error");
    }
    else if (isValid == true) {
        printOrder();
    }
});
function printOrder() {
    var saleDetails = [];
    var total = 0;
    var qtyVE = 0;
    invoiceAmount = $('#subAmount').val();
    //if (totalInvoice == "" || totalInvoice == 0) {
    var paidAmount = $('#amountPaid').val();
    //console.log(totalInvoice);
    var vat = parseFloat($('#vatAmount').val());
    var exp = parseFloat($('#expenseInput').val());
    if (vat == "" || isNaN(vat)) {
        vat = 0;
        $('#vatInput').val(0);
    }
    //  get Table DAta 
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);

        var pId = $row.find('input[id*="productID"]').val();
        var partNumber = $row.find("td").eq(2).text();
        var Location = $row.find("#Location").text();
        var Qty = $row.find("#ProductQty").text();
        var SalePrice = $row.find("#ProductSalePrice").text();
        var SubTotal = $row.find("#ProductSubTotal").text();

        saleDetails.push({
            PartNo: partNumber,
            ProductId: pId,
            Location: Location,
            Qty: Qty,
            IsReturned: false,
            ReturnedQty: 0,
            SalePrice: SalePrice,
            Total: SubTotal
        });

    });
    if (saleDetails.length) {
        var SOID = $("#hdnSOID").val();
        //total = $('#tbl tfoot tr').find('input[id*="txtTotal"]').val();
        //console.log("Prod Total" + total);
        //total = $('#Total').val()
        var paymentStatus = 0;
        if ($('#PaymentStatus option:selected').val() != 0) {
            paymentStatus = $('#PaymentStatus option:selected').text()
        }
        var SaleAmount = parseFloat(total + vat).toFixed(3);
        var data = {
            'HoldID': SOID,
            'QuoteID': $('#hdnQuoteID').val(),
            'AccountID': $('#hdnAccountID').val(),
            'BranchID': $('#BranchID option:selected').val(),
            'PONO': $('#PONO').val(),
            'PaymentStatus': $('#PaymentStatus').val(),
            'SalesDate': $('#SaleDate').val(),
            'PaymentStatus': paymentStatus,
            'PaymentTypeID': $('#PaymentType option:selected').val(),
            'VAT': vat,
            'TotalAmount': invoiceAmount,
            'DiscountAmount': $('#discInput').val(),
            'AmountPaid': $('#amountPaid').val(),
            'TotalPaid': $('#amountPaid').val(),
            'QuoteDetails': saleDetails
        };
        
        var json = JSON.stringify({ 'model': data });
        //console.log(json);
        ajaxCall("POST", json, "application/json; charset=utf-8", "/SalesOrder/HoldPrintOrder", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (parseInt(Result) > 0) {                
                //window.location.href = '/Quotation/HoldOrders';                              
                window.open('/SalesOrder/printOrder?id='+parseInt(Result)+'');               
            }
            else {
                alert("Some error Ocurred! Please Check Your Entries");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                console.log("OKError");
            }
            else {
                alert("Some error Occured check your entries!");
            }
        }
    }
}
//function print() {   
//    var saleDetails = [];
//    var stockLog = [];
//    var total = 0;
//    var qtyVE = 0;
//    invoiceAmount = $('#subAmount').val();
//    //if (totalInvoice == "" || totalInvoice == 0) {
//    var paidAmount = $('#amountPaid').val();
//    //console.log(totalInvoice);
//    var vat = parseFloat($('#vatAmount').val());
//    var exp = parseFloat($('#expenseInput').val());
//    if (vat == "" || isNaN(vat)) {
//        vat = 0;
//        $('#vatInput').val(0);
//    }
//    //  get Table DAta 
//    $('#tblProduct tbody tr').each(function (i, n) {
//        var $row = $(n);
//        var pId = $row.find('input[id*="productID"]').val();
//        var partNumber = $row.find("td").eq(2).text();
//        var Location = $row.find("#Location").text();
//        var Qty = $row.find("#ProductQty").text();
//        var SalePrice = $row.find("#ProductSalePrice").text();
//        var SubTotal = $row.find("#ProductSubTotal").text();

//        saleDetails.push({
//            PartNo: partNumber,
//            ProductId: pId,
//            Location: Location,
//            Qty: Qty,
//            IsReturned: false,
//            ReturnedQty: 0,
//            SalePrice: SalePrice,
//            Total: SubTotal
//        });        
//    });
//    if (saleDetails.length) {        
//        var htmlData;
//        var paymentStatus = 0;
//        if ($('#PaymentStatus option:selected').val() != 0) {
//            paymentStatus = $('#PaymentStatus option:selected').text()
//        }
//        var SaleAmount = parseFloat(total + vat).toFixed(2);
//        var data = {
//            'AccountID': $('#hdnAccountID').val(),
//            'QuoteID': $('#hdnQuoteID').val(),
//            'BranchID': $('#BranchID option:selected').val(),
//            'PONO': $('#PONO').val(),
//            'PaymentStatus': $('#PaymentStatus').val(),
//            'SalesDate': $('#SaleDate').val(),
//            'PaymentStatus': paymentStatus,
//            'PaymentTypeID': $('#PaymentType option:selected').val(),
//            'VAT': vat,
//            'TotalAmount': invoiceAmount,
//            'DiscountAmount': $('#discInput').val(),
//            'AmountPaid': $('#amountPaid').val(),
//            'SaleDetails': saleDetails
//        };
//        var json = JSON.stringify({ 'model': data });
//        //console.log(json);
//        ajaxCall("POST", json, "application/json; charset=utf-8", "/SalesOrder/printOrder", "json", onSuccess, onFailure);
//        function onSuccess(Result) {
//            if (Result == "success") {
//                $("#dialogcontent").html(Result);
//                console.log(Result);
//            }
//            else {
//                alert("Some error Ocurred! Please Check Your Entries");
//            }
//        }
//        function onFailure(error) {
//            if (error.statusText == "OK") {
//                $("#dialogcontent").html(data);
//                window.open('@Url.Action("printOrder", "SalesOrder")');
//            }
//            else {
//                alert("Some error Occured check your entries!");
//            }
//        }
        
//       // window.location.href = '/SalesOrder/printOrder?model=' + data;
        
//        //window.open('@Url.Action("printOrder", "SalesOrder", ,@Html.Raw(Json.Encode("' + data + '"),new { target="_blank" })');
//        //@Html.ActionLink("printOrder", "SalesOrder", "model", data, null);
        
//    }
    
//}