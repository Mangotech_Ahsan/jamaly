﻿var isExist = false;
function PostNewProduct(btnClicked) {
    uiBlock();
    var $form = $(btnClicked).parents('form');
    
    if ($form.valid()) {
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize(),
            error: function (xhr, status, error) {
                //do something about the error
            },
            success: function (response) {
                var $summaryUl = $(".validation-summary-valid").find("ul");
                if (response == true) {
                    $summaryUl.text("Already Exists!");
                }
                else {
                    $summaryUl.text("");
                    $('#newProductModal').modal('hide');
                    $('#newProductContainer').html("");
                    getProducts(vehCodeID);
                }
                uiUnBlock();
            }
        });

    } else {
        console.log('inv');
    }  

    return false;// if it's a link to prevent post
}
function PostNewGroup(btnClicked) {
    uiBlock();
    var $form = $(btnClicked).parents('form');
    if ($form.valid()) {
    $.ajax({
        type: "POST",
        url: $form.attr('action'),
        data: $form.serialize(),
        error: function (xhr, status, error) {
            alert("error");
        },
        success: function (response) {
            var $summaryUl = $(".validation-summary-valid").find("ul");
            if (response == true) {                
                $summaryUl.text("Already Exists!");                
            }
            else {
                $summaryUl.text("");
                $('#newGroupModal').modal('hide');
                $('#newGroupContainer').html("");
            }
            uiUnBlock();
        }
    });
    } else {
        console.log('inv');
    }
}
function PostNewVehicle(btnClicked) {
    uiBlock();
    var $form = $(btnClicked).parents('form');
    if ($form.valid()) {
    $.ajax({
        type: "POST",
        url: $form.attr('action'),
        data: $form.serialize(),
        error: function (xhr, status, error) {
            //do something about the error
        },
        success: function (response) {
            var $summaryUl = $(".validation-summary-valid").find("ul");
            if (response == true) {
                $summaryUl.text("Already Exists!");
            }
            else {
                $summaryUl.text("");
                $('#newVehicleModal').modal('hide');
                $('#newVehicleContainer').html("");
            }
            uiUnBlock();
        }
    });
    } else {
        console.log('inv');
    }   
}

function PostEditVehicle(btnClicked) {
    uiBlock();
    var $form = $(btnClicked).parents('form');    
    //$('.preloader').fadeIn('slow');
    if ($form.valid()) {
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize(),
            error: function (xhr, status, error) {
                //do something about the error
            },
            success: function (response) {
                if (response == true) {
                    var $summaryUl = $(".validation-summary-valid").find("ul");
                    $summaryUl.text("Already Exists!");
                    //$summaryUl.append($("<li>").text("Already Exists!"));
                }
                else {
                    $('#editProductModal').modal('hide');
                    $('#editProductContainer').html("");
                }
                uiUnBlock();
                //$('.preloader').fadeOut('slow');
            }       
    });
    } else {
        console.log('inv');
    }
}
$("#btnCreateProduct").on("click", function () {
    //$('#spinner').fadeOut('slow');
    uiBlock();
    var url = $(this).data("url");

    $.get(url, function (data) {
        $('#newProductContainer').html(data);
        //$('#spinner').fadeOut('slow');
        
        $('#newProductModal').modal('show');
    });
    uiUnBlock();
});
function editProduct(input) {
    uiBlock();
    var tr = $(input).closest('tr');
    var pId = $(tr).find('td:eq(1) :selected').val();
    //event.preventDefault();

    var url = '/PartialProduct/Edit/?id=' + pId;
    $.get(url, function (data) {
        $('#editProductContainer').html(data);
        $('#editProductModal').modal('show');
        //debugger;
    });
    uiUnBlock();
}
$("#btnCreateGroup").on("click", function () {
    uiBlock();
    var url = $(this).data("url");

    $.get(url, function (data) {
        $('#newGroupContainer').html(data);

        $('#newGroupModal').modal('show');
    });
    uiUnBlock();
});
$("#btnCreateVehicle").on("click", function () {
    uiBlock();
    var url = $(this).data("url");
    
    $.get(url, function (data) {
        $('#newVehicleContainer').html(data);
        
        $('#newVehicleModal').modal('show');
        uiUnBlock();
    });
    
});
function CheckAvailability(form) {       
    var PartNo = $("#PartNumber").val();
    var $summaryUl = $(".validation-summary-valid").find("ul");    
            $.ajax({
                type: "POST",
                url: "/PartialProduct/CheckPartNO",
                data: '{PartNo: "' + PartNo + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response == true) {
                        $summaryUl.append($("<li>").text("Already Exists!"));                        
                    }
                    else {
                        $.ajax({
                            type: "POST",
                            url: form.attr('action'),
                            data: form.serialize(),
                            error: function (xhr, status, error) {
                                //do something about the error
                            },
                            success: function (response) {
                                $('#newProductModal').modal('hide');
                                $('#newProductContainer').html("");
                                
                            }
                        });
                    }
                }                
            });   
        };
 
function ClearMessage() {
    $("#message").html("");
};
