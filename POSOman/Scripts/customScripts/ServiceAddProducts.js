﻿$tableItemCounter = 0;
$addedProductIDs = [];
$selCustID = 0;
var StockCheck = 0;
CostPriceCheck = 0;
var vc = '';
$(document).keypress(
    function (event) {
        if (event.which == '13') {
            event.preventDefault();
        }
    });
document.getElementById("Packet").readOnly = true;
$('#Qty').on('input', function (e) {
    var qty = $('#Qty').val();
    var unitPerCarton = $('#unitPerCarton').val();
    calcSubTotal();
    //calcTotalWeight();
});
var validate = function (e) {
    var t = e.value;
    e.value = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 4)) : t;
}
$('#SalePrice').on('input', function (e) {
    calcSubTotal();
});
$('#SalePrice').change(function () {
    var price = parseFloat($('#SalePrice').val()) || 0;
    var costPrice = parseFloat($('#hdnCostPrice').val()) || 0;
    if (price < costPrice) {
        swal("Error", "The sale price is below cost price of this item! " + costPrice + "PKR", "error");
        // $("#SalePrice").val('');
    }
});
function calcSubTotal() {
    //console.log("unitPerCarton=" + unitPerCarton);
    var ctnQty = parseFloat($('#Qty').val() || 0);
    var ctnPrice = parseFloat($('#SalePrice').val());
    //var qty = totalQty;
    var qty = ctnQty;
    var price = ctnPrice;
    if (($.isNumeric(price)) && ($.isNumeric(qty))) {
        var amount = parseFloat(qty * price);
        $('#SubTotal').val(parseFloat(amount).toFixed(2));
        calcTotal();
    }
    else {
        $('#subAmount').val(parseFloat(amount).toFixed(2));
    }
}

$("#ddlVehCode").change(function () {
    var vehCodeID = $('#ddlVehCode').val();
    getProducts(vehCodeID);
});

$("#CustomerCode").change(function () {
    var customerID = $('#CustomerCode').val();
    $('#hdnAccountID').val(customerID);
    $('#AccountID').val(customerID).trigger('change.select2');
    $('#CustomerPhone').val(customerID).trigger('change.select2');
    getCustomerDetail(customerID);
});
$("#CustomerPhone").change(function () {
    var customerID = $('#CustomerPhone').val();
    $('#hdnAccountID').val(customerID);
    $('#AccountID').val(customerID).trigger('change.select2');
    $('#CustomerCode').val(customerID).trigger('change.select2');
    getCustomerDetail(customerID);
});
$("#ddlPartNumber").change(function () {
    var pId = $('#ddlPartNumber').val();
    if (pId != "" || pId > 0) {
        getDetail(pId);
        calcSubTotal();
    }


});
// When data is edited in table
//$("#tblProduct").bind("DOMSubtreeModified", function () {
//    var Qty = $row.find("#ProductQty").text();
//    console.log(Qty);
//});
//$("#tblProduct").on('update', function () {
//    alert('Table updated, launching kill space goats sequence now.')
//});

//$("#tblProduct").focusout(function () {
//    var total = 0;
//    $('#tblProduct tbody tr').each(function (i, n) {
//        var $row = $(n);        
//        var qty = $row.find("#ProductQty").text();        
//        var price = $row.find("#ProductSalePrice").text();       
//        if (($.isNumeric(price)) && ($.isNumeric(qty))) {


//            if (qty > StockCheck) { swal("Stock", "Stock Not Available ! Total Stock is " + StockCheck, "warning"); }
//            else if(price < CostPriceCheck) { swal("Warning", "Sale Price must be greater than cost price ! " + CostPriceCheck + " PKR", "warning"); }

//            var amount = (qty * price);
//            $row.find("#ProductSubTotal").text(parseFloat(amount).toFixed(2));            
//            var subTotal = parseFloat($row.find('#ProductSubTotal').text());
//            total += parseFloat(subTotal);
//            calcTotal();
//        }
//        //if (price < CostPriceCheck) { swal("Error", "Sale Price must be greater than cost price ! " + CostPriceCheck + " PKR", "error"); }
//        //else if (qty > StockCheck) { swal("Error", "Stock Not Available ! " + StockCheck, "error"); }
//        //else {
//        //    {
//        //        var amount = (qty * price);
//        //        $row.find("#ProductSubTotal").text(parseFloat(amount).toFixed(3));
//        //        var subTotal = parseFloat($row.find('#ProductSubTotal').text());
//        //        total += parseFloat(subTotal);
//        //        calcTotal();
//        //    }
//        //}
//    });       
//});


function calcTotal() {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('#ProductSubTotal').text());
        total += parseFloat(subTotal);
    });
    $("#subAmount").val(parseFloat(total).toFixed(2));
    $("#totalAmount").val(parseFloat(total).toFixed(2));
    $("#finalAmountWithVAT").val(parseFloat(total).toFixed(2));
    //calcDiscount();
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(total).toFixed(2);
    }
    //var customerBalance = $('#customerBalance').val();
    var creditLimit = parseFloat($('#customerCrLimit').val()) - parseFloat($('#customerBalance').val());
    if (creditLimit != "" && total > creditLimit) { swal("Credit Limit", "You can not sale more than Credit Limit! ", "error"); }

}
$("#addRow").click(function () {
    var vehCode = $("#ddlVehCode :selected").text();  // 1
    var ProductID = $("#ddlPartNumber").val(); // hidden
    var PartNO = $("#ddlPartNumber :selected").text();  // 1
    var Description = $("#Description").text(); // 2
    var UnitCode = $("#unitCode").val();
    var Qty = $("#Qty").val();
    var Packet = $("#Packet").val();
    var costPrice = parseFloat($('#hdnCostPrice').val()) || 0;
    var SalePrice = $("#SalePrice").val(); // 7
    var SubTotal = $("#SubTotal").val();
    var stock = parseFloat($('#Stock').val());
    var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';
    var isPack = '<input type="hidden" id="isPacket" value="' + $("#isPacket").val() + '"/>';
    var ctnPrice = parseFloat($('#SalePrice').val());

    var packetID = 0;
    var cPrice = '<input type="hidden" id="costPrice" value="' + costPrice + '"/>';
    //console.log("COGS=" + costPrice);
    if (ProductID > 0 && ((parseFloat(Qty)) > 0) && Number(parseFloat(SalePrice)) && SubTotal > 0) {
        var index = $.inArray(ProductID, $addedProductIDs);
        if (index >= 0) {
            swal("Error", "Product Already Added!", "error");
        } else {
            //editable
            // with Box and Carton
            //var markup = "<tr><td><input type='checkbox' name='record'></td><td hidden>" + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td id=unitCode hidden>" + isPack + UnitCode + "</td><td>"+cPrice+"<input id='pQty' name='pQty' type = 'number' oninput='isNumberKey(this)' value=" + Qty + " class='form-control' style='width: 80px;'> </td><td id='Packet' hidden>" + Packet + "</td><td id='ProductSalePrice'>" + parseFloat(SalePrice).toFixed(3) + "</td><td id='ProductSubTotal'>" + SubTotal + "</td></tr>";
            var markup = "<tr><td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deleteRow(this)'/></td><td hidden>" + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td id=unitCode hidden>" + isPack + UnitCode + "</td><td>" + cPrice + "<input id='pQty' name='pQty' type = 'number' oninput='isNumberKey(this)' value=" + Qty + " class='form-control' style='width: 80px;'> </td><td id='Packet' hidden>" + Packet + "</td><td id='ProductSalePrice'>" + parseFloat(SalePrice).toFixed(3) + "</td><td id='ProductSubTotal'>" + SubTotal + "</td></tr>";
            //$("#tblProduct tbody").append(markup);  
            $("#tblProduct tbody").prepend(markup);
            $tableItemCounter++;
            $addedProductIDs.push(ProductID);
            clearFields();
            calcTotal();
            getNewTotal();
            $('#BarCode').focus();
        }
    }
    else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
    //else if (parseInt(Qty) > stock) { swal("Error", "Stock Not Available!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
    else if (SalePrice == "" || SalePrice == "undefined" || !Number(parseFloat(SalePrice))) { swal("Error", "Please enter Sale Price!", "error"); }
});
// Find and remove selected table rows   
function remove(input) {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {
            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            var productID = row.find('input[id*="productID"]').val(); // find hidden id 
            var index = $.inArray(productID, $addedProductIDs);
            if (index >= 0) { $addedProductIDs.splice(index, 1); }
            calcTotal();
        }
    });
}
function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tblProduct").deleteRow(i);
    $tableItemCounter--;
    var row = $(r).closest("tr");
    var productID = row.find('input[id*="productID"]').val(); // find hidden id 
    var index = $.inArray(productID, $addedProductIDs);
    if (index >= 0) { $addedProductIDs.splice(index, 1); }
    calcTotal();
}
function clearFields() {
    $("#isPack").val("");
    $("#Description").text("");
    $("#BarCode").val("");
    $("#Packet").val("");
    $("#Stock").val("");
    $("#Qty").val("");
    $("#SalePrice").val("");
    $("#SubTotal").val("");
    $("#ddlVehCode").focus();
    $('#ddlVehCode').val(null).trigger('change');
    $('#ddlPartNumber').val(null).trigger('change.select2');
}
function getProducts(vehCodeID) {

    if (vehCodeID == "") { vehCodeID = -1; }
    var json = { "vehCodeId": vehCodeID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getServiceProducts',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("ddlPartNumber", data, true);
        },
        error: function (err) { console.log(err); }
    });
}
// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
//Counter to count number of entries in item table

function clearLabels() {
    $('#DescriptionTitle').text(''); $('#Description').text('');
    $('#SubsTitle').text(''); $('#SubstituteNo').text('');
    $('#GroupTitle').text(''); $('#Group').text('');
    $('#VehModelTitle').text(''); $('#VehModel').text('');
    $('#LocationTitle').text(''); $('#Location').text('');
    $('#UnitCodeTitle').text(''); $('#UnitCode').text('');
    $('#MinorUnitCodeTitle').text(''); $('#MinorUnitCode').text('');
    $('#LeastUnitCodeTitle').text(''); $('#LeastUnitCode').text('');
    $('#QtyPerUnitTitle').text(''); $('#QtyPerUnit').text('');
    $('#UnitPerCartonTitle').text(''); $('#UnitPerCarton').text('');
}
//$('#BarCode').on('change', function (e) {
//    var BarCode = $("#BarCode").val();
//    //console.log(BarCode);
//    getDetailByBarCode(BarCode);
//    //AddProductByBCode(BarCode);
//    event.preventDefault();
//});
$('#BarCode').on('input', function (e) {
    var BarCode = $("#BarCode").val();
    if (BarCode != "" || BarCode != "undefined") {
        addByBarCode(BarCode);
    }
    event.preventDefault();
});
// Get by BArCode 
function getDetailByBarCode(barCode) {
    clearLabels();
    var accID = $('#hdnAccountID').val();
    var BranchId = $('#BranchID option:selected').val();
    // get product description
    var pId = 0;
    // get product description
    //var json = { "barCode": barCode };   
    var json = { "barCode": barCode, "BranchID": BranchId };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetailByBarCode',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            if (data[0].qry.VehicleCode) {
                var vc = data.VehicleCode;
                $("#ddlVehCode option:contains(" + data[0].qry.VehicleCode + ")").attr("selected", true);
            }
            if (data[0].qry.UnitCode) {

                $('#unitCode').val(data[0].qry.UnitCode);
            }
            var qty = data[0].stock;
            $('#Stock').val(qty);
            $('#hdnCostPrice').val(data[0].costPrice);
            var salePrice = data[0].SalePrice;
            $('#hdnSalePrice').val(salePrice);
            $('#SalePrice').val(salePrice);
            if (salePrice > 0) {
                document.getElementById("SalePrice").readOnly = true;
            }
            else {
                document.getElementById("SalePrice").readOnly = false;
            }
            pId = data[0].qry.ProductID;
            //console.log("PID="+pId);
            $("#hdnProductID").val(pId);

            $('#ddlPartNumber').val(parseInt(pId)).trigger('change.select2');
            $("#Qty").val(1);
            calcSubTotal();
        },
        error: function (err) { console.log(err); }
    });
}
/// Get Product Detail with One Request
function getDetail(pId) {
    clearLabels();
    var BranchId = $('#BranchID option:selected').val();
    var accID = $('#hdnAccountID').val();
    // get product description
    //var json = { "productId": pId };
    var json = { "productId": pId, "BranchID": BranchId };
    //ajaxCall("GET", json, "application/json; charset=utf-8", "/Purchase/getProductDetail", "json", onSuccess, onFailure);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            // console.log('data=' + data[0].qry.LeastUnitCode);
            // console.log('C.p=' + data[0].costPrice);
            var unitPerCarton = 1;
            var level = 0;
            if (data[0].qry.Description) { $('#DescriptionTitle').text("Desc: "); $('#Description').text(data[0].qry.Description); }
            if (data[0].qry.SubstituteNo) { $('#SubsTitle').text("Su No.: "); $('#SubstituteNo').text(data[0].qry.SubstituteNo); }
            if (data[0].qry.GroupName) { $('#GroupTitle').text("Group: "); $('#Group').text(data[0].qry.GroupName); }
            if (data[0].qry.VehicleName) { $('#VehModelTitle').text("Model: "); $('#VehModel').text(data[0].qry.VehicleName); }
            if (data[0].qry.Location) { $('#LocationTitle').text("Location: "); $('#Location').text(data[0].qry.Location); }
            if (data[0].qry.UnitCode) { $('#UnitCodeTitle').text("Pack Unit: "); $('#UnitCode').text(data[0].qry.UnitCode); }
            if (data[0].qry.OpenUnitCode) { $('#MinorUnitCodeTitle').text("Minor Unit: "); $('#MinorUnitCode').text(data[0].qry.OpenUnitCode); }
            if (data[0].qry.LeastUnitCode) { $('#LeastUnitCodeTitle').text("Least Unit: "); $('#LeastUnitCode').text(data[0].qry.LeastUnitCode); }
            if (data[0].qry.VehicleCode) {
                var vc = data.VehicleCode;
                $("#ddlVehCode option:contains(" + data[0].qry.VehicleCode + ")").attr("selected", true);
            }

            if (data[0].qry.UnitCode) {

                $('#unitCode').val(data[0].qry.UnitCode);
            }

            var isPack = $('#isPacket').val();
            var qty = data[0].stock;
            $('#Stock').val(qty);

            $('#hdnCostPrice').val(data[0].costPrice);
            var salePrice = data[0].SalePrice;
            if (salePrice > 0) {
                document.getElementById("SalePrice").readOnly = true;

            }
            else {
                document.getElementById("SalePrice").readOnly = false;

            }
            $('#hdnSalePrice').val(salePrice);

            $('#SalePrice').val(salePrice);
            $("#Qty").val(1);
            calcSubTotal();

        },
        error: function (err) { console.log(err); }
    });
}
function addByBarCode(barCode) {
    clearLabels();
    var accID = $('#hdnAccountID').val();
    // get product description
    var pId = 0;
    // get product description
    var json = { "barCode": barCode };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductByBarCode',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            if (data) {
                $('#hdnCostPrice').val(0);
                var salePrice = data.SalePrice;
                $('#hdnSalePrice').val(salePrice);
                pId = data.ProductID;
                $("#hdnProductID").val(pId);
                var ProductID = pId; // hidden
                var PartNO = data.PartNo;  // 1            
                var Qty = 1;
                var costPrice = 0;
                var SalePrice = salePrice;
                var SubTotal = 0;
                var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';
                var isPack = '<input type="hidden" id="isPacket" value=0/>';

                if (($.isNumeric(SalePrice)) && ($.isNumeric(Qty))) {
                    var amount = parseFloat(SalePrice * Qty);
                    SubTotal = parseFloat(amount).toFixed(2);
                }
                var packetID = 0;
                var cPrice = '<input type="hidden" id="costPrice" value="' + costPrice + '"/>';

                if (ProductID > 0 && ((parseFloat(Qty)) > 0) && Number(parseFloat(SalePrice)) && SubTotal > 0) {
                    var index = $.inArray(ProductID, $addedProductIDs);
                    if (index >= 0) {
                        swal("Error", "Product Already Added!", "error");
                    } else {
                        //editable
                        // with Box and Carton
                        var markup = "<tr><td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' onclick='deleteRow(this)'/></td><td hidden></td><td>" + pid + "" + PartNO + "</td><td id=unitCode hidden></td><td>" + cPrice + "<input id='pQty' name='pQty' type = 'number' oninput='isNumberKey(this)' value=" + Qty + " class='form-control' style='width: 80px;'> </td><td id='Packet' hidden>" + Packet + "</td><td id='ProductSalePrice'>" + parseFloat(SalePrice).toFixed(2) + "</td><td id='ProductSubTotal'>" + SubTotal + "</td></tr>";
                        $("#tblProduct tbody").prepend(markup);
                        $tableItemCounter++;
                        $addedProductIDs.push(ProductID);
                        clearFields();
                        calcTotal();
                        getNewTotal();
                        $('#BarCode').focus();
                    }
                }
                else if (ProductID == "" || ProductID == "undefined") {
                    swal("Error", "Please Select Product!", "error");
                }

                else if (SalePrice == "" || SalePrice == "undefined" || !Number(parseFloat(SalePrice))) {
                    swal("Error", "Please enter Sale Price!", "error");
                    $('#ddlPartNumber').val(parseInt(pId)).trigger('change.select2');
                    $("#Qty").val(1);
                    calcSubTotal();
                }
                if (salePrice > 0) {
                    document.getElementById("SalePrice").readOnly = true;
                }
                else {
                    document.getElementById("SalePrice").readOnly = false;
                }
            }


        },
        error: function (err) { console.log(err); }
    });
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
$('#tblProduct').keyup(function (e) {

    $field = $(e.target).closest("tr").find('input[id*="pQty"]');
    var qty = $(e.target).closest("tr").find('input[id*="pQty"]').val();
    var price = parseFloat($(e.target).closest("tr").find("#ProductSalePrice").text());

    if (($.isNumeric(price)) && ($.isNumeric(qty))) {
        var amount = parseFloat(qty * price).toFixed(2);

        $(e.target).closest('tr').find("#ProductSubTotal").text(amount);
        calcTotal();
    }
    else {
        $(e.target).closest('tr').find("#ProductSubTotal").text();
    }

});
