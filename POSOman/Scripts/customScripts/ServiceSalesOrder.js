﻿var customerID = 0;
var accID = 0;
var isPONOExist = false;
var isQuote = false;
var SOID = 0;
var isLimitExceed = false;
getHoldID();
getSOID();
$("#Bank").prop("disabled", true);
//  Get New SOID 
function getSOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/getNewSOID',
        async: true,
        success: function (data) {
            {
                $("#hdnSOID").val(data);
                SOID = data;
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function getHoldID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Quotation/getLastHoldID',
        async: true,
        success: function (data) {
            {
                var value = data;
                var newValue = value.split('-');
                var increase = newValue[1];
                increase++;
                value = "H-" + increase;
                $("#hdnHoldID").val(value);
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}
$("#PONO").on("change", function () {
    var accID = $('#hdnAccountID').val();
    if (accID == "" || accID == 0) {
        swal("Customer", "Please Select Customer First!", "error");
    } else {
        var PONO = $("#PONO").val();
        checkPONO(PONO, accID);

    }
});

function checkPONO(PONO, accID) {

    var json = {
        "accountID": accID,
        "PONO": PONO
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/checkPONO',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            if (data == true) {
                toastr.warning('PONo already Exists for selected Customer!')
                $("#PONO").val("");
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}
// Get Last Sales Invoice 
$('#getLastSales').click(function () {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/getLastSalesItems',
        async: true,
        success: function (data) {
            var ien = data.qry.length;
            $('#tblProduct').find('tbody').empty();
            if (ien > 0) {
                for (var i = 0; i < ien; i++) {
                    var vehCode = data.qry[i].VehicleCode;
                    var ProductID = data.qry[i].ProductID;
                    var PartNO = data.qry[i].PartNo;
                    var Description = data.qry[i].Description;
                    var UnitCode = data.qry[i].UnitCode;
                    var costPrice = data.qry[i].UnitPrice;
                    var SalePrice = data.qry[i].SalePrice;
                    var SubTotal = data.qry[i].Total;
                    var unitPerCTN = parseInt(data.qry[i].UnitPerCarton);
                    var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';
                    var cPrice = '<input type="hidden" id="costPrice" value="' + costPrice + '"/>';
                    var unitCTN = '<input type="hidden" id="unitPerCTN" value="' + unitPerCTN + '"/>';
                    var qtyBox = parseInt(data.qry[i].Qty);
                    var qty = 0;
                    var boxQty = 0;
                    if ((qtyBox % unitPerCTN) != 0) {
                        qty = parseInt(qtyBox / unitPerCTN);
                        boxQty = qtyBox % unitPerCTN;
                    } else {
                        qty = parseInt(qtyBox / unitPerCTN);
                        boxQty = 0;
                    }
                    var Qty = qty;
                    var Box = boxQty;
                    var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td>" + Description + "</td><td id=Location hidden>" + cPrice + "-" + "</td><td>" + UnitCode + "</td><td id='ProductQty'>" + Qty + unitCTN + "</td><td id='ProductBox'>" + Box + "</td><td id='ProductSalePrice'>" + SalePrice + "</td><td id='ProductSubTotal'>" + SubTotal + "</td></tr>";
                    $("#tblProduct tbody").append(markup);
                    $tableItemCounter++;
                    $addedProductIDs.push(ProductID);
                }
                calcTotal();
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
});
$('#getQuotation').click(function () {
    var qouteID = $('#QID').val();
    var orderID = 0;
    if (qouteID != "") {
        var json = {
            "QuoteID": qouteID
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Quotation/getQuotationOrderID',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                if (data > 0) {
                    orderID = data;
                    //console.log(orderID);
                    window.location.href = '/Quotation/SubmitOrders?OrderID=' + orderID;
                } else {
                    swal("Submitted", "Quotation Already Submitted!", "error");
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    } else {
        swal("QuotationID", "Enter QuotationID!", "error");
    }


});
$('#btnHold').click(function () {
    accID = $('#hdnAccountID').val();
    var isValid = true;
    var PONo = $('#PONO').val();
    var SalesDate = $('#SaleDate').val();
    var paymentStatus = $('#PaymentStatus option:selected').val();
    var rowCount = $('#tblProduct tbody tr').length;
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //var BranchID = $('#BranchID option:selected').val();
    var BranchID = $('#hdnBranchId').val();
    var amountPaid = $("#amountPaid").val();
    if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Customer", "Please Select Customer!", "error");
    } else if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Sales Date!", "error");
        //console.log(PurchaseDate + "date");
    } else if (rowCount == 0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");
        //console.log(PurchaseDate + "date");
    } else if (isValid == true) {
        $('#btnSubmit').prop('disabled', true);
        $('#btnHold').prop('disabled', true);
        uiBlock();
        Hold();
    }
});
$('#btnSubmit').click(function () {
    accID = $('#hdnAccountID').val();
    var isPOS = $('#hdnPOS').val();

    var isValid = true;
    var PONo = $('#PONO').val();
    var SalesDate = $('#SaleDate').val();
    var paymentStatus = $('#PaymentStatus option:selected').val();
    var rowCount = $('#tblProduct tbody tr').length;
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //var BranchID = $('#BranchID option:selected').val();
    var BranchID = $('#hdnBranchId').val();
    var amountPaid = $("#amountPaid").val();
    var balance = $("#balanceAmount").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    var bankAccountId = $('#Bank option:selected').val();
    var cashRec = parseFloat($('#cashReceived').val());
    var cashRet = parseFloat($('#cashReturned').val());
    var invoiceAmount = parseFloat($('#totalAmount').val());
    if (PONo != "") {
        // check duplication
    }
    if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Customer", "Please Select Customer!", "error");
    } else if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Sales Date!", "error");
        //console.log(PurchaseDate + "date");
    } else if (rowCount == 0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");
        //console.log(PurchaseDate + "date");
    } else if (paymentStatus == 0) {
        isValid = false;
        swal("Payment Status", "Please Select Payment Status!", "error");
    } else if ((paymentStatus == 1 || paymentStatus == 2) && (PaymentTypeID == "" || PaymentTypeID == undefined)) {
        isValid = false;
        swal("Payment Type", "Please Select Payment Type!", "error");
    } else if ((paymentStatus == 2) && (amountPaid == "" || amountPaid == 0)) {
        isValid = false;
        swal("Amount Paid!", "Please Enter Amount!", "error");
    }
    else if ((PaymentTypeID == 3 || PaymentTypeID == 2) && (bankAccountId == "" || bankAccountId == undefined)) {
        isValid = false;
        swal("Bank!", "Please Select  Bank!", "error");
    }
    else if ((paymentStatus == 2) && (parseFloat(balance) > parseFloat(creditLimit - customerBalance))) {
        isLimitExceed = false;
        //console.log('Partial Paid');
        swal("Credit Limit", "Credit Limit Exceeds! ", "error");
    } else if (isLimitExceed == true) {
        //swal("Credit Limit", "You can not sale more than Credit Limit! ", "error");
        swal("Credit Limit", "Credit Limit Exceeds! ", "error");
    }
    else if ((isPOS == 1) && (cashRec == "" || cashRec == 0 || isNaN(cashRec)) && (paymentStatus != 3)) {
        isValid = false;
        swal("Cash!", "Please Enter Cash Received!", "error");
    }
    else if ((isPOS == 1) && (cashRec < invoiceAmount)) {
        isValid = false;
        swal("Cash!", "Cash Received less than Total!", "error");
    }
    else if ((isPOS != 1) && (BranchID == "" || BranchID == null)) {
        isValid = false;
        swal("Branch!", "Select Branch!", "error");
    }
    else if (isValid == true && isLimitExceed == false) {
        $('#btnSubmit').prop('disabled', true);
        $('#btnHold').prop('disabled', true);
        uiBlock();
        insert();
    }
});
// Payment Status DropDOwn, Action according to Payment Status Selection


// Check credit limit when Unpaid Selected 
function checkCreditLimit() {
    var creditLimit = $('#customerCrLimit').val();
    var balance = $('#customerBalance').val();
    if ((parseFloat(totalAmount) > parseFloat(creditLimit))) {
        isLimitExceed = false;
        swal("Credit Limit", "Credit Limit not available! ", "error");
    } else {
        isLimitExceed = false;
    }
}

function disableBank() {
    $("#Bank").prop("disabled", true);
    $("#chqNumber").prop("disabled", true);
    $("#chqDate").prop("disabled", true);
}
// Payment Type Change 
$("#PaymentType").on("change", function () {
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //Cash
    if (PaymentTypeID == 1) {
        //$("#bankName").prop("disabled", true);
        $("#Bank").prop("disabled", true);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);
    }
    // Bank
    else if (PaymentTypeID == 2) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);

    }
    // Cheque
    else if (PaymentTypeID == 3) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", false);
        $("#chqDate").prop("disabled", false);
    }
    else if (PaymentTypeID == 5) {   // Credit Card        
        var invoiceAmount = parseFloat($('#totalAmount').val());
        $('#cashReceived').val(invoiceAmount);
        //$("#cashReceived").prop("disabled", true);
    }
});

function getNewTotal() {
    applyDiscount();
    applyLoyaltyDiscount();
    calcVAT();
    calcAmountPaid();
    var subTotal = $("#subAmount").val();
    var VAT = $('#vatAmount').val();
    var discount = $('#discInput').val();
    var loyaltyDiscount = $('#redeemPoints').val();
    if (VAT == "" || isNaN(VAT)) {
        VAT = 0;
        applyZeroVAT();
    }
    if (discount == "" || isNaN(discount)) {
        discount = 0;
        applyDiscount();
    }
    if (loyaltyDiscount == "" || isNaN(loyaltyDiscount)) {
        loyaltyDiscount = 0;
        applyLoyaltyDiscount();
    }
    else {
        applyLoyaltyDiscount();
    }
    var newTotal = parseFloat(subTotal) + parseFloat(VAT) - parseFloat(discount) - parseFloat(loyaltyDiscount);
    // var amountPaid = parseFloat(subTotal) + parseFloat(VAT);

    $('#finalAmountWithVAT').val(parseFloat(newTotal).toFixed(2));
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(newTotal).toFixed(2);
    }

}
function cash() {
    var paymentStatus = $('#PaymentStatus option:selected').val();
    if (paymentStatus != 3) {
        var cashRec = parseFloat($('#cashReceived').val());
        var invoiceAmount = parseFloat($('#totalAmount').val());
        if (cashRec > 0 && cashRec >= invoiceAmount) {
            var retAmount = parseFloat(cashRec) - parseFloat(invoiceAmount);
            $('#cashReturned').val(parseFloat(retAmount).toFixed(0));
        }
        else if (cashRec < invoiceAmount) {
            swal("Short", "Recd cash must greater than total!", "error");
        }
    }

}
function applyZeroVAT() {
    var vat = parseFloat($('#vatInput').val());
    //var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        //$('#vatInput').val(0);
        vat = 0;
        $('#vatAmount').val(0);
        $('#finalAmountWithVAT').val($("#totalAmount").val());
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat($("#totalAmount").val()).toFixed(2);
        }
    }
}
function applyDiscount() {
    var discount = $('#discInput').val();
    var subTotal = $("#subAmount").val();
    if (discount == "" || isNaN(discount)) {
        //$('#discInput').val(0);
        discount = 0;
        $('#totalAmount').val(subTotal);
    } else if (parseFloat(discount) >= parseFloat(subTotal)) {
        toastr.warning('Discount must less than total!')
        $('#discInput').val(0);
        $('#totalAmount').val(subTotal);
    } else {
        var totalAmount = parseFloat(subTotal) - parseFloat(discount);
        $('#totalAmount').val(parseFloat(totalAmount).toFixed(2));

    }
    cash();

}
function applyLoyaltyDiscount() {
    var discount = $('#redeemPoints').val();
    var subTotal = $("#subAmount").val();
    if (discount == "" || isNaN(discount)) {
        //$('#discInput').val(0);
        discount = 0;
        $('#totalAmount').val(subTotal);
    } else if (parseFloat(discount) >= parseFloat(subTotal)) {
        toastr.warning('Discount must less than total!')
        $('#redeemPoints').val(0);
        $('#totalAmount').val(subTotal);
    } else {
        var totalAmount = parseFloat(subTotal) - parseFloat(discount);
        $('#totalAmount').val(parseFloat(totalAmount).toFixed(2));

    }

    cash();

}
// Get Customer Info 
function getCustomerInfo(accountID) {
    if (accountID == "") {
        accountID = -1;
    }
    var json = {
        "accountID": accountID
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Customer/getInfo',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#hdnIsLoyaltCard').val(data);
            if (data > 0) {
                $("#btnRedeem").prop("disabled", false);
                $("#redeemPoints").prop("disabled", false);
            }
            else {
                $("#btnRedeem").prop("disabled", true);
                $("#redeemPoints").prop("disabled", true);
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}
// Get Customer Balance and Credit Limit 
function getCustomerDetail(accountID) {
    if (accountID == "") {
        accountID = -1;
    }
    var json = {
        "accountID": accountID
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Customer/getDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#customerCrLimit').val(data.creditLimit);
            $('#customerBalance').val(data.Balance);
            if (data.Discount) {
                $('#customerDiscount').val(data.Discount);
            }
            else {
                $('#customerDiscount').val(0);
            }

        },
        error: function (err) {
            console.log(err);
        }
    });
}
// calculate balance if amountPaid entered and make changes every where needed
function calcAmountPaid() {
    isLimitExceed = false;
    var amountPaid = $("#amountPaid").val();
    var finalAmount = $('#finalAmountWithVAT').val();
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 2) {
        if (parseFloat(amountPaid) >= parseFloat(finalAmount)) {
            toastr.warning('Partial ! AmountPaid should be less than total amount ')
            $("#amountPaid").val(0)
        } else {
            var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
            $("#balanceAmount").val(parseFloat(balance).toFixed(2));
        }
    } else {

        var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
        $("#balanceAmount").val(parseFloat(balance).toFixed(2));
    }
}
// Calculate Discount Amount from Discount Percentage 
function calcDiscount() {
    var disc = parseFloat($('#customerDiscount').val());
    var subTotal = $("#subAmount").val();
    if (disc == "" || isNaN(disc)) { applyDiscount(); }
    else if (parseFloat(subTotal) > 0) {
        var discAmount = parseFloat((disc * subTotal) / 100);
        $('#discInput').val(parseFloat(discAmount).toFixed(2));

        getNewTotal();

    }
}
// in case of vat entered please do calculations
function calcVAT() {
    var vat = parseFloat($('#vatInput').val());
    //var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        applyZeroVAT();
    } else {
        var totalInvoice = $("#totalAmount").val();
        var vatAmount = parseFloat((vat * totalInvoice) / 100);

        $('#vatAmount').val(parseFloat(vatAmount).toFixed(2));
        var finalAmount = parseFloat(vatAmount) + parseFloat(totalInvoice);
        $('#finalAmountWithVAT').val(finalAmount);
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat(finalAmount).toFixed(2);
        }
    }
}
// if Vat then calculate and make effects on total amount
$('#vatInput').on('input', function (e) {
    getNewTotal();
});
$('#vatAmount').on('input', function (e) {
    getNewTotal();
});
// if Discount then calculate and make effects on total amount
$('#discInput').on('input', function (e) {
    getNewTotal();
});
$('#redeemPoints').on('input', function (e) {

    getNewTotal();
});
$("#amountPaid").on("change", function () {
    calcAmountPaid();

});
$("#cashReceived").on("change", function () {
    cash();
});
// Add data to array and send it to controller for order creation
function insert() {
    var saleDetails = [];
    var stockLog = [];
    var total = 0;
    var qtyVE = 0;
    var COGS = 0;
    var BranchId = $('#hdnBranchId').val();
    var isPOS = $('#hdnPOS').val();
    // console.log(BranchId +" branch id");
    invoiceAmount = $('#subAmount').val();
    //if (totalInvoice == "" || totalInvoice == 0) {
    var paidAmount = $('#amountPaid').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    // console.log(invoiceAmount);
    var vat = parseFloat($('#vatAmount').val());
    var exp = parseFloat($('#expenseInput').val());
    if (vat == "" || isNaN(vat)) {
        vat = 0;
        $('#vatInput').val(0);
    }
    var cashRec = parseFloat($('#cashReceived').val());
    var cashRet = parseFloat($('#cashReturned').val());
    //  get Table DAta 
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = $row.find('input[id*="productID"]').val();
        var costPrice = $row.find('input[id*="costPrice"]').val();
        var partNumber = $row.find("td").eq(2).text();
        //var Qty = parseFloat($row.find("#ProductQty").text() || 0);
        var Qty = $row.find('input[id*="pQty"]').val();
        var Packet = parseInt($row.find("#Packet").text() || 0);
        var SalePrice = $row.find("#ProductSalePrice").text();
        var SubTotal = parseFloat($row.find("#ProductSubTotal").text());
        var unitCode = $row.find("#unitCode").text();
        var isPack = $row.find('input[id*="isPacket"]').val();
        var qty = parseFloat(Qty);
        SalePrice = parseFloat(SubTotal / qty);
        COGS += parseFloat(qty) * parseFloat(costPrice);

        saleDetails.push({
            PartNo: partNumber,
            ProductId: pId,
            Location: Location,
            UnitPrice: costPrice,
            UnitCode: unitCode,
            IsPack: isPack,
            Qty: qty,
            Packet: Packet,
            IsReturned: false,
            ReturnedQty: 0,
            SalePrice: SalePrice,
            Total: SubTotal,
            BranchID: BranchId,
        });
        stockLog.push({
            AccountID: $('#hdnAccountID').val(),
            ProductId: pId,
            StockOut: qty,
            Packet: Packet,
            SalePrice: SalePrice,
            CostPrice: costPrice,
            OutReference: 'Sales',
            OrderTypeID: 3,
            UserReferenceID: $('#PONO').val(),
            BranchID: BranchId,
            InvoiceDate: $('#SaleDate').val(),
            UnitCode: unitCode,
            IsPack: isPack
        });
    });
    if (saleDetails.length && invoiceAmount > 0) {
        var QuoteOrderID = $('#OrderID').val();
        var paymentStatus = 0;
        if ($('#PaymentStatus option:selected').val() != 0) {
            paymentStatus = $('#PaymentStatus option:selected').text()
        }
        var SaleAmount = parseFloat(total + vat).toFixed(2);
        var data = {
            'AccountID': $('#hdnAccountID').val(),
            'QuoteID': $('#hdnQuoteID').val(),
            'OnlineOrderID': QuoteOrderID,
            'SOID': $("#hdnSOID").val(),
            'BranchID': BranchId,
            'PONO': $('#PONO').val(),
            'CreditDays': $('#creditDays').val(),
            'VehicleNo': $('#VehicleNo').val(),
            'PaymentStatus': $('#PaymentStatus').val(),
            'SalesDate': $('#SaleDate').val(),
            'PaymentStatus': paymentStatus,
            'PaymentTypeID': $('#PaymentType option:selected').val(),
            'VAT': vat,
            'TotalAmount': invoiceAmount,
            'CashReceived': cashRec,
            'CashReturned': cashRet,
            'DiscountAmount': $('#discInput').val(),
            'LoyaltyDiscount': $('#redeemPoints').val(),
            'DiscountPercent': $('#customerDiscount').val(),
            'AmountPaid': $('#amountPaid').val(),
            'TotalPaid': $('#amountPaid').val(),
            'ChequeDate': $("#chqDate").val(),
            'ChequeNo': $("#chqNumber").val(),
            'BankName': bank,
            'COGS': parseFloat(COGS),
            'SaleDetails': saleDetails
        };
        var json = JSON.stringify({
            'model': data,
            'modelStockLog': stockLog,
            'isQuote': isQuote,
            'QuoteOrderID': QuoteOrderID,
            'bankAccId': bankAccountId,
            'OrderTypeID': isPOS
        });
        //console.log(json);
        ajaxCall("POST", json, "application/json; charset=utf-8", "/SalesOrder/SaveOrder", "json", onSuccess, onFailure);

        function onSuccess(Result) {
            if (Result > 0) {
                uiUnBlock();
                //window.location.href = '/Quotation'; 
                if (isPOS == 0) {
                    window.open('/SalesOrder/printOrder?id=' + parseInt(Result) + '');
                    //window.open('/SalesOrder/printOrder?id=' + parseInt(Result) + '');
                    //console.log("OK");
                    window.location.href = '/SalesOrder';//?IsNew=true';
                }
                else if (isPOS == 1) {
                    window.open('/SalesOrder/printOrder?id=' + parseInt(Result) + '');
                    window.location.href = '/SalesOrder/POS';
                }
                else if (isPOS == 2) { // online orders
                    window.open('/OnlineOrders/printOrder?id=' + parseInt(Result) + '');
                    window.location.href = '/OnlineOrders';
                }

            } else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                // $('#btnHold').prop('disabled', false);

                alert("Some error Ocurred! Please Check Your Entries");
            }
        }

        function onFailure(error) {
            if (error.statusText == "OK") {
                console.log("OK");
            } else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    } else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        $('#btnHold').prop('disabled', false);
        swal("Total", "Some error Ocurred! Please Check Sub Total !", "error");
    }
}

function Hold() {
    var BranchId = $('#hdnBranchId').val();
    var saleDetails = [];
    var total = 0;
    var qtyVE = 0;
    var isPOS = $('#hdnPOS').val();
    invoiceAmount = $('#subAmount').val();
    //if (totalInvoice == "" || totalInvoice == 0) {
    var paidAmount = $('#amountPaid').val();
    var vat = parseFloat($('#vatAmount').val());
    var exp = parseFloat($('#expenseInput').val());
    if (vat == "" || isNaN(vat)) {
        vat = 0;
        $('#vatInput').val(0);
    }
    //  get Table DAta 
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = $row.find('input[id*="productID"]').val();
        var costPrice = $row.find('input[id*="costPrice"]').val();
        var partNumber = $row.find("td").eq(2).text();
        //var Qty = parseFloat($row.find("#ProductQty").text() || 0);
        var Qty = $row.find('input[id*="pQty"]').val();

        var Packet = parseInt($row.find("#Packet").text() || 0);
        var SalePrice = $row.find("#ProductSalePrice").text();
        var SubTotal = parseFloat($row.find("#ProductSubTotal").text());
        var unitCode = $row.find("#unitCode").text();
        var isPack = $row.find('input[id*="isPacket"]').val();
        var qty = parseFloat(Qty);
        saleDetails.push({
            PartNo: partNumber,
            ProductId: pId,
            Location: Location,
            UnitPrice: costPrice,
            UnitCode: unitCode,
            IsPack: isPack,
            Qty: qty,
            Packet: Packet,
            IsReturned: false,
            ReturnedQty: 0,
            SalePrice: SalePrice,
            Total: SubTotal,
            BranchID: BranchId
        });
    });
    if (saleDetails.length) {
        var HoldID = $("#hdnHoldID").val();
        var paymentStatus = 0;
        if ($('#PaymentStatus option:selected').val() != 0) {
            paymentStatus = $('#PaymentStatus option:selected').text()
        }
        var SaleAmount = parseFloat(total + vat).toFixed(2);
        var data = {
            'HoldID': HoldID,
            'QuoteID': $('#hdnQuoteID').val(),
            'AccountID': $('#hdnAccountID').val(),
            'BranchID': BranchId,
            'PONO': $('#PONO').val(),
            'PaymentStatus': $('#PaymentStatus').val(),
            'SalesDate': $('#SaleDate').val(),
            'PaymentStatus': paymentStatus,
            'PaymentTypeID': $('#PaymentType option:selected').val(),
            'VAT': vat,
            'TotalAmount': invoiceAmount,
            'DiscountAmount': $('#discInput').val(),
            'AmountPaid': $('#amountPaid').val(),
            'TotalPaid': $('#amountPaid').val(),
            'QuoteDetails': saleDetails
        };
        var json = JSON.stringify({
            'model': data
        });

        ajaxCall("POST", json, "application/json; charset=utf-8", "/SalesOrder/HoldOrder", "json", onSuccess, onFailure);

        function onSuccess(Result) {
            if (Result == "success") {
                if (isPOS == 0) {
                    window.location.href = '/Quotation/HoldOrders';
                }
                else if (isPOS == 1) {
                    window.open('/Quotation/POSHoldOrders');
                    window.location.href = '/SalesOrder/POS';

                }
                uiUnBlock();

                //console.log("OK");
            } else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                $('#btnHold').prop('disabled', false);
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }

        function onFailure(error) {
            if (error.statusText == "OK") {
                console.log("OKError");
            } else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }
}
