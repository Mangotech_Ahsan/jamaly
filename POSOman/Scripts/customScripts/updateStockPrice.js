﻿var controlId = 0;
var products;
var pID = 0;
$(document).ready(function () {
    var vehCodeID = $('#ddlVehCode').val();    
    //The url we will send our get request to
    var valueUrl = '@Url.Action("GetValues", "Product")';
    // if (vehCodeID > 0) {
    $('#ddlPartNo').select2(
      {
          placeholder: 'Enter Product',
          //Does the user have to enter any data before sending the ajax request
          minimumInputLength: 0,
          allowClear: true,
          ajax: {
              //How long the user has to pause their typing before sending the next request
              delay: 200,
              //The url of the json service
              url: '/Product/GetValues',
              dataType: 'json',
              async: true,
              //Our search term and what page we are on
              data: function (params) {
                  return {
                      pageSize: 1500,
                      pageNum: params.page || 1,
                      searchTerm: params.term,
                      //Value from client side.
                      countyId: $('#ddlVehCode').val()
                  };
              },
              processResults: function (data, params) {
                  params.page = params.page || 1;
                  return {
                      results: $.map(data.Results, function (obj) {
                          return { id: obj.ProductID, text: obj.PartNo };
                      }),
                      pagination: {
                          more: (params.page * 1500) <= data.Total
                      }
                  };
              }
          }
      });
    getProductStock(-1);
});
$(function () {
    $('#btnSave').click(function () {
        insert();
    })
})
$("#ddlVehCode").change(function () {
    var vehCodeID = $('#ddlVehCode').val();
    //if (vehCodeID == "")
    //{ vehCodeID = -1; }
    //var json = { "vehCodeId": vehCodeID };
    //$.ajax({
    //    type: "POST",
    //    contentType: "application/json; charset=utf-8",
    //    url: '/Product/getProducts',
    //    async: true,
    //    data: JSON.stringify(json),
    //    success: function (data) {            
    //        GetDropdown1("ddlPartNo", data, true);
    //    },
    //    error: function (err) { console.log(err); }
    //});
});

// Get  Stock in All Branches by selected Product
function getProductStock(productID) {
    
    if (productID == "")
    { productID = -1;}
    ajaxCall("GET", { "iProductID": productID }, "application/json; charset=utf-8", "/Stock/getStockForUpdatePrice",
        "json", onSuccess, onFailure);
    function onSuccess(data) {
        getProducts();
        var total = 0;
        var ien = data.qry.length;
        $('#tbl').find('tbody').empty();
        if (ien > 0) {
            var html = '';
            for (var i = 0; i < ien; i++) {
                var BranchSelect = '<select id="ddlBranch' + controlId + '" class="form-control"></select>';
                var prodID = data.qry[i].ProductID;
                var stockID = data.qry[i].StockID;
                var vehCode = (data.qry[i].VehicleCode);
                var partNo = (data.qry[i].PartNo);
                var branchID = (data.qry[i].BranchID);
                var costPrice = (data.qry[i].CostPrice);
                var salePrice = (data.qry[i].SalePrice);
                var unitPerCarton = parseInt(data.qry[i].UnitPerCarton);
                var levelID = parseInt(data.qry[i].LevelID);
                var levelIDField = '<input type="hidden" id="LevelID" value="' + levelID + '"/>';
                var unitCTN = '<input type="hidden" id="unitPerCTN" value="' + unitPerCarton + '"/>';
                if (data.qry[i].LevelID == 1)
                {
                    costPrice = parseFloat(data.qry[i].CostPrice * data.qry[i].UnitPerCarton).toFixed(2);
                    salePrice = parseFloat(data.qry[i].SalePrice * data.qry[i].UnitPerCarton).toFixed(2);
                }
                else if (data.qry[i].LevelID == 2 || data.qry[i].LevelID == 3)
                {
                    costPrice = parseFloat(data.qry[i].CostPrice * data.qry[i].UnitPerCarton * 1000).toFixed(2);
                    salePrice = parseFloat(data.qry[i].SalePrice * data.qry[i].UnitPerCarton * 1000).toFixed(2);
                }
                else
                {
                    costPrice = parseFloat(data.qry[i].CostPrice).toFixed(2);
                    salePrice = parseFloat(data.qry[i].SalePrice).toFixed(2);
                }
                html += '<tr>';
                html += '<td><input type="hidden" id="hdnstockID" value="' + stockID + '">' + vehCode + '</td>';
                html += '<td><input type="hidden" id="hdnpID" value="' + prodID + '">' + partNo + '</td>';                
                html += '<td>'+ unitCTN + costPrice + '</td>';
                html += '<td>'+ levelIDField + salePrice + '</td>';
                html += '<td><input id="hdnSPrice" type = "Number"  min="0" class="form-control" style="width:100px;"></td>';
                html += '</tr>';
                controlId += 1;
            }
            $('#tbl tbody').append(html);
        }
    }
    function onFailure(err) {
        console.log(err);
    }
}
// Get Branch Lists for dropdown
function getProducts() {
    ajaxCall("POST", {}, "application/json; charset=utf-8", "/Stock/getBranches", "json", onSuccess);
    function onSuccess(data) {
        products = data;
    }
    function onFailure(err) {
        console.log(err);
    }
}

function checkNull(value) {
    return JSON.stringify(value).replace(/null/g, "\"\"");
}
// Prevent user to enter quantity more than avaialable qty 
$('#tbl').keyup(function (e) {
    
    $('#btnSave').prop('disabled', false);
});

// Move Stock
function insert() {
    var updatePrice = [];
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        var stockID = $row.find('td:eq(0)').html();
        var newPrice = $row.find('input[id*="hdnSPrice"]').val();
        var unitPerCTN = $row.find('input[id*="unitPerCTN"]').val();
        var unitCode = $row.find("#unitCode").text();       
        var levelID = $row.find('input[id*="LevelID"]').val();
        if (levelID == '1') {

            newPrice = parseFloat(newPrice / unitPerCTN);
        }
        else if (levelID == '2' || levelID == '3') {
            newPrice = parseFloat(newPrice / unitPerCTN / 1000);
        }
        else {
            newPrice = parseFloat(newPrice);
        }
        if (newPrice != "" && newPrice > 0) {
            pID = $row.find('input[id*="hdnpID"]').val();   // can also select from main ddl                
            updatePrice.push({
                ProductId: pID,
                StockID: location,
                NewSalePrice: newPrice
            });
        }
    });
    if (updatePrice.length) {
        var json = JSON.stringify({ 'modelStockPrice': updatePrice });
        //console.log(json);            
        ajaxCall("POST", json, "application/json; charset=utf-8", "/Stock/UpdateSalePrice", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result == "success") {
                getProductStock(-1);
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK")
                console.log(error);
        }
    }
}