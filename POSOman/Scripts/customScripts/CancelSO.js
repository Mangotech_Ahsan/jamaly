﻿var AccountID = 0;
var BranchID = 0;
var ChequeID = 0;
var UPJEntryID =0;
var PaidJEntryID = 0;
$('#btnSubmit').click(function () {
    if (confirm('Are you sure you want to delete this SO ?')) {
        insert();
    }

});
function bindOrderData(data) {
    customer = data.qry[0].AccountName;

    document.getElementById('customer').value = customer;
    if (data.qry[0].SalesDate != "") {
        var num = (data.qry[0].SalesDate).match(/\d+/g);
        var date = new Date(parseFloat(num));
        var dateString = date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear();
    }
    document.getElementById('date').value = dateString;

}
$("#btnSearch").on("click", function () {

    var SONO = $('#SOID').val();
    var branchID = $('#Branch').val();
    if (SONO == "") {
        swal("Error", "Please Enter Invoice NO.", "error");
    }
    else if (branchID == "") {
        swal("Error", "Please Select Branch", "error");
    }
    else { getInvoiceDetails(SONO, branchID); }

});
function getInvoiceDetails(SOID, BranchID) {
    ajaxCall("GET", { "SOID": SOID, "BranchID": BranchID }, "application/json; charset=utf-8", "/CancelOrders/getSODetails",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        var jen = data.JentryUP.length;
        var jenPaid = data.JentryPaid.length;
        var chq = data.Cheque.length;       
        $('#tblSO').find('tbody').empty();
        if (ien > 0) {
            bindOrderData(data);
            var html = '';
            for (var i = 0; i < ien; i++) {
                AccountID = data.qry[i].AccountID;
                BranchID = data.qry[i].BranchID;
                html += '<tr>';
                html += '<td id=description>' + data.qry[i].BranchName + '</td>';
                html += '<td><input id="orderID" type="hidden" value="' + data.qry[i].OrderID + '">' + data.qry[i].SOID + '</td>';
                html += '<td><input id="prodID" type="hidden" value="' + data.qry[i].ProductID + '">' + data.qry[i].PartNo + '</td>';
                html += '<td id=description>' + data.qry[i].Description + '</td>';
                html += '<td id=SaleQty>' + data.qry[i].Qty + '</td>';
                html += '<td id=SalePrice>' + data.qry[i].SalePrice + '</td>';
                html += '<td>' + data.qry[i].Total + '</td>';
                html += '</tr>';
            }
            $('#tblSO tbody').append(html);
        }
        else {
            alert('Not found!!!');
        }
        $('#tblJdetail').find('tbody').empty();
        if (jen > 0) {
            UPJEntryID =  data.JentryUP[0].JEntryID;
            var html = '';
            for (var i = 0; i < jen; i++) {
                var num = (data.JentryUP[i].VoucherDate).match(/\d+/g);
                var date = new Date(parseFloat(num));
                var dateString = date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear();
                html += '<tr>';
                html += '<td>' + data.JentryUP[i].BranchName + '</td>';
                html += '<td>' + data.JentryUP[i].AccountName + '</td>';
                html += '<td><input id="jentryID" type="hidden" value="' + data.JentryUP[i].JentryID + '">' + dateString + '</td>';
                html += '<td>' + data.JentryUP[i].Detail + '</td>';
                html += '<td id=dr>' + data.JentryUP[i].Dr + '</td>';
                html += '<td id=cr>' + data.JentryUP[i].Cr + '</td>';
                html += '</tr>';
            }
            $('#tblJdetail tbody').append(html);
        }
        if (jenPaid > 0) {
            PaidJEntryID = data.JentryPaid[0].JEntryID;
            var html = '';
            for (var i = 0; i < jenPaid; i++) {
                var num = (data.JentryPaid[i].VoucherDate).match(/\d+/g);
                var date = new Date(parseFloat(num));
                var dateString = date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear();
                html += '<tr>';
                html += '<td>' + data.JentryPaid[i].BranchName + '</td>';
                html += '<td>' + data.JentryPaid[i].AccountName + '</td>';
                html += '<td><input id="jentryID" type="hidden" value="' + data.JentryPaid[i].JentryID + '">' + dateString + '</td>';
                html += '<td>' + data.JentryPaid[i].Detail + '</td>';
                html += '<td id=dr>' + data.JentryPaid[i].Dr + '</td>';
                html += '<td id=cr>' + data.JentryPaid[i].Cr + '</td>';
                html += '</tr>';
            }
            $('#tblJdetail tbody').append(html);
        }
        $('#tblChq').find('tbody').empty();
        if (chq > 0) {
            ChequeID = data.Cheque[0].ChequeID;
            var html = '';
            for (var i = 0; i < chq; i++) {
                var num = (data.Cheque[i].ChequeDate).match(/\d+/g);
                var date = new Date(parseFloat(num));
                var dateString = date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear();
                html += '<tr>';
                html += '<td>' + data.Cheque[i].BranchName + '</td>';
                html += '<td id=description>' + data.Cheque[i].ChequeNumber + '</td>';
                html += '<td><input id="chequeID" type="hidden" value="' + data.Cheque[i].chequeID + '">' + dateString + '</td>';
                html += '<td id=description>' + data.Cheque[i].Description + '</td>';
                html += '<td id=Amount>' + data.Cheque[i].Amount + '</td>';
                //html += '<td id=Detail>' + data.Cheque[i].Detail + '</td>';
                html += '</tr>';
            }
            $('#tblChq tbody').append(html);
        }
        
    }
    function onFailure(err) {
        console.log(err);
    }
}

function insert() {

    var soReturn = [];
    var stockLog = [];
    var total = 0;
    var qtyVE = 0;
    var isValid = false;
    var saleOrderID = 0;
    var SOID = 0;
    var branchID = $('#Branch').val();
    $('#tblSO tbody tr').each(function (i, n) {
        var $row = $(n);
        SalesOrderID = $row.find("input[id*='orderID']").val();
        BranchID = BranchID;
        SOID = $row.find("td:eq(1)").text();
        var pId = $row.find('input[id*="prodID"]').val();
        var qty = $row.find("#SaleQty").text();
        var price = $row.find("#SalePrice").text();
        var subTotal = $row.find('input[id*="txtAmount"]').val();
        {
            isValid = true;
            soReturn.push({
                ProductId: pId,
                Qty: qty,
                SalePrice: price,
                Total: subTotal,
                BranchID: branchID,
            });
        }
    });
    if (soReturn.length && isValid == true) {
        var data = {
            'AccountID': AccountID,
            'BranchID': branchID,
            'SaleOrderID': SalesOrderID,
            'SOReturnDetails': soReturn
        };
        var json = JSON.stringify({ 'model': data,'SalesOrderID':SalesOrderID, 'SOID':SOID ,'BranchID':branchID,'ChequeID':ChequeID, 'UPJEntryID' : UPJEntryID , 'PaidJEntryID':PaidJEntryID});
        //console.log(json);
        ajaxCall("POST", json, "application/json; charset=utf-8", "/CancelOrders/DeleteSalesOrder", "json", onSuccess, onFailure);

        function onSuccess(Result) {
            if (parseInt(Result) > 0) {
                uiUnBlock();
                window.location.href = '/CancelOrders/SOCancel';
            }
            else {
                uiUnBlock();
                $('#btnSave').prop('disabled', false);
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                window.location.href = '/CancelOrders/SOCancel';
                console.log(error.statusText);
            }
            else {
                uiUnBlock();
                $('#btnSave').prop('disabled', false);
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }
    else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);
        swal("DO", "Please Enter DONO!", "error");
    }
}