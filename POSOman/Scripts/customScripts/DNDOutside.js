﻿function renderPartialViewJS() {
    changeBoardItemWidth($("#hdnBoardCount").val());
    var deptId = +$("#hdnDeptID").val() || null;
    var dragContainer = document.querySelector('.drag-container');
    var itemContainers = [].slice.call(document.querySelectorAll('.board-column-content'));
    var columnGrids_offset = [];
    var columnGrids_satin = [];
    var columnGrids_sticker = [];
    var columnGrids_head = [];
    var genericColumnGrids = null;
    var genericDeptGrids = [];
    var boardGrid;
    var toGridId;
    var itemToIndex = null;

    let isDragMoveStarted = false;
    let isDragMoveEnded = true;

    // Init the column grids so we can drag those items around.
    itemContainers.forEach(function (container) {
        var grid = new Muuri(container, {
            items: '.board-item',
            dragEnabled: true,
            dragSort: function () {
                if (deptId == 1) {//offset
                    return columnGrids_offset;
                }
                else if (deptId == 2) { // satin
                    return columnGrids_satin;
                }
                else if (deptId == 3) { //sticker
                    return columnGrids_sticker;
                }
                else if (deptId == 5) { // head
                    return columnGrids_head;
                }
            },
            dragContainer: dragContainer,
            dragAutoScroll: {
                targets: (item) => {
                    return [
                        { element: window, priority: 0 },
                        { element: item.getGrid().getElement().parentNode, priority: 1 },
                    ];
                }
            },
        })
            .on('dragInit', function (item) {
                item.getElement().style.width = item.getWidth() + 'px';
                item.getElement().style.height = item.getHeight() + 'px';
            })
            .on('dragReleaseEnd', function (item) {
                //console.log(item);


                //var toGridIndex = columnGrids.map(function (x) { return x._id; }).indexOf(item._gridId);
                //var toGridData = columnGrids[toGridData];
                ////console.log("to gridIndex: " + toGridIndex);

                ////console.log(boardGrid.getItems()[3]); // get the grid name (machine name)
                //// console.log("from Grid: " + boardGrid.getItems()[fromGridIndex]._element.innerText.split(/\r?\n/)[0]); // get the grid name (machine name)
                ////console.log("InnerText: " + item._element.innerText); 
                ////console.log("innerHTML: " + item._element.innerHTML);
                //const parser = new DOMParser();
                //dom = parser.parseFromString(item._element.innerHTML, "text/html");
                ////var data = document.getElementById('hdnPID').val;
                ////console.log("PID: " + dom.querySelector('.hdnPID').innerHTML);
                ////console.log("to Grid: " + boardGrid.getItems()[toGridIndex]._element.innerText.split(/\r?\n/)[0]); // get the grid name (machine name)
                ////console.log("M_ID: " + boardGrid.getItems()[toGridIndex]._element.innerText.split('|')[0]); // get the grid name (machine name)
                ////console.log("JOID: " + item._element.innerText.split('|')[0]);
                ////console.log("PID: " + dom.querySelector('.hdnPID').innerHTML);

                //changeOrderMachine(item._element.innerText.split('|')[0], boardGrid.getItems()[toGridIndex]._element.innerText.split('|')[0], dom.querySelector('.hdnPID').innerHTML);


                item.getElement().style.width = '';
                item.getElement().style.height = '';
                item.getGrid().refreshItems([item]);
            })
            .on('dragMove', function (item) {
                console.log("dragMove");
                isDragMoveStarted = true;
                isDragMoveEnded = false;
            })
            .on('dragEnd', function (item) {
                isDragMoveEnded = true;

                if (isDragMoveStarted && isDragMoveEnded) {
                    console.log("dragEnd");
                   // console.log(item)
                    isDragMoveStarted = false;
                    var toGridIndex = null;//columnGrids.map(function (x) { return x._id; }).indexOf(item._gridId);
                    var machineId = null;
                    if (deptId == 1) {//offset
                        
                        console.log("####### offset grid data ########");
                        console.log(columnGrids_offset);
                        //toGridIndex = columnGrids_offset.map(function (x) { return x._id; }).indexOf(item._gridId);
                        machineId = genericColumnGrids.find(x => x.gridId === item._gridId).machineId;

                    }
                    else if (deptId == 2) { // satin
                        console.log(columnGrids_satin);
                        //toGridIndex = columnGrids_satin.map(function (x) { return x._id; }).indexOf(item._gridId);
                        machineId = genericColumnGrids.find(x => x.gridId === item._gridId).machineId;

                    }
                    else if (deptId == 3) { //sticker
                        console.log(columnGrids_sticker);
                        //toGridIndex = columnGrids_sticker.map(function (x) { return x._id; }).indexOf(item._gridId);
                        machineId = genericColumnGrids.find(x => x.gridId === item._gridId).machineId;
                    }
                    else if (deptId == 5) { // head
                        //toGridIndex = columnGrids_head.map(function (x) { return x._id; }).indexOf(item._gridId);
                        machineId = genericColumnGrids.find(x => x.gridId === item._gridId).machineId;
                    }
                    const parser = new DOMParser();
                    dom = parser.parseFromString(item._element.innerHTML, "text/html");
                    //changeOrderMachine(item._element.innerText.split('|')[0], boardGrid.getItems()[toGridIndex]._element.innerText.split('|')[0], dom.querySelector('.hdnPID').innerHTML);
                    changeOrderMachine(item._element.innerText.split('|')[0], machineId, dom.querySelector('.hdnPID').innerHTML, itemToIndex);

                    //var layout = window.localStorage.getItem(deptId + 'layout');
                    //console.log(layout);


                    //console.log("*******board*********")
                    //console.log(boardGrid);
                    //console.log("********board items********")
                    //for (var i = 0; i < boardGrid._items.length; i++) {
                    //    var item = boardGrid._items[i];

                    //    console.log(item);
                    //    console.log(item._element.dataset.id);

                    //}
                    //console.log("****************")
                    //console.log("***to gridIndex: " + toGridIndex);
                    //console.log("get Grid******: ");
                    //console.log(item.getGrid());
                    //console.log("get Element ***** ");
                    //console.log(item.getElement());
                    //console.log("***** ");
                    //console.log(boardGrid.getItems()[item.getElement()]);

                    //console.log(boardGrid.getItems()[3]); // get the grid name (machine name)
                    // console.log("from Grid: " + boardGrid.getItems()[fromGridIndex]._element.innerText.split(/\r?\n/)[0]); // get the grid name (machine name)
                    //console.log("InnerText: " + item._element.innerText); 
                    //console.log("innerHTML: " + item._element.innerHTML);
                    
                    //var data = document.getElementById('hdnPID').val;
                    //console.log("PID: " + dom.querySelector('.hdnPID').innerHTML);
                    //console.log("to Grid: " + boardGrid.getItems()[toGridIndex]._element.innerText.split(/\r?\n/)[0]); // get the grid name (machine name)
                    //console.log("M_ID: " + boardGrid.getItems()[toGridIndex]._element.innerText.split('|')[0]); // get the grid name (machine name)
                    //console.log("JOID: " + item._element.innerText.split('|')[0]);
                    //console.log("PID: " + dom.querySelector('.hdnPID').innerHTML);
                    




                }
                else {
                    console.log("you can add click functionality here...")
                }
            })
            .on('layoutStart', function () {
                boardGrid.refreshItems().layout();
            })
            .on('receive', function (item) {
                console.log("****receive*********")
                console.log(item);
                toGridId = item.toGrid._id;
                itemToIndex = item.toIndex;
            })
            .on('send', function (item) {

            })
            .on('move', function (item) {
                console.log("****move*********")
                console.log(item)
                itemToIndex = item.toIndex;
            })
            ;

        //console.log("column grid array");
        //console.log(columnGrids);

        if (deptId == 1) {//offset
            columnGrids_offset.push(grid);
            genericDeptGrids = columnGrids_offset;
            //console.log("offset array")
            //console.log(columnGrids_offset)
        }
        else if (deptId == 2) { // satin
            columnGrids_satin.push(grid);
            genericDeptGrids = columnGrids_satin;
            console.log("satin array")
        }
        else if (deptId == 3) { //sticker
            columnGrids_sticker.push(grid);
            genericDeptGrids = columnGrids_sticker;
            console.log("sticker array")
        }
        else if (deptId == 5) { // head
            columnGrids_head.push(grid);
            genericDeptGrids = columnGrids_head;
            console.log("head array")
        }
    });

    // Init board grid so we can drag those columns around.
    boardGrid = new Muuri('.board', {
        dragEnabled: true,
        dragHandle: '.board-column-header'
    }).on('move', function (board) {
        console.log("boardGrid");

        // console.log(item)
        saveLayout(boardGrid, false, deptId);

    });

    var layout = window.localStorage.getItem(deptId + 'layout');
    console.log("layout called************")
    console.log(layout);

    

    

    
    //if (deptId == 1 && layout != null) {//offset
    //    columnGrids_offset = layout;
    //}
    //else if (deptId == 2 && layout != null) { // satin
    //    columnGrids_satin = layout;
    //}
    //else if (deptId == 3 && layout != null) { //sticker
    //    columnGrids_sticker = layout;
    //}
    //else if (deptId == 5 && layout != null) { // head
    //    columnGrids_head = layout;
    //}

    //console.log(layout)
    if (layout) {

        loadLayout(boardGrid, layout);
    }
    else {
        boardGrid.layout(true);
    }

    if (boardGrid._items.length == genericDeptGrids.length && boardGrid._items.length > 0 && genericDeptGrids.length > 0) {
        console.log("success!!!");
        var bGridName = deptId == 1 ? 'offset_boardGrid' : deptId == 2
            ? 'satin_boardGrid' : deptId == 3 ? 'sticker_boardGrid' : null;
        if (layout == null) {
            var bGrid = [];
            for (var b = 0; b < boardGrid._items.length; b++) {
                bGrid.push({
                    'gridId': boardGrid._items[b]._id,
                    'machineId': boardGrid._items[b]._element.dataset.id
                })
            }
            
            window.localStorage.setItem(bGridName, JSON.stringify(bGrid));
        }
        console.log("*******board grid*********")
        console.log(boardGrid._items)

        console.log("*******grid*********")
        console.log(genericDeptGrids)

        var machineArray = [];
        var bGrid = JSON.parse(window.localStorage.getItem(bGridName) || [])
        for (var i = 0; i < bGrid.length; i++) {

            //machineArray.push({
            //    'machineId': bGrid[i]._items[i]._element.dataset.id,
            //    'boardId': bGrid._items[i]._id,
            //    'gridId': deptId == 1 ? columnGrids_offset[i]._id : deptId == 2
            //        ? columnGrids_satin[i]._id : deptId == 3 ? columnGrids_sticker[i]._id : null// genericDeptGrids[i]._id
            //});
            machineArray.push({
                'machineId': bGrid[i].machineId,
                'boardId': bGrid[i].gridId,
                'gridId': deptId == 1 ? columnGrids_offset[i]._id : deptId == 2
                    ? columnGrids_satin[i]._id : deptId == 3 ? columnGrids_sticker[i]._id : null// genericDeptGrids[i]._id
            });
        }

        console.log(machineArray);

        const newArr = machineArray.map(obj => {
            if (obj.id != null) {
                return {
                    ...obj, gridId: deptId == 1 ? columnGrids_offset[i]._id : deptId == 2
                        ? columnGrids_satin[i]._id : deptId == 3 ? columnGrids_sticker[i]._id : null };
            }

            return obj;
        });

        var deptWiseArray = deptId == 1 ? 'columnGrids_offset_Array' : deptId == 2
            ? 'columnGrids_satin_Array' : deptId == 3 ? 'columnGrids_sticker_Array' : null;

        //var arr = window.localStorage.getItem(deptWiseArray);
        //if (arr == null) {
            window.localStorage.setItem(deptWiseArray, JSON.stringify(newArr));
            genericColumnGrids = machineArray;
        //}
        //else {
        //    genericColumnGrids = JSON.parse(arr||[]);
        //}


    }

    console.log('genericColumnGrids**********');
    console.log(genericColumnGrids);

}
////////////////////////


function serializeLayout(grid) {
    //console.log("serealizeLayout");
    //console.log(grid);
    var itemIds = grid.getItems().map(function (item) {

        //console.log("*** inside serialization ***")
        //console.log(item.getElement());
        //console.log(item);
        //console.log(item.getElement().getAttribute('data-id'));

        return item.getElement().getAttribute('data-id');
        //return item._id;
    });
    return JSON.stringify(itemIds);
}

function saveLayout(grid, isItem, deptId) {

    var layout = serializeLayout(grid);
    if (isItem) {
        window.localStorage.setItem('gridItems', layout);

    }
    else {
        if (deptId != null) {
            window.localStorage.setItem(deptId + 'layout', layout);
        }

    }
}

function loadLayout(grid, serializedLayout) {
    var layout = JSON.parse(serializedLayout);
    var currentItems = grid.getItems();
    var currentItemIds = currentItems.map(function (item) {
        return item.getElement().getAttribute('data-id')
    });
    var newItems = [];
    var itemId;
    var itemIndex;

    for (var i = 0; i < layout.length; i++) {
        itemId = layout[i];
        itemIndex = currentItemIds.indexOf(itemId);
        if (itemIndex > -1) {
            newItems.push(currentItems[itemIndex])
        }
    }

    //console.log("new items");
    //console.log(newItems);
    grid.sort(newItems, { layout: 'instant' });
}
        //////////////////////////


changeOrderMachine = (JOID, MID, PID, toIndex) => {
    //uiBlock("Please wait...");
    if (JOID > 0 && MID > 0) {
        var json = { "OrderID": JOID, "MachineID": MID, "ProductID": PID,"toIndex":toIndex };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Sales/TrelloTransferProcess',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                if (data == 1) {
                    toastr.success('JO #' + JOID + ' successfully moved on Machine #: ' + MID);
                }
                else {
                    //uiUnBlock();
                    $("#btn").click();
                    toastr.error('JO #' + JOID + ' unable to move. Please contact admin.');
                }


            },
            error: function (err) { /*uiUnBlock();*/ console.log(err); }
        });
    }

}


function MarkJoAsCompleted() {

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Sales/MarkJoAsCompleted',
        async: true,
        success: function (data) {
            console.log("MarkJoAsCompleted DND Page:" + data);
        },
        error: function (err) { console.log(err); }
    });
}


setInterval(function () {
    MarkJoAsCompleted();
}, 1800000)
