﻿var customerID = 0;
var accID = 0;
var isPONOExist = false;
$(document).ready(function () {
    // Self Generated QuotationID  
    getQuotationID();
    
});

$('#vatInput').on('input', function (e) {


    var vatid = $('#vatInput').val();
   
    var test = Number(vatid);

    var json = {
        "vatid": test
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Quotation/getDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#vatInputgs').val(data);

        },
        error: function (err) {
            console.log(err);
        }
    });
    getNewTotal();
});
function getQuotationID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Quotation/getLastQuotationID',
        async: true,
        success: function (data) {
            {
                var value = data;
                var newValue = value.split('-');
                var increase = newValue[1];
                increase++;
                value = "Q-" + increase;
                $("#QID").val(value);                
            }
        },
        error: function (err) { console.log(err); }
    });
}
$("#PONO").on("change", function () {
    var accID = $('#hdnAccountID').val();
    if(accID =="" || accID ==0)
    {
        swal("Customer", "Please Select Customer First!", "error");
    }
    else{
        var PONO = $("#PONO").val();
        checkPONO(PONO, accID);
        
    }    
});
function checkPONO(PONO, accID) {    
    var json = { "accountID": accID, "PONO": PONO };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/checkPONO',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            if (data == true)
            {
                toastr.warning('PONo already Exists for selected Customer!')
                $("#PONO").val("");             
            }
        },
        error: function (err) { console.log(err); }
    });
}
$('#btnSubmit').click(function () {    
    accID = $('#AccountID').val();
    var isValid = true;    
    var PONo = $('#PONO').val();
    var SalesDate = $('#SaleDate').val();    
    var rowCount = $('#tblProduct tbody tr').length;    
    //var BranchID = $('#BranchID option:selected').val();
    var BranchID = $('#hdnBranchId').val();
    
    if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Customer", "Please Select Customer!", "error");
    }
    else if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Sales Date!", "error");
        //console.log(PurchaseDate + "date");
    }
    else if (rowCount ==0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");
        //console.log(PurchaseDate + "date");
    }    
    else if (isValid == true) {  
        $('#btnSubmit').prop('disabled', true);
        uiBlock();
        insert();
    }
});
function applyDiscount() {
    var discount = $('#discInput').val();
    var subTotal = $("#subAmount").val();
    if (discount == "" || isNaN(discount)) {
        //$('#discInput').val(0);
        discount = 0;
        $('#totalAmount').val(subTotal);
    } else if (parseFloat(discount) >= parseFloat(subTotal)) {
        toastr.warning('Discount must less than total!')
        $('#discInput').val(0);
        $('#totalAmount').val(subTotal);
    } else {
        var totalAmount = parseFloat(subTotal) - parseFloat(discount);
        $('#totalAmount').val(parseFloat(totalAmount).toFixed(2));

    }
   

}
function applyZeroVAT() {
    var vat = parseFloat($('#vatInput').val());
    //var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        //$('#vatInput').val(0);
        vat = 0;
        $('#vatAmount').val(0);
        $('#finalAmountWithVAT').val($("#totalAmount").val());
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat($("#totalAmount").val()).toFixed(2);
        }
    }
}
function calcVAT() {
    var vat = parseFloat($('#vatInput').val());
    //var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        applyZeroVAT();
    } else {
        var totalInvoice = $("#totalAmount").val();
        var vatAmount = parseFloat((vat * totalInvoice) / 100);

        $('#vatAmount').val(parseFloat(vatAmount).toFixed(2));
        var finalAmount = parseFloat(vatAmount) + parseFloat(totalInvoice);
        $('#finalAmountWithVAT').val(finalAmount);
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat(finalAmount).toFixed(2);
        }
    }
}
function getNewTotal() {
  //  alert($('#vatAmount').val());
    applyDiscount();
   // applyLoyaltyDiscount();
    calcVAT();
   // calcAmountPaid();
    var subTotal = $("#subAmount").val();
    var VAT = $('#vatAmount').val();
    var discount = $('#discInput').val();
   // var loyaltyDiscount = $('#redeemPoints').val();
    if (VAT == "" || isNaN(VAT)) {
        VAT = 0;
        applyZeroVAT();
    }
    if (discount == "" || isNaN(discount)) {
        discount = 0;
        applyDiscount();
    }
    //if (loyaltyDiscount == "" || isNaN(loyaltyDiscount)) {
    //    loyaltyDiscount = 0;
    //    applyLoyaltyDiscount();
    //}
    //else {
    //    applyLoyaltyDiscount();
    //}
    var newTotal = parseFloat(subTotal) + parseFloat(VAT) - parseFloat(discount);/*- parseFloat(loyaltyDiscount);*/
    // var amountPaid = parseFloat(subTotal) + parseFloat(VAT);

    $('#finalAmountWithVAT').val(parseFloat(newTotal).toFixed(2));
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(newTotal).toFixed(2);
    }

}
// Get Customer Balance and Credit Limit 
function getCustomerDetail(accountID) {
    var json = {
        "accountID": accountID
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Customer/getDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#customerCrLimit').val(data.creditLimit);
            $('#customerBalance').val(data.Balance);
        },
        error: function (err) {
            console.log(err);
        }
    });
}
// Add data to array and send it to controller for order creation
function insert() {    
    var saleDetails = [];    
    var total = 0;
    var qtyVE = 0;    
    var Vat = $("#vatAmount").val() || 0;
    invoiceAmount = $('#subAmount').val();            
    var BranchID = $('#hdnBranchId').val();

    var taxs = $('#vatInput').val();
    var tax = Number(taxs);
    var taxwithname = $('#vatInputgs').val();
    
    //  get Table DAta
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = $row.find('input[id*="productID"]').val();
        var costPrice = $row.find('input[id*="costPrice"]').val();
        var partNumber = $row.find("td").eq(2).text();
        //var Qty = parseFloat($row.find("#ProductQty").text() || 0);
        var Qty = $row.find('input[id*="pQty"]').val();

        var Packet = parseInt($row.find("#Packet").text() || 0);
        var SalePrice = $row.find("#ProductSalePrice").text();
        var SubTotal = parseFloat($row.find("#ProductSubTotal").text());
        var unitCode = $row.find("#unitCode").text();
        var isPack = $row.find('input[id*="isPacket"]').val();
        var qty = parseFloat(Qty);
        //SalePrice = parseFloat(SubTotal / qty);

        saleDetails.push({
            PartNo: partNumber,
            ProductId: pId,
            Location: Location,
            UnitPrice: costPrice,
            UnitCode: unitCode,
            IsPack: isPack,
            Qty: qty,
            Packet: Packet,
            IsReturned: false,
            ReturnedQty: 0,
            SalePrice: SalePrice,
            Total: SubTotal,
            BranchID: BranchID
        });
    });
    if (saleDetails.length) {
        uiBlock();
        //total = $('#tbl tfoot tr').find('input[id*="txtTotal"]').val();
        //console.log("Prod Total" + total);
        //total = $('#Total').val()
        var paymentStatus = 0;
        if ($('#PaymentStatus option:selected').val() != 0) {
            paymentStatus = $('#PaymentStatus option:selected').text()
        }
        var SaleAmount = parseFloat(total).toFixed(3);
        var data = {
            'AccountID': $('#AccountID').val(),
            'BranchID': BranchID,
            'QuoteID': $('#QID').val(),
            'PONO': $('#PONO').val(),
            'VehicleNo': $('#VehicleNo').val(),
            'Comment': $('#Comment').val(),
            'Tax': tax,
            'TaxName': taxwithname,
            'PaymentStatus': $('#PaymentStatus').val(),
            'SalesDate': $('#SaleDate').val(),
            'VAT':Vat,
            'PaymentStatus': paymentStatus,
            'PaymentTypeID': $('#PaymentType option:selected').val(),                                          
            'TotalAmount': invoiceAmount,     
            'CreditDays': $("#creditDays").val(),
            'DiscountAmount': $('#discInput').val(),
            'AmountPaid': $('#amountPaid').val(),
           
            'QuoteDetails': saleDetails
        };
        var json = JSON.stringify({ 'model': data });        
        ajaxCall("POST", json, "application/json; charset=utf-8", "/Quotation/SaveQuotation", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result > 0) {
                //window.location.href = '/Quotation'; 
                uiUnBlock();
                window.open('/Quotation/Export?id=' + parseInt(Result) + '&ReportType/PDF' );
                //console.log("OK");
                window.location.href = '/Quotation'; 
            }
            else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                alert("Some error Ocurred! Please Check Your Entries");
            }            
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                console.log("OK");
            }
            else {
                alert("Some error Occured check your entries!");
            }
        }
    }
}
