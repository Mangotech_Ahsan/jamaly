﻿$("#btnCreateCustomer").on("click", function () {    
    uiBlock();
    var url = $(this).data("url");
    $.get(url, function (data) {
        $('#newCustomerContainer').html(data);
        $('#newCustomerModal').modal('show');
        uiUnBlock();
    });    
});

function PostNewCustomer(btnClicked) {
    uiBlock();
    var $form = $(btnClicked).parents('form');

    if ($form.valid()) {
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize(),
            error: function (xhr, status, error) {
                //do something about the error
            },
            success: function (response) {
                var $summaryUl = $(".validation-summary-valid").find("ul");
                if (response == true) {
                    $summaryUl.text("Already Exists!");
                }
                else {
                    $summaryUl.text("");
                    $('#newCustomerModal').modal('hide');
                    $('#newCustomerContainer').html("");
                    getCustomers();
                }
                uiUnBlock();
            }
        });

    } else {
        console.log('inv');
    }

    return false;// if it's a link to prevent post
}
function getCustomers() {
    console.log("getCustomers");
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Customer/getCustomers',
        async: true,
        success: function (data) {            
            GetDropdown1("AccountID", data, true);
        },
        error: function (err) { console.log(err); }
    });    
}