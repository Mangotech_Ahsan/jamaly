﻿var chqDat = "";
$(function () {
    //document.getElementById("Bank").readOnly = true;
    $("#Bank").prop("disabled", true);
    document.getElementById("VoucherDate").readOnly = true;
    document.getElementById("ChequeDate").readOnly = true;
    document.getElementById("ChequeNumber").readOnly = true;
    document.getElementById("ChequeDate").readOnly = true;
    $('#btnSave').click(function () {
        var vendorID = $('#AccountID option:selected').val();
        var voucherDate = $('#VoucherDate').val();
        chqDate = $('#ChequeDate').val();
        var PaymentTypeID = $('#PayTypeID option:selected').val();
        var bankAccountId = $('#Bank option:selected').val();
        if (chqDate == "") {
            //alert("Please cheque date");
            chqDat = "";
            //console.log(PurchaseDate+"date");
        }
        if (vendorID == "" || vendorID == 0) {
            swal("Vendor", "Please Select Vendor!", "error");
        }
        else if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
         else if ((PaymentTypeID == 3 || PaymentTypeID == 2) && (bankAccountId == "" || bankAccountId == undefined)) {
        isValid = false;
        swal("Bank!", "Please Select  Bank!", "error");
    }
        else {
            $('#btnSave').prop('disabled', true);
            uiBlock();
           insert();
        }
    });

});
var invoiceTotal = 0;
function getVendorDetails(accountId) {
    //console.log(accountId);

    var controlPaying = '<input type = "Number" class="form-control">';

    ajaxCall("GET", { "accountId": accountId }, "application/json; charset=utf-8", "/VendorPayment/getVendorDetail",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        $('#tbl').find('tbody').empty();
        if (ien > 0) {

            var controlPaying = '<input type = "Number"  value=0>';
            var html = '';

            for (var i = 0; i < ien; i++) {
                var num = (data.qry[i].PurchaseDate).match(/\d+/g);
                var date = new Date(parseFloat(num));
                //console.log("date" +date);
                var month = parseInt(date.getMonth()) + 1;
                var dateString = date.getDate() + "/" + month + "/" + date.getFullYear();
                //console.log(dateString +"dstring");
                var totalAmount = parseFloat(data.qry[i].TotalAmount);
                var expense = parseFloat(data.qry[i].Expenses || 0);
                var returnAmount = parseFloat(data.qry[i].ReturnAmount || 0);
                var VAT = parseFloat(data.qry[i].VAT || 0);
                var invoiceTotal = parseFloat(totalAmount + VAT).toFixed(3);
                var balance = balance = parseFloat(invoiceTotal - expense - data.qry[i].TotalPaid - returnAmount).toFixed(3);
                var totalPaid = parseFloat(data.qry[i].TotalPaid);
                //if (totalPaid > 0) {
                //    balance = parseFloat(invoiceTotal - data.qry[i].TotalPaid - returnAmount).toFixed(3);
                //}
                //else {                    
                //    
                //}
                var status = data.qry[i].PaymentStatus;
                if (status == null || status == "") {
                    status = "";
                }


                html += '<tr>';
                //html+='<td>' + <input type=checkbox >+ '</td>';
                html += '<td><input type="hidden" value="' + data.qry[i].OrderID + '">' + data.qry[i].POID + '</td>';
                html += '<td>' + data.qry[i].InvoiceNo + '</td>';
                html += '<td><input type="hidden" value="' + data.qry[i].AccountID + '">' + data.qry[i].AccountName + '</td>';
                html += '<td>' + dateString + '</td>';
                html += '<td>' + invoiceTotal + '</td>';
                html += '<td>' + totalPaid + '</td>';
                html += '<td>' + returnAmount + '</td>';
                html += '<td>' + status + '</td>';
                html += '<td>' + balance + '</td>';
                html += '<td><input type = "Number"  min="0" max=' + balance + ' step="0" class="form-control"></td> <span id="Error"></span>';

                html += '</tr>';
            }
            $('#tbl tbody').append(html);
        }
    }
    function onFailure(err) {
        console.log("ERROR");
    }

}

$("#PayTypeID").on("change", function () {
    var payStatusSelection = $("#PayTypeID").val();
    var totalAmount = $("#TotalAmount").val();
    if (payStatusSelection == 1) {
        //document.getElementById("Bank").readOnly = true;
        $("#Bank").prop("disabled", true);
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;        
    }    
    else if (payStatusSelection == 2) {
        $("#Bank").prop("disabled", false);
        //document.getElementById("Bank").readOnly = false;
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;
    }
    else if (payStatusSelection == 3) {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeDate").readOnly = false;
        document.getElementById("ChequeNumber").readOnly = false;
    }

});
function calcTotal() {
    var total = 0;
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('td:eq(9) input[type="Number"]').val());
        $field = $row.find('td:eq(9) input[type="Number"]')
        //console.log("subTotal : " + subTotal);
        if (subTotal > Number($field.attr("max"))) {
            $field.val($field.attr("max"));
            toastr.warning('Paying Amount must be equal or less than balance!')
            subTotal = parseFloat($row.find('td:eq(9) input[type="Number"]').val());
        }
        if ($.isNumeric(subTotal)) {
            total += parseFloat(subTotal);
            $('#Amount').val(parseFloat(total).toFixed(3));
        }

        //console.log(subTotal);
        //console.log(total);
    });
    $('#tbl tfoot tr').find('td:eq(9) input[type="text"]').val(total);
    //$('#TotalPaying').val(total);    
}
$('#tbl').keyup(function (e) {
    calcTotal();
});
function insert() {
    var rows = [];
    var payLog = [];
    var JEntryLog = [];
    var totalAmount = 0;
    var totalPaying = $('#tbl tfoot tr').find('td:eq(9) input[type="text"]').val();
    //var totalPaying = $('#TotalPaying').val();
    //console.log("Total Paying"+totalPaying);
    var orderId = 0;
    var vendorID = $('#AccountID option:selected').val();
    var desc = $('#Description').val();
    var payType = $('#PayTypeID option:selected').val()
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var voucherDate = $('#VoucherDate').val();
    //var chqDate = $('#ChequeDate').val();
    var chqNumber = $('#ChequeNumber').val();
    var amount = parseFloat($('#Amount').val());

    if (totalPaying == amount) {
        $('#tbl tbody tr').each(function (i, n) {

            var $row = $(n);
            orderId = $row.find('td:eq(0) input[type="hidden"]').val();
            var accountId = $row.find('td:eq(2) input[type="hidden"]').val();
            var balance = $row.find('td:eq(8)').html();
            var paying = $row.find('td:eq(9) input[type="Number"]').val();

            if (orderId != "" && balance != "" && paying != "") {

                totalAmount += parseFloat(paying);
                balance = parseFloat(balance) - parseFloat(paying);
                //console.log('orderId:' + orderId + ', accountId: ' + accountId + ', balance:' + balance + ', paying: ' + paying);
                //console.log('total: ' + totalAmount);

                rows.push({
                    OrderID: orderId,
                    Paying: paying,
                    Balance: balance
                });
                JEntryLog.push({
                    //BranchID : BranchID,
                    Amount: paying,
                    OrderTypeID: 1,
                    EntryTypeID: 1,
                    OrderID: orderId,
                    isReversed: false
                });
            }
        });
        if (rows.length) {

            var data = {

                //'VoucherName': 'Payment',
                'Description': desc,
                'Amount': amount,
                'AccountID': vendorID,
                'VoucherDate': voucherDate,
                'PaymentTypeID': payType,
                'BankName': bank,
                'ChequeDate': chqDate,
                'ChequeNumber': chqNumber,
                'Details': rows
            };
            
            var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog, 'bankAccId': bankAccountId });
            //console.log(json);            
            ajaxCall("POST", json, "application/json; charset=utf-8", "/VendorPayment/SavePayment", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result == "success") {
                    window.location.href = '/VendorPayment/Voucher?isNew=true&id=' + vendorID;
                }
                else {
                    swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                }
                //location.reload();
                //window.location.href = 'Index';
                //alert("success");
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    location.reload();
                //console.log(error);
                //location.reload();
            }
        }
        else {
            var data = {

                //'VoucherName': 'Payment',
                'Description': desc,
                'Amount': amount,
                'AccountID': vendorID,
                'VoucherDate': voucherDate,
                'PaymentTypeID': payType,
                'BankName': bank,
                'ChequeDate': chqDate,
                'ChequeNumber': chqNumber,
            };
            JEntryLog.push({
                    //BranchID : BranchID,
                    Amount: paying,
                    OrderTypeID: 1,
                    OrderID: orderId,
                    EntryTypeID: 1,
                    isReversed: false
                });
            var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog, 'bankAccId': bankAccountId });
            //console.log(json);            
            ajaxCall("POST", json, "application/json; charset=utf-8", "/VendorPayment/SavePayment", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result == "success") {
                    uiUnBlock();
                    //window.location.href = '/VendorPayment/VendorStatement?AccountID=' + vendorID;
                    //window.location.href = '/VendorPayment/Details?isNew=true&id=' + vendorID;
                    window.location.href = '/VendorPayment';
                }
                else {
                    uiUnBlock();
                    $('#btnSave').prop('disabled', false);
                    swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                }
                //location.reload();
                //window.location.href = 'Index';
                //alert("success");
            }
            function onFailure(error) {
                if (error.statusText == "OK") {
                    //alert("success");
                    //console.log(error);
                    window.location.reload();                    
                }
            }
        }
    }
    else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);

        swal("Total", "Total Paying Must be Equal to Total Amount of Payment/Cheque!", "error");        
    }
}

