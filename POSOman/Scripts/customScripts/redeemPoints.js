﻿var isExist = false;

function getRedeemPoints() {
    uiBlock();
    var accountID = $('#hdnAccountID').val();

    var url = '/LoyaltyCards/RedeemPoints?id=' + accountID;
    $.get(url, function (data) {
        $('#redeemPointsContainer').html(data);
        $('#redeemPointsModal').modal('show');
    });
    uiUnBlock();
}

function ClearMessage() {
    $("#message").html("");
};

function PostRedeemPoints(btnClicked) {
    uiBlock();
    var $form = $(btnClicked).parents('form');   
    if ($form.valid()) {
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize(),
            error: function (xhr, status, error) {
                //do something about the error
            },
            success: function (response) {
                if (response == true) {
                    var $summaryUl = $(".validation-summary-valid").find("ul");
                    $summaryUl.text("Already Exists!");
                    //$summaryUl.append($("<li>").text("Already Exists!"));
                }
                else {
                    $('#redeemPointsModal').modal('hide');
                    $('#redeemPointsContainer').html("");
                }
                uiUnBlock();
            }       
    });
    } else {
        console.log('inv');
    }
}
