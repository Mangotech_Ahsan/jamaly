﻿$(document).ready(function () {
    var vehCodeID = $('#ddlVehCode').val();

    //The url we will send our get request to
    var valueUrl = '@Url.Action("GetStockProducts", "Stock")';
    // if (vehCodeID > 0) {
    $('#ddlPartNumber').select2(
      {
          placeholder: 'Enter PartNo',
          //Does the user have to enter any data before sending the ajax request
          minimumInputLength: 0,
          allowClear: true,
          ajax: {
              //How long the user has to pause their typing before sending the next request
              delay: 200,
              //The url of the json service
              url: '/Stock/GetStockProducts',
              dataType: 'json',
              async: true,
              //Our search term and what page we are on
              data: function (params) {
                  return {
                      pageSize: 500,
                      pageNum: params.page || 1,
                      searchTerm: params.term,
                      //Value from client side.
                      countyId: $('#ddlVehCode').val()
                  };
              },
              processResults: function (data, params) {
                  params.page = params.page || 1;
                  return {
                      results: $.map(data.Results, function (obj) {
                          return { id: obj.ProductID, text: obj.PartNo };
                      }),
                      pagination: {
                          more: (params.page * 100) <= data.Total
                      }
                  };
              }
          }
      });
});