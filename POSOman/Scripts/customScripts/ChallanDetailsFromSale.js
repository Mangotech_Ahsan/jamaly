﻿$tableItemCounter = 0;
$addedProductIDs = [];
itemWiseDescArray = [];

var pkgList = [];
$selCustID = 0;
var QtyinPack = 0;
var vc = '';
var PrUntLnth = 0;
var Count = 0;
var OrderId = $('#InvOrderId').val();
//var index = null;
var proIdEdit = 0;
//console.log(OrderId + "OID");
$("#ddlVehCode").prop('disabled', true);
$("#ddlPartNumber").prop('disabled', true);
$("#ddlproductname").prop('disabled', true);
$("#addRow").prop('disabled', true);
$("#BranchID").prop('disabled', true);
$("#btnRemove").prop('disabled', false);

GetEditCustomers(OrderId);
//Edit Customer
function GetEditCustomers(OrderId) {
    let currentUserRole = $("#currentUserRole").val() || null;
    if (OrderId == "") { OrderId = -1; }
    var json = { "OrderId": OrderId, "currentUserRole": currentUserRole };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/GetChallanDetailsFromSales',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            var num = (data[0].salesDate).match(/\d+/g);
            var date = new Date(parseFloat(num));

            if (data[0].chequeDate != null) {
                var num1 = (data[0].chequeDate).match(/\d+/g);
                var date1 = new Date(parseFloat(num));
                document.getElementById('chqDate').valueAsDate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate(), date.getUTCHours() + 5, date.getMinutes());
            }

            $('#AccountID').val(data[0].Qry.Value).trigger('change.select2');
            $('#custPONumber').val(data[0].CustomerPO);
            $('#Remarks').val(data[0].Remarks);

            $('#BranchID').val(data[0].Qry1.Value).trigger('change.select2');
            $('#customerCrLimit').val(data[0].Qry2);
            var customerID = $('#AccountID').val();
            $('#hdnAccountID').val(customerID);
            getCustomerDetail(customerID);
            $('#PaymentStatus').val(data[0].PayStatus).trigger('change.select2');
            $("#Bank").val(data[0].Bank.Value).trigger('change.select2');

            // console.log(data[0].PayStatus);
            if (data[0].PayStatus == 3) {
                $("#PaymentType").prop('disabled', true);
            }
            $('#chqNumber').val(data[0].Cheque);
            $('#amountPaid').val(data[0].AmountPaid);
            $('#vatAmount').val(data[0].Tax);
            $('#creditDays').val(data[0].CreditDays);
            $('#finalAmountWithVAT').val(data[0].FinalAmount);
            $('#totalAmount').val(0);
            $('#subAmount').val(0);
            $('#discInput').val(data[0].Discount);
            ////////////populate product edit table/////////////
            data[0].ProductsList.forEach(ListProduct);
            document.getElementById('SaleDate').valueAsDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getUTCHours() + 5, date.getMinutes());
            //console.log(ToJSDate(data[0].salesDate));
            if (data[0].Paytype == "" || data[0].Paytype == null) {
            }
            else {
                $('#PaymentType').val(1).trigger('change.select2');
                if (data[0].Paytype.Value == 1) {
                    $("#Bank").prop("disabled", true);
                    $("#chqNumber").prop("disabled", true);
                    $("#chqDate").prop("disabled", true);
                }
                else if (data[0].Paytype.Value == 2) {
                    $("#Bank").prop("disabled", false);
                    $("#chqNumber").prop("disabled", true);
                    $("#chqDate").prop("disabled", true);
                }
                else {
                    $("#Bank").prop("disabled", false);
                    $("#chqNumber").prop("disabled", false);
                    $("#chqDate").prop("disabled", false);
                }
            }


        },
        error: function (err) { console.log(err); }
    });
}


//function for edit product list
////////////
function ListProduct(item) {
    var packet = parseInt(item.Packet);
    var ReqQty = +item.Qty || 0;
    var ReadyQty = +item.ReadyQty || 0;

    var DeliveredQty = +item.DeliveredQty || 0;
    var vehCode = item.Cat;
    var ProductID = item.ProductID;
    var PartNO = item.PartNo.replace(/[^\w\s]/gi, '');
    var UnitCode = item.UnitCode;
    var costPrice = parseFloat(item.CostPrice) || 0;
    var SalePrice = +item.SalePrice || 0;
    var SubTotal = 0;//item.PTotal;
    var unitID = item.UnitID;
    var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';
    var isPack = '<input type="hidden" id="isPacket" value="' + item.IsPack + '"/>';
    var packetID = 0;
    var isMinor = 0;
    var Description = "";
    var BalQty = 0;
    BalQty = ReqQty - DeliveredQty;

    //console.log(item.PartNo);
    //console.log(PartNO);

    // SalePrice = parseFloat(item.SalePrice).toFixed(3);
    var cPrice = '<input type="hidden" id="costPrice" value="' + item.CostPrice + '"/>';
    var markup = "<tr><td hidden><input type='checkbox' name='record'></td><td>" + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td id=unitCode >" + UnitCode + "</td><td id='ReqQty'>" + ReqQty + "</td><td id='ReadyQty'>" + ReadyQty + "</td><td id='ProductQty' contenteditable='false'>0</td><td id='DeliveredQty'>" + DeliveredQty + "</td><td id='BalQty'>" + BalQty + "</td><td id='ProductSalePrice' contenteditable='true'>" + parseFloat(SalePrice).toFixed(3) + "</td><td id='ProductSubTotal'>0</td> <td id='btnAddPackages'><a href='#' data-toggle='modal' data-target='#exampleModal' onclick='getPackagesModal(" + ProductID + ",\"" + PartNO + "\"," + ReqQty+")' >Add Packages</a></td>\
                       </tr>";

    $("#tblProduct tbody").append(markup);
    $tableItemCounter++;
    $addedProductIDs.push(item.ProductID);
    proIdEdit = item.ProductID;
    getNewTotal();
}

function getPackagesModal(ProductID, ProductName,Qty) {


    $("#rawProductQty").val(null);
    $("#rawProductID").val(null).trigger('change');
    //   console.log("in function->" + ProductID)
    let pID = +ProductID || 0;

    $("#itemQtyField").val(0);
    $("#pkgQtyField").val(0);
    $("#balQtyField").val(0);
    $("#pkgDeliveredField").val(0);

    if (pID > 0 && OrderId > 0) {
        $("#lbl_itemID").text(ProductName);
        $("#hdnItemID").val(pID);

        /////////
        var json = { "OrderID": OrderId, "ItemID": pID };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/SalesOrder/GetItemPackageDetails',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                console.log("data received...");
                console.log(data);
                var itemWiseProducts = data;

                if (itemWiseProducts.length) {
                    const found = itemWiseProducts.some(el => el.ItemID === pID);
                    // console.log('found array->');

                    if (!found) {
                        // console.log(found)
                        console.log(itemWiseProducts)
                        $("#addPackagesTable tbody").empty();
                        swal('', 'not found', 'error');
                    }
                    else {
                        // console.log(found)
                        // console.log(itemWiseProducts)
                        var newArr = itemWiseProducts.filter(x => x.ItemID === pID);
                        var DeliveredQty = 0;

                        console.log('pID->' + pID);
                        console.log(newArr)
                        if (newArr.length) {
                            $("#itemQtyField").val(Qty);

                            $("#pkgQtyField").val(0);
                            $("#addPackagesTable tbody").empty();
                            for (var i = 0; i < newArr.length; i++) {

                                if (newArr[i].IsChkBoxEnabled == true) {
                                    var markup = "<tr>\
                   <td  id='chkBox_td'> <input type='checkbox' id='chkbox_tbl'> </td> \
                   <td hidden id='hdnItemID_td'> "+ newArr[i].ItemID + " </td> \
                   <td hidden id='rowID_td'> "+ newArr[i].RowID + " </td> \
                   <td hidden id='hdnOrderID_td'> "+ newArr[i].OrderID + " </td> \
                   <td  id='hdnDescription_td'> "+ newArr[i].Description + " </td> \
                   <td id='hdnRawProductQty_td'>  "+ newArr[i].Qty + " </td> \
                   <td id='hdnRawProductDelQty_td' contenteditable='false'> "+ newArr[i].DeliverQty + " </td> \
                   <td  id='hdnRawProductDeliveredQty_td'> "+ newArr[i].DeliveredQty + " </td> \
                                    <td  id='hdnPackageId_td' hidden> "+ newArr[i].PackageId + " </td> \
                    </tr>";
                                }
                                else if (+newArr[i].Qty != +newArr[i].DeliveredQty && newArr[i].IsChkBoxEnabled == false && newArr[i].DeliveredQty <= 0) {
                                    var markup = "<tr>\
                   <td  id='chkBox_td'><span style='color:red;'>Un-Delivered</span> <input hidden type='checkbox' id='chkbox_tbl'> </td> \
                   <td hidden id='hdnItemID_td'> "+ newArr[i].ItemID + " </td> \
                   <td hidden id='rowID_td'> "+ newArr[i].RowID + " </td> \
                   <td hidden id='hdnOrderID_td'> "+ newArr[i].OrderID + " </td> \
                   <td  id='hdnDescription_td'> "+ newArr[i].Description + " </td> \
                   <td id='hdnRawProductQty_td'>  "+ newArr[i].Qty + " </td> \
                   <td id='hdnRawProductDelQty_td' contenteditable='false'> "+ newArr[i].DeliverQty + " </td> \
                   <td  id='hdnRawProductDeliveredQty_td'> "+ newArr[i].DeliveredQty + " </td> \
                                    <td  id='hdnPackageId_td' hidden> "+ newArr[i].PackageId + " </td> \
                    </tr>";
                                }
                                else if (+newArr[i].Qty != +newArr[i].DeliveredQty && newArr[i].IsChkBoxEnabled == false) {
                                    var markup = "<tr>\
                   <td  id='chkBox_td'><span style='color:orange;'>Partial-Delivered</span> <input hidden type='checkbox' id='chkbox_tbl'> </td> \
                   <td hidden id='hdnItemID_td'> "+ newArr[i].ItemID + " </td> \
                   <td hidden id='rowID_td'> "+ newArr[i].RowID + " </td> \
                   <td hidden id='hdnOrderID_td'> "+ newArr[i].OrderID + " </td> \
                   <td  id='hdnDescription_td'> "+ newArr[i].Description + " </td> \
                   <td id='hdnRawProductQty_td'>  "+ newArr[i].Qty + " </td> \
                   <td id='hdnRawProductDelQty_td' contenteditable='false'> "+ newArr[i].DeliverQty + " </td> \
                   <td  id='hdnRawProductDeliveredQty_td'> "+ newArr[i].DeliveredQty + " </td> \
                                    <td  id='hdnPackageId_td' hidden> "+ newArr[i].PackageId + " </td> \
                    </tr>";
                                }
                                else {
                                    var markup = "<tr>\
                   <td  id='chkBox_td'><span style='color:green;'>Delivered</span> <input hidden type='checkbox' id='chkbox_tbl'> </td> \
                   <td hidden id='hdnItemID_td'> "+ newArr[i].ItemID + " </td> \
                   <td hidden id='rowID_td'> "+ newArr[i].RowID + " </td> \
                   <td hidden id='hdnOrderID_td'> "+ newArr[i].OrderID + " </td> \
                   <td  id='hdnDescription_td'> "+ newArr[i].Description + " </td> \
                   <td id='hdnRawProductQty_td'>  "+ newArr[i].Qty + " </td> \
                   <td id='hdnRawProductDelQty_td' contenteditable='false'> "+ newArr[i].DeliverQty + " </td> \
                   <td  id='hdnRawProductDeliveredQty_td'> "+ newArr[i].DeliveredQty + " </td> \
                   <td  id='hdnPackageId_td' hidden> "+ newArr[i].PackageId + " </td> \
                    </tr>";
                                }

                                DeliveredQty += +newArr[i].DeliveredQty;
                                itemWiseDescArray.push({ ItemID: pID })
                                $("#addPackagesTable tbody").append(markup);
                            }


                            $("#pkgDeliveredField").val(+DeliveredQty);

                            $("#balQtyField").val(+(Qty - DeliveredQty));
                        }

                    }

                }
                else {
                    swal("No Data Found", "Challan Already Created! No packages found to add.", "warning");
                }
            },
            error: function (err) { console.log(err); }
        });
        ///////////

    }
    else {
        $("#lbl_itemID").val(null);
        $("#hdnItemID").val(null);

    }
}

$("#addPackagesTable").on('click', function () {
    
    if (pkgList.length) {
        const ids = pkgList.map(o => o.RowID)
        pkgList = pkgList.filter(({ RowID }, ind) => !ids.includes(RowID, ind + 1));
    }
    
    var qty = 0;
    var itemID = 0;
    var uncheckRowCount = 0;
    $('#addPackagesTable tbody tr').each(function (i, n) {

        var $row = $(n);
        var chkVal = $row.find('input[id*="chkbox_tbl"]').val();
        var RowID = +$row.find("#rowID_td").text() || 0;
        var ItemID = +$row.find("#hdnItemID_td").text() || 0;
        var PackageId = $row.find("#hdnPackageId_td").text() || null;

        var DelQty = +$row.find("#hdnRawProductDelQty_td").text() || 0;


        var PQty = +$row.find("#hdnRawProductQty_td").text() || 0;
        var DeliveredQty = +$row.find("#hdnRawProductDeliveredQty_td").text() || 0;
        var orderID = +$row.find("#hdnOrderID_td").text() || 0;

        var desc = $row.find("#hdnDescription_td").text();

        itemID = ItemID;
        
        if ($row.find('input[id*="chkbox_tbl"]').is(":checked")) {
            //console.log('checked->' + RowID + '->' + chkVal);
            
            qty += DelQty;

            
            pkgList.push({ RowID: RowID, ItemID: ItemID, OrderID: orderID, Description: desc, Qty: PQty, DeliverQty: DelQty, DeliveredQty: DeliveredQty,PackageId:PackageId });
            //console.log(pkgList)
        }
        else {

            var index = pkgList.map(x => {
                return x.RowID;
            }).indexOf(RowID);
            //console.log("index->")
            //console.log(index)
            if (index !== -1) {
                pkgList.splice(index, 1);
            }

            //console.log(pkgList)
            uncheckRowCount++;
            //console.log('not-checked->' + RowID + '->' + chkVal);
            
            //console.log("filtered->");
            //console.log(pkgList)

        }
        console.log(pkgList)
        
        
    });

    //console.log(+$('#addPackagesTable tbody tr').length);
    //console.log("uncheckRowCount->" + (+uncheckRowCount));
    if (+uncheckRowCount === +$('#addPackagesTable tbody tr').length) {

        console.log("resetted qty")
        qty = 0;
        //console.log(qty)
        //console.log(itemID)
    }

    if (itemID > 0) {

        var itemQty = +$("#itemQtyField").val() || 0;
        var pkgDelQty = +$("#pkgDeliveredField").val() || 0;

        var bal = itemQty -( qty + pkgDelQty);

        //$("#pkgDeliveredField").val(+DeliveredQty);
        $("#balQtyField").val(+bal);
        
        $("#pkgQtyField").val(qty);

        calculateMasterTableDeliveredQty(itemID, +qty);
    }
})

$('#selectAll').click(function (e) {
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
});

$('#addPackagesTable').keyup(function (e) {
    var itemQty = +$("#itemQtyField").val() || 0;
    var balQty = +$("#balQtyField").val() || 0;
    var pkgQty = 0;
    $('#addPackagesTable tbody tr').each(function (i, n) {
        var $row = $(n);
        //var Qty = $row.find('input[id*="ProductQty"]').val();
        var DelQty = +$row.find("#hdnRawProductDelQty_td").text() || 0;
       // console.log("DelQty->" + DelQty);
        pkgQty += DelQty;

       // console.log("pkgQty->" + pkgQty);
       
        var BalQty = 0;
        BalQty = itemQty - pkgQty;
        //console.log("BalQty->" + BalQty);
        //console.log("itemQty->" + itemQty);
        
        if (+BalQty >= 0) {

            $("#balQtyField").val(+BalQty.toFixed(2));
            $("#pkgQtyField").val(+pkgQty.toFixed(2));
           
        }
        else {
            var DelQty = +$row.find("#hdnRawProductDelQty_td").text() || 0;
            $row.find("#hdnRawProductDelQty_td").text(0);
            
            pkgQty -= DelQty;

            var BalQty = 0;
            BalQty = itemQty - pkgQty;

            $("#balQtyField").val(+BalQty.toFixed(2));
            $("#pkgQtyField").val(+pkgQty.toFixed(2));
        }


    });

});


$('#tblProduct').keyup(function (e) {
    // console.log("in")
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        //var Qty = $row.find('input[id*="ProductQty"]').val();
        var Qty = +$row.find("#ProductQty").text() || 0;
        var ReadyQty = +$row.find("#ReadyQty").text() || 0;

        var DeliveredQty = +$row.find("#DeliveredQty").text() || 0;
        var ReqQty = +$row.find("#ReqQty").text() || 0;
        var prevBalQty = ReqQty - DeliveredQty;
        var BalQty = 0;
        BalQty = ReqQty - (DeliveredQty + Qty);

        var SalePrice = +$row.find("#ProductSalePrice").text() || 0;
        // console.log(ReadyQty)
        // console.log(Qty)
        if (Qty > ReadyQty) {
            toastr.warning("Delivering Qty must be less than or equal to Ready Qty");
            $row.find("#ProductQty").text(0);
            $row.find("#BalQty").text(+prevBalQty.toFixed(2) || 0);
        }
        else {
            let SubTotal = Qty * SalePrice;

            $row.find("#BalQty").text(+BalQty.toFixed(2));
            $row.find("#ProductSubTotal").text(+SubTotal.toFixed(2));
            calcTotal();
        }


    });

});


function calculateMasterTableDeliveredQty(ItemID, delQty) {
    
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = +$row.find('input[id*="productID"]').val() || 0;

        if (pId == ItemID && delQty >= 0) {
            console.log("inside calculateMasterTableDeliveredQty");
            console.log(pId)
            console.log(delQty)
            var Qty = +$row.find("#ProductQty").text() || 0;
            var ReadyQty = +$row.find("#ReadyQty").text() || 0;

            var DeliveredQty = +$row.find("#DeliveredQty").text() || 0;
            var ReqQty = +$row.find("#ReqQty").text() || 0;
            var prevBalQty = ReqQty - DeliveredQty;
            var BalQty = 0;
            BalQty = ReqQty - (DeliveredQty + Qty);

            var SalePrice = +$row.find("#ProductSalePrice").text() || 0;
            // console.log(ReadyQty)
            // console.log(Qty)

            let SubTotal = delQty * SalePrice;
            if (Qty > ReadyQty) {

                
                toastr.warning("Delivering Qty must be less than or equal to Ready Qty");
                $row.find("#ProductQty").text(0);
                $row.find("#BalQty").text(+prevBalQty.toFixed(2) || 0);
            }
            else {
                
                $row.find("#BalQty").text(+BalQty.toFixed(2));
                $row.find("#ProductQty").text(+delQty.toFixed(2));
                
            }
            $row.find("#ProductSubTotal").text(+SubTotal.toFixed(2));
            calcTotal();
        }
        
    });
}

//old logic 
//$('#exampleModal').on('hidden.bs.modal', function (e) {
//    overrideTotalItemQty();
//})

//function overrideTotalItemQty() {
//    console.log("in");
//    uiBlock();
   
//    $('#tblProduct tbody tr').each(function (i, n) {
//        var $row = $(n);
//        var pId = +$row.find('input[id*="productID"]').val() || 0;
//        var Qty = +$row.find('#ProductQty').text() || 0;
//        var ReadyQty = +$row.find('#ReadyQty').text() || 0;
//        console.log(pId)
//        if (pId > 0) {
//            var pkgDelQty = 0;
//            var arr = [];
//            $('#addPackagesTable tbody tr').each(function (i, n) {
//                var $row = $(n);
//                var itemID = +$row.find('#hdnItemID_td').text() || null;
//                if (pId == itemID) {
//                    var DelQty = +$row.find('#hdnRawProductDelQty_td').text() || 0;
//                    var ProdQty = +$row.find('#hdnRawProductQty_td').text() || 0;
//                    var desc = $row.find('#hdnDescription_td').text() || 0;
//                    var OrderID = $row.find('#hdnOrderID_td').text() || 0;
//                    pkgDelQty += DelQty;

//                    arr.push({ ItemID: itemID, Description: desc, DeliverQty: DelQty, Qty: ProdQty, OrderID: OrderID  });
//                    console.log(arr)
//                }
               
//            });
//            console.log("pId->" + pId+"pkgDelQty->"+(+pkgDelQty))
//            //  var arr = itemWiseProducts.filter(x => x.ItemID === pId);

//            // if (arr.length) {
//            //    totalQty = arr.reduce((accum, item) => accum + item.Qty, 0);
//            console.log('pkgQty->' + pkgDelQty)
//            console.log('Qty->' + Qty)
//            console.log('ReadyQty->' + ReadyQty)
//           // if (+pkgDelQty > +Qty) {
//            if (+Qty > +ReadyQty) {
//                uiUnBlock();
//                toastr.warning('Package del qty must be less than or equal to item del qty! challan will not submit');
//                $("#btnSubmit").prop('disabled', true);

//            }
//            else {

//                if (+pkgDelQty > 0 && +pkgDelQty <= +Qty) {
//                    console.log("in else")
//                    console.log(arr)
//                    if (arr.length) {
//                        var json = JSON.stringify({

//                            'packageModel': arr
//                        });
//                        //var json = { "OrderID": OrderId, "ItemID": pID };
//                        $.ajax({
//                            type: "POST",
//                            contentType: "application/json; charset=utf-8",
//                            url: '/SalesOrder/UpdatePackageDetails',
//                            async: true,
//                            data: json,
//                            success: function (data) {
//                                console.log("status: "+data);
//                                uiUnBlock();
//                            },
//                            error: function (err) { console.log(err); uiUnBlock(); }
//                        });
//                    }
//                }
//                uiUnBlock();
//                $("#btnSubmit").prop('disabled', false);
//            }
//            // }

//        }

//    });

//}

////////////
//get other edit details

$('#Qty').on('input', function (e) {
    var qty = $('#Qty').val();
    var unitPerCarton = $('#unitPerCarton').val();
    calcSubTotal();
    calcTotalWeight();
});
$('#BOX').on('input', function (e) {
    calcSubTotal();
    calcTotalWeight();
});
$('#SalePrice').on('input', function (e) {
    calcSubTotal();
});
$('#SalePrice').change(function () {
    var price = parseFloat($('#SalePrice').val()) || 0;
    var costPrice = parseFloat($('#hdnCostPrice').val()) || 0;
    if (price < costPrice) {
        swal("Error", "The sale price is below cost price of this item! " + costPrice + "PKR", "error");
        // $("#SalePrice").val('');
    }
});
function calcSubTotal() {
    //console.log("unitPerCarton=" + unitPerCarton);
    var ctnQty = parseFloat($('#Qty').val() || 0);
    var ctnPrice = parseFloat($('#SalePrice').val());
    var qty = ctnQty;
    var price = ctnPrice;
    if (($.isNumeric(price)) && ($.isNumeric(qty))) {
        var amount = parseFloat(qty * price);
        $('#SubTotal').val(parseFloat(amount).toFixed(2));
        calcTotal();
    }
    else {
        $('#subAmount').val(parseFloat(amount).toFixed(2));
    }
}

function calcTotalWeight() {
    //console.log("Total Wright ");
    var unitPerCarton = parseInt($('#unitPerCarton').val());
    var ctnQty = parseInt($('#Qty').val() || 0);
    var boxQty = parseInt($('#BOX').val() || 0);
    var totalQty = parseInt((ctnQty * unitPerCarton));
    var isMinor = $('#isMinor').val();
    var isPack = $('#UOMID').val();
    var levelID = $('#LevelID').val();
    var qty = totalQty;

    if (isPack == 0) {
        qty = totalQty * 1000;
    }
    else if (isPack == 1) {
        qty = boxQty * 1000;
    }
    else if (isPack == 2) {
        qty = boxQty;
    }
    else {
        qty = totalQty;
    }
    $('#totalWeight').val(parseInt(qty));
}
$("#UOMID").change(function () {
    var isPack = $('#UOMID').val();
    var isMinor = $('#IsMinor').val();
    var unitPerCarton = parseInt($('#unitPerCarton').val());

    if (isPack == 0) {
        //var price = parseFloat(salePrice / unitPerCarton);
        //$('#SalePrice').val(price);
        document.getElementById("Qty").readOnly = false;
        document.getElementById("BOX").readOnly = true;
    }
    else if (isPack == 1 || isPack == 2) {// && isMinor == 0)  { // open 
        //var price = parseFloat(salePrice / unitPerCarton / 1000);
        //$('#SalePrice').val(price);
        document.getElementById("BOX").readOnly = false;
        document.getElementById("Qty").readOnly = true;
    }
    var unitPerCarton = parseInt($('#unitPerCarton').val());
    var ctnPrice = parseFloat($('#hdnSalePrice').val());
    // console.log('ctnPrice=' + ctnPrice);
    var boxPrice = parseFloat(ctnPrice / unitPerCarton);
    var levelID = $('#LevelID').val();
    if (isPack == 0) {
        boxPrice = parseFloat(ctnPrice * unitPerCarton * 1000);

    }
    else if (isPack == 1) {
        boxPrice = parseFloat(ctnPrice * 1000);
    }
    else if (isPack == 2) {
        boxPrice = parseFloat(ctnPrice);
    }
    var price = boxPrice;
    $('#SalePrice').val(price)
});

////////////////////////
$("#BranchID").change(function () {
    // console.log('on branch change');
    var branchids = $('#BranchID option:selected').val();
    // console.log("branch id" + branchids);
    if (branchids == null || branchids == 'undefiend' || branchids == 0) {
        $("#ddlVehCode").prop('disabled', true);
        $("#ddlPartNumber").prop('disabled', true);
    }
    else {

        $("#ddlVehCode").prop('disabled', false);
        $("#ddlPartNumber").prop('disabled', false);
    }
});

$("#AccountID").change(function () {
    var customerID = $('#AccountID').val();
    $('#hdnAccountID').val(customerID);

    getCustomerDetail(customerID);
});
$("#CustomerCode").change(function () {
    var customerID = $('#CustomerCode').val();
    $('#hdnAccountID').val(customerID);
    $('#AccountID').val(customerID).trigger('change.select2');
    $('#CustomerPhone').val(customerID).trigger('change.select2');
    getCustomerDetail(customerID);
});
$("#CustomerPhone").change(function () {
    var customerID = $('#CustomerPhone').val();
    $('#hdnAccountID').val(customerID);
    $('#AccountID').val(customerID).trigger('change.select2');
    $('#CustomerCode').val(customerID).trigger('change.select2');
    getCustomerDetail(customerID);
});
$("#ddlPartNumber").change(function () {
    var pId = $('#ddlPartNumber').val();
    $("#addRow").prop('disabled', false);
    if (pId != "" || pId > 0) { getDetail(pId); }
});
var validate = function (e) {
    var t = e.value;
    e.value = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 4)) : t;
}
function calcTotal() {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('#ProductSubTotal').text());
        total += parseFloat(subTotal);
    });
    $("#subAmount").val(parseFloat(total).toFixed(2));
    $("#totalAmount").val(parseFloat(total).toFixed(2));
    $("#finalAmountWithVAT").val(parseFloat(total).toFixed(2));
    calcDiscount();
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(total).toFixed(2);
    }
    //var customerBalance = $('#customerBalance').val();
    //var creditLimit = parseFloat($('#customerCrLimit').val()) - parseFloat($('#customerBalance').val());
    //if (creditLimit != "" && total > creditLimit) { swal("Credit Limit", "You can not sale more than Credit Limit! ", "error"); }

}

$("#addRow").click(function () {
    var vehCode = $("#ddlVehCode :selected").text();  // 1
    var ProductID = $("#ddlPartNumber").val(); // hidden
    var PartNO = $("#ddlPartNumber :selected").text();  // 1
    var Description = $("#Description").text(); // 2
    var UnitCode = $("#UnitCode").text();
    var Qty = $("#Qty").val();
    var Packet = $("#Packet").val();
    var costPrice = parseFloat($('#hdnCostPrice').val()) || 0;
    var SalePrice = $("#SalePrice").val(); // 7
    var SubTotal = $("#SubTotal").val();
    var stock = parseFloat($('#Stock').val());
    var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';
    var isPack = '<input type="hidden" id="isPacket" value="' + $("#isPacket").val() + '"/>';
    var ctnPrice = parseFloat($('#SalePrice').val());

    var packetID = 0;
    var cPrice = '<input type="hidden" id="costPrice" value="' + costPrice + '"/>';
    if (ProductID > 0 && parseInt(Qty) > 0 && Number(parseFloat(SalePrice)) && SubTotal > 0) {
        var index = $.inArray(ProductID, $addedProductIDs);
        if (index >= 0) {
            swal("Error", "Product Already Added!", "error");
        } else {
            //editable
            // with Box and Carton
            var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td id=unitCode hidden>" + isPack + UnitCode + "</td><td id='ProductQty'>" + Qty + "</td><td id='Packet' hidden>" + Packet + "</td><td id='ProductSalePrice'>" + parseFloat(SalePrice).toFixed(3) + "</td><td id='ProductSubTotal'>" + SubTotal + "</td></tr>";
            $("#tblProduct tbody").append(markup);
            $tableItemCounter++;
            $addedProductIDs.push(ProductID);
            clearFields();
            calcTotal();
            getNewTotal();
            $("#addRow").prop('disabled', true);
            $("#btnRemove").prop('disabled', false);
        }
    }
    else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
    //else if (parseInt(Qty) > stock) { swal("Error", "Stock Not Available!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseFloat(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
    else if (SalePrice == "" || SalePrice == "undefined" || !Number(parseFloat(SalePrice))) { swal("Error", "Please enter Sale Price!", "error"); }
});
function clearFields() {
    $("#isPack").val("");
    $("#Description").text("");
    $("#BarCode").val("");
    $("#Packet").val("");
    $("#Stock").val("");
    $("#Qty").val("");
    $("#SalePrice").val("");
    $("#SubTotal").val("");
    $("#ddlVehCode").focus();
    $('#ddlVehCode').val(null).trigger('change');
    $('#ddlPartNumber').val(null).trigger('change.select2');
}
function GetEditProductDetail(OrderId, ProductID, BranchID) {
    var json = { "OrderId": OrderId, "ProductID": ProductID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/GetEditProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#ddlPartNumber').val(data[0].ProductsList.ProductID).trigger('change.select2');
            $('#ddlVehCode').val(data[0].ProductsList.CatID).trigger('change.select2');
            $('#SubTotal').val(data[0].ProductsList.PTotal);

            var qtyBox = parseFloat(data[0].ProductsList.Qty);
            var unitPerCTN = 1;
            var qty = 0;
            var UnitCode = data[0].ProductsList.UnitCode;
            var SalePrice = data[0].ProductsList.SalePrice;
            var SubTotal = data[0].ProductsList.PTotal;
            var unitID = data[0].ProductsList.UnitID;
            var levelID = data[0].ProductsList.LevelID;
            var Box = 0;
            Qty = qtyBox;
            $('#Qty').val(Qty);
            if (data[0].ProductsList.IsPack == true) {
                $('#Packet').val(data[0].ProductsList.Packet);
                $("#Packet").prop('disabled', false);
            }
            else {
                $('#Packet').val();
                $("#Packet").prop('disabled', true);
            }
            $('#SalePrice').val(SalePrice);
            $('#UnitCode').val(UnitCode);

            $('#unitPerCarton').val(data[0].ProductsList.UnitPerCarton);
            var VatInput = data
            getDetail(data[0].ProductsList.ProductID);

            $("#addRow").prop('disabled', false);
            $("#btnRemove").prop('disabled', true);
        },
        error: function (err) { console.log(err); }
    });
}

function EditProduct(input) {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {

            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            var productID = row.find('input[id*="productID"]').val(); // find hidden id 
            //console.log(productID + "IN Edit Function");

            var index = $.inArray(productID, $addedProductIDs);
            if (index >= 0) { $addedProductIDs.splice(index, 1); }
            var BranchID = "";
            GetEditProductDetail(OrderId, productID, BranchID);

            calcTotal();
        }
    });
}
// Remove Selected Products 
function remove(input) {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {
            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            calcTotal();
            var productID = row.find('input[id*="productID"]').val(); // find hidden id 
            var index = $.inArray(productID, $addedProductIDs);
            if (index >= 0) { $addedProductIDs.splice(index, 1); }
            Count = $tableItemCounter;
        }
    });
}
// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function clearLabels() {
    $('#DescriptionTitle').text(''); $('#Description').text('');
    $('#SubsTitle').text(''); $('#SubstituteNo').text('');
    $('#GroupTitle').text(''); $('#Group').text('');
    $('#VehModelTitle').text(''); $('#VehModel').text('');
    $('#LocationTitle').text(''); $('#Location').text('');
    $('#UnitCodeTitle').text(''); $('#UnitCode').text('');
    $('#MinorUnitCodeTitle').text(''); $('#MinorUnitCode').text('');
    $('#LeastUnitCodeTitle').text(''); $('#LeastUnitCode').text('');
    $('#QtyPerUnitTitle').text(''); $('#QtyPerUnit').text('');
    $('#UnitPerCartonTitle').text(''); $('#UnitPerCarton').text('');
}

function getDetail(pId) {
    clearLabels();
    var accID = $('#hdnAccountID').val();
    // get product description
    var json = { "productId": pId };
    //ajaxCall("GET", json, "application/json; charset=utf-8", "/Purchase/getProductDetail", "json", onSuccess, onFailure);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            // console.log('data=' + data[0].qry.LeastUnitCode);
            // console.log('C.p=' + data[0].costPrice);
            var unitPerCarton = 1;
            var level = 0;
            if (data[0].qry.Description) { $('#DescriptionTitle').text("Desc: "); $('#Description').text(data[0].qry.Description); }
            if (data[0].qry.SubstituteNo) { $('#SubsTitle').text("Su No.: "); $('#SubstituteNo').text(data[0].qry.SubstituteNo); }
            if (data[0].qry.GroupName) { $('#GroupTitle').text("Group: "); $('#Group').text(data[0].qry.GroupName); }
            if (data[0].qry.VehicleName) { $('#VehModelTitle').text("Model: "); $('#VehModel').text(data[0].qry.VehicleName); }
            if (data[0].qry.Location) { $('#LocationTitle').text("Location: "); $('#Location').text(data[0].qry.Location); }
            if (data[0].qry.UnitCode) { $('#UnitCodeTitle').text("Pack Unit: "); $('#UnitCode').text(data[0].qry.UnitCode); }
            if (data[0].qry.OpenUnitCode) { $('#MinorUnitCodeTitle').text("Minor Unit: "); $('#MinorUnitCode').text(data[0].qry.OpenUnitCode); }
            if (data[0].qry.LeastUnitCode) { $('#LeastUnitCodeTitle').text("Least Unit: "); $('#LeastUnitCode').text(data[0].qry.LeastUnitCode); }
            if (data[0].qry.VehicleCode) {
                var vc = data.VehicleCode;
                $("#ddlVehCode option:contains(" + data[0].qry.VehicleCode + ")").attr("selected", true);
            }
            if (data[0].qry.UnitPerCtn) {
                unitPerCarton = data[0].qry.UnitPerCtn;
                $('#unitPerCarton').val(unitPerCarton);
                $('#UnitPerCtnTitle').text("Unit/Ctn: "); $('#UnitPerCtn').text(unitPerCarton);
            }
            if (data[0].qry.QtyPerUnit) {
                $('#QtyPerUnitTitle').text("Qty/Unit: "); $('#QtyPerUnit').text(data[0].qry.QtyPerUnit);
                $('#qtyPerUnit').val(data[0].qry.QtyPerUnit);
            }
            if (data[0].qry.IsMinor) {
                $('#isMinor').val(data[0].qry.IsMinor);
            }
            else {
                $('#isMinor').val('false');
            }
            if (data[0].qry.LevelID) {
                level = data[0].qry.LevelID;
                $('#LevelID').val(level);
            }
            if (data[0].qry.IsPacket == "1") {
                $('#isPacket').val(data[0].qry.IsPacket);
                document.getElementById("Packet").readOnly = false;
            }
            else if (data[0].qry.IsPacket == "0") {
                document.getElementById("Packet").readOnly = true;
                $('#isPacket').val('false');
            }
            if (data[0].qry.UnitCode) {

                $('#unitCode').val(data[0].qry.UnitCode);
            }

            var isPack = $('#isPacket').val();
            var qty = data[0].stock;
            $('#Stock').val(qty);

            $('#hdnCostPrice').val(data[0].costPrice);
            var salePrice = data[0].SalePrice;
            $('#hdnSalePrice').val(salePrice);
            $('#SalePrice').val(salePrice);

        },
        error: function (err) { console.log(err); }
    });
}
