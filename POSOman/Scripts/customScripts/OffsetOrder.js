﻿getSOID();
//  Get New SOID 
function getSOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/getNewSOID',
        async: true,
        success: function (data) {
            {
                $("#hdnSOID").val(data);
                SOID = data;
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

//$("#btnSave").click(function () {
//    try {
//        $('#btnSave').prop('disabled', true);
//        insert();
//    }
//    catch (err) {
//        $('#btnSave').prop('disabled', false);
//        console.log(err);
//    }
//})


function insert() {
    var saleDetails = [];
    let ItemDescription = $("#ItemDescription").val() || null;
    let pId = 1;
    let qty = +$("#Quantity").val() || 0;
    let CardQuality = +$("#CardQuality").val() || 0;
    let Length = +$("#Length").val() || 0;
    let Width = +$("#Width").val() || 0;
    let CardLength = +$("#CardLength").val() || 0;
    let CardWidth = +$("#CardWidth").val() || 0;
    let CutLength = +$("#CutLength").val() || 0;
    let CutWidth = +$("#CutWidth").val() || 0;
    let Upping = +$("#Upping").val() || 0;
    let RatePerKG = +$("#RatePerKG").val() || 0;
    let CardGSM = +$("#CardGSM").val() || 0;
    let NumberOfSheets = +$("#NumberOfSheets").val() || 0;
    let Impression = +$("#Impression").val() || 0;
    let Wastage = +$("#Wastage").val() || 0;
    let TotalNumberOfSheets = +$("#TotalNumberOfSheets").val() || 0;
    let PerSheetCost = +$("#PerSheetCost").val() || 0;
    let MachinePlate = +$("#MachinePlate").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineGroundCost = +$("#MachineGroundCost").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let MachineTextCost = +$("#MachineTextCost").val() || 0;
    let MachinePlates = +$("#MachinePlates").val() || 0;
    let MachinePlateCost = +$("#MachinePlateCost").val() || 0;
    let MachinePrintingCost = +$("#MachinePrintingCost").val() || 0;
    let TotalSheetCost = +$("#TotalSheetCost").val() || 0;
    let LaminationCost = +$("#LaminationCost").val() || 0;
    let DieCost = +$("#DieCost").val() || 0;
    let DieCuttingCost = +$("#DieCuttingCost").val() || 0;
    let UVCost = +$("#UVCost").val() || 0;
    let SlittingCost = +$("#SlittingCost").val() || 0;
    let DesignCost = +$("#DesignCost").val() || 0;
    let FoilPrintingCost = +$("#FoilPrintingCost").val() || 0;
    let FinalCostPerPc = +$("#FinalCostPerPc").val() || 0;
    let LaminationTypeID = null;
    let BranchId = 9001;

    if ($('#GlossLaminationCost').is(':checked')) {
        LaminationTypeID = 1;
       
    }
    if ($('#MatLaminationCost').is(':checked')) {
        LaminationTypeID = 2;
       
    }
    
    let MarkupRate = +$("#MarkupRate").val() || 0;
    let MarkedUpPrice = +$("#MarkedUpPrice").val() || 0;

    saleDetails.push({
        PartNo: 'Card Quality',
        ProductId: 1,
        ItemDescription: ItemDescription,
        Qty: qty,
       // SalePrice: SalePrice,
        Total: TotalSheetCost,
        BranchID: BranchId,
        CardQuality: CardQuality,
        Length: Length,
        Width: Width,
        CardLength: CardLength,
        CardWidth: CardWidth,
        CutLength: CutLength,
        CutWidth: CutWidth,
        Upping: Upping,
        RatePerKG: RatePerKG,
        CardGSM: CardGSM,
        NoOfSheets: NumberOfSheets,
        NOOfImpressions: Impression,
        Wastage: Wastage,
        TotalSheets: TotalNumberOfSheets,
        PerSheetCost: PerSheetCost,
        TotalSheetCost: TotalSheetCost,
        MachineID: MachinePlate,
        GroundColor: MachineGroundColor,
        GroundCost: MachineGroundCost,
        TextColor: MachineTextColor,
        TextCost: MachineTextCost,
        NoOfPlates: MachinePlates,
        PlateCost: MachinePlateCost,
        PrintingCost: MachinePrintingCost,
        LaminationTypeID: LaminationTypeID,
        LaminationCost: LaminationCost,
        DieCost: DieCost,
        DieCuttingCost: DieCuttingCost,
        UVCost: UVCost,
        SlittingCost: SlittingCost,
        DesignCost: DesignCost,
        FoilPrintingCost: FoilPrintingCost,
        FinalCostPerPc: FinalCostPerPc
    });
    if (saleDetails.length && MarkedUpPrice > 0) {
       
       
        var data = {
            'AccountID': $('#AccountID').val(),
            'SalePersonAccID': $('#SalePersonAccountID').val(),
            //'SOID': $("#hdnSOID").val(),
            'BranchID': BranchId,
            'OffsetTypeID': 1,
            'TotalAmount': MarkedUpPrice,
            'MarkupRate': MarkupRate,
            'MarkedupPrice': MarkedUpPrice,
            'SaleDetails': saleDetails
        };
        var json = JSON.stringify({
            'model': data,
           
        });
        //console.log(json);
       // ajaxCall("POST", json, "application/json; charset=utf-8", "/MachinePlate/SaveOffsetOrder", "json", onSuccess, onFailure);

        function onSuccess(Result) {
            if (Result > 0) {
                uiUnBlock();
                window.location.href = '/MachinePlate/OffsetOrders';
                

            } else {
                uiUnBlock();
                $('#btnSave').prop('disabled', false);
              
                alert("Some error Ocurred! Please Check Your Entries");
            }
        }

        function onFailure(error) {
            if (error.statusText == "OK") {
                console.log("OK");
            } else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    } else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);
       
        swal("", "Some error Ocurred! Please Check Rates!", "error");
    }
}
