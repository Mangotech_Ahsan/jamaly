﻿function IsOverlappingMinMax(min, max, minNew, maxNew) {
    if ((min <= minNew && max >= minNew) ||
        (max >= minNew && max <= maxNew) ||
        (min <= minNew && max >= maxNew) ||
        (maxNew >= min && maxNew <= max)
        ) {
        return true;
    }
    return false;
}
function ConvertJsonDateString(jsonDate) {
    var shortDate = null;
    if (jsonDate) {
        var regex = /-?\d+/;
        var matches = regex.exec(jsonDate);
        var dt = new Date(parseInt(matches[0]));
        var month = dt.getMonth() + 1;
        var monthString = month > 9 ? month : '0' + month;
        var day = dt.getDate();
        var dayString = day > 9 ? day : '0' + day;
        var year = dt.getFullYear();
        //shortDate = monthString + '-' + dayString + '-' + year;
        shortDate = year + '-' + monthString + '-' + dayString;
    }
    return shortDate;
};

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 189) {
        return false;
    }
    else if (charCode == 189 || charCode == 46 || (charCode > 31 && (charCode < 48 || charCode > 57))) {
        return false;
    }

    else if (charCode == 45) {
        if (evt.currentTarget.id == "TxtTo") {
            $("#TxtTo").val(' '); return false;
        }
        else if (evt.currentTarget.id == "TxtFrom") {
            $("#TxtFrom").val(' '); return false;
        }
        else if (evt.currentTarget.id == "Txtquantitymin") {
            $("#Txtquantitymin").val(' '); return false;
        }
        else if (evt.currentTarget.id == "Txtquantitymax") {
            $("#Txtquantitymax").val(' '); return false;
        }
        else if (evt.currentTarget.id == "TxtFrom") {
            $("#TxtFrom").val(' '); return false;
        }
        else if (evt.currentTarget.id == "Txtmax") {
            $("#Txtmax").val(' '); return false;
        }
        else if (evt.currentTarget.id == "Txtmin") {
            $("#Txtmin").val(' '); return false;
        }
    }
    return true;
}


function isNumberKey2(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var val = evt.target.value;
    var lastcode = $(evt.target).attr("data-last-code");
    $(evt.target).attr("data-last-code", charCode);
    var hasDecimal = (val * 1 % 1 != 0);
    if (lastcode == 46 || lastcode == 44) {
        hasDecimal = true;
    }

    if (charCode == 45) {
        return false;
    }
    if ((hasDecimal && (charCode == 46 || charCode == 44)) && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    return true;
}

function NotAllowDecimalAndNegative(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 189) {
        return false;
    }
    else if (charCode == 189 || charCode == 46 || (charCode > 31 && (charCode < 48 || charCode > 57))) {
        return false;
    }

}


function AllowDecimalAndNegative(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var val = evt.target.value;
    var lastcode = $(evt.target).attr("data-last-code");
    $(evt.target).attr("data-last-code", charCode);
    var hasDecimal = (val * 1 % 1 != 0);
    if (lastcode == 46 || lastcode == 44) {
        hasDecimal = true;
    }


    if ((hasDecimal && (charCode == 46 || charCode == 44)) && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    return true;
}

function discountrange(evt) {
    var value = evt.target.value * 1;
    //  var dis = $("#TxtDiscount").val();
    var dis = evt.target.value;
    if (value >= 100) {
        //   $("#TxtDiscount").val(dis.slice(0, 2));

        $("#" + evt.target.id).val(dis.slice(0, 2));
        return true;

    }
    else if (value <= -100) {
        $("#" + evt.target.id).val(dis.slice(0, 3));
        return true;


    }
    return false;
}
function uiBlock(msg, el, loaderOnTop) {
    lastBlockedUI = el;
    if (msg == '' || msg == null || msg == "") {
        msg = "LOADING";
    }
    if (el) {
        jQuery(el).block({
            message: '<div class="loading-message loading-message-boxed"><img src="Images/loading-spinner-grey.gif" align="absmiddle"><span>&nbsp;&nbsp;' + msg + '...</span></div>',
            baseZ: 2000,
            css: {
                border: 'none',
                padding: '2px',
                backgroundColor: 'none',
            },
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.05,
                cursor: 'wait'
            }
        });
    } else {
        if (msg == '' || msg == null || msg == "") {
            msg = "LOADING";
        }
        $.blockUI({
            message: '<div class="loading-message loading-message-boxed"><img src="/Content/images/loading-spinner-grey.gif" align="absmiddle"><span>&nbsp;&nbsp;' + msg + '...</span></div>',
            baseZ: 2000,
            css: {
                border: 'none',
                padding: '2px',
                backgroundColor: 'none'
            },
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.05,
                cursor: 'wait'
            }
        });
    }
}

function uiUnBlock(msg, el) {
    if (msg == '' || msg == null || msg == "") {
        msg = "LOADING";
    }
    if (el) {
        jQuery(el).unblock({
            onUnblock: function () {
                jQuery(el).removeAttr("style");
            }
        });
    }
    else {
          $.unblockUI();
    }
}

function showConfirm(_text, _success, _cancel) {

    notyfy({
        layout: 'center',
        text: _text,
        modal: true,
        buttons: [
          {
              addClass: 'btn btn-primary', text: 'OK', onClick: function ($noty) {
                  _success();
                  $noty.close();
              }
          },
          {
              addClass: 'btn btn',
              text: 'Cancel',
              onClick: function ($noty) {
                  if (_cancel) {
                      _cancel();
                  }
                  $noty.close();
              }
          }
        ]
    });
    //notyfy(
    //{
    //    layout: 'top',
    //    text: text,
    //    type: 'confirm',
    //    dismissQueue: true,
    //    buttons: [{
    //        addClass: 'btn btn-success btn-small glyphicons btn-icon ok_2',
    //        text: '<i></i> Ok',
    //        onClick: function ($notyfy) {
    //            $notyfy.close();
    //            success();
    //        }
    //    }, {
    //        addClass: 'btn btn-danger btn-small glyphicons btn-icon remove_2',
    //        text: 'Cancel',
    //        onClick: function ($notyfy) {
    //            $notyfy.close();
    //        }
    //    }]
    //});
}

function showNotification(_message, _type) {

    switch (_type) {
        case 'success':
            toastr.success(_message);
            break;
        case 'error':
            toastr.error(_message)
            break;
        case 'warning':
            toastr.warning(_message)
            break;
    }
}


function XHRPOSTRequestForFormData(_url, _data, _onsuccess) {
    return $.ajax({
        type: "POST",
        url: _url,
        processData: false,
        contentType: false,
        data: _data,
        dataType: "json",
        beforeSend: function () {
            // uiBlock();
        },
        success: _onsuccess,
        error: function (error) {
            //uiUnBlock();
            if (error.status == 403) {
                RedirectLogin();
            }
        }
    });
}

function XHRGETRequest(_url, _data, _onsuccess) {
    $.ajax({
        type: "GET",
        url: _url,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: _data,
        beforeSend: function () {
            // uiBlock();
        },
        success: _onsuccess,
        error: function (error) { uiUnBlock(); }
    });
}

function XHRPOSTRequest(_url, _data, _onsuccess) {
    return $.ajax({
        type: "POST",
        url: _url,
        data: _data,
        dataType: "json",
        beforeSend: function () {
            // uiBlock();
        },
        success: _onsuccess,
        error: function (error) {
            //uiUnBlock();
            if (error.status == 403) {
                RedirectLogin();
            }
        }
    });
}

function XHRPOSTRequestAsync(_url, _data, _onsuccess) {
    $.ajax({
        type: "POST",
        url: _url,
        data: _data,
        dataType: "json",
        success: _onsuccess,
        error: function (error) {

            //  uiUnBlock();
            if (error.status == 403) {
                RedirectLogin();

            }

        }
    });
}

function RedirectLogin() {
    swal({
        title: "Session Expire",
        text: "Need to Re-Login",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
    }, function () {

        window.location.href = "/Home";
        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
    });
}
var roles = {
    AdministrativeAssistants: "1",
    SalesPeople: "2",
    Customer: "3",
    PurchaseManager: "4",
    Scheduler: "5",
    Vendor: "6",
    SubContractor: "7",
    Account: "8",
    Manager: "9",
    Admin: "10"
}

var visitstatus = {
    Pending: "Pending",
    Incomplete: "Incomplete",
    Complete: "Complete"
}

$(document).on('keydown', '.readonly', function (e) {
    e.preventDefault();
});
//$(".date input:eq(0),.BeginDate,.readonly").on('keydown',function (e) {
//    e.preventDefault();
//});