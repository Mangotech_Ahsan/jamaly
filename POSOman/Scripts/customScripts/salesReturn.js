﻿var chqDat = "";
var AccountID = 0;
var Expense = 0;
var salesVAT = 0;
var VATPercent = 0;
var vatAmount = 0;
var SalesDiscount = 0;
var discAmount = 0;
var BranchID = 0;
var PaymentAccountID = 0;
var DiscountPercent = 0;
var paymentStatus = "";
var prevAmountPaid = 0;
$("#Bank").prop("disabled", true);
$("#PaymentType").prop("disabled", true);
$(function () {
    var table = $('#tblSales').DataTable();

    $('#tblSales tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
    
    $('#btnSave').click(function () {
    $('#btnSave').prop('disabled', true);
        uiBlock();
        insert();        
    });
});

function calcTotal() {
    var total = 0;
    $('#tblReturn tbody tr').each(function (i, n) {
        var $row = $(n);
        //var subTotal = parseFloat($row.find('td:eq(13) input[id*="txtSub"]').val());  
        var subTotal = parseFloat($row.find('input[id*="txtSub"]').val());
        total += parseFloat(subTotal).toFixed(2);
    });
    $('#tblReturn tfoot tr').find('input[id*="txtTotal"]').val(parseFloat(total).toFixed(2));
    $('#TotalAmount').val(parseFloat(total).toFixed(2));
    $('#returnAmount').val(parseFloat(total).toFixed(2));
}
var invoiceTotal = 0;
function getInvoiceDetails(orderID) {    
    ajaxCall("GET", { "OrderID": orderID }, "application/json; charset=utf-8", "/SalesReturn/getInvoiceDetails",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;        
        var discountPerItem = 0;
       
        if (data.discountPerItem > 0)
            discountPerItem = data.discountPerItem;
            $("#tblReturn").dataTable().fnDestroy();
        DiscountPercent = data.DiscountPercent;
        $('#tblReturn').find('tbody').empty();
        if (ien > 0) {
            var totalQty = 0;
            var desiredStep = 1;
            //$("#steps-uid-0-t-" + desiredStep).click();
            var controlReturning = '<input id="rQty" type = "Number" min="0" max=' + returningBalance + ' step="0" style="width: 50px;" class="form-control"  value=0>';
            var html = '';
            for (var i = 0; i < ien; i++) {
                var qtyBox = parseFloat(data.qry[i].Qty);
                var packet = parseInt(data.qry[i].Packet);
                var unitPerCTN = parseInt(data.qry[i].UnitPerCarton);
                var qty = qtyBox;
                var isPack = data.qry[i].IsPack;
                var unitCode = data.qry[i].UnitCode;
                var subTotal = parseFloat(data.qry[i].Total).toFixed(2);
                var boxQty = 0;
                var returnedQty = parseInt(data.qry[i].ReturnedQty);
                
                
                var salePrice = parseFloat(subTotal / qty).toFixed(3);
              //  console.log("SP="+salePrice);
                var costPrice = data.qry[i].UnitPrice
                var isPackField = '<input type="hidden" id="isPacket" value="' + isPack + '"/>';
                var newSalePrice = salePrice;
                AccountID = data.qry[i].AccountID;
                SalesDiscount = data.qry[i].DiscountAmount;
                BranchID = data.qry[i].BranchID;
                salesVAT = parseFloat(data.qry[i].VAT);
                paymentStatus = data.qry[i].PaymentStatus;
                prevAmountPaid = data.qry[i].AmountPaid;
                invoiceTotal = parseFloat(data.qry[i].TotalAmount  + SalesDiscount).toFixed(2);                
                var returningBalance = parseFloat(qty - returnedQty);
	           
                html += '<tr>';
                html += '<td><input id="orderID" type="hidden" value="' +  data.qry[i].OrderID  + '">' + data.qry[i].SOID + '</td>';                
                html += '<td><input id="prodID" type="hidden" value="' + data.qry[i].ProductID + '">' + data.qry[i].PartNo + '</td>';
                html += '<td id=unitCode>' + unitCode + '</td>';
                html += '<td id=SaleQty><input id="costPrice" type="hidden" value="' + data.qry[i].UnitPrice + '">' + qty + '</td>';
                html += '<td hidden>' + packet + '</td>';
                //html += '<td id=boxQty>' + boxQty +unitCTN+ '</td>';
                html += '<td id=SalePrice>'  + parseFloat(newSalePrice).toFixed(3) + '</td>';
                html += '<td>'+ isPackField + data.qry[i].Total + '</td>';
                html += '<td id=OldReturnQty>' +  returnedQty + '</td>';
                html += '<input id="returningBalQty" type="hidden" value="' + returningBalance + '">';
                html += '<td> <input id="rQty" name="rQty" type = "number" oninput="validate(this)"  max=' + returningBalance + ' value=0 class="form-control" style="width: 80px;"> </td>';
                if (isPack == false) {
                    html += '<td hidden> <input id="rPacket" name="rPacket" type = "number" onkeypress="return isNumberKey(event)" value=0 class="form-control" style="width: 70px;" readonly> </td>';
                }
                else {
                    html += '<td hidden> <input id="rPacket" name="rPacket" type = "number" onkeypress="return isNumberKey(event)" value=0 class="form-control" style="width: 70px;"> </td>';
                }
                html += '<td><input type="number"  class="form-control" style="width: 100px;" name="sub"  id="txtAmount" disabled />' + '</td>';
                html += '</tr>';
            }            
            $('#tblReturn tbody').append(html);
            //if (paymentStatus == "Paid") {  }
            paymentOptions();
        }
        $('#tblReturn').DataTable({
            "paging": false,
            "ordering": false,
            "info": false
            //"columnDefs": [{
            //    "targets": '_all',
            //    "createdCell": function (td, cellData, rowData, row, col) {
            //        $(td).css('padding', '12px')
            //    }
            //}]
        });
        
    }
    function onFailure(err) {
        console.log(err);
    }
}
// Allow Decimal upto 2 digits
var validate = function (e) {
    var t = e.value;
    e.value = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 3)) : t;
}
// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
$('#tblReturn').keyup(function (e) {
    $field = $(e.target).closest("tr").find('input[id*="rQty"]');
    var returningQty = $(e.target).closest("tr").find('input[id*="rQty"]').val();
    var saleQty = ($(e.target).closest("tr").find("#SaleQty").text());
    var ctnPrice = parseFloat($(e.target).closest("tr").find("#SalePrice").text());    
    var price = ctnPrice;
    if (returningQty > Number($field.attr("max"))) {
        var max = $field.attr("max");
        $field.val(0);
        toastr.warning('Returning quantity must be equal or less than balance qty!')
        calcTotal();
    }
   
    if (($.isNumeric(price)) && ($.isNumeric(qty))) {
        var amount = parseFloat(qty * price);
        $('#SubTotal').val(parseFloat(amount).toFixed(2));
        calcTotal();
    }
    else {
        $('#subAmount').val(parseFloat(amount).toFixed(2));
    }
    var qty = returningQty;
    if (($.isNumeric(price)) && ($.isNumeric(qty))) {
        var amount = parseFloat(qty * price).toFixed(2);
        $(e.target).closest('tr').find('input[id*="txtAmount"]').val(amount);
        calcTotal();
    }
    else {
        $(e.target).closest('tr').find('input[id*="txtSub"]').val(''
        );
    }          
});
function calcTotal() {
    var total = 0;
    $('#tblReturn tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('input[id*="txtAmount"]').val());
        if ($.isNumeric(subTotal)) {
            total += parseFloat(subTotal);
        }       
    });
    $('#tblReturn tfoot tr').find('input[id*="txtTotal"]').val(parseFloat(total).toFixed(2));
    $('#returnAmount').val(parseFloat(total).toFixed(2));
    calcVAT();
    //$('#TotalPaying').val(total);    
}
$('#tblReturn').keyup(function (e) {
    calcTotal();
});
function paymentOptions() {
    if (paymentStatus == 'UnPaid') {
        document.getElementById("PaymentStatus").options[1].disabled = true;
        document.getElementById("PaymentStatus").options[2].disabled = true;
        $("#PaymentStatus option:contains(" + paymentStatus + ")").attr("selected", true);
    }
    else if (paymentStatus == 'Partial Paid') {
        document.getElementById("PaymentStatus").options[1].disabled = true;
        $("#PaymentStatus option:contains(" + paymentStatus + ")").attr("selected", true);        
        $("#amountPaid").prop("disabled", false);
        $("#PaymentType").prop("disabled", false);
        document.getElementById("amountPaid").value = 0;
    }    
}
function getNewTotal() {
    calcAmountPaid();
    var subTotal = $("#returnAmount").val();            
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(subTotal).toFixed(2);
    }
}
// in case of vat entered please do calculations
function calcVAT() {    
    // var vatAmount = VAT;
    var total = 0;
    var totalWithDisc = 0;
    if (salesVAT == "" || isNaN(salesVAT)) {
        vatAmount = 0;
        calcDiscount();
        total = parseFloat($('#tblReturn tfoot tr').find('input[id="txtTotal"]').val()).toFixed(2);
        totalWithDisc = total - parseFloat(discAmount).toFixed(2);        
    }
    else {
        total = parseFloat($('#tblReturn tfoot tr').find('input[id="txtTotal"]').val()).toFixed(2);
        var invTotalWithDisc = invoiceTotal - parseFloat(SalesDiscount).toFixed(2);
        var vat = parseFloat((salesVAT / invTotalWithDisc) * 100);
        calcDiscount();
        total = parseFloat($('#tblReturn tfoot tr').find('input[id="txtTotal"]').val()).toFixed(2);
        totalWithDisc = total - parseFloat(discAmount).toFixed(2);
        //console.log(vat + " =vat");
        vatAmount = parseFloat((vat * totalWithDisc) / 100);
    }
    console.log("total=" + total);
    console.log("totalWithDisc=" + totalWithDisc);
    console.log("vatAmount=" + vatAmount);
    var totalAmount = parseFloat(totalWithDisc + vatAmount);
    $('#returnAmount').val(parseFloat(totalAmount).toFixed(2));
}
// if Discount
function calcDiscount() {
    if (SalesDiscount == "" || isNaN(SalesDiscount)) { SalesDiscount = 0; discAmount = 0; }
    else {
        var total = $('#tblReturn tfoot tr').find('input[id="txtTotal"]').val();
       // var invTotalWOVAT = parseFloat(invoiceTotal - vatAmount).toFixed(2);
        //console.log("vatAmount=" + vatAmount);
        var invTotalWOVAT = total;
        var discPercent = parseFloat((SalesDiscount / invTotalWOVAT) * 100);
        if (discPercent == "" || isNaN(discPercent)) { discPercent = 0; discAmount = 0;}
        else {
            var total = $('#tblReturn tfoot tr').find('input[id="txtTotal"]').val();
            discAmount = parseFloat((discPercent * total) / 100);
        }
    }
}
// Payment Status DropDOwn, Action according to Payment Status Selection
$("#PaymentStatus").on("change", function () {
    var payStatusSelection = $("#PaymentStatus").val();
    var totalAmount = $("#returnAmount").val();
    
    if (payStatusSelection == 1) {
        $("#amountPaid").prop("disabled", true);
        getNewTotal();
        //document.getElementById("amountPaid").value = totalAmount;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("balanceAmount").value = 0;        
    }
        // Partial PAid
    else if (payStatusSelection == 2) {        
        $("#amountPaid").prop("disabled", false);
        $("#PaymentType").prop("disabled", false);
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
    }
        //Unpaid    
    else if (payStatusSelection == 3) {        
        $("#amountPaid").prop("disabled", true);
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;        
        document.getElementById("PaymentType").selectedIndex = "0";
        $("#PaymentType").prop("disabled", true);
        disableBank();
    }   
});
function disableBank() {
    $("#Bank").prop("disabled", true);
    $("#chqNumber").prop("disabled", true);
    $("#chqDate").prop("disabled", true);
}
// Payment Type Change 
$("#PaymentType").on("change", function () {
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //Cash
    if (PaymentTypeID == 1) {        
        $("#Bank").prop("disabled", true);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);
    }
        // Bank
    else if (PaymentTypeID == 2) {        
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);

    }
        // Cheque
    else if (PaymentTypeID == 3) {        
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", false);
        $("#chqDate").prop("disabled", false);
    }
});
// calculate balance if amountPaid entered and make changes every where needed
function calcAmountPaid() {
    var amountPaid = $("#amountPaid").val();
    var finalAmount = $('#returnAmount').val();
    var checkAmount = 0;

    if (prevAmountPaid > finalAmount) {
        checkAmount = finalAmount;
    }
    else if (prevAmountPaid < finalAmount) {
        checkAmount = prevAmountPaid;
    }
    else {
        checkAmount = finalAmount;
    }
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 2) {
        
        if (parseFloat(amountPaid) > parseFloat(checkAmount)) {
            toastr.warning('Partial ! AmountPaid should be less than total amount ')
            $("#amountPaid").val(0)
        }
        else {
            var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
            $("#balanceAmount").val(parseFloat(balance).toFixed(2));
        }
    }
    else {

        var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
        $("#balanceAmount").val(parseFloat(balance).toFixed(2));
    }
}
$("#amountPaid").on("input", function () {
    calcAmountPaid();
})
function insert() {
    uiBlock();
    calcDiscount();
    calcVAT();
    var soReturn = [];
    var stockLog = [];
    var total = 0;
    var qtyVE = 0;
    var COGS = 0;
    var isValid = false;
    totalInvoice = $('#TotalAmount').val();     
    var paidAmount = $('#amountPaid').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var saleOrderID = 0;
    var PONo = 0;
    var SOID = 0;
    $('#tblReturn tbody tr').each(function (i, n) {        
        var $row = $(n);
        SalesOrderID = $row.find("input[id*='orderID']").val();
        SOID = $row.find("td:eq(0)").text();
        var pId = $row.find('input[id*="prodID"]').val();
        var costPrice = $row.find('input[id*="costPrice"]').val();
        var qty = $row.find("#SaleQty").text();
        var price = $row.find("#SalePrice").text();
        var subTotal = $row.find('input[id*="txtAmount"]').val();
        var returningCTNQty = $row.find('input[id*="rQty"]').val();
        var returningPacket = $row.find('input[id*="rPacket"]').val();
        var unitCode = $row.find("#unitCode").text();
        var isPack = $row.find('input[id*="isPacket"]').val();        
        COGS += parseInt(returningCTNQty) * parseFloat(costPrice);
       
        if (returningCTNQty > 0) {
            priceStockLog = parseFloat(subTotal / returningCTNQty);
            price = parseFloat(subTotal / returningCTNQty);
            isValid = true;
            soReturn.push({                
                ProductId: pId,                
                Qty: returningCTNQty,
                Packet: returningPacket,
                UnitCode: unitCode,
                IsPack: isPack,
                SalePrice: price,
                UnitPrice: costPrice,
                BranchID: BranchID,
                Total: subTotal
            });            
            stockLog.push({                
                ProductId: pId,
                StockIN: returningCTNQty,
                Packet: returningPacket,
                SalePrice: priceStockLog,
                CostPrice: costPrice,
                InReference: 'Sales Return',
                OrderTypeID: 4,
                UserReferenceID: SOID,
                BranchID: BranchID,
                UnitCode: unitCode,
                IsPack: isPack,             
                InvoiceDate: new Date()
            });
        }                     
        else {
            uiUnBlock();
            $('#btnSave').prop('disabled', false);
        }
    });
    if (soReturn.length && isValid == true) {
        var paymentStatus = 0;
        if ($('#PaymentStatus option:selected').val() != 0) {
            paymentStatus = $('#PaymentStatus option:selected').text()
        }
        var vat = parseFloat(vatAmount).toFixed(2);

        //total = $('#tblReturn tfoot tr').find('input[id="txtTotal"]').val();
        total = $('#returnAmount').val();
        if (vat > 0) {
            total = total - vat;
        }
        else {
            vat = 0;
        }
        var data = {
            'AccountID': AccountID,
            'PaymentStatus' : paymentStatus,
            'BranchID': BranchID,
            'DiscountAmount': parseFloat(discAmount).toFixed(2),
            'SaleOrderID': SalesOrderID,
            'PONo': SOID,
            'ReturnDate': new Date(),
            'VAT': vat,
            'TotalAmount': total,
            'PaymentStatus': paymentStatus,
            'PaymentTypeID': $('#PaymentType option:selected').val(),        
            'AmountPaid': $('#amountPaid').val(),        
            'ChequeDate': $("#chqDate").val(),
            'ChequeNo': $("#chqNumber").val(),
            'BankName': bank,
            'DiscountPercent': DiscountPercent,
            'COGS':COGS,
            'SOReturnDetails': soReturn
        };
        var json = JSON.stringify({ 'model': data, 'modelStockLog': stockLog, 'bankAccId': bankAccountId });
       // console.log(json);
       ajaxCall("POST", json, "application/json; charset=utf-8", "/SalesReturn/ReturnOrder", "json", onSuccess, onFailure);

        function onSuccess(Result) {
            if (parseInt(Result) > 0) {
                uiUnBlock();
                window.location.href = '/SalesReturn/Details?id='+Result+'';
            }
            else {
                uiUnBlock();                
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                window.location.href = '/SalesReturn/Details?id=' + Result + '';
                console.log(error.statusText);
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }
    else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);
        swal("Quantity", "Please Enter Some Qty!", "error");
    }
}

