﻿var chqDat = "";
$(function () {    
    $('#btnSave').click(function () {
        var VendorAccountId = $('#AccountID option:selected').val();
        var voucherDate = $('#VoucherDate').val();
        var amount = $('#Amount').val();                
        
        if (VendorAccountId == "" || VendorAccountId == 0) {
            swal("Vendor", "Please Select Vendor!", "error");
        }
        else if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if (amount == "" || amount == 0) {
            swal("Amount", "Please Enter Amount!", "error");
            //console.log(PurchaseDate + "date");
        }        
        else {
            insert();
        }
    });

});
function insert() {
    var payLog = "";
    var totalAmount = 0;
    var vendorAccountId = $('#AccountID option:selected').val();
    var BranchID = $('#hdnBranchId').val();
    var desc = $('#Description').val();    
    //var bank = $('#Bank').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var voucherDate = $('#VoucherDate').val();
    //var chqDate = $('#ChequeDate').val();
    var chqNumber = $('#ChequeNumber').val();
    var amount = parseFloat($('#Amount').val());
    //if (totalPaying == amount)
    var data = {
        'BranchID': BranchID,
        'Description': desc,
        'Amount': amount,
        'AccountID': vendorAccountId,
        'VoucherDate': voucherDate        
    };
    payLog = {
        'BranchID': BranchID,
        'Paid': amount,
        'Date': voucherDate,        
        //UserReferenceID: PONO,
        'AddBy': 1,
        'IsActive': 1
    };
    var json = JSON.stringify({ 'model': data, 'CustomerName': '', 'isVendor' : true});
    //console.log(json);
    ajaxCall("POST", json, "application/json; charset=utf-8", "/AdjustAccount/SaveAdjustment", "json", onSuccess, onFailure);
    function onSuccess(Result) {
        if (Result == "success") {
            window.location.href = 'Index';
        }
        else {
            swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
        }
        //location.reload();
        //window.location.href = 'Index';
        //alert("success");
    }
    function onFailure(error) {
        if (error.statusText == "OK")
            //alert("success");
            //console.log(error);
            window.location.reload();
    }



}
