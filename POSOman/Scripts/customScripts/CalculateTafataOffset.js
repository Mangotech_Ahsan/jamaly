﻿
var globalData = null;
var dieCuttingCostGlobal = 0;
var slittingCostGlobal = 0;
var uvCostGlobal = 0;

$(document).ready(function () {
    // FetchLaminationCosts();
})
//  getCode();
function getRollQualityID(ID) {
    
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/FetchRollQualityDetails?ID=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data)
                    $("#Length").val(+data.RollLength);
                    $("#RollCost").val(+data.RollCost);
                    $("#MaterialCostPerMeter").val(+data.MaterialCostPerMeter);
                    //  $("#hdnCardTypeID").val(data.CardTypeID);

                    CalculateSheetCost();
                    let split = +$("#Split").val() || 0;
                    if (split > 0) {
                        calculateHSize();
                    }

                }
            },
            error: function (err) { console.log(err); }
        });
    }

}

calculateTotalSheets = () => {
    let upping = +$("#Upping").val() || 1;
    let qty = +$("#Quantity").val() || 0;

    let sheets = qty / upping;
    $("#NumberOfSheets").val(+sheets);
    getImpression(+sheets);

}

calculateSplitCount = () => {
    let totSheets = +$("#NumberOfSheets").val() || 0;
    let split = +$("#Split").val() || 1;
    console.log(totSheets)
    console.log(split)
    let count = Math.round(totSheets / split);
    $("#SplinterCount").val(+count);

    calculateWInch();
}

calculateWInch = () => {
    let splinterCount = +$("#SplinterCount").val() || 0;
    let wSheetSize = +$("#WSheetSize").val() || 0;

    let inch = splinterCount * wSheetSize;
    $("#TotalWinInches").val(+inch);
    calculateTotalMeters(+inch);
}

//commented on 18-12-2021
//$("#Quantity").change(() => {
//    calculateTotalSheets();
//})

$("#Upping").change(() => {
    calculateTotalSheets();
    calculateSplitCount();
})

$("#Split").change(() => {
    calculateSplitCount();

})




$("#SplinterID").change(() => {
    let SplinterWidth = $("#SplinterID option:selected").text() || 0;

    $("#WSheetSize").val(+SplinterWidth);
    calculateWInch();

    //setTimeout(() => {

    //}, 3000);

})

calculateHSize = async () => {
    let split = +$("#Split").val() || 1;
    let rollLength = +$("#Length").val() || 0;

    let HSize = rollLength / split;

    $("#HSheetSize").val(+HSize);
}


$("#Split").change(() => {
    calculateHSize();
});

calculateTotalMeters = (WSize) => {
    let wSheetSize = +WSize;

    let meters = wSheetSize / 38;
    console.log(meters)
    console.log(WSize)
    $("#TotalMeters").val(+meters.toFixed(4));
    calculateTotalMaterialCost(+meters.toFixed(4));


}


calculateTotalMaterialCost = (totalMeters) => {
    let matCost = +$("#MaterialCostPerMeter").val() || 0;
    let totalCost = matCost * totalMeters;

    $("#TotalMaterialCostPerMeter").val(+totalCost.toFixed(5));
}

$("#Quantity").on('input', function () {

    calculateUpping();
    setTimeout(() => { FinalPriceCalculation() }, 500);

});

$("#Upping").on('input', function () {

    calculateUpping();
});

calculateUpping = () => {
    var Quantity = +$("#Quantity").val() || 0;
    var Upping = +$("#Upping").val() || 0;
    var Wastage = +$("#Wastage").val() || 0;
    if (Quantity > 0) {
        $("#Wastage").val(0);
        if (Upping > 0) {
            let Calculated = parseFloat(Quantity / Upping).toFixed(2);
            $("#NumberOfSheets").val(Calculated);
            // getNoOfSheetsValue();
            let total = Calculated + Wastage;
            getImpression(+total);
            $("#TotalNumberOfSheets").val(Calculated + Wastage);
        }
        else {
            $("#NumberOfSheets").val(0);
            $("#TotalNumberOfSheets").val(Wastage);
        }
        CalculateSheetCost();
    }
    else {
        toastr.warning("Please Insert Quantity First!")
        $("#Upping").val('');
    }
}

$("#Wastage").on('input', function () {

    var NumberOfSheets = +$("#NumberOfSheets").val() || 0;

    var Wastage = +$("#Wastage").val() || 0;
    if (NumberOfSheets > 0) {


        let cal = NumberOfSheets + Wastage;
        $("#TotalNumberOfSheets").val(cal);
        getImpression(+cal);

    }
    else {
        toastr.warning("Please Insert Relative Data First!")
        $("#Wastage").val('');
        $("#TotalNumberOfSheets").val(0);
    }
});

$("#Length").on('input', function () {

    var Length = +$("#Length").val() || 0;


    if (Length > 0) {
        CalculateSheetCost();
    }

});

$("#Width").on('input', function () {

    var Width = +$("#Width").val() || 0;


    if (Width > 0) {
        CalculateSheetCost();
    }

});

$("#CardGSM").on('input', function () {

    var CardGSM = +$("#CardGSM").val() || 0;


    if (CardGSM > 0) {
        CalculateSheetCost();
    }

});


function getNoOfSheetsValue() {

    var NumberOfSheets = +$("#NumberOfSheets").val() || 0;

    if (NumberOfSheets > 0) {
        if (NumberOfSheets > 0 && NumberOfSheets <= 1200) {
            $("#Impression").val(1);
        }
        else if (NumberOfSheets > 1200 && NumberOfSheets <= 2200) {
            $("#Impression").val(2);
        }
        if (NumberOfSheets > 2200) {
            $("#Impression").val(3);
        }

    }

}

getImpression = (Sheets) => {
    if (+Sheets > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/GetImpression?Sheets=' + (+Sheets),

            async: true,
            success: function (data) {
                {
                    console.log(data);
                    if (+data > 0) {
                        $("#Impression").val(+data);
                    }
                    else {
                        toastr.warning(data.toString())
                        $("#Impression").val(0);
                    }


                }
            },
            error: function (err) { console.log(err); }
        });
    }
}

function CalculateSheetCost() {
    console.log("in CalculateSheetCost")

    let hdnCardTypeID = +$("#hdnCardTypeID").val() || 0;
    let Length = +$("#Length").val() || 0;
    let Width = +$("#Width").val() || 0;
    let CardGSM = +$("#CardGSM").val() || 0;
    var RatePerKG = +$("#RatePerKG").val() || 0;
    var NumberOfSheets = +$("#NumberOfSheets").val() || 0;

    if (hdnCardTypeID > 0) {
        if (hdnCardTypeID === 1) {//Card
            let Calculate = ((Length * Width * CardGSM * RatePerKG) / 15500) / 100;
            $("#PerSheetCost").val(parseFloat(Calculate / NumberOfSheets).toFixed(12));
            $("#TotalSheetCost").val(parseFloat(Calculate).toFixed(12));
        }
        else if (hdnCardTypeID === 2) {//Paper
            let Calculate = ((Length * Width * CardGSM * RatePerKG) / 16600) / 500;
            $("#PerSheetCost").val(parseFloat(Calculate / NumberOfSheets).toFixed(12));
            $("#TotalSheetCost").val(parseFloat(Calculate).toFixed(12));
        }
    }

}

function getMachineDetails(ID) {
    console.log(ID)
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/FetchMachineDetails?ID=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data);
                    globalData = data;
                    dieCuttingCostGlobal = data.DieCuttingCost;
                    slittingCostGlobal = data.SlittingCost;
                    uvCostGlobal = data.UVCost;

                    //console.log("slittingCostGlobal->" + slittingCostGlobal)
                    FetchLaminationCosts();
                    setTimeout(function () {
                        CalculateMachineSelectedCosts(data);
                    }, 1000);


                }
            },
            error: function (err) { console.log(err); }
        });
    }

}

function FetchLaminationCosts() {


    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/MachinePlate/FetchLaminationCosts',

        async: true,
        success: function (data) {
            {
                console.log(data);
                $("#GlossLaminationCost").val(parseFloat(+data.GlossLaminationCost).toFixed(2));
                $("#MatLaminationCost").val(parseFloat(+data.MatLaminationCost).toFixed(2));
                $("#HotLaminationCost").val(parseFloat(+data.HotLaminationCost).toFixed(2));
                //$("#UVCost").val(parseFloat(+data.UVCost).toFixed(2));
                //$("#SlittingCost").val(parseFloat(+data.SlittingCost).toFixed(2));
                if ($("#includeUVCost").is(":checked")) {
                    $("#UVCost").val(parseFloat(+uvCostGlobal).toFixed(2));
                }
                else {
                    $("#UVCost").val(parseFloat(0).toFixed(2));
                }
                if ($("#includeSlittingCost").is(":checked")) {
                    console.log("in-slittingcost->" + slittingCostGlobal)
                    $("#SlittingCost").val(parseFloat(+slittingCostGlobal).toFixed(2));
                }
                else {
                    $("#SlittingCost").val(parseFloat(0).toFixed(2));
                }


            }
        },
        error: function (err) { console.log(err); }
    });


}

function CalculateMachineSelectedCosts(data) {
    console.log(data)
    if (data != null) {
        //let getMachinePlates = +$("#MachinePlates").val() || 0;
        let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
        let MachineTextColor = +$("#MachineTextColor").val() || 0;
        let Length = +$("#Length").val() || 0;
        let DieCost = +$("#DieCost").val() || 0;
        let Width = 1;//+$("#Width").val() || 0;
        let NoOfSheets = +$("#NumberOfSheets").val() || 0;
        let GroundCost = (+data.GroundCost * MachineGroundColor) || 0;
        let TextCost = (+data.TextCost * MachineTextColor) || 0;
        let GlossLaminationCost = +data.GlossLaminationCost || 0;
        let MatLaminationCost = +data.MatLaminationCost || 0;
        let HotLaminationCost = +data.HotLaminationCost || 0;
        //let NoOfPlates = +GroundCost + +TextCost;
        let NoOfPlates = MachineGroundColor + MachineTextColor;
        let PlateCost = +data.PlateCost * +NoOfPlates;
        let UVCost = +data.UVCost || 0;
        let TotalUVCost = (+UVCost / 144) * (+Length * +Width * +NoOfSheets);
        let Impressions = +$("#Impression").val() || 0;
        let DieCuttingCost = +data.DieCuttingCost * +Impressions + +DieCost;
        //let DieCuttingCost = +data.DieCuttingCost * + +DieCost;
        let SlittingCost = +data.SlittingCost || 0;
        let TotalSlittingCost = +SlittingCost * Impressions;
        // let TotalSlittingCost = +SlittingCost ;
        let TotalGlossLaminationCost = (+GlossLaminationCost / 144) * +Length * +Width * +NoOfSheets;
        let TotalMatLaminationCost = (+MatLaminationCost / 144) * +Length * +Width * +NoOfSheets;
        let TotalHotLaminationCost = (+HotLaminationCost / 144) * +Length * +Width * +NoOfSheets;

        let PrintingCost = (+Impressions * +GroundCost) + (+Impressions * +TextCost);

        // let PrintingCost = +GroundCost + ( +TextCost);
        $("#GlossLaminationCost").val(parseFloat(+GlossLaminationCost).toFixed(2));
        $("#MatLaminationCost").val(parseFloat(+MatLaminationCost).toFixed(2));
        $("#HotLaminationCost").val(parseFloat(+HotLaminationCost).toFixed(2));
        //  if (getMachinePlates != NoOfPlates) {
        $("#MachinePlates").val(NoOfPlates);
        // }

        $("#MachineGroundCost").val(parseFloat(+GroundCost).toFixed(2));
        $("#MachineTextCost").val(parseFloat(+TextCost).toFixed(2));

        if ($('#GlossLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); $("#OffsetTypeID").val(1); }

        if ($('#MatLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); $("#OffsetTypeID").val(1); }

        if ($('#HotLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalHotLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalHotLaminationCost).toFixed(2)); $("#OffsetTypeID").val(1); }



        $("#MachinePlateCost").val(parseFloat(+PlateCost).toFixed(2));
        $("#MachinePrintingCost").val(parseFloat(+PrintingCost).toFixed(2));
        //$("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        //$("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        //$("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));
        if ($("#includeUVCost").is(":checked")) {
            $("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        }
        else {
            $("#UVCost").val(parseFloat(0).toFixed(2));
        }
        if ($("#includeSlittingCost").is(":checked")) {
            $("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));
        }
        else {
            $("#SlittingCost").val(parseFloat(0).toFixed(2));
        }
        if ($("#includeDieCost").is(":checked")) {
            $("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        }
        else {
            $("#DieCuttingCost").val(parseFloat(0).toFixed(2));
        }
    }
}


function CalculateMachineSelectedCostsWithManualMachinePlatesCost(data) {
    console.log(data)
    if (data != null) {
        let getMachinePlates = +$("#MachinePlates").val() || 0;
        let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
        let MachineTextColor = +$("#MachineTextColor").val() || 0;
        let Length = +$("#Length").val() || 0;
        let DieCost = +$("#DieCost").val() || 0;
        let Width = +$("#Width").val() || 0;
        let NoOfSheets = +$("#NumberOfSheets").val() || 0;
        let GroundCost = (+data.GroundCost * MachineGroundColor) || 0;
        let TextCost = (+data.TextCost * MachineTextColor) || 0;
        let GlossLaminationCost = +data.GlossLaminationCost || 0;
        let MatLaminationCost = +data.MatLaminationCost || 0;
        let HotLaminationCost = +data.HotLaminationCost || 0;
        //let NoOfPlates = +GroundCost + +TextCost;
        // let NoOfPlates = MachineGroundColor + MachineTextColor;
        let PlateCost = +data.PlateCost * +getMachinePlates;
        let UVCost = +data.UVCost || 0;
        let TotalUVCost = (+UVCost / 144) * (+Length * +Width * +NoOfSheets);
        let Impressions = +$("#Impression").val() || 0;
        //let PrintingCost = +Impressions * +GroundCost * +TextCost;
        let PrintingCost = (+Impressions * +GroundCost) + (+Impressions * +TextCost);
        let DieCuttingCost = +data.DieCuttingCost * +Impressions + +DieCost;
        let SlittingCost = +data.SlittingCost || 0;
        let TotalSlittingCost = +SlittingCost * Impressions;
        let TotalGlossLaminationCost = (+GlossLaminationCost / 144) * +Length * +Width * +NoOfSheets;
        let TotalMatLaminationCost = (+MatLaminationCost / 144) * +Length * +Width * +NoOfSheets;
        let TotalHotLaminationCost = (+HotLaminationCost / 144) * +Length * +Width * +NoOfSheets;

        $("#GlossLaminationCost").val(parseFloat(+GlossLaminationCost).toFixed(2));
        $("#MatLaminationCost").val(parseFloat(+MatLaminationCost).toFixed(2));
        //  if (getMachinePlates != NoOfPlates) {
        //$("#MachinePlates").val(NoOfPlates);
        // }

        $("#MachineGroundCost").val(parseFloat(+GroundCost).toFixed(2));
        $("#MachineTextCost").val(parseFloat(+TextCost).toFixed(2));
        $("#HotLaminationCost").val(parseFloat(+HotLaminationCost).toFixed(2));

        if ($('#GlossLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); $("#OffsetTypeID").val(1); }

        if ($('#MatLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); $("#OffsetTypeID").val(2); }

        if ($('#HotLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalHotLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalHotLaminationCost).toFixed(2)); $("#OffsetTypeID").val(3); }


        $("#MachinePlateCost").val(parseFloat(+PlateCost).toFixed(2));
        $("#MachinePrintingCost").val(parseFloat(+PrintingCost).toFixed(2));
        //$("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        //$("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        //$("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));
        if ($("#includeUVCost").is(":checked")) {
            $("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        }
        else {
            $("#UVCost").val(parseFloat(0).toFixed(2));
        }
        if ($("#includeSlittingCost").is(":checked")) {
            $("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));
        }
        else {
            $("#SlittingCost").val(parseFloat(0).toFixed(2));
        }
        if ($("#includeDieCost").is(":checked")) {
            $("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        }
        else {
            $("#DieCuttingCost").val(parseFloat(0).toFixed(2));
        }
    }
}

$('input[name="rad"]').click(function () {
    try {
        var $radio = $(this);
        console.log($radio.data('waschecked'));
        // if this was previously checked
        if ($radio.data('waschecked') == true) {
            $radio.prop('checked', false);
            $radio.data('waschecked', false);
        }
        else
            $radio.data('waschecked', true);

        // remove was checked from other radios
        $radio.siblings('input[name="rad"]').data('waschecked', false);

        if ($('#' + $radio.attr('id')).is(':checked')) {
            console.log($radio.attr('id') + "-checked");

            if ($radio.attr('id').toString() === 'MatLaminationCost') {
                console.log("Mat");

                //$("#GlossLaminationCost").prop('checked', false);
                let getMachinePlates = +$("#MachinePlates").val() || 0;
                let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
                let MachineTextColor = +$("#MachineTextColor").val() || 0;
                let finalColorRate = MachineGroundColor + MachineTextColor;
                if (globalData != null) {
                    console.log("In Mat")
                    // if ($('#MatLaminationCost').is(':checked')) {
                    if (getMachinePlates === finalColorRate) {
                        CalculateMachineSelectedCosts(globalData);
                    }
                    else {
                        CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
                    }


                    FinalPriceCalculation();
                    //  }
                }

            }

            else if ($radio.attr('id').toString() === 'GlossLaminationCost') {
                console.log("Gloss");
                //$("#MatLaminationCost").prop('checked', false);
                let getMachinePlates = +$("#MachinePlates").val() || 0;
                let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
                let MachineTextColor = +$("#MachineTextColor").val() || 0;
                let finalColorRate = MachineGroundColor + MachineTextColor;
                if (globalData != null) {
                    console.log("In Gloss")
                    //if ($('#GlossLaminationCost').is(':checked')) {
                    if (getMachinePlates === finalColorRate) {
                        CalculateMachineSelectedCosts(globalData);
                    }
                    else {
                        CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
                    }

                    FinalPriceCalculation();
                    //   }
                }
            }

            else if ($radio.attr('id').toString() === 'HotLaminationCost') {
                console.log("Hot");
                //$("#MatLaminationCost").prop('checked', false);
                let getMachinePlates = +$("#MachinePlates").val() || 0;
                let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
                let MachineTextColor = +$("#MachineTextColor").val() || 0;
                let finalColorRate = MachineGroundColor + MachineTextColor;
                if (globalData != null) {
                    console.log("In Gloss")
                    //if ($('#GlossLaminationCost').is(':checked')) {
                    if (getMachinePlates === finalColorRate) {
                        CalculateMachineSelectedCosts(globalData);
                    }
                    else {
                        CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
                    }

                    FinalPriceCalculation();
                    //   }
                }
            }
        }
        else {
            console.log($radio.attr('id') + "-un-checked");

            $("#LaminationCost").val(0);
            FinalPriceCalculation();
        }
    }
    catch (err) {
        console.log("checkbox error->" + err);
    }

});


//$("#MatLaminationCost").click(function () {
//    console.log("Mat")
//    let getMachinePlates = +$("#MachinePlates").val() || 0;
//    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
//    let MachineTextColor = +$("#MachineTextColor").val() || 0;
//    let finalColorRate = MachineGroundColor + MachineTextColor;
//    if (globalData != null) {
//        console.log("In Mat")
//        if ($('#MatLaminationCost').is(':checked')) {
//            if (getMachinePlates === finalColorRate) {
//                CalculateMachineSelectedCosts(globalData);
//            }
//            else {
//                CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
//            }


//            FinalPriceCalculation();
//        }
//    }

//})

//$("#GlossLaminationCost").click(function () {
//    console.log("Gloss");
//    let getMachinePlates = +$("#MachinePlates").val() || 0;
//    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
//    let MachineTextColor = +$("#MachineTextColor").val() || 0;
//    let finalColorRate = MachineGroundColor + MachineTextColor;
//    if (globalData != null) {
//        console.log("In Gloss")
//        if ($('#GlossLaminationCost').is(':checked')) {
//            if (getMachinePlates === finalColorRate) {
//                CalculateMachineSelectedCosts(globalData);
//            }
//            else {
//                CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
//            }

//            FinalPriceCalculation();
//        }
//    }

//})



function FinalPriceCalculation() {
    let TotalSheetCost = +$("#TotalMaterialCostPerMeter").val() || 0;
    let NoOfSheets = +$("#NumberOfSheets").val() || 0;
    let Upping = +$("#Upping").val() || 0;
    let Qty = +$("#Quantity").val() || 1;
    let MachinePlateCost = +$("#MachinePlateCost").val() || 0;
    let MachinePrintingCost = +$("#MachinePrintingCost").val() || 0;
    let LaminationCost = +$("#LaminationCost").val() || 0;
    let DieCuttingCost = +$("#DieCuttingCost").val() || 0;
    let UVCost = +$("#UVCost").val() || 0;
    let SlittingCost = +$("#SlittingCost").val() || 0;
    let DesignCost = +$("#DesignCost").val() || 0;
    let FoilPrintingCost = +$("#FoilPrintingCost").val() || 0;
    let MarkupRate = +$("#MarkupRate").val() || 0;

    //let GrandTotal = (TotalSheetCost + MachinePlateCost + MachinePrintingCost + LaminationCost +
    //    DieCuttingCost + UVCost + SlittingCost + DesignCost + FoilPrintingCost) / +NoOfSheets / +Upping;

    let GrandTotal = (TotalSheetCost + MachinePlateCost + MachinePrintingCost + LaminationCost +
        DieCuttingCost + UVCost + SlittingCost + DesignCost + FoilPrintingCost) / +Qty;
    let updatedTotal = +GrandTotal;
    if (MarkupRate > 0) {
        $("#FinalCostPerPc").val(parseFloat(updatedTotal).toFixed(2));

        let calculatedMarkupPrice = (+MarkupRate / 100) * +GrandTotal;

        // if (calculatedMarkupPrice < GrandTotal) {
        GrandTotal = GrandTotal + calculatedMarkupPrice;
        $("#MarkedUpPrice").val(parseFloat(GrandTotal).toFixed(2));
        //}
        //else {
        //    $("#MarkupRate").val('');
        //    toastr.warning("Invalid MarkUp Rate Entered!")
        //    $("#MarkedUpPrice").val(parseFloat(updatedTotal).toFixed(4));
        //}
    }
    else {
        $("#FinalCostPerPc").val(parseFloat(updatedTotal).toFixed(2));
        $("#MarkedUpPrice").val(parseFloat(updatedTotal).toFixed(2));
    }


}

$("#MarkupRate").focusout(function () {

    let MarkUpRate = +$("#MarkupRate").val() || 0;
    let FinalCostPerPc = +$("#FinalCostPerPc").val() || 0;
    if (MarkUpRate > 0 && FinalCostPerPc > 0) {

        console.log(MarkUpRate)
        console.log(FinalCostPerPc)

        var PercentAmount = parseFloat((MarkUpRate / 100) * (FinalCostPerPc)).toPrecision(2);
        console.log(+PercentAmount)
        var FinalPrice = parseFloat(+PercentAmount + FinalCostPerPc).toPrecision(2);
        console.log(+FinalPrice)
        $("#MarkedUpPrice").val(parseFloat(+FinalPrice).toFixed(2));
        // FinalPriceCalculation();
    }
});
$("#MarkedUpPrice").focusout(function () {
    let MarkedUpPrice = +$("#MarkedUpPrice").val() || 0;
    let FinalCostPerPc = +$("#FinalCostPerPc").val() || 0;
    if (MarkedUpPrice > 0 && +FinalCostPerPc > 0) {
        var actualPrice = parseFloat(MarkedUpPrice - FinalCostPerPc).toPrecision(2);//487

        PercentRate = parseFloat((+actualPrice * 100) / +MarkedUpPrice).toPrecision(2);
        $("#MarkupRate").val(parseFloat(+PercentRate).toFixed(2));

    }
    else {
        $("#MarkupRate").val(0);
        FinalPriceCalculation();
    }
});

$("#DieCost").change(function () {
    let DieCost = +$("#DieCost").val() || 0;
    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;
    // if (DieCost > 0) {
    if (globalData != null) {
        console.log("In")
        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }

    // }
});

$("#DesignCost").change(function () {
    let DesignCost = +$("#DesignCost").val() || 0;
    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;
    //if (DesignCost > 0) {
    if (globalData != null) {
        console.log("In")
        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }

    //}
});

$("#MachineGroundColor").change(function () {
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    if (MachineGroundColor > 0) {
        if (globalData != null) {
            console.log("In")
            CalculateMachineSelectedCosts(globalData); FinalPriceCalculation();
        }

    }
}); $("#MachineTextColor").change(function () {
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    if (MachineTextColor > 0) {
        if (globalData != null) {
            console.log("In")
            CalculateMachineSelectedCosts(globalData); FinalPriceCalculation();
        }

    }
});

$("#FoilPrintingCost").change(function () {
    let FoilPrintingCost = +$("#FoilPrintingCost").val() || 0;
    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;
    // if (FoilPrintingCost > 0) {
    if (globalData != null) {
        console.log("In");
        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }

    // }
});

$("#MachinePlates").change(function () {
    let MachinePlates = +$("#MachinePlates").val() || 0;
    if (MachinePlates > 0) {
        if (globalData != null) {
            console.log("In");
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData); FinalPriceCalculation();
        }

    }
});

$("#includeDieCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    if ($('#includeDieCost').is(':checked')) {
        $("#DieCost").prop('readonly', false);
        console.log("die-checked")
        $("#DieCuttingCost").val(+dieCuttingCostGlobal);

    }
    else {
        console.log("die-un-checked")
        $("#DieCost").prop('readonly', true);
        $("#DieCuttingCost").val(0);
    }

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})


$("#includeUVCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    if ($('#includeUVCost').is(':checked')) {
        console.log("uv-checked")
        $("#UVCost").val(+uvCostGlobal);

    }
    else {
        console.log("uv-un-checked")
        $("#UVCost").val(0);
    }

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})


$("#includeSlittingCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    if ($('#includeSlittingCost').is(':checked')) {
        console.log("slit---checked")
        console.log(+slittingCostGlobal)
        $("#SlittingCost").val(+slittingCostGlobal.toFixed(2));

    }
    else {
        console.log("slit-un-checked")
        $("#SlittingCost").val(0);
    }

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})