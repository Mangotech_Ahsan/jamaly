﻿var chqDat = "";
$(function () {
    $("#Bank").prop("disabled", true);
    document.getElementById("ChequeDate").readOnly = true;
    document.getElementById("ChequeNumber").readOnly = true;
    $('#btnSave').click(function () {
        var vendorID = $('#AccountID option:selected').val();
        var voucherDate = $('#VoucherDate').val();
        chqDate = $('#ChequeDate').val();
        if (chqDate == "") {
            chqDat = "";
        }
        if (vendorID == "" || vendorID == 0) {
            swal("Vendor", "Please Select Vendor!", "error");
        }
        else if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
        }

        else {
            $('#btnSave').prop('disabled', true);
            uiBlock();
            insert();
        }
    });
});
var invoiceTotal = 0;

$("#AccountID").change(function () {
    getVenderBalance($("#AccountID").val());
});

function getVenderBalance(accountId) {
    if (accountId == "") {
        accountId = -1;
    }
    var json = {
        "accountID": accountId
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/VendorPayment/GetVendorBalance',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#Balance').val(data);
        },
        error: function (err) {
            console.log(err);
        }
    });
}




$("#PayTypeID").on("change", function () {
    var payStatusSelection = $("#PayTypeID").val();
    var totalAmount = $("#TotalAmount").val();
    if (payStatusSelection == 1) {
        $("#Bank").prop("disabled", true);
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;
    }
    else if (payStatusSelection == 2) {
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;
    }
    else if (payStatusSelection == 3) {
        $("#Bank").prop("disabled", false);
        //document.getElementById("ChequeDate").readOnly = false;
        document.getElementById("ChequeNumber").readOnly = false;
    }

});





function insert() {
    var rows = [];
    var payLog = [];
    var JEntryLog = [];
    var totalAmount = 0;
    var orderId = 0;
    var vendorID = $('#AccountID option:selected').val();
    var desc = $('#Description').val();
    var payType = $('#PayTypeID option:selected').val()
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var voucherDate = $('#VoucherDate').val();
    var chqNumber = $('#ChequeNumber').val();
    var amount = parseFloat($('#Amount').val());
    var data = {
        'Description': desc,
        'Amount': amount,
        'AccountID': vendorID,
        'VoucherDate': voucherDate,
        'PaymentTypeID': payType,
        'BankName': bank,
        'ChequeDate': chqDate,
        'ChequeNumber': chqNumber,
    };

    JEntryLog.push({
        Amount: amount,
        OrderTypeID: 1,
        OrderID: 0,
        EntryTypeID: 1,
        isReversed: false
    });
    var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog, 'bankAccId': bankAccountId });
    ajaxCall("POST", json, "application/json; charset=utf-8", "/VendorPayment/SaveSinglePayment", "json", onSuccess, onFailure);
    function onSuccess(Result) {
        if (Result == "success") {
            uiUnBlock();
            window.location.href = '/VendorPayment/Details?isNew=true&id=' + vendorID;
        }
        else {
            uiUnBlock();
            $('#btnSave').prop('disabled', false);
            swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
        }
    }
    function onFailure(error) {
        if (error.statusText == "OK") {
            window.location.reload();
        }
    }
}
    