﻿var customerID = 0;
var accID = 0;
var isPONOExist = false;
var isQuote = false;
var SOID = 0;
var isLimitExceed = false;
$("#Bank").prop("disabled", true);

$('#btnSubmit').click(function () {
    accID = $('#hdnAccountID').val();    
    var isValid = true;    
    var PONo = $('#PONO').val();
    var SalesDate = $('#SaleDate').val();
    var paymentStatus = $('#PaymentStatus option:selected').val();
    var rowCount = $('#tblProduct tbody tr').length;
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //var BranchID = $('#BranchID option:selected').val();
    var BranchID = $('#hdnBranchId').val();
    var amountPaid = $("#amountPaid").val();
    var balance = $("#balanceAmount").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    
    if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Customer", "Please Select Customer!", "error");
    }
    else if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Return Date!", "error");        
    }
    else if (rowCount ==0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");
        //console.log(PurchaseDate + "date");
    }
    else if (paymentStatus == 0) {
        isValid = false;
        swal("Payment Status", "Please Select Payment Status!", "error");
    }
    else if ((paymentStatus == 1 || paymentStatus == 2) && (PaymentTypeID == "" || PaymentTypeID == undefined)) {
        isValid = false;
        swal("Payment Type", "Please Select Payment Type!", "error");
    }
    else if ((paymentStatus == 2) && (amountPaid == "" || amountPaid == 0)) {
        isValid = false;
        swal("Amount Paid!", "Please Enter Amount!", "error");
    }    
    else if (isValid == true) {        
        $('#btnSubmit').prop('disabled', true);        
        uiBlock();
        insert();
    }
});
// Payment Status DropDOwn, Action according to Payment Status Selection
$("#PaymentStatus").on("change", function () {
    var payStatusSelection = $("#PaymentStatus").val();
    var totalAmount = $("#totalAmount").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    //Paid
    if (payStatusSelection == 1) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        getNewTotal();
        //document.getElementById("amountPaid").value = totalAmount;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("balanceAmount").value = 0;
        //document.getElementById("expenseInput").readOnly = false;
        //document.getElementById("vatInput").readOnly = false;
    }
        // Partial PAid
    else if (payStatusSelection == 2) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = false;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
    }
        //Unpaid    
    else if (payStatusSelection == 3) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        document.getElementById("PaymentType").selectedIndex = "0";
        $("#PaymentType").prop("disabled", true);
        disableBank();
    }
});

function disableBank() {
    $("#Bank").prop("disabled", true);
    $("#chqNumber").prop("disabled", true);
    $("#chqDate").prop("disabled", true);
}
    // Payment Type Change 
$("#PaymentType").on("change", function () {
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //Cash
    if (PaymentTypeID == 1) {
        //$("#bankName").prop("disabled", true);
        $("#Bank").prop("disabled", true);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);
    }
        // Bank
    else if (PaymentTypeID == 2) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);

    }
        // Cheque
    else if (PaymentTypeID == 3) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", false);
        $("#chqDate").prop("disabled", false);
    }
});
function getNewTotal() {
    calcAmountPaid();
    var subTotal = $("#subAmount").val();
    var newTotal = parseFloat(subTotal);
    $('#totalAmount').val(parseFloat(newTotal).toFixed(3));
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(newTotal).toFixed(3);
    }
}
       
    // Get Customer Balance and Credit Limit 
        function getCustomerDetail(accountID) {
            if (accountID == "")
            { accountID = -1; }
            var json = {
                "accountID": accountID
            };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '/Customer/getDetail',
                async: true,
                data: JSON.stringify(json),
                success: function (data) {
                    $('#customerCrLimit').val(data.creditLimit);
                    $('#customerBalance').val(data.Balance);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        }
    // calculate balance if amountPaid entered and make changes every where needed
        function calcAmountPaid() {
            isLimitExceed = false;
            var amountPaid = $("#amountPaid").val();
            var finalAmount = $('#totalAmount').val();
            var payStatusSelection = $("#PaymentStatus").val();
            if (payStatusSelection == 2) {
                if (parseFloat(amountPaid) >= parseFloat(finalAmount)) {
                    toastr.warning('Partial ! AmountPaid should be less than total amount ')
                    $("#amountPaid").val(0)
                }
                else {
                    var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
                    $("#balanceAmount").val(parseFloat(balance).toFixed(3));
                }
            }
            else {

                var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
                $("#balanceAmount").val(parseFloat(balance).toFixed(3));
            }
        }
   
    
    
$("#amountPaid").on("change", function () {
    calcAmountPaid();
});
    // Add data to array and send it to controller for order creation
function insert() {
    var returnDetails = [];
    var stockLog = [];
    var total = 0;
    var qtyVE = 0;
    var BranchId = $('#hdnBranchId').val();
    invoiceAmount = $('#subAmount').val();
    var paidAmount = $('#amountPaid').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = $row.find('input[id*="productID"]').val();
        var costPrice = $row.find('input[id*="costPrice"]').val();
        var partNumber = $row.find("td").eq(2).text();
        var Location = $row.find("#Location").text();
        var Qty = $row.find("#ProductQty").text();
        var SalePrice = $row.find("#ProductSalePrice").text();
        var SubTotal = $row.find("#ProductSubTotal").text();
        returnDetails.push({
            PartNo: partNumber,
            ProductId: pId,
            UnitPrice: costPrice,
            Qty: Qty,
            SalePrice: SalePrice,
            Total: SubTotal,
            BranchID: BranchId,
        });
        stockLog.push({
            AccountID: $('#hdnAccountID').val(),
            ProductId: pId,
            StockIn: Qty,
            SalePrice: SalePrice,
            Location: Location,
            CostPrice: costPrice,
            InReference: 'Previou Sales Return',
            OrderTypeID: 10,
            UserReferenceID: $('#InvoiceNo').val(),
            BranchID: BranchId,
            InvoiceDate: $('#SaleDate').val()
        });

    });
    if (returnDetails.length) {
        var paymentStatus = 0;
        if ($('#PaymentStatus option:selected').val() != 0) {
            paymentStatus = $('#PaymentStatus option:selected').text()
        }
        var SaleAmount = parseFloat(total).toFixed(3);
        var data = {
            'AccountID': $('#hdnAccountID').val(),
            'BranchID': BranchId,
            'InvoiceNo': $('#InvoiceNo').val(),
            'VehicleNo': $('#VehicleNo').val(),
            'PaymentStatus': $('#PaymentStatus').val(),
            'ReturnDate': $('#ReturnDate').val(),
            'PaymentStatus': paymentStatus,
            'PaymentTypeID': $('#PaymentType option:selected').val(),
            'TotalAmount': invoiceAmount,
            'AmountPaid': $('#amountPaid').val(),
            'ChequeDate': $("#chqDate").val(),
            'ChequeNo': $("#chqNumber").val(),
            'BankName': bank,
            'tbl_PrevSOReturnDetails': returnDetails
        };
        var json = JSON.stringify({
            'model': data, 'modelStockLog': stockLog, 'bankAccId': bankAccountId
        });
        //console.log(json);
        ajaxCall("POST", json, "application/json; charset=utf-8", "/PreviousSalesReturn/SaveOrder", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result > 0) {
                uiUnBlock();
                window.location.href = '/PreviousSalesReturn/Details?id=' + Result + '';
                window.location.href = '/PreviousSalesReturn';
            }
            else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                $('#btnHold').prop('disabled', false);

                alert("Some error Ocurred! Please Check Your Entries");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                uiUnBlock();
                window.location.href = '/PreviousSalesReturn/Details?id=' + Result + '';
                window.location.href = '/PreviousSalesReturn';
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }
}
    
