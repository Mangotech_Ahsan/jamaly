﻿var controlId = 0;

$(function () {
    $('#PurchaseDate').val(new Date().toLocaleDateString());

    addTable();
    dummyData();
    $('#btnAddRow').click(function () {
        addRow();
    })

    $('#btnSubmit').click(function () {
        insert();
    });

    $('#tbl').keyup(function (e) {
        var qty = $(e.target).closest("tr").find('.qty').val();
        var price = $(e.target).closest("tr").find('.price').val();
        var disc = $(e.target).closest("tr").find('.discPercent').val();
        if (disc == "")
            disc = 0;
        if (($.isNumeric(price)) && ($.isNumeric(qty))) {
            var amount = (qty * price) - (qty * price * disc) / 100;
            $(e.target).closest('tr').find('.sub').val(amount
            );
            calcTotal();
        }
        else {
            $(e.target).closest('tr').find('.sub').val('');
        }
    });
})

function dummyData() {
    $('#InvoiceNo').val('inv001');
    $('#PaymentStatus').val('PAID');
    $('#VAT').val('5');
    $('#Expenses').val('1000');
    $('#PurchaseCode').val('PO897');
    $('#Currency').val('AED');
    $('#DiscountPercent').val('0');
    $('#DiscountAmount').val('0');
}

function addTable() {
    var tbl = '<table id="tbl" class="table table-striped table-sm">';
    tbl += '<thead><tr>';
    tbl += '<th>Part Number</th>';
    tbl += '<th>Description</th>';
    tbl += '<th>Substitute Number</th>';
    tbl += '<th>Vehicle Model</th>';
    tbl += '<th>Group</th>';
    tbl += '<th>Location</th>';
    tbl += '<th>Qty</th>';
    tbl += '<th>Unit Price</th>';
    tbl += '<th>Discount %</th>';
    tbl += '<th>Sale Price</th>';
    tbl += '<th>Total</th>';
    tbl += '<th><button type="button" value="Add" id="btnAdd" name="btnAdd" class="btn btn-primary btn-sm" onclick="addRow()"><span class="glyphicon glyphicon-plus"></span></button></th>'
    tbl += '</tr></thead>';
    tbl += '<tbody></tbody>';
    tbl += '<tfoot><tr>';
    tbl += '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td><td><input type="text" class="form-control input-sm" id="txtTotal" disabled/></td>';
    tbl += '</tr></tfoot>'
    tbl += '</table>';
    $('#dvtbl').append(tbl);
    addRow();

}


function addRow() {

    var row = '';
    row += '<tr>';
    row += '<td style="width: 10%;">';
    row += '<select id="ddlPartNumber' + controlId + '" name="ddlPartNumber' + controlId + '" onchange="getDetail(this)" class="form-control"></select>';
    row += '<input type="hidden" id="hdnPId' + controlId + '" />';
    row += '</td>';

    row += '<td><input type="text" class="form-control input-sm" name="" id="txtDescription" disabled/></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtSubNumber" disabled/></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtVmodel" disabled/></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtGroup" disabled/></td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtLocation" disabled/></td>';
    row += '<td><input type="Number" class="form-control input-sm qty" name="qty[]" id="txtQty" /></td>';
    row += '<td><input type="Number" class="form-control input-sm price" name="price[]" id="txtPrice" />' + '</td>';
    row += '<td><input type="Number" class="form-control input-sm discPercent" name="discPercent[]" id="txtDiscount" />' + '</td>';
    row += '<td><input type="text" class="form-control input-sm" name="" id="txtSalePrice" /></td>';
    row += '<td><input type="Number" class="form-control input-sm sub" name="sub" id="txtSub" disabled />' + '</td>';
    row += '<td><button type="button" value="Remove" id="btnRemove" name="btnRemove" class="btn btn-danger btn-sm" onclick="remove(this)"><span class="glyphicon glyphicon-remove"></span></button></td>';
    row += '</tr>';

    $("#tbl tbody").append(row);
    GetDropdown("ddlPartNumber" + controlId, "/Purchase/getProduct_PartNumbers", "{}", true);
    controlId += 1;
}

function getDetail(pro) {

    var tr = $(pro).closest('tr');
    var pId = $(tr).find('option:selected').val();
    $(tr).find('input[type="hidden"]').val(pId);

    // get product description
    var json = { "productId": pId };
    //ajaxCall("GET", json, "application/json; charset=utf-8", "/Purchase/getProductDetail", "json", onSuccess, onFailure);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {

            $(tr).find('td:eq(1) input[type="text"]').val(data.Description);
            $(tr).find('td:eq(2) input[type="text"]').val(data.SubstituteNo);
            $(tr).find('td:eq(3) input[type="text"]').val(data.GroupName);
            $(tr).find('td:eq(4) input[type="text"]').val(data.VehicleName);
            $(tr).find('td:eq(5) input[type="text"]').val(data.Location);

        },
        error: function (err) { console.log(err); }
    });
    //function onSuccess(res) {
    //    debugger;
    //    console.log(res.d);
    //    //console.log(response);
    //    //console.log($(tr).html());
    //    //$(tr).find('td:eq(1) input[type="text"]').val(Result.Description);
    //    //$(tr).find('td:eq(2) input[type="text"]').val("text");

    //}
    //function onFailure(Result) {
    //    console.log(Result);
    //}
}

function remove(input) {

    var totalRowCount = $("#tbl tbody tr").length;
    if (totalRowCount > 1) {
        $(input).parent().parent().remove();
        calcTotal();
        //reIndextbl();
    }
    else {
        alert("Cannot remove all rows");
    }

}

function calcTotal() {
    var total = 0;
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        //var subTotal = parseFloat($row.find('td:eq(9) input[id*="txtSub"]').val());
        // after adding discount td
        var subTotal = parseFloat($row.find('td:eq(10) input[id*="txtSub"]').val());
        total += parseFloat(subTotal);
        //console.log(subTotal);
        //console.log(total);
    });

    $('#tbl tfoot tr').find('td:eq(10) input[type="text"]').val(total);
    $('#TotalAmount').val(total);
}

function insert() {
    var rows = [];
    var stockLog = [];
    var total = 0;


    // 
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = $row.find('td:eq(0) :selected').val();
        var partNumber = $row.find('td:eq(0) option:selected').text();
        var pLocation = $row.find('td').eq(5).find('input').val();
        var qty = $row.find('td:eq(6) input[type="Number"]').val();
        var price = $row.find('td:eq(7) input[type="Number"]').val();
        var discountPercent = $row.find('td:eq(8) input[type="Number"]').val();
        // without discount column
        //var salePrice = $row.find('td:eq(8) input[type="Number"]').val();
        //var subTotal = $row.find('td:eq(9) input[type="Number"]').val();
        // with discount column
        var salePrice = $row.find('td:eq(9) input[type="Number"]').val();
        var subTotal = $row.find('td:eq(10) input[type="Number"]').val();

        if (pId != "" && qty != "" && price != "") {

            rows.push({
                PartNo: partNumber,
                ProductId: pId,
                Qty: qty,
                UnitPrice: price,
                discountPercent: discountPercent,
                SalePrice: salePrice,
                Total: subTotal
            });
            var vat = parseFloat($('#VAT').val());
            var exp = parseFloat($('#Expenses').val());
            if (vat == "" || isNaN(vat)) { vat = 0; $('#VAT').val(0); }
            if (exp == "" || isNaN(exp)) { exp = 0; $('#Expenses').val(0); }
            var totalVExp = vat + exp;
            var perPieceExp = 0;
            var totalRowCount = $("#tbl tbody tr").length;
            if (totalVExp > 0) {
                //var totalProducts = rows.length;
                perPieceExp = parseFloat(totalVExp / totalRowCount);
                price = parseFloat(price) + parseFloat(perPieceExp);
            }
            stockLog.push({
                ProductId: pId,
                StockIN: qty,
                CostPrice: price,
                SalePrice: salePrice,
                Location: pLocation,
                InReference: 'Purchase'
            });
        }
        else {
            alert("Error!!!");
        }
    });

    if (rows.length) {

        total = $('#tbl tfoot tr').find('td:eq(10) input[id="txtTotal"]').val();
        //total = $('#Total').val()
        var orderDate = $('#OrderDate').val();

        var data = {

            'AccountID': $('#AccountID option:selected').val(),
            'InvoiceNo': $('#InvoiceNo').val(),
            'PurchaseDate': orderDate,
            'PaymentStatus': $('#PaymentStatus').val(),
            'PaymentTypeID': $('#ID option:selected').val(),
            'VAT': $('#VAT').val(),
            'Expenses': $('#Expenses').val(),
            'PurchaseCode': $('#PurchaseCode').val(),
            'Currency': $('#Currency').val(),
            'TotalAmount': total,
            'DiscountPercent': $('#DiscountPercent').val(),
            'DiscountAmount': $('#DiscountAmount').val(),
            'AmountPaid': $('#AmountPaid').val(),
            'tbl_PODetails': rows

        };

        var json = JSON.stringify({ 'model': data, 'modelStockLog': stockLog });
        console.log(json);

        ajaxCall("POST", json, "application/json; charset=utf-8", "/Purchase/SaveOrder", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            location.reload();
            //alert("success");
        }
        function onFailure(Result) {
            console.log(Result);
        }
    }
}