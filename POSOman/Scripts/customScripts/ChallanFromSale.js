﻿var customerID = 0;
var accID = 0;
var isPONOExist = false;
var isQuote = false;
var SOID = 0;
var DatE = null;

var isLimitExceed = false;
getHoldID();
getSOID();
$("#Bank").prop("disabled", true);
//  Get New SOID 
function ToJSDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
}
// Calculate Discount Amount from Discount Percentage 
function calcDiscount() {
    var disc = parseFloat($('#customerDiscount').val());
    var subTotal = $("#subAmount").val();
    if (disc == "" || isNaN(disc)) { applyDiscount(); }
    else if (parseFloat(subTotal) > 0) {
        var discAmount = parseFloat((disc * subTotal) / 100);
        $('#discInput').val(parseFloat(discAmount).toFixed(2));
        getNewTotal();
    }
}
function getSOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/getNewSOID',
        async: true,
        success: function (data) {
            {
                $("#hdnSOID").val(data);
                SOID = data;
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function getHoldID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Quotation/getLastHoldID',
        async: true,
        success: function (data) {
            {
                var value = data;
                var newValue = value.split('-');
                var increase = newValue[1];
                increase++;
                value = "H-" + increase;
                $("#hdnHoldID").val(value);
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}
$("#PONO").on("change", function () {
    var accID = $('#hdnAccountID').val();
    if (accID == "" || accID == 0) {
        swal("Customer", "Please Select Customer First!", "error");
    } else {
        var PONO = $("#PONO").val();
        checkPONO(PONO, accID);

    }
});

function checkPONO(PONO, accID) {

    var json = {
        "accountID": accID,
        "PONO": PONO
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/checkPONO',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            if (data == true) {
                toastr.warning('PONo already Exists for selected Customer!')
                $("#PONO").val("");
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

$('#getQuotation').click(function () {
    var qouteID = $('#QID').val();
    var orderID = 0;
    if (qouteID != "") {
        var json = {
            "QuoteID": qouteID
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Quotation/getQuotationOrderID',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                if (data > 0) {
                    orderID = data;
                    //console.log(orderID);
                    window.location.href = '/Quotation/SubmitOrders?OrderID=' + orderID;
                } else {
                    swal("Submitted", "Quotation Already Submitted!", "error");
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    } else {
        swal("QuotationID", "Enter QuotationID!", "error");
    }


});

$('#btnSubmit').click(function () {
    accID = $('#hdnAccountID').val();
    var isValid = true;
    var PONo = $('#PONO').val();
    var SalesDate = $('#SaleDate').val();
    var paymentStatus = $('#PaymentStatus option:selected').val();
    var rowCount = $('#tblProduct tbody tr').length;
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //var BranchID = $('#BranchID option:selected').val();
    var BranchID = $('#hdnBranchId').val();
    var amountPaid = $("#amountPaid").val();
    var balance = $("#balanceAmount").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    var bankAccountId = $('#Bank option:selected').val();
    if (PONo != "") {
        // check duplication
    }
    if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Customer", "Please Select Customer!", "error");
    }
    //if ($tableItemCounter == Count) {
    //    isValid = false;
    //    swal("Missed Products", "Products are not added after editing!", "warning");
    //} else 
    if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Sales Date!", "error");
        //console.log(PurchaseDate + "date");
    } else if (BranchID == "" || BranchID == 0 || BranchID == undefined) {
        isValid = false;
        swal("Branch", "Please Enter Branch!", "error");
        //alert("Please Select Vendor");
    } else if (rowCount == 0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");
        //console.log(PurchaseDate + "date");
    } else if (paymentStatus == 0) {
        isValid = false;
        swal("Payment Status", "Please Select Payment Status!", "error");
    } else if ((paymentStatus == 1 || paymentStatus == 2) && (PaymentTypeID == "" || PaymentTypeID == undefined)) {
        isValid = false;
        swal("Payment Type", "Please Select Payment Type!", "error");
    } else if ((paymentStatus == 2) && (amountPaid == "" || amountPaid == 0)) {
        isValid = false;
        swal("Amount Paid!", "Please Enter Amount!", "error");
    } else if ((PaymentTypeID == 3 || PaymentTypeID == 2) && (Bank == null || Bank == "" || Bank == undefined)) {
        isValid = false;
        console.log(Bank + "Bank");

        swal("Bank", "Please Select Bank Account!", "error");
    } else if (PaymentTypeID == 3 && (ChqNo == null || ChqNo == "")) {
        isValid = false;
        console.log(ChqNo + "ChqNo");


        swal("Cheque No.", "Please Enter Cheque Number!", "error");
    } else if ((paymentStatus == 2) && (parseFloat(balance) > parseFloat(creditLimit - customerBalance))) {
        isLimitExceed = false;
        //console.log('Partial Paid');
        swal("Credit Limit", "Credit Limit Exceeds! ", "error");
    } else if (PaymentTypeID == 3 && (DatE == null || DatE == "") && (ChqDate == null || ChqDate == "" || ChqDate == undefined)) {

        isValid = false;
        console.log(DatE + "chDate");
        console.log(ChqDate + "chnewDate");

        swal("Date", "Please Enter Cheque Date!", "error");
        //console.log(PurchaseDate + "date");
    }
    //else if (PaymentTypeID == 3 && (DatE == null || DatE == "") ) {

    //    isValid = false;
    //    console.log(DatE + "chDate");
    //    swal("Date", "Please Enter Cheque Date!", "error");
    //    //console.log(PurchaseDate + "date");
    //}
    else if (isLimitExceed == true) {
        //swal("Credit Limit", "You can not sale more than Credit Limit! ", "error");
        swal("Credit Limit", "Credit Limit Exceeds! ", "error");
    } else if (isValid == true && isLimitExceed == false) {
        $('#btnSubmit').prop('disabled', true);
        $('#btnHold').prop('disabled', true);
        uiBlock();
        insert();
    }
});
// Payment Status DropDOwn, Action according to Payment Status Selection
$("#PaymentStatus").on("change", function () {
    var payStatusSelection = $("#PaymentStatus").val();
    var totalAmount = $("#finalAmountWithVAT").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    //Paid
    if (payStatusSelection == 1) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        getNewTotal();
        //document.getElementById("amountPaid").value = totalAmount;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("balanceAmount").value = 0;
        //document.getElementById("expenseInput").readOnly = false;
        //document.getElementById("vatInput").readOnly = false;
    }
    // Partial PAid
    else if (payStatusSelection == 2) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = false;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
    }
    //Unpaid
    else if ((payStatusSelection == 3) && (parseFloat(totalAmount) > parseFloat(creditLimit - customerBalance))) { // Remove Nested if Condition 
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        $("#PaymentType").prop("disabled", true);
        disableBank();
        swal("Credit Limit", "Credit Limit Exceeds! ", "error");
    }
    //else if ((payStatusSelection == 3) && (parseFloat(totalAmount) <= parseFloat(creditLimit))) {                      // Remove Nested if Condition 

    //    isLimitExceed = false;//isLimitExceed = true;
    //    document.getElementById("amountPaid").readOnly = true;
    //    document.getElementById("amountPaid").value = 0;
    //    document.getElementById("balanceAmount").value = totalAmount;
    //    $("#PaymentType").prop("disabled", true);
    //    disableBank();
    //}
    else if (payStatusSelection == 3 && (creditLimit == "")) {
        isLimitExceed = false;
        swal("Credit Limit", "Credit Limit not available! ", "error");
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        $("#PaymentType").prop("disabled", true);
        disableBank();
    } else if (payStatusSelection == 3) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        document.getElementById("PaymentType").selectedIndex = "0";
        $("#PaymentType").prop("disabled", true);
        disableBank();
    } else {
        isLimitExceed = false;

    }

});

// Check credit limit when Unpaid Selected 
function checkCreditLimit() {
    var creditLimit = $('#customerCrLimit').val();
    var balance = $('#customerBalance').val();
    if ((parseFloat(totalAmount) > parseFloat(creditLimit))) {
        isLimitExceed = false;
        swal("Credit Limit", "Credit Limit not available! ", "error");
    } else {
        isLimitExceed = false;
    }
}

function disableBank() {
    $("#Bank").prop("disabled", true);
    $("#chqNumber").prop("disabled", true);
    $("#chqDate").prop("disabled", true);
}
// Payment Type Change 
$("#PaymentType").on("change", function () {
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //Cash
    if (PaymentTypeID == 1) {
        //$("#bankName").prop("disabled", true);
        $("#Bank").prop("disabled", true);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);
    }
    // Bank
    else if (PaymentTypeID == 2) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);

    }
    // Cheque
    else if (PaymentTypeID == 3) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", false);
        $("#chqDate").prop("disabled", false);
    }
});

function getNewTotal() {
    applyDiscount();
    calcVAT();
    calcAmountPaid();
    var subTotal = $("#subAmount").val();
    var VAT = $('#vatAmount').val();
    var discount = $('#discInput').val();
    if (VAT == "" || isNaN(VAT)) {
        VAT = 0;
        applyZeroVAT();
    }
    if (discount == "" || isNaN(discount)) {
        discount = 0;
        applyDiscount();
    }
    var newTotal = parseFloat(subTotal) + parseFloat(VAT) - parseFloat(discount);
    // var amountPaid = parseFloat(subTotal) + parseFloat(VAT);
    $('#finalAmountWithVAT').val(parseFloat(newTotal).toFixed(3));
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(newTotal).toFixed(3);
    }
}

function applyZeroVAT() {
    //var vat = parseFloat($('#vatInput').val());
    var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        //$('#vatInput').val(0);
        vat = 0;
        $('#vatAmount').val(0);
        $('#finalAmountWithVAT').val($("#totalAmount").val());
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat($("#totalAmount").val()).toFixed(3);
        }
    }
}


function applyDiscount() {
    var discount = $('#discInput').val();
    var subTotal = $("#subAmount").val();
    if (discount == "" || isNaN(discount)) {
        //$('#discInput').val(0);
        discount = 0;
        $('#totalAmount').val(subTotal);
    } else if (parseFloat(discount) >= parseFloat(subTotal)) {
        // toastr.warning('Discount must less than total!')
        $('#discInput').val(0);
        $('#totalAmount').val(subTotal);
    } else {
        var totalAmount = parseFloat(subTotal) - parseFloat(discount);
        $('#totalAmount').val(parseFloat(totalAmount).toFixed(3));
    }
}
$('#vatAmount').on('input', function (e) {
    getNewTotal();
});
// Get Customer Balance and Credit Limit 
function getCustomerDetail(accountID) {
    if (accountID == "") {
        accountID = -1;
    }
    var json = {
        "accountID": accountID
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Customer/getDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#customerCrLimit').val(data.creditLimit);
            $('#customerBalance').val(data.Balance);
            $('#customerDiscount').val(data.Discount);
        },
        error: function (err) {
            console.log(err);
        }
    });
}
// calculate balance if amountPaid entered and make changes every where needed
function calcAmountPaid() {
    isLimitExceed = false;
    var amountPaid = $("#amountPaid").val();
    var finalAmount = $('#finalAmountWithVAT').val();
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 2) {
        if (parseFloat(amountPaid) >= parseFloat(finalAmount)) {
            toastr.warning('Partial ! AmountPaid should be less than total amount ')
            $("#amountPaid").val(0)
        } else {
            var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
            $("#balanceAmount").val(parseFloat(balance).toFixed(3));
        }
    } else {

        var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
        $("#balanceAmount").val(parseFloat(balance).toFixed(3));
    }
}
// in case of vat entered please do calculations
function calcVAT() {
    //var vat = parseFloat($('#vatInput').val());
    var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        applyZeroVAT();
    } else {
        var totalInvoice = $("#totalAmount").val();
        //var vatAmount = parseFloat((vat * totalInvoice) / 100);
        var vatAmount = parseFloat(vat);
        // $('#vatAmount').val(parseFloat(vatAmount).toFixed(3));
        var finalAmount = parseFloat(vatAmount) + parseFloat(totalInvoice);
        $('#finalAmountWithVAT').val(finalAmount);
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat(finalAmount).toFixed(3);
        }
    }
}
// if Vat then calculate and make effects on total amount
$('#vatInput').on('input', function (e) {
    getNewTotal();
});
$('#vatAmount').on('input', function (e) {
    getNewTotal();
});
// if Discount then calculate and make effects on total amount
$('#discInput').on('input', function (e) {
    getNewTotal();
});
$("#cashReceived").on("change", function () {
    cash();
});
$("#amountPaid").on("change", function () {
    calcAmountPaid();
});
function cash() {
    var cashRec = parseFloat($('#cashReceived').val());
    var invoiceAmount = parseFloat($('#totalAmount').val());
    if (cashRec >= invoiceAmount) {
        var retAmount = parseFloat(cashRec) - parseFloat(invoiceAmount);
        $('#cashReturned').val(retAmount);
    }
    else {
        swal("Short", "Recd cash must greater than total!", "error");
    }
}
// Add data to array and send it to controller for order creation
function insert() {
    var saleDetails = [];
    
    var stockLog = [];
    var total = 0;
    var qtyVE = 0;
    var COGS = 0;
    var BranchId = $('#hdnBranchId').val();
    var Driver = $('#driver').val();
    var Transport = $('#transport').val();
    var driverVehicleNo = $('#driverVehicleNo').val();
    var grossWeight = $('#grossWeight').val();
    var netWeight = $('#netWeight').val();
    var isPOS = $('#hdnPOS').val();
    // console.log(BranchId +" branch id");
    invoiceAmount = $('#subAmount').val();
    //if (totalInvoice == "" || totalInvoice == 0) {
    var paidAmount = $('#amountPaid').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var cashRec = parseFloat($('#cashReceived').val());
    var cashRet = parseFloat($('#cashReturned').val());
    //console.log(totalInvoice);
    var vat = parseFloat($('#vatAmount').val());
    var exp = parseFloat($('#expenseInput').val());
    if (vat == "" || isNaN(vat)) {
        vat = 0;
        $('#vatInput').val(0);
    }
    //  get Table DAta 
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = $row.find('input[id*="productID"]').val();
        var costPrice = $row.find('input[id*="costPrice"]').val();
        var partNumber = $row.find("td").eq(2).text();
        var Qty = parseFloat($row.find("#ProductQty").text() || 0);
       
        var Packet = parseInt($row.find("#Packet").text() || 0);
        var SalePrice = +$row.find("#ProductSalePrice").text()||0;
        var SubTotal = parseFloat($row.find("#ProductSubTotal").text());
        var unitCode = $row.find("#unitCode").text();
        var isPack = $row.find('input[id*="isPacket"]').val();
        var qty = parseFloat(Qty);
       // SalePrice = parseFloat(SubTotal / qty);
        COGS += parseFloat(qty) * parseFloat(costPrice);
        saleDetails.push({
            PartNo: partNumber,
            ProductId: pId,
            Location: Location,
            UnitPrice: costPrice,
            UnitCode: unitCode,
            IsPack: isPack,
            Qty: qty,
            Packet: Packet,
            IsReturned: false,
            ReturnedQty: 0,
            SalePrice: SalePrice,
            Total: SubTotal,
            BranchID: BranchId,
        });
        stockLog.push({
            AccountID: $('#hdnAccountID').val(),
            ProductId: pId,
            StockOut: qty,
            Packet: Packet,
            SalePrice: SalePrice,
            CostPrice: costPrice,
            OutReference: 'Sales',
            OrderTypeID: 3,
            UserReferenceID: $('#PONO').val(),
            BranchID: BranchId,
            InvoiceDate: $('#SaleDate').val(),
            UnitCode: unitCode,
            IsPack: isPack
        });
    });

    $('#addPackagesTable tbody tr').each(function (i, n) {

        var $row = $(n);
        var RowID = +$row.find("#rowID_td").text() || 0;
        var ItemID = +$row.find("#hdnItemID_td").text() || 0;
        var DelQty = +$row.find("#hdnRawProductDelQty_td").text() || 0;
        var PQty = +$row.find("#hdnRawProductQty_td").text() || 0;
        var DeliveredQty = +$row.find("#hdnRawProductDeliveredQty_td").text() || 0;
        var orderID = +$row.find("#hdnOrderID_td").text() || 0;

        var desc = $row.find("#hdnDescription_td").text() ;
       
        //if ($row.find('input[id*="chkbox_tbl"]').is(":checked")) {
        //    pkgList.push({ RowID: RowID, ItemID: ItemID, OrderID: orderID, Description: desc, Qty: PQty, DeliverQty: DelQty, DeliveredQty: DeliveredQty  });
        //}
       
    });

    if (saleDetails.length && invoiceAmount > 0 && pkgList.length) {
       // var QuoteOrderID = $('#OrderID').val();
        var OrderID = $('#InvOrderId').val();
        //console.log("QuoteOrderID" +QuoteOrderID);
        var paymentStatus = 0;
        if ($('#PaymentStatus option:selected').val() != 0) {
            paymentStatus = $('#PaymentStatus option:selected').text()
        }
        var SaleAmount = parseFloat(total + vat).toFixed(3);
        var fsc_Cert = $("#FSC_Cert").is(":checked");
        var grs_Cert = $("#GRS_Cert").is(":checked");
        var oeko_tex_Cert = $("#OEKO_TEX_Cert").is(":checked");

        var data = {
            'OrderID': OrderId,
            'AccountID': $('#hdnAccountID').val(),
            'QuoteID': $('#hdnQuoteID').val(),
            'SOID': $("#hdnSOID").val(),
            'BranchID': BranchId,
            'PONO': $('#PONO').val(),
            'Remarks': $('#Remarks').val(),
            'CreditDays': $('#creditDays').val(),
            'VehicleNo': $('#VehicleNo').val(),
            'PaymentStatus': $('#PaymentStatus').val(),
            'SalesDate': $('#SaleDate').val(),
            'PaymentStatus': paymentStatus,
            'PaymentTypeID': $('#PaymentType option:selected').val(),
            'VAT': vat,
            'CustomerPO': $("#custPONumber").val(),
            'TotalAmount': invoiceAmount,
            'CashReceived': cashRec,
            'CashReturned': cashRet,
            'DiscountAmount': $('#discInput').val(),
            'AmountPaid': $('#amountPaid').val(),
            'TotalPaid': $('#amountPaid').val(),
            'ChequeDate': $("#chqDate").val(),
            'ChequeNo': $("#chqNumber").val(),
            'BankName': bank,
            'COGS': parseFloat(COGS),
            'tbl_SaleDetails': saleDetails
            
        };

        if (pkgList.length) {
            const ids = pkgList.map(o => o.RowID)
            pkgList = pkgList.filter(({ RowID }, ind) => !ids.includes(RowID, ind + 1));
        }
        var json = JSON.stringify({
           
            'model1': data,
            'pkgList': pkgList,
            'modelStockLog1': stockLog,
            'SaleOrderID': OrderID,
            'bankAccId': bankAccountId,
            'Driver': Driver,
            'Transport': Transport,
            'GrossWeight': grossWeight,
            'NetWeight': netWeight,
            'DriverVehicleNo': driverVehicleNo,
            'fsc_Cert': fsc_Cert,
            'grs_Cert': grs_Cert,
            'oeko_tex_Cert': oeko_tex_Cert
        });
        // console.log(json);
        ajaxCall("POST", json, "application/json; charset=utf-8", "/Sales/CreateChallanFromSales", "json", onSuccess, onFailure);

        function onSuccess(Result) {
            console.log(Result)
            uiUnBlock();
            if (Result != null) {
                if (Result.ChallanID > 0 && Result.JOID > 0) {
                    uiUnBlock();
                    swal("", "Updated", "success");
                    console.log(Result)
                    //window.open('/SalesOrder/printOrder?id=' + parseInt(Result) + '');
                    //window.open('/SalesOrder/Details?id=' + parseInt(Result) + '');
                    //console.log("OK");
                    window.open('/Challan/Export/' + Result.ChallanID + '?InvoiceType='+ Result.InvoiceType +'&JOID=' + Result.JOID + '&ReportType=PDF', 'Challan Invoice');
                    //window.location.href = '/Sales';
                    window.location.href = '/Sales/DepartmentWiseOrdersIndex';
                }
                else if (Result == -3) {
                    swal("", "Customer PO Required", "error");
                    console.log(Result)
                }
                else {
                    swal("", "Some error occured", "error");
                    console.log(Result)
                }
                
            } else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                $('#btnHold').prop('disabled', false);

                alert("Some error Ocurred! Please Check Your Entries");
            }
        }

        function onFailure(error) {
            uiUnBlock();
            if (error.statusText == "OK") {
                console.log("OK");
            } else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    } else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        $('#btnHold').prop('disabled', false);
        swal("Total", "Some error Ocurred! Please Check Sub Total !", "error");
    }
}

