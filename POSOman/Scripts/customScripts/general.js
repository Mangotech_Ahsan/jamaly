﻿function GetDropdown(ddl, method, _data, isSelectIncluded) {

    $("#" + ddl).empty();    
    if (isSelectIncluded)
        $("#" + ddl).append($("<option></option>").val("").html("Select"));
    $("#" + ddl).select2();
    ajaxCall("POST", _data, "application/json; charset=utf-8", method, "json", onSuccess);

    // to remove some  option try 
    //ddl.find('option').not(':first').remove();
    //$('#ddl option:not(:first)').remove();

    function onSuccess(Result) {

        var data = Result;// JSON.parse(Result.d);
        //var s = '<option value="-1">Please Select</option>'; 
        $.each(data, function (key, value) {
            $("#" + ddl).append($("<option></option>").val(value.Value).html(value.Name));
        });
    }
    // });

}
function GetDropdown1(ddl,data, isSelectIncluded) {
    
    $("#" + ddl).empty();
    if (isSelectIncluded)
        $("#" + ddl).append($("<option></option>").val("").html("Select"));
    $("#" + ddl).select2();
   { 
        $.each(data, function (key, value) {
            $("#" + ddl).append($("<option></option>").val(value.Value).html(value.Name));            
        });
    }
}
//Generic method use for making ajax call
                        
function ajaxCall(requestType, parameters, contentType, url, dataType, success, error, complete, beforeSend) {    
    $.ajax({
        type: requestType,
        contentType: contentType,
        url: url,
        async: false,
        data: parameters,
        dataType: dataType,
        beforeSend: beforeSend,
        success: success,
        error: error,
        complete: complete
    });    
}


function GetTableData(col, orderindex = 0, ordertype = "desc", extrainfo = '') {
    $('#tblData').DataTable({
        dom: 'Bfrtip',
        "order": [[orderindex, ordertype]],
        buttons: [
            {
                extend: 'print',
                title: '',
                messageTop: function () {
                    var html = '';//$("#PrintHeader").html();
                    html += extrainfo;
                    return html;

                },
                messageBottom: $("#PrintBottom").html(),
                exportOptions: {
                    columns: col
                }
            },
            {
                extend: 'excel',
                title: extrainfo,
                messageTop: '',
                messageBottom: "Developed By: Mangotech Solutions",
                exportOptions: {
                    columns: col
                }
            }
            //{
            //    extend: 'pdf',
            //    title: '',
            //    messageTop: '',
            //    messageBottom: '',
            //    exportOptions: {
            //        columns: col
            //    }
            //}
        ]
    });
}


function SpecificTableData(col, orderindex = 0, ordertype = "desc", extrainfo = '') {
    $('#tblData').DataTable({
        dom: 'Bfrtip',
        "paging": false,
        "order": [[orderindex, ordertype]],
        buttons: [
            {
                extend: 'print',
                title: '',
                messageTop: function () {
                    var html = '';//$("#PrintHeader").html();
                    html += extrainfo;
                    return html;

                },
                messageBottom: $("#PrintBottom").html(),
                exportOptions: {
                    columns: col
                }
            },
            {
                extend: 'excel',
                title: extrainfo,
                messageTop: '',
                messageBottom: "Developed By: Mangotech Solutions",
                exportOptions: {
                    columns: col
                }
            }
            //{
            //    extend: 'pdf',
            //    title: '',
            //    messageTop: '',
            //    messageBottom: '',
            //    exportOptions: {
            //        columns: col
            //    }
            //}
        ]
        
    });
}

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}



