﻿
var chqDat = "";
$(function () {
    $("#Bank").prop("disabled", true);
    document.getElementById("VoucherDate").readOnly = false;
    document.getElementById("ChequeDate").readOnly = true;
    document.getElementById("ChequeNumber").readOnly = true;
    $('#btnSave').click(function () {
        var vendorID = $('#AccountID option:selected').val();
        var voucherDate = $('#VoucherDate').val();
        chqDate = $('#ChequeDate').val();
        var PaymentTypeID = $('#PayTypeID option:selected').val();
        var bankAccountId = $('#Bank option:selected').val();
        if (chqDate == "") {
            chqDat = "";
        }
        if (vendorID == "" || vendorID == 0) {
            swal("Vendor", "Please Select Vendor!", "error");
        }
        else if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
        }
         else if ((PaymentTypeID == 3 || PaymentTypeID == 2) && (bankAccountId == "" || bankAccountId == undefined)) {
        isValid = false;
        swal("Bank!", "Please Select  Bank!", "error");
        }
        else {
            $('#btnSave').prop('disabled', true);
            uiBlock();
            insert();
        }
    });

});
var invoiceTotal = 0;



$("#AccountID").change(function () {
    getVenderBalance($("#AccountID").val());
    getVendorDetails($("#AccountID").val());
});

function getVenderBalance(accountId) {
    if (accountId == "") {
        accountId = -1;
    }
    var json = {
        "accountID": accountId
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/VendorPayment/GetVendorBalance',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#Balance').val(data);
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function getVendorDetails(accountId) {
    var controlPaying = '<input type = "Number" class="form-control">';

    ajaxCall("GET", { "accountId": accountId }, "application/json; charset=utf-8", "/VendorPayment/getVendorDetail",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        $('#tbl').find('tbody').empty();
        if (ien > 0) {

            var controlPaying = '<input type = "Number"  value=0>';
            var html = '';

            for (var i = 0; i < ien; i++) {
                var num = (data.qry[i].PurchaseDate).match(/\d+/g);
                var date = new Date(parseFloat(num));
                var month = parseInt(date.getMonth()) + 1;
                var dateString = date.getDate() + "/" + month + "/" + date.getFullYear();
                var totalAmount = parseFloat(data.qry[i].TotalAmount);
                var discount = parseFloat(data.qry[i].DiscountAmount);
                var expense = parseFloat(data.qry[i].Expenses || 0);
                var returnAmount = parseFloat(data.qry[i].ReturnAmount || 0);
                var VAT = parseFloat(data.qry[i].VAT || 0);
                //var invoiceTotal = parseFloat(totalAmount + VAT - discount).toFixed(3);
                var invoiceTotal = parseFloat(totalAmount).toFixed(3);
                var balance = parseFloat(invoiceTotal - expense - data.qry[i].TotalPaid - returnAmount).toFixed(3);
                var totalPaid = parseFloat(data.qry[i].TotalPaid);

                var adjustment = parseFloat(data.qry[i].Adjustment);
                var adjustmentDesc = data.qry[i].AdjustmentDescription;
                var vatType = data.qry[i].VATType||null;
                var vatPercent = data.qry[i].VATPercent;
                var PurchaseRequestID = data.qry[i].PurchaseRequest;

                var status = data.qry[i].PaymentStatus;
                if (status == null || status == "") {
                    status = "";
                }

                
                html += '<tr>';
                html += '<td><input type="hidden" value="' + data.qry[i].OrderID + '">' + data.qry[i].POID + '</td>';
                html += '<td>' + data.qry[i].InvoiceNo + '</td>';
                html += '<td>' + data.qry[i].PurchaseRequest + '</td>';
                html += '<td><input type="hidden" value="' + data.qry[i].AccountID + '">' + data.qry[i].AccountName + '</td>';
                html += '<td>' + dateString + '</td>';
                html += '<td>' + invoiceTotal + '</td>';
                html += '<td>' + totalPaid + '</td>';
                html += '<td>' + returnAmount + '</td>';
                html += '<td>' + status + '</td>';
                html += '<td>' + balance + '</td>';
                if (discount > 0) {
                    html += '<td>' + discount + '</td>';

                }
                else {
                    html += '<td><input type = "Number" value="0" min="0" max=' + balance + ' step="0" class="form-control" style="width:90px"></td>';

                }
                if (adjustment > 0) {
                    html += '<td>' + adjustment + '</td>';
                }
                else {
                    html += '<td><input type = "Number" value="0" min="0" max=' + balance + ' step="0" class="form-control" style="width:90px"></td>';
                }
                if (adjustmentDesc !== "noDesc") {
                    html += '<td>' + adjustmentDesc + '</td>';
                }
                else {
                    html += '<td><input type = "text" class="form-control"></td>';
                }
               
                if (vatType !=="noType") {
                    html += '<td><select><option selected value = ' + vatType + '>' + vatType + '</option></select></td>';
                }
                else {
                    
                    html += '<td><select><option value="GST">GST</option><option value="WHT">WHT</option></select></td>';
                }
                
                if (VAT > 0) {
                    html += '<td>' + vatPercent+'</td>';
                }
                else {
                    html += '<td><input type = "Number" value="0" min="0" max=' + balance + ' step="0" class="form-control" style="width:90px"></td>';
                }
                
                html += '<td><input type = "Number" value=' + balance +' readonly step="0" class="form-control" style="width:150px"></td>';
                html += '<td><input type = "Number"  min="0" max=' + balance + ' step="0" class="form-control" style="width:150px"></td> <span id="Error"></span>';

                html += '</tr>';
            }
            $('#tbl tbody').append(html);
        }
    }
    function onFailure(err) {
        console.log("ERROR");
    }

}

$("#PayTypeID").on("change", function () {
    var payStatusSelection = $("#PayTypeID").val();
    var totalAmount = $("#TotalAmount").val();
    if (payStatusSelection == 1) {
        $("#Bank").prop("disabled", true);
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;        
    }    
    else if (payStatusSelection == 2) {
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;
    }
    else if (payStatusSelection == 3) {
        $("#Bank").prop("disabled", false);
        //document.getElementById("ChequeDate").readOnly = false;
        document.getElementById("ChequeNumber").readOnly = false;
    }

});
function calcTotal() {
    console.log('this')
    var total = 0;
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = +$row.find('td:eq(16) input[type="Number"]').val() || 0;
        var balance = +$row.find('td:eq(9)').text() || 0;
        console.log($row.find("td").eq(0).text())
        console.log($row.find("td").eq(1).text())
        console.log($row.find("td").eq(2).text())
        console.log($row.find("td").eq(3).text())
        console.log($row.find("td").eq(4).text())
        console.log($row.find("td").eq(5).text())
        console.log($row.find("td").eq(6).text())
        console.log($row.find("td").eq(7).text())
        console.log($row.find("td").eq(8).text())
        console.log($row.find('td:eq(9) input[type="Number"]').val())
        console.log($row.find('td:eq(10) input[type="Number"]').val())
        console.log($row.find('td:eq(11) input[type="text"]').val())
        console.log($row.find('td:eq(12) :selected').text())
        //console.log($row.find("td").eq(10).val())
        //console.log($row.find("td").eq(11).val())
        //console.log($row.find("td").eq(12).text())
        console.log($row.find('td:eq(13) input[type="Number"]').val())
        console.log($row.find('td:eq(14) input[type="Number"]').val())
        console.log($row.find('td:eq(15) input[type="Number"]').val())
        //console.log($row.find("td").eq(14).text())
        //console.log($row.find("td").eq(15).val())
       
        $field = $row.find('td:eq(8)').text();
       // if (subTotal > Number($field.attr("max"))) {
        if (subTotal > Number($field)) {
            //$field.val($field.attr("max"));
            $field.val(Number($field));
            toastr.warning('Paying Amount must be equal or less than balance!')
            subTotal = parseFloat($row.find('td:eq(9)').text());
        }
        if ($.isNumeric(subTotal)) {
            console.log("inside ")
            var discount = +$row.find('td:eq(10) input[type="Number"]').val() || 0;
            var adjustments = +$row.find('td:eq(11) input[type="Number"]').val() || 0;
            var adjustmentDesc = $row.find('td:eq(12) input[type="text"]').val() || 0;
            var taxPercent = +$row.find('td:eq(14) input[type="Number"]').val() || 0;
            var paying = +$row.find('td:eq(16) input[type="Number"]').val() || 0;
            var taxAmount = taxPercent / 100 * (balance);
            var totBal = parseFloat(balance - (discount + adjustments + taxAmount)).toFixed(2);
            console.log("paying->" + paying);
            $row.find('td:eq(15) input[type="Number"]').val(totBal);

            total += parseFloat(subTotal);
            $('#Amount').val(parseFloat(total).toFixed(2));
        }
    });
    
    $('#tbl tfoot tr').find('td:eq(16) input[type="text"]').val(total);
}

$('#tbl').keyup(function (e) {
    calcTotal();
});

function insert() {
    var rows = [];
    var payLog = [];
    var JEntryLog = [];
    var totalAmount = 0;
    var totalPaying = parseFloat($('#tbl tfoot tr').find('td:eq(16) input[type="text"]').val()).toFixed(2);
    var orderId = 0;
    var vendorID = $('#AccountID option:selected').val();
    var desc = $('#Description').val();
    var payType = $('#PayTypeID option:selected').val()
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var voucherDate = $('#VoucherDate').val();
    var chqNumber = $('#ChequeNumber').val();
    var amount = parseFloat($('#Amount').val()).toFixed(2);
    console.log("total pay->" + totalPaying)
    console.log("amount->" + amount)
    if (totalPaying == amount) {
        $('#tbl tbody tr').each(function (i, n) {

            var $row = $(n);
            orderId = $row.find('td:eq(0) input[type="hidden"]').val();
            var accountId = $row.find('td:eq(2) input[type="hidden"]').val();
            var balance = parseFloat($row.find("td").eq(9).text()).toFixed(2) || 0;//$row.find('td:eq(8)').html();
            //var paying = $row.find('td:eq(9) input[type="Number"]').val();


            var discount = parseFloat($row.find('td:eq(10) input[type="Number"]').val()).toFixed(2) || 0;
            var totBal = parseFloat(+$row.find('td:eq(11) input[type="Number"]').val()).toFixed(2) || 0;
            var adjustments = +$row.find('td:eq(11) input[type="Number"]').val() || 0;
            var adjustmentDesc = $row.find('td:eq(12) input[type="text"]').val() || 0;
            var taxPercent = parseFloat($row.find('td:eq(14) input[type="Number"]').val()).toFixed(2) || 0;
            var taxType = $row.find('td:eq(13) :selected').text();
            var paying = +$row.find('td:eq(16) input[type="Number"]').val()|| 0;
            
            var taxAmount = parseFloat(taxPercent / 100 * (balance)).toFixed(2);
            console.log("tax Amount->" + taxAmount)
            //var totBal = parseFloat(balance - (discount + adjustments + taxAmount)).toFixed(2);
            if (orderId != "" && totBal != "" && paying != "") {
                debugger
                totalAmount += parseFloat(paying);
                totBal = parseFloat(totBal) - parseFloat(paying);
                rows.push({
                    OrderID: orderId,
                    Paying: paying,
                    Balance: totBal,
                    Discount: discount === "NaN" ? 0 : discount,
                    Tax: taxAmount === "NaN" ? 0 : taxAmount,
                    TaxType: taxType,
                    TaxPercent: taxPercent === "NaN" ? 0 : taxPercent,
                    Adjustment: adjustments,
                    AdjustmentDescription: adjustmentDesc
                });
                JEntryLog.push({
                    Amount: paying,
                    OrderTypeID: 1,
                    EntryTypeID: 1,
                    OrderID: orderId,
                    isReversed: false
                });
            }
        });
        if (rows.length) {

            var data = {
                'Description': desc,
                'Amount': amount,
                'AccountID': vendorID,
                'VoucherDate': voucherDate,
                'PaymentTypeID': payType,
                'BankName': bank,
                'ChequeDate': chqDate,
                'ChequeNumber': chqNumber,
                'Details': rows
            };
            
            var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog, 'bankAccId': bankAccountId });
            ajaxCall("POST", json, "application/json; charset=utf-8", "/VendorPayment/SavePayment", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result == "success") {
                    window.location.href = '/VendorPayment/Voucher?isNew=true&id=' + vendorID;
                }
                else {
                    swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                    uiUnBlock();
                }
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    location.reload();
            }
        }
        else {
            var data = {
                'Description': desc,
                'Amount': amount,
                'AccountID': vendorID,
                'VoucherDate': voucherDate,
                'PaymentTypeID': payType,
                'BankName': bank,
                'ChequeDate': chqDate,
                'ChequeNumber': chqNumber,
            };
            JEntryLog.push({
                    Amount: paying,
                    OrderTypeID: 1,
                    OrderID: orderId,
                    EntryTypeID: 1,
                    isReversed: false
                });
            var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog, 'bankAccId': bankAccountId });
            ajaxCall("POST", json, "application/json; charset=utf-8", "/VendorPayment/SavePayment", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result == "success") {
                    uiUnBlock();
                    window.location.href = '/VendorPayment';
                }
                else {
                    uiUnBlock();
                    $('#btnSave').prop('disabled', false);
                    swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                    uiUnBlock();
                }
            }
            function onFailure(error) {
                if (error.statusText == "OK") {
                    window.location.reload();                    
                }
            }
        }
    }
    else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);
        swal("Total", "Total Paying Must be Equal to Total Amount of Payment/Cheque!", "error");        
    }
}

