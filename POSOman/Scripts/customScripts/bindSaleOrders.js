﻿$tableItemCounter = 0;
$addedProductIDs = [];
var OrderID = 0;
var price = 0;


$("#tblProduct").focusout(function () {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var qty = $row.find("#ProductQty").text();
        var price = $row.find("#ProductSalePrice").text();
        if (($.isNumeric(price)) && ($.isNumeric(qty))) {
            var amount = (qty * price);
            $row.find("#ProductSubTotal").text(parseFloat(amount).toFixed(3));
            var subTotal = parseFloat($row.find('#ProductSubTotal').text());
            total += parseFloat(subTotal);
            calcTotal();
        }
    });
});
// Get Unsaved order and its details
function getTempOrderDetails() {    
    OrderID = $('#OrderID').val();
    ajaxCall("GET", { "OrderID": OrderID }, "application/json; charset=utf-8", "/Quotation/getOrderDetails",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        //console.log(ien);
        $('#tblProduct').find('tbody').empty();

        if (ien > 0) {
            bindOrderData(data);
            for (var i = 0; i < ien; i++) {                
                var vehCode = data.qry[i].VehicleCode;
                                      
                var vehCode = "";
                        var ProductID = data.qry[i].ProductID;
                        var PartNO = data.qry[i].PartNo;
                        var UnitCode = "";
                        var Qty =parseInt(data.qry[i].Qty); 
                        var Packet = $("#Packet").val();
                        var costPrice = parseFloat(data.qry[i].UnitPrice || 0);
                        var SalePrice = data.qry[i].SalePrice;
                        var SubTotal = data.qry[i].Total;
                        var cPrice = '<input type="hidden" id="costPrice" value="' + costPrice + '"/>';
                        var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';
                        var isPack = '<input type="hidden" id="isPacket" value="' + $("#isPacket").val() + '"/>';
                        var markup = "<tr><td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deleteRow(this)'/></td><td hidden>" + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td id=unitCode hidden>" + isPack + UnitCode + "</td><td>" + cPrice + "<input id='pQty' name='pQty' type = 'number' oninput='isNumberKey(this)' value=" + Qty + " class='form-control' style='width: 80px;'> </td><td id='Packet' hidden>" + Packet + "</td><td id='ProductSalePrice'>" + parseFloat(SalePrice).toFixed(3) + "</td><td id='ProductSubTotal'>" + SubTotal + "</td></tr>";
                $("#tblProduct tbody").append(markup);
                $tableItemCounter++;                
                $addedProductIDs.push(ProductID);                
                calcTotal();
                
            }
            setTimeout(1000);
            $("#vatAmount").val(Vatt);
            var FA = parseFloat($("#totalAmount").val()) + parseFloat($("#vatAmount").val());
            $('#finalAmountWithVAT').val(parseFloat(FA).toFixed(2));
            $("#amountPaid").val(parseFloat(FA).toFixed(2));
   

           // calcTotal();
          //  getNewTotal();
           

           
        }
    }
    function onFailure(err) {
        console.log(err);
    }
}
function checkNull(value) {
    return JSON.stringify(value).replace(/null/g, "\"\"");
}

$(function () {
    getTempOrderDetails();  
    
    function updateOrderStatus() {
        var json = { "OrderID": OrderID };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/TempPO/updateOrderStatus',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                console.log("IsDeleted flag true successfully!");
            },
            error: function (err) { console.log(err); }
        });
    }
});
