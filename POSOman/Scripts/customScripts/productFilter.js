﻿
$("#ddlVehCode").change(function () {
    var vehCodeID = $('#ddlVehCode').val();
   // getProductsByVehCode(vehCodeID);
});
$("#Group").change(function () {
    var groupID = $('#Group').val();
   // getProductsByGroup(groupID);
});
$("#Model").change(function () {
    var modelID = $('#Model').val();
    //getProductsByModel(modelID);
});
function getProductsByVehCode(vehCodeID) {

    //if (vehCodeID == "")
    //{ vehCodeID = -1; }
    //var json = { "vehCodeId": vehCodeID };
    //$.ajax({
    //    type: "POST",
    //    contentType: "application/json; charset=utf-8",
    //    url: '/Product/getProducts',
    //    async: true,
    //    data: JSON.stringify(json),
    //    success: function (data) {
    //        GetDropdown1("PartNo", data, true);
    //    },
    //    error: function (err) { console.log(err); }
    //});
}
function getProductsByGroup(groupID) {

    if (groupID == "")
    { groupID = -1; }
    var json = { "groupID": groupID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getProductsByGroup',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("PartNo", data, true);
        },
        error: function (err) { console.log(err); }
    });
}
function getProductsByModel(modelID) {

    if (modelID == "")
    { modelID = -1; }
    var json = { "modelID": modelID};
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getProductsByModel',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("PartNo", data, true);
        },
        error: function (err) { console.log(err); }
    });
}
function GetDropdown1(ddl, data, isSelectIncluded) {

    $("#" + ddl).empty();
    if (isSelectIncluded)
        $("#" + ddl).append($("<option></option>").val("").html("Select"));
    $("#" + ddl).select2();
    {
        $.each(data, function (key, value) {
            $("#" + ddl).append($("<option></option>").val(value.Value).html(value.Name));
        });
    }
}
