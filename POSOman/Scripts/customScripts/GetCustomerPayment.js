﻿var chqDat = "";
var AccountID = 0;
var soPaymentTypeID = 0;
$(function () {
    $("#Bank").prop("disabled", true);
    document.getElementById("ChequeDate").readOnly = true;
    document.getElementById("ChequeNumber").readOnly = true;
    document.getElementById("ChequeDate").readOnly = true;
   

    $('#btnSave').click(function () {
        //var customerID = $('#AccountID option:selected').val();
        var customerID = $('#hdnAccountID').val();
        var voucherDate = $('#VoucherDate').val();
        var BranchID = $('#BranchID option:selected').val();
        chqDate = $('#ChequeDate').val();

        if (chqDate == "") {
            //alert("Please cheque date");
            chqDat = "";
            //console.log(PurchaseDate+"date");
        }
        if (customerID == "" || customerID == 0) {
            swal("Customer", "Please Select Customer! ", "error");
            //alert("Please Select Vendor");
        }
        else if (voucherDate == "") {
            swal("Date", "Please Enter Payment Date! ", "error");
            //alert("Please Enter Payment Date");
            //console.log(PurchaseDate + "date");
        }
        else {
            $('#btnSave').prop('disabled', true);
            uiBlock();
            insert();
        }
    });

});
var invoiceTotal = 0;

$("#isSettlementAmount").change(function () {
        console.log($('#isSettlementAmount').is(":checked"))
    
})

$("#PayTypeID").on("change", function () {
    var payStatusSelection = $("#PayTypeID").val();
    var totalAmount = $("#TotalAmount").val();
    if (payStatusSelection == 1) {
        //document.getElementById("Bank").readOnly = true;
        $("#Bank").prop("disabled", true);
        //document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;
    }
    else if (payStatusSelection == 2) {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        //document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;
    }
    else if (payStatusSelection == 3) {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        //document.getElementById("ChequeDate").readOnly = false;
        document.getElementById("ChequeNumber").readOnly = false;
    }
});
function insert() {
    
    var JEntryLog = [];
    var totalAmount = 0;
    AccountID = $('#hdnAccountID').val();
    var desc = $('#Description').val();
    var payType = +$('#PayTypeID option:selected').val() || null;
    //var bank = $('#Bank').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var voucherDate = $('#VoucherDate').val();
    //var chqDate = $('#ChequeDate').val();
    var chqNumber = $('#ChequeNumber').val();
    var amount = parseFloat($('#Amount').val());
    var data = {
        //'VoucherName': 'Payment',
        'Description': desc,
        'Amount': amount,
        'AccountID': AccountID,
        'VoucherDate': voucherDate,
        'PaymentTypeID': payType,
        'IsSettleEntry': $('#isSettlementAmount').is(":checked"),
        'BankName': bank,
        'ChequeDate': chqDate,
        'ChequeNumber': chqNumber,
    };
    JEntryLog.push({
        //BranchID : BranchID,
        Amount: amount,
        OrderTypeID: 3,
        EntryTypeID: 2,
        isReversed: false
    });
    var json = JSON.stringify({ 'model': data, 'jentryLog': JEntryLog, 'bankAccId': bankAccountId });
    //console.log(json);            
    ajaxCall("POST", json, "application/json; charset=utf-8", "/CustomerPayment/SaveReceiving", "json", onSuccess, onFailure);
    function onSuccess(Result) {
        if (Result > 0) {
            window.location.href = '/CustomerPayment/GetCustomerPaymentReportFilterWise';
        }
        else {
            $('#btnSave').prop('disabled', false);
            uiUnBlock();
            swal("Error!", "Please Check Your Entries! ", "error");
            //alert("Some error Ocurred! Please Check Your Entries");
        }
    }
    function onFailure(error) {
        if (error.statusText == "OK")
            window.location.reload();
    }
}
