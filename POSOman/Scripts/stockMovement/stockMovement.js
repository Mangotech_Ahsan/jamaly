﻿var controlId = 0;
var products;
var pID = 0;
$(document).ready(function () {
        var vehCodeID = $('#ddlVehCode').val();
        
        //The url we will send our get request to
        var valueUrl = '@Url.Action("GetValues", "Product")';
        // if (vehCodeID > 0) {
        $('#ddlPartNo').select2(
          {
              placeholder: 'Enter Product',
              //Does the user have to enter any data before sending the ajax request
              minimumInputLength: 0,
              allowClear: true,
              ajax: {
                  //How long the user has to pause their typing before sending the next request
                  delay: 200,
                  //The url of the json service
                  url: '/Product/GetValues',
                  dataType: 'json',
                  async: true,
                  //Our search term and what page we are on
                  data: function (params) {
                      return {
                          pageSize: 1500,
                          pageNum: params.page || 1,
                          searchTerm: params.term,
                          //Value from client side.
                          countyId: $('#ddlVehCode').val()
                      };
                  },
                  processResults: function (data, params) {
                      params.page = params.page || 1;
                      return {
                          results: $.map(data.Results, function (obj) {
                              return { id: obj.ProductID, text: obj.PartNo };
                          }),
                          pagination: {
                              more: (params.page * 1500) <= data.Total
                          }
                      };
                  }
              }
          });
});
// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
$(function () {
    $('#btnSave').click(function () {        
        insert();
    })
})
$("#ddlVehCode").change(function () {    
    var vehCodeID = $('#ddlVehCode').val();    
    //if (vehCodeID == "")
    //{ vehCodeID = -1; }
    //var json = { "vehCodeId": vehCodeID };
    //$.ajax({
    //    type: "POST",
    //    contentType: "application/json; charset=utf-8",
    //    url: '/Product/getProducts',
    //    async: true,
    //    data: JSON.stringify(json),
    //    success: function (data) {            
    //        GetDropdown1("ddlPartNo", data, true);
    //    },
    //    error: function (err) { console.log(err); }
    //});
});
 
// Get  Stock in All Branches by selected Product
function getProductStock(productID) {
    
    //console.log(accountId);

    var controlMovingQty = '<input type = "Number" class="form-control" id="qtyMoving">';

    ajaxCall("GET", { "iProductID": productID }, "application/json; charset=utf-8", "/Stock/getStockDetail",
        "json", onSuccess, onFailure);    
    function onSuccess(data) {
        getProducts();
        var total = 0;
        var ien = data.qry.length;
        $('#tbl').find('tbody').empty();
        if (ien > 0) {            
            var html = '';           
            for (var i = 0; i < ien; i++) {
                var BranchSelect = '<select id="ddlBranch' + controlId + '" class="form-control"></select>';
                var prodID = data.qry[i].ProductID;
                var onMove = 0;
                if (data.qry[i].OnMove > 0)
                {
                    onMove = data.qry[i].OnMove;
                }
                var BalanceQty = parseFloat(data.qry[i].Qty - data.qry[i].OnMove);
                var vehCode = (data.qry[i].VehicleCode);                
                var location = (data.qry[i].Location);  
                var partNo = (data.qry[i].PartNo);         
                var branch = (data.qry[i].BranchName);
                var branchID = (data.qry[i].BranchID);
                var qty = data.qry[i].Qty;
                var isPack = data.qry[i].IsPacket;
                var unitPerCarton = 1;//data.qry[i].UnitPerCarton;
                
                var levelID = data.qry[i].LevelID;
                var levelIDField = '<input type="hidden" id="LevelID" value="' + levelID + '"/>';
                var isPackField = '<input type="hidden" id="isPacket" value="' + isPack + '"/>';
                var unitCTN = '<input type="hidden" id="unitPerCTN" value="' + unitPerCarton + '"/>';
                if (levelID == 1) {
                    qty = parseFloat(qty / unitPerCarton).toFixed(2);
                    onMove = parseFloat(onMove / unitPerCarton).toFixed(2);
                    BalanceQty = parseFloat(BalanceQty / unitPerCarton).toFixed(2);
                }
                else if (levelID == 2 || levelID == 3) {
                    qty = parseFloat(qty / unitPerCarton / 1000).toFixed(2);
                    onMove = parseFloat(onMove / unitPerCarton / 1000).toFixed(2);
                    BalanceQty = parseFloat(BalanceQty / unitPerCarton / 1000).toFixed(2);
                }
                else {
                    qty = parseFloat(qty).toFixed(0);
                    onMove = parseFloat(onMove).toFixed(0);
                    BalanceQty = parseFloat(BalanceQty).toFixed(0);
                }
                var costPrice = (data.qry[i].CostPrice);
                var salePrice = (data.qry[i].SalePrice);
                html += '<tr>';                
                html += '<td><input type="hidden" id="hdnCPrice" value="' + costPrice + '">' + vehCode + '</td>';                
                html += '<td><input type="hidden" id="hdnpID" value="' + prodID + '">' + partNo + '</td>';               
                html += '<td><input type="hidden" id="hdnBranchID" value="' + branchID + '">' + branch + '</td>';                
                html += '<td><input type="hidden" id="hdnSPrice" value="' + salePrice + '">' + qty + '</td>';
                html += '<td>' + unitCTN + isPackField + onMove + '</td>';
                html += '<td><input type="hidden" id="hdnLocation" value="' + location + '">' + levelIDField + BalanceQty + '</td>';
                html += '<td><input type="hidden" id="hdnControlID" value="' + controlId + '">' + BranchSelect + '</td>';
                html += '<td><input id="movingQty" name="movingQty" type = "Number" onkeypress="return isNumberKey(event)"  min="0" max=' + BalanceQty + ' step="0" class="form-control"></td> <span id="Error"></span>';
                html += '</tr>';               
                controlId += 1;                              
                var subQty = qty;
                total += parseInt(subQty);
                $('#totalQty').val(total);
            }
            $('#tbl tbody').append(html);
            GetBranchDropdown();
            
        }
    }
    function onFailure(err) {
        console.log(err);
    }
}
// Get Branch Lists for dropdown
function getProducts(){           
    ajaxCall("POST", {}, "application/json; charset=utf-8", "/Stock/getBranches", "json", onSuccess);
    function onSuccess(data) {   
        products = data;        
    }    
    function onFailure(err) {
        console.log(err);
    }    
}
// Populate Dropdown Branches , Excluding Branch in which Product Exists
function GetBranchDropdown() {

    $('#tbl tbody tr').each(function (i, n) {
        
        var $row = $(n);
        var branchID = $row.find('input[id*="hdnBranchID"]').val();
        //var pId = $row.find('input[id*="hdnPId"]').val(pId);
        var controlId = $row.find('input[id*="hdnControlID"]').val();
        var ddl = "ddlBranch" + controlId;
        $(ddl).empty();
        $(ddl).select2();        
        $.each(products, function (key, value) {
            $("#" + ddl).append($("<option></option>").val(value.Value).html(value.Name));
            //$("#" + ddl + "option[value='++']").remove();
            $("#" +ddl).find('option[value='+branchID+']').remove();            
        });
    });        
}
function checkNull(value) {
    return JSON.stringify(value).replace(/null/g, "\"\"");
}
// Prevent user to enter quantity more than avaialable qty 
$('#tbl').keyup(function (e) {
    $field = $(e.target).closest("tr").find('input[id*="movingQty"]');
    var movingQty = $(e.target).closest("tr").find('input[id*="movingQty"]').val();    
    if (movingQty > Number($field.attr("max"))) {
        var max = $field.attr("max");
        $field.val(0);
        toastr.warning('Moving quantity must be equal or less than Balance Qty!')
    }
    if (parseInt(movingQty) > 0) {
        $('#btnSave').prop('disabled', false);
    }    
});

// Move Stock
function insert() {
    var stockMoving = [];    
    $('#tbl tbody tr').each(function (i, n) {
        var $row = $(n);
        // var orderID = $row.find('td:eq(0)').html();        
        var movingQty = $row.find('input[id*="movingQty"]').val();
        if (movingQty != "" && movingQty>0) {
        var branchID = $row.find('input[id*="hdnBranchID"]').val(); // Moving From 
        pID = $row.find('input[id*="hdnpID"]').val();   // can also select from main ddl
        var movingBranchID = $row.find('td:eq(6) :selected').val();  // branch id moving to   
        var unitPerCTN = $row.find('input[id*="unitPerCTN"]').val();
        var costPrice = $row.find('input[id*="hdnCPrice"]').val();
        var salePrice = $row.find('input[id*="hdnSPrice"]').val();
        var location = $row.find('input[id*="hdnLocation"]').val();
        var qty = parseFloat(movingQty * unitPerCTN);
        var levelID = $row.find('input[id*="LevelID"]').val();
        var isPack = $row.find('input[id*="isPacket"]').val();
        if (levelID == '2' || levelID == '3') {
            qty = parseFloat(qty * 1000);
        }
        var minorDivisor = 1000;
            stockMoving.push({
                ProductId: pID,
                Location : location,
                StockMoved: qty,
                StockReceived: 0,
                CostPrice: parseFloat(costPrice).toFixed(3),
                SalePrice: salePrice,                             
                FromBranchID: branchID,
                ToBranchID: movingBranchID,
                LevelID: levelID,
                UnitPerCarton: unitPerCTN,
                IsPack: isPack,
                MinorDivisor: minorDivisor
            });
        }
    });
    if (stockMoving.length) {
        var json = JSON.stringify({ 'modelStockMoving': stockMoving});
        //console.log(json);            
        ajaxCall("POST", json, "application/json; charset=utf-8", "/Stock/MoveStock", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result == "success") {
            //if (Result) {
                getProductStock(pID);
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK")               
            console.log(error);
            //location.reload();
        }
    }   
}