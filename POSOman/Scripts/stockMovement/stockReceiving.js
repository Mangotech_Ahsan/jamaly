﻿var controlId = 0;
var detailID = 0;
var products;
var pID = 0;
$(function () {
    var BranchId = $('#hdnBranchId').val();
    getPendingStock(BranchId);
    $('#btnSave').click(function () {        
        insert();
    })
})
// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

$("#ddlVehCode").change(function () {    
    var vehCodeID = $('#ddlVehCode').val();    
    if (vehCodeID == "")
    { vehCodeID = -1; }
    var json = { "vehCodeId": vehCodeID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getProducts',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {            
            GetDropdown1("ddlPartNo", data, true);
        },
        error: function (err) { console.log(err); }
    });
});
 
// Get  Stock in All Branches by selected Product
function getPendingStock(branchID) {    

    var controlMovingQty = '<input type = "Number" class="form-control" id="qtyReceiving">';

    ajaxCall("GET", { "iBranchID": branchID }, "application/json; charset=utf-8", "/Stock/getPendingStock",
        "json", onSuccess, onFailure);    
    function onSuccess(data) {        
        var total = 0;
        var ien = data.qry.length;
        $('#tbl').find('tbody').empty();
        if (ien > 0) {
            var html = '';
            for (var i = 0; i < ien; i++) {
                var inputLoc = '<input id="Location' + controlId + '" class="form-control" value=' + checkNull(data.qry[i].Location) + '>';
                var inputDetail = '<input id="Detail' + detailID + '" class="form-control">';
                var prodID = data.qry[i].ProductID;
                var ID = data.qry[i].ID;
                var vehCode = (data.qry[i].VehicleCode);
                var partNo = (data.qry[i].PartNo);
                var branchFrom = (data.qry[i].FromBranchName);
                var branchTo = (data.qry[i].ToBranchName);
                var branchIDFrom = (data.qry[i].FromBranchID);
                var branchIDTo = (data.qry[i].ToBranchID);
                var qtyMoved = (data.qry[i].StockMoved);
                var costPrice = (data.qry[i].CostPrice);           
                var salePrice = (data.qry[i].SalePrice);
                var isPack = data.qry[i].IsPacket;
                var unitPerCarton = data.qry[i].UnitPerCarton;
                var levelID = data.qry[i].LevelID;
                var levelIDField = '<input type="hidden" id="LevelID" value="' + levelID + '"/>';
                var isPackField = '<input type="hidden" id="isPacket" value="' + isPack + '"/>';
                var unitCTN = '<input type="hidden" id="unitPerCTN" value="' + unitPerCarton + '"/>';
                if (levelID == 1) {
                    qtyMoved = parseFloat(qtyMoved / unitPerCarton).toFixed(2);
                }
                else if (levelID == 2 || levelID == 3) {
                    qtyMoved = parseFloat(qtyMoved / unitPerCarton / 1000).toFixed(2);
                }
                else {
                    qtyMoved = parseFloat(qtyMoved).toFixed(0);
                }
                html += '<tr>';
                //html+='<td>' + <input type=checkbox >+ '</td>';
                html += '<td><input type="hidden" id="hdnCPrice" value="' + costPrice + '">' + unitCTN + vehCode + '</td>';
                html += '<td><input type="hidden" id="hdnpID" value="' + prodID + '">' + partNo + '</td>';
                html += '<td><input type="hidden" id="hdnBranchIDFrom" value="' + branchIDFrom + '">' + branchFrom + '</td>';
                html += '<td><input type="hidden" id="hdnBranchIDTo" value="' + branchIDTo + '">' + branchTo + '</td>';
                html += '<td id="qtyMoved"><input type="hidden" id="hdnSPrice" value="' + salePrice + '">' + qtyMoved + '</td>';
                html += '<td><input type="hidden" id="hdnID" value="' + ID + '"><input id="movingQty" name="movingQty" type = "Number" onkeypress="return isNumberKey(event)"  min="0" max=' + qtyMoved + ' step="0" class="form-control"></td> <span id="Error"></span>';               
                html += '<td>' + levelIDField + isPackField + inputDetail + '</td>';
                html += '</tr>';
                controlId += 1;
                detailID += 1;
            }
            var bottom = '<div><table class="table table-bordered">';
                bottom += ' <tbody><tr>';
                bottom += '<td colspan="6" style="padding-top:130px;">';
                bottom += '<span style="float:left; padding:10px 0px 10px 50px;"><strong style="border-top:2px solid #7a102e;color:#7a102e;">Issuer Signature</strong></span>';
                bottom += '<span style="float:right; padding:10px 50px 10px 0px"><strong style="border-top:2px solid #7a102e; color:#7a102e;">Receiver Signature</strong></span>';
                bottom += '</td></tr>';
                bottom += '</tbody></table></div>';
            $('#tbl tbody').append(html);
            $('#tbl').dataTable({
                "paging": false,
                dom: 'Brtip',
                buttons: [
                    {
                        extend: 'print',
                        messageTop: function () {
                            //if ( printCounter === 1 ) {
                            return 'Stock Receiving..';
                            //}
                            //else {
                            //    return 'You have printed this document '+printCounter+' times';
                            //}
                        },
                        messageBottom: bottom,
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, ]
                        }
                    }
                ]
            });
        }
        else {
            swal("No Stock", "No Stock to Receive!", "error");
            //alert("No Stock to Receive!!");
        }
    }
    function onFailure(err) {
        console.log(err);
    }
}

// Populate Dropdown Branches , Excluding Branch in which Product Exists
function checkNull(value) {
    return JSON.stringify(value).replace(/null/g, "\"\"");
}
// Prevent user to enter quantity more than avaialable qty 
$('#tbl').keyup(function (e) {
    $field = $(e.target).closest("tr").find('input[id*="movingQty"]');
    var movingQty = $(e.target).closest("tr").find('input[id*="movingQty"]').val();
    //console.log(movingQty);
    if (movingQty > Number($field.attr("max"))) {
        var max = $field.attr("max");
        $field.val(0);
        toastr.warning('Receiving quantity must be equal or less than Moved Qty!')
    }
    $('#btnSave').prop('disabled', false);
});

// Move Stock
function insert() {
    var stockLogIn = [];
    var stockLogOut = [];
    var stockMoving = [];
    $('#tbl tbody tr').each(function (i, n) {

        var $row = $(n);
        // var orderID = $row.find('td:eq(0)').html();        
        var movingQty = $row.find('input[id*="movingQty"]').val();
        
        if (movingQty != "" && movingQty >= 0) {
        var fromBranchID = $row.find('input[id*="hdnBranchIDFrom"]').val(); // Moving From 
        pID = $row.find('input[id*="hdnpID"]').val();   // can also select from main ddl
        ID = $row.find('input[id*="hdnID"]').val();    // stock moving Primary key 
        var toBranchID = $row.find('input[id*="hdnBranchIDTo"]').val();  // branch id moving to                 
        var costPrice = $row.find('input[id*="hdnCPrice"]').val();
        var salePrice = $row.find('input[id*="hdnSPrice"]').val();        
        var detail = $row.find('input[id*="Detail"]').val();
        var qtyMoved = $row.find("#qtyMoved").text();
        
        var unitPerCTN = $row.find('input[id*="unitPerCTN"]').val();       
        var levelID = $row.find('input[id*="LevelID"]').val();
        var isPack = $row.find('input[id*="isPacket"]').val();
        
        movingQty = parseFloat(movingQty * unitPerCTN);
        if (levelID == '2' || levelID == '3') {
            movingQty = parseFloat(movingQty * 1000);
        }

        var minorDivisor = 1000;
            stockMoving.push({
                ID       : ID,
                ProductId: pID,
                StockMoved: qtyMoved,
                StockReceived: movingQty,
                CostPrice: parseFloat(costPrice).toFixed(3),
                SalePrice: salePrice,
                FromBranchID: fromBranchID,
                ToBranchID: toBranchID,
                Detail :detail,
                LevelID: levelID,
                UnitPerCarton: unitPerCTN,
                IsPack: isPack,
                MinorDivisor: minorDivisor
            });
            stockLogIn.push({
                ProductId: pID,
                StockIN: movingQty,
                StockOut: movingQty,
                CostPrice: parseFloat(costPrice).toFixed(3),
                SalePrice: salePrice,
                InReference: 'Stock Moving',
                OrderID: ID,                
                FromBranchID: fromBranchID,
                ToBranchID: toBranchID,
                LevelID: levelID,
                UnitPerCarton: unitPerCTN,
                IsPack: isPack,
                MinorDivisor: minorDivisor
            });
        }
    });
    if (stockLogIn.length) {
        var json = JSON.stringify({ 'modelStockLogIn': stockLogIn, 'modelStockMoving': stockMoving });
      //  console.log(json);            
        ajaxCall("POST", json, "application/json; charset=utf-8", "/Stock/SaveStockReceiving", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result == "success") {
                //getProductStock(pID);
                // alert("Success");
                window.location.href = '/Stock/ReceiveStock';
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                //alert("Some error Ocurred! Please Check Your Entries");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK")
                console.log(error);
        }
    }
    else {
        swal("Quantity", "Please Enter Some Qty!", "error");
    }
}