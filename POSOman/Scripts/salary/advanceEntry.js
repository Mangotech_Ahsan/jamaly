﻿var chqDat = "";
$(function () {
    $("#Bank").prop("disabled", true);
    //document.getElementById("Bank").readOnly = true;
    document.getElementById("ChequeDate").readOnly = true;
    document.getElementById("ChequeNumber").readOnly = true;
    document.getElementById("ChequeDate").readOnly = true;
    $('#btnSave').click(function () {
        var employeeID = $('#AccountID option:selected').val();
        var voucherDate = $('#VoucherDate').val();
        var amount = $('#Amount').val();
        var chqNumber = $('#ChequeNumber').val();
        chqDate = $('#ChequeDate').val();
        var PaymentTypeID = $('#PaymentType option:selected').val();
        //if (chqDate == "") {
        //    //alert("Please cheque date");
        //    chqDat = "";
        //    //console.log(PurchaseDate+"date");
        //}
        if (employeeID == "" || employeeID == 0) {
            swal("Employee", "Please Select Employee Type!", "error");
        }
        else if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if (amount == "" || amount == 0) {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if ((PaymentTypeID == 3) && (chqNumber == "")) {
            isValid = false;
            swal("Cheque Number", "Please Enter Cheque Number!", "error");
        }
        else if ((PaymentTypeID == 3) && (chqDate == "")) {
            isValid = false;
            swal("Cheque Date", "Please Enter Cheque Date!", "error");
        }
        else {
            $('#btnSave').prop('disabled', true);
            uiBlock();
            insert();
        }
    });

});
$("#PayTypeID").on("change", function () {
    var payStatusSelection = $("#PayTypeID").val();
    var totalAmount = $("#TotalAmount").val();
    if (payStatusSelection == 1) {
        //document.getElementById("Bank").readOnly = true;  
        $("#Bank").prop("disabled", true);
        document.getElementById("ChequeNumber").readOnly = true;
        document.getElementById("ChequeDate").readOnly = true;
        chqDate = "";
    }
    else if (payStatusSelection == 2) {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeNumber").readOnly = true;
        document.getElementById("ChequeDate").readOnly = true;
        chqDate = "";
    }
    else if (payStatusSelection == 3) {
        //document.getElementById("Bank").readOnly = false;        
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeNumber").readOnly = false;
        document.getElementById("ChequeDate").readOnly = false;
        chqDate = $('#ChequeDate').val();
    }
});
function insert() {    
    var Details = "";
    var totalAmount = 0;          
    var employeeID = $('#AccountID option:selected').val();
    var BranchID = $('#BranchID option:selected').val();   // should be readonly in Desktop
    var desc = $('#Description').val();
    var payType = $('#PayTypeID option:selected').val()
    //var bank = $('#Bank').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var voucherDate = $('#VoucherDate').val();
    //var chqDate = $('#ChequeDate').val();
    var chqNumber = $('#ChequeNumber').val();
    var amount = parseFloat($('#Amount').val());
    //if (totalPaying == amount)
     
            var data = {
                'BranchID': BranchID,
                'Description': desc,
                'Amount': amount,
                'AccountID': employeeID,
                'VoucherDate': voucherDate,
                'PaymentTypeID': payType,
                'BankName': bank,
                'ChequeDate': chqDate,
                'ChequeNumber': chqNumber
            };
            Details = {
                'EmployeeID': employeeID,
                'BranchID': BranchID,
                'Dr': amount,
                'Cr': 0,
                'JEntryTypeID': 6,
                'Description': 'Advance/Loan',
                'Date': voucherDate
            };
            //console.log(Details + " Details");
            //console.log(data+ " data");
            var json = JSON.stringify({ 'model': data, 'employeeLog': Details, 'bankAccId': bankAccountId });
            //console.log(json);            
            ajaxCall("POST", json, "application/json; charset=utf-8", "/Salary/SaveAdvance", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result > 0) {
                    uiUnBlock();
                    window.open('/Salary/Details?isNew=true&id=' + Result + '');

                   // window.location.href = '/Salary/Details?isNew=true&id=' + Result;
                    window.location.href = '/Salary/CreateAdvance';
                }
                else {
                    uiUnBlock();
                    $('#btnSave').prop('disabled', false);                    
                    swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                }
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    //alert("success");
                    //console.log(error);
                    window.location.reload();
            }
        
    
    
}