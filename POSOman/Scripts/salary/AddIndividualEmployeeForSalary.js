﻿$tableItemCounter = 0;
$addedEmployeeIDs = [];
$selCustID = 0;
//$('#advance').on('input', function (e) {
//    calcSubTotal();
//});
//$('#Salary').on('input', function (e) {
//    calcSubTotal();
//});
//$('#deduction').on('input', function (e) {    
//    calcSubTotal();
//});

$('#NoOfDays').on('input', function () {
    var FromBasicSal = 0;
    var TotSal = 0;
    var NoOfDays = $("#NoOfDays").val() || 0;
    var BasicSal = $("#BasicSalary").val() || 0;

    if (NoOfDays > 0 && NoOfDays <= 26) {
       

        if (BasicSal > 0) {
            FromBasicSal = (BasicSal / 26);
            FromBasicSal = FromBasicSal * (26 - NoOfDays);
            TotSal = BasicSal - FromBasicSal;

            $("#Salary").val(parseFloat(TotSal).toFixed(2));
            $("#totalOverTime").val(parseFloat(TotSal).toFixed(2));
        }
        else {
            $("#Salary").val(parseFloat(BasicSal).toFixed(2));
            $("#totalOverTime").val(parseFloat(BasicSal).toFixed(2));
           $("#NoOfDays").val('');
        }

    }
    else {
        $("#Salary").val(parseFloat(BasicSal).toFixed(2));
        $("#totalOverTime").val(parseFloat(BasicSal).toFixed(2));
        $("#NoOfDays").val('');
    }

});

$('#overTime').on('input', function (e) {
    calcSubTotal();
});

function calcSubTotal() {
    // var salary = $('#Salary').val();
    // var adv = $('#advance').val();
    var overTime = $('#overTime').val();
    //  var deduction = $('#deduction').val();
    //if (salary == "")
    //    salary = 0;
    //if (deduction == "")
    //    deduction = 0;
    //if (adv == "")
    //    adv = 0;
    if (overTime == "")
        overTime = 0;
    var total = /*parseFloat(salary) +*/ parseFloat(overTime);//- parseFloat(deduction);
    $('#SubTotal').val(parseFloat(total).toFixed(3));
    calcTotal();
}

$("#ddlEmployee").change(function () {
    var employeeID = $('#ddlEmployee').val();
    $('#hdnAccountID').val(employeeID);
    if (employeeID > 0) { getDetail(employeeID); }

});
$("#tblProduct").focusout(function () {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        //  var salary = $row.find("#Salary").text();
        // var advance = $row.find("#advance").text();
        var overTime = $row.find("#overTime").text();
        //  var deduction = $row.find("#deduction").text();
        //if (salary == "")
        //    salary = 0;
        //if (deduction == "")
        //    deduction = 0;
        //if (advance == "")
        //    advance = 0;
        if (overTime == "")
            overTime = 0;
        var total =/* parseFloat(salary) +*/ parseFloat(overTime);// - parseFloat(deduction);
        $row.find("#ProductSubTotal").text(parseFloat(total).toFixed(3));
        total += parseFloat(total);
    });
    calcTotal();
});


function calcTotal() {
    var totalSalary = 0;
    var totalOverTime = parseFloat($("#Salary").val())||0;
    var totalDeduction = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subOverTime = parseFloat($row.find('#overTime').text());

        if (subOverTime == "" || isNaN(subOverTime)) {
            subOverTime = 0;
        }

        if ($row.find('#addsub').text() == "Addition") {
            totalOverTime += parseFloat(subOverTime);
        }
        else {
            totalOverTime -= parseFloat(subOverTime);
        }
        
    });

    $("#totalOverTime").val(parseFloat(totalOverTime).toFixed(3));
}

$("#addEmployee").click(function () {
    var employee = $("#ddlEmployee :selected").text();  // 1
    var employeeID = $("#ddlEmployee").val(); // hidden        
    //  var salary = $("#Salary").val();    
    //var advance = $("#advance").val();
    var IDNo = $("#IDNO").text();
    var Desc = $("#Desc").val() || "null";
    //  alert(Desc);
    //console.log(advance + " advance");
    var overTime = $("#overTime").val();
    //console.log(overTime + " overTime");
    // var deduction = $("#deduction").val();
    //console.log(deduction + " deduction");
    var SubTotal = $("#SubTotal").val();

    var devisionID = $("#devision").val();
    var devision = $("#devision :selected").text();
    var div = '<input type="hidden" id="devisionID" value="' + devisionID + '"/>';


    var empId = '<input type="hidden" id="employeeID" value="' + employeeID + '"/>';
    if (employeeID > 0 && SubTotal != 0 && Desc != "null") {

        var markup = "<tr><td><input type='checkbox' name='record'></td><td hidden>" + empId + "" + employee + "</td><td id=IDNO hidden>" + IDNo + "</td><td id=Desc>" + Desc + "</td><td id='addsub'>" + $("#devision option:selected").text() + "</td><td contenteditable='true' id='decesion' hidden>" + div + devision + "</td><td contenteditable='true' id='overTime'>" + overTime + "</td><td id='ProductSubTotal'>" + SubTotal + "</td></tr>";
        $("#tblProduct tbody").append(markup);
        $tableItemCounter++;
        $addedEmployeeIDs.push(employeeID);
        clearFields();
        calcTotal();
    }

    else if (employeeID == "" || employeeID == "undefined") { swal("Error", "Please Select Employee!", "error"); }
    else if (Desc == "null") { swal("Error", "Please Enter Description!", "error"); }

    //  else if (salary == "" || salary == "undefined") { swal("Error", "Please enter Salary!", "error"); }
});
// Find and remove selected table rows   
function remove(input) {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {
            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            var employeeID = row.find('input[id*="employeeID"]').val(); // find hidden id 
            var index = $.inArray(employeeID, $addedEmployeeIDs);
            if (index >= 0) { $addedEmployeeIDs.splice(index, 1); }
        }
    });
    calcTotal();
}

function clearFields() {
    //  $("#Salary").val("");        
    //   $("#advance").val("");
    //  $("#deduction").val("");
    $("#overTime").val("");
    $("#SubTotal").val("");
    $("#Desc").val("");
    // $("#ddlEmployee").focus();
}
// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
//Counter to count number of entries in item table

//  Get all added Products from table
function getTableData() {
    $('#tblProduct tbody tr').each(function (i, n) {

        var $row = $(n);
        var employeeId = $row.find('input[id*="employeeID"]').val();
        var Employee = $row.find("td").eq(1).text();
        //  var salary = $row.find("#Salary").text();        
        // var advance = $row.find("#advance").text();        
        var overTime = $row.find("#overTime").text();
        var SubTotal = $row.find("#ProductSubTotal").text();
        //console.log($row.index);
        //console.log("employeeId" + employeeId);
        //console.log("Employee" + Employee);
        //console.log("salary" + salary);
        //console.log("advance" + advance);
        //console.log("overTime" + overTime);
        //console.log("SubTotal" + SubTotal);
    });
}
// Json date to standar date converter 
function getDate(date) {
    //console.log(date);
    var num = (date).match(/\d+/g);
    //console.log(num);
    var date = new Date(parseFloat(num));
    var month = parseInt(date.getMonth()) + 1;
    //console.log("date" +date);
    var dateString = date.getDate() + "/" + month + "/" + date.getFullYear();
    return dateString;
}
// clear employee info labels
function clearLabels() {
    $('#IDNOLabel').text(''); $('#IDNO').text('');
    $('#IDExpiryLabel').text(''); $('#IDExpiry').text('');
    $('#PassportNOLabel').text(''); $('#PassportNO').text('');
    $('#PassportExpiryLabel').text(''); $('#PassportExpiry').text('');
}
// Get Employee's Details 
function getDetail(employeeId) {
    clearLabels();
    var accID = $('#hdnAccountID').val();
    // Get Employee Data 
    var json = { "employeeID": employeeId };
    //ajaxCall("GET", json, "application/json; charset=utf-8", "/Employee/getEmployeeDetail", "json", onSuccess, onFailure);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Employee/getEmployeeDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            if (data[0].qry.IDno) { $('#IDNOLabel').text("IDNo.: "); $('#IDNO').text(data[0].qry.IDno); }
            if (data[0].qry.IDExpiry) { $('#IDExpiryLabel').text("ID Expiry: "); $('#IDExpiry').text(getDate(data[0].qry.IDExpiry)); }
            if (data[0].qry.PassportID) { $('#PassportNOLabel').text("Passport No.: "); $('#PassportNO').text(data[0].qry.PassportID); }
            if (data[0].qry.PassportExpiry) { $('#PassportExpiryLabel').text("Passport Expiry: "); $('#PassportExpiry').text(getDate(data[0].qry.PassportExpiry)); }
            //if (data.Location) { $('#LocationTitle').text("Location: "); $('#Location').text(data.Location); }
            if (data[0].qry.Salary) { $('#Salary').val(data[0].qry.Salary); $('#totalOverTime').val(data[0].qry.Salary); $('#BasicSalary').val(data[0].qry.Salary);  }
            if (data[0].adv) { $('#advance').val(data[0].adv); }
        },
        error: function (err) { console.log(err); }
    });
}