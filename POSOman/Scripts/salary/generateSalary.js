﻿var customerID = 0;
var accID = 0;
var isPONOExist = false;
var isQuote = false;
var SOID = 0;
var isLimitExceed = false;
$("#Bank").prop("disabled", true);
$('#btnSubmit').click(function () {
    accID = $('#hdnAccountID').val();    
    var isValid = true;    
    var VoucherDate = $('#VoucherDate').val();   
    var rowCount = $('#tblProduct tbody tr').length;
    var PaymentTypeID = $('#PaymentType option:selected').val();
    var BranchID = $('#BranchID option:selected').val();    
    
   if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Employee", "Please Select Employee!", "error");
    }
    else if (VoucherDate == "") {
        isValid = false;
        swal("Date", "Please Enter Date!", "error");
        //console.log(PurchaseDate + "date");
    }
    else if (rowCount ==0 || rowCount < 0) {
        isValid = false;
        swal("Employee", "Please Enter Employees!", "error");
        //console.log(PurchaseDate + "date");
    }    
    else if ((PaymentTypeID == "" || PaymentTypeID == undefined)) {
        isValid = false;
        swal("Payment Type", "Please Select Payment Type!", "error");
    }    
    
    else if (isValid == true) {
    $('#btnSubmit').prop('disabled', true);
        uiBlock();
        insert();
    }
});
    // Payment Type Change 
$("#PaymentType").on("change", function () {
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //Cash
    if (PaymentTypeID == 1) {
        //$("#bankName").prop("disabled", true);
        $("#Bank").prop("disabled", true);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);
    }
        // Bank
    else if (PaymentTypeID == 2) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);

    }
        // Cheque
    else if (PaymentTypeID == 3) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", false);
        $("#chqDate").prop("disabled", false);
    }
});
// Add data to array and send it to controller for order creation
function insert() {    
    var Details = [];
    var total = 0;
    var BranchID = $('#BranchID option:selected').val();
    var PaymentTypeID = $('#PaymentType option:selected').val();
    var totalSalary = $('#totalSalary').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var totalOverTime = $('#totalOverTime').val();
    
    var totalDeduction = $('#totalDeduction').val();
    var VoucherDate = $('#VoucherDate').val();
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var employeeId = $row.find('input[id*="employeeID"]').val();        
        var Employee = $row.find("td").eq(1).text();
        var salary = $row.find("#Salary").text();
        //var advance = $row.find("#advance").text();
        var deduction = $row.find("#deduction").text();
        var overTime = $row.find("#overTime").text();
        var SubTotal = $row.find("#ProductSubTotal").text();       
        //Salary Entry 
        Details.push({
            EmployeeID: employeeId,
            BranchID: BranchID,
            Dr: salary,
            Cr: 0,
            JEntryTypeID: 5,
            Description: 'Monthly Salary',
            Date: VoucherDate
        });
        // if Advance deduction is made 
        if (parseFloat(deduction) > 0) {
            Details.push({
                EmployeeID: employeeId,
                BranchID: BranchID,
                Dr: 0,
                Cr: deduction,
                JEntryTypeID: 11,
                Description: 'Advance Deduction',
                Date: VoucherDate
            });
        }
        // if Overtime is paid 
        if (parseFloat(overTime) > 0) {
            Details.push({
                EmployeeID: employeeId,
                BranchID: BranchID,
                Dr: overTime,
                Cr: 0,
                JEntryTypeID: 12,
                Description: 'Overtime',
                Date: VoucherDate
            });
        }
    });
    
    if (Details.length > 0) {
                
        var data = {            
            'BranchID': $('#BranchID option:selected').val(),            
            'ChequeDate': $("#chqDate").val(),            
            'VoucherDate': VoucherDate,
            'ChequeNo': $("#chqNumber").val(),
            'BankName': $("#bankName").val(),
            'PaymentAccountID': PaymentTypeID,
            'TotalSalary': totalSalary,
            'TotalOverTime': totalOverTime,
            'TotalDeduction': totalDeduction,
        };
    //    console.log(data + "  data");
      //  console.log(employeeLog + "  employeeLog");
        var json = JSON.stringify({ 'model': data, 'modelemployeeLog': Details, 'bankAccId': bankAccountId });
        //console.log(json);
        ajaxCall("POST", json, "application/json; charset=utf-8", "/Salary/GenerateSalary", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result > 0) {
                uiUnBlock();                
                window.open('/Salary/SalaryReceipt?id=' + parseInt(Result) + '');
                
                window.location.href = '/Salary';                
            }
            else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                window.location.reload();
                console.log("OK");
            }
            else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }
    else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);        
        swal("Error", "Please Check your entries!", "error");
    }
}