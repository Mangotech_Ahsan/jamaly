﻿getSOID();
//  Get New SOID 
function getSOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/getNewSOID',
        async: true,
        success: function (data) {
            {
                $("#hdnSOID").val(data);
                SOID = data;
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

//$("#btnSave").click(function () {
//    try {
//        $('#btnSave').prop('disabled', true);
//        insert();
//    }
//    catch (err) {
//        $('#btnSave').prop('disabled', false);
//        console.log(err);
//    }
//})

$('#btnSubmit').click(function () {
    let ReadyQty = 1;//+$("#ReadyQty").val() || 0;
    let ExpDate = $("#ExpectedDeliveryDate").val() || null;
    let SaleDate = $("#SaleDate").val() || null;
    let isValid = true;

    console.log(ReadyQty)
    console.log(ExpDate)
    console.log(SaleDate)
    console.log(isValid)

    if (ExpDate == null) {
        swal("", "Expected Delivery Date Required", "error");
        isValid = false;
    }
    else if (SaleDate == null) {
        swal("", "Sales Date Required", "error");
        isValid = false;
    }
    else if (ReadyQty == null || ReadyQty <= 0) {
        swal("", "Qty Required", "error");
        isValid = false;
    }

    if (isValid == true) {
        $('#btnSubmit').prop('disabled', true);
        $('#btnHold').prop('disabled', true);
        uiBlock();
        insert();
    }




});
var stockLog = [];
function GetFinalData() {

    let OrderID = +$("#InvOrderId").val() || 0;
    let DepartmentID = +$("#hdnDeptID").val() || 0;
    let ProductID = +$("#hdnProdID").val() || 0;
    //let machineTypeID = +$("#MachineTypeID").val() || 0;
    //let machineID = +$("#AttachedMachineID").val() || 0;
    let ReadyQty = 1;//+$("#ReadyQty").val() || 0;

    //let SlittingOMachineID = +$("#SlittingOMachineID").val() || null;
    //let DieCuttingOMachineID = +$("#DieCuttingOMachineID").val() || null;
    //let UVOMachineID = +$("#UVOMachineID").val() || null;
    //let LaminationOMachineID = +$("#LaminationOMachineID").val() || null;
    //let FoilOMachineID = +$("#FoilOMachineID").val() || null;
    //let EmbosingOMachineID = +$("#EmbosingOMachineID").val() || null;
    //$('#tblProduct tbody tr').each(function (i, n) {
    //    var $row = $(n);
    //    var pId = parseInt($row.find('#ProductID').text());
    //    var partNumber = $row.find("#PartNo").text();
    //    var UnitCode = $row.find("#UnitCode").text();
    //    var UnitPrice = parseFloat($row.find('#UnitPrice').text()) || 0;
    //    var Qty = parseFloat($row.find('#Qty').text());
    //    var SubTotal = parseFloat($row.find("#SubTotal").text()) || 0;

    //});
    if (ProductID > 0 && DepartmentID > 0 && OrderID > 0 && ReadyQty > 0 && machineTypeID > 0 && machineID>0) {
        var data = {

            'OrderID': OrderID,
            'DepartmentID': DepartmentID,
            'ProductID': ProductID,
            'ExpectedDeliveryDate': $("#ExpectedDeliveryDate").val(),
            'SalesDate': $("#SaleDate").val(),
            'ReadyQty': ReadyQty,
            //'AttachedMachineID': machineID,
            //'MachineTypeID': machineTypeID
           //'SlittingOMachineID': SlittingOMachineID,
           // 'DieCuttingOMachineID': DieCuttingOMachineID,
           // 'UVOMachineID': UVOMachineID,
           // 'LaminationOMachineID': LaminationOMachineID,
           // 'FoilOMachineID': FoilOMachineID,
           // 'EmbosingOMachineID': EmbosingOMachineID

        };

        return data;
    }



    return null;
}


function insert() {

    var data = GetFinalData();
    if (data != null) {
        var json = JSON.stringify({
            'model': data,

        });

        ajaxCall("POST", json, "application/json; charset=utf-8", "/Sales/UpdateStickerOrderProduct", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result > 0) {
                uiUnBlock();
                // window.open('/SalesOrder/printOrder?id=' + parseInt(Result) + '');
                //window.open('/SalesOrder/Details?id=' + parseInt(Result) + '');
                //console.log("OK");

                swal("", "Updated", "success");

                setTimeout(function () {
                    window.location.href = '/Sales/DepartmentWiseOrdersIndex';
                }, 1500)

            } else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);

                alert("Some error Ocurred! Please Check Your Entries");
            }
        }

        function onFailure(error) {
            if (error.statusText == "OK") {
                console.log("OK");
            } else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }
    else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("Check Entries", "Some error Ocurred! Please Check your Entries!", "error");
    }
}

