﻿
var globalData = null;

var dieCuttingCostGlobal = 0;
var slittingCostGlobal = 0;
var uvCostGlobal = 0;

var uvMattVarnishG = 0;
var uvGlossVarnishG = 0;
var aqMatVarnishG = 0;
var aqGlossVarnishG = 0;
var EmbCosthG = 0;
var labourCostG = 0;
var tr2PlateCostG = 0;

var OrderId = $('#InvOrderId').val();
var deptID = $('#hdnDeptID').val();
//var index = null;
$(document).ready(() => {
    getAdditionalCosts();
})

GetEditCustomers(OrderId);
function GetEditCustomers(OrderId) {
    let currentUserRole = $("#currentUserRole").val() || null;
    let prodID = +$("#hdnProdID").val() || 0;
    if (OrderId == "") { OrderId = -1; }
    var json = { "OrderId": OrderId, "currentUserRole": currentUserRole, "DeptID": deptID, "ProductID": prodID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Sales/GetSaleOrderDetails',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            var num = (data[0].salesDate).match(/\d+/g);
            var date = new Date(parseFloat(num));


            //if (data[0].chequeDate != null) {
            //    var num1 = (data[0].chequeDate).match(/\d+/g);
            //    var date1 = new Date(parseFloat(num));
            //    document.getElementById('chqDate').valueAsDate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate(), date.getUTCHours() + 5, date.getMinutes());
            //}

            console.log(data[0].ProductsList)
            $('#PrintingMachineID').val(data[0].ProductsList.PrintingMachineID).trigger('change.select2');
            $('#CylinderID').val(data[0].ProductsList.CylinderID).trigger('change.select2');
            $('#FinishingTypeID').val(data[0].ProductsList.FinishingTypeID).trigger('change.select2');
            $('#BlockTypeID').val(data[0].ProductsList.BlockTypeID).trigger('change.select2');
            $('#PrintingRatePerRoll').val(data[0].ProductsList.PrintingRatePerRoll);
            $('#QtyPerUnit').val(data[0].ProductsList.Qty);
            $('#Length').val(data[0].ProductsList.Length);
            $('#Width').val(data[0].ProductsList.Width);
            $('#CylinderLength').val(data[0].ProductsList.CylinderLength);
            $('#AroundUps').val(data[0].ProductsList.AroundUps);
            $('#NoOfPrintingColors').val(data[0].ProductsList.NoOfPrintingColor);
            $('#RollID').val(data[0].ProductsList.RollID).trigger('change.select2');
            $('#TotalPcsInRoll').val(data[0].ProductsList.TotalPcInRoll);
            $('#WastagePcsInRoll').val(data[0].ProductsList.WastagePcInRoll);
            $('#TotalRequiredRolls').val(data[0].ProductsList.TotalReqRoll);

            $('#StickerMachineID').val(data[0].ProductsList.StickerMachineID);
            $('#LSize').val(data[0].ProductsList.LSize);
            $('#WSize').val(data[0].ProductsList.WSize);
            $('#RepeatLength').val(data[0].ProductsList.RepeatLength);
            $('#StickerCylinderID').val(data[0].ProductsList.StickerCylinderID);
            $('#Across').val(data[0].ProductsList.Across);
            $('#ReelSize').val(data[0].ProductsList.ReelSize);
            $('#Upping').val(data[0].ProductsList.Upping);
            $('#RunningMeter').val(data[0].ProductsList.RunningMeter);
            $('#RequiredM2').val(data[0].ProductsList.RequiredM2);
            $('#StickerQualityID').val(data[0].ProductsList.StickerQualityID);
            $('#KGRequired').val(data[0].ProductsList.KGRequired);
            $('#MaterialCostPerMeter').val(data[0].ProductsList.MaterialCostPerMeter);
            $('#Wastage').val(data[0].ProductsList.Wastage);
            $('#TotalM2ReqIncludingWastage').val(data[0].ProductsList.TotalM2ReqIncludingWastage);
            $('#TotalQtyIncludingWastage').val(data[0].ProductsList.TotalQtyIncludingWastage);
            $('#RunningMeterIncludingWastage').val(data[0].ProductsList.RunningMeterIncludingWastage);
            $('#StickerRibbonID_T').val(data[0].ProductsList.StickerRibbonID_T);
            $('#StickerRibbonID_F').val(data[0].ProductsList.StickerRibbonID_F);
            $('#StickerRibbonID_S').val(data[0].ProductsList.StickerRibbonID_S);
            $('#StickerRibbonID_RFID').val(data[0].ProductsList.StickerRibbonID_RFID);
            $('#StickerDieCutiingID').val(data[0].ProductsList.StickerDieCutiingID);
            $('#BlockTypeID').val(data[0].ProductsList.BlockTypeID);

            //GetDropdown1('SlittingOMachineID', data[0].slittingMachineDataDD, true);
            //GetDropdown1('DieCuttingOMachineID', data[0].renderDieCuttingMDD, true);
            //GetDropdown1('UVOMachineID', data[0].renderUVMDD, true);
            //GetDropdown1('LaminationOMachineID', data[0].renderLaminationMDD, true);
            //GetDropdown1('FoilOMachineID', data[0].renderFoilMDD, true);
            //GetDropdown1('EmbosingOMachineID', data[0].renderEmbosingMDD, true);


            $('#SlittingOMachineID').val(data[0].ProductsList.SlittingOMachineID).trigger('change');
            $('#DieCuttingOMachineID').val(data[0].ProductsList.DieCuttingOMachineID).trigger('change');
            $('#UVOMachineID').val(data[0].ProductsList.UVOMachineID).trigger('change');
            $('#LaminationOMachineID').val(data[0].ProductsList.LaminationOMachineID).trigger('change');
            $('#FoilOMachineID').val(data[0].ProductsList.FoilOMachineID).trigger('change');
            $('#EmbosingOMachineID').val(data[0].ProductsList.EmbosingOMachineID).trigger('change');

            $('#ReadyQty').val(data[0].ProductsList.ReadyQty);

            $('#AccountID').val(data[0].Qry.Value).trigger('change.select2');
            $('#SalePersonAccID').val(data[0].SalePersonAccID).trigger('change.select2');

            $('#BranchID').val(data[0].Qry1.Value).trigger('change.select2');
            $('#customerCrLimit').val(data[0].Qry2);
            // console.log(convertJsonDate(data[0].ExpectedDeliveryDate));
            //$('#ExpectedDeliveryDate').val(data[0].ExpectedDeliveryDate);
            var customerID = $('#AccountID').val();
            $('#hdnAccountID').val(customerID);
            //getCustomerDetail(customerID);
            $('#PaymentStatus').val(data[0].PayStatus).trigger('change.select2');
            $("#Bank").val(data[0].Bank.Value).trigger('change.select2');

            // console.log(data[0].PayStatus);
            if (data[0].PayStatus == 3) {
                $("#PaymentType").prop('disabled', true);
            }
            $('#chqNumber').val(data[0].Cheque);
            $('#amountPaid').val(data[0].AmountPaid);
            $('#vatAmount').val(data[0].Tax);
            $('#creditDays').val(data[0].CreditDays);
            $('#finalAmountWithVAT').val(data[0].FinalAmount);
            $('#totalAmount').val(data[0].TotalAmount);
            $('#subAmount').val(data[0].SubAmount);
            $('#discInput').val(data[0].Discount);

            $('#AccountID').prop('disabled', true);
            $('#SalePersonAccID').prop('disabled', true);
            if (data[0].userRole == "SuperAdmin") {

                $('#SaleDate').prop('disabled', false);
            }
            if (data[0].userRole != "SuperAdmin") {

                $('#SaleDate').prop('disabled', true);
            }
            document.getElementById('SaleDate').valueAsDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getUTCHours() + 5, date.getMinutes());

            if (data[0].ProductsList.ExpectedDeliveryDate != null) {
                var expNum = (data[0].ProductsList.ExpectedDeliveryDate).match(/\d+/g);
                var ExpectedDate = new Date(parseFloat(expNum));
                document.getElementById('ExpectedDeliveryDate').valueAsDate = new Date(ExpectedDate.getFullYear(), ExpectedDate.getMonth(), ExpectedDate.getDate(), ExpectedDate.getUTCHours() + 5, ExpectedDate.getMinutes());

            }
            //console.log(ToJSDate(data[0].salesDate));

            if (data[0].Paytype == "" || data[0].Paytype == null) {
            }
            else {
                $('#PaymentType').val(data[0].Paytype.Value).trigger('change.select2');
                if (data[0].Paytype.Value == 1) {
                    $("#Bank").prop("disabled", true);
                    $("#chqNumber").prop("disabled", true);
                    $("#chqDate").prop("disabled", true);
                }
                else if (data[0].Paytype.Value == 2) {
                    $("#Bank").prop("disabled", false);
                    $("#chqNumber").prop("disabled", true);
                    $("#chqDate").prop("disabled", true);
                }
                else {
                    $("#Bank").prop("disabled", false);
                    $("#chqNumber").prop("disabled", false);
                    $("#chqDate").prop("disabled", false);
                }
            }


        },
        error: function (err) { console.log(err); }
    });
}

enableDisableStickerMachineScenario = (id) => {
    console.log(id)
    if (id === 1) {
        $("#Length").prop('disabled', false);
        $('#StickerCylinderID').val(null).trigger('change');
        $("#StickerCylinderID").prop('disabled', true);
        $("#CylinderTeeth").val(0);
    }
    else if (id === 2) {
        $("#Length").prop('disabled', true);
        $("#Length").val(0);
        $("#StickerCylinderID").prop('disabled', false);
    }
}

$("#StickerCylinderID").change(() => {
    let ID = +$("#StickerCylinderID").val() || 0;
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/GetStickerCylinderDetails?id=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data)
                    $("#CylinderTeeth").val(+data.CylinderTeeth);

                }
            },
            error: function (err) { console.log(err); }
        });
    }
});

$("#StickerQualityID").change(() => {
    let ID = +$("#StickerQualityID").val() || 0;
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/GetStickerQualityDetails?id=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data)
                    $("#MaterialCostPerMeter").val(+data.Rate);
                    $("#hdnGSM").val(+data.GSM);
                    console.log(data.GSM);
                    getKGRequired();
                    FinalPriceCalculation();

                }
            },
            error: function (err) { console.log(err); }
        });
    }
});

$("#StickerDieCutiingID").change(() => {
    let ID = +$("#StickerDieCutiingID").val() || 0;
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/GetStickerDieCuttingDetails?id=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data)
                    $("#DieCuttingCost").val(+data.Rate);
                    FinalPriceCalculation();

                }
            },
            error: function (err) { console.log(err); }
        });
    }
    else {
        $("#DieCuttingCost").val(0);
        FinalPriceCalculation();
    }
});

$("#StickerMachineID").change(() => {
    let ID = +$("#StickerMachineID").val() || 0;
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/GetStickerMachineDetails?id=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data)
                    enableDisableStickerMachineScenario(+data.StickerMachineTypeID || null);
                    $("#hdnTextColorRate").val(+data.TextColorRate);
                    $("#hdnGroundColorRate").val(+data.GroundColorRate);
                    labourCostG = +data.LaborCost;

                }
            },
            error: function (err) { console.log(err); }
        });
    }
});

$("#BlockTypeID").change(() => {
    let ID = +$("#BlockTypeID").val() || 0;
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/FetchBlockData?ID=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data)
                    $("#BlockCost").val(+data.RatesPerSqInch);
                    getPrintingBlockCost();

                }
            },
            error: function (err) { console.log(err); }
        });
    }
});

$("#StickerDieCutiingID").change(() => {
    let ID = +$("#StickerDieCutiingID").val() || 0;
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/GetStickerDieCuttingDetails?id=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data)
                    $("#DieCuttingCost").val(+data.Rate)

                }
            },
            error: function (err) { console.log(err); }
        });
    }
});

getRibbonDetails = (ID) => {
    console.log(ID)

    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/GetStickerRibbonDetails?id=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data)
                    if (data.RibbonTypeID === 1) {//Foil
                        $("#StickerRibbonFCost").val(+data.Rate);
                    }
                    else if (data.RibbonTypeID === 2) {//Thermal
                        $("#StickerRibbonTCost").val(+data.Rate);
                    }
                    else if (data.RibbonTypeID === 3) {//Satin
                        $("#StickerRibbonSCost").val(+data.Rate);
                    }
                    else if (data.RibbonTypeID === 4) {//RFID
                        $("#StickerRibbonRFIDCost").val(+data.Rate);
                    }

                    FinalPriceCalculation();
                }
            },
            error: function (err) { console.log(err); }
        });
    }

}

getServo_CYLTypeRepeatLength = () => {
    let around = +$("#AroundUps").val() || 0;
    let length = +$("#Length").val() || 0;
    let cyl_teeth = +$("#CylinderTeeth").val() || 0;
    let machine_type = +$("#StickerMachineTypeID").val() || 0;

    let total_length = 0;
    if (machine_type === 1) {
        total_length = around * length;
        $("#RepeatLength").val(+total_length);
    }
    else if (machine_type === 2) {
        total_length = cyl_teeth * 3.175;
        $("#RepeatLength").val(+total_length);
        getCYLTypeLength();
    }
    getPrintingBlockCost();


}

getCYLTypeLength = () => {
    let repeat_length = +$("#RepeatLength").val() || 0;
    let around = +$("#AroundUps").val() || 1;

    let length = repeat_length / around;
    $("#Length").val(+length.toFixed(2));
}


getWidth = () => {
    let reel_size = +$("#ReelSize").val() || 0;
    let across = +$("#Across").val() || 1;

    let width = reel_size / across;
    $("#Width").val(+width.toFixed(2));
}
getNoOfUps = () => {
    let around = +$("#AroundUps").val() || 0;
    let across = +$("#Across").val() || 1;

    let ups = around * across;
    $("#Upping").val(+ups.toFixed(2));
}

getRunningMeter = () => {
    let qty = +$("#QtyPerUnit").val() || 0;
    let ups = +$("#Upping").val() || 1;
    let repeat_length = +$("#RepeatLength").val() || 0;
    console.log("in run meter")
    let run_meter = qty / ups * (repeat_length / 1000);
    console.log("qty: " + qty)
    console.log("ups: " + ups)
    console.log("RepeatLength: " + repeat_length)
    console.log("run_meter: " + run_meter)

    $("#RunningMeter").val(+run_meter.toFixed(2));
}

getM2Required = () => {

    let reel_size = +$("#ReelSize").val() || 0;
    let run_meter = +$("#RunningMeter").val() || 0;
    console.log("in m2")
    let m2_req = run_meter * reel_size / 1000;
    console.log("ReelSize: " + reel_size)
    console.log("RunningMeter: " + run_meter)
    console.log("m2: " + m2_req)
    $("#RequiredM2").val(+m2_req.toFixed(2));
    getKGRequired();
}

getKGRequired = () => {

    let m2_req = +$("#RequiredM2").val() || 0;
    let gsm = +$("#hdnGSM").val() || 0;

    let kg_req = m2_req * (gsm / 1000);
    console.log("gsm")
    console.log(gsm)
    $("#KGRequired").val(+kg_req.toFixed(2));
}

getM2PrintingCost = () => {

    let text_color = +$("#TextColor").val() || 0;
    let ground_color = +$("#GroundColor").val() || 0;
    let hdn_ground_color_rate = +$("#hdnGroundColorRate").val() || 0;
    let hdn_text_color_rate = +$("#hdnTextColorRate").val() || 0;
    let print_cost = (hdn_text_color_rate * text_color) + (hdn_ground_color_rate * ground_color);
    $("#PrintingCost").val(+print_cost.toFixed(2));

}

getPrintingBlockCost = () => {

    let repeat_length = +$("#RepeatLength").val() || 0;
    let reel_size = +$("#ReelSize").val() || 0;
    let block_rate = +$("#BlockCost").val() || 0;
    let text_color = +$("#TextColor").val() || 0;
    let ground_color = +$("#GroundColor").val() || 0;

    let print_cost = +(repeat_length * reel_size) / 645.16 * block_rate * (text_color + ground_color) || 0;
    $("#StickerPrintingBlockCost").val(+print_cost.toFixed(2));

    FinalPriceCalculation();

}

getAdditionalCosts = () => {

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/MachinePlate/GetAdditionalCostingDetails',

        async: true,
        success: function (data) {
            {
                console.log(data)
                globalData = data;
                uvGlossVarnishG = +data.UVGlossVarnish.toFixed(2);
                uvMatVarnishG = +data.UVMattVarnish.toFixed(2);
                aqGlossVarnishG = +data.AQGlossVarnish.toFixed(2);
                aqMattVarnishG = +data.AQMattVarnish.toFixed(2);
                EmbCosthG = +data.EmbosingCost.toFixed(2);
                tr2PlateCostG = +data.tr2Cost.toFixed(2);

                $("#GlossLaminationCost").val(parseFloat(+data.GlossLaminationCost).toFixed(2));
                $("#MatLaminationCost").val(parseFloat(+data.MatLaminationCost).toFixed(2));
                $("#HotLaminationCost").val(parseFloat(+data.HotLaminationCost).toFixed(2));
                //$("#EmbosingCost").val(parseFloat(+data.EmbosingCost).toFixed(2));

                slittingCostGlobal = data.SlittingCost;


            }
        },
        error: function (err) { console.log(err); }
    });

}

$("#ReelSize").on('input', () => {
    getWidth();
    getM2Required();
    getPrintingBlockCost();
    getRunningMeter();
})
$("#QtyPerUnit").on('input', () => {
    getRunningMeter();
    getM2Required();
})

$("#TextColor").on('input', () => {
    getM2PrintingCost();
    getPrintingBlockCost();
    FinalPriceCalculation();
})

$("#GroundColor").on('input', () => {
    getM2PrintingCost();
    getPrintingBlockCost();
    FinalPriceCalculation();
})

$("#Across").on('input', () => {
    getWidth();
    getNoOfUps();
    getServo_CYLTypeRepeatLength();
    getRunningMeter();

})

$("#AroundUps").on('input', () => {
    getServo_CYLTypeRepeatLength();
    getNoOfUps();
    getRunningMeter();
})


$("#Length").on('input', () => {
    getServo_CYLTypeRepeatLength();
})

resetTCost = () => {
    $('#StickerRibbonID_T').val(null).trigger('change');
    $("#StickerRibbonTCost").val(0);
}

resetFCost = () => {
    $('#StickerRibbonID_F').val(null).trigger('change');
    $("#StickerRibbonFCost").val(0);
}

resetSCost = () => {
    $('#StickerRibbonID_S').val(null).trigger('change');
    $("#StickerRibbonSCost").val(0);
}

resetRFIDCost = () => {
    $('#StickerRibbonID_RFID').val(null).trigger('change');
    $("#StickerRibbonRFIDCost").val(0);
}
//  getCode();
function getCardQualityID(ID) {
    console.log(ID)
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/FetchCardQualityDetails?ID=' + ID,

            async: true,
            success: function (data) {
                {
                    // console.log(data)
                    $("#RatePerKG").val(data.Rate);
                    $("#hdnCardTypeID").val(data.CardTypeID);

                    CalculateSheetCost();
                }
            },
            error: function (err) { console.log(err); }
        });
    }

}


$("#Quantity").focusout( function () {

    calculateUpping();
    setTimeout(() => { FinalPriceCalculation() }, 500);
});

$("#Upping").on('input', function () {

    calculateUpping();
});

calculateUpping = () => {
    var Quantity = +$("#Quantity").val() || 0;
    var Upping = +$("#Upping").val() || 0;
    var Wastage = +$("#Wastage").val() || 0;
    if (Quantity > 0) {
        $("#Wastage").val(0);
        if (Upping > 0) {
            let Calculated = parseFloat(Quantity / Upping).toFixed(2);
            $("#NumberOfSheets").val(Calculated);
            //getNoOfSheetsValue();
            let total = Calculated + Wastage;
            getImpression(+total);
            $("#TotalNumberOfSheets").val(Calculated + Wastage);
        }
        else {
            $("#NumberOfSheets").val(0);
            $("#TotalNumberOfSheets").val(Wastage);
        }
        CalculateSheetCost();
        getRawSizeSheets();
    }
    else {
        toastr.warning("Please Insert Quantity First!")
        $("#Upping").val('');
    }
}

$("#Wastage").on('input', function () {

    //var NumberOfSheets = +$("#NumberOfSheets").val() || 0;

    //var Wastage = +$("#Wastage").val() || 0;
    //if (NumberOfSheets > 0) {


    //    let cal = NumberOfSheets + Wastage;
    //    $("#TotalNumberOfSheets").val(cal);
    //    getImpression(+cal);

    //}
    //else {
    //    toastr.warning("Please Insert Relative Data First!")
    //    $("#Wastage").val('');
    //    $("#TotalNumberOfSheets").val(0);

    //}
    //CalculateSheetCost();
    //getRawSizeSheets();

    FinalPriceCalculation();
});

$("#TotalM2Cost").change(() => {



    //var NumberOfSheets = +$("#NumberOfSheets").val() || 0;

    //var Wastage = +$("#Wastage").val() || 0;
    //if (NumberOfSheets > 0) {


    //    let cal = NumberOfSheets + Wastage;
    //    $("#TotalNumberOfSheets").val(cal);
    //    getImpression(+cal);

    //}
    //else {
    //    toastr.warning("Please Insert Relative Data First!")
    //    $("#Wastage").val('');
    //    $("#TotalNumberOfSheets").val(0);

    //}
    //CalculateSheetCost();
    //getRawSizeSheets();

    FinalPriceCalculation();

});

$("#Length").on('input', function () {

    var Length = +$("#Length").val() || 0;


    if (Length > 0) {
        CalculateSheetCost();
    }

});

$("#Width").on('input', function () {

    var Width = +$("#Width").val() || 0;


    if (Width > 0) {
        CalculateSheetCost();
    }

});

$("#CardGSM").on('input', function () {

    var CardGSM = +$("#CardGSM").val() || 0;


    if (CardGSM > 0) {
        CalculateSheetCost();
    }

});

getRawSizeSheets = () => {
    let totalSheet = +$("#TotalNumberOfSheets").val() || 0;
    let cutLength = +$("#CutLength").val() || 1;
    let cutWidth = +$("#CutWidth").val() || 0;

    let rawSize = totalSheet / (cutLength + cutWidth);
    $("#RawSizeSheets").val(+rawSize.toFixed(2));
}

getImpression = (Sheets) => {
    if (+Sheets > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/GetImpression?Sheets=' + (+Sheets),

            async: true,
            success: function (data) {
                {
                    console.log(data);
                    if (+data > 0) {
                        $("#Impression").val(+data);
                    }
                    else {
                        toastr.warning(data.toString())
                        $("#Impression").val(0);
                    }


                }
            },
            error: function (err) { console.log(err); }
        });
    }
}



function CalculateSheetCost() {
    console.log("in CalculateSheetCost")

    let hdnCardTypeID = +$("#hdnCardTypeID").val() || 0;
    let Length = +$("#Length").val() || 0;
    let Width = +$("#Width").val() || 0;
    let CardGSM = +$("#CardGSM").val() || 0;
    var RatePerKG = +$("#RatePerKG").val() || 0;
    //var NumberOfSheets = +$("#NumberOfSheets").val() || 0;
    var TotalNumberOfSheets = +$("#TotalNumberOfSheets").val() || 0;

    if (hdnCardTypeID > 0) {
        if (hdnCardTypeID === 1) {//Card
            let Calculate = ((Length * Width * CardGSM * RatePerKG) / 15500) / 100;
            //$("#PerSheetCost").val(parseFloat(Calculate / NumberOfSheets).toFixed(12));
            //$("#TotalSheetCost").val(parseFloat(Calculate).toFixed(12));
            $("#PerSheetCost").val(parseFloat(Calculate).toFixed(12));
            $("#TotalSheetCost").val(parseFloat(Calculate * TotalNumberOfSheets).toFixed(12));
        }
        else if (hdnCardTypeID === 2) {//Paper
            let Calculate = ((Length * Width * CardGSM * RatePerKG) / 16600) / 500;
            //$("#PerSheetCost").val(parseFloat(Calculate / NumberOfSheets).toFixed(12));
            //$("#TotalSheetCost").val(parseFloat(Calculate).toFixed(12));
            $("#PerSheetCost").val(parseFloat(Calculate).toFixed(12));
            $("#TotalSheetCost").val(parseFloat(Calculate * TotalNumberOfSheets).toFixed(12));
        }
    }

}


calculateLength = () => {
    let CardLength = +$("#CardLength").val() || 0;
    let CutLength = +$("#CutLength").val() || 1;

    let Length = CardLength / CutLength;
    $("#Length").val(+Length.toFixed(2));

}


calculateWidth = () => {
    let CardWidth = +$("#CardWidth").val() || 0;
    let CutWidth = +$("#CutWidth").val() || 1;

    let Width = CardWidth / CutWidth;
    $("#Width").val(+Width.toFixed(2));

}

$("#CardLength").change(() => {
    calculateLength();
})

$("#CutLength").change(() => {
    calculateLength();
    getRawSizeSheets();
})

$("#CardWidth").change(() => {
    calculateWidth();
})
$("#CutWidth").change(() => {
    calculateWidth();
    getRawSizeSheets();
})

function getMachineDetails(ID) {
    console.log(ID)
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/FetchMachineDetails?ID=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data);
                    //globalData = data;
                    dieCuttingCostGlobal = data.DieCuttingCost;
                    slittingCostGlobal = data.SlittingCost;
                    uvCostGlobal = data.UVCost;
                    FetchLaminationCosts();
                    setTimeout(function () {
                        CalculateMachineSelectedCosts(data);
                    }, 1000);


                }
            },
            error: function (err) { console.log(err); }
        });
    }

}

function FetchLaminationCosts() {


    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/MachinePlate/FetchLaminationCosts',

        async: true,
        success: function (data) {
            {
                console.log(data);
                $("#GlossLaminationCost").val(parseFloat(+data.GlossLaminationCost).toFixed(2));
                $("#MatLaminationCost").val(parseFloat(+data.MatLaminationCost).toFixed(2));
                $("#HotLaminationCost").val(parseFloat(+data.HotLaminationCost).toFixed(2));

                if ($("#includeUVCost").is(":checked")) {
                    $("#UVCost").val(parseFloat(+uvCostGlobal).toFixed(2));
                }
                else {
                    $("#UVCost").val(parseFloat(0).toFixed(2));
                }
                if ($("#includeSlittingCost").is(":checked")) {
                    $("#SlittingCost").val(parseFloat(+slittingCostGlobal).toFixed(2));
                }
                else {
                    $("#SlittingCost").val(parseFloat(0).toFixed(2));
                }




            }
        },
        error: function (err) { console.log(err); }
    });


}

function CalculateMachineSelectedCosts(data) {
    console.log(data)
    if (data != null) {
        let getMachinePlates = +$("#MachinePlates").val() || 0;
        let GroundColor = +$("#GroundColor").val() || 0;
        let TextColor = +$("#TextColor").val() || 0;
        let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
        let MachineTextColor = +$("#MachineTextColor").val() || 0;
        let Length = +$("#Length").val() || 0;
        let DieCost = +$("#DieCost").val() || 0;
        let Width = +$("#Width").val() || 0;
        //let NoOfSheets = +$("#NumberOfSheets").val() || 0;
        let NoOfSheets = +$("#TotalNumberOfSheets").val() || 0;
        let GroundCost = (+data.GroundCost * MachineGroundColor) || 0;
        let TextCost = (+data.TextCost * MachineTextColor) || 0;
        let GlossLaminationCost = +data.GlossLaminationCost || 0;
        let MatLaminationCost = +data.MatLaminationCost || 0;
        let HotLaminationCost = +data.HotLaminationCost || 0;

        let uvMattVarnish = +data.UVMattVarnish.toFixed(2) || 0;
        let uvGlossVarnish = +data.UVGlossVarnish.toFixed(2) || 0;
        let aqMattVarnish = +data.AQMattVarnish.toFixed(2) || 0;
        let aqGlossVarnish = +data.AQGlossVarnish.toFixed(2) || 0;
        let embCost = +data.EmbosingCost.toFixed(2) || 0;
        let OverheadExpense = +data.OverheadExpense.toFixed(2) || 0;

        let tr2Cost = +data.tr2Cost.toFixed(2) || 0;
        console.log(tr2Cost);
        console.log(GroundColor);
        console.log(TextColor);
        let totTr2 = (GroundColor + TextColor) * tr2Cost || 0;
        console.log(totTr2);



        //let NoOfPlates = +GroundCost + +TextCost;
        let NoOfPlates = MachineGroundColor + MachineTextColor;
        let PlateCost = +data.PlateCost * +NoOfPlates;
        let UVCost = +data.UVCost || 0;
        let TotalUVCost = (+UVCost / 144) * (+Length * +Width * +NoOfSheets);
        let Impressions = +$("#Impression").val() || 0;
        let DieCuttingCost = +data.DieCuttingCost * +Impressions + +DieCost;
        let SlittingCost = +data.SlittingCost || 0;
        let TotalSlittingCost = +SlittingCost;//* Impressions;
        let TotalGlossLaminationCost = (+GlossLaminationCost);/// 144) * +Length * +Width * +NoOfSheets;
        let TotalMatLaminationCost = (+MatLaminationCost);// / 144) * +Length * +Width * +NoOfSheets;
        let TotalHotLaminationCost = (+HotLaminationCost);/// 144) * +Length * +Width * +NoOfSheets;

        let PrintingCost = (+Impressions * +GroundCost) + (+Impressions * +TextCost);


        $("#GlossLaminationCost").val(parseFloat(+GlossLaminationCost).toFixed(2));
        $("#MatLaminationCost").val(parseFloat(+MatLaminationCost).toFixed(2));
        $("#HotLaminationCost").val(parseFloat(+HotLaminationCost).toFixed(2));
        //  if (getMachinePlates != NoOfPlates) {
        $("#MachinePlates").val(NoOfPlates);
        // }

        $("#MachineGroundCost").val(parseFloat(+GroundCost).toFixed(2));
        $("#MachineTextCost").val(parseFloat(+TextCost).toFixed(2));

        if ($('#GlossLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); $("#OffsetTypeID").val(1); }

        if ($('#MatLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); $("#OffsetTypeID").val(1); }

        if ($('#HotLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalHotLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalHotLaminationCost).toFixed(2)); $("#OffsetTypeID").val(1); }



        $("#MachinePlateCost").val(parseFloat(+PlateCost).toFixed(2));
        $("#MachinePrintingCost").val(parseFloat(+PrintingCost).toFixed(2));
        //$("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        //$("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        //$("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));

        if ($("#includeUVCost").is(":checked")) {
            $("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        }
        else {
            $("#UVCost").val(parseFloat(0).toFixed(2));
        }
        if ($("#includeSlittingCost").is(":checked")) {
            $("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));
        }
        else {
            $("#SlittingCost").val(parseFloat(0).toFixed(2));
        }
        //if ($("#includeDieCost").is(":checked")) {
        //    $("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        //}
        //else {
        //    $("#DieCuttingCost").val(parseFloat(0).toFixed(2));
        //}

        ////////////////////////////
        if ($("#includeUVMattCost").is(":checked")) {
            $("#UVMattVarnish").val(parseFloat(+uvMattVarnish).toFixed(2));
        }
        else {
            $("#UVMattVarnish").val(parseFloat(0).toFixed(2));
        }

        if ($("#includeUVGlossCost").is(":checked")) {
            $("#UVGlossVarnish").val(parseFloat(+uvGlossVarnish).toFixed(2));
        }
        else {
            $("#UVGlossVarnish").val(parseFloat(0).toFixed(2));
        }

        if ($("#includeAQMattCost").is(":checked")) {
            $("#AQMattVarnish").val(parseFloat(+aqMattVarnish).toFixed(2));
        }
        else {
            $("#AQMattVarnish").val(parseFloat(0).toFixed(2));
        }

        if ($("#includeAQGlossCost").is(":checked")) {
            $("#AQGlossVarnish").val(parseFloat(+aqGlossVarnish).toFixed(2));
        }
        else {
            $("#AQGlossVarnish").val(parseFloat(0).toFixed(2));
        }

        if ($("#includeEmbosingCost").is(":checked")) {
            $("#EmbosingCost").val(parseFloat(+embCost).toFixed(2));
        }
        if ($("#includeTr2PlateCost").is(":checked")) {
            $("#tr2PlateCost").val(parseFloat(+totTr2).toFixed(2));
        }
        else {
            $("#tr2PlateCost").val(parseFloat(0).toFixed(2));
        }

        if ($("#includeExpenseCost").is(":checked")) {
            $("#GeneralExpense").val(parseFloat(+OverheadExpense).toFixed(2));
        }
        else {
            $("#GeneralExpense").val(parseFloat(0).toFixed(2));
        }
    }
}


function CalculateMachineSelectedCostsWithManualMachinePlatesCost(data) {
    console.log(data)
    if (data != null) {
        let getMachinePlates = +$("#MachinePlates").val() || 0;
        let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
        let MachineTextColor = +$("#MachineTextColor").val() || 0;
        let Length = +$("#Length").val() || 0;
        let DieCost = +$("#DieCost").val() || 0;
        let Width = +$("#Width").val() || 0;
        //let NoOfSheets = +$("#NumberOfSheets").val() || 0;
        let NoOfSheets = +$("#TotalNumberOfSheets").val() || 0;
        let GroundCost = (+data.GroundCost * MachineGroundColor) || 0;
        let TextCost = (+data.TextCost * MachineTextColor) || 0;
        let GlossLaminationCost = +data.GlossLaminationCost || 0;
        let MatLaminationCost = +data.MatLaminationCost || 0;
        let HotLaminationCost = +data.HotLaminationCost || 0;
        //let NoOfPlates = +GroundCost + +TextCost;
        // let NoOfPlates = MachineGroundColor + MachineTextColor;
        let PlateCost = +data.PlateCost * +getMachinePlates;
        let UVCost = +data.UVCost || 0;
        let TotalUVCost = (+UVCost / 144) * (+Length * +Width * +NoOfSheets);
        let Impressions = +$("#Impression").val() || 0;
        //let PrintingCost = +Impressions * +GroundCost * +TextCost;
        let PrintingCost = (+Impressions * +GroundCost) + (+Impressions * +TextCost);
        let DieCuttingCost = +data.DieCuttingCost * +Impressions + +DieCost;
        let SlittingCost = +data.SlittingCost || 0;
        let TotalSlittingCost = +SlittingCost * Impressions;
        let TotalGlossLaminationCost = (+GlossLaminationCost);// / 144) * +Length * +Width * +NoOfSheets;
        let TotalMatLaminationCost = (+MatLaminationCost);// / 144) * +Length * +Width * +NoOfSheets;
        let TotalHotLaminationCost = (+HotLaminationCost);// / 144) * +Length * +Width * +NoOfSheets;


        $("#GlossLaminationCost").val(parseFloat(+GlossLaminationCost).toFixed(2));
        $("#MatLaminationCost").val(parseFloat(+MatLaminationCost).toFixed(2));
        $("#HotLaminationCost").val(parseFloat(+HotLaminationCost).toFixed(2));
        //  if (getMachinePlates != NoOfPlates) {
        //$("#MachinePlates").val(NoOfPlates);
        // }

        $("#MachineGroundCost").val(parseFloat(+GroundCost).toFixed(2));
        $("#MachineTextCost").val(parseFloat(+TextCost).toFixed(2));

        if ($('#GlossLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); $("#OffsetTypeID").val(1); }

        if ($('#MatLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); $("#OffsetTypeID").val(2); }

        if ($('#HotLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalHotLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalHotLaminationCost).toFixed(2)); $("#OffsetTypeID").val(3); }


        $("#MachinePlateCost").val(parseFloat(+PlateCost).toFixed(2));
        $("#MachinePrintingCost").val(parseFloat(+PrintingCost).toFixed(2));
        //$("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        //$("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        //$("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));
        if ($("#includeUVCost").is(":checked")) {
            $("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        }
        else {
            $("#UVCost").val(parseFloat(0).toFixed(2));
        }
        if ($("#includeSlittingCost").is(":checked")) {
            $("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));
        }
        else {
            $("#SlittingCost").val(parseFloat(0).toFixed(2));
        }
        //if ($("#includeDieCost").is(":checked")) {
        //    $("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        //}
        //else {
        //    $("#DieCuttingCost").val(parseFloat(0).toFixed(2));
        //}


    }
}
$('input[name="rad"]').click(function () {
    try {
        var $radio = $(this);
        console.log($radio.data('waschecked'));
        // if this was previously checked
        if ($radio.data('waschecked') == true) {
            $radio.prop('checked', false);
            $radio.data('waschecked', false);
        }
        else
            $radio.data('waschecked', true);

        // remove was checked from other radios
        $radio.siblings('input[name="rad"]').data('waschecked', false);

        if ($('#' + $radio.attr('id')).is(':checked')) {
            console.log($radio.attr('id') + "-checked");

            if ($radio.attr('id').toString() === 'MatLaminationCost') {
                console.log("Mat");

                //$("#GlossLaminationCost").prop('checked', false);
                let getMachinePlates = +$("#MachinePlates").val() || 0;
                let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
                let MachineTextColor = +$("#MachineTextColor").val() || 0;
                let finalColorRate = MachineGroundColor + MachineTextColor;
                if (globalData != null) {
                    console.log("In Mat")
                    // if ($('#MatLaminationCost').is(':checked')) {
                    if (getMachinePlates === finalColorRate) {
                        CalculateMachineSelectedCosts(globalData);
                    }
                    else {
                        CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
                    }


                    FinalPriceCalculation();
                    //  }
                }

            }

            else if ($radio.attr('id').toString() === 'GlossLaminationCost') {
                console.log("Gloss");
                //$("#MatLaminationCost").prop('checked', false);
                let getMachinePlates = +$("#MachinePlates").val() || 0;
                let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
                let MachineTextColor = +$("#MachineTextColor").val() || 0;
                let finalColorRate = MachineGroundColor + MachineTextColor;
                if (globalData != null) {
                    console.log("In Gloss")
                    //if ($('#GlossLaminationCost').is(':checked')) {
                    if (getMachinePlates === finalColorRate) {
                        CalculateMachineSelectedCosts(globalData);
                    }
                    else {
                        CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
                    }

                    FinalPriceCalculation();
                    //   }
                }
            }

            else if ($radio.attr('id').toString() === 'HotLaminationCost') {
                console.log("Hot");
                //$("#MatLaminationCost").prop('checked', false);
                let getMachinePlates = +$("#MachinePlates").val() || 0;
                let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
                let MachineTextColor = +$("#MachineTextColor").val() || 0;
                let finalColorRate = MachineGroundColor + MachineTextColor;
                if (globalData != null) {
                    console.log("In Hot")
                    //if ($('#GlossLaminationCost').is(':checked')) {
                    if (getMachinePlates === finalColorRate) {
                        CalculateMachineSelectedCosts(globalData);
                    }
                    else {
                        CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
                    }

                    FinalPriceCalculation();
                    //   }
                }
            }
        }
        else {
            console.log($radio.attr('id') + "-un-checked");

            $("#LaminationCost").val(0);
            FinalPriceCalculation();
        }
    }
    catch (err) {
        console.log("checkbox error->" + err);
    }

});

//$("#MatLaminationCost").click(function () {
//    console.log("Mat")
//    let getMachinePlates = +$("#MachinePlates").val() || 0;
//    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
//    let MachineTextColor = +$("#MachineTextColor").val() || 0;
//    let finalColorRate = MachineGroundColor + MachineTextColor;
//    if (globalData != null) {
//        console.log("In Mat")
//        if ($('#MatLaminationCost').is(':checked')) {
//            if (getMachinePlates === finalColorRate) {
//                CalculateMachineSelectedCosts(globalData);
//            }
//            else {
//                CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
//            }


//            FinalPriceCalculation();
//        }
//    }

//})

//$("#GlossLaminationCost").click(function () {
//    console.log("Gloss");
//    let getMachinePlates = +$("#MachinePlates").val() || 0;
//    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
//    let MachineTextColor = +$("#MachineTextColor").val() || 0;
//    let finalColorRate = MachineGroundColor + MachineTextColor;
//    if (globalData != null) {
//        console.log("In Gloss")
//        if ($('#GlossLaminationCost').is(':checked')) {
//            if (getMachinePlates === finalColorRate) {
//                CalculateMachineSelectedCosts(globalData);
//            }
//            else {
//                CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
//            }

//            FinalPriceCalculation();
//        }
//    }

//})


function FinalPriceCalculation() {
    let TotalSheetCost = +$("#TotalSheetCost").val() || 0;
    //let NoOfSheets = +$("#NumberOfSheets").val() || 0;
    let NoOfSheets = +$("#TotalNumberOfSheets").val() || 0;
    let MaterialCostM2 = +$("#MaterialCostPerMeter").val() || 0;
    let Qty = +$("#QtyPerUnit").val() || 1;
    let Upping = +$("#Upping").val() || 0;
    let MachinePlateCost = +$("#MachinePlateCost").val() || 0;
    let MachinePrintingCost = +$("#MachinePrintingCost").val() || 0;
    let LaminationCost = +$("#LaminationCost").val() || 0;
    let DieCuttingCost = +$("#DieCuttingCost").val() || 0;
    let UVCost = +$("#UVCost").val() || 0;
    let SlittingCost = +$("#SlittingCost").val() || 0;
    let DesignCost = +$("#DesignCost").val() || 0;
    let FoilPrintingCost = +$("#FoilPrintingCost").val() || 0;
    let MarkupRate = +$("#MarkupRate").val() || 0;

    let wastage_running_mtr = +$("#Wastage").val() || 0;
    let running_mtr = +$("#RunningMeter").val() || 0;
    let reel_size = +$("#ReelSize").val() || 0;
    let ground_color = +$("#GroundColor").val() || 0;
    let text_color = +$("#TextColor").val() || 0;
    let thermal_cost = +$("#StickerRibbonTCost").val() || 0;
    let foil_cost = +$("#StickerRibbonFCost").val() || 0;
    let rfid_cost = +$("#StickerRibbonRFIDCost").val() || 0;
    let satin_cost = +$("#StickerRibbonSCost").val() || 0;
    let m2_required = +$("#RequiredM2").val() || 1;
    let emb_cost = +$("#EmbosingCost").val() || 0;
    let overhead_exp = +$("#GeneralExpense").val() || 0;
    let labour_cost = +labourCostG || 0;
    let printing_cost = +$("#PrintingCost").val() || 0;
    let printing_block_cost = +$("#StickerPrintingBlockCost").val() || 0;
    let foil_block = +$("#FoilPrintingCost").val() || 0;
    let embosing_block = +$("#EmbosingBlock").val() || 0;
    let die_block = +$("#DieBlock").val() || 0;
    let material_cost = +$("#TotalMaterialCost").val() || 0;

    let length = +$("#Length").val() || 0;
    let width = +$("#Width").val() || 0;

    let UVMattVarnish = +$("#UVMattVarnish").val() || 0;
    let UVGlossVarnish = +$("#UVGlossVarnish").val() || 0;
    let AQMattVarnish = +$("#AQMattVarnish").val() || 0;
    let AQGlossVarnish = +$("#AQGlossVarnish").val() || 0;
    let additional_selection_rate = UVMattVarnish + UVGlossVarnish + AQMattVarnish + AQGlossVarnish;
    //let tr2_plate_cost = tr2PlateCostG || 0;
    let repeat_length = +$("#RepeatLength").val() || 0;



    let cost_per_plate = +$("#tr2PlateCost").val() || 0;//tr2_plate_cost * (ground_color + text_color);

    //$("#tr2PlateCost").val(+cost_per_plate.toFixed(2));

    //let total_m2_cost_w = (wastage_percent + 100) / 100 * m2_required;

    let total_m2_cost_w = ((wastage_running_mtr + running_mtr) * reel_size / 1000);
    $("#TotalM2ReqIncludingWastage").val(+total_m2_cost_w.toFixed(2));

    let total_qty_req_inc_wastage = +(total_m2_cost_w * 1000) / ((length * width) / 1000) || 0;
    $("#TotalQtyIncludingWastage").val(+total_qty_req_inc_wastage.toFixed(2));

    let running_mtr_inc_wastage = +(total_qty_req_inc_wastage / Upping * (repeat_length / 1000)) || 0;
    $("#RunningMeterIncludingWastage").val(+running_mtr_inc_wastage.toFixed(2));

    let total_material_cost = (+total_m2_cost_w.toFixed(2)) * MaterialCostM2;//Rate
    $("#TotalMaterialCost").val(+total_material_cost.toFixed(2));




    //let total_m2_cost = overhead_exp + labour_cost + emb_cost + thermal_cost + foil_cost + rfid_cost + DieCuttingCost + printing_cost + material_cost + additional_selection_rate;
    let total_m2_cost = SlittingCost + LaminationCost + satin_cost + emb_cost + thermal_cost + foil_cost + rfid_cost + DieCuttingCost + printing_cost + MaterialCostM2 + additional_selection_rate + labour_cost + overhead_exp;
    //let printing_block_cost = +$("#StickerPrintingBlockCost").val() || 0;
    //let total_m2_inc_wastage = +$("#TotalM2ReqIncludingWastage").val() || 0;
    // let stk_printing_blk_cost = +$("#StickerPrintingBlockCost").val() || 0;
    //console.log("SlittingCost:"+SlittingCost)
    //console.log("LaminationCost:" + LaminationCost)
    //console.log("satin_cost:" + satin_cost)
    //console.log("emb_cost:" + emb_cost)
    //console.log("thermal_cost:" + thermal_cost)
    //console.log("foil_cost:" + foil_cost)
    //console.log("rfid_cost:" + rfid_cost)
    //console.log("DieCuttingCost:" + DieCuttingCost)
    //console.log("printing_cost:" + printing_cost)
    //console.log("MaterialCostM2:" + MaterialCostM2)
    //console.log("additional_selection_rate:" + additional_selection_rate)
    //console.log("labour_cost:" + labour_cost)

    $("#TotalM2Cost").val(+total_m2_cost.toFixed(2));

    let sum_block_cost = +(embosing_block + foil_block + die_block + printing_block_cost + cost_per_plate) || 0;

    //console.log("embosing_block: " + embosing_block);
    //console.log("foil_block: " + foil_block);
    //console.log("die_block: " + die_block);
    //console.log("printing_block_cost: " + printing_block_cost);
    //console.log("cost_per_plate: " + cost_per_plate);
    //console.log("sum_block_cost: " + sum_block_cost);

    $("#SumOfBlockCost").val(+sum_block_cost.toFixed(2));

    let final_price = +(((total_m2_cost * total_m2_cost_w) + sum_block_cost) / parseFloat(Qty).toFixed(2));


    console.log("total_m2_cost: " + total_m2_cost);
    console.log("total_m2_cost_w: " + total_m2_cost_w);
    console.log("sum_block_cost: " + sum_block_cost);
    console.log("Qty: " + Qty);
    console.log("final_price: " + final_price);

    $("#MarkedUpPrice").val(+final_price.toFixed(2));


    //////////////////////////////////////////////////////////

    let GrandTotal = (TotalSheetCost + MachinePlateCost + MachinePrintingCost + LaminationCost +
        DieCuttingCost + UVCost + SlittingCost + DesignCost + FoilPrintingCost) / +Qty;
    let updatedTotal = +GrandTotal;

    //if (MarkupRate > 0) {
    //    $("#FinalCostPerPc").val(parseFloat(updatedTotal).toFixed(2));

    //    let calculatedMarkupPrice = (+MarkupRate / 100) * +GrandTotal;

    //     GrandTotal = GrandTotal + calculatedMarkupPrice;
    //    $("#MarkedUpPrice").val(parseFloat(GrandTotal).toFixed(2));

    //}
    //else {
    //    $("#FinalCostPerPc").val(parseFloat(updatedTotal).toFixed(2));
    //    $("#MarkedUpPrice").val(parseFloat(updatedTotal).toFixed(2));
    //}


}

$("#MarkupRate").focusout(function () {

    let MarkUpRate = +$("#MarkupRate").val() || 0;
    let FinalCostPerPc = +$("#FinalCostPerPc").val() || 0;
    if (MarkUpRate > 0 && FinalCostPerPc > 0) {

        console.log(MarkUpRate)
        console.log(FinalCostPerPc)

        var PercentAmount = parseFloat((MarkUpRate / 100) * (FinalCostPerPc)).toPrecision(2);
        console.log(+PercentAmount)
        var FinalPrice = parseFloat(+PercentAmount + FinalCostPerPc).toPrecision(2);
        console.log(+FinalPrice)
        $("#MarkedUpPrice").val(parseFloat(+FinalPrice).toFixed(2));
        // FinalPriceCalculation();
    }
});
$("#MarkedUpPrice").focusout(function () {
    let MarkedUpPrice = +$("#MarkedUpPrice").val() || 0;
    let FinalCostPerPc = +$("#FinalCostPerPc").val() || 0;
    if (MarkedUpPrice > 0 && +FinalCostPerPc > 0) {
        var actualPrice = parseFloat(MarkedUpPrice - FinalCostPerPc).toPrecision(2);//487

        PercentRate = parseFloat((+actualPrice * 100) / +MarkedUpPrice).toPrecision(2);
        $("#MarkupRate").val(parseFloat(+PercentRate).toFixed(2));

    }
    else {
        $("#MarkupRate").val(0);
        FinalPriceCalculation();
    }
});

$("#DieCost").change(function () {
    let DieCost = +$("#DieCost").val() || 0;
    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;
    //if (DieCost > 0) {
    if (globalData != null) {
        console.log("In")
        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }

    //}
});

$("#DesignCost").change(function () {
    let DesignCost = +$("#DesignCost").val() || 0;
    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;
    //if (DesignCost > 0) {
    if (globalData != null) {
        console.log("In")
        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }

    //}
});

$("#MachineGroundColor").change(function () {
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    if (MachineGroundColor > 0) {
        if (globalData != null) {
            console.log("In")
            CalculateMachineSelectedCosts(globalData); FinalPriceCalculation();
        }

    }
});

$("#MachineTextColor").change(function () {
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    if (MachineTextColor > 0) {
        if (globalData != null) {
            console.log("In")
            CalculateMachineSelectedCosts(globalData); FinalPriceCalculation();
        }

    }
});

$("#FoilPrintingCost").change(function () {
    let FoilPrintingCost = +$("#FoilPrintingCost").val() || 0;
    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;
    //if (FoilPrintingCost > 0) {
    if (globalData != null) {
        console.log("In");
        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }

    //}
});

$("#MachinePlates").change(function () {
    let MachinePlates = +$("#MachinePlates").val() || 0;
    if (MachinePlates > 0) {
        if (globalData != null) {
            console.log("In");
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData); FinalPriceCalculation();
        }

    }
});

$("#includeDieCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    if ($('#includeDieCost').is(':checked')) {
        $("#DieCost").prop('readonly', false);
        console.log("die-checked")
        $("#DieCuttingCost").val(+dieCuttingCostGlobal);

    }
    else {
        console.log("die-un-checked")
        $("#DieCost").prop('readonly', true);
        $("#DieCuttingCost").val(0);
    }

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})


$("#includeUVCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    if ($('#includeUVCost').is(':checked')) {
        console.log("uv-checked")
        $("#UVCost").val(+uvCostGlobal);

    }
    else {
        console.log("uv-un-checked")
        $("#UVCost").val(0);
    }

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})


$("#includeSlittingCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    if ($('#includeSlittingCost').is(':checked')) {
        console.log("slitchecked")
        $("#SlittingCost").val(+slittingCostGlobal);

    }
    else {
        console.log("slit-un-checked")
        $("#SlittingCost").val(0);
    }

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})


$("#includeUVMattCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    //if ($('#includeDieCost').is(':checked')) {
    //    $("#DieCost").prop('readonly', false);
    //    console.log("die-checked")
    //    $("#DieCuttingCost").val(+dieCuttingCostGlobal);

    //}
    //else {
    //    console.log("die-un-checked")
    //    $("#DieCost").prop('readonly', true);
    //    $("#DieCuttingCost").val(0);
    //}

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})

$("#includeUVGlossCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    //if ($('#includeDieCost').is(':checked')) {
    //    $("#DieCost").prop('readonly', false);
    //    console.log("die-checked")
    //    $("#DieCuttingCost").val(+dieCuttingCostGlobal);

    //}
    //else {
    //    console.log("die-un-checked")
    //    $("#DieCost").prop('readonly', true);
    //    $("#DieCuttingCost").val(0);
    //}

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})

$("#includeAQMattCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    //if ($('#includeDieCost').is(':checked')) {
    //    $("#DieCost").prop('readonly', false);
    //    console.log("die-checked")
    //    $("#DieCuttingCost").val(+dieCuttingCostGlobal);

    //}
    //else {
    //    console.log("die-un-checked")
    //    $("#DieCost").prop('readonly', true);
    //    $("#DieCuttingCost").val(0);
    //}

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})

$("#includeAQGlossCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    //if ($('#includeDieCost').is(':checked')) {
    //    $("#DieCost").prop('readonly', false);
    //    console.log("die-checked")
    //    $("#DieCuttingCost").val(+dieCuttingCostGlobal);

    //}
    //else {
    //    console.log("die-un-checked")
    //    $("#DieCost").prop('readonly', true);
    //    $("#DieCuttingCost").val(0);
    //}

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})

$("#includeEmbosingCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    //if ($('#includeDieCost').is(':checked')) {
    //    $("#DieCost").prop('readonly', false);
    //    console.log("die-checked")
    //    $("#DieCuttingCost").val(+dieCuttingCostGlobal);

    //}
    //else {
    //    console.log("die-un-checked")
    //    $("#DieCost").prop('readonly', true);
    //    $("#DieCuttingCost").val(0);
    //}

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})

$("#includeTr2PlateCost").click(() => {

    //let tr2_plate_cost = tr2PlateCostG || 0;
    //let TextColor = +$("#TextColor").val() || 0;
    //let GroundColor = +$("#GroundColor").val() || 0;
    //let total = tr2_plate_cost * (TextColor + GroundColor);

    //if ($('#includeTr2PlateCost').is(':checked')) {
    //  ///  $("#tr2PlateCost").prop('readonly', false);
    //    console.log("die-checked")
    //    $("#tr2PlateCost").val(+total.toFixed(2));

    //}
    //else {
    //    console.log("tr2-un-checked")
    //   // $("#tr2PlateCost").prop('readonly', true);
    //    $("#tr2PlateCost").val(0);
    //}

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    //if ($('#includeDieCost').is(':checked')) {
    //    $("#DieCost").prop('readonly', false);
    //    console.log("die-checked")
    //    $("#DieCuttingCost").val(+dieCuttingCostGlobal);

    //}
    //else {
    //    console.log("die-un-checked")
    //    $("#DieCost").prop('readonly', true);
    //    $("#DieCuttingCost").val(0);
    //}



    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }


});

$("#includeExpenseCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    //if ($('#includeDieCost').is(':checked')) {
    //    $("#DieCost").prop('readonly', false);
    //    console.log("die-checked")
    //    $("#DieCuttingCost").val(+dieCuttingCostGlobal);

    //}
    //else {
    //    console.log("die-un-checked")
    //    $("#DieCost").prop('readonly', true);
    //    $("#DieCuttingCost").val(0);
    //}

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})

