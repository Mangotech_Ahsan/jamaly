﻿var customerID = 0;
var accID = 0;
var isPONOExist = false;
var isQuote = false;
var SOID = 0;
var DatE = null;
var OrderId = $('#InvOrderId').val();
var deptID = $('#hdnDeptID').val();

var isLimitExceed = false;
getHoldID();
getSOID();
$("#Bank").prop("disabled", true);

GetEditCustomers(OrderId);
//Edit Customer
function GetEditCustomers(OrderId) {
    let currentUserRole = $("#currentUserRole").val() || null;
    let prodID = +$("#hdnProdID").val() || 0;
    if (OrderId == "") { OrderId = -1; }
    var json = { "OrderId": OrderId, "currentUserRole": currentUserRole, "DeptID": deptID, "ProductID": prodID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Sales/GetSaleOrderDetails',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            var num = (data[0].salesDate).match(/\d+/g);
            var date = new Date(parseFloat(num));

            $('#PrintingMachineID').val(data[0].ProductsList.PrintingMachineID).trigger('change.select2');
            $('#CylinderID').val(data[0].ProductsList.CylinderID).trigger('change.select2');
            $('#CardQuality').val(data[0].ProductsList.CardQualityID).trigger('change.select2');
            $('#FinishingTypeID').val(data[0].ProductsList.FinishingTypeID).trigger('change.select2');
            $('#BlockTypeID').val(data[0].ProductsList.BlockTypeID).trigger('change.select2');
            $('#PrintingRatePerRoll').val(data[0].ProductsList.PrintingRatePerRoll);
            $('#QtyPerUnit').val(data[0].ProductsList.Qty);
            $('#Length').val(data[0].ProductsList.Length);
            $('#CardLength').val(data[0].ProductsList.CardLength);
            $('#CardWidth').val(data[0].ProductsList.CardWidth);
            $('#CutLength').val(data[0].ProductsList.CutLength);
            $('#CutWidth').val(data[0].ProductsList.CutWidth);
            $('#CardGSM').val(data[0].ProductsList.GSM);
            $('#Upping').val(data[0].ProductsList.Upping);
            $('#NoOfSheets').val(data[0].ProductsList.NoOfSheets);
            $('#RawSizeSheets').val(data[0].ProductsList.RawSize);
            $('#NOOfImpressions').val(data[0].ProductsList.Impression);
            $('#Wastage').val(data[0].ProductsList.Wastage);
            $('#TotalSheets').val(data[0].ProductsList.TotalSheets);

            $('#Width').val(data[0].ProductsList.Width);
            $('#CylinderLength').val(data[0].ProductsList.CylinderLength);
            $('#AroundUps').val(data[0].ProductsList.AroundUps);
            $('#NoOfPrintingColors').val(data[0].ProductsList.NoOfPrintingColor);
            $('#RollID').val(data[0].ProductsList.RollID).trigger('change.select2');
            $('#TotalPcsInRoll').val(data[0].ProductsList.TotalPcInRoll);
            $('#WastagePcsInRoll').val(data[0].ProductsList.WastagePcInRoll);
            $('#TotalRequiredRolls').val(data[0].ProductsList.TotalReqRoll);

            $('#AccountID').val(data[0].Qry.Value).trigger('change.select2');
            $('#SalePersonAccID').val(data[0].SalePersonAccID).trigger('change.select2');

            $('#BranchID').val(data[0].Qry1.Value).trigger('change.select2');
            $('#customerCrLimit').val(data[0].Qry2);
            // console.log(convertJsonDate(data[0].ExpectedDeliveryDate));
            $('#ExpectedDeliveryDate').val(data[0].ExpectedDeliveryDate);
            var customerID = $('#AccountID').val();
            $('#hdnAccountID').val(customerID);
            getCustomerDetail(customerID);
            $('#PaymentStatus').val(data[0].PayStatus).trigger('change.select2');
            $("#Bank").val(data[0].Bank.Value).trigger('change.select2');

            // console.log(data[0].PayStatus);
            if (data[0].PayStatus == 3) {
                $("#PaymentType").prop('disabled', true);
            }
            $('#chqNumber').val(data[0].Cheque);
            $('#amountPaid').val(data[0].AmountPaid);
            $('#vatAmount').val(data[0].Tax);
            $('#creditDays').val(data[0].CreditDays);
            $('#finalAmountWithVAT').val(data[0].FinalAmount);
            $('#totalAmount').val(data[0].TotalAmount);
            $('#subAmount').val(data[0].SubAmount);
            $('#discInput').val(data[0].Discount);

            $('#AccountID').prop('disabled', true);
            $('#SalePersonAccID').prop('disabled', true);
            if (data[0].userRole == "SuperAdmin") {

                $('#SaleDate').prop('disabled', false);
            }
            if (data[0].userRole != "SuperAdmin") {

                $('#SaleDate').prop('disabled', true);
            }

            document.getElementById('SaleDate').valueAsDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getUTCHours() + 5, date.getMinutes());

            if (data[0].expectedDeliveryDate != null) {
                var expNum = (data[0].expectedDeliveryDate).match(/\d+/g);
                var ExpectedDate = new Date(parseFloat(expNum));
                document.getElementById('ExpectedDeliveryDate').valueAsDate = new Date(ExpectedDate.getFullYear(), ExpectedDate.getMonth(), ExpectedDate.getDate(), ExpectedDate.getUTCHours() + 5, ExpectedDate.getMinutes());

            }
            //console.log(ToJSDate(data[0].salesDate));

            if (data[0].Paytype == "" || data[0].Paytype == null) {
            }
            else {
                $('#PaymentType').val(data[0].Paytype.Value).trigger('change.select2');
                if (data[0].Paytype.Value == 1) {
                    $("#Bank").prop("disabled", true);
                    $("#chqNumber").prop("disabled", true);
                    $("#chqDate").prop("disabled", true);
                }
                else if (data[0].Paytype.Value == 2) {
                    $("#Bank").prop("disabled", false);
                    $("#chqNumber").prop("disabled", true);
                    $("#chqDate").prop("disabled", true);
                }
                else {
                    $("#Bank").prop("disabled", false);
                    $("#chqNumber").prop("disabled", false);
                    $("#chqDate").prop("disabled", false);
                }
            }


        },
        error: function (err) { console.log(err); }
    });
}

//  Get New SOID 
function ToJSDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
}
// Calculate Discount Amount from Discount Percentage 
function calcDiscount() {
    var disc = parseFloat($('#customerDiscount').val());
    var subTotal = $("#subAmount").val();
    if (disc == "" || isNaN(disc)) { applyDiscount(); }
    else if (parseFloat(subTotal) > 0) {
        var discAmount = parseFloat((disc * subTotal) / 100);
        $('#discInput').val(parseFloat(discAmount).toFixed(2));
        getNewTotal();
    }
}
function getSOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/getNewSOID',
        async: true,
        success: function (data) {
            {
                $("#hdnSOID").val(data);
                SOID = data;
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function getHoldID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/Quotation/getLastHoldID',
        async: true,
        success: function (data) {
            {
                var value = data;
                var newValue = value.split('-');
                var increase = newValue[1];
                increase++;
                value = "H-" + increase;
                $("#hdnHoldID").val(value);
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}
$("#PONO").on("change", function () {
    var accID = $('#hdnAccountID').val();
    if (accID == "" || accID == 0) {
        swal("Customer", "Please Select Customer First!", "error");
    } else {
        var PONO = $("#PONO").val();
        checkPONO(PONO, accID);

    }
});

function checkPONO(PONO, accID) {

    var json = {
        "accountID": accID,
        "PONO": PONO
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/checkPONO',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            if (data == true) {
                toastr.warning('PONo already Exists for selected Customer!')
                $("#PONO").val("");
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

$('#getQuotation').click(function () {
    var qouteID = $('#QID').val();
    var orderID = 0;
    if (qouteID != "") {
        var json = {
            "QuoteID": qouteID
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Quotation/getQuotationOrderID',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
                if (data > 0) {
                    orderID = data;
                    //console.log(orderID);
                    window.location.href = '/Quotation/SubmitOrders?OrderID=' + orderID;
                } else {
                    swal("Submitted", "Quotation Already Submitted!", "error");
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    } else {
        swal("QuotationID", "Enter QuotationID!", "error");
    }


});

//$('#btnSubmit').click(function () {
//    accID = $('#hdnAccountID').val();
//    var isValid = true;
//    var PONo = $('#PONO').val();
//    var SalePersonAccID = $('#SalePersonAccID').val() || 0;
//    var SalesDate = $('#SaleDate').val();
//    var paymentStatus = $('#PaymentStatus option:selected').val();
//    var rowCount = $('#tblProduct tbody tr').length;
//    var PaymentTypeID = $('#PaymentType option:selected').val();
//    //var BranchID = $('#BranchID option:selected').val();
//    var BranchID = $('#hdnBranchId').val();
//    var amountPaid = $("#amountPaid").val();
//    var balance = $("#balanceAmount").val();
//    var creditLimit = $('#customerCrLimit').val();
//    var customerBalance = $('#customerBalance').val();
//    var bankAccountId = $('#Bank option:selected').val();
//    if (PONo != "") {
//        // check duplication
//    }
//    if (accID == "" || typeof accID == undefined || accID == 0) {
//        isValid = false;
//        swal("Customer", "Please Select Customer!", "error");
//    }
//    else if (SalePersonAccID == "" || typeof SalePersonAccID == undefined || SalePersonAccID == 0) {
//        isValid = false;
//        swal("Sale Person", "Please Select Sale Person!", "error");
//    }
//    //if ($tableItemCounter == Count) {
//    //    isValid = false;
//    //    swal("Missed Products", "Products are not added after editing!", "warning");
//    //} else 
//    if (SalesDate == "") {
//        isValid = false;
//        swal("Date", "Please Enter Sales Date!", "error");
//        //console.log(PurchaseDate + "date");
//    } else if (BranchID == "" || BranchID == 0 || BranchID == undefined) {
//        isValid = false;
//        swal("Branch", "Please Enter Branch!", "error");
//        //alert("Please Select Vendor");
//    } else if (rowCount == 0 || rowCount < 0) {
//        isValid = false;
//        swal("Product", "Please Enter Products!", "error");
//        //console.log(PurchaseDate + "date");
//    } else if (paymentStatus == 0) {
//        isValid = false;
//        swal("Payment Status", "Please Select Payment Status!", "error");
//    } else if ((paymentStatus == 1 || paymentStatus == 2) && (PaymentTypeID == "" || PaymentTypeID == undefined)) {
//        isValid = false;
//        swal("Payment Type", "Please Select Payment Type!", "error");
//    } else if ((paymentStatus == 2) && (amountPaid == "" || amountPaid == 0)) {
//        isValid = false;
//        swal("Amount Paid!", "Please Enter Amount!", "error");
//    } else if ((PaymentTypeID == 3 || PaymentTypeID == 2) && (Bank == null || Bank == "" || Bank == undefined)) {
//        isValid = false;
//        console.log(Bank + "Bank");

//        swal("Bank", "Please Select Bank Account!", "error");
//    } else if (PaymentTypeID == 3 && (ChqNo == null || ChqNo == "")) {
//        isValid = false;
//        console.log(ChqNo + "ChqNo");


//        swal("Cheque No.", "Please Enter Cheque Number!", "error");
//    } else if ((paymentStatus == 2) && (parseFloat(balance) > parseFloat(creditLimit - customerBalance))) {
//        isLimitExceed = false;
//        //console.log('Partial Paid');
//        swal("Credit Limit", "Credit Limit Exceeds! ", "error");
//    } else if (PaymentTypeID == 3 && (DatE == null || DatE == "") && (ChqDate == null || ChqDate == "" || ChqDate == undefined)) {

//        isValid = false;
//        console.log(DatE + "chDate");
//        console.log(ChqDate + "chnewDate");

//        swal("Date", "Please Enter Cheque Date!", "error");
//        //console.log(PurchaseDate + "date");
//    }
//    //else if (PaymentTypeID == 3 && (DatE == null || DatE == "") ) {

//    //    isValid = false;
//    //    console.log(DatE + "chDate");
//    //    swal("Date", "Please Enter Cheque Date!", "error");
//    //    //console.log(PurchaseDate + "date");
//    //}
//    else if (isLimitExceed == true) {
//        //swal("Credit Limit", "You can not sale more than Credit Limit! ", "error");
//        swal("Credit Limit", "Credit Limit Exceeds! ", "error");
//    } else if (isValid == true && isLimitExceed == false) {
//        $('#btnSubmit').prop('disabled', true);
//        $('#btnHold').prop('disabled', true);
//        uiBlock();
//        insert();
//    }
//});


// Payment Status DropDOwn, Action according to Payment Status Selection
$("#PaymentStatus").on("change", function () {
    var payStatusSelection = $("#PaymentStatus").val();
    var totalAmount = $("#finalAmountWithVAT").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    //Paid
    if (payStatusSelection == 1) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        getNewTotal();
        //document.getElementById("amountPaid").value = totalAmount;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("balanceAmount").value = 0;
        //document.getElementById("expenseInput").readOnly = false;
        //document.getElementById("vatInput").readOnly = false;
    }
    // Partial PAid
    else if (payStatusSelection == 2) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = false;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
    }
    //Unpaid
    else if ((payStatusSelection == 3) && (parseFloat(totalAmount) > parseFloat(creditLimit - customerBalance))) { // Remove Nested if Condition 
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        $("#PaymentType").prop("disabled", true);
        disableBank();
        swal("Credit Limit", "Credit Limit Exceeds! ", "error");
    }
    //else if ((payStatusSelection == 3) && (parseFloat(totalAmount) <= parseFloat(creditLimit))) {                      // Remove Nested if Condition 

    //    isLimitExceed = false;//isLimitExceed = true;
    //    document.getElementById("amountPaid").readOnly = true;
    //    document.getElementById("amountPaid").value = 0;
    //    document.getElementById("balanceAmount").value = totalAmount;
    //    $("#PaymentType").prop("disabled", true);
    //    disableBank();
    //}
    else if (payStatusSelection == 3 && (creditLimit == "")) {
        isLimitExceed = false;
        swal("Credit Limit", "Credit Limit not available! ", "error");
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        $("#PaymentType").prop("disabled", true);
        disableBank();
    } else if (payStatusSelection == 3) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        document.getElementById("PaymentType").selectedIndex = "0";
        $("#PaymentType").prop("disabled", true);
        disableBank();
    } else {
        isLimitExceed = false;

    }

});

// Check credit limit when Unpaid Selected 
function checkCreditLimit() {
    var creditLimit = $('#customerCrLimit').val();
    var balance = $('#customerBalance').val();
    if ((parseFloat(totalAmount) > parseFloat(creditLimit))) {
        isLimitExceed = false;
        swal("Credit Limit", "Credit Limit not available! ", "error");
    } else {
        isLimitExceed = false;
    }
}

function disableBank() {
    $("#Bank").prop("disabled", true);
    $("#chqNumber").prop("disabled", true);
    $("#chqDate").prop("disabled", true);
}
// Payment Type Change 
$("#PaymentType").on("change", function () {
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //Cash
    if (PaymentTypeID == 1) {
        //$("#bankName").prop("disabled", true);
        $("#Bank").prop("disabled", true);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);
    }
    // Bank
    else if (PaymentTypeID == 2) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", true);
        $("#chqDate").prop("disabled", true);

    }
    // Cheque
    else if (PaymentTypeID == 3) {
        //$("#bankName").prop("disabled", false);
        $("#Bank").prop("disabled", false);
        $("#chqNumber").prop("disabled", false);
        $("#chqDate").prop("disabled", false);
    }
});

function getNewTotal() {
    applyDiscount();
    calcVAT();
    calcAmountPaid();
    var subTotal = $("#subAmount").val();
    var VAT = $('#vatAmount').val();
    var discount = $('#discInput').val();
    if (VAT == "" || isNaN(VAT)) {
        VAT = 0;
        applyZeroVAT();
    }
    if (discount == "" || isNaN(discount)) {
        discount = 0;
        applyDiscount();
    }
    var newTotal = parseFloat(subTotal) + parseFloat(VAT) - parseFloat(discount);
    // var amountPaid = parseFloat(subTotal) + parseFloat(VAT);
    $('#finalAmountWithVAT').val(parseFloat(newTotal).toFixed(3));
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(newTotal).toFixed(3);
    }
}

function applyZeroVAT() {
    //var vat = parseFloat($('#vatInput').val());
    var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        //$('#vatInput').val(0);
        vat = 0;
        $('#vatAmount').val(0);
        $('#finalAmountWithVAT').val($("#totalAmount").val());
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat($("#totalAmount").val()).toFixed(3);
        }
    }
}


function applyDiscount() {
    var discount = $('#discInput').val();
    var subTotal = $("#subAmount").val();
    if (discount == "" || isNaN(discount)) {
        //$('#discInput').val(0);
        discount = 0;
        $('#totalAmount').val(subTotal);
    } else if (parseFloat(discount) >= parseFloat(subTotal)) {
        // toastr.warning('Discount must less than total!')
        $('#discInput').val(0);
        $('#totalAmount').val(subTotal);
    } else {
        var totalAmount = parseFloat(subTotal) - parseFloat(discount);
        $('#totalAmount').val(parseFloat(totalAmount).toFixed(3));
    }
}
$('#vatAmount').on('input', function (e) {
    getNewTotal();
});
// Get Customer Balance and Credit Limit 
function getCustomerDetail(accountID) {
    if (accountID == "") {
        accountID = -1;
    }
    var json = {
        "accountID": accountID
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Customer/getDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#customerCrLimit').val(data.creditLimit);
            $('#customerBalance').val(data.Balance);
            $('#customerDiscount').val(data.Discount);
        },
        error: function (err) {
            console.log(err);
        }
    });
}
// calculate balance if amountPaid entered and make changes every where needed
function calcAmountPaid() {
    isLimitExceed = false;
    var amountPaid = $("#amountPaid").val();
    var finalAmount = $('#finalAmountWithVAT').val();
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 2) {
        if (parseFloat(amountPaid) >= parseFloat(finalAmount)) {
            toastr.warning('Partial ! AmountPaid should be less than total amount ')
            $("#amountPaid").val(0)
        } else {
            var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
            $("#balanceAmount").val(parseFloat(balance).toFixed(3));
        }
    } else {

        var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
        $("#balanceAmount").val(parseFloat(balance).toFixed(3));
    }
}
// in case of vat entered please do calculations
function calcVAT() {
    //var vat = parseFloat($('#vatInput').val());
    var vat = parseFloat($('#vatAmount').val());
    var payStatusSelection = $("#PaymentStatus").val();
    if (vat == "" || isNaN(vat)) {
        applyZeroVAT();
    } else {
        var totalInvoice = $("#totalAmount").val();
        //var vatAmount = parseFloat((vat * totalInvoice) / 100);
        var vatAmount = parseFloat(vat);
        // $('#vatAmount').val(parseFloat(vatAmount).toFixed(3));
        var finalAmount = parseFloat(vatAmount) + parseFloat(totalInvoice);
        $('#finalAmountWithVAT').val(finalAmount);
        if (payStatusSelection == 1) {
            document.getElementById("amountPaid").value = parseFloat(finalAmount).toFixed(3);
        }
    }
}
// if Vat then calculate and make effects on total amount
$('#vatInput').on('input', function (e) {
    getNewTotal();
});
$('#vatAmount').on('input', function (e) {
    getNewTotal();
});
// if Discount then calculate and make effects on total amount
$('#discInput').on('input', function (e) {
    getNewTotal();
});
$("#cashReceived").on("change", function () {
    cash();
});
$("#amountPaid").on("change", function () {
    calcAmountPaid();
});
function cash() {
    var cashRec = parseFloat($('#cashReceived').val());
    var invoiceAmount = parseFloat($('#totalAmount').val());
    if (cashRec >= invoiceAmount) {
        var retAmount = parseFloat(cashRec) - parseFloat(invoiceAmount);
        $('#cashReturned').val(retAmount);
    }
    else {
        swal("Short", "Recd cash must greater than total!", "error");
    }
}

$('#btnSubmit').click(function () {
    let ReadyQty = 1;//+$("#ReadyQty").val() || 0;
    let ExpDate = $("#ExpectedDeliveryDate").val() || null;
    let SaleDate = $("#SaleDate").val() || null;
    let isValid = true;

    console.log(ReadyQty)
    console.log(ExpDate)
    console.log(SaleDate)
    console.log(isValid)

    if (ExpDate == null) {
        swal("", "Expected Delivery Date Required", "error");
        isValid = false;
    }
    else if (SaleDate == null) {
        swal("", "Sales Date Required", "error");
        isValid = false;
    }
    else if (ReadyQty == null || ReadyQty <= 0) {
        swal("", "Qty Required", "error");
        isValid = false;
    }

    if (isValid == true) {
        $('#btnSubmit').prop('disabled', true);
        $('#btnHold').prop('disabled', true);
        uiBlock();
        insert();
    }




});
var stockLog = [];
function GetFinalData() {

    let OrderID = +$("#InvOrderId").val() || 0;
    let DepartmentID = +$("#hdnDeptID").val() || 0;
    let ProductID = +$("#hdnProdID").val() || 0;
    //let machineTypeID = +$("#MachineTypeID").val() || 0;
    //let machineID = +$("#AttachedMachineID").val() || 0;
    let ReadyQty = 1;//+$("#ReadyQty").val() || 0;

    //let SlittingOMachineID = +$("#SlittingOMachineID").val() || null;
    //let DieCuttingOMachineID = +$("#DieCuttingOMachineID").val() || null;
    //let UVOMachineID = +$("#UVOMachineID").val() || null;
    //let LaminationOMachineID = +$("#LaminationOMachineID").val() || null;
    //let FoilOMachineID = +$("#FoilOMachineID").val() || null;
    //let EmbosingOMachineID = +$("#EmbosingOMachineID").val() || null;

    //$('#tblProduct tbody tr').each(function (i, n) {
    //    var $row = $(n);
    //    var pId = parseInt($row.find('#ProductID').text());
    //    var partNumber = $row.find("#PartNo").text();
    //    var UnitCode = $row.find("#UnitCode").text();
    //    var UnitPrice = parseFloat($row.find('#UnitPrice').text()) || 0;
    //    var Qty = parseFloat($row.find('#Qty').text());
    //    var SubTotal = parseFloat($row.find("#SubTotal").text()) || 0;

    //});
    if (ProductID > 0 && DepartmentID > 0 && OrderID > 0 && ReadyQty > 0) {
        var data = {

            'OrderID': OrderID,
            'DepartmentID': DepartmentID,
            'ProductID': ProductID,
            'ExpectedDeliveryDate': $("#ExpectedDeliveryDate").val(),
            'SalesDate': $("#SaleDate").val(),
            'ReadyQty': ReadyQty,
            //'AttachedMachineID': machineID,
            //'MachineTypeID': machineTypeID
            //'SlittingOMachineID': SlittingOMachineID,
            //'DieCuttingOMachineID': DieCuttingOMachineID,
            //'UVOMachineID': UVOMachineID,
            //'LaminationOMachineID': LaminationOMachineID,
            //'FoilOMachineID': FoilOMachineID,
            //'EmbosingOMachineID': EmbosingOMachineID

        };

        return data;
    }



    return null;
}


function insert() {

    var data = GetFinalData();
    if (data != null) {
        var json = JSON.stringify({
            'model': data,

        });

        ajaxCall("POST", json, "application/json; charset=utf-8", "/Sales/UpdateSatinOrderProduct", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result > 0) {
                uiUnBlock();
                // window.open('/SalesOrder/printOrder?id=' + parseInt(Result) + '');
                //window.open('/SalesOrder/Details?id=' + parseInt(Result) + '');
                //console.log("OK");

                swal("", "Updated", "success");

                setTimeout(function () {
                    window.location.href = '/Sales/DepartmentWiseOrdersIndex';
                }, 1500)

            } else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);

                alert("Some error Ocurred! Please Check Your Entries");
            }
        }

        function onFailure(error) {
            if (error.statusText == "OK") {
                console.log("OK");
            } else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }
    else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("Check Entries", "Some error Ocurred! Please Check your Entries!", "error");
    }
}

