﻿
var $addedProcessIDs = [];
$("#AttachedMachineID").select2();
$("#MachineTypeID").select2();

$("#AttachedMachineID").prop('disabled', true);

$("#MachineTypeID").change(() => {
    let machineTypeID = $("#MachineTypeID").val() || 0;
    let DepartmentID = +$("#hdnprocessModalDepartmentID").val() || 0;
    console.log(machineTypeID)
    if (machineTypeID > 0 && DepartmentID>0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/Sales/GetMachinesByTypeIDWise?ID=' + machineTypeID + '&&DepartmentID=' + DepartmentID,
            async: true,
            // data: JSON.stringify(json),
            success: function (data) {
                console.log(data)
                GetDropdown1('AttachedMachineID', data, true);
                $("#AttachedMachineID").prop('disabled', false);
            },
            error: function (err) { console.log(err); $("#AttachedMachineID").val('').change(); $("#AttachedMachineID").prop('disabled', true); }
        });

       
    }
    else {
        $("#AttachedMachineID").val('').change();
        $("#AttachedMachineID").prop('disabled', true);
    }
    
}) 

$("#processBtn").click(() => {

    let machineTypeID = +$("#MachineTypeID").val() || 0;
    let machineID = +$("#AttachedMachineID").val() || 0;
    let OrderID = +$("#hdnprocessModalOrderID").val() || 0;
    let ProductID = +$("#hdnprocessModalProductID").val() || 0;
    let DepartmentID = +$("#hdnprocessModalDepartmentID").val() || 0;
    let machineArray = [];
    
    $('#attachProcessTable tbody tr').each(function (i, n) {
        var $row = $(n);
        var processID = parseInt($row.find('#rowProcessID').text());
        var processTypeID = parseInt($row.find('#rowProcessTypeID').text());
      

        ///// 
        machineArray.push({
            AttachedMachineID: processID,
            MachineTypeID: processTypeID,
           
        });

    });

    var data = {

        'OrderID': OrderID,
        'DepartmentID': DepartmentID,
        'ProductID': ProductID,
        'AttachedMachinesList': machineArray,
        
    };


    if (data != null) {
        var json = JSON.stringify({
            'model': data,

        });

        ajaxCall("POST", json, "application/json; charset=utf-8", "/Sales/UpdateMachineProcess", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result > 0) {
                uiUnBlock();
                
                swal("", "Updated", "success");
                $("#proc_modalCloseBtn").click();
                
                $("#btnSearch").click();

            } else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);

                alert("Some error Ocurred! Please Check Your Entries");
            }
        }

        function onFailure(error) {
            if (error.statusText == "OK") {
                console.log("OK");
            } else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }
    else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);
        swal("Check Entries", "Some error Ocurred! Please Check your Entries!", "error");
    }
});


$("#attachProcessRowBtn").click(function () {

    const processTypeID = +$("#MachineTypeID").val() || 0;
    const processType = $("#MachineTypeID option:selected").text() || null;
    const processID = +$("#AttachedMachineID").val() || 0;
    const process = $("#AttachedMachineID option:selected").text() || null;

    const ItemID = +$("#hdnprocessModalProductID").val() || 0;
    const OrderID = +$("#hdnprocessModalOrderID").val() || 0;
    const DeptID = +$("#hdnprocessModalDepartmentID").val() || 0;
   
    if (ItemID > 0 && OrderID > 0 && DeptID > 0 && processTypeID > 0 && processID >0) {

       
            //const found = itemWiseProducts.some(el => el.ItemID === pID);
            //console.log('found array->');

            //if (!found) {
            //    console.log(found)
            //    console.log(itemWiseProducts)
            //    $("#attachProcessTable tbody").empty();
            //    //swal('', 'not found', 'error');
            //    console.log('not found')
            //}
            //else {
                
                   

        //var index = $.inArray(+processID, $addedProcessIDs);
        //if (index >= 0) {
        //    swal("Error", "Already Added!", "error");
        //}
        //else {
            var markup = "<tr>\
                             <td><input type='image' id='myimage' src='/Content/images/icons/remsIcon.png' tabindex='-1' onclick='deleteRow(this,"+ ItemID + "," + OrderID + ")'/></td>\
                                         <td hidden  id='rowProcessTypeID'> " + processTypeID + " </td> \
                   <td  id='rowProcessType'> "+ processType + " </td> \
                   <td hidden id='rowProcessID'> "+ processID + " </td> \
                   <td id='rowProcess'>  "+ process + " </td> \
                    </tr>";

            $("#attachProcessTable tbody").append(markup);
            $addedProcessIDs.push(+processID);
        //}
       
    }
    else {
        swal("Error", "Data Error", "error");

    }


})


function deleteRow(r, ItemID, RowID) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("attachProcessTable").deleteRow(i);
    var row = $(r).closest("tr");

    var processID = +row.find('#rowProcessID').text() || 0; // find hidden id
    if (processID > 0) {
        var index = $.inArray(+processID, $addedProcessIDs);
        if (index >= 0) { $addedProcessIDs.splice(index, 1); }

    }

    //console.log(ItemID)
    //console.log("before deleting...")
    //console.log(itemWiseProducts)
    
    //var data = itemWiseProducts.filter(obj => obj.RowID === RowID);
    //itemWiseProducts = itemWiseProducts.filter(obj => obj.RowID !== RowID);

    //console.log("deleting object...")
    //console.log(data)
    //console.log("after deleting...")
    //console.log(itemWiseProducts)
    //overrideTotalItemQty();

    //calcTotalItemQty();

}
