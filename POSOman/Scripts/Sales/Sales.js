﻿


$('#AccountID').select2();
$('#ddlVehCode').select2();
$('#ddlPartNumber').select2();
$('#InquiryID').select2();
$('#TaxPercent').select2();

//document.getElementById('chqDate').valueAsDate = new Date();
document.getElementById('SaleDate').valueAsDate = new Date();



function isValid() {

    var isValid = true;
    accID = $('#AccountID').val();
    inquiryID = $('#InquiryID').val();
    var PONo = $('#PONO').val();
    var SalePersonAccID = $('#SalePersonAccID').val() || 0;
    var SalesDate = $('#SaleDate').val();
    var DeliveryDate = SalesDate;//$('#ExpectedDeliveryDate').val();
    var paymentStatus = $('#PaymentStatus option:selected').val();
    var rowCount = $('#tblProduct tbody tr').length;
    var PaymentTypeID = $('#PaymentType option:selected').val();
    var BranchID = $('#BranchID option:selected').val();
    var amountPaid = $("#amountPaid").val();
    var balance = $("#balanceAmount").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    var bankAccountId = $('#Bank option:selected').val();
    var cashRec = parseFloat($('#cashReceived').val());
    var cashRet = parseFloat($('#cashReturned').val());
    var invoiceAmount = parseFloat($('#totalAmount').val());

    if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Customer", "Please Select Customer!", "error");
    }
    else if (SalePersonAccID == "" || typeof SalePersonAccID == undefined || SalePersonAccID == 0) {
        isValid = false;
        swal("Sale Person", "Please Select Sale Person!", "error");
    }
    //else if (inquiryID == "" || typeof inquiryID == undefined || inquiryID == 0) {
    //    isValid = false;
    //    swal("Inquiry", "Please Select Inquiry!", "error");
    //}
    else if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Sales Date!", "error");
    }
    else if (DeliveryDate == "") {
        isValid = false;
        swal("Date", "Please Enter Delivery Date!", "error");
    }
    else if (rowCount == 0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");
    }
    else if (paymentStatus == 0) {
        isValid = false;
        swal("Payment Status", "Please Select Payment Status!", "error");
    }
    else if ((paymentStatus == 1 || paymentStatus == 2) && (PaymentTypeID == "" || PaymentTypeID == undefined)) {
        isValid = false;
        swal("Payment Type", "Please Select Payment Type!", "error");
    }
    else if ((paymentStatus == 2) && (amountPaid == "" || amountPaid == 0)) {
        isValid = false;
        swal("Amount Paid!", "Please Enter Amount!", "error");
    }
    else if ((PaymentTypeID == 3 || PaymentTypeID == 2) && (bankAccountId == "" || bankAccountId == undefined)) {
        isValid = false;
        swal("Bank!", "Please Select  Bank!", "error");
    }

    return isValid;
    
}


$('#submitButton').click(function () {
    console.log("btn pressed")
    if (isValid()) {
        console.log("valid")
        $('#submitButton').prop('disabled', true);
       // $('#btnHold').prop('disabled', true);
       // uiBlock();
        insertOrder();
    }
});

function GetFinalData() {
    
    var saleDetails = [];
    var BranchId = $('#BranchID option:selected').val();

    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = parseInt($row.find('#ProductID').text());
        var storeStatusID = parseInt($row.find('#hdnStoreStatusID').text());
        var deliveryStatusID = parseInt($row.find('#hdnDeliveryStatusID').text());
        var orderMachineID = parseInt($row.find('#hdnOrderMachineID').text());
        var impression = parseInt($row.find('#impression').text());
        var sheets = +$row.find('#sheets').text()||0;
        var totalSheets = +$row.find('#totalsheets').text()||0;
        var partNumber = $row.find("#PartNo").text();
        var UnitCode = $row.find("#UnitCode").text();
        var UnitCodeID = +$row.find("#UnitCodeID").text()||null;
        var UnitPrice = parseFloat($row.find('#UnitPrice').text()) || 0;
        var salePrice = parseFloat($row.find('#SalePrice').text()) || 0;
        var Qty = parseFloat($row.find('#Qty').text());
        var SubTotal = parseFloat($row.find("#SubTotal").text()) || 0;
        var isSameAsPkgQty = $row.find("#chkbx").is(":checked");
        
        ///// Sale Details ////
        saleDetails.push({
            ProductID: pId,
            StoreStatusID: storeStatusID,
            DeliveryStatusID: deliveryStatusID,
            OrderMachineID: orderMachineID,
            MachineImpression: impression,
            NoOfSheets: sheets,
            TotalSheets: totalSheets,
            UnitCode: UnitCode,
            UnitCodeID: UnitCodeID,
            Qty: Qty,
            UnitPrice: UnitPrice,            
            SalePrice: salePrice,
            Total: SubTotal,
            BranchID: BranchId,
            isSameAsItemQty: isSameAsPkgQty
        });
       
    });

    if (saleDetails.length) {
        var itemQtySum = saleDetails.reduce((accum, item) => accum + item.Qty, 0);
        var pkgQtySum = itemWiseProducts.reduce((accum, item) => accum + item.Qty, 0);

        console.log("itemQtySum->" + itemQtySum);
        console.log("pkgQtySum->" + pkgQtySum);

        //if (+itemQtySum != +pkgQtySum) {
        //    return null;
        //}
        var data = {
            'AccountID': $('#AccountID').val(),
            'SalePersonAccID': $('#SalePersonAccID').val(),
            'SOID': $("#SOID").val(),
            'BranchID': BranchId,
            'PaymentStatus': "UnPaid",
            'SalesDate': $('#SaleDate').val(),
            'ExpectedDeliveryDate': null,//$('#ExpectedDeliveryDate').val(),
            'PaymentTypeID': 2,
            'TotalAmount': parseFloat($("#totalAmount").val()),
            'DiscountPercent': parseFloat($('#DiscountPercent').val()) || 0,
            'DiscountAmount': parseFloat($('#DiscountAmount').val()) || 0,
            'AmountPaid': $('#amountPaid').val() || 0,
            'TotalPaid': $('#amountPaid').val() || 0,
            'CustomerPO': $('#CustomerPO').val() ,
            'SaleOrderNatureID': $('#OrderNature').val() ,
            'Remarks': $('#Remarks').val() ,
            'InquiryID': $("#InquiryID").val(),
            'TaxPercent': $("#TaxPercent").val(),
            'TaxAmount': $("#TaxAmount").val(),
            'QuotationID': $("#QuotationID").val(),
            'tbl_SaleDetails': saleDetails
        };

        return data;
    }

    return null;

}

// Add data to array and send it to controller for order creation
function insertOrder() {
    
    var data = GetFinalData();
    if (data != null) {
        var json = JSON.stringify({
            'model': data,
            'packageModel': itemWiseProducts
        });
        
        ajaxCall("POST", json, "application/json; charset=utf-8", "/Sales/SaveOrder", "json", onSuccess, onFailure);
    }
    else {
       // uiUnBlock();
        $('#submitButton').prop('disabled', false);
        swal("Check Entries", "Some error Ocurred! Please Check your Entries/Package/Item Qty!", "error");
    }
}


function onSuccess(Result) {
    console.log("success")
    console.log("result->"+Result)
    if (+Result > 0) {
        // uiUnBlock();
        swal('Order Created', 'Job Order created successfully!', 'success');

        //window.location.href = '/Sales';
        window.location.href = '/Sales/Export/'+Result+'?ReportType=PDF';

    }
    else if(Result == -2) {
       // uiUnBlock();
        $('#submitButton').prop('disabled', false);
        swal("Critical Error", "Item Didnot exist to issue!", "error");
    }
    else if (Result == -5) {
       // uiUnBlock();
        $('#submitButton').prop('disabled', false);
        swal("Package Qty Error", "Package qty is not equal! Please check.", "error");
    }
    else if (Result == -6) {
       // uiUnBlock();
        $('#submitButton').prop('disabled', false);
        swal("Package Error", "Package qty for item cannot be same at same time i.e. same as item qty and add packages", "error");
    }
    else {
      //  uiUnBlock();
        $('#submitButton').prop('disabled', false);
        swal("Critical Error", "Something went Wrong! Please try Again!", "error");
    }
}

function onFailure(error) {
   // uiUnBlock();
    $('#submitButton').prop('disabled', false);
    console.log("error")
    console.log("result->" + error)
    if (error.statusText == "OK") {
        console.log("OK");

    } else {
        swal("Critical Error", "Something went Wrong! Please try Again!", "error");
    }
}

