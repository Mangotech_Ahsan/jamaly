﻿
$(document).ready(function () {
    hideTableColumns();
})
$("#AccountID").change(function () {
    let AccID = +$("#AccountID").val() || 0;
  //  console.log(AccID)
    if (AccID > 0) {
        $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
            url: '/Customer/GetCustomerDetail?accID=' + AccID,
        async: true,
        success: function (data) {
          //  $("#AccountID").val(data.AccountID).change();
            $("#CreditLimit").val(+data.CreditLimit);
            $("#SalePersonAccID").val(+data.SalePersonAccID).trigger('change.select2');
            $("#CustomerGSTPercentage").val(+data.GST);
            $("#InvType").val(data.InvoiceType);
        },
        error: function (err) { console.log(err); }
    });
    }
   
});

$("#tblProduct").click(function () {
    console.log("in")
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
       
        var isSameAsPkgQty = $row.find("#chkbx").is(":checked");
        if (isSameAsPkgQty == true) {
            $row.find("#addPackageHref").prop("disabled", true);
           // $('#addPackageHref').css('color', '#ff0000');
        }
        else {
            $row.find("#addPackageHref").prop("disabled", false);
          //  $('#addPackageHref').css('color', '#1D9293');
        }
    });
})

$("#OrderNature").change(function () {
    let OrderNatureID = +$("#OrderNature").val() || 0;

    if (OrderNatureID == 1) {//Sample
        $("#Qty").val(1);
        $("#SalePrice").val(1);

       // $("#Qty").prop('readonly', true);
        $("#SalePrice").prop('readonly', true);
    }
    else {//Order or not selected
        $("#Qty").val('');
        $("#SalePrice").val('');

      //  $("#Qty").prop('readonly', false);
        $("#SalePrice").prop('readonly', false);
    }

})

$("#btnSearch").click(function () {
    if ($("#QuotationID").val() != "") {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/InquiryQuotation/QuotationDetail?id=' + $("#QuotationID").val(),
            async: true,
            success: function (data) {
                $("#AccountID").val(data[0].AccountID).change();
                $("#InquiryID").val(data[0].InquiryID).change();

                $.each(data, function (index, value) {
                    var CategoryID = value.CategoryID;
                    var CategoryName = value.CategoryName;
                    var ProductID = value.ProductID;
                    var ProductName = value.ProductName;
                    var UnitCode = value.UnitCode;
                    var Qty = parseFloat(value.Qty) || 0;
                    var UnitPrice = parseFloat(value.UnitPrice) || 0;
                    var SubTotal = parseFloat(value.Total) || 0;


                    GenerateProductTable(CategoryID, CategoryName, ProductID, ProductName, UnitCode, Qty, UnitPrice, SubTotal);
                })
            },
            error: function (err) { console.log(err); }
        });
    }
    else {
        swal("Quotation", "Please Select Quotation!", "error")
    }
});



$("#TaxPercent").change(function () {
    var SubTotal = parseFloat($("#subAmount").val()) || 0;
    var TaxPercent = parseFloat($("#TaxPercent").val()) || 0;
    var TaxAmt = parseFloat((TaxPercent / 100) * SubTotal).toFixed(2);

    $("#TaxAmount").val(TaxAmt);
    CalcTotal();
});

$("#DiscountPercent").keyup(function () {
    var SubTotal = parseFloat($("#subAmount").val()) || 0;
    var DiscPercent = parseFloat($("#DiscountPercent").val()) || 0;
    var DiscAmt = parseFloat((DiscPercent / 100) * SubTotal).toFixed(2);

    $("#DiscountAmount").val(DiscAmt);
    CalcTotal();
});






