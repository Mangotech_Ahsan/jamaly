﻿var chqDat = "";
var AccountID = 0;
var BranchID = 0;

$(function () {
    var table = $('#tblSales').DataTable();

    $('#tblSales tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('#btnSave').click(function () {
        $('#btnSave').prop('disabled', true);
        uiBlock();
        insert();
    });
});

function calcTotal() {
    var total = 0;
    $('#tblReturn tbody tr').each(function (i, n) {
        var $row = $(n);
        //var subTotal = parseFloat($row.find('td:eq(13) input[id*="txtSub"]').val());  
        var subTotal = parseFloat($row.find('input[id*="txtSub"]').val());
        total += parseFloat(subTotal).toFixed(3);
        //console.log(subTotal);
        //console.log(total);
    });

    $('#tblReturn tfoot tr').find('input[id*="txtTotal"]').val(parseFloat(total).toFixed(3));
    $('#TotalAmount').val(parseFloat(total).toFixed(3));
}
var invoiceTotal = 0;
function getInvoiceDetails(orderID) {
    ajaxCall("GET", { "OrderID": orderID }, "application/json; charset=utf-8", "/DOReturn/getDODetails",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        $('#tblReturn').find('tbody').empty();
        if (ien > 0) {

            var desiredStep = 1;
            //$("#steps-uid-0-t-" + desiredStep).click();
            var controlReturning = '<input id="rQty" type = "Number" min="0" max=' + returningBalance + ' step="0" style="width: 50px;" class="form-control"  value=0>';
            var html = '';
            for (var i = 0; i < ien; i++) {

                AccountID = data.qry[i].AccountID;
                DiscountAmount = data.qry[i].DiscountAmount;
                BranchID = data.qry[i].BranchID;
                invoiceTotal = data.qry[i].TotalAmount;
                var returningBalance = parseFloat(data.qry[i].Qty - data.qry[i].ReturnedQty);
                html += '<tr>';
                //html+='<td> <input type=checkbox > </td>';
                //html += '<td>' + data.qry[i].OrderID + '</td>';
                html += '<td><input id="orderID" type="hidden" value="' + data.qry[i].OrderID + '">' + data.qry[i].DOID + '</td>';
                html += '<td><input id="prodID" type="hidden" value="' + data.qry[i].ProductID + '">' + data.qry[i].PartNo + '</td>';
                html += '<td id=SaleQty><input id="costPrice" type="hidden" value="' + data.qry[i].UnitPrice + '">' + data.qry[i].Qty + '</td>';
                html += '<td id=SalePrice>' + data.qry[i].SalePrice + '</td>';
                html += '<td>' + data.qry[i].Total + '</td>';
                html += '<td id=OldReturnQty>' + data.qry[i].ReturnedQty + '</td>';
                html += '<input id="returningBalQty" type="hidden" value="' + returningBalance + '">';
                html += '<td> <input id="rQty" name="rQty" type = "number" min="0" max=' + returningBalance + ' step="0"  value=0 class="form-control" style="width: 120px;"> </td>';
                html += '<td><input type="number"  class="form-control" style="width: 120px;" name="sub" style="width: 70px" id="txtAmount" disabled />' + '</td>';
                html += '</tr>';
            }
            $('#tblReturn tbody').append(html);
        }

    }
    function onFailure(err) {
        console.log(err);
    }
}
$('#tblReturn').keyup(function (e) {
    $field = $(e.target).closest("tr").find('input[id*="rQty"]');
    var returningQty = $(e.target).closest("tr").find('input[id*="rQty"]').val();
    var price = $(e.target).closest("tr").find("#SalePrice").text();
    // Prevent Entering more than balance qty 
    if (returningQty > Number($field.attr("max"))) {
        var max = $field.attr("max");
        $field.val(0);
        toastr.warning('Returning quantity must be equal or less than balance qty!')
    }
    if (($.isNumeric(price)) && ($.isNumeric(returningQty))) {
        var amount = (returningQty * price);
        $(e.target).closest('tr').find('input[id*="txtAmount"]').val(amount);
        calcTotal();
    }
    else {
        $(e.target).closest('tr').find('input[id*="txtSub"]').val(''
        );
    }
});
function calcTotal() {
    var total = 0;
    $('#tblReturn tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('input[id*="txtAmount"]').val());
        if ($.isNumeric(subTotal)) {
            total += parseFloat(subTotal);
        }
    });
    $('#tblReturn tfoot tr').find('input[id*="txtTotal"]').val(total);
    //$('#TotalPaying').val(total);    
}
$('#tblReturn').keyup(function (e) {
    calcTotal();
});

function insert() {

    var doReturn = [];
    var stockLog = [];
    var total = 0;
    var qtyVE = 0;
    var isValid = false;
    totalInvoice = $('#TotalAmount').val();    
    var saleOrderID = 0;    
    var SOID = 0;
    $('#tblReturn tbody tr').each(function (i, n) {
        var $row = $(n);
        DeliveryOrderID = $row.find("input[id*='orderID']").val();        
        SOID = $row.find("td:eq(0)").text();        
        var pId = $row.find('input[id*="prodID"]').val();
        var costPrice = $row.find('input[id*="costPrice"]').val();
        var qty = $row.find("#SaleQty").text();
        var price = $row.find("#SalePrice").text();
        var subTotal = $row.find('input[id*="txtAmount"]').val();
        var returningQty = $row.find('input[id*="rQty"]').val();
        //console.log(costPrice + " costPrice");
        //console.log(price + " price");
        if (returningQty > 0) {
            isValid = true;
            doReturn.push({
                ProductId: pId,
                Qty: returningQty,
                SalePrice: price,
                UnitPrice: costPrice,
                Total: subTotal,
                BranchID: BranchID,
            });
            stockLog.push({
                ProductId: pId,
                StockIN: returningQty,
                SalePrice: price,
                CostPrice: costPrice,
                InReference: 'Sales Return',
                OrderTypeID: 8,
                UserReferenceID: SOID,
                BranchID: BranchID,
                InvoiceDate: new Date()
            });
        }
        else {
            uiUnBlock();
            $('#btnSave').prop('disabled', false);            
            //alert("Please Enter Some Qty to return");
        }
    });
    if (doReturn.length && isValid == true) {

        total = $('#tblReturn tfoot tr').find('input[id="txtTotal"]').val();

        var data = {
            'AccountID': AccountID,            
            'BranchID': BranchID,            
            'DeliveryOrderID': DeliveryOrderID,            
            'ReturnDate': new Date(),            
            'TotalAmount': total,
            'DOReturnDetails': doReturn
        };
        var json = JSON.stringify({ 'model': data, 'modelStockLog': stockLog });
        //console.log(json);
        ajaxCall("POST", json, "application/json; charset=utf-8", "/DOReturn/ReturnOrder", "json", onSuccess, onFailure);

        function onSuccess(Result) {
            if (parseInt(Result) > 0) {
                uiUnBlock();                
                window.location.href = '/DOReturn/Details?id=' + Result + '';
            }
            else {
                uiUnBlock();
                $('#btnSave').prop('disabled', false);
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                window.location.href = '/DOReturn/Details?id=' + Result + '';
                console.log(error.statusText);
            }
            else {
                uiUnBlock();
                $('#btnSave').prop('disabled', false);
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }
    else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);
        swal("Quantity", "Please Enter Some Qty!", "error");
    }
}

