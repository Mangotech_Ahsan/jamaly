﻿var OrderIDs = [];
var price = 0;
getSOID();
$("#Bank").prop("disabled", true);

function bindOrderData(data) {    
    $('#InvoiceNo').val(data.qry[0].InvoiceNo);
    var customerAccountID = data.qry[0].AccountID;    
    document.getElementById('hdnAccountID').value = customerAccountID;
    //document.getElementById('CustomerCode').value = data.qry[0].AccountID;
    getCustomerDetail(customerAccountID);
    document.getElementById('CustomerPhone').value = data.qry[0].AccountID;
    document.getElementById('AccountID').value = data.qry[0].AccountID;
    //document.getElementById('BranchID').value = data.qry[0].BranchID;    
    document.getElementById('subAmount').value = data.qry[0].TotalAmount;
    document.getElementById('PONO').value = data.qry[0].PONo;
    document.getElementById('VehicleNo').value = data.qry[0].VehicleNo;
    
}
function calcTotal() {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var subTotal = parseFloat($row.find('#ProductSubTotal').text());
        total += parseFloat(subTotal);
    });
    $("#subAmount").val(parseFloat(total).toFixed(3));
    $("#totalAmount").val(parseFloat(total).toFixed(3));
    $("#finalAmountWithVAT").val(parseFloat(total).toFixed(3));
    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(total).toFixed(3);
    }

}
// Get Unsaved order and its details
function getTempOrderDetails(orderIDs) {

    var json = JSON.stringify(orderIDs);       
    ajaxCall("POST", json, "application/json; charset=utf-8", "/DeliveryOrder/getOrdersData", "json", onSuccess, onFailure);
    function onSuccess(data) {
        var ien = data.qry.length;
        //console.log(ien);
        $('#tblProduct').find('tbody').empty();

        if (ien > 0) {
            bindOrderData(data);
            for (var i = 0; i < ien; i++) {
                var vehCode = (data.qry[i].VehicleCode);
                var salePrice = checkNull(data.qry[i].SalePrice);                
                var ProductID = data.qry[i].ProductID;
                var PartNO = data.qry[i].PartNo;  // 1
                var Description = data.qry[i].Description;
                var UnitCode = checkNull(data.qry[i].UnitCode);
                var Location = checkNull(data.qry[i].Location);
                var Qty = data.qry[i].Qty;
                //console.log(Qty + " Qty");
                var returnedQty = data.qry[i].ReturnedQty;
                //console.log(returnedQty + " returnedQty");
                var FinalQty = parseInt(Qty) - parseInt(returnedQty || 0);
                var SalePrice = data.qry[i].SalePrice; // 7
                var SubTotal = data.qry[i].Total; // 8
                var FinalSubTotal = parseFloat(FinalQty * SalePrice);
                var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';
                var markup = "<tr><td>" + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td>" + Description + "</td><td id=Location>" + Location + "</td><td>" + UnitCode + "</td><td id='ProductQty'>" + FinalQty + "</td><td contenteditable='true' id='ProductSalePrice'>" + SalePrice + "</td><td id='ProductSubTotal'>" + parseFloat(FinalSubTotal).toFixed(3) + "</td></tr>";
                $("#tblProduct tbody").append(markup);                               
                calcTotal();                
            }
            calcTotal();
        }
    }
    function onFailure(err) {
        console.log(err);
    }
}
$("#tblProduct").focusout(function () {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var qty = $row.find("#ProductQty").text();
        var price = $row.find("#ProductSalePrice").text();
        if (($.isNumeric(price)) && ($.isNumeric(qty))) {
            var amount = (qty * price);
            $row.find("#ProductSubTotal").text(parseFloat(amount).toFixed(3));
            var subTotal = parseFloat($row.find('#ProductSubTotal').text());
            total += parseFloat(subTotal);
            calcTotal();
        }
    });
});
function checkNull(value) {
    return JSON.stringify(value).replace(/null/g, "\"\"");
}
var customerID = 0;
var accID = 0;
var SOID = 0;
var isLimitExceed = false;
//  Get New SOID 
function getSOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/getNewSOID',
        async: true,
        success: function (data) {
            {
                $("#hdnSOID").val(data);
                SOID = data;
            }
        },
        error: function (err) { console.log(err); }
    });
}
// Get Customer Balance and Credit Limit 
function getCustomerDetail(accountID) {
    if (accountID == "")
    { accountID = -1; }
    var json = {
        "accountID": accountID
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Customer/getDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#customerCrLimit').val(data.creditLimit);
            $('#customerBalance').val(data.Balance);
        },
        error: function (err) {
            console.log(err);
        }
    });
}

$('#btnSubmit').click(function () {
    accID = $('#hdnAccountID').val();    
    var isValid = true;    
    var PONo = $('#PONO').val();
    var SalesDate = $('#SaleDate').val();
    var paymentStatus = $('#PaymentStatus option:selected').val();
    var rowCount = $('#tblProduct tbody tr').length;
    var PaymentTypeID = $('#PaymentType option:selected').val();
    //var BranchID = $('#BranchID option:selected').val();
    var BranchID = $('#hdnBranchId').val();
    var amountPaid = $("#amountPaid").val();    
    var balance = $("#balanceAmount").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    
    if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Customer", "Please Select Customer!", "error");
    }
    else if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Sales Date!", "error");        
    }
    else if (rowCount ==0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");        
    }
    else if (paymentStatus == 0) {
        isValid = false;
        swal("Payment Status", "Please Select Payment Status!", "error");
    }
    else if ((paymentStatus == 1 || paymentStatus == 2) && (PaymentTypeID == "" || PaymentTypeID == undefined)) {
        isValid = false;
        swal("Payment Type", "Please Select Payment Type!", "error");
    }
    else if ((paymentStatus == 2) && (amountPaid == "" || amountPaid == 0)) {
        isValid = false;
        swal("Amount Paid!", "Please Enter Amount!", "error");
    }
    else if ((paymentStatus == 2) && (parseFloat(balance) > parseFloat(creditLimit - customerBalance))) {
        isLimitExceed = true;
        //console.log('Partial Paid');
        swal("Credit Limit", "Credit Limit Exceeds! ", "error");
    }
    else if (isLimitExceed == true) {
        swal("Credit Limit", "You can not sale more than Credit Limit! ", "error");
    }
    else if (isValid == true && isLimitExceed == false) {
        $('#btnSubmit').prop('disabled', true);
        uiBlock();
        insert();
    }
});
// Payment Status DropDOwn, Action according to Payment Status Selection
$("#PaymentStatus").on("change", function () {
    var payStatusSelection = $("#PaymentStatus").val();
    var totalAmount = $("#finalAmountWithVAT").val();
    var creditLimit = $('#customerCrLimit').val();
    var customerBalance = $('#customerBalance').val();
    //Paid
    if (payStatusSelection == 1) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = true;
        getNewTotal();
        //document.getElementById("amountPaid").value = totalAmount;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("balanceAmount").value = 0;
        //document.getElementById("expenseInput").readOnly = false;
        //document.getElementById("vatInput").readOnly = false;
    }
        // Partial PAid
    else if (payStatusSelection == 2) {
        isLimitExceed = false;
        document.getElementById("amountPaid").readOnly = false;
        $("#PaymentType").prop("disabled", false);
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
    }
        //Unpaid
    else if ((payStatusSelection == 3) && (parseFloat(totalAmount) > parseFloat(creditLimit - customerBalance))) {                      // Remove Nested if Condition 
        isLimitExceed = true;
        //checkCreditLimit();
        //isLimitExceed = false;

        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        $("#PaymentType").prop("disabled", true);
        disableBank();
        swal("Credit Limit", "Credit Limit Exceeds! ", "error");
    }
    else if (payStatusSelection == 3 && (creditLimit == "")) {
        isLimitExceed = false;
        swal("Credit Limit", "Credit Limit not available! ", "error");
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        $("#PaymentType").prop("disabled", true);
        disableBank();
    }
    else if (payStatusSelection == 3) {
        isLimitExceed = false;        
        document.getElementById("amountPaid").readOnly = true;
        document.getElementById("amountPaid").value = 0;
        document.getElementById("balanceAmount").value = totalAmount;
        $("#PaymentType").prop("disabled", true);
        disableBank();
    }
    else {
        isLimitExceed = false;
    }

});
function disableBank() {
    $("#Bank").prop("disabled", true);
    $("#chqNumber").prop("disabled", true);
    $("#chqDate").prop("disabled", true);
}
    // Payment Type Change 
    $("#PaymentType").on("change", function () {
        var PaymentTypeID = $('#PaymentType option:selected').val();
        //Cash
        if (PaymentTypeID == 1) {
            //$("#bankName").prop("disabled", true);
            $("#Bank").prop("disabled", true);
            $("#chqNumber").prop("disabled", true);
            $("#chqDate").prop("disabled", true);
        }
            // Bank
            else if (PaymentTypeID == 2) {
                //$("#bankName").prop("disabled", false);
                $("#Bank").prop("disabled", false);
                $("#chqNumber").prop("disabled", true);
                $("#chqDate").prop("disabled", true);

            }
                // Cheque
                else if (PaymentTypeID == 3) {
                    //$("#bankName").prop("disabled", false);
                    $("#Bank").prop("disabled", false);
                    $("#chqNumber").prop("disabled", false);
                    $("#chqDate").prop("disabled", false);
                    }
                    });
                    function getNewTotal() {
                        applyDiscount();
                        calcVAT();
                        calcAmountPaid();
                        var subTotal = $("#subAmount").val();
                        var VAT = $('#vatAmount').val();
                        var discount = $('#discInput').val();
                        if (VAT == "" || isNaN(VAT)) { VAT = 0; applyZeroVAT(); }
                        if (discount == "" || isNaN(discount)) { discount = 0; applyDiscount(); }
                        var newTotal = parseFloat(subTotal) + parseFloat(VAT) -parseFloat(discount);
                        // var amountPaid = parseFloat(subTotal) + parseFloat(VAT);
                         $('#finalAmountWithVAT').val(parseFloat(newTotal).toFixed(3));
                         var payStatusSelection = $("#PaymentStatus").val();
                         if (payStatusSelection == 1) {
                             document.getElementById("amountPaid").value = parseFloat(newTotal).toFixed(3);
                             }
                             }
                             function applyZeroVAT() {
                                 var vat = parseFloat($('#vatInput').val());
                                 var payStatusSelection = $("#PaymentStatus").val();
                                 if (vat == "" || isNaN(vat)) {
                                     //$('#vatInput').val(0);
                                     vat = 0;
                                     $('#vatAmount').val(0);
                                     $('#finalAmountWithVAT').val($("#totalAmount").val());
                                     if (payStatusSelection == 1) {
                                         document.getElementById("amountPaid").value = parseFloat($("#totalAmount").val()).toFixed(3);
        }
        }
        }
                             function applyDiscount() {
                                 var discount = $('#discInput').val();
                                 var subTotal = $("#subAmount").val();
                                 if (discount == "" || isNaN(discount)) {
                                     //$('#discInput').val(0);
                                     discount = 0;
                                     $('#totalAmount').val(subTotal);
                                 }
                                 else if (parseFloat(discount) >= parseFloat(subTotal)) {
                                     toastr.warning('Discount must less than total!')
                                     $('#discInput').val(0);
                                     $('#totalAmount').val(subTotal);
                                 }
                                 else {
                                     var totalAmount = parseFloat(subTotal) - parseFloat(discount);
                                     $('#totalAmount').val(parseFloat(totalAmount).toFixed(3));
                                 }
                             }
    // calculate balance if amountPaid entered and make changes every where needed
                             function calcAmountPaid() {
                                 isLimitExceed = false;
                                 var amountPaid = $("#amountPaid").val();
                                 var finalAmount = $('#finalAmountWithVAT').val();
                                 var payStatusSelection = $("#PaymentStatus").val();
                                 if (payStatusSelection == 2) {
                                     if (parseFloat(amountPaid) >= parseFloat(finalAmount)) {
                                         toastr.warning('Partial ! AmountPaid should be less than total amount ')
                                         $("#amountPaid").val(0)
                                     }
                                     else {

                                         var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
                                         $("#balanceAmount").val(parseFloat(balance).toFixed(3));
                                     }
                                 }
                                 else {

                                     var balance = parseFloat(finalAmount) - parseFloat(amountPaid);
                                     $("#balanceAmount").val(parseFloat(balance).toFixed(3));
                                 }
                             }
    // in case of vat entered please do calculations
                             function calcVAT() {
                                 var vat = parseFloat($('#vatInput').val());
                                 var payStatusSelection = $("#PaymentStatus").val();
                                 if (vat == "" || isNaN(vat)) { applyZeroVAT(); }
                                 else {
                                     var totalInvoice = $("#totalAmount").val();
                                     var vatAmount = parseFloat((vat * totalInvoice) / 100);
                                     $('#vatAmount').val(parseFloat(vatAmount).toFixed(3));
                                     var finalAmount = parseFloat(vatAmount) + parseFloat(totalInvoice);
                                     $('#finalAmountWithVAT').val(finalAmount);
                                     if (payStatusSelection == 1) {
                                         document.getElementById("amountPaid").value = parseFloat(finalAmount).toFixed(3);
                                     }
                                 }
                             }
    // if Vat then calculate and make effects on total amount
    $('#vatInput').on('input', function (e) {
    getNewTotal();
});
    // if Discount then calculate and make effects on total amount
    $('#discInput').on('input', function (e) {
    getNewTotal();
    });
$("#amountPaid").on("change", function () {
    calcAmountPaid();
});
    // Add data to array and send it to controller for order creation
function insert() {
    var saleDetails = [];
    var total = 0;
    var qtyVE = 0;
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var BranchId = $('#hdnBranchId').val();
    
    invoiceAmount = $('#subAmount').val();
    var paidAmount = $('#amountPaid').val();
    var vat = parseFloat($('#vatAmount').val());
    if (vat == "" || isNaN(vat)) {
        vat = 0;
        $('#vatInput').val(0);
    }
    //  get Table DAta 
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = $row.find('input[id*="productID"]').val();
        var partNumber = $row.find("td").eq(2).text();
        var Location = $row.find("#Location").text();
        var Qty = $row.find("#ProductQty").text();
        var SalePrice = $row.find("#ProductSalePrice").text();
        var SubTotal = $row.find("#ProductSubTotal").text();

        saleDetails.push({
            PartNo: partNumber,
            ProductId: pId,
            Location: Location,
            Qty: Qty,
            IsReturned: false,
            ReturnedQty: 0,
            SalePrice: SalePrice,
            Total: SubTotal,
            BranchID : BranchId
        });

    });
    if (saleDetails.length) {
        var paymentStatus = 0;
        if ($('#PaymentStatus option:selected').val() != 0) {
            paymentStatus = $('#PaymentStatus option:selected').text()
        }
        var SaleAmount = parseFloat(total + vat).toFixed(3);
        var data = {
            'AccountID': $('#hdnAccountID').val(),
            'SOID': $("#hdnSOID").val(),
            'BranchID': BranchId,
            'PONO': $('#PONO').val(),
            'VehicleNo': $('#VehicleNo').val(),
            'SalesDate': $('#SaleDate').val(),
            'PaymentStatus': paymentStatus,
            'PaymentTypeID': $('#PaymentType option:selected').val(),
            'VAT': vat,
            'TotalAmount': invoiceAmount,
            'DiscountAmount': $('#discInput').val(),
            'AmountPaid': $('#amountPaid').val(),
            'TotalPaid': $('#amountPaid').val(),
            'ChequeDate': $("#chqDate").val(),
            'ChequeNo': $("#chqNumber").val(),
            'BankName': bank,
            'SaleDetails': saleDetails
        };
        var json = JSON.stringify({ 'model': data, 'bankAccId': bankAccountId });
        //console.log(json);       
        ajaxCall("POST", json, "application/json; charset=utf-8", "/DeliveryOrder/CreateInvoice", "json", onSuccess, onFailure);        
        function onSuccess(Result) {
            if (Result > 0) {
                uiUnBlock();
                window.open('/SalesOrder/Details?id=' + parseInt(Result) + '');                
                window.location.href = '/SalesOrder';
            }
            else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                alert("Some error Ocurred! Please Check Your Entries");
            }
        }
        function onFailure(error) {
            console.log("Result " + Result);
            if (error.statusText == "OK") {
                uiUnblock();
                window.open('/SalesOrder/Details?id=' + parseInt(Result) + '');
                window.location.href = '/SalesOrder';
                console.log(error);
            }
            else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                alert("Some error Occured check your entries!");
            }
        }
    }
    else
    {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);        
    }
}
        
 
