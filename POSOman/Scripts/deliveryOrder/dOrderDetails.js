﻿$tableItemCounter = 0;
$addedProductIDs = [];
$selCustID = 0;
$(document).ready(function () {
    // Self Generated QuotationID  
    getDOID();    
});
function getDOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/DeliveryOrder/getLastDOID',
        async: true,
        success: function (data) {
            {
                var value = data;
                var newValue = value.split('-');
                var increase = newValue[1];
                increase++;
                value = "DO-" + increase;
                $("#DOID").val(value);                
            }
        },
        error: function (err) { console.log(err); }
    });
}
$('#Qty').on('input', function (e) {
    calcSubTotal();
});
$('#SalePrice').on('input', function (e) {    
    calcSubTotal();
});
$('#SalePrice').change(function () {
    var price = parseFloat($('#SalePrice').val()) || 0;
    var costPrice = parseFloat($('#hdnCostPrice').val()) || 0;
    if (price < costPrice) {
        swal("Error", "The sale price is below cost price of this item! " + costPrice + "OMR", "error");
    }    
});
$('#Qty').change(function () {
    //var qty = parseInt($('#Qty').val()) || 0;
    var stock = ($('#Stock').val()) || 0;
    if (stock < 1 || stock == "") {
        swal("No Stock", "Stock Not Available! ", "error").then(function () {
            $("#SalePrice").focus();
        });;
    }    
});
function calcSubTotal() {
    var qty = $('#Qty').val();
    var price = $('#SalePrice').val();
    if (($.isNumeric(price)) && ($.isNumeric(qty))) {
        var amount = (qty * price);
        $('#SubTotal').val(parseFloat(amount).toFixed(3));
        calcTotal();
    }
    else {
        $('#subAmount').val(parseFloat(amount).toFixed(3));
    }
}
$("#ddlVehCode").change(function () {
    var vehCodeID = $('#ddlVehCode').val();
   // getProducts(vehCodeID);
});
$("#AccountID").change(function () {
    var customerID = $('#AccountID').val();    
    $('#hdnAccountID').val(customerID);
    $('#CustomerPhone').val(customerID).trigger('change.select2');
    $('#CustomerCode').val(customerID).trigger('change.select2');
    //$('#CustomerPhone').val(customerID);
    //$('#CustomerPhone').trigger('change');
    //$('#CustomerCode').val(customerID);
    //$('#CustomerCode').trigger('change');    
    getCustomerDetail(customerID);
});
$("#CustomerCode").change(function () {
    var customerID = $('#CustomerCode').val();
    $('#hdnAccountID').val(customerID);    
    $('#AccountID').val(customerID).trigger('change.select2');
    $('#CustomerPhone').val(customerID).trigger('change.select2');
    getCustomerDetail(customerID);
});
$("#CustomerPhone").change(function () {
    var customerID = $('#CustomerPhone').val();
    $('#hdnAccountID').val(customerID);
    $('#AccountID').val(customerID).trigger('change.select2');
    $('#CustomerCode').val(customerID).trigger('change.select2');
    getCustomerDetail(customerID);
});
$("#ddlPartNumber").change(function () {
    var pId = $('#ddlPartNumber').val();
    getDetail(pId);
});

$("#tblProduct").focusout(function () {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);        
        var qty = $row.find("#ProductQty").text();        
        var price = $row.find("#ProductSalePrice").text();       
        if (($.isNumeric(price)) && ($.isNumeric(qty))) {
            var amount = (qty * price);
            $row.find("#ProductSubTotal").text(parseFloat(amount).toFixed(3));            
            var subTotal = parseFloat($row.find('#ProductSubTotal').text());
            total += parseFloat(subTotal);
            calcTotal();
        }
    });       
});

function calcTotal() {
    var total = 0;
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);        
        var subTotal = parseFloat($row.find('#ProductSubTotal').text());
        total += parseFloat(subTotal);        
    });

    $("#subAmount").val(parseFloat(total).toFixed(3));
    $("#totalAmount").val(parseFloat(total).toFixed(3));
    $("#finalAmountWithVAT").val(parseFloat(total).toFixed(3));

    var payStatusSelection = $("#PaymentStatus").val();
    if (payStatusSelection == 1) {
        document.getElementById("amountPaid").value = parseFloat(total).toFixed(3);
    }
    var creditLimit = parseFloat($('#customerCrLimit').val()) - parseFloat($('#customerBalance').val());
    if (creditLimit != "" && total > creditLimit)
    { swal("Credit Limit", "You can not sale more than Credit Limit! ", "error"); }
}
$("#addRow").click(function () {
    var vehCode = $("#ddlVehCode :selected").text();  // 1
    var ProductID = $("#ddlPartNumber").val(); // hidden
    var PartNO = $("#ddlPartNumber :selected").text();  // 1
    var Description = $("#Description").text(); // 2
    var UnitCode = $("#UnitCode").text(); // 3
    var Location = $("#Location").text(); // 4 
    var Qty = $("#Qty").val(); // 5   
    var SalePrice = $("#SalePrice").val(); // 7
    var SubTotal = $("#SubTotal").val(); // 8
    var costPrice = parseFloat($('#hdnCostPrice').val()) || 0;
    var pid = '<input type="hidden" id="productID" value="' + ProductID + '"/>';
    var cPrice = '<input type="hidden" id="costPrice" value="' + costPrice + '"/>';
    //console.log(costPrice);
    if (ProductID > 0 && (parseInt(Qty)) > 0 && Number(parseFloat(SalePrice)) && SubTotal > 0) {
        var index = $.inArray(ProductID, $addedProductIDs);    
        if (index >= 0) {
            swal("Error", "Product Already Added!", "error");
        } else {            
            var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td>" + Description + "</td><td id=Location>" + cPrice + Location + "</td><td>" + UnitCode + "</td><td contenteditable='true' id='ProductQty'>" + Qty + "</td><td contenteditable='true' id='ProductSalePrice'>" + SalePrice + "</td><td id='ProductSubTotal'>" + SubTotal + "</td></tr>";
            $("#tblProduct tbody").append(markup);            
            $tableItemCounter++;            
            $addedProductIDs.push(ProductID);
            clearFields();
            calcTotal();
        }
    }
    else if (ProductID == "" || ProductID == "undefined") { swal("Error", "Please Select Product!", "error"); }
    else if (Qty == "" || Qty == "undefined" || !Number.isInteger(parseInt(Qty))) { swal("Error", "Please enter Quantity!", "error"); }
    else if (SalePrice == "" || SalePrice == "undefined" || !Number(parseFloat(SalePrice))) { swal("Error", "Please enter Sale Price!", "error"); }
});
// Find and remove selected table rows   
function remove(input) {    
    $("table tbody").find('input[name="record"]').each(function () {        
        if ($(this).is(":checked")) {            
            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            var productID = row.find('input[id*="productID"]').val(); // find hidden id 
            var index = $.inArray(productID, $addedProductIDs);
            if (index >= 0) { $addedProductIDs.splice(index, 1); }
        }
    });
}
function clearFields() {
    $("#Description").text("");
    $("#UnitCode").text("");
    $("#Location").text("");
    $("#Qty").val("");        
    $("#SalePrice").val("");
    $("#SubTotal").val("");    
    $("#ddlVehCode").focus();
    $('#ddlVehCode').val(null).trigger('change');
    $('#ddlPartNumber').val(null).trigger('change.select2');
}
function getProducts(vehCodeID) {    
    if (vehCodeID == "")
    { vehCodeID = -1;  }
    var json = { "vehCodeId": vehCodeID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Product/getProducts',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("ddlPartNumber", data, true);
        },
        error: function (err) { console.log(err); }
    });
}
// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

//  Get all added Products from table
function getTableData() {
    $('#tblProduct tbody tr').each(function (i, n) {

        var $row = $(n);
        var pId = $row.find('input[id*="productID"]').val();
        var partNumber = $row.find("td").eq(2).text();                  
        var Location = $row.find("#Location").text();
        var Qty = $row.find("#ProductQty").text();        
        var SalePrice = $row.find("#ProductSalePrice").text();
        var SubTotal = $row.find("td").eq(8).text(); // 8
        var SubTotal = $row.find("#ProductSubTotal").text();         
    });
}
// Get Product's Details 
function getDetail(pId) {
    var accID = $('#hdnAccountID').val();
    // get product description
    var json = { "productId": pId };    
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Purchase/getProductDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {            
            if (data.Description) { $('#DescriptionTitle').text("Desc: "); $('#Description').text(data.Description); }
            if (data.SubstituteNo) { $('#SubsTitle').text("Su No.: "); $('#SubstituteNo').text(data.SubstituteNo); }
            if (data.GroupName) { $('#GroupTitle').text("Group: "); $('#Group').text(data.GroupName); }
            if (data.VehicleName) { $('#VehModelTitle').text("Model: "); $('#VehModel').text(data.VehicleName); }
            if (data.Location) { $('#LocationTitle').text("Location: "); $('#Location').text(data.Location); }
            if (data.UnitCode) { $('#UnitCodeTitle').text("Un Code: "); $('#UnitCode').text(data.UnitCode); }
            if (data.VehicleCode) {
                var vc = data.VehicleCode;
                $("#ddlVehCode option:contains(" + data.VehicleCode + ")").attr("selected", true);
                //$('#ddlVehCode').val(vc).trigger('change.select2');
            }
        },
        error: function (err) { console.log(err); }
    });
    if (accID == "" || typeof accID == undefined)
    {
        accID = 0;
    }
    var json = { "productId": pId, "accountID": accID };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/getLastSalePrice',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {            
            $('#SalePrice').attr('placeholder', data);
        },
        error: function (err) { console.log(err); }
    });
    //var BranchID = $('#BranchID option:selected').val();
    var BranchID = $('#hdnBranchId').val();
    var json = { "productId": pId, "BranchID": BranchID };
    {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Stock/getStock',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {                
                $('#Stock').val(data[0].stock);
                $('#hdnCostPrice').val(data[0].costPrice);                
            },
            error: function (err) { console.log(err); }
        });
    }
   
}
