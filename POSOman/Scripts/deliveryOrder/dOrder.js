﻿var customerID = 0;
var accID = 0;
var isPONOExist = false;
var SOID = 0;
var isLimitExceed = false;

$("#PONO").on("change", function () {
    var accID = $('#hdnAccountID').val();
    if (accID == "" || accID == 0) {
        swal("Customer", "Please Select Customer First!", "error");
    }
    else {
        var PONO = $("#PONO").val();
        checkPONO(PONO, accID);
    }
});
function checkPONO(PONO, accID) {

    var json = { "accountID": accID, "PONO": PONO };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/DeliveryOrder/checkPONO',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            if (data == true) {
                toastr.warning('PONo already Exists for selected Customer!')
                $("#PONO").val("");
            }
        },
        error: function (err) { console.log(err); }
    });
}

$('#btnSubmit').click(function () {    
    accID = $('#hdnAccountID').val();
    var isValid = true;
    var PONo = $('#PONO').val();
    var SalesDate = $('#SaleDate').val();    
    var rowCount = $('#tblProduct tbody tr').length;
    var invoiceAmount = $('#subAmount').val();
    var creditLimit = $('#customerCrLimit').val();
    //var BranchID = $('#BranchID option:selected').val();  
    var BranchID = $('#hdnBranchId').val();
    var customerBalance = $('#customerBalance').val();
    if (PONo != "") {
        // check duplication
    }
    if (accID == "" || typeof accID == undefined || accID == 0) {
        isValid = false;
        swal("Customer", "Please Select Customer!", "error");
    }
    else if (SalesDate == "") {
        isValid = false;
        swal("Date", "Please Enter Sales Date!", "error");
        //console.log(PurchaseDate + "date");
    }
    else if (rowCount == 0 || rowCount < 0) {
        isValid = false;
        swal("Product", "Please Enter Products!", "error");        
    }    
    //else if (isLimitExceed == true) {
    //    swal("Credit Limit", "You can not sale more than Credit Limit! ", "error");
    //}
    else if (parseFloat(invoiceAmount) > parseFloat(creditLimit - customerBalance)) {
        swal("Credit Limit", "You can not make order more than Credit Limit! ", "error");
    }
    else if (isValid == true && isLimitExceed == false) {
        $('#btnSubmit').prop('disabled', true);
        uiBlock();
        insert();
    }
});

// Get Customer Balance and Credit Limit 
function getCustomerDetail(accountID) {
    if (accountID == "")
    { accountID = -1; }
    var json = {
        "accountID": accountID
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/Customer/getDetail',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            $('#customerCrLimit').val(data.creditLimit);
            $('#customerBalance').val(data.Balance);
        },
        error: function (err) {
            console.log(err);
        }
    });
}

// Add data to array and send it to controller for order creation
function insert() {
    var saleDetails = [];
    var stockLog = [];
    var total = 0;
    var qtyVE = 0;
    invoiceAmount = $('#subAmount').val();
    var BranchId = $('#hdnBranchId').val();
    //console.log(BranchId +"  Branch");
    //  get Table DAta     
    $('#tblProduct tbody tr').each(function (i, n) {
        var $row = $(n);
        var pId = $row.find('input[id*="productID"]').val();
        var costPrice = $row.find('input[id*="costPrice"]').val();
        var partNumber = $row.find("td").eq(2).text();
        var Location = $row.find("#Location").text();
        var Qty = $row.find("#ProductQty").text();
        var SalePrice = $row.find("#ProductSalePrice").text();
        var SubTotal = $row.find("#ProductSubTotal").text();
        
        saleDetails.push({
            PartNo: partNumber,
            ProductId: pId,
            Location: Location,
            UnitPrice: costPrice,
            Qty: Qty,
            IsReturned: false,
            ReturnedQty: 0,
            SalePrice: SalePrice,
            Total: SubTotal,
            BranchID: BranchId
        });
        stockLog.push({
            AccountID: $('#hdnAccountID').val(),
            ProductId: pId,
            StockOut: Qty,
            SalePrice: SalePrice,
            CostPrice: costPrice,
            Location: Location,
            OutReference: 'Sales',
            OrderTypeID: 3,
            UserReferenceID: $('#PONO').val(),
            BranchID: BranchId,
            InvoiceDate: $('#SaleDate').val()
        });

    });
    if (saleDetails.length) {                 
        var data = {
            'AccountID': $('#hdnAccountID').val(),            
            'DOID': $("#DOID").val(),
            'BranchID': BranchId,
            'VehicleNo': $("#VehicleNo").val(),
            'PONO': $('#PONO').val(),            
            'SalesDate': $('#SaleDate').val(),            
            'TotalAmount': invoiceAmount,            
            'DODetails': saleDetails
        };
        var json = JSON.stringify({
            'model': data, 'modelStockLog': stockLog
        });
        //console.log(json);
        ajaxCall("POST", json, "application/json; charset=utf-8", "/DeliveryOrder/SaveOrder", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result == "success") {
                uiUnBlock();
                window.location.href = '/DeliveryOrder';                
            }
            else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);                
                alert("Some error Ocurred! Please Check Your Entries");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                console.log("OK");
            }
            else {
                $('#btnSubmit').prop('disabled', false);
                alert("Some error Occured check your entries!");
            }
        }
    }
}
