﻿var AccountID = 0;
var BranchID = 0;

$('#btnSubmit').click(function () {
    if (confirm('Are you sure you want to delete this DO ?')) {
        insert();
    }
    
});
function bindOrderData(data) {    
    customer = data.qry[0].AccountName;
    
    document.getElementById('customer').value = customer;
    if (data.qry[0].SalesDate != "") {
        var num = (data.qry[0].SalesDate).match(/\d+/g);
        var date = new Date(parseFloat(num));
    var dateString = date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear();}  
    document.getElementById('date').value = dateString;

}
$("#btnSearch").on("click", function () {
    
    var DONO = $('#DOID').val();
    var branchID = $('#Branch').val();
     if (DONO =="") {
        swal("Error", "Please Enter DO NO.", "error");
     }
     else if (branchID == "") {
         swal("Error", "Please Select Branch", "error");
     }
    else{getInvoiceDetails(DONO, branchID);}
    
});
function getInvoiceDetails(DOID,BranchID) {
    ajaxCall("GET", { "DOID": DOID , "BranchID":BranchID  }, "application/json; charset=utf-8", "/CancelOrders/getDODetails",
        "json", onSuccess, onFailure);

    function onSuccess(data) {
        var ien = data.qry.length;
        $('#tblReturn').find('tbody').empty();
        if (ien > 0) {
            bindOrderData(data);
            var html = '';
            for (var i = 0; i < ien; i++) {
                AccountID = data.qry[i].AccountID;                
                BranchID = data.qry[i].BranchID;                
                html += '<tr>';
                html += '<td id=description>' + data.qry[i].BranchName + '</td>';
                html += '<td><input id="orderID" type="hidden" value="' + data.qry[i].OrderID + '">' + data.qry[i].DOID + '</td>';
                html += '<td><input id="prodID" type="hidden" value="' + data.qry[i].ProductID + '">' + data.qry[i].PartNo + '</td>';
                html += '<td id=description>' + data.qry[i].Description + '</td>';
                html += '<td id=SaleQty>' + data.qry[i].Qty + '</td>';
                html += '<td id=SalePrice>' + data.qry[i].SalePrice + '</td>';
                html += '<td>' + data.qry[i].Total + '</td>';                                
                html += '</tr>';
            }
            $('#tblReturn tbody').append(html);
        }
        else {
            swal("Error", "Not Found!!!", "error");
        }
    }
    function onFailure(err) {
        console.log(err);
    }
}

function insert() {

    var doReturn = [];
    var stockLog = [];
    var total = 0;
    var qtyVE = 0;
    var isValid = false;      
    var saleOrderID = 0;    
    var SOID = 0;
    var branchID = $('#Branch').val();
    $('#tblReturn tbody tr').each(function (i, n) {
        var $row = $(n);
        DeliveryOrderID = $row.find("input[id*='orderID']").val();        
        BranchID = $row.find("td:eq(0)").text();
        DOID = $row.find("td:eq(1)").text();
        var pId = $row.find('input[id*="prodID"]').val();        
        var qty = $row.find("#SaleQty").text();
        var price = $row.find("#SalePrice").text();
        var subTotal = $row.find('input[id*="txtAmount"]').val();        
        {
            isValid = true;
            doReturn.push({
                ProductId: pId,
                Qty: qty,
                SalePrice: price,                
                Total: subTotal,
                BranchID: branchID,
            });           
        }       
    });
    if (doReturn.length && isValid == true) {
        var data = {
            'AccountID': AccountID,            
            'BranchID': branchID,
            'DeliveryOrderID': DeliveryOrderID,            
            'DOReturnDetails': doReturn
        };
        var json = JSON.stringify({ 'model': data });
        //console.log(json);
        ajaxCall("POST", json, "application/json; charset=utf-8", "/CancelOrders/DeleteOrder", "json", onSuccess, onFailure);

        function onSuccess(Result) {
            if (parseInt(Result) > 0) {
                uiUnBlock();                
                window.location.href = '/CancelOrders/DOCancel';
            }
            else {
                uiUnBlock();
                $('#btnSave').prop('disabled', false);
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                window.location.href = '/CancelOrders/DOCancel';
                console.log(error.statusText);
            }
            else {
                uiUnBlock();
                $('#btnSave').prop('disabled', false);
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }
    else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);
        swal("DO", "Please Enter DONO!", "error");
    }
}