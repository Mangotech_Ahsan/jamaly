﻿var chqDat = "";
$(function () {
    //document.getElementById("Bank").readOnly = true;
    $("#Bank").prop("disabled", true);
    document.getElementById("ChequeDate").readOnly = true;
    document.getElementById("ChequeNumber").readOnly = true;
    document.getElementById("ChequeDate").readOnly = true;
    $('#btnSave').click(function () {
        var expenseTypeID = $('#AccountID option:selected').val();
        var voucherDate = $('#VoucherDate').val();
        var amount = $('#Amount').val();
        var chqNumber = $('#ChequeNumber').val();        
        var bankAccountId = $('#Bank option:selected').val();
        chqDate = $('#ChequeDate').val();
        var PaymentTypeID = $('#PaymentType option:selected').val();
        //if (chqDate == "") {
        //    //alert("Please cheque date");
        //    chqDat = "";
        //    //console.log(PurchaseDate+"date");
        //}
        if (expenseTypeID == "" || expenseTypeID == 0) {
            swal("Expense", "Please Select Expense Type!", "error");
        }
        else if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if (amount == "" || amount == 0) {
            swal("Amount", "Please Enter Amount!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if ((PaymentTypeID == 3) && (chqNumber == "")) {
            isValid = false;
            swal("Cheque Number", "Please Enter Cheque Number!", "error");
        }
        else if ((PaymentTypeID == 3) && (chqDate == "")) {
            isValid = false;
            swal("Cheque Date", "Please Enter Cheque Date!", "error");
        }
        else {
        $('#btnSave').prop('disabled', true);
        uiBlock();
            insert();
        }
    });

});
$("#PayTypeID").on("change", function () {
    var payStatusSelection = $("#PayTypeID").val();
    var totalAmount = $("#TotalAmount").val();
    if (payStatusSelection == 1) {
        //document.getElementById("Bank").readOnly = true;     
        $("#Bank").prop("disabled", true);
        document.getElementById("ChequeNumber").readOnly = true;
        document.getElementById("ChequeDate").readOnly = true;
        chqDate = "";
    }
    else if (payStatusSelection == 2) {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeNumber").readOnly = true;
        document.getElementById("ChequeDate").readOnly = true;
        chqDate = "";
    }
    else if (payStatusSelection == 3) {
        //document.getElementById("Bank").readOnly = false;        
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeNumber").readOnly = false;
        document.getElementById("ChequeDate").readOnly = false;
        chqDate = $('#ChequeDate').val();
    }
});

function insert() {    
    var payLog = "";
    var totalAmount = 0;          
    var expenseTypeID = $('#AccountID option:selected').val();
    var DepartmentID = +$('#DepartmentID').val()||null;
    var BranchID = $('#hdnBranchId').val();
    var desc = $('#Description').val();
    var payType = $('#PayTypeID option:selected').val()
    //var bank = $('#Bank').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var voucherDate = $('#VoucherDate').val();
    //var chqDate = $('#ChequeDate').val();
    var chqNumber = $('#ChequeNumber').val();
    var amount = parseFloat($('#Amount').val());
    //if (totalPaying == amount)
     
            var data = {
                'BranchID': BranchID,
                'DepartmentID': DepartmentID,
                'Description': desc,
                'Amount': amount,
                'AccountID': expenseTypeID,
                'VoucherDate': voucherDate,
                'PaymentTypeID': payType,
                'BankName': bank,
                'ChequeDate': chqDate,
                'ChequeNumber': chqNumber,
                'SaleOrderID': $("#SaleOrderID").val() || 0
            };

            payLog = {
                'BranchID' : BranchID,
                'Paid': amount,
                'Date': voucherDate,
                'JEntryTypeID': 7,  // Expenses
                'AddBy': 1,
                'IsActive': 1                
            };

            var json = JSON.stringify({ 'model': data, 'payLog': payLog, 'bankAccId': bankAccountId });         
            ajaxCall("POST", json, "application/json; charset=utf-8", "/Expenses/SaveExpense", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result > 0) {
                    uiUnBlock();
                    window.location.href = '/Expenses/Details?id=' + Result;
                }
                else {
                    uiUnBlock();
                    $('#btnSave').prop('disabled', false);                    
                    swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                }
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    window.location.reload();
            }
        
    
    
}
