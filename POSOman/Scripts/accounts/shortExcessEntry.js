﻿var chqDat = "";
$(function () {    
    $('#btnSave').click(function () {
        var entryId = $('#AccountID option:selected').val();
        var voucherDate = $('#VoucherDate').val();
        var amount = $('#Amount').val();
        
        
        if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if (amount == "" || amount == 0) {
            swal("Amount", "Please Enter Amount!", "error");
            //console.log(PurchaseDate + "date");
        }        
        else {
        $('#btnSave').prop('disabled', true);
        uiBlock();
            insert();
        }
    });

});
function insert() {
    var payLog = "";
    var totalAmount = 0;
    var entryId = $('#AccountID option:selected').val();
    var BranchID = $('#hdnBranchId').val();
    var desc = $('#Description').val();    
    var amount = parseFloat($('#Amount').val());    
    var voucherDate = $('#VoucherDate').val();
    var data = {
        'BranchID': BranchID,
        'Description': desc,
        'Amount': amount,
        'AccountID': entryId,
        'VoucherDate': voucherDate        
    };    
    var json = JSON.stringify({ 'model': data });
    //console.log(json);
    ajaxCall("POST", json, "application/json; charset=utf-8", "/ShortExcess/SaveShortExcess", "json", onSuccess, onFailure);
    function onSuccess(Result) {
        if (Result == "success") {        
        uiUnBlock();
            window.location.href = '/ShortExcess';
        }
        else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);
            swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
        }
        //location.reload();
        //window.location.href = 'Index';
        //alert("success");
    }
    function onFailure(error) {
        if (error.statusText == "OK")
            //alert("success");
            //console.log(error);
            window.location.reload();
    }



}
