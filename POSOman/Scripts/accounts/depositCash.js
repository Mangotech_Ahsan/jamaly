﻿$(document).ready(function () {
    $('#btnSubmit').click(function () {
        var amount = $('#Amount').val();
        {            
        $('#btnSubmit').prop('disabled', true);
        uiBlock();
            insert();
        }
    });
    $("#totalDeposit").on("change", function () {
        calcAmountDeposit();
        var depositAmount = $("#totalDeposit").val();
        var totalCash = $('#totalCash').val();
        // Prevent Entering more than cash
        if (parseFloat(depositAmount) > parseFloat(totalCash)) {
            $("#totalDeposit").val(totalCash);
            $("#balanceAmount").val('0');
            toastr.warning('Returning quantity must be equal or less than balance qty!')
        }        
    });
});

// calculate balance if deposit
function calcAmountDeposit() {
    var depositAmount = $("#totalDeposit").val();
    var totalCash = $('#totalCash').val();
    var balance = parseFloat(totalCash) - parseFloat(depositAmount);
    $("#balanceAmount").val(parseFloat(balance).toFixed(3));
}
function insert() {
    var payLog = "";
    var totalAmount = 0;
    var bankAccountID = $('#Bank option:selected').val();
    
    var BranchID = $('#hdnBranchId').val();
    var desc = $('#Description').val();
    var amount = parseFloat($('#totalDeposit').val());
    if (amount > 0 ){var data = {
        'BranchID': BranchID,
        'Description': desc,
        'Amount': amount,
        'AccountID': bankAccountID
    };
    var json = JSON.stringify({ 'model': data });
    //console.log(json);
    ajaxCall("POST", json, "application/json; charset=utf-8", "/Reports/SaveCashDepositEntry", "json", onSuccess, onFailure);
    function onSuccess(Result) {
        if (Result == "success") {
        uiUnBlock();            
            window.location.href = '/Reports/GetBankStatement';
        }
        else {
        uiUnBlock();
        $('#btnSubmit').prop('disabled', false);        
            swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
        }
        //location.reload();
        //window.location.href = 'Index';
        //alert("success");
    }
    function onFailure(error) {
        if (error.statusText == "OK")
            //alert("success");
            //console.log(error);
            window.location.reload();
    }
    }
    else {
    uiUnBlock();
        $('#btnSubmit').prop('disabled', false);  
        swal("Amount", "Please Enter Deposit Amount!", "error");
    }
    
}
