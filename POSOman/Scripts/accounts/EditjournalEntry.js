﻿$tableItemCounter = 0;
$addedAccountIDs = [];
$selCustID = 0;


$("#BranchID").prop("disabled", false);

var vc = '';
///////////////////////multi entry date///////////////////////////////////////////////

$("#AssignEntryDate").click(function () {
    var EntryDate = new Date();//$("#EntryDate").val();

    var dr = parseFloat($("#drTotal").val()).toFixed(2);
    var cr = parseFloat($("#crTotal").val()).toFixed(2);


    if (EntryDate == null || EntryDate == "") {

        swal("Entry Date", "Please Enter Entry Date", "error");
    }
    else if (dr == null || cr == null || (dr <= 0 || cr <= 0) || (dr == "" || cr == "")) {

        swal("Debit/Credit null entry", "Please enter Debit/Credit Amount", "error");
    }
    else if (dr != cr) {

        swal("Debit/Credit unbalanced entry", "Please enter Debit/Credit balanced Amount", "error");
    }
    else {

        var Detail = [];
        $('#tblEntry tbody tr').each(function (i, n) {
            var $row = $(n);
            var accountId = $row.find('input[id*="accountId"]').val();
            var MultiEntryDate = $row.find("#MultiEntryDate").text();
            var memo = $row.find("td").eq(2).text();
            var debitAmount = $row.find("#debitAmount").text();
            var creditAmount = $row.find("#creditAmount").text();
            Detail.push({
                AccountID: accountId,
                Memo: memo,
                MultiEntryDate: MultiEntryDate,
                Dr: debitAmount,
                Cr: creditAmount,
                BranchID: null
            });
        });
        var data = {
            'BranchID': null,
            'EntryDate': EntryDate,
            'Amount': null,
            'Description': null,
            'VoucherName': null,
            'JDetail': Detail
        };
        var json = JSON.stringify({ 'model': data });


        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/JournalEntry/AssignEntryDate',
            //  async: true,
            data: json,
            success: function (data) {
                // alert("suc");
              //  console.log(data);
                $("#tblEntry tbody").empty();
                $tableItemCounter = 0;
                $addedAccountIDs = [];

                data.forEach(ListEntry);
            },
            error: function (err) { console.log(err); }
        });
        //console.log(json);


    }



});

function ListEntry(item) {
    //    var pid = '<input type="hidden" id="productID" value="' + item.ProductID + '"/>';
    //    var cPrice = '<input type="hidden" id="costPrice" value="' + item.cPrice + '"/>';
    //var markup = "<tr><td><input type='radio' name='record'></td><td>" + item.Cat + "</td><td>" + pid + "" + item.ArticleNo + "</td><td>" + item.Desc + "</td><td id=Location hidden>" + cPrice + Location + "</td><td contenteditable='false' id='ProductQty'>" + item.Qty + "</td></tr>";
    var dateString = item.MultiEntryDate.substr(6);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var date = day + "/" + month + "/" + year;
    console.log(item.options)
    //   var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + item.Cat + "</td><td>" + pid + "" + item.ArticleNo + "</td><td hidden>" + item.Desc + "</td><td id=Location hidden>" + cPrice + Location + "</td><td id='unitCode'>" + item.UnitCode + "</td><td contenteditable='true' id='ProductQty'>" + item.Qty + "</td><td contenteditable='true' id='ProductSalePrice' hidden>" + SalePrice + "</td><td id='ProductSubTotal' hidden>" + SubTotal + "</td></tr>";
    var accId = '<input type="hidden" id="accountId" value="' + item.accId + '"/>';
    var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + accId + "" + item.accountName + "</td><td >" + item.memo + "</td><td contenteditable='true' id='debitAmount'>" + item.drAmount + "</td><td contenteditable='true' id='creditAmount'>" + item.crAmount + "</td><td id = 'MultiEntryDate'>" + date + "</td></tr>";


    // var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + vehCode + "</td><td>" + pid + "" + PartNO + "</td><td>" + Description + "</td><td id=Location hidden>" + cPrice + Location + "</td><td contenteditable='true' id=PackUnit >" + TotalPack + "</td><td contenteditable='true' id='ProductQty'>" + Qty + "</td><td  id='ProductPrUntLnth'>" + PrUntLnth.toFixed(4) + "</td><td  id='ProductTotMeters'>" + parseFloat(TotMtrs).toFixed(2) + "</td><td contenteditable='true' id='ProductSalePrice'>" + SalePrice + "</td><td id='ProductSubTotal'>" + SubTotal + "</td></tr>";

    //$("#tblProduct tbody").append(markup);
    //$tableItemCounter++;
    //$addedProductIDs.push(item.ProductID);
    //proIdEdit = item.ProductID;





    $("#tblEntry tbody").append(markup);
    $tableItemCounter++;
    $addedAccountIDs.push(item.accId);
    clearFields();
    calcTotal();
    //index = $.inArray(proIdEdit, $addedProductIDs);
    //console.log(index + "Indexedit");
    //console.log(item.ProductID + "editPID");
    //console.log($tableItemCounter + "editcounter");
    //console.log($addedProductIDs + "editProductidarray");

    //calcTotal();
}

///////////////////////////////////////////////////////////////////////


$(document).ready(function () {
    var JEntryid = $('#JEntryID').val() || 0;

    var json = { "JEntryID": JEntryid };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/JournalEntry/getEditGJEntryData',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            console.log("->"+data[0].options);
           
            $("#Description").val(data[0].Description.Description);
            $("#VoucherName").val(data[0].Description.VoucherName);
          //  GetDropdown1("ddlAccount", data, true);
            data[0].JDetail.forEach(ListProduct);
          

        },
        error: function (err) { console.log(err); }
    });



   



});

//
function ListProduct(item) {
    
   // var accountType = $("#AccountTypeId :selected").text();  // 1
    var accountId = item.AccountID;//$("#ddlAccount").val(); // hidden
    var accountName = item.AccountName;//$("#ddlAccount :selected").text();  // 1
    var memo =item.Memo;// $("#Memo").val();
    var drAmount = item.Debit;//$("#debit").val();
    var crAmount = item.Credit;//$("#credit").val();
    if (item.MultiEntryDate != null) {
        var dateString = item.MultiEntryDate.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        var EntryDate = day + "/" + month + "/" + year;
    } else {
        var EntryDate = null;
    }
   
  
    $('#BranchID').val(item.BranchID).trigger('change.select2');
    
    var accId = '<input type="hidden" id="accountId" value="' + item.AccountID + '"/>';
    if (accountId > 0 && ((parseFloat(drAmount)) > 0 || parseFloat(crAmount) > 0)) {
        var index = $.inArray(accountId, $addedAccountIDs);
        if (index >= 0) {
            swal("Error", "Account Entry Already Added!", "error");
        } else {

            var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + accId + "" + accountName + "</td><td contenteditable='true'>" + memo + "</td><td contenteditable='true' id='debitAmount'>" + drAmount + "</td><td contenteditable='true' id='creditAmount'>" + crAmount + "</td><td id = 'MultiEntryDate' hidden>" + EntryDate + "</td></tr>";
            $("#tblEntry tbody").append(markup);
            $tableItemCounter++;
            $addedAccountIDs.push(accountId);
           // console.log($addedAccountIDs);
            clearFields();
            calcTotal();
        }
    }
    else if (accountId == "" || accountId == "undefined") { swal("Error", "Please Select Account!", "error"); }
    else if ((drAmount == "" || drAmount == "undefined" || !Number.isInteger(parseFloat(drAmount))) || (crAmount == "" || crAmount == "undefined" || !Number.isInteger(parseFloat(crAmount)))) { swal("Error", "Please enter Debit or Credit Amount!", "error"); }

    //$("#BranchID").val(item.BranchID).trigger();
}

//

$('#debit').change(function () {
    $('#credit').val('0');
});
$('#credit').change(function () {
    $('#debit').val('0');
});

$("#AccountTypeId").change(function () {
    var accountTypeId = $('#AccountTypeId').val();
    getAccounts(accountTypeId);
});

$("#tblEntry").focusout(function () {
    var total = 0;
    $('#tblEntry tbody tr').each(function (i, n) {
        var $row = $(n);
        var dr = $row.find("#debitAmount").text();
        var cr = $row.find("#creditAmount").text();
        if (($.isNumeric(dr)) && ($.isNumeric(cr))) {
            calcTotal();
        }
    });
});






function calcTotal() {
    var drTotal = 0;
    var crTotal = 0;
    $('#tblEntry tbody tr').each(function (i, n) {
        var $row = $(n);
        var drAmount = parseFloat($row.find('#debitAmount').text());
        var crAmount = parseFloat($row.find('#creditAmount').text());
        drTotal = parseFloat(drTotal + drAmount);
        crTotal = parseFloat(crTotal + crAmount);
    });
    $("#drTotal").val(parseFloat(drTotal).toFixed(3));
    $("#crTotal").val(parseFloat(crTotal).toFixed(3));
}
$("#addRow").click(function () {
    var accountType = $("#AccountTypeId :selected").text();  // 1
    var accountId = $("#ddlAccount").val(); // hidden
    var accountName = $("#ddlAccount :selected").text();  // 1
    var memo = $("#Description").val();//$("#Memo").val();
    var drAmount = +$("#debit").val() || 0;
    var crAmount = +$("#credit").val() || 0;
    var EntryDate = null;
    var accId = '<input type="hidden" id="accountId" value="' + accountId + '"/>';
    if (accountId > 0 && ((parseFloat(drAmount)) > 0 || parseFloat(crAmount) > 0)) {
        //var index = $.inArray(accountId, $addedAccountIDs);
        //if (index >= 0) {
        //    swal("Error", "Account Entry Already Added!", "error");
        //} else {



            //  var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + accId + "" + accountName + "</td><td contenteditable='true'>" + memo + "</td><td contenteditable='true' id='debitAmount'>" + drAmount + "</td><td contenteditable='true' id='creditAmount'>" + crAmount + "</td></tr>";
            var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + accId + "" + accountName + "</td><td >" + memo + "</td><td contenteditable='true' id='debitAmount'>" + drAmount + "</td><td contenteditable='true' id='creditAmount'>" + crAmount + "</td><td id = 'MultiEntryDate' hidden>" + EntryDate + "</td></tr>";

            $("#tblEntry tbody").append(markup);
            $tableItemCounter++;
            $addedAccountIDs.push(accountId);
            console.log($addedAccountIDs);
            clearFields();
            calcTotal();
      //  }
    }
    else if (accountId == "" || accountId == "undefined") { swal("Error", "Please Select Account!", "error"); }
    //else if ((drAmount == "" || drAmount == "undefined" || !Number.isInteger(parseFloat(drAmount))) || (crAmount == "" || crAmount == "undefined" || !Number.isInteger(parseFloat(crAmount)))) { swal("Error", "Please enter Debit or Credit Amount!", "error"); }
    else if (drAmount < 0 || crAmount < 0) { swal("Error", "Please enter Debit or Credit Amount!", "error"); }
});
// Find and remove selected table rows   
function remove(input) {
    $("table tbody").find('input[name="record"]').each(function () {
        if ($(this).is(":checked")) {
            $(this).parents("tr").remove();
            $tableItemCounter--;
            var row = $(this).closest("tr");
            var accountId = row.find('input[id*="accountId"]').val(); // find hidden id 
            var index = $.inArray(accountId, $addedAccountIDs);
            if (index >= 0) { $addedAccountIDs.splice(index, 1); }
            calcTotal();
        }
    });
}

function clearFields() {
    $("#Memo").val("");
    $("#debit").val("");
    $("#credit").val("");
    $("#AccountTypeID").focus();
    $('#AccountTypeID').val(null).trigger('change');
    $('#ddlAccount').val(null).trigger('change.select2');
}
function getAccounts(accountTypeId) {

    if (accountTypeId == "" || accountTypeId == 0)
    { accountTypeId = -1; }
    var json = { "accountTypeId": accountTypeId };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/NewAccount/getAccounts',
        async: true,
        data: JSON.stringify(json),
        success: function (data) {
            GetDropdown1("ddlAccount", data, true);
        },
        error: function (err) { console.log(err); }
    });
}
// Only allow int 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
var customerID = 0;
var accID = 0;
var isPONOExist = false;
var isQuote = false;
var SOID = 0;
var isLimitExceed = false;
//getentryID();
$("#Bank").prop("disabled", true);
//  Get New entry 
function getentryID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/getNewSOID',
        async: true,
        success: function (data) {
            {
                $("#hdnSOID").val(data);
                SOID = data;
            }
        },
        error: function (err) { console.log(err); }
    });
}

$('#btnSubmit').click(function () {
    var isValid = true;
    var VoucherName = $("#VoucherName").val();
    var totalDr = $("#drTotal").val();
    var totalCr = $("#crTotal").val();
    var rowCount = $('#tblEntry tbody tr').length;
    var voucherDate = $('#VoucherDate').val();
    var BranchID = $('#BranchID').val();
    if (VoucherName == "" || VoucherName == null) {
        isValid = false;
        swal("Voucher Name", "Please Enter Voucher Name!", "error");
    }
    if (voucherDate == "") {
        isValid = false;
        swal("Date", "Please Enter Date!", "error");
    }
    else if (parseFloat(totalDr) != parseFloat(totalCr)) {
        isValid = false;
        swal("Total", "Total Debit And Total Credit Should Be Equal!", "error");
    }
    else if (BranchID == null || BranchID <= 0 || BranchID == '') {
        isValid = false;
        swal("Project", "Please Select Project For This Entry!", "error");
    }
    else if (rowCount == 0 || rowCount < 0) {
        isValid = false;
        swal("Entries", "Please Enter Some Entries!", "error");
    }
    else if (isValid == true) {
        $('#btnSubmit').prop('disabled', true);
        uiBlock();
        insert();
    }
});

// Add data to array and send it to controller for order creation
function insert() {
    var entryDetails = [];
    var total = 0;
    var qtyVE = 0;
    var VoucherName = $("#VoucherName").val();
    var JEntryID = $('#JEntryID').val();
    var voucherDate = $('#VoucherDate').val();
    var Description = $('#Description').val();
    var BranchId = $('#BranchID').val();//$('#hdnBranchId').val();
    //  get Table DAta 
    $('#tblEntry tbody tr').each(function (i, n) {
        var $row = $(n);
        var accountId = $row.find('input[id*="accountId"]').val();
        var memo = $row.find("td").eq(2).text();
        var MultiEntryDate = $row.find("#MultiEntryDate").text();
        var debitAmount = $row.find("#debitAmount").text();
        var creditAmount = $row.find("#creditAmount").text();
        entryDetails.push({
            AccountID: accountId,
            Memo: Description,  //memo,
            MultiEntryDate: MultiEntryDate,
            Dr: debitAmount,
            Cr: creditAmount,
            BranchID: BranchId
        });
    });
    if (entryDetails.length) {
        var totalDebit = parseFloat($('#drTotal').val()).toFixed(3);
        var totalCredit = parseFloat($('#crTotal').val()).toFixed(3);
        var data = {
            'BranchID': BranchId,
            'VoucherDate': voucherDate,
            'Amount': totalCredit,
            'Description': Description,
            'JEntryId': JEntryID,
            'VoucherName': VoucherName,
            'JDetail': entryDetails
        };
        var json = JSON.stringify({ 'model': data });
        //console.log(json);
        ajaxCall("POST", json, "application/json; charset=utf-8", "/JournalEntry/SaveEditGJEntry", "json", onSuccess, onFailure);
        function onSuccess(Result) {
            if (Result > 0) {
                uiUnBlock();
                // window.open('/JournalEntry/Details?id=' + parseInt(Result) + '');                
                //window.location.href = '/JournalEntry/NewIndex';
                window.location.href = '/JournalEntry/Create';
            }
            else {
                uiUnBlock();
                $('#btnSubmit').prop('disabled', false);
                alert("Some error Ocurred! Please Check Your Entries");
            }
        }
        function onFailure(error) {
            if (error.statusText == "OK") {
                console.log("OK");
            }
            else {
                swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
            }
        }
    }
}