﻿var chqDat = "";
var balance = "";
$(function () {
    $("#Bank").prop("disabled", true);
    $("#BankDetail").prop("disabled", true);
    document.getElementById("VoucherDate").readOnly = true;
    document.getElementById("ChequeDate").readOnly = true;
    document.getElementById("ChequeNumber").readOnly = true;
    document.getElementById("ChequeDate").readOnly = true;

    $('#btnSave').click(function () {
        var FromBank_ID = $('#FromBank option:selected').val();
        var ToBank_ID = $('#ToBank option:selected').val();
        var voucherDate = $('#VoucherDate').val();
        var amount = $('#Amount').val();
        var description = $('#exchangerate').val();

        var SlipNumber = $('#SlipNumber').val();
        if (FromBank_ID == "" || FromBank_ID == 0) {
            swal("From Account", "Please Select From Account!", "error");
        }
        else if (ToBank_ID == FromBank_ID ) {
            swal("To Account && Form Account", "Please Select Both Account Same", "error");
        }
        else if (ToBank_ID == "" || ToBank_ID == 0) {
            swal("To Account", "Please Select To Account!", "error");
        }

        else if (amount == "" || amount == 0) {
            swal("Amount", "Please Enter Amount!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if (Description == "" ) {
            swal("Description", "Please Enter Description!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if (SlipNumber == "" || SlipNumber == 0) {
            swal("Amount", "Please Enter SlipNumber!", "error");
            //console.log(PurchaseDate + "date");
        }

        else {
            $('#btnSave').prop('disabled', true);
            uiBlock();
            insert();
        }
    });

});
$("#PayTypeID").on("change", function () {
    var payStatusSelection = $("#PayTypeID").val();
    var totalAmount = $("#TotalAmount").val();
    if (payStatusSelection == 1) {
        //document.getElementById("Bank").readOnly = true;
        $("#Bank").prop("disabled", true);
        $("#BankDetail").prop("disabled", true);
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;
    }
    else if (payStatusSelection == 2) {
        $("#Bank").prop("disabled", false);
        $("#BankDetail").prop("disabled", false);
        //document.getElementById("Bank").readOnly = false;
        document.getElementById("ChequeDate").readOnly = true;
        document.getElementById("ChequeNumber").readOnly = true;
    }
    else if (payStatusSelection == 3) {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        $("#BankDetail").prop("disabled", false);
        document.getElementById("ChequeDate").readOnly = false;
        document.getElementById("ChequeNumber").readOnly = false;
    }

});


function insert() {
    var payLog = "";
    var totalAmount = 0;
    var FromBank_ID = $('#FromBank option:selected').val();
    var ToBank_ID = $('#ToBank option:selected').val();
    var BranchID = $('#hdnBranchId').val();
    var desc = $('#Description').val();
    var bankAccountId = $('#Bank option:selected').val();
    var referenceNo = $('#ReferenceNo').val();
    var voucherDate = $('#VoucherDate').val();
    var amount = parseFloat($('#Amount').val());
    var slipnumber = $('#SlipNumber').val();
    var BankDetail = $('#BankDetail').val();
 
    var payType = $('#PayTypeID option:selected').val()
    var bank = $('#Bank option:selected').text();
    chqNumber = $('#ChequeNumber').val();
    var bankAccountId = $('#Bank option:selected').val();
    chqDate = $('#ChequeDate').val();
    var data = {
        'BranchID': BranchID,
        'Description': desc,
        'SlipNumber': slipnumber,
        'BankDetail'  : BankDetail,
        'Amount': amount,
        'FromBankID': FromBank_ID,
        'ToBankID': ToBank_ID,
        'PaymentTypeID': payType,
        'ChequeDate': chqDate,
        'ChequeNumber': chqNumber,
        'BankName': bank,
        'UserReferenceID': referenceNo,
        'VoucherDate': voucherDate,
    };
    var json = JSON.stringify({ 'model': data, 'bankAccId': bankAccountId });
    //console.log(json);            
    ajaxCall("POST", json, "application/json; charset=utf-8", "/PartyTransfer/SaveTransferEntry", "json", onSuccess, onFailure);
    function onSuccess(Result) {
        if (Result > 0) {
          
            window.location.href = '/CustomerPayment/Statement';
        }
        else if (Result == 'refError') {
            uiUnBlock();
            $('#btnSave').prop('disabled', false);
            swal("Reference No. Conflict", "Enter another Reference Code!", "error");
        }
        else {
            uiUnBlock();
            $('#btnSave').prop('disabled', false);
            swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
        }
        
    }
    function onFailure(error) {
        if (error.statusText == "OK")
            console.log(error);
    }
}

