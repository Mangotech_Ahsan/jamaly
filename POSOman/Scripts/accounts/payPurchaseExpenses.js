﻿var chqDat = "";
$(function () {
    //document.getElementById("Bank").readOnly = true;
    $("#Bank").prop("disabled", true);
    document.getElementById("ChequeDate").readOnly = true;
    document.getElementById("ChequeNumber").readOnly = true;
    document.getElementById("ChequeDate").readOnly = true;
    $('#btnSave').click(function () {
        var expenseTypeID = 7;
        var voucherDate = $('#VoucherDate').val();
        var amount = $('#Amount').val();
        var chqNumber = $('#ChequeNumber').val();
        var bankAccountId = $('#Bank option:selected').val();
        chqDate = $('#ChequeDate').val();
        var PaymentTypeID = $('#PaymentType option:selected').val();
        
        if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if (amount == "" || amount == 0) {
            swal("Amount", "Please Enter Amount!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if ((PaymentTypeID == 3) && (chqNumber == "")) {
            isValid = false;
            swal("Cheque Number", "Please Enter Cheque Number!", "error");
        }
        else if ((PaymentTypeID == 3) && (chqDate == "")) {
            isValid = false;
            swal("Cheque Date", "Please Enter Cheque Date!", "error");
        }
        else {
        $('#btnSave').prop('disabled', true);
        uiBlock();
            insert();
        }
    });
});
$("#PayTypeID").on("change", function () {
    var payStatusSelection = $("#PayTypeID").val();
    var totalAmount = $("#TotalAmount").val();
    if (payStatusSelection == 1) {
        //document.getElementById("Bank").readOnly = true;     
        $("#Bank").prop("disabled", true);
        document.getElementById("ChequeNumber").readOnly = true;
        document.getElementById("ChequeDate").readOnly = true;
        chqDate = "";
    }
    else if (payStatusSelection == 2) {
        //document.getElementById("Bank").readOnly = false;
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeNumber").readOnly = true;
        document.getElementById("ChequeDate").readOnly = true;
        chqDate = "";
    }
    else if (payStatusSelection == 3) {
        //document.getElementById("Bank").readOnly = false;        
        $("#Bank").prop("disabled", false);
        document.getElementById("ChequeNumber").readOnly = false;
        document.getElementById("ChequeDate").readOnly = false;
        chqDate = $('#ChequeDate').val();
    }
});
function insert() {
    var payLog = "";
    var totalAmount = 0;
    var expenseTypeID = 7;
    var BranchID = $('#hdnBranchId').val();
    var desc = $('#Description').val();
    var payType = $('#PayTypeID option:selected').val()
    //var bank = $('#Bank').val();
    var bank = $('#Bank option:selected').text();
    var bankAccountId = $('#Bank option:selected').val();
    var voucherDate = $('#VoucherDate').val();
    //var chqDate = $('#ChequeDate').val();
    var chqNumber = $('#ChequeNumber').val();
    var amount = parseFloat($('#Amount').val());
    //if (totalPaying == amount)

    var data = {
        'BranchID': BranchID,
        'Description': desc,
        'Amount': amount,
        'AccountID': expenseTypeID,
        'VoucherDate': voucherDate,
        'PaymentTypeID': payType,
        'BankName': bank,
        'ChequeDate': chqDate,
        'ChequeNumber': chqNumber
    };
    payLog = {
        'BranchID': BranchID,
        'Paid': amount,
        'Date': voucherDate,
        'JEntryTypeID': 7,  // Expenses
        //UserReferenceID: PONO,
        'AddBy': 1,
        'IsActive': 1
    };
    var json = JSON.stringify({ 'model': data, 'bankAccId': bankAccountId });
    //console.log(json);
    ajaxCall("POST", json, "application/json; charset=utf-8", "/PayPurchaseExpenses/PayPurchaseExpense", "json", onSuccess, onFailure);
    function onSuccess(Result) {
        if (Result == "success") {
        uiUnBlock();        
            window.location.href = '/Expenses/Details?isNew=true&id=' + expenseTypeID;
        }
        else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);        
            swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
        }
        //location.reload();
        //window.location.href = 'Index';
        //alert("success");
    }
    function onFailure(error) {
        if (error.statusText == "OK")
            //alert("success");
            //console.log(error);
            window.location.reload();
    }



}
