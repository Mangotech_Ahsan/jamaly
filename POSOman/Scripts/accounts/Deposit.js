﻿var balance = "";
$(function () {
    $('#btnSave').click(function () {
        var FromBank_ID = 5;//$('#FromBank option:selected').val();
        var referenceAccountID = $('#referenceAccount option:selected').val();
        var ToBank_ID = $('#ToBank option:selected').val();
        var voucherDate = $('#VoucherDate').val();
        var amount = $('#Amount').val();        
        if (FromBank_ID == "" || FromBank_ID == 0) {
            swal("From Bank", "Please Select From Bank!", "error");
        }
        if (ToBank_ID == "" || ToBank_ID == 0) {
            swal("To Bank", "Please Select To Bank!", "error");
        }
        //else if (referenceAccountID == "" || referenceAccountID == 0) {
        //    swal("Person", "Please Select Person!", "error");
        //}
        else if (FromBank_ID == ToBank_ID) {
            //console.log("FromBank_ID=" + FromBank_ID);
            //console.log("ToBank_ID=" + ToBank_ID);
            swal("Same Bank", "Please Select different Bank!", "error");
        }
        
        else if (voucherDate == "") {
            swal("Date", "Please Enter Date!", "error");
            //console.log(PurchaseDate + "date");
        }
        else if (amount == "" || amount == 0) {
            swal("Amount", "Please Enter Amount!", "error");
            //console.log(PurchaseDate + "date");
        }
        
        else {
        $('#btnSave').prop('disabled', true);
        uiBlock();
            insert();
        }
    });

});
$("#FromBank").change(function () {
    var FromBank_ID = $('#FromBank option:selected').val();
    getBankBalance(FromBank_ID);
});
function getBankBalance(FromBank_ID) {

    if (FromBank_ID != "") {
        var json = { "bankID": FromBank_ID };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/BankTransfer/getBankBalance',
            async: true,
            data: JSON.stringify(json),
            success: function (data) {
               // console.log(data);
                balance = data;
            },
            error: function (err) { console.log(err); }
        });
    }
}
function insert() {    
    var payLog = "";
    var totalAmount = 0;          
    var FromBank_ID = 1;//$('#FromBank option:selected').val();
    var ToBank_ID = $('#ToBank option:selected').val();
    var referenceAccountID = $('#referenceAccount option:selected').val();
    var BranchID = $('#hdnBranchId').val();
    var desc = $('#Description').val();
    var referenceNo = $('#ReferenceNo').val();
    var voucherDate = $('#VoucherDate').val();        
    var amount = parseFloat($('#Amount').val());  
            var data = {
                'BranchID': BranchID,
                'Description': desc,
                'Amount': amount,
                'FromBankID': FromBank_ID,
                'ToBankID': ToBank_ID,
                'UserReferenceID': referenceNo,
                'ReferenceAccountID': 1,
                'VoucherDate': voucherDate,                                
            };            
            var json = JSON.stringify({ 'model': data });
            //console.log(json);            
            ajaxCall("POST", json, "application/json; charset=utf-8", "/BankTransfer/SaveDepositEntry", "json", onSuccess, onFailure);
            function onSuccess(Result) {
                if (Result > 0) {                    
                    // window.location.href = '/BankTransfer/Details?id='+Result;
                    window.location.href = '/BankTransfer/Deposit';
                }
                else if (Result == 'refError') {
                    uiUnBlock();
                    $('#btnSave').prop('disabled', false);
                    swal("Reference No. Conflict", "Enter another Reference Code!", "error");
                }
                else {
                uiUnBlock();
                $('#btnSave').prop('disabled', false);        
                    swal("critical error", "Some error Ocurred! Please Check Your Entries!", "error");
                }
                //location.reload();
                //window.location.href = 'Index';
                //alert("success");
            }
            function onFailure(error) {
                if (error.statusText == "OK")
                    //alert("success");
                    //console.log(error);
                    window.location.reload();
            }
        
    
    
}
