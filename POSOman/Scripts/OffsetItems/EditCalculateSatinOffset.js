﻿
var globalData = null;
var dieCuttingCostGlobal = 0;
var slittingCostGlobal = 0;
var uvCostGlobal = 0;
var noOfCylindersPresentGlobal = 0;
var rollLengthinMMGlobal = 0;
var rollWidthinMMGlobal = 0;
var blockCostPerSqInchGlobal = 0;

$(document).ready(function () {
    // FetchLaminationCosts();
    var CylinderID = +$("#CylinderID").val() || 0;
    if (CylinderID > 0) {
        getCylinderData(CylinderID);
    }

    var PrintingMachineID = +$("#PrintingMachineID").val() || 0;
    if (PrintingMachineID > 0) {
        if (PrintingMachineID > 0) {

            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: '/MachinePlate/FetchPrintingMachineData?ID=' + PrintingMachineID,

                async: true,
                success: function (data) {
                    {
                        console.log(data)

                        $("#PrintingRatePerRoll").val(+data[0].data.RatesPerRoll);
                        $("#CylinderID").prop('disabled', false);
                        //GetDropdown1("CylinderID", data[0].cylinderDD, true);
                        //calculatePrintingCost();

                    }
                },
                error: function (err) { console.log(err); }
            });
        }
        else {
            $("#CylinderID").prop('disabled', true);
        }
    }
    
})
//  getCode();
function getCylinderData(ID) {
    console.log(ID)
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/FetchCylinderData?ID=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data)

                    $("#CylinderLength").val(+data.CylinderLength);
                    noOfCylindersPresentGlobal = +data.NoOfCylinders;

                    CalculateLength();

                }
            },
            error: function (err) { console.log(err); }
        });
    }

}

getRollQualityData = (ID) => {
    console.log(ID)
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/FetchRollQualityData?ID=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log("roll cost testing--------------------")
                    console.log(data)

                    $("#RollLength").val(+data.RollMeters);
                    rollLengthinMMGlobal = +data.RollMeters;
                    rollWidthinMMGlobal = +data.RollLength;
                    calculateRollCostAndPcs(+data.SatinRibbonCost);
                    
                }
            },
            error: function (err) { console.log(err); }
        });
    }
}

$("#WastagePcsInRoll").change(() => {
    calculateRollRequired();
    calculateMaterialCost();
    calculatePrintingCost();
})
getFinishingData = (ID) => {

    //if (ID > 0) {
    var typeID = ID || 0;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/MachinePlate/FetchFinishingData?ID=' + typeID,

        async: true,
        success: function (data) {
            {
                console.log(data)
                if (data != null && data != 0) {
                    $("#FinishingCost").val(+data.RatesPerPC);
                    calculateTotalCostPerPC();
                }
                else {
                    $("#FinishingCost").val(0);
                    calculateTotalCostPerPC();
                }

            }
        },
        error: function (err) { console.log(err); }
    });
    // }
};


getBlockData = (ID) => {

    var typeID = ID || 0;

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/MachinePlate/FetchBlockData?ID=' + typeID,

        async: true,
        success: function (data) {
            {
                if (data != null && data != 0) {
                    console.log(data)

                    blockCostPerSqInchGlobal = +data.RatesPerSqInch;
                    calculateBlockCost();
                }
                else {
                    console.log(data)

                    blockCostPerSqInchGlobal = 0;
                    calculateBlockCost();
                }


            }
        },
        error: function (err) { console.log(err); }
    });

};


calculateBlockCost = () => {

    let rates = +blockCostPerSqInchGlobal === 0 ? 1 : +blockCostPerSqInchGlobal;
    let cylinderLength = +$("#CylinderLength").val() || 0;
    let printingColors = +$("#NoOfPrintingColors").val() || 0;
    let width = +$("#Width").val() || 0;
    //let total = (cylinderLength * width) / 25.4 * rates;
    let total = ((cylinderLength * width) / 645.16 * rates) * printingColors;

    $("#BlockCost").val(+total.toFixed(2));
    calculateTotalCostPerPC();

}

$("#BlockCost").change(() => {
    // calculateBlockCost();
    calculateTotalCostPerPC();
})

calculateTotalCostPerPC = () => {
    let materialCost = +$("#MaterialCostPerMeter").val() || 0;
    let printingCost = +$("#PrintingCost").val() || 0;
    let finishingCost = +$("#FinishingCost").val() || 0;
    let blockCost = +$("#BlockCost").val() || 0;
    let qty = +$("#QtyPerUnit").val() || 1;
    let generalExpense = +$("#GeneralExpense").val() || 0;

    let total = materialCost + printingCost + finishingCost + (blockCost / qty) + generalExpense;

    $("#CostPerPc").val(+total.toFixed(2));
}


function calculateRollRequired() {

    let qty = +$("#QtyPerUnit").val() || 0;
    let wastagePcs = +$("#WastagePcsInRoll").val() || 0;
    let totalPcs = +$("#TotalPcsInRoll").val() || 0;
    console.log("this is total pcs->" + totalPcs)
    if (totalPcs > 0) {
        let totalReq = qty / (totalPcs - wastagePcs);
        console.log("checking this->" + totalReq)
        $("#TotalRequiredRolls").val(+Math.round(totalReq).toFixed(2));
    }
    else {
        $("#TotalRequiredRolls").val(0);

    }


}

$("#QtyPerUnit").change(() => {
    calculateRollRequired();
    calculateMaterialCost();
    calculatePrintingCost();
    setTimeout(() => { calculateTotalCostPerPC(); }, 1000);
  
})

$("#GeneralExpense").change(() => {

    calculateTotalCostPerPC();

})

calculateRollCostAndPcs = (cost) => {
    if (cost > 0) {
        //let Width = +$("#Width").val() || 0;
        //let Length = +$("#Length").val() || 1;
        //let total = cost / 25 * Width;
        //let totalPcs = (rollLengthinMMGlobal*1000) / Length; // Convert it in mm (mm x 1000) and take meters instead of length
        //$("#RollCost").val(+total.toFixed(2));
        let Width = +$("#Width").val() || 0;
        let Length = +$("#Length").val() || 1;
        //let total = ((rollWidthinMMGlobal / 1000) * rollLengthinMMGlobal) * cost;
        let total = ((Width / 1000) * rollLengthinMMGlobal) * cost;
        let totalPcs = (rollLengthinMMGlobal * 1000) / Length; // Convert it in mm (mm x 1000) and take meters instead of length
        $("#RollCost").val(+total.toFixed(2));
        $("#TotalPcsInRoll").val(+totalPcs.toFixed(2));

        calculateRollRequired();
        calculateMaterialCost();
        calculatePrintingCost();
        setTimeout(() => { calculateTotalCostPerPC(); }, 1000);

    }

}


$("#RollCost").change(() => {

    calculateRollRequired();
    calculateMaterialCost();
    calculatePrintingCost();
    setTimeout(() => { calculateTotalCostPerPC(); }, 1000);


})

getPrintingMachineData = (ID) => {
    console.log(ID)
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/FetchPrintingMachineData?ID=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data)

                    $("#PrintingRatePerRoll").val(+data[0].data.RatesPerRoll);
                    $("#CylinderID").prop('disabled', false);
                    GetDropdown1("CylinderID", data[0].cylinderDD, true);
                    calculatePrintingCost();

                }
            },
            error: function (err) { console.log(err); }
        });
    }
    else {
        $("#CylinderID").prop('disabled', true);
    }
}
$("#CylinderID").change(() => {
    checkPrintingCylinders();
})

CalculateLength = () => {
    let cylinder_length = +$("#CylinderLength").val() || 0;
    let ups = +$("#AroundUps").val() || 1;


    let length = cylinder_length / ups;
    $("#Length").val(+length);
}

$("#AroundUps").change(() => {
    CalculateLength();
});

$("#NoOfPrintingColors").change(() => {
    checkPrintingCylinders();
});

checkPrintingCylinders = () => {
    let no_printing_color = +$("#NoOfPrintingColors").val() || 0;
    if (no_printing_color > noOfCylindersPresentGlobal) {
        toastr.warning('Printing Color Cannot Be Greater Then Cylinders Present i.e. ' + noOfCylindersPresentGlobal);
        $('#NoOfPrintingColors').val('');
    }
    calculatePrintingCost();
}
calculateTotalSheets = () => {
    let upping = +$("#Upping").val() || 1;
    let qty = +$("#Quantity").val() || 0;

    let sheets = qty / upping;
    $("#NumberOfSheets").val(+sheets);
    getImpression(+sheets);

}

calculateSplitCount = () => {
    let totSheets = +$("#NumberOfSheets").val() || 0;
    let split = +$("#Split").val() || 1;
    console.log(totSheets)
    console.log(split)
    let count = Math.round(totSheets / split);
    $("#SplinterCount").val(+count);

    calculateWInch();
}

calculateWInch = () => {
    let splinterCount = +$("#SplinterCount").val() || 0;
    let wSheetSize = +$("#WSheetSize").val() || 0;

    let inch = splinterCount * wSheetSize;
    $("#TotalWinInches").val(+inch);
    calculateTotalMeters(+inch);
}

$("#Quantity").change(() => {
    calculateTotalSheets();
})

$("#Upping").change(() => {
    calculateTotalSheets();
    calculateSplitCount();
})

$("#Split").change(() => {
    calculateSplitCount();

})




$("#SplinterID").change(() => {
    let SplinterWidth = $("#SplinterID option:selected").text() || 0;

    $("#WSheetSize").val(+SplinterWidth);
    calculateWInch();

    //setTimeout(() => {

    //}, 3000);

})

calculateHSize = async () => {
    let split = +$("#Split").val() || 1;
    let rollLength = +$("#Length").val() || 0;

    let HSize = rollLength / split;

    $("#HSheetSize").val(+HSize);
}


$("#Split").change(() => {
    calculateHSize();
});

calculateTotalMeters = (WSize) => {
    let wSheetSize = +WSize;

    let meters = wSheetSize / 38;
    console.log(meters)
    console.log(WSize)
    $("#TotalMeters").val(+meters);



}

calculateMaterialCost = () => {
    let rollCost = +$("#RollCost").val() || 0;
    let wastagePcs = +$("#WastagePcsInRoll").val() || 0;
    let totalPcs = +$("#TotalPcsInRoll").val() || 0;

    if (totalPcs > 0) {
        let total = rollCost / (totalPcs - wastagePcs);

        $("#MaterialCostPerMeter").val(+total.toFixed(2));
        calculateTotalCostPerPC();
    }

}

calculatePrintingCost = () => {
    let printingRatePerRoll = +$("#PrintingRatePerRoll").val() || 0;
    let wastagePcs = +$("#WastagePcsInRoll").val() || 0;
    let totalPcs = +$("#TotalPcsInRoll").val() || 0;
    let printingColors = +$("#NoOfPrintingColors").val() || 0;

    if (totalPcs > 0) {
        let total = (printingRatePerRoll * printingColors) / (totalPcs - wastagePcs);

        $("#PrintingCost").val(+total.toFixed(2));
        calculateTotalCostPerPC();
    }

}

$("#Quantity").on('input', function () {

    var CardQuality = $("#CardQuality").val() || 0;
    var Length = +$("#Length").val() || 0;
    var Width = +$("#Width").val() || 0;
    var CardGSM = +$("#CardGSM").val() || 0;

    //if (CardQuality <= 0) {
    //    toastr.warning("Please Select Card Quality First!")
    //    $("#Quantity").val('');
    //}
    //if (Length <= 0) {
    //    toastr.warning("Please Insert Length First!")
    //    $("#Length").val('');
    //    $("#Quantity").val('');
    //}
    //if (Width <= 0) {
    //    toastr.warning("Please Insert Width First!")
    //    $("#Width").val('');
    //    $("#Quantity").val('');
    //}
    //if (CardGSM <= 0) {
    //    toastr.warning("Please Insert Card GSM First!")
    //    $("#CardGSM").val('');
    //    $("#Quantity").val('');
    //}

});

$("#Upping").on('input', function () {

    var Quantity = +$("#Quantity").val() || 0;
    var Upping = +$("#Upping").val() || 0;
    var Wastage = +$("#Wastage").val() || 0;
    if (Quantity > 0) {
        $("#Wastage").val(0);
        if (Upping > 0) {
            let Calculated = parseFloat(Quantity / Upping).toFixed(2);
            $("#NumberOfSheets").val(Calculated);
            // getNoOfSheetsValue();
            getImpression(+Calculated);
            $("#TotalNumberOfSheets").val(Calculated + Wastage);
        }
        else {
            $("#NumberOfSheets").val(0);
            $("#TotalNumberOfSheets").val(Wastage);
        }
        CalculateSheetCost();
    }
    else {
        toastr.warning("Please Insert Quantity First!")
        $("#Upping").val('');
    }
});

$("#Wastage").on('input', function () {

    var NumberOfSheets = +$("#NumberOfSheets").val() || 0;

    var Wastage = +$("#Wastage").val() || 0;
    if (NumberOfSheets > 0) {


        let cal = NumberOfSheets + Wastage;
        $("#TotalNumberOfSheets").val(cal);

    }
    else {
        toastr.warning("Please Insert Relative Data First!")
        $("#Wastage").val('');
        $("#TotalNumberOfSheets").val(0);
    }
});

$("#Length").on('input', function () {

    var Length = +$("#Length").val() || 0;


    if (Length > 0) {
        CalculateSheetCost();
    }

});

$("#Width").on('input', function () {

    var Width = +$("#Width").val() || 0;


    if (Width > 0) {
        CalculateSheetCost();
    }

});

$("#CardGSM").on('input', function () {

    var CardGSM = +$("#CardGSM").val() || 0;


    if (CardGSM > 0) {
        CalculateSheetCost();
    }

});


function getNoOfSheetsValue() {

    var NumberOfSheets = +$("#NumberOfSheets").val() || 0;

    if (NumberOfSheets > 0) {
        if (NumberOfSheets > 0 && NumberOfSheets <= 1200) {
            $("#Impression").val(1);
        }
        else if (NumberOfSheets > 1200 && NumberOfSheets <= 2200) {
            $("#Impression").val(2);
        }
        if (NumberOfSheets > 2200) {
            $("#Impression").val(3);
        }

    }

}

getImpression = (Sheets) => {
    if (+Sheets > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/GetImpression?Sheets=' + (+Sheets),

            async: true,
            success: function (data) {
                {
                    console.log(data);
                    if (+data > 0) {
                        $("#Impression").val(+data);
                    }
                    else {
                        toastr.warning(data.toString())
                        $("#Impression").val(0);
                    }


                }
            },
            error: function (err) { console.log(err); }
        });
    }
}

function CalculateSheetCost() {
    console.log("in CalculateSheetCost")

    let hdnCardTypeID = +$("#hdnCardTypeID").val() || 0;
    let Length = +$("#Length").val() || 0;
    let Width = +$("#Width").val() || 0;
    let CardGSM = +$("#CardGSM").val() || 0;
    var RatePerKG = +$("#RatePerKG").val() || 0;
    var NumberOfSheets = +$("#NumberOfSheets").val() || 0;

    if (hdnCardTypeID > 0) {
        if (hdnCardTypeID === 1) {//Card
            let Calculate = ((Length * Width * CardGSM * RatePerKG) / 15500) / 100;
            $("#PerSheetCost").val(parseFloat(Calculate / NumberOfSheets).toFixed(12));
            $("#TotalSheetCost").val(parseFloat(Calculate).toFixed(12));
        }
        else if (hdnCardTypeID === 2) {//Paper
            let Calculate = ((Length * Width * CardGSM * RatePerKG) / 16600) / 500;
            $("#PerSheetCost").val(parseFloat(Calculate / NumberOfSheets).toFixed(12));
            $("#TotalSheetCost").val(parseFloat(Calculate).toFixed(12));
        }
    }

}

function getMachineDetails(ID) {
    console.log(ID)
    if (ID > 0) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '/MachinePlate/FetchMachineDetails?ID=' + ID,

            async: true,
            success: function (data) {
                {
                    console.log(data);
                    globalData = data;
                    dieCuttingCostGlobal = data.DieCuttingCost;
                    slittingCostGlobal = data.SlittingCost;
                    uvCostGlobal = data.UVCost;

                    //console.log("slittingCostGlobal->" + slittingCostGlobal)
                    FetchLaminationCosts();
                    setTimeout(function () {
                        CalculateMachineSelectedCosts(data);
                    }, 1000);


                }
            },
            error: function (err) { console.log(err); }
        });
    }

}

function FetchLaminationCosts() {


    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/MachinePlate/FetchLaminationCosts',

        async: true,
        success: function (data) {
            {
                console.log(data);
                $("#GlossLaminationCost").val(parseFloat(+data.GlossLaminationCost).toFixed(2));
                $("#MatLaminationCost").val(parseFloat(+data.MatLaminationCost).toFixed(2));
                //$("#UVCost").val(parseFloat(+data.UVCost).toFixed(2));
                //$("#SlittingCost").val(parseFloat(+data.SlittingCost).toFixed(2));
                if ($("#includeUVCost").is(":checked")) {
                    $("#UVCost").val(parseFloat(+uvCostGlobal).toFixed(2));
                }
                else {
                    $("#UVCost").val(parseFloat(0).toFixed(2));
                }
                if ($("#includeSlittingCost").is(":checked")) {
                    console.log("in-slittingcost->" + slittingCostGlobal)
                    $("#SlittingCost").val(parseFloat(+slittingCostGlobal).toFixed(2));
                }
                else {
                    $("#SlittingCost").val(parseFloat(0).toFixed(2));
                }


            }
        },
        error: function (err) { console.log(err); }
    });


}

function CalculateMachineSelectedCosts(data) {
    console.log(data)
    if (data != null) {
        //let getMachinePlates = +$("#MachinePlates").val() || 0;
        let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
        let MachineTextColor = +$("#MachineTextColor").val() || 0;
        let Length = +$("#Length").val() || 0;
        let DieCost = +$("#DieCost").val() || 0;
        let Width = 1;//+$("#Width").val() || 0;
        let NoOfSheets = +$("#NumberOfSheets").val() || 0;
        let GroundCost = (+data.GroundCost * MachineGroundColor) || 0;
        let TextCost = (+data.TextCost * MachineTextColor) || 0;
        let GlossLaminationCost = +data.GlossLaminationCost || 0;
        let MatLaminationCost = +data.MatLaminationCost || 0;
        //let NoOfPlates = +GroundCost + +TextCost;
        let NoOfPlates = MachineGroundColor + MachineTextColor;
        let PlateCost = +data.PlateCost * +NoOfPlates;
        let UVCost = +data.UVCost || 0;
        let TotalUVCost = (+UVCost / 144) * (+Length * +Width * +NoOfSheets);
        let Impressions = +$("#Impression").val() || 0;
        let DieCuttingCost = +data.DieCuttingCost * +Impressions + +DieCost;
        //let DieCuttingCost = +data.DieCuttingCost * + +DieCost;
        let SlittingCost = +data.SlittingCost || 0;
        let TotalSlittingCost = +SlittingCost * Impressions;
        // let TotalSlittingCost = +SlittingCost ;
        let TotalGlossLaminationCost = (+GlossLaminationCost / 144) * +Length * +Width * +NoOfSheets;
        let TotalMatLaminationCost = (+MatLaminationCost / 144) * +Length * +Width * +NoOfSheets;
        let PrintingCost = (+Impressions * +GroundCost) + (+Impressions * +TextCost);

        // let PrintingCost = +GroundCost + ( +TextCost);
        $("#GlossLaminationCost").val(parseFloat(+GlossLaminationCost).toFixed(2));
        $("#MatLaminationCost").val(parseFloat(+MatLaminationCost).toFixed(2));
        //  if (getMachinePlates != NoOfPlates) {
        $("#MachinePlates").val(NoOfPlates);
        // }

        $("#MachineGroundCost").val(parseFloat(+GroundCost).toFixed(2));
        $("#MachineTextCost").val(parseFloat(+TextCost).toFixed(2));

        if ($('#GlossLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); }

        if ($('#MatLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); }



        $("#MachinePlateCost").val(parseFloat(+PlateCost).toFixed(2));
        $("#MachinePrintingCost").val(parseFloat(+PrintingCost).toFixed(2));
        //$("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        //$("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        //$("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));
        if ($("#includeUVCost").is(":checked")) {
            $("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        }
        else {
            $("#UVCost").val(parseFloat(0).toFixed(2));
        }
        if ($("#includeSlittingCost").is(":checked")) {
            $("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));
        }
        else {
            $("#SlittingCost").val(parseFloat(0).toFixed(2));
        }
        if ($("#includeDieCost").is(":checked")) {
            $("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        }
        else {
            $("#DieCuttingCost").val(parseFloat(0).toFixed(2));
        }
    }
}


function CalculateMachineSelectedCostsWithManualMachinePlatesCost(data) {
    console.log(data)
    if (data != null) {
        let getMachinePlates = +$("#MachinePlates").val() || 0;
        let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
        let MachineTextColor = +$("#MachineTextColor").val() || 0;
        let Length = +$("#Length").val() || 0;
        let DieCost = +$("#DieCost").val() || 0;
        let Width = +$("#Width").val() || 0;
        let NoOfSheets = +$("#NumberOfSheets").val() || 0;
        let GroundCost = (+data.GroundCost * MachineGroundColor) || 0;
        let TextCost = (+data.TextCost * MachineTextColor) || 0;
        let GlossLaminationCost = +data.GlossLaminationCost || 0;
        let MatLaminationCost = +data.MatLaminationCost || 0;
        //let NoOfPlates = +GroundCost + +TextCost;
        // let NoOfPlates = MachineGroundColor + MachineTextColor;
        let PlateCost = +data.PlateCost * +getMachinePlates;
        let UVCost = +data.UVCost || 0;
        let TotalUVCost = (+UVCost / 144) * (+Length * +Width * +NoOfSheets);
        let Impressions = +$("#Impression").val() || 0;
        //let PrintingCost = +Impressions * +GroundCost * +TextCost;
        let PrintingCost = (+Impressions * +GroundCost) + (+Impressions * +TextCost);
        let DieCuttingCost = +data.DieCuttingCost * +Impressions + +DieCost;
        let SlittingCost = +data.SlittingCost || 0;
        let TotalSlittingCost = +SlittingCost * Impressions;
        let TotalGlossLaminationCost = (+GlossLaminationCost / 144) * +Length * +Width * +NoOfSheets;
        let TotalMatLaminationCost = (+MatLaminationCost / 144) * +Length * +Width * +NoOfSheets;


        $("#GlossLaminationCost").val(parseFloat(+GlossLaminationCost).toFixed(2));
        $("#MatLaminationCost").val(parseFloat(+MatLaminationCost).toFixed(2));
        //  if (getMachinePlates != NoOfPlates) {
        //$("#MachinePlates").val(NoOfPlates);
        // }

        $("#MachineGroundCost").val(parseFloat(+GroundCost).toFixed(2));
        $("#MachineTextCost").val(parseFloat(+TextCost).toFixed(2));

        if ($('#GlossLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalGlossLaminationCost).toFixed(2)); }

        if ($('#MatLaminationCost').is(':checked')) { $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); $("#LaminationCost").val(parseFloat(+TotalMatLaminationCost).toFixed(2)); }



        $("#MachinePlateCost").val(parseFloat(+PlateCost).toFixed(2));
        $("#MachinePrintingCost").val(parseFloat(+PrintingCost).toFixed(2));
        //$("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        //$("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        //$("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));
        if ($("#includeUVCost").is(":checked")) {
            $("#UVCost").val(parseFloat(+TotalUVCost).toFixed(2));
        }
        else {
            $("#UVCost").val(parseFloat(0).toFixed(2));
        }
        if ($("#includeSlittingCost").is(":checked")) {
            $("#SlittingCost").val(parseFloat(+TotalSlittingCost).toFixed(2));
        }
        else {
            $("#SlittingCost").val(parseFloat(0).toFixed(2));
        }
        if ($("#includeDieCost").is(":checked")) {
            $("#DieCuttingCost").val(parseFloat(+DieCuttingCost).toFixed(2));
        }
        else {
            $("#DieCuttingCost").val(parseFloat(0).toFixed(2));
        }
    }
}

$('input[name="rad"]').click(function () {
    try {
        var $radio = $(this);
        console.log($radio.data('waschecked'));
        // if this was previously checked
        if ($radio.data('waschecked') == true) {
            $radio.prop('checked', false);
            $radio.data('waschecked', false);
        }
        else
            $radio.data('waschecked', true);

        // remove was checked from other radios
        $radio.siblings('input[name="rad"]').data('waschecked', false);

        if ($('#' + $radio.attr('id')).is(':checked')) {
            console.log($radio.attr('id') + "-checked");

            if ($radio.attr('id').toString() === 'MatLaminationCost') {
                console.log("Mat");

                //$("#GlossLaminationCost").prop('checked', false);
                let getMachinePlates = +$("#MachinePlates").val() || 0;
                let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
                let MachineTextColor = +$("#MachineTextColor").val() || 0;
                let finalColorRate = MachineGroundColor + MachineTextColor;
                if (globalData != null) {
                    console.log("In Mat")
                    // if ($('#MatLaminationCost').is(':checked')) {
                    if (getMachinePlates === finalColorRate) {
                        CalculateMachineSelectedCosts(globalData);
                    }
                    else {
                        CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
                    }


                    FinalPriceCalculation();
                    //  }
                }

            }

            else if ($radio.attr('id').toString() === 'GlossLaminationCost') {
                console.log("Gloss");
                //$("#MatLaminationCost").prop('checked', false);
                let getMachinePlates = +$("#MachinePlates").val() || 0;
                let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
                let MachineTextColor = +$("#MachineTextColor").val() || 0;
                let finalColorRate = MachineGroundColor + MachineTextColor;
                if (globalData != null) {
                    console.log("In Gloss")
                    //if ($('#GlossLaminationCost').is(':checked')) {
                    if (getMachinePlates === finalColorRate) {
                        CalculateMachineSelectedCosts(globalData);
                    }
                    else {
                        CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
                    }

                    FinalPriceCalculation();
                    //   }
                }
            }
        }
        else {
            console.log($radio.attr('id') + "-un-checked");

            $("#LaminationCost").val(0);
            FinalPriceCalculation();
        }
    }
    catch (err) {
        console.log("checkbox error->" + err);
    }

});


//$("#MatLaminationCost").click(function () {
//    console.log("Mat")
//    let getMachinePlates = +$("#MachinePlates").val() || 0;
//    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
//    let MachineTextColor = +$("#MachineTextColor").val() || 0;
//    let finalColorRate = MachineGroundColor + MachineTextColor;
//    if (globalData != null) {
//        console.log("In Mat")
//        if ($('#MatLaminationCost').is(':checked')) {
//            if (getMachinePlates === finalColorRate) {
//                CalculateMachineSelectedCosts(globalData);
//            }
//            else {
//                CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
//            }


//            FinalPriceCalculation();
//        }
//    }

//})

//$("#GlossLaminationCost").click(function () {
//    console.log("Gloss");
//    let getMachinePlates = +$("#MachinePlates").val() || 0;
//    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
//    let MachineTextColor = +$("#MachineTextColor").val() || 0;
//    let finalColorRate = MachineGroundColor + MachineTextColor;
//    if (globalData != null) {
//        console.log("In Gloss")
//        if ($('#GlossLaminationCost').is(':checked')) {
//            if (getMachinePlates === finalColorRate) {
//                CalculateMachineSelectedCosts(globalData);
//            }
//            else {
//                CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
//            }

//            FinalPriceCalculation();
//        }
//    }

//})



function FinalPriceCalculation() {
    let TotalSheetCost = +$("#TotalMaterialCostPerMeter").val() || 0;
    let NoOfSheets = +$("#NumberOfSheets").val() || 0;
    let Upping = +$("#Upping").val() || 0;
    let MachinePlateCost = +$("#MachinePlateCost").val() || 0;
    let MachinePrintingCost = +$("#MachinePrintingCost").val() || 0;
    let LaminationCost = +$("#LaminationCost").val() || 0;
    let DieCuttingCost = +$("#DieCuttingCost").val() || 0;
    let UVCost = +$("#UVCost").val() || 0;
    let SlittingCost = +$("#SlittingCost").val() || 0;
    let DesignCost = +$("#DesignCost").val() || 0;
    let FoilPrintingCost = +$("#FoilPrintingCost").val() || 0;
    let MarkupRate = +$("#MarkupRate").val() || 0;

    let GrandTotal = (TotalSheetCost + MachinePlateCost + MachinePrintingCost + LaminationCost +
        DieCuttingCost + UVCost + SlittingCost + DesignCost + FoilPrintingCost) / +NoOfSheets / +Upping;
    let updatedTotal = +GrandTotal;
    if (MarkupRate > 0) {
        $("#FinalCostPerPc").val(parseFloat(updatedTotal).toFixed(2));

        let calculatedMarkupPrice = (+MarkupRate / 100) * +GrandTotal;

        // if (calculatedMarkupPrice < GrandTotal) {
        GrandTotal = GrandTotal + calculatedMarkupPrice;
        $("#MarkedUpPrice").val(parseFloat(GrandTotal).toFixed(2));
        //}
        //else {
        //    $("#MarkupRate").val('');
        //    toastr.warning("Invalid MarkUp Rate Entered!")
        //    $("#MarkedUpPrice").val(parseFloat(updatedTotal).toFixed(4));
        //}
    }
    else {
        $("#FinalCostPerPc").val(parseFloat(updatedTotal).toFixed(2));
        $("#MarkedUpPrice").val(parseFloat(updatedTotal).toFixed(2));
    }


}

$("#MarkupRate").focusout(function () {

    let MarkUpRate = +$("#MarkupRate").val() || 0;
    let FinalCostPerPc = +$("#FinalCostPerPc").val() || 0;
    if (MarkUpRate > 0 && FinalCostPerPc > 0) {

        console.log(MarkUpRate)
        console.log(FinalCostPerPc)

        var PercentAmount = parseFloat((MarkUpRate / 100) * (FinalCostPerPc)).toPrecision(2);
        console.log(+PercentAmount)
        var FinalPrice = parseFloat(+PercentAmount + FinalCostPerPc).toPrecision(2);
        console.log(+FinalPrice)
        $("#MarkedUpPrice").val(parseFloat(+FinalPrice).toFixed(2));
        // FinalPriceCalculation();
    }
});
$("#MarkedUpPrice").focusout(function () {
    let MarkedUpPrice = +$("#MarkedUpPrice").val() || 0;
    let FinalCostPerPc = +$("#FinalCostPerPc").val() || 0;
    if (MarkedUpPrice > 0 && +FinalCostPerPc > 0) {
        var actualPrice = parseFloat(MarkedUpPrice - FinalCostPerPc).toPrecision(2);//487

        PercentRate = parseFloat((+actualPrice * 100) / +MarkedUpPrice).toPrecision(2);
        $("#MarkupRate").val(parseFloat(+PercentRate).toFixed(2));

    }
    else {
        $("#MarkupRate").val(0);
        FinalPriceCalculation();
    }
});

$("#DieCost").change(function () {
    let DieCost = +$("#DieCost").val() || 0;
    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;
    // if (DieCost > 0) {
    if (globalData != null) {
        console.log("In")
        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }

    // }
});

$("#DesignCost").change(function () {
    let DesignCost = +$("#DesignCost").val() || 0;
    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;
    //if (DesignCost > 0) {
    if (globalData != null) {
        console.log("In")
        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }

    //}
});

$("#MachineGroundColor").change(function () {
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    if (MachineGroundColor > 0) {
        if (globalData != null) {
            console.log("In")
            CalculateMachineSelectedCosts(globalData); FinalPriceCalculation();
        }

    }
}); $("#MachineTextColor").change(function () {
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    if (MachineTextColor > 0) {
        if (globalData != null) {
            console.log("In")
            CalculateMachineSelectedCosts(globalData); FinalPriceCalculation();
        }

    }
});

$("#FoilPrintingCost").change(function () {
    let FoilPrintingCost = +$("#FoilPrintingCost").val() || 0;
    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;
    // if (FoilPrintingCost > 0) {
    if (globalData != null) {
        console.log("In");
        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }

    // }
});

$("#MachinePlates").change(function () {
    let MachinePlates = +$("#MachinePlates").val() || 0;
    if (MachinePlates > 0) {
        if (globalData != null) {
            console.log("In");
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData); FinalPriceCalculation();
        }

    }
});

$("#includeDieCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    if ($('#includeDieCost').is(':checked')) {
        $("#DieCost").prop('readonly', false);
        console.log("die-checked")
        $("#DieCuttingCost").val(+dieCuttingCostGlobal);

    }
    else {
        console.log("die-un-checked")
        $("#DieCost").prop('readonly', true);
        $("#DieCuttingCost").val(0);
    }

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})


$("#includeUVCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    if ($('#includeUVCost').is(':checked')) {
        console.log("uv-checked")
        $("#UVCost").val(+uvCostGlobal);

    }
    else {
        console.log("uv-un-checked")
        $("#UVCost").val(0);
    }

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})


$("#includeSlittingCost").click(() => {

    let getMachinePlates = +$("#MachinePlates").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let finalColorRate = MachineGroundColor + MachineTextColor;

    if ($('#includeSlittingCost').is(':checked')) {
        console.log("slit---checked")
        console.log(+slittingCostGlobal)
        $("#SlittingCost").val(+slittingCostGlobal.toFixed(2));

    }
    else {
        console.log("slit-un-checked")
        $("#SlittingCost").val(0);
    }

    if (globalData != null) {

        if (getMachinePlates === finalColorRate) {
            CalculateMachineSelectedCosts(globalData);
        }
        else {
            CalculateMachineSelectedCostsWithManualMachinePlatesCost(globalData);
        }
        FinalPriceCalculation();
    }
})