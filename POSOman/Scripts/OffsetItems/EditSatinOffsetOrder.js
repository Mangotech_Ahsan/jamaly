﻿getSOID();
//  Get New SOID 
function getSOID() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '/SalesOrder/getNewSOID',
        async: true,
        success: function (data) {
            {
                $("#hdnSOID").val(data);
                SOID = data;
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

//$("#btnSave").click(function () {
//    try {
//        $('#btnSave').prop('disabled', true);
//        insert();
//    }
//    catch (err) {
//        $('#btnSave').prop('disabled', false);
//        console.log(err);
//    }
//})


function insert() {
    var saleDetails = [];
    let ItemDescription = $("#ItemDescription").val() || null;
    let pId = 1;
    let qty = +$("#Quantity").val() || 0;
    let RollQuality = +$("#RollQuality").val() || 0;
    let Length = +$("#Length").val() || 0;
    let Width = 0;
    let HSheetSize = +$("#HSheetSize").val() || 0;
    let WSheetSize = +$("#WSheetSize").val() || 0;
    let Split = +$("#Split").val() || 0;
    let SplinterID = +$("#SplinterID").val() || 0;

    let Upping = +$("#Upping").val() || 0;
    let RollCost = +$("#RollCost").val() || 0;
    let RollID = +$("#RollQuality").val() || 0;
    let NumberOfSheets = +$("#NumberOfSheets").val() || 0;
    let SplinterCount = +$("#SplinterCount").val() || 0;
    let TotalWinInches = +$("#TotalWinInches").val() || 0;
    let TotalMeters = +$("#TotalMeters").val() || 0;
    let MaterialCostPerMeter = +$("#MaterialCostPerMeter").val() || 0;
    let TotalMaterialCostPerMeter = +$("#TotalMaterialCostPerMeter").val() || 0;
    let MachinePlate = +$("#MachinePlate").val() || 0;
    let MachineGroundColor = +$("#MachineGroundColor").val() || 0;
    let MachineGroundCost = +$("#MachineGroundCost").val() || 0;
    let MachineTextColor = +$("#MachineTextColor").val() || 0;
    let MachineTextCost = +$("#MachineTextCost").val() || 0;
    let MachinePlates = +$("#MachinePlates").val() || 0;
    let MachinePlateCost = +$("#MachinePlateCost").val() || 0;
    let MachinePrintingCost = +$("#MachinePrintingCost").val() || 0;
    let TotalSheetCost = +$("#TotalSheetCost").val() || 0;
    let LaminationCost = +$("#LaminationCost").val() || 0;
    let DieCost = +$("#DieCost").val() || 0;
    let DieCuttingCost = +$("#DieCuttingCost").val() || 0;
    let UVCost = +$("#UVCost").val() || 0;
    let SlittingCost = +$("#SlittingCost").val() || 0;
    let DesignCost = +$("#DesignCost").val() || 0;
    let FoilPrintingCost = +$("#FoilPrintingCost").val() || 0;
    let FinalCostPerPc = +$("#FinalCostPerPc").val() || 0;
    let LaminationTypeID = null;
    let BranchId = 9001;

    if ($('#GlossLaminationCost').is(':checked')) {
        LaminationTypeID = 1;

    }
    if ($('#MatLaminationCost').is(':checked')) {
        LaminationTypeID = 2;

    }

    let MarkupRate = +$("#MarkupRate").val() || 0;
    let MarkedUpPrice = +$("#MarkedUpPrice").val() || 0;

    saleDetails.push({
        PartNo: 'Cloth',
        ProductId: 2,
        ItemDescription: ItemDescription,
        Qty: qty,
        // SalePrice: SalePrice,
        Total: TotalSheetCost,
        BranchID: BranchId,
        //CardQuality: CardQuality,
        Length: Length,
        RollQuality: RollQuality,
        //Width: Width,
        HSheetSize: HSheetSize,
        WSheetSize: WSheetSize,
        Split: Split,
        SplinterID: SplinterID,
        SplinterCount: SplinterCount,
        TotalWinInches: TotalWinInches,
        Upping: Upping,
        RollID: RollID,
        TotalMeters: TotalMeters,
        NoOfSheets: NumberOfSheets,
        MaterialCostPerMeter: MaterialCostPerMeter,
        TotalMaterialCost: TotalMaterialCostPerMeter,
        MachineID: MachinePlate,
        GroundColor: MachineGroundColor,
        GroundCost: MachineGroundCost,
        TextColor: MachineTextColor,
        TextCost: MachineTextCost,
        NoOfPlates: MachinePlates,
        PlateCost: MachinePlateCost,
        PrintingCost: MachinePrintingCost,
        LaminationTypeID: LaminationTypeID,
        LaminationCost: LaminationCost,
        DieCost: DieCost,
        DieCuttingCost: DieCuttingCost,
        UVCost: UVCost,
        SlittingCost: SlittingCost,
        DesignCost: DesignCost,
        FoilPrintingCost: FoilPrintingCost,
        FinalCostPerPc: FinalCostPerPc
    });
    if (saleDetails.length && MarkedUpPrice > 0) {


        var data = {
            'AccountID': $('#AccountID').val(),
            'SalePersonAccID': $('#SalePersonAccountID').val(),

            //'SOID': $("#hdnSOID").val(),
            'BranchID': BranchId,
            'OffsetTypeID': 2,
            'TotalAmount': MarkedUpPrice,
            'MarkupRate': MarkupRate,
            'MarkedupPrice': MarkedUpPrice,
            'SaleDetails': saleDetails
        };
        var json = JSON.stringify({
            'model': data,

        });
        //console.log(json);
        //  ajaxCall("POST", json, "application/json; charset=utf-8", "/MachinePlate/SaveSatinOffset", "json", onSuccess, onFailure);

        function onSuccess(Result) {
            if (Result > 0) {
                uiUnBlock();
                window.location.href = '/MachinePlate/OffsetOrders';


            } else {
                uiUnBlock();
                $('#btnSave').prop('disabled', false);

                alert("Some error Ocurred! Please Check Your Entries");
            }
        }

        function onFailure(error) {
            if (error.statusText == "OK") {
                console.log("OK");
            } else {
                swal("critical error", error.toString(), "error");
            }
        }
    } else {
        uiUnBlock();
        $('#btnSave').prop('disabled', false);

        swal("", "Some error Ocurred! Please Check Rates!", "error");
    }
}
