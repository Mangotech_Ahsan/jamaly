﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.DTO;
using System.Net;
using System.Text;
using System.Data.Entity;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace POSOman.Controllers
{
    public class PurchaseReturnController : Controller
    {
        dbPOS db = new dbPOS();

        private int GetBranchID()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        // GET: PurchaseReturn        
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            int branchId = GetBranchID();

            return View(db.tbl_PurchaseReturn.ToList().OrderByDescending(p => p.PurchaseReturnID));
        }

        public ActionResult POReturn()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            return View("POReturn_1",db.tbl_PurchaseOrder.Where(p => p.IsReturned == false  && p.POID > 1000 && p.IsDeleted != true).ToList());         
        }
        public JsonResult ReturnOrder(tbl_PurchaseReturn model, List<tbl_StockLog> modelStockLog)
        {
            try
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                model.AddBy = userID;
                POSOman.Models.BLL.PurchaseReturn returnOrder = new Models.BLL.PurchaseReturn();
                returnOrder.Save(model, modelStockLog,branchId);
                return Json("success");
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }            
        }
        public JsonResult getPurchaseDetail(string invoiceNO)
        {
            if (invoiceNO != "")
            {
                try
                {
                    int branchId = 0;
                    
                    if (Session["BranchID"] != null)
                    {
                        branchId = Convert.ToInt32(Session["BranchID"]);
                    }
                    else
                    {
                        var user = User.Identity;
                        string currentUserId = User.Identity.GetUserId();
                        var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                        branchId = currentUser.BranchID;
                    }
                    var qry = db.tbl_PurchaseOrder.Where(p => p.InvoiceNo == invoiceNO && p.IsReturned == false)
                        .Select(p => new
                        {
                            p.OrderID,
                            p.InvoiceNo,
                            p.tbl_AccountDetails.AccountName,
                            p.PurchaseDate,
                            p.VAT,
                            p.TotalAmount,
                            p.PaymentStatus                            
                        }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }

            return Json("");
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //tbl_PurchaseOrder purchaseOrder = db.tbl_PurchaseOrder.Find(id);
            List<tbl_POReturnDetails> _PORDetails = db.tbl_POReturnDetails.Where(p => p.ReturnID == id).ToList();
            if (_PORDetails == null)
            {
                return HttpNotFound();
            }
            return View(_PORDetails);
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            ////tbl_PurchaseOrder purchaseOrder = db.tbl_PurchaseOrder.Find(id);
            //List<tbl_PODetails> _PODetails = db.tbl_PODetails.Where(p => p.OrderID == id).ToList();            
            //if (_PODetails == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(_PODetails);

        }
        public JsonResult getInvoiceDetails(int orderID)
        {
            if (orderID > 0)
            {
                try
                {
                    int branchId = 0;
                    
                    if (Session["BranchID"] != null)
                    {
                        branchId = Convert.ToInt32(Session["BranchID"]);
                    }
                    else
                    {
                        var user = User.Identity;
                        string currentUserId = User.Identity.GetUserId();
                        var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                        branchId = currentUser.BranchID;
                    }                   
                    var qry = db.tbl_PODetails.Where(p => p.OrderID == orderID && p.IsReturned != true)
                        .Select(p => new
                        {
                            p.OrderID,
                            p.tbl_PurchaseOrder.POID,
                            p.ProductID,
                            p.PartNo,
                            p.tbl_PurchaseOrder.AccountID,
                            p.tbl_PurchaseOrder.BranchID,
                            p.tbl_PurchaseOrder.InvoiceNo,
                            p.tbl_PurchaseOrder.Expenses,
                            p.tbl_PurchaseOrder.VAT,
                            p.tbl_PurchaseOrder.DiscountAmount,
                            p.tbl_PurchaseOrder.TotalAmount,
                            p.Qty,
                            p.Packet,
                            PrDisc = p.DiscountAmount,
                            PrGST = p.GSTAmount,
                            p.IsPack,
                            p.LevelID,
                            p.UnitCode,
                            p.UnitPerCarton,
                            p.UnitPrice,
                            p.Total,
                            p.ReturnedQty,
                            FIFOQty = (p.tbl_Product.tbl_StockBatch.Where(sb => sb.ProductID == p.ProductID && sb.BatchID == p.OrderID + "-" + p.ProductID).FirstOrDefault() != null) ? p.tbl_Product.tbl_StockBatch.Where(sb => sb.ProductID == p.ProductID && sb.BatchID == p.OrderID + "-" + p.ProductID).FirstOrDefault().Qty : -999
                        }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }

            }

            return Json("");

        }      
    }
}
