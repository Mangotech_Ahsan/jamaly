﻿using POSOman.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class DOReturnController : Controller
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        
        // GET: SalesReturn
        public ActionResult Index()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            return View(db.tbl_DOReturn.Where(d => d.BranchID == branchId).ToList().OrderByDescending(p => p.DOReturnID));
        }
        // Get Sales Orders to return
        public ActionResult DOReturn()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            var tbl_DeliveryOrder = db.tbl_DeliveryOrder.Where(s => s.IsReturned == false && s.IsPaid != true && s.BranchID == branchId);
            return View(tbl_DeliveryOrder);
        }
        // Get Return Order Details 
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //tbl_PurchaseOrder purchaseOrder = db.tbl_PurchaseOrder.Find(id);
            List<tbl_DOReturnDetails> _DORDetails = db.tbl_DOReturnDetails.Where(p => p.DOReturnID == id).ToList();
            if (_DORDetails == null)
            {
                return HttpNotFound();
            }
            return PartialView(_DORDetails);
        }
        // Get Sales Detail of selected invoice 
        public JsonResult getDODetails(int orderID)
        {
            if (orderID > 0)
            {
                try
                {
                    int branchId = 0;
                    
                    if (Session["BranchID"] != null)
                    {
                        branchId = Convert.ToInt32(Session["BranchID"]);
                    }
                    else
                    {
                        var user = User.Identity;
                        string currentUserId = User.Identity.GetUserId();
                        var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                        branchId = currentUser.BranchID;
                    }
                    var qry = db.tbl_DODetails.Where(s => s.OrderID == orderID && s.IsReturned != true && s.BranchID == branchId)
                        .Select(s => new
                        {
                            s.OrderID,
                            s.tbl_DeliveryOrder.DOID,
                            s.ProductID,
                            s.tbl_Product.PartNo,
                            s.tbl_DeliveryOrder.AccountID,
                            s.tbl_DeliveryOrder.BranchID,
                            s.tbl_DeliveryOrder.TotalAmount,                            
                            s.Qty,
                            s.SalePrice,
                            s.UnitPrice,    // CostPrice
                            s.Total,
                            s.ReturnedQty
                        }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }
            return Json("");
        }
        public JsonResult ReturnOrder(Models.DTO.DOReturn model, List<Models.DTO.StockLog> modelStockLog)
        {
            try
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                POSOman.Models.BLL.DOReturn returnOrder = new Models.BLL.DOReturn();
                object result = returnOrder.Save(model, modelStockLog,branchId);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Return Delivery Orders Action ".ToString());

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }
    }
}
