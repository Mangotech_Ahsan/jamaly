﻿using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class OnlineCustomersController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        // GET: OnlineCustomers
        public ActionResult Index()
        {
            return View();
        }

        // Get Online Customer Sales Date Wise 
        public ActionResult CustomerSales(bool? isSearch, int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchId)
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.customer = db.tbl_OnlineCustomer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.customerPhone = db.tbl_OnlineCustomer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();           
            if (isSearch == true)
            {
                List<GetOnlineCustomerSalesDateWise_Result> customer = db.GetOnlineCustomerSalesDateWise(AccountID, fromDate, toDate, BranchId).ToList();
                if (customer.Count == 0)
                {
                    return PartialView("_CustomerSales");
                }
                else
                    return PartialView("_CustomerSales", customer);
            }
            return View();
        }
        // Get Statement of Customer 
        public ActionResult CustomerStatement(int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.customer = db.tbl_OnlineCustomer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();            
            ViewBag.customerPhone = db.tbl_OnlineCustomer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            if (AccountID > 0)
            {
                List<GetOnlineCustomerPaymentDateWise_Result> customer = db.GetOnlineCustomerPaymentDateWise(AccountID, fromDate, toDate, BranchID).ToList();
                if (customer.Count == 0)
                {
                    return PartialView("_CustomerStatement");
                }
                else
                    return PartialView("_CustomerStatement", customer);
            }
            return View();
        }
        // Get Receiving Page
        public ActionResult Receiving()
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                ViewBag.customer = db.tbl_OnlineCustomer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
                ViewBag.customerPhone = db.tbl_OnlineCustomer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
    }
}