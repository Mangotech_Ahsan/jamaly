﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class OrderMachineTypesController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: OrderMachineTypes
        public ActionResult Index()
        {
            return View(db.tbl_OrderMachineTypes.ToList());
        }

        // GET: OrderMachineTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_OrderMachineTypes tbl_OrderMachineTypes = db.tbl_OrderMachineTypes.Find(id);
            if (tbl_OrderMachineTypes == null)
            {
                return HttpNotFound();
            }
            return View(tbl_OrderMachineTypes);
        }

        // GET: OrderMachineTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OrderMachineTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( tbl_OrderMachineTypes tbl_OrderMachineTypes)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(tbl_OrderMachineTypes.TypeName))
                {
                    ModelState.AddModelError("TypeName", "Required");
                    return View(tbl_OrderMachineTypes);
                }
                tbl_OrderMachineTypes.AddedOn = Helper.PST();
                db.tbl_OrderMachineTypes.Add(tbl_OrderMachineTypes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_OrderMachineTypes);
        }

        // GET: OrderMachineTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_OrderMachineTypes tbl_OrderMachineTypes = db.tbl_OrderMachineTypes.Find(id);
            if (tbl_OrderMachineTypes == null)
            {
                return HttpNotFound();
            }
            return View(tbl_OrderMachineTypes);
        }

        // POST: OrderMachineTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( tbl_OrderMachineTypes tbl_OrderMachineTypes)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(tbl_OrderMachineTypes.TypeName))
                {
                    ModelState.AddModelError("TypeName", "Required");
                    return View(tbl_OrderMachineTypes);
                }
                db.Entry(tbl_OrderMachineTypes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_OrderMachineTypes);
        }

        // GET: OrderMachineTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_OrderMachineTypes tbl_OrderMachineTypes = db.tbl_OrderMachineTypes.Find(id);
            if (tbl_OrderMachineTypes == null)
            {
                return HttpNotFound();
            }
            return View(tbl_OrderMachineTypes);
        }

        // POST: OrderMachineTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_OrderMachineTypes tbl_OrderMachineTypes = db.tbl_OrderMachineTypes.Find(id);
            db.tbl_OrderMachineTypes.Remove(tbl_OrderMachineTypes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
