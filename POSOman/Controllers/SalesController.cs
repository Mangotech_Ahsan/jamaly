﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using POSOman.Models.BLL;
using Microsoft.Reporting.WebForms;
using System.Text;
using POSOman.Models.DTO;
using System.Threading.Tasks;
using System.IO;
using OfficeOpenXml;
using Syncfusion.Linq;

namespace POSOman.Controllers
{
    public class SalesController : Controller
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        Common com = new Common();
        SalesOrder custom = new SalesOrder();

        private int GetCode()
        {
            int basecode = 1001;
            //var data = db.tbl_SalesOrder1.OrderByDescending(a => a.SOID).FirstOrDefault();
            var data = db.tbl_SalesOrder.OrderByDescending(a => a.SOID).FirstOrDefault();
            if (data != null)
            {
                return (data.SOID + 1);
            }

            return basecode;
        }

        public JsonResult GetCustomerPO(int OrderID)
        {
            try
            {
                if (OrderID > 0)
                {
                    var data = db.tbl_SalesOrder.Where(x => x.OrderID == OrderID).Select(x => x.CustomerPO).FirstOrDefault();
                    if (data != null)
                    {
                        return Json(data, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(-1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UpdateCustomerPO(tbl_SalesOrder model)
        {
            try
            {
                if (model.OrderID > 0 && !string.IsNullOrWhiteSpace(model.CustomerPO))
                {
                    var order = db.tbl_SalesOrder.Where(x => x.OrderID == model.OrderID).FirstOrDefault();
                    if (order != null)
                    {
                        order.CustomerPO = model.CustomerPO;
                        db.Entry(order).State = EntityState.Modified;
                        db.SaveChanges();
                        return Json(1);
                    }
                }
                return Json(-1);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message);
            }
        }
        [HttpGet]
        public JsonResult SaleOrderDetail(int? id)
        {
            var data = db.tbl_SaleDetails1.Where(a => a.OrderID == id).Select(a => new
            {
                AccountID = a.tbl_SalesOrder1.AccountID,
                CategoryID = a.tbl_Product.VehicleCodeID,
                CategoryName = a.tbl_Product.tbl_VehicleCode.VehicleCode,
                ProductID = a.ProductID,
                ProductName = a.tbl_Product.PartNo,
                UnitCode = a.UnitCode,
                UnitPrice = a.UnitPrice,
                Qty = a.Qty,
                Total = a.Total,
            }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void GetViewBagValues()
        {
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.saleperson = db.tbl_SalePerson.Select(c => new { Value = c.AccountID, Name = c.SalePerson }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Product = db.tbl_Product.Where(x => (x.isActive == 1 || x.isActive == null) && x.IsOffsetItem == false && x.IsGeneralItem == false).Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.RawProduct = db.tbl_Product.Where(x => (x.isActive == 1 || x.isActive == null) && x.IsOffsetItem == false && x.IsGeneralItem == true).Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.Desc = db.tbl_Product.Where(x => x.VehicleCodeID != 1).Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();
            ViewBag.Vat = db.tbl_TAX.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AmountInPercentage, Name = c.TaxDescription + " | " + c.AmountInPercentage.ToString() + "%" }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Where(x => x.VehicleCodeID != 1).Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.PaymentTypeID = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Inquiry = db.tbl_Inquiry.Where(a => a.isActive == true).Select(a => new { Value = a.InquiryID, Name = a.InquiryNumber }).ToList();
            ViewBag.Quotation = db.tbl_InquiryQuotation.Where(a => a.isActive == true && a.isDelete == false).Select(a => new { Value = a.QuotationID, Name = a.StrCode }).ToList();
            ViewBag.UnitCode = db.tbl_UnitCode.Where(a => a.IsDeleted == null || a.IsDeleted != true).Select(a => new { Value = a.UnitCodeID, Name = a.UnitCode }).ToList();

            ViewBag.StoreStatus = db.tbl_StoreStatus.Where(a => a.IsDeleted == false).Select(a => new { Value = a.ID, Name = a.StoreStatus }).ToList();
            ViewBag.DeliveryStatus = db.tbl_DeliveryStatus.Where(a => a.IsDeleted == false).Select(a => new { Value = a.ID, Name = a.DeliveryStatus }).ToList();
            ViewBag.OrderMachine = db.tbl_OrderMachine.Where(a => a.IsDeleted == false).Select(a => new { Value = a.ID, Name = a.OrderMachine }).ToList();
            ViewBag.OrderNature = db.tbl_OrderNature.Where(a => a.IsDeleted == false).Select(a => new { Value = a.ID, Name = a.OrderNature }).ToList();

            ViewBag.SlittingMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 1).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
            ViewBag.DieCuttingMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 3).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
            ViewBag.UVMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 4).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
            ViewBag.LaminationMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 5).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
            ViewBag.FoilMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 6).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
            ViewBag.EmbosingMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 7).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();

            ViewBag.MachineTypes = db.tbl_OrderMachineTypes.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.TypeName }).ToList();

            //Satin
            ViewBag.Cylinder = db.tbl_Cylinder.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Cylinder }).ToList();
            ViewBag.FinishingType = db.tbl_FinishingType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.FinishingType }).ToList();
            ViewBag.Roll = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 3).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
            ViewBag.SatinDepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
            ViewBag.PrintingMachine = db.tbl_PrintingMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.PrintingMachine }).ToList();
            //
            //Sticker
            ViewBag.MachineType = db.tbl_StickerMachineType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Type }).ToList();
            ViewBag.StickerDepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.StickerMachine = db.tbl_StickerMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachineName }).ToList();
            ViewBag.StickerCylinder = db.tbl_StickerCylinder.Select(x => new { Value = x.ID, Name = x.CylinderType }).ToList();
            ViewBag.StickerQuality = db.tbl_StickerQuality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Quality }).ToList();
            ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
            ViewBag.DieCutting = db.tbl_StickerDieCutting.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.DieCutting }).ToList();
            ViewBag.StickerRibbonFoil = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 1).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            ViewBag.StickerRibbonThermal = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 2).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            ViewBag.StickerRibbonSatin = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 3).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            ViewBag.StickerRibbonRFID = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 4).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            //
            // Offset
            ViewBag.CardQlty = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
            ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
            ViewBag.OffsetDepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.TafataRollQuality = db.tbl_Roll.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
            ViewBag.Splinter = db.tbl_SplinterSize.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.SplinterSize }).ToList();

        }

        public JsonResult GetMachinesByTypeIDWise(int ID, int DepartmentID)
        {
            if (ID > 0 && DepartmentID > 0)
            {
                var machines = db.tbl_OrderMachine.Where(x => x.MachineTypeID == ID && x.DeptID == DepartmentID && x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
                if (machines != null && machines.Count > 0)
                {
                    return Json(machines, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        // GET: Sales
        public ActionResult Index(bool? btn, int? AccountID, int? OID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            //ViewBag.RedirectLink = "/Sales";
            if (btn.HasValue)
            {
                List<GetSalesOrder_Result> data = db.GetSalesOrder(BranchID, AccountID, OID, fromDate, toDate, null, null).ToList();
                if (data.Count > 0)
                {
                    return PartialView("_Index", data.OrderByDescending(so => so.OrderID));
                }

                return PartialView("_Index");
            }

            //var data = db.tbl_SalesOrder1.Where(a => a.IsActive == true && a.IsDeleted == false).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            return View();
        }

        public ActionResult WastageSOQtyIndex(bool? btn, int? DeptID, int? OID, DateTime? fromDate, DateTime? toDate, int? ItemID)
        {
            if (btn.HasValue)
            {
                List<GetWastageSOItemQtyFilterWise_Result> data = db.GetWastageSOItemQtyFilterWise(fromDate, toDate, DeptID, OID, ItemID).ToList();
                if (data.Count > 0)
                {
                    return PartialView("_WastageSOQtyIndex", data.OrderByDescending(so => so.AddedOn));
                }

                return PartialView("_WastageSOQtyIndex");
            }

            ViewBag.Departments = db.tbl_Department.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.Department }).ToList();
            ViewBag.Items = db.tbl_Product.Where(x => x.isActive == 1 || x.isActive == null).Select(b => new { Value = b.ProductID, Name = b.PartNo }).ToList();
            return View();
        }

        public ActionResult CompletedJobOrders(bool? btn, int? AccountID, int? OID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            if (btn.HasValue)
            {
                List<GetSalesOrder_Result> data = db.GetSalesOrder(BranchID, AccountID, OID, fromDate, toDate, null, null).Where(x => x.IsCompleted == true).ToList();
                if (data.Count > 0)
                {
                    return PartialView("_CompletedJobOrders", data.OrderByDescending(so => so.OrderID));
                }

                return PartialView("_CompletedJobOrders");
            }

            //var data = db.tbl_SalesOrder1.Where(a => a.IsActive == true && a.IsDeleted == false).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            return View();
        }

        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult SODelete(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_SalesOrder model = db.tbl_SalesOrder.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);


        }

        [HttpPost, ActionName("SODelete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                bool isExist = db.tbl_Challan.Any(p => p.JOID == id && p.IsDeleted != true);
                if (isExist)
                {
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                    tbl_SalesOrder model = db.tbl_SalesOrder.Find(id);
                    return View(model);
                }
                else
                {
                    var data = db.tbl_SalesOrder.Where(x => x.OrderID == id && x.IsDeleted != true).FirstOrDefault();
                    if (data != null)
                    {

                        if (data.tbl_SaleDetails != null)
                        {
                            data.tbl_SaleDetails.ToList().ForEach(x => x.IsDeleted = true);
                        }

                        var det = db.tbl_SOItemWisePackageDetails.Where(x => x.OrderID == id).ToList(); // for job order
                        if (det.Count > 0 && det != null)
                        {
                            det.ForEach(x => x.IsDeleted = true);

                            //db.Entry(det).State = EntityState.Modified;
                            // db.SaveChanges();
                        }
                        data.IsDeleted = true;
                        data.DeletedOn = Helper.PST();
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        //db.tbl_Product.Remove(tbl_Product);
                        UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Job Order #: " + id.ToString());
                        return RedirectToAction("Index");
                    }

                    ModelState.AddModelError(string.Empty, "Some error occured");
                    tbl_SalesOrder model = db.tbl_SalesOrder.Find(id);
                    return View(model);

                }
            }
            catch (Exception err)
            {
                while (err.InnerException != null)
                {
                    err = err.InnerException;
                }
                ModelState.AddModelError(String.Empty, err.Message);
                tbl_SalesOrder model = db.tbl_SalesOrder.Find(id);
                return View(model);
            }
        }
        public ActionResult GetOffsetData(int ItemID)
        {

            var data = db.tbl_Product.Where(x => x.ProductID == ItemID).FirstOrDefault();
            GetViewBagValues();

            if (data.DepartmentID == 1 && data.OffsetTypeID == 3)
            {
                return PartialView("_CalculateTafataOffset", data);
            }
            else if (data.DepartmentID == 1 && data.OffsetTypeID != 3)
            {
                return PartialView("_CalculateOffset", data);
            }
            else if (data.DepartmentID == 2)
            {
                return PartialView("_CalculateSatinOffset", data);
            }
            else if (data.DepartmentID == 3)
            {
                return PartialView("_CalculateStickerOffset", data);
            }
            return PartialView("_NotFound");
        }

        [HttpPost]
        public JsonResult ChangeOrderStatus(int StatusID, int OrderID, int ItemID)
        {
            if (ItemID > 0 && OrderID > 0)
            {
                using (var t = db.Database.BeginTransaction())
                {
                    var sd = db.tbl_SaleDetails.Where(x => x.OrderID == OrderID && x.ProductID == ItemID).FirstOrDefault();
                    if (sd != null)
                    {
                        sd.isCompleted = StatusID == 1 ? true : false;
                        db.Entry(sd).State = EntityState.Modified;
                        db.SaveChanges();
                        var s = db.tbl_SaleDetails.Where(x => x.OrderID == OrderID).Select(x => x.isCompleted).ToList();
                        var sale = db.tbl_SalesOrder.Where(x => x.OrderID == OrderID).FirstOrDefault();
                        if (sale != null)
                        {
                            if (s.All(x => x == true))
                            {
                                sale.isCompleted = true;
                                db.Entry(sale).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                sale.isCompleted = false;
                                db.Entry(sale).State = EntityState.Modified;
                                db.SaveChanges();

                            }

                        }

                        t.Commit();
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(-1, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> DepartmentWiseOrdersIndex(bool? btn, int? accountID, DateTime? fromDate, DateTime? toDate, int? BranchID, int? salepersonID, int? prodID, int? departmentID, int? orderID, int? MachineID)
        {
            //ViewBag.RedirectLink = "/Sales/DepartmentWiseOrdersIndex";
            var response = RSharp.RestSharpClient(1);
            if (response == true)
            {
                return RedirectToActionPermanent("CustomLogOff", "Account");
            }

            List<object> statusList = new List<object>();
            statusList.Add(new { Value = 1, Name = "Completed" });
            //statusList.Add(new { Value = 0, Name = "Not-Completed" });
            ViewBag.OrderStatus = statusList.ToList();
            if (btn.HasValue)
            {
                //await Helper.CompleteJoIfDelivered();
                List<GetJobOrderListProductDepartmentWiseSP_Result> data = db.GetJobOrderListProductDepartmentWiseSP(orderID, prodID, departmentID, accountID, salepersonID, fromDate, toDate, null, MachineID).ToList();
                if (data.Count > 0)
                {
                    return PartialView("_DepartmentWiseOrdersIndex", data.OrderByDescending(so => so.OrderID));
                }

                return PartialView("_DepartmentWiseOrdersIndex");
            }

            //var data = db.tbl_SalesOrder1.Where(a => a.IsActive == true && a.IsDeleted == false).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.product = db.tbl_Product.Where(x => x.isActive != 0).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();

            if (User.IsInRole("Satin Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            else if (User.IsInRole("Sticker Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            else if (User.IsInRole("Offset Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }

            else
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            ViewBag.orderMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.OrderMachine }).ToList();
            ViewBag.saleperson = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.SalePerson }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.DieCuttingMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 3).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
            ViewBag.MachineTypes = db.tbl_OrderMachineTypes.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.TypeName }).ToList();

            return View();
        }

        // Calls from DND View List for every 30 minutes
        [HttpPost]
        public JsonResult MarkJoAsCompleted()
        {
            try
            {
                return Json(Helper.CompleteJoIfDelivered(), JsonRequestBehavior.AllowGet); 
            }
            catch(Exception e)
            {

            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DepartmentWiseOrdersIndex_ListView(bool? btn, int? accountID, DateTime? fromDate, DateTime? toDate, int? BranchID, int? salepersonID, int? prodID, int? departmentID, int? orderID, int? MachineID)
        {
            //ViewBag.RedirectLink = "/Sales/DepartmentWiseOrdersIndex";
            var response = RSharp.RestSharpClient(1);
            if (response == true)
            {
                return RedirectToActionPermanent("CustomLogOff", "Account");
            }

            List<object> statusList = new List<object>();
            statusList.Add(new { Value = 1, Name = "Completed" });
            //statusList.Add(new { Value = 0, Name = "Not-Completed" });
            ViewBag.OrderStatus = statusList.ToList();
            if (btn.HasValue)
            {
                List<GetJobOrderListProductDepartmentWiseSP_Result> data = db.GetJobOrderListProductDepartmentWiseSP(orderID, prodID, departmentID, accountID, salepersonID, fromDate, toDate, null, MachineID).ToList();
                if (data.Count > 0)
                {
                    
                    List<MachinesDTO> machines = new List<MachinesDTO>();
                    List<DragDropDTO> orders = new List<DragDropDTO>();
                    if (departmentID != null && departmentID > 0)
                    {
                        machines.Add(new MachinesDTO { MachineId = null, MachineName = "Pending" });

                        var machineList = db.tbl_OrderMachine.Where(x => x.DeptID == departmentID && x.IsDeleted != true).Select(x => new { MachineAttachedID = x.ID, MachineAttached = x.OrderMachine }).ToList();//data.Where(x => x.DepartmentID == departmentID).Select(x => new { x.MachineAttachedID, x.MachineAttached }).Distinct().ToList();

                        if (machineList != null && machineList.Count > 0)
                        {
                            foreach(var i in machineList)
                            {
                                if (i.MachineAttachedID > 0 && !i.MachineAttached.Trim().ToLower().Equals("ready to deliver"))
                                {
                                    machines.Add(new MachinesDTO { MachineId = i.MachineAttachedID, MachineName = i.MachineAttached });
                                }
                            }
                           
                        }
                        machines.Add(new MachinesDTO { MachineId = 48, MachineName = "Ready To Deliver" });

                        var orderList = data.Where(x => x.DepartmentID == departmentID).ToList();

                        if (orderList != null && orderList.Count > 0)
                        {
                            foreach (var o in machines)
                            {
                                if (o != null)
                                {
                                    if (o.MachineId == null)//Pending
                                    {
                                        var List = orderList?.Where(x => x.MachineAttachedID == null).ToList();
                                        if (List?.Count > 0)
                                        {
                                            foreach(var i in List)
                                            {
                                                orders.Add(new DragDropDTO { OrderID = i.OrderID, MachineName = "Pending", CustomerPO = i.CustomerPO, Customer = i.Name, MachineId = i.MachineAttachedID });
                                            }
                                            
                                        }
                                    }
                                    
                                    else
                                    {
                                        var List = orderList?.Where(x => x.MachineAttachedID == o.MachineId).ToList();
                                        if (List?.Count > 0)
                                        {
                                            foreach (var i in List)
                                            {
                                                orders.Add(new DragDropDTO { OrderID = i.OrderID, MachineName = i.MachineAttached, CustomerPO = i.CustomerPO, Customer = i.Name, MachineId = i.MachineAttachedID });

                                            }
                                           
                                        }
                                    }
                                }
                                
                            }

                        }


                    }



                    Tuple<List<DragDropDTO>, List<MachinesDTO>> combinedList = new Tuple<List<DragDropDTO>, List<MachinesDTO>>(orders, machines);

                    return PartialView("_DepartmentWiseOrdersIndex_ListView", combinedList);
                }

                return PartialView("_DepartmentWiseOrdersIndex_ListView");
            }

            //var data = db.tbl_SalesOrder1.Where(a => a.IsActive == true && a.IsDeleted == false).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.product = db.tbl_Product.Where(x => x.isActive != 0).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();

            if (User.IsInRole("Satin Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            else if (User.IsInRole("Sticker Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            else if (User.IsInRole("Offset Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }

            else
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            ViewBag.orderMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.OrderMachine }).ToList();
            ViewBag.saleperson = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.SalePerson }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.DieCuttingMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 3).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
            ViewBag.MachineTypes = db.tbl_OrderMachineTypes.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.TypeName }).ToList();

            return View();
        }

        [Authorize]
        public ActionResult DNDOutSide(bool? btn, int? accountID, DateTime? fromDate, DateTime? toDate, int? BranchID, int? salepersonID, int? prodID, int? departmentID, int? orderID, int? MachineID)
        {
            //ViewBag.RedirectLink = "/Sales/DepartmentWiseOrdersIndex";
            var response = RSharp.RestSharpClient(1);
            if (response == true)
            {
                return RedirectToActionPermanent("CustomLogOff", "Account");
            }

            //var data = db.tbl_SalesOrder1.Where(a => a.IsActive == true && a.IsDeleted == false).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.product = db.tbl_Product.Where(x => x.isActive != 0).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();

            if (User.IsInRole("Satin Manager"))
            {
                departmentID = 2;
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            else if (User.IsInRole("Sticker Manager"))
            {
                departmentID = 3;
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            else if (User.IsInRole("Offset Manager"))
            {
                departmentID = 1;
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }

            else
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            ViewBag.orderMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.OrderMachine }).ToList();
            ViewBag.saleperson = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.SalePerson }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.DieCuttingMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 3).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
            ViewBag.MachineTypes = db.tbl_OrderMachineTypes.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.TypeName }).ToList();
           

            List<object> statusList = new List<object>();
            statusList.Add(new { Value = 1, Name = "Completed" });
            //statusList.Add(new { Value = 0, Name = "Not-Completed" });
            ViewBag.OrderStatus = statusList.ToList();
            //departmentID = 2;
            if (btn.HasValue)
            {
                
                List<GetJobOrderListProductDepartmentWiseSP_Result> data = db.GetJobOrderListProductDepartmentWiseSP(orderID, prodID, departmentID, accountID, salepersonID, fromDate, toDate, null, MachineID).ToList();
                if (data.Count > 0)
                {

                    List<MachinesDTO> machines = new List<MachinesDTO>();
                    List<DragDropDTO> orders = new List<DragDropDTO>();
                    if (departmentID != null && departmentID > 0)
                    {
                        machines.Add(new MachinesDTO { MachineId = null, MachineName = "Pending" });

                        var machineList = db.tbl_OrderMachine.Where(x => x.DeptID == departmentID && x.IsDeleted != true).Select(x => new { MachineAttachedID = x.ID, MachineAttached = x.OrderMachine }).ToList();//data.Where(x => x.DepartmentID == departmentID).Select(x => new { x.MachineAttachedID, x.MachineAttached }).Distinct().ToList();

                        if (machineList != null && machineList.Count > 0)
                        {
                            foreach (var i in machineList)
                            {
                                if (i.MachineAttachedID > 0 && !i.MachineAttached.Trim().ToLower().Equals("ready to deliver"))
                                {
                                    machines.Add(new MachinesDTO { MachineId = i.MachineAttachedID, MachineName = i.MachineAttached });
                                }
                            }

                        }
                        machines.Add(new MachinesDTO { MachineId = 48, MachineName = "Ready To Deliver" });

                        var orderList = data.Where(x => x.DepartmentID == departmentID).ToList();

                        if (orderList != null && orderList.Count > 0)
                        {
                            foreach (var o in machines)
                            {
                                if (o != null)
                                {
                                    if (o.MachineId == null)//Pending
                                    {
                                        var List = orderList?.Where(x => x.MachineAttachedID == null).OrderBy(x=>x.ItemRank).ToList();
                                        if (List?.Count > 0)
                                        {
                                            foreach (var i in List)
                                            {
                                                orders.Add(new DragDropDTO { OrderID = i.OrderID, MachineName = "Pending", CustomerPO = i.CustomerPO, Customer = i.Name, MachineId = i.MachineAttachedID,ProductID = i.ProductID,ProductName = i.ProductName, Qty = Convert.ToInt32(i.Qty),SaleDetailId = i.SaleDetailId });
                                            }

                                        }
                                    }

                                    else
                                    {
                                        var List = orderList?.Where(x => x.MachineAttachedID == o.MachineId).OrderBy(x => x.ItemRank).ToList();
                                        if (List?.Count > 0)
                                        {
                                            foreach (var i in List)
                                            {
                                                orders.Add(new DragDropDTO { OrderID = i.OrderID, MachineName = i.MachineAttached, CustomerPO = i.CustomerPO, Customer = i.Name, MachineId = i.MachineAttachedID, ProductID = i.ProductID, ProductName = i.ProductName, Qty = Convert.ToInt32(i.Qty), SaleDetailId = i.SaleDetailId });

                                            }

                                        }
                                    }
                                }

                            }

                        }


                    }

                    Tuple<List<DragDropDTO>, List<MachinesDTO>> combinedList = new Tuple<List<DragDropDTO>, List<MachinesDTO>>(orders, machines);
                    ViewBag.BoardWidthCount = combinedList?.Item2?.Count() + 1;
                    return PartialView("_DNDOutSide",combinedList);
                }

            }
            return View();

        }

        //public ActionResult DepartmentWiseOrdersIndex_ListView(bool? btn, int? accountID, DateTime? fromDate, DateTime? toDate, int? BranchID, int? salepersonID, int? prodID, int? departmentID, int? orderID, int? MachineID)
        //{
        //    ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
        //    ViewBag.product = db.tbl_Product.Where(x => x.isActive != 0).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();

        //    if (User.IsInRole("Satin Manager"))
        //    {
        //        ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

        //    }
        //    else if (User.IsInRole("Sticker Manager"))
        //    {
        //        ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

        //    }
        //    else if (User.IsInRole("Offset Manager"))
        //    {
        //        ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

        //    }

        //    else
        //    {
        //        ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

        //    }
        //    ViewBag.orderMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.OrderMachine }).ToList();
        //    ViewBag.saleperson = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.SalePerson }).ToList();
        //    ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
        //    ViewBag.DieCuttingMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 3).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
        //    ViewBag.MachineTypes = db.tbl_OrderMachineTypes.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.TypeName }).ToList();

        //    //ViewBag.RedirectLink = "/Sales/DepartmentWiseOrdersIndex";
        //    var response = RSharp.RestSharpClient(1);
        //    if (response == true)
        //    {
        //        return RedirectToActionPermanent("CustomLogOff", "Account");
        //    }

        //    List<object> statusList = new List<object>();
        //    statusList.Add(new { Value = 1, Name = "Completed" });
        //    //statusList.Add(new { Value = 0, Name = "Not-Completed" });
        //    ViewBag.OrderStatus = statusList.ToList();
        //    departmentID = 2;

        //        List<GetJobOrderListProductDepartmentWiseSP_Result> data = db.GetJobOrderListProductDepartmentWiseSP(orderID, prodID, departmentID, accountID, salepersonID, fromDate, toDate, null, MachineID).ToList();
        //        if (data.Count > 0)
        //        {

        //            List<MachinesDTO> machines = new List<MachinesDTO>();
        //            List<DragDropDTO> orders = new List<DragDropDTO>();
        //            if (departmentID != null && departmentID > 0)
        //            {
        //                machines.Add(new MachinesDTO { MachineId = null, MachineName = "Pending" });

        //                var machineList = db.tbl_OrderMachine.Where(x => x.DeptID == departmentID && x.IsDeleted != true).Select(x => new { MachineAttachedID = x.ID, MachineAttached = x.OrderMachine }).ToList();//data.Where(x => x.DepartmentID == departmentID).Select(x => new { x.MachineAttachedID, x.MachineAttached }).Distinct().ToList();

        //                if (machineList != null && machineList.Count > 0)
        //                {
        //                    foreach (var i in machineList)
        //                    {
        //                        if (i.MachineAttachedID > 0 && !i.MachineAttached.Trim().ToLower().Equals("ready to deliver"))
        //                        {
        //                            machines.Add(new MachinesDTO { MachineId = i.MachineAttachedID, MachineName = i.MachineAttached });
        //                        }
        //                    }

        //                }
        //                machines.Add(new MachinesDTO { MachineId = 48, MachineName = "Ready To Deliver" });

        //                var orderList = data.Where(x => x.DepartmentID == departmentID).ToList();

        //                if (orderList != null && orderList.Count > 0)
        //                {
        //                    foreach (var o in machines)
        //                    {
        //                        if (o != null)
        //                        {
        //                            if (o.MachineId == null)//Pending
        //                            {
        //                                var List = orderList?.Where(x => x.MachineAttachedID == null).ToList();
        //                                if (List?.Count > 0)
        //                                {
        //                                    foreach (var i in List)
        //                                    {
        //                                        orders.Add(new DragDropDTO { OrderID = i.OrderID, MachineName = "Pending", CustomerPO = i.CustomerPO, Customer = i.Name, MachineId = i.MachineAttachedID });
        //                                    }

        //                                }
        //                            }

        //                            else
        //                            {
        //                                var List = orderList?.Where(x => x.MachineAttachedID == o.MachineId).ToList();
        //                                if (List?.Count > 0)
        //                                {
        //                                    foreach (var i in List)
        //                                    {
        //                                        orders.Add(new DragDropDTO { OrderID = i.OrderID, MachineName = i.MachineAttached, CustomerPO = i.CustomerPO, Customer = i.Name, MachineId = i.MachineAttachedID });

        //                                    }

        //                                }
        //                            }
        //                        }

        //                    }

        //                }


        //            }



        //            Tuple<List<DragDropDTO>, List<MachinesDTO>> combinedList = new Tuple<List<DragDropDTO>, List<MachinesDTO>>(orders, machines);

        //        return View("TESTDND", combinedList);
        //    }

        //        return View("TESTDND");

        //    //var data = db.tbl_SalesOrder1.Where(a => a.IsActive == true && a.IsDeleted == false).ToList();

        //}

        public JsonResult GetSaleOrderDetails(int? OrderId, string currentUserRole, int DeptID, int ProductID)
        {
            List<object> objectList = new List<object>();
            if (string.IsNullOrWhiteSpace(currentUserRole))
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            // int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (OrderId > 0 && DeptID > 0)
            {
                var SaleOrderInv = db.tbl_SalesOrder.Where(x => x.OrderID == OrderId).FirstOrDefault();
                var qry = db.tbl_Customer.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(p => new { Value = p.AccountID, Name = p.Name }).FirstOrDefault();
                var qry1 = db.tbl_SalesOrder.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(p => new { Value = p.BranchID, Name = p.tbl_Branch.BranchName }).FirstOrDefault();
                var qry2 = db.tbl_Customer.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(x => x.CreditLimit).FirstOrDefault();
                var FinalAmount = (SaleOrderInv.VAT == 0) ? SaleOrderInv.TotalAmount : SaleOrderInv.TotalAmount + SaleOrderInv.VAT;
                var Discount = SaleOrderInv.DiscountAmount;
                //string SalesDate = Convert.ToDateTime(SaleOrderInv.SalesDate).ToShortDateString();
                var SalePersonAccID = SaleOrderInv.SalePersonAccID ?? 0;
                var SubAmount = SaleOrderInv.TotalAmount + SaleOrderInv.DiscountAmount;
                var TotalAmount = SaleOrderInv.TotalAmount;
                var Tax = SaleOrderInv.VAT;
                var CreditDays = SaleOrderInv.CreditDays;
                var AmountPaid = SaleOrderInv.AmountPaid;
                var chequeDate = SaleOrderInv.ChequeDate;
                var salesDate = SaleOrderInv.SalesDate;
                //var expectedDeliveryDate = SaleOrderInv.ExpectedDeliveryDate ?? null;
                var Paytype = db.tbl_PaymentTypes.Where(x => x.ID == SaleOrderInv.PaymentTypeID).Select(x => new { Value = x.ID, Name = x.Name }).FirstOrDefault() == null ? null : db.tbl_PaymentTypes.Where(x => x.ID == SaleOrderInv.PaymentTypeID).Select(x => new { Value = x.ID, Name = x.Name }).FirstOrDefault();
                var Cheque = SaleOrderInv.ChequeNo;
                var ChqDate = SaleOrderInv.ChequeDate;
                var ProductsList = db.tbl_SaleDetails.Where(x => x.OrderID == OrderId && (x.IsDeleted != true || x.IsDeleted == null) && x.tbl_Product.DepartmentID == DeptID && x.ProductID == ProductID).
                    Select(p => new
                    {
                        ProductID = p.ProductID,
                        ExpectedDeliveryDate = p.ExpectedItemDeliveryDate ?? null,
                        Cat = p.tbl_Product.tbl_VehicleCode.VehicleCode,
                        CatName = p.tbl_Product.VehicleCodeID,
                        PartNo = p.PartNo,
                        Desc = p.tbl_Product.Description,
                        UnitCode = p.UnitCode,
                        Qty = p.Qty,
                        ReadyQty = p.ReadyQty,
                        QtyBalanced = p.QtyBalanced,
                        QtyDelivered = p.QtyDelivered,
                        Packet = p.Packet,
                        IsMinor = p.IsMinor,
                        IsPack = p.IsPack,
                        UnitID = p.UnitID,
                        LevelID = p.LevelID,
                        MinorDivisor = p.MinorDivisor,
                        UnitPerCarton = p.UnitPerCarton,
                        CardQuality = p.tbl_Product.CardQuality == null ? "-" : p.tbl_Product.tbl_Card_Quality.CardName,
                        CardQualityID = p.tbl_Product.CardQuality == null ? null : p.tbl_Product.CardQuality,
                        SalePrice = p.SalePrice,
                        UnitPrice = p.UnitPrice ?? 0,
                        PTotal = p.Total,
                        RollID = p.tbl_Product.RollID,

                        p.MachineImpression,
                        CardLength = p.tbl_Product.CardLength,
                        CardWidth = p.tbl_Product.CardWidth,
                        CutLength = p.tbl_Product.CutLength,
                        CutWidth = p.tbl_Product.CutWidth,
                        Impression = p.tbl_Product.NOOfImpressions,//* ((p.tbl_Product.TextColor * p.tbl_Product.TextCost)
                        //+ (p.tbl_Product.GroundColor * p.tbl_Product.GroundCost)),
                        p.OrderMachineID,
                        TotalSheets = p.tbl_Product.TotalSheets,
                        TotalMeters = p.tbl_Product.TotalMeters,
                        OrderMachine = p.tbl_OrderMachine.OrderMachine ?? "-",
                        p.StoreStatusID,
                        StoreStatus = p.tbl_StoreStatus.StoreStatus ?? "-",
                        p.DeliveryStatusID,
                        DeliveryStatus = p.tbl_DeliveryStatus.DeliveryStatus ?? "-",
                        p.NoOfSheets,
                        p.tbl_Product.SplinterCount,
                        p.tbl_Product.TotalWinInches,

                        SplinterID = p.tbl_Product.SplinterID,
                        Upping = p.tbl_Product.Upping,
                        JobSize = (p.tbl_Product.HSheetSize.ToString() + " X " + p.tbl_Product.WSheetSize.ToString()).ToString(),
                        GSM = p.tbl_Product.CardGSM,
                        RawSize = p.tbl_Product.RawSizeSheets,
                        SplitSheets = p.tbl_Product.Split,
                        PrintingRatePerRoll = p.tbl_Product.PrintingRatePerRoll,
                        HSheetSize = p.tbl_Product.HSheetSize,
                        WSheetSize = p.tbl_Product.WSheetSize,
                        RollCost = p.tbl_Product.RollCost,
                        Split = p.tbl_Product.Split,
                        NOOfImpressions = p.tbl_Product.NOOfImpressions,
                        PrintingMachine = p.tbl_Product.tbl_PrintingMachine == null ? "-" : p.tbl_Product.tbl_PrintingMachine.PrintingMachine ?? "-",
                        PrintingMachineID = p.tbl_Product.PrintingMachineID,
                        Cylinder = p.tbl_Product.tbl_Cylinder == null ? "-" : p.tbl_Product.tbl_Cylinder.Cylinder ?? "-",
                        CylinderID = p.tbl_Product.CylinderID,
                        Length = p.tbl_Product.Length,
                        Width = p.tbl_Product.Width,
                        CylinderLength = p.tbl_Product.tbl_Cylinder == null ? 0 : p.tbl_Product.tbl_Cylinder.CylinderLength,
                        AroundUps = p.tbl_Product.AroundUps,
                        NoOfPrintingColor = p.tbl_Product.NoOfPrintingColors,
                        Roll = p.tbl_Product.tbl_Roll == null ? "-" : p.tbl_Product.tbl_Roll.Roll ?? "-",
                        TotalPcInRoll = p.tbl_Product.TotalPcsInRoll,
                        WastagePcInRoll = p.tbl_Product.WastagePcsInRoll,
                        TotalReqRoll = p.tbl_Product.TotalRequiredRolls,
                        FinishingType = p.tbl_Product.tbl_FinishingType == null ? "-" : p.tbl_Product.tbl_FinishingType.FinishingType ?? "-",
                        FinishingTypeID = p.tbl_Product.FinishingTypeID,
                        BlockType = p.tbl_Product.tbl_BlockType == null ? "-" : p.tbl_Product.tbl_BlockType.BlockType ?? "-",
                        BlockTypeID = p.tbl_Product.BlockTypeID,

                        StickerMachineID = p.tbl_Product.StickerMachineID,
                        LSize = p.tbl_Product.LSize,
                        WSize = p.tbl_Product.WSize,
                        RepeatLength = p.tbl_Product.RepeatLength,
                        StickerCylinderID = p.tbl_Product.StickerCylinderID,
                        Across = p.tbl_Product.Across,
                        ReelSize = p.tbl_Product.ReelSize,
                        RunningMeter = p.tbl_Product.RunningMeter,
                        StickerQualityID = p.tbl_Product.StickerQualityID,
                        KGRequired = p.tbl_Product.KGRequired,
                        MaterialCostPerMeter = p.tbl_Product.MaterialCostPerMeter,
                        TotalMaterialCost = p.tbl_Product.TotalMaterialCost,
                        MachineID = p.tbl_Product.MachineID,
                        GroundColor = p.tbl_Product.GroundColor,
                        GroundCost = p.tbl_Product.GroundCost,
                        TextColor = p.tbl_Product.TextColor,
                        TextCost = p.tbl_Product.TextCost,
                        NoOfPlates = p.tbl_Product.NoOfPlates,
                        PlateCost = p.tbl_Product.PlateCost,
                        PrintingCost = p.tbl_Product.PrintingCost,
                        LaminationCost = p.tbl_Product.LaminationCost,
                        DieCost = p.tbl_Product.DieCost,
                        DieCuttingCost = p.tbl_Product.DieCuttingCost,
                        UVCost = p.tbl_Product.UVCost,
                        SlittingCost = p.tbl_Product.SlittingCost,
                        DesignCost = p.tbl_Product.DesignCost,
                        FoilPrintingCost = p.tbl_Product.FoilPrintingCost,
                        FinalCostPerPc = p.tbl_Product.FinalCostPerPc,
                        MarkupRate = p.tbl_Product.MarkupRate,
                        MarkedupPrice = p.tbl_Product.MarkedupPrice,

                        Wastage = p.tbl_Product.Wastage,
                        TotalM2ReqIncludingWastage = p.tbl_Product.TotalM2ReqIncludingWastage,
                        TotalQtyIncludingWastage = p.tbl_Product.TotalQtyIncludingWastage,
                        RequiredM2 = p.tbl_Product.RequiredM2,
                        RunningMeterIncludingWastage = p.tbl_Product.RunningMeterIncludingWastage,
                        StickerRibbonID_T = p.tbl_Product.StickerRibbonID_T,
                        StickerRibbonID_F = p.tbl_Product.StickerRibbonID_F,
                        StickerRibbonID_S = p.tbl_Product.StickerRibbonID_S,
                        StickerRibbonID_RFID = p.tbl_Product.StickerRibbonID_RFID,
                        StickerDieCutiingID = p.tbl_Product.StickerDieCutiingID,
                        SlittingOMachineID = p.SlittingOMachineID,
                        DieCuttingOMachineID = p.DieCuttingOMachineID,
                        UVOMachineID = p.UVOMachineID,
                        LaminationOMachineID = p.LaminationOMachineID,
                        FoilOMachineID = p.FoilOMachineID,
                        EmbosingOMachineID = p.EmbosingOMachineID

                    }).FirstOrDefault();

                StringBuilder deliveryData = new StringBuilder();
                var renderDeliveryStatusDD = db.tbl_DeliveryStatus.Where(x => x.IsDeleted != true).Select(x => new { ID = x.ID, Value = x.DeliveryStatus }).ToList();

                StringBuilder storeData = new StringBuilder();
                var renderStoreStatusDD = db.tbl_StoreStatus.Where(x => x.IsDeleted != true).Select(x => new { ID = x.ID, Value = x.StoreStatus }).ToList();

                StringBuilder orderMachineData = new StringBuilder();
                var renderSlittingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 1 && x.DeptID == DeptID).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
                var renderPrintingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 2 && x.DeptID == DeptID).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
                var renderDieCuttingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 3 && x.DeptID == DeptID).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
                var renderUVMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 4 && x.DeptID == DeptID).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
                var renderLaminationMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 5 && x.DeptID == DeptID).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
                var renderFoilMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 6 && x.DeptID == DeptID).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
                var renderEmbosingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 7 && x.DeptID == DeptID).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();

                var Banks = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 18).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                int BankId = 0;
                foreach (var i in Banks)
                {
                    if (i.Name.Equals(SaleOrderInv.BankName))
                    {
                        BankId = i.Value;
                    }
                }
                var Bank = db.tbl_SalesOrder.Where(x => x.OrderID == OrderId).Select(p => new { Value = BankId, Name = p.BankName }).FirstOrDefault();
                var PayStatus = 0;

                if (SaleOrderInv.PaymentStatus == "UnPaid")
                {
                    PayStatus = 3;
                }
                else if (SaleOrderInv.PaymentStatus == "Paid")
                {
                    PayStatus = 1;
                }
                if (SaleOrderInv.PaymentStatus == "Partial Paid")
                {
                    PayStatus = 2;
                }



                objectList.Add(new
                {
                    Qry = qry,
                    Qry1 = qry1,
                    Qry2 = qry2,
                    chequeDate = chequeDate,
                    SalePersonAccID = SalePersonAccID,
                    salesDate = salesDate,
                    //expectedDeliveryDate = expectedDeliveryDate,
                    //ExpectedDeliveryDate = Convert.ToDateTime(SaleOrderInv.ExpectedDeliveryDate).ToShortDateString(),
                    FinalAmount = FinalAmount,
                    Discount = Discount,
                    SubAmount = SubAmount,
                    TotalAmount = TotalAmount,
                    CreditDays = CreditDays,
                    Tax = Tax,
                    AmountPaid = AmountPaid,
                    Paytype = Paytype,
                    PayStatus = PayStatus,
                    Cheque = Cheque,
                    Bank = Bank,
                    ProductsList = ProductsList,
                    deliveryDataDD = renderDeliveryStatusDD,
                    storeDataDD = renderStoreStatusDD,

                    slittingMachineDataDD = renderSlittingMDD,
                    printingMachineDataDD = renderPrintingMDD,
                    dieCuttingMachineDataDD = renderDieCuttingMDD,
                    uvMachineDataDD = renderUVMDD,
                    laminationMachineDataDD = renderLaminationMDD,
                    foilMachineDataDD = renderFoilMDD,
                    embosingMachineDataDD = renderEmbosingMDD,
                    userRole = currentUserRole

                });
            }
            return Json(objectList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Create()
        {

            GetViewBagValues();
            return View();
        }

        public JsonResult UploadExcel(FormCollection formCollection)
        {

            var prodList = new List<ExcelProductDTO>();
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files[0];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    var epoch = new DateTime(1970, 1, 1, 5, 0, 0); //--5 for GMT 5
                    long time = Convert.ToInt64((DateTime.Now - epoch).TotalSeconds);
                    #region Save File for future record 
                    string docName = time + Path.GetFileName(file.FileName);
                    string docPath = "/Content/Excels/Products/" + docName;
                    file.SaveAs(Server.MapPath(docPath));
                    #endregion
                    string fileName = file.FileName;
                    string ext = Path.GetExtension(docPath);
                    if (ext == ".xlsx")
                    {
                        string fileContentType = file.ContentType;
                        byte[] fileBytes = new byte[file.ContentLength];
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                        using (var package = new ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                try
                                {
                                    var Desc = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 1].Value, null);
                                    // var catName = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 2].Value, null);
                                    //  var packaging = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 3].Value, null);
                                    var qty = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 2].Value, null);
                                    //var vendor = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 3].Value, null);
                                    //var prodSize = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 4].Value, null);
                                    //var packaging = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 5].Value, null);
                                    //var partNo = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 6].Value, null);
                                    //var uniqueNumber = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 7].Value, null);
                                    int uniqueNumber = rowIterator;


                                    // var vendor = (workSheet.Cells[rowIterator, 3].Text);
                                    if (!string.IsNullOrWhiteSpace(Desc) && Convert.ToInt32(qty) > 0)
                                    {
                                        var product = new ExcelProductDTO();
                                        product.Description = Desc;
                                        product.Qty = Convert.ToInt32(qty);
                                        product.RowID = uniqueNumber;
                                        prodList.Add(product);
                                    }
                                    else
                                    {
                                        return Json("Invalid Column Defined", JsonRequestBehavior.AllowGet);
                                    }
                                }
                                catch (Exception ex)
                                {

                                    while (ex.InnerException != null)
                                    {
                                        ex = ex.InnerException;
                                    }
                                    return Json(ex.Message, JsonRequestBehavior.AllowGet);

                                }

                            }

                            if (prodList != null && prodList.Count > 0)
                            {
                                return Json(prodList, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        return Json("File type is incorrect.", JsonRequestBehavior.AllowGet);

                    }
                }
            }

            return Json(-1, JsonRequestBehavior.AllowGet);
        }

        //Save Job Order
        public JsonResult SaveOrder(tbl_SalesOrder model, List<ItemWisePackageDTO> packageModel)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    int c = 0;
                    if (model.tbl_SaleDetails != null)
                    {
                        foreach (var i in model.tbl_SaleDetails)
                        {
                            i.PartNo = db.tbl_Product.Where(x => x.ProductID == i.ProductID).Select(x => x.PartNo).FirstOrDefault() ?? "-";
                            if (packageModel == null)
                            {
                                packageModel = new List<ItemWisePackageDTO>();
                                packageModel.Add(new ItemWisePackageDTO
                                {
                                    ItemID = i.ProductID,
                                    Description = i.PartNo,
                                    Qty = Convert.ToInt32(i.Qty)
                                });
                            }
                            else
                            {
                                if (i.isSameAsItemQty == true && !packageModel.Any(x => x.ItemID == i.ProductID))
                                {
                                    packageModel.Add(new ItemWisePackageDTO
                                    {
                                        ItemID = i.ProductID,
                                        Description = i.PartNo,
                                        Qty = Convert.ToInt32(i.Qty)
                                    });
                                }
                                else if (i.isSameAsItemQty == true && packageModel.Any(x => x.ItemID == i.ProductID))
                                {
                                    t.Rollback();
                                    return Json(-6, JsonRequestBehavior.AllowGet);
                                }
                            }

                        }

                        List<int> ItemIDs = model.tbl_SaleDetails.Select(x => x.ProductID).Distinct().ToList();
                        if (ItemIDs.Count > 0)
                        {
                            foreach (var i in ItemIDs)
                            {
                                if (model.tbl_SaleDetails.Where(x => x.ProductID == i).Sum(x => x.Qty) != packageModel.Where(x => x.ItemID == i).Sum(x => x.Qty))
                                {
                                    t.Rollback();
                                    return Json(-5, JsonRequestBehavior.AllowGet);
                                }

                            }
                        }

                    }
                    model.AddBy = com.GetUserID();
                    model.AddOn = Helper.PST();
                    model.IsDeleted = false;
                    model.IsSaleOrder = true;
                    model.SOID = GetCode();
                    model.Barcode = "JO-" + model.SOID.ToString();
                    model.SalesCode = "JO-" + model.SOID.ToString();
                    db.tbl_SalesOrder.Add(model);
                    db.SaveChanges();

                    if (packageModel != null || packageModel.Count > 0)
                    {

                        List<tbl_SOItemWisePackageDetails> list = new List<tbl_SOItemWisePackageDetails>();
                        foreach (var i in packageModel)
                        {
                            //Guid guid = Guid.NewGuid();

                            tbl_SOItemWisePackageDetails det = new tbl_SOItemWisePackageDetails();
                            det.AddedOn = Helper.PST();
                            det.IsDeleted = false;
                            det.ItemID = i.ItemID;
                            det.OrderID = model.OrderID;
                            det.Description = string.IsNullOrWhiteSpace(i.Description) ? "-" : i.Description.ToString().Trim();
                            det.ProductQty = i.Qty;
                            det.PackageId = Guid.NewGuid().ToString().Trim();
                            list.Add(det);
                        }
                        db.tbl_SOItemWisePackageDetails.AddRange(list);
                        c = db.SaveChanges();
                    }


                    if (c > 0)
                    {
                        UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Job Order #: " + model.OrderID.ToString());
                        t.Commit();
                        return Json(model.OrderID);
                    }
                    else
                    {
                        t.Rollback();
                        return Json(-2);
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.InnerException.Message);
                return Json(200);
            }

        }

        [Authorize]
        public ActionResult UpdateSaleOrder(int OrderID, int deptID, int ProductID)
        {
            try
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                string userRoleId = db.AspNetUserRoles.Where(x => x.UserId.Equals(currentUserId)).Select(x => x.RoleId).FirstOrDefault();
                string userRole = "";

                if (!string.IsNullOrWhiteSpace(userRoleId))
                {
                    userRole = db.AspNetRoles.Where(x => x.Id.Equals(userRoleId)).Select(x => x.Name).FirstOrDefault();

                }
                //var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);

                ViewBag.OrdId = OrderID;
                ViewBag.DeptID = deptID;
                ViewBag.ItemID = ProductID;
                ViewBag.userRole = userRole.ToString();
                GetViewBagValues();

                var data = db.tbl_Product.Where(x => x.ProductID == ProductID).FirstOrDefault();

                if (data.OffsetTypeID != 3 && deptID == 1)//Offset Department
                {
                    return View("UpdateOffsetSaleOrder");
                }
                if (data.OffsetTypeID == 3 && deptID == 1)//Tafata Department
                {
                    return View("UpdateTafataOffsetSaleOrder");
                }
                else if (deptID == 2)//Satin Department
                {
                    return View("UpdateSatinSaleOrder");
                }
                else if (deptID == 3)//Sticker Department
                {
                    return View("UpdateStickerSaleOrder");
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return View(e.Message);
            }
        }

        public async Task<JsonResult> GetDeptartmentWiseMachineList(int DeptID)
        {
            try
            {
                var machineList = await db.tbl_OrderMachine.Where(x => x.DeptID == DeptID && x.IsDeleted != true).Select(x => new { Name = x.OrderMachine, Value = x.ID }).ToListAsync();
                if (machineList.Count > 0) { return Json(machineList, JsonRequestBehavior.AllowGet); }
                else
                {
                    var machineListWODeptID = await db.tbl_OrderMachine.Where(x => x.IsDeleted != true).Select(x => new { Name = x.OrderMachine, Value = x.ID }).ToListAsync();
                    if (machineListWODeptID.Count > 0) { return Json(machineListWODeptID, JsonRequestBehavior.AllowGet); }
                }

                return Json(-1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public FileResult PackageReport(int OrderID, int OffsetTypeID, int deptID, int ProductID, String ReportType)
        {
            try
            {
                LocalReport lo = new LocalReport();
                if (deptID == 1 && OffsetTypeID != 3)
                {
                    ReportDataSource rs = new ReportDataSource();
                    rs.Name = "PackageDataset";
                    rs.Value = db.GetJobOrderDetailSummaryProductDepartmentWiseSP(OrderID, ProductID, deptID, null, null, null, null, null);
                    lo.DataSources.Add(rs);
                    lo.ReportPath = Server.MapPath("~/Models/Reports/OffsetReport.rdlc");

                }
                else if (deptID == 1 && OffsetTypeID == 3)
                {
                    ReportDataSource rs = new ReportDataSource();
                    rs.Name = "PackageDataset";
                    rs.Value = db.GetJobOrderDetailSummaryProductDepartmentWiseSP(OrderID, ProductID, deptID, null, null, null, null, null);
                    lo.DataSources.Add(rs);
                    lo.ReportPath = Server.MapPath("~/Models/Reports/TafataReport.rdlc");

                }
                else if (deptID == 2)
                {
                    ReportDataSource rs = new ReportDataSource();
                    rs.Name = "PackageDataset";
                    rs.Value = db.GetJobOrderDetailSummaryProductDepartmentWiseSP(OrderID, ProductID, deptID, null, null, null, null, null);
                    lo.DataSources.Add(rs);
                    lo.ReportPath = Server.MapPath("~/Models/Reports/SatinReport.rdlc");

                }
                else if (deptID == 3)
                {
                    ReportDataSource rs = new ReportDataSource();
                    rs.Name = "PackageDataset";
                    rs.Value = db.GetJobOrderDetailSummaryProductDepartmentWiseSP(OrderID, ProductID, deptID, null, null, null, null, null);
                    lo.DataSources.Add(rs);
                    lo.ReportPath = Server.MapPath("~/Models/Reports/StickerReport.rdlc");

                }

                string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
                Warning[] warnings;
                string[] streams;
                string mimeType;
                byte[] renderedBytes;
                string encoding;
                string fileNameExtension;
                renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

                return new FileContentResult(renderedBytes, mimeType);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return null;
            }
        }

        public JsonResult CreateChallanFromSales(tbl_SalesOrder model1, List<Models.DTO.StockLog> modelStockLog1, List<ItemWisePackageDTO> pkgList, bool? isQuote, int? QuoteOrderID, int? bankAccId, int? SaleOrderID, string Driver, string Transport, string DriverVehicleNo, string GrossWeight, string NetWeight, bool fsc_Cert, bool grs_Cert, bool oeko_tex_Cert)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            POSOman.Models.BLL.SalesOrder sales = new Models.BLL.SalesOrder();
            branchId = 9001;// model1.BranchID;
            if (model1 == null || model1.OrderID <= 0)
            {
                return Json(-2, JsonRequestBehavior.AllowGet);
            }
            else if (string.IsNullOrWhiteSpace(model1.CustomerPO))
            {
                return Json(-3, JsonRequestBehavior.AllowGet);
            }
            var orderID = sales.createOffsetChallan(model1, pkgList, modelStockLog1, bankAccId, 3, SaleOrderID, Driver, Transport, DriverVehicleNo, GrossWeight, NetWeight, fsc_Cert, grs_Cert, oeko_tex_Cert);
            if (Convert.ToInt32(orderID) > 0)
            {
                var custInvoiceType = db.tbl_Customer.Where(x => x.AccountID == model1.AccountID).Select(x => x.InvoiceType).FirstOrDefault();
                var obj = new { JOID = model1.OrderID, ChallanID = Convert.ToInt32(orderID), InvoiceType = Convert.ToInt32(custInvoiceType) };
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Challan Created #: " + orderID.ToString());
                return Json(obj, JsonRequestBehavior.AllowGet);

            }
            return Json(null, JsonRequestBehavior.AllowGet);
            //else {
            //    var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
            //}
        }

        public JsonResult SaveEditedOffsetOrder(tbl_SalesOrder model1, List<Models.DTO.StockLog> modelStockLog1, bool? isQuote, int? QuoteOrderID, int? bankAccId, int? SaleOrderID)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            POSOman.Models.BLL.SalesOrder sales = new Models.BLL.SalesOrder();
            branchId = 9001;// model1.BranchID;
            if (model1 == null || model1.OrderID <= 0)
            {
                return Json(-2);
            }
            var orderID = sales.createOffsetSales(model1, modelStockLog1, bankAccId, 3, SaleOrderID);
            return Json(orderID);
            //else {
            //    var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
            //}
        }
        public JsonResult UpdateMachineProcess(SaleProductDTO model)
        {
            try
            {

                if (model.OrderID > 0 && model.ProductID > 0 && model.AttachedMachinesList != null && model.AttachedMachinesList.Count > 0)
                {
                    var result = custom.UpdateMachineProcess(model);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                return Json(-1, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult TrelloTransferProcess(SaleProductDTO model)
        {
            try
            {

                if (model.OrderID > 0 && model.MachineID > 0 && model.ProductID > 0)
                {
                    var result = custom.TrelloTransferMachineProcess(model);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                return Json(-1, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult TransferProcess(SaleProductDTO model)
        {
            try
            {

                if (model.OrderID > 0 && model.ProductID > 0 && model.AttachedMachineID > 0 && model.DepartmentID > 0)
                {
                    var result = custom.TransferMachineProcess(model);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                return Json(-1, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DiscardSOQty(SaleProductDTO model)
        {
            try
            {

                if (model.OrderID > 0 && model.ProductID > 0 && model.DiscardingQty > 0 && model.DepartmentID > 0 && model.DeliveredQty >= 0)
                {
                    var result = custom.DiscardRemainingQty(model);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                return Json(-1, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult MoveSOQtyToStock(SaleProductDTO model)
        {
            try
            {

                if (model.OrderID > 0 && model.ProductID > 0 && model.MovingQty > 0 && model.DepartmentID > 0 && model.DeliveredQty >= 0)
                {
                    var result = custom.MoveQtyToStock(model);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                return Json(-1, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateSatinOrderProduct(SaleProductDTO model)
        {
            try
            {
                if (model.ExpectedDeliveryDate != null && model.SalesDate != null)
                {
                    if (model.ExpectedDeliveryDate < model.SalesDate)
                    {
                        return Json(-1, JsonRequestBehavior.AllowGet);
                    }
                    var result = custom.updateSatinOrderProduct(model);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                return Json(-2, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateStickerOrderProduct(SaleProductDTO model)
        {
            try
            {
                if (model.ExpectedDeliveryDate != null && model.SalesDate != null)
                {
                    if (model.ExpectedDeliveryDate < model.SalesDate)
                    {
                        return Json(-1, JsonRequestBehavior.AllowGet);
                    }
                    var result = custom.updateStickerOrderProduct(model);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                return Json(-2, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public FileResult Export(int id, String ReportType)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/SaleOrderReport.rdlc");

            ReportDataSource rs = new ReportDataSource();
            ReportDataSource packageRs = new ReportDataSource();
            rs.Name = "SaleOrderReport";
            //packageRs.Name = "PackageDetails";
            //rs.Value = db.SaleOrderInvoiceNew(id).Where(x => x.QtyDelivered > 0);
            rs.Value = db.GetPackageDetailsItemAndOrderIDWiseSP(id);
            //packageRs.Value = db.GetPackageDetailsOrderIDWiseSP(id,null);
            lo.DataSources.Add(rs);
            //lo.DataSources.Add(packageRs);


            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            //Response.AddHeader("Content-Disposition", "attachment; filename=SaleInvoice.pdf");

            return new FileContentResult(renderedBytes, mimeType);
        }

    }
}