﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class CurrencyController : Controller
    {

        UserActionsPerformed UserActions = new UserActionsPerformed();
        private dbPOS db = new dbPOS();
        [Authorize(Roles = "SuperAdmin,Admin")]
        // GET: Currency
        public ActionResult Index()
        {
            //int branchId = 0;            
            //if (Session["BranchID"] != null)
            //{
            //    branchId = Convert.ToInt32(Session["BranchID"]);
            //}
            //else
            //{
            //    var user = User.Identity;
            //    string currentUserId = User.Identity.GetUserId();
            //    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
            //    branchId = currentUser.BranchID;
            //}
            if (db.tbl_Currency.Count() <= 0)
            {
                return HttpNotFound();
            }
            else
            {
                //var id = db.tbl_Currency.Max(c => c.ID);
                //int Id = Convert.ToInt32(id);
                //tbl_Currency tbl_Currency = db.tbl_Currency.Where(c => c.ID == Id).FirstOrDefault();
                var tbl_Currency = db.tbl_Currency.ToList();
                if (tbl_Currency == null)
                {
                    return HttpNotFound();
                }
                return View(tbl_Currency);
            }
            
            
            //var tbl_Currency = db.tbl_Currency.Include(t => t.tbl_User);
            //return View(tbl_Currency.ToList());
        }

        // GET: Currency/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Currency tbl_Currency = db.tbl_Currency.Find(id);
            if (tbl_Currency == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Currency);
        }
        [Authorize(Roles = "SuperAdmin,Admin")]
        // GET: Currency/Create
        public ActionResult Create()
        {
            //ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName");
            return View();
        }

        // POST: Currency/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ExchangeRate",Exclude="CurrencyName,Code,Country,Symbol,RateInUSD,IsActive,AddOn,Addby,UpdateOn,UpdateBy,DeleteOn,DeleteBy,IsDeleted")] tbl_Currency tbl_Currency)
        {
            if (ModelState.IsValid)
            {               
                int branchId =9001;

                //if (Session["BranchID"] != null)
                //{
                //    branchId = Convert.ToInt32(Session["BranchID"]);
                //}
                //else
                //{                    
                //    var _currentUserId = User.Identity.GetUserId();
                //    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == _currentUserId);
                //    branchId = currentUser.BranchID;
                //}
                var user = User.Identity;
                var currentUserId = User.Identity.GetUserId();
                tbl_Currency.UserID = currentUserId;
                tbl_Currency.AddOn = DateTime.UtcNow.AddHours(5).Date;
                tbl_Currency.Code = "PKR";
                tbl_Currency.CurrencyName = "Rupees";
                tbl_Currency.IsDeleted = false;
                tbl_Currency.BranchID = branchId;
                db.tbl_Currency.Add(tbl_Currency);
                db.SaveChanges();

                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added Currency".ToString());
                return RedirectToAction("Index");
            }

           // ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Currency.Addby);
            return View(tbl_Currency);
        }

        // GET: Currency/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Currency tbl_Currency = db.tbl_Currency.Find(id);
            if (tbl_Currency == null)
            {
                return HttpNotFound();
            }
            //ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Currency.Addby);
            return View(tbl_Currency);
        }

        // POST: Currency/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CurrencyName,Code,Country,Symbol,RateInUSD,ExchangeRate,IsActive,AddOn,Addby,UpdateOn,UpdateBy,DeleteOn,DeleteBy,IsDeleted")] tbl_Currency tbl_Currency)
        {
            if (ModelState.IsValid)
            {

                db.Entry(tbl_Currency).State = EntityState.Modified;
                db.SaveChanges();

                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edited Currency".ToString());
                return RedirectToAction("Index");
            }
            //ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Currency.Addby);
            return View(tbl_Currency);
        }

        // GET: Currency/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Currency tbl_Currency = db.tbl_Currency.Find(id);
            if (tbl_Currency == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Currency);
        }

        // POST: Currency/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Currency tbl_Currency = db.tbl_Currency.Find(id);
            db.tbl_Currency.Remove(tbl_Currency);
            db.SaveChanges();
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Currency".ToString());
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
