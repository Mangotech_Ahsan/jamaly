﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using Microsoft.AspNet.Identity;
using AutoMapper;
using System.Web.Configuration;
using POSOman.Models.DTO;

namespace POSOman.Controllers
{
    public class ChequesController : Controller
    {
        private dbPOS db = new dbPOS();
        Models.BLL.Cheques chq = new Models.BLL.Cheques();
        [Authorize]
        // GET: Cheques
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Index(bool? isIssued)
        {
            int branchId = 0;            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            if (isIssued == false)
            {
                var tbl_Cheques = db.tbl_Cheques.Where(c => c.ChequeTypeID == 1 && c.BranchID == branchId).Include(t => t.tbl_Branch).Include(t => t.tbl_ChequeType).Include(t => t.tbl_JEntry);
                return View(tbl_Cheques.ToList());
            }
            else
            {
                var tbl_Cheques = db.tbl_Cheques.Where(c => c.ChequeTypeID == 2 && c.BranchID == branchId).Include(t => t.tbl_Branch).Include(t => t.tbl_ChequeType).Include(t => t.tbl_JEntry);
                return View(tbl_Cheques.ToList());
            }
        }
        // GET: Monthly Cheques
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult GetMonthlyIssuedCheques()
        {
            int branchId = 0;            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            List<GetMonthlyIssuedCheques_Result> chqs = db.GetMonthlyIssuedCheques().ToList();
            Mapper.CreateMap<GetMonthlyIssuedCheques_Result,Models.DTO.Cheques>();
            var cheques = Mapper.Map<List<GetMonthlyIssuedCheques_Result>, List<Models.DTO.Cheques>>(chqs);
            return View("Index", cheques);           
        }
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult GetMonthlyRecdCheques()
        {
            int branchId = 0;            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            List<GetMonthlyRecdCheques_Result> chqs = db.GetMonthlyRecdCheques().ToList();
            Mapper.CreateMap<GetMonthlyRecdCheques_Result, Models.DTO.Cheques>();
            var cheques = Mapper.Map<List<GetMonthlyRecdCheques_Result>, List<Models.DTO.Cheques>>(chqs);
            return View("Index", cheques);
        }
        // GET PDC Cheques

        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult GetPDCCheques(bool? isSearch, int? customerID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();


            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            if (isSearch == true)
            {
                List<GetPDCChequesDateWise_Result> cheques = db.GetPDCChequesDateWise(customerID,fromDate, toDate, BranchID).ToList();
                if (cheques.Count == 0)
                {
                    return PartialView("_PDCCheques");
                }
                else
                    return PartialView("_PDCCheques", cheques);
            }

            return View("PDCCheques");
        }
        // GET PDC Issued

        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult GetIssuedPDC(bool? isSearch, int? vendorID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.Vendor = db.tbl_Vendor.Select(c => new { Value = c.AccountID, Name = c.VendorCode +" | "+ c.Name }).ToList();            
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            if (isSearch == true)
            {
                List<GetIssuedPDCDateWise_Result> cheques = db.GetIssuedPDCDateWise(vendorID, fromDate, toDate, BranchID).ToList();
                if (cheques.Count == 0)
                {
                    return PartialView("_IssuedPDC");
                }
                else
                    return PartialView("_IssuedPDC", cheques);
            }

            return View("IssuedPDC");
        }
        // GET: Cheques/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Cheques tbl_Cheques = db.tbl_Cheques.Find(id);
            if (tbl_Cheques == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Cheques);
        }

        // GET: Cheques/Create
        public ActionResult Create()
        {
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "Code");
            ViewBag.ChequeTypeID = new SelectList(db.tbl_ChequeType, "ChequeTypeID", "ChequeType");
            ViewBag.JEntryId = new SelectList(db.tbl_JEntry, "JEntryId", "VoucherName");
            return View();
        }

        // POST: Cheques/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ChequeID,JEntryId,ChequeTypeID,BranchID,Amount,Description,ReferenceDetail,BankName,BankBranch,ChequeDate,ChequeNumber,ChequeType,Bearer,IsCleared,IsBounced,BounceDetails,ClearingDate,DepositDate,AddBy,AddOn,UpdateOn,UpdateBy,DeleteOn,DeleteBy,IsDeleted")] tbl_Cheques tbl_Cheques)
        {
            if (ModelState.IsValid)
            {
                db.tbl_Cheques.Add(tbl_Cheques);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "Code", tbl_Cheques.BranchID);
            ViewBag.ChequeTypeID = new SelectList(db.tbl_ChequeType, "ChequeTypeID", "ChequeType", tbl_Cheques.ChequeTypeID);
            ViewBag.JEntryId = new SelectList(db.tbl_JEntry, "JEntryId", "VoucherName", tbl_Cheques.JEntryId);
            return View(tbl_Cheques);
        }

        // GET: Cheques/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Cheques tbl_Cheques = db.tbl_Cheques.Find(id);
            if (tbl_Cheques == null)
            {
                return HttpNotFound();
            }
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "Code", tbl_Cheques.BranchID);
            ViewBag.ChequeTypeID = new SelectList(db.tbl_ChequeType, "ChequeTypeID", "ChequeType", tbl_Cheques.ChequeTypeID);
            ViewBag.JEntryId = new SelectList(db.tbl_JEntry, "JEntryId", "VoucherName", tbl_Cheques.JEntryId);
            return View(tbl_Cheques);
        }

        // POST: Cheques/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ChequeID,JEntryId,ChequeTypeID,BranchID,Amount,Description,ReferenceDetail,BankName,BankBranch,ChequeDate,ChequeNumber,ChequeType,Bearer,IsCleared,IsBounced,BounceDetails,ClearingDate,DepositDate,AddBy,AddOn,UpdateOn,UpdateBy,DeleteOn,DeleteBy,IsDeleted")] tbl_Cheques tbl_Cheques)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Cheques).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "Code", tbl_Cheques.BranchID);
            ViewBag.ChequeTypeID = new SelectList(db.tbl_ChequeType, "ChequeTypeID", "ChequeType", tbl_Cheques.ChequeTypeID);
            ViewBag.JEntryId = new SelectList(db.tbl_JEntry, "JEntryId", "VoucherName", tbl_Cheques.JEntryId);
            return View(tbl_Cheques);
        }

        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        // GET: Jentry/Delete/5
        public ActionResult BounceIssuedCheque(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            Models.DTO.PaymentLog jLog = new Models.DTO.PaymentLog();
            var query = from jlog in db.tbl_JEntryLog
                        join je in db.tbl_JEntry on jlog.JEntryID equals je.JEntryId
                        join jd in db.tbl_JDetail on je.JEntryId equals jd.JEntryID
                        join po in db.tbl_PurchaseOrder on jlog.OrderID equals po.OrderID
                        join acd in db.tbl_AccountDetails on jd.AccountID equals acd.AccountID
                        where jd.Cr > 0 && jlog.JEntryID == id
                        select new Models.DTO.JEntryLogModel
                        {
                            AccountName = acd.AccountName,
                            InvoiceNo = po.POID,
                            Amount = jlog.Amount ?? 0
                        };
            if (query == null)
            {
                return HttpNotFound();
            }
            return View(query.ToList());
        }
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        // POST: Entry/Delete/5
        [HttpPost, ActionName("BounceIssuedCheque")]
        [ValidateAntiForgeryToken]
        public ActionResult BounceIssuedChequeConfirmed(int id)
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                chq.BounceVendorPDC(id);
                return RedirectToAction("GetCheques", "Reports", new { isRec = 2 }); ;
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
        }
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        // GET: Jentry/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            //Models.DTO.PaymentLog jLog = new Models.DTO.PaymentLog();
            //var query = from jlog in db.tbl_JEntryLog
            //            join je in db.tbl_JEntry on jlog.JEntryID equals je.JEntryId
            //            join jd in db.tbl_JDetail on je.JEntryId equals jd.JEntryID
            //            join so in db.tbl_SalesOrder on jlog.OrderID equals so.OrderID
            //            join acd in db.tbl_AccountDetails on jd.AccountID equals acd.AccountID
            //            where jd.Cr > 0 && jlog.JEntryID == id && jd.EntryTypeID != 34
            //            select new Models.DTO.JEntryLogModel
            //            {
            //                AccountName = acd.AccountName,
            //                InvoiceNo = so.SOID,
            //                Amount = jlog.Amount ?? 0
            //            };
            tbl_Cheques query = db.tbl_Cheques.Where(x => x.JEntryId == id).FirstOrDefault();

            if (query == null)
            {
                return HttpNotFound();
            }
            return View(query);
        }
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        // POST: Entry/Delete/5
        [HttpPost]//, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(tbl_Cheques model)
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                if (model!=null && model.JEntryId>0)
                {
                    chq.DeleteReceivedEntry(model.JEntryId);
                    return RedirectToAction("GetCheques", "Reports", new { isRec = 1 }); ;

                }
                ModelState.AddModelError(String.Empty, "Id Is Null");
                return View(model);

            }
            catch (Exception err)
            {
                while (err.InnerException != null) { err = err.InnerException; }
                ModelState.AddModelError(String.Empty, err.Message);
                return View(model);
            }
        }

        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        // GET: 
        public ActionResult ClearCheque(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            //Models.DTO.PaymentLog jLog = new Models.DTO.PaymentLog();
            //var query = from jlog in db.tbl_JEntryLog
            //            join je in db.tbl_JEntry on jlog.JEntryID equals je.JEntryId
            //            join jd in db.tbl_JDetail on je.JEntryId equals jd.JEntryID
            //            join so in db.tbl_SalesOrder on jlog.OrderID equals so.OrderID
            //            join acd in db.tbl_AccountDetails on jd.AccountID equals acd.AccountID
            //            where jd.Cr > 0 && jlog.JEntryID == id
            //            select new Models.DTO.JEntryLogModel
            //            {
            //                AccountName = acd.AccountName,
            //                InvoiceNo = so.SOID,
            //                Amount = jlog.Amount ?? 0
            //            };
            //ViewBag.JEntryID = id;

            List<DropDownDTO> dd = new List<DropDownDTO>();

            var data = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();

            if(data!=null && data.Count > 0)
            {
                
                foreach (var i in data)
                {
                    dd.Add(new DropDownDTO
                    {
                        Name = i.Name,
                        Value = i.Value
                    });
                }
            }
            tbl_Cheques query = db.tbl_Cheques.Where(x => x.JEntryId == id).FirstOrDefault();
            //ViewBag.Bank = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 && acd.IsActive == true && (acd.IsDeleted!=true || acd.IsDeleted == null)).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            var ddData = (from acc in db.tbl_AccountDetails
                          where acc.AccountID == 1
                          select new DropDownDTO
                          {
                              Name = acc.AccountName,
                              Value = acc.AccountID
                          }).Concat(from acc in db.tbl_AccountDetails
                                    where acc.AccountTypeID == 27
                                    select
                         new DropDownDTO
                         {
                             Name = acc.AccountName + " | " + acc.Bank,
                             Value = acc.AccountID
                         }
                                                                                                                                                                               ).ToList();
            ViewBag.Bank = ddData.ToList();
            if (query == null)
            {
                return HttpNotFound();
            }
            return View(query);
        }
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        [HttpPost]//, ActionName("ClearCheque")]
        //[ValidateAntiForgeryToken]
        // POST: Clear                
        public ActionResult ClearCheque(tbl_Cheques model)
        {
            var ddData = (from acc in db.tbl_AccountDetails
                          where acc.AccountID == 1
                          select new DropDownDTO
                          {
                              Name = acc.AccountName ,
                              Value = acc.AccountID
                          }).Concat(from acc in db.tbl_AccountDetails
                                    where acc.AccountTypeID == 27
                                    select
                         new DropDownDTO
                         {
                             Name = acc.AccountName + " | " + acc.Bank,
                             Value = acc.AccountID
                         }
                                                                                                                                                                                 ).ToList();
            ViewBag.Bank = ddData.ToList();
            try
            {
                int branchId = 0;
                //tbl_Cheques m = new tbl_Cheques();
                //m = db.tbl_Cheques.Where(x => x.JEntryId == model.JEntryId).FirstOrDefault();

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                if(model!=null && model?.JEntryId>0 && model?.DepositBankAccountID > 0)
                {
                    chq.ChequeClearedEntry(model.JEntryId, model.DepositBankAccountID);
                    return RedirectToAction("GetCheques", "Reports", new { isRec = 1 }); 

                }
                ModelState.AddModelError(String.Empty, "Model Is Empty");
                return View(model) ;
            }
            catch (Exception err)
            {
                while (err.InnerException != null) { err = err.InnerException; }
                ModelState.AddModelError(String.Empty, err.Message);
                //return View("Index");
                return View(model) ;
            }
        }
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        // Clear Vendor Cheque               
        public ActionResult VendorChequeClear(int id)
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                chq.VendorChequeClearedEntry(id);
                return RedirectToAction("GetCheques", "Reports", new { isRec = 2 }); ;
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
