﻿using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.BLL;
using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class LoyaltyCardsController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();

        // GET: LoyaltyCards
        public ActionResult Index()
        {
            return View();
        }

        #region Add New Customer With Loyalty Card 
        // GET: Customer/Create
        public ActionResult CreateLoyaltyCustomer()
        {
            return View();
        }
        // Get Loyalty Customers 
        public ActionResult CustomerList()
        {
            return View(db.tbl_Customer.Where(cst => cst.LoyaltyCardID > 0).ToList());
        }
       
        // Get Loyalty card info for customer
        public ActionResult RedeemPoints(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var row = db.tbl_Customer.Where(cst => cst.AccountID == id).FirstOrDefault();
            tbl_LoyaltyCard loyaltyCard = db.tbl_LoyaltyCard.Find(row.LoyaltyCardID);
            if (loyaltyCard == null)
            {
                return HttpNotFound();
            }
            RedeemPointsDTO points = new RedeemPointsDTO();
            points.LoyaltyCardID = loyaltyCard.LoyaltyCardID;
            points.LoyaltyCardNo = loyaltyCard.LoyaltyCardNo;
            points.TotalPoints = loyaltyCard.TotalPoints;
            points.UsedPoints = loyaltyCard.UsedPoints;
            points.RedeemPoints = points.TotalPoints - points.UsedPoints;
            return PartialView("_RedeemPoints", points);
        }
        [HttpPost]
        public async Task<ActionResult> RedeemPoints(RedeemPointsDTO redeem)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            if (!ModelState.IsValid)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return View(Request.IsAjaxRequest() ? "_RedeemPoints" : "RedeemPoints", redeem);
            }
            bool isValid = true;

            var row = db.tbl_LoyaltyCard.Where(lc => lc.LoyaltyCardID == redeem.LoyaltyCardID).FirstOrDefault();
            db.Entry(row).State = EntityState.Modified;
            row.UsedPoints = row.UsedPoints + redeem.RedeemPoints;
            db.Entry(row).Property(p => p.LoyaltyCardNo).IsModified = false;
            db.Entry(row).Property(p => p.BarCode).IsModified = false;
            db.Entry(row).Property(p => p.CardType).IsModified = false;
            db.Entry(row).Property(p => p.DiscountAmount).IsModified = false;
            db.Entry(row).Property(p => p.DiscountOnAmount).IsModified = false;
            db.Entry(row).Property(p => p.DiscountPercent).IsModified = false;
            db.Entry(row).Property(p => p.TotalPoints).IsModified = false;
            tbl_LoyaltyCardLog lcLog = new tbl_LoyaltyCardLog();
            lcLog.LogDate = DateTime.UtcNow.AddHours(5);
            lcLog.LoyaltyCardID = redeem.LoyaltyCardID;
            lcLog.OrderID = 1001;
            lcLog.OrderTypeID = "3";
            lcLog.Points = redeem.RedeemPoints ?? 0;
            db.tbl_LoyaltyCardLog.Add(lcLog);
            var task = db.SaveChangesAsync();
                await task;

                if (task.Exception != null)
                {
                    ModelState.AddModelError("", "Unable to update!");
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return View(Request.IsAjaxRequest() ? "_RedeemPoints" : "RedeemPoints", redeem);
                }
            
            return Json(isValid);
            //return Content("success");

        }
        // POST: Customer/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateLoyaltyCustomer(tbl_Customer Customer)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            if (Customer.OpeningBalance == null)
            {
                Customer.OpeningBalance = 0;
            }

            Customer.UserID = HttpContext.User.Identity.GetUserId();
            Customer.BranchID = branchId;
            Customer.Addby = userID;
            Customer.AddOn = DateTime.UtcNow.AddHours(5);
            Customer.IsDeleted = false;
            Customer.IsActive = true;
            if (ModelState.IsValid)
            {
                if (db.tbl_Customer.Any(c => c.Name == Customer.Name))
                {
                    ModelState.AddModelError("Name", "Customer Already Exists!");
                    return View("Create");
                }
                else
                {
                    db.InsertLoyaltyCustomer(Customer.Name,Customer.NameUR,Customer.Code,Customer.Address,Customer.Phone,Customer.Phone1,Customer.Phone2,Customer.Company,Customer.CRNo,Customer.Cell,Customer.Email,Customer.tbl_LoyaltyCard.LoyaltyCardNo,Customer.tbl_LoyaltyCard.BarCode,Customer.tbl_LoyaltyCard.DiscountOnAmount,Customer.tbl_LoyaltyCard.DiscountAmount,Customer.OpeningBalance,Customer.CreditLimit,Customer.Addby,branchId, Customer.UserID);
                   // db.tbl_Customer.Add(tbl_Customer);
                    //db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added Loyalty Customer".ToString());

                    return RedirectToAction("CustomerList");
                }

            }

            return View(Customer);
        }
        #endregion
    }
}