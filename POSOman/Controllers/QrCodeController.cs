﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.DTO;
using System.Web.Configuration;
using System.Net;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;
using QRCoder;
using System.Drawing;
using System.IO;

namespace QRCodeBitmap.Controllers
{
    public class QRCodeController : Controller
    {
        [HttpGet]

        public ActionResult Index()

        {

            return View();

        }



        [HttpPost]

        public ActionResult Index(QRModel qr)

        {

            QRCodeGenerator ObjQr = new QRCodeGenerator();

            QRCodeData qrCodeData = ObjQr.CreateQrCode(qr.Message, QRCodeGenerator.ECCLevel.Q);

            Bitmap bitMap = new QRCode(qrCodeData).GetGraphic(20);

            using (MemoryStream ms = new MemoryStream())

            {

                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                byte[] byteImage = ms.ToArray();

                ViewBag.Url = "data:image/png;base64," + Convert.ToBase64String(byteImage);

            }

            return View();

        }
    }
}