﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Data.Entity.Validation;
using System.Data.Entity;
using System.Text;
using System.Net;
using System.Data.Entity.Core.Objects;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;
using System.Drawing;
using System.IO;
using ZXing;
using QRCoder;
using Microsoft.Reporting.WebForms;
using Microsoft.Ajax.Utilities;

namespace POSOman.Controllers
{
    public class PurchaseController : Controller
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();

        private int getBranchID()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        private int GetUserID()
        {
            int userID = 0;
            string currentUserId = "";
            if (Session["UserID"] != null)
            {
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                userID = currentUser.UserId;
            }

            return userID;
        }

        public JsonResult UploadImg(int OrderID,int ForOrderType) //Type 1 = PurchaseRequest, 2 = Purchases 
        {

            if (OrderID > 0 && ForOrderType > 0 && Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        using(var t = db.Database.BeginTransaction())
                        {
                            var data = db.tbl_PurchaseOrder.Where(x => x.OrderID == OrderID && (x.IsDeleted != true || x.IsDeleted == null)).FirstOrDefault();
                            if (data != null)
                            {
                                string fileName = OrderID.ToString() + Path.GetExtension(file.FileName);
                                string filePath = ForOrderType == 1 ? "/Content/Records/PurchaseRequest/" + fileName :ForOrderType == 2 ? "/Content/Records/Purchase/" + fileName : string.Empty;
                                //Delete File If Exists

                                if (System.IO.File.Exists(Server.MapPath(filePath)))
                                {
                                    // If file found, delete it    
                                    //System.IO.File.Delete(Path.Combine(filePath, fileName));
                                    System.IO.File.Delete(Server.MapPath(filePath));
                                }

                                file.SaveAs(Server.MapPath(filePath));
                                data.Picture = filePath;

                                db.Entry(data).State = EntityState.Modified;
                                db.SaveChanges();
                                t.Commit();
                            }
                        }
                        // Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        //file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!",JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    while(ex.InnerException != null) { ex = ex.InnerException; }
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }

           // return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: Purchase
        //[Authorize(Roles = "SuperAdmin,Admin,BDM")]
        public ActionResult Index(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, string PartNo, int? BranchID,int? ProdID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.vendor = db.tbl_Vendor.Where(x=>x.IsDeleted !=true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.VendorCode + "|" + v.Name }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.product = db.tbl_Product.Where(x => x.IsGeneralItem == true && (x.isActive == 1 || x.isActive == null)).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();

            int branchId = getBranchID();

            //var data = db.tbl_PurchaseOrder.Where(p => p.POID > 999 && p.IsDeleted != true && p.isOpening != true && p.PurchaseTypeID == 2).ToList().OrderByDescending(p => p.OrderID);
            //return View(data);

            if (btn.HasValue)
            {
                List<GetPurchaseFilterWise_Result> data = db.GetPurchaseFilterWise(ProdID, PartNo, BranchID, AccountID, fromDate, toDate).ToList();
                if (data.Count == 0)
                {
                    return PartialView("_Index");
                }
                else
                    return PartialView("_Index", data);
            }
            return View();
        }
        public ActionResult PurchaseRequestList()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            int branchId = getBranchID();

            var data = db.tbl_PurchaseOrder.Where(p => p.POID > 999 && p.IsDeleted != true && p.isOpening != true && p.PurchaseTypeID == 1).ToList().OrderByDescending(p => p.OrderID);
            return View(data);
        }

        //[Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult PurchaseOrderIndex()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var tmp_PO = db.tmp_PO.Where(t => t.IsDeleted == false || t.IsDeleted == null && t.IsPO == true && t.BranchID == branchId).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_PaymentTypes).OrderByDescending(t => t.tmpID);
            return View(tmp_PO.ToList());
        }
        public ActionResult PurchaseOrderDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //tbl_PurchaseOrder purchaseOrder = db.tbl_PurchaseOrder.Find(id);
            List<tmp_OrderDetails> _tmpPODetails = db.tmp_OrderDetails.Where(p => p.OrderID == id).ToList();
            if (_tmpPODetails == null)
            {
                return HttpNotFound();
            }
            return PartialView(_tmpPODetails);
        }

        private void GetViewBagData()
        {
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.vendorCode = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode }).ToList();
            ViewBag.Desc = db.tbl_Product.Where(x => x.VehicleCodeID != 1).Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();
            ViewBag.Product = db.tbl_Product.Where(x => (x.IsOffsetItem == false && x.IsGeneralItem == true) && x.VehicleCodeID > 5 && (x.isActive == null || x.isActive == 1)).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            //ViewBag.Product = db.tbl_Product.Where(x => x.VehicleCodeID != 1).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Where(x => x.VehicleCodeID != 1).Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
        }

        //Create Service
       // [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult ServiceC()
        {
            int branchId = getBranchID();
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.vendorCode = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode }).ToList();
            ViewBag.Desc = db.tbl_Product.Where(x => x.VehicleCodeID == 1).Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();
            ViewBag.Product = db.tbl_Product.Where(x => (x.IsOffsetItem == true || x.IsGeneralItem == true) && x.VehicleCodeID!=1 && (x.isActive == null || x.isActive == 1)).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Where(x=>x.HeadID == 1).Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            return View();
        }

        //Create Purchase From Purchase Request
        public ActionResult PurchaseFromPurchaseRequest(int id)
        {
           
            ViewBag.OrdId = id;
            ViewBag.PurchaseTitle = "Purchase Invoice For PR-"+db.tbl_PurchaseOrder.Where(x=>x.OrderID == id).Select(x=>x.POID).FirstOrDefault().ToString();
            ViewBag.BranchId = db.tbl_Branch.Select(v => new { Value = v.BranchID, Name = v.BranchName }).ToList(); //branchId;

            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.vendor = db.tbl_Vendor.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            //ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            //ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.Product = db.tbl_Product.Where(x => (x.IsOffsetItem == false && x.IsGeneralItem == true) && x.VehicleCodeID > 5 && (x.isActive == null || x.isActive == 1)).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.Desc = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            //  ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            //return View("PurchaseEdit_1");
            return View();
        }
        //Create Purchase
        //[Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult Create()
        {
            int branchId = getBranchID();
            GetViewBagData();
            return View();
        }
       
        public ActionResult CreatePurchaseRequest()
        {
            int branchId = getBranchID();
            GetViewBagData();
            return View();
        }

        public ActionResult DeletePOrder(int? id)
        {
            ModelState.AddModelError(string.Empty, "Deleting this record results in deleting all related entries! Proceed to delete?");
            tmp_PO tbl_PurchaseOrder = db.tmp_PO.Find(id);
            return View(tbl_PurchaseOrder);
        }

        [HttpPost, ActionName("DeletePOrder")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePOrder(int? id , int i =0)
        {
            tmp_PO tbl_PurchaseOrder = db.tmp_PO.Find(id);
            tbl_PurchaseOrder.IsDeleted = true;
            db.Entry(tbl_PurchaseOrder).State = EntityState.Modified;
            db.SaveChanges();
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Purchase Order Entry".ToString());

            
            return RedirectToAction("PurchaseOrderIndex");
        }

        public ActionResult PODelete(int? id)
        {
            ModelState.AddModelError(string.Empty, "Deleting this record results in deleting all entries and related actions! Proceed to delete?");
            tbl_PurchaseOrder tbl_PurchaseOrder = db.tbl_PurchaseOrder.Find(id);
            return View(tbl_PurchaseOrder);
        }

        [HttpPost, ActionName("PODelete")]
        [ValidateAntiForgeryToken]
        public ActionResult PODeleteConfirmed(int? id,int i=0)
        {
            try
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }

                DeleteEntry DeletePE = new DeleteEntry();
                {
                    List<int> prodIDs = db.tbl_PODetails.Where(pd => pd.OrderID == id).Select(pd => pd.ProductID).ToList();
                    List<int> batchIDs = new List<int>();
                    foreach (var item in prodIDs)
                    {
                        var BatchID = id + "-" + item;
                        int bID = db.tbl_StockBatch.Where(pd => pd.BatchID == BatchID).Select(pd => pd.StockBatchID).FirstOrDefault();
                        batchIDs.Add(bID);
                    }
                    
                    
                    //bool isExist = db.tbl_SaleDetails.Any(sd => prodIDs.Contains(sd.ProductID));
                    bool isExist = db.tbl_StockLog.Any(sd => batchIDs.Contains(sd.StockBatchID ?? 0) && sd.OrderTypeID == 3);
                    if (isExist == true)
                    {
                        ModelState.AddModelError(string.Empty, "Record Can not be deleted!");
                        return RedirectToAction("PODelete");
                    }
                    bool Check = DeletePE.DeletePO(id, Convert.ToInt32(Session["LoginUserID"]),userID);
                    if (Check == true)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Some Error Occured!");
                        return RedirectToAction("PODelete");
                    }

                    //db.tbl_PurchaseOrder.Remove(tbl_PurchaseOrder);
                    //UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Purchase Entry".ToString());

                    //db.SaveChanges();


                }
            }
               
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return RedirectToAction("Index");
            }
        }
        public ActionResult PurchaseRequestDelete(int? id)
        {
            ModelState.AddModelError(string.Empty, "Deleting this record results in deleting all entries and related actions! Proceed to delete?");
            tbl_PurchaseOrder tbl_PurchaseOrder = db.tbl_PurchaseOrder.Find(id);
            return View(tbl_PurchaseOrder);
        }

        [HttpPost, ActionName("PurchaseRequestDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult PurchaseRequestDeleteConfirmed(int? id, int i = 0)
        {
            try
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }

                DeleteEntry DeletePE = new DeleteEntry();
                {
                   
                    bool Check = DeletePE.DeletePurchaseRequest(id, Convert.ToInt32(Session["LoginUserID"]), userID);
                    if (Check == true)
                    {
                        return RedirectToAction("PurchaseRequestList");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Some Error Occured!");
                        return RedirectToAction("PurchaseRequestDelete");
                    }

                    //db.tbl_PurchaseOrder.Remove(tbl_PurchaseOrder);
                    //UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Purchase Entry".ToString());

                    //db.SaveChanges();


                }
            }

            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return RedirectToAction("Index");
            }
        }
        //[Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult NewOrder()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.BranchID = branchId; 
            ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode+" | "+v.Name }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            //ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.tbl_VehicleCode.VehicleCode + " | " + v.PartNo }).ToList();            
            return View();
        }
        // Purchase Order Form 
        //[Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult PurchaseOrder()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.BranchID = branchId;
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode + " | " + v.Name }).ToList();            
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            //ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            return View();
        }
        // Save Purchase Order
        public JsonResult SavePurchaseOrder(tmp_PO model)
        {
            try
            {
                int branchId = 0;
                
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }                
                POSOman.Models.BLL.tmpOrder tmpOrder = new Models.BLL.tmpOrder();                
                tmpOrder.SavePurchaseOrder(model,branchId);
                
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Create PurchaseOrders".ToString());

                return Json("success");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        public ActionResult Edit()
        {
            return View();
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //tbl_PurchaseOrder purchaseOrder = db.tbl_PurchaseOrder.Find(id);
            List<tbl_PODetails> _PODetails = db.tbl_PODetails.Where(p => p.OrderID == id && p.Qty > 0).ToList();
            var po = db.tbl_PurchaseOrder.Where(x => x.OrderID == id).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(po.Barcode))
            {
                RenderBarcode(po.Barcode.ToString(), po.QRCode);
                ViewBag.BarCodeImg = Session["BCImage"];
                ViewBag.BarCodeID = po.Barcode;
                //ViewBag.QRCode = SaleOrder.QRCode.ToString();//Session["QRCode"];
            }
            else
            {
                ViewBag.BarCodeImg = null;
            }
            if (_PODetails == null)
            {
                return HttpNotFound();
            }
            else
            {
                string isNegative = "";
                try
                {

                    string number = (Convert.ToDecimal(_PODetails.First().tbl_PurchaseOrder.TotalAmount) - Convert.ToDecimal(_PODetails.First().tbl_PurchaseOrder.DiscountAmount)).ToString();

                    number = Convert.ToDouble(number).ToString();

                    if (number.Contains("-"))
                    {
                        isNegative = "Minus ";
                        number = number.Substring(1, number.Length - 1);
                    }
                    if (number == "0")
                    {
                        ViewBag.AmInWord = "Zero Rupees Only";

                    }
                    else
                    {
                        AmountInWordsBLL amInWord = new AmountInWordsBLL();
                        ViewBag.AmInWord = isNegative + amInWord.ConvertToWords(number);
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.AmInWord = "NIL";


                }
                //return PartialView("_Invoice",_PODetails);
                return PartialView("_InvoiceTextile", _PODetails);
            }
        }
        
        public FileResult Export(int id)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/PurInvoice.rdlc");


            ReportDataSource rs = new ReportDataSource();
            rs.Name = "PurchaseInvoiceData";
            rs.Value = db.PurchaseInvoiceNew(id);

            lo.DataSources.Add(rs);


            byte[] renderbyte;

            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            //Response.AddHeader("Content-Disposition", "attachment; filename=PurchaseInvoice.pdf");

            return new FileContentResult(renderedBytes, mimeType);

        }
        public FileResult ExportPurchaseRequest(int id)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/PurchaseRequestReport.rdlc");


            ReportDataSource rs = new ReportDataSource();
            rs.Name = "PurchaseInvoiceData";
            rs.Value = db.PurchaseInvoiceNew(id);

            lo.DataSources.Add(rs);


            byte[] renderbyte;

            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            //Response.AddHeader("Content-Disposition", "attachment; filename=PurchaseInvoice.pdf");

            return new FileContentResult(renderedBytes, mimeType);

        }
        //BarCode    
        public void RenderBarcode(string Code, string QRCode)
        {
            Image img = null;
            using (var ms = new MemoryStream())
            {
                var writer = new ZXing.BarcodeWriter() { Format = BarcodeFormat.CODE_128 };
                writer.Options.Height = 80;
                writer.Options.Width = 280;
                writer.Options.PureBarcode = true;
                img = writer.Write(Code);
                // save image in files
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                Session["BCImage"] = "data:image/jpeg;base64," + Convert.ToBase64String(ms.ToArray());

                //QRCodeGenerator qr = new QRCodeGenerator();
                //QRCodeData data = qr.CreateQrCode(QRCode, QRCodeGenerator.ECCLevel.Q);
                //QRCode code = new QRCode(data);
                //Session["QRCode"] = code.GetGraphic(5);
            }
        }
        public JsonResult getVendors()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var qry = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode+'|'+v.Name }).ToList();
            return Json(qry);
        }
        public JsonResult getProducts()
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            var qry = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            return Json(qry);
        }
        public JsonResult getCurrency()
        {
            //int branchId = 0;
            
            //if (Session["BranchID"] != null)
            //{
            //    branchId = Convert.ToInt32(Session["BranchID"]);
            //}
            //else
            //{
            //    var user = User.Identity;
            //    string currentUserId = User.Identity.GetUserId();
            //    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
            //    branchId = currentUser.BranchID;
            //}
            decimal dExchangeRate = (decimal)(db.tbl_Currency.OrderByDescending(v => v.ID).First().ExchangeRate);
            return Json(dExchangeRate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getLastUnitPrice(int ProductID)
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            decimal dUnitPrice = 0;
            var qry = db.tbl_PODetails.Where(p => p.ProductID == ProductID && p.BranchID == branchId).OrderByDescending(v => v.DetailID).FirstOrDefault();
            if (qry != null)
            {
                dUnitPrice = Convert.ToDecimal(qry.UnitPrice);
            }            
            return Json(dUnitPrice, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPaymentType()
        {
            var qry = db.tbl_PaymentTypes.Select(p => new { Value = p.AccountID, Name = p.Name }).ToList();
            return Json(qry);
        }      
            
        public JsonResult getProduct_PartNumbers()
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            var qry = db.tbl_Product.Select(p => new { Value = p.ProductID, Name = p.PartNo }).ToList();
            return Json(qry);
        }
        // GEt  POID 
        public JsonResult getNewPOID()
        {
            int POID = 0;
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var tmp = db.tbl_PurchaseOrder.Where(po =>  po.isOpening != true && po.IsDeleted !=true).OrderByDescending(v => v.OrderID).FirstOrDefault();
            if (tmp != null)
            {                
               POID = tmp.POID + 1;                
            }
            else
            {
                POID = 10001;
            }

            return Json(POID, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPOOrderID(string POID)
        {
           
            var OrderID = db.tmp_PO.Where(po => po.PurchaseCode == POID && po.IsPurchased == false).Select(q => q.OrderID).FirstOrDefault();
            if (OrderID > 0)
            {
                return Json(OrderID, JsonRequestBehavior.AllowGet);
            }
            else
            { return Json(false, JsonRequestBehavior.AllowGet); }

        }
        // Get Last Purchase code and return it to js and increment in it +1 
        public JsonResult getLastPOCode()
        {
            string tempID = "";
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var tmp = db.tmp_PO.Where(v => v.IsPO == true && v.BranchID == branchId).OrderByDescending(v => v.OrderID).FirstOrDefault();
            if (tmp != null)
            {
                tempID = tmp.PurchaseCode;
                if (tempID == null)
                {
                    tempID = "PO-10000";
                }
            }
            else
            {
                tempID = "PO-10000";
            }
            
            return Json(tempID, JsonRequestBehavior.AllowGet);
        }

        // Get PurchaseOrder Details and bind it to Submit Order 
        public JsonResult getPurchaseOrder(int tOrderID)
        {
            if (tOrderID > 0)
            {
                try
                {
                    int branchId = 0;
                    
                    if (Session["BranchID"] != null)
                    {
                        branchId = 9001;// Convert.ToInt32(Session["BranchID"]);
                    }
                    else
                    {
                        var user = User.Identity;
                        string currentUserId = User.Identity.GetUserId();
                        var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                        branchId = 9001;// currentUser.BranchID;
                    }
                    var qry = db.tmp_OrderDetails.Where(p => p.OrderID == tOrderID && p.tmp_PO.IsPO == true && p.tmp_PO.IsPurchased == false && p.BranchID == branchId)
                        .Select(p => new
                        {
                            p.OrderID,                            
                            p.tmp_PO.PurchaseCode,   // if user need tracking for every Purchase Order submitted, 
                            p.ProductID,            // then pass purchase code to tbl_PurchaseOrder and pass InvoiceNo/OrderID to tmp_PO
                            p.PartNo,
                            p.tbl_Product.Description,
                            p.tbl_Product.VehicleCodeID,
                            p.tbl_Product.tbl_VehicleCode.VehicleCode,
                            p.tbl_Product.Product_Code,
                            p.tbl_Product.Location,                            
                            p.tmp_PO.AccountID,
                            p.tmp_PO.PurchaseDate,
                            p.tmp_PO.Currency,
                            CurrencyRate = p.tmp_PO.ExchangeRate,   // Actual Exchange rate
                            p.tmp_PO.TotalAmount,
                            p.Qty,
                            p.UnitPrice,
                            p.tbl_Product.UnitCode,
                            p.tbl_Product.OpenUnitCode,
                            p.tbl_Product.LeastUnitCode,
                            p.tbl_Product.UnitPerCarton,
                            p.SalePrice,
                            p.IsPack,
                            p.LevelID,
                            //p.ExchangeRate,
                            p.Total
                        }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }
            return Json("");
        }
        // Change status of PurchaseOrder set status = Purchased , and isPO = false in tmp_Po changePOStatus
        public JsonResult changePOStatus(int tOrderID,string invoiceNo)
        {
            var result = db.tmp_PO.SingleOrDefault(p => p.OrderID == tOrderID);
            if (result != null)
            {
                result.IsPurchased = true;
                result.Status = "Purchased";
                result.InvoiceNo = invoiceNo;
                db.SaveChanges();
            }
            return Json("Success");

        }
        // GotoNewOrder Edit and Submit Purchase Order 
        //public ActionResult GotoNewOrder(int tOrderID)
        //{
        //    int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
        //    ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 && acd.BranchID == branchId).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
        //    ViewBag.BranchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
        //    ViewBag.vendor = db.tbl_Vendor.Where(b => b.BranchID == branchId).Select(v => new { Value = v.AccountID, Name = v.VendorCode + " | " + v.Name }).ToList();
        //    //ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
        //    ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
        //    ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
        //    ViewBag.product = db.tbl_Product.Where(b => b.BranchID == branchId).Select(v => new { Value = v.ProductID, Name = v.tbl_VehicleCode.VehicleCode + " | " + v.PartNo }).ToList();
        //    ViewBag.tOrderID = tOrderID;
        //    return View("SubmitPurchaseOrder");
        //}
        // GotoNewOrder Edit and Submit Purchase Order 
        public ActionResult GotoNewOrder(int tOrderID)
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            //ViewBag.BranchID = branchId;
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.vendorCode = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode }).ToList();
           // ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.tOrderID = tOrderID;
            ViewBag.isPO = 1;
            return View("SubmitPurchaseOrder");
        }
        //public JsonResult getVendorUnitPrice(int productId,int accountID)
        //{
        //    try
        //    {
        //        decimal resultValue =0;
        //        ObjectResult<decimal?> result = db.GetVendorLastPrice(productId, accountID);
        //        if (result != null)
        //        {
        //            decimal? theResult = result.SingleOrDefault();

        //            if (theResult != null)
        //            {
        //                resultValue = theResult.Value;
        //            }
        //        }


        //        return Json(resultValue);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(ex.Message.ToString());
        //    }

        //    // return Json("null data");
        //}
        #region Get Product by BAr Code 
        public JsonResult getProductDetailByBarCode(string barCode,int? BranchID)
        {
            try
            {
                List<object> objectList = new List<object>();
                int stockQty = 0;
                decimal cost = 0;
                decimal SalePrice = 0;
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                var qry = db.tbl_Product.Where(p => p.BarCode == barCode).Select(p => new {
                    pid =p.ProductID,
                    partno= p.PartNo,
                    Description = p.Description,
                    SubstituteNo = p.Product_Code,
                    ProductID = p.ProductID,
                    Location = p.tbl_Stock.Where(st => st.tbl_Product.BarCode == barCode && st.BranchID == branchId).FirstOrDefault().Location,
                    VehicleCode = p.tbl_VehicleCode.VehicleCode,
                    UnitCode = p.UnitCode,
                    OpenUnitCode = p.OpenUnitCode,
                    LeastUnitCode = p.LeastUnitCode,
                    UnitPerCtn = p.UnitPerCarton,
                    QtyPerUnit = p.QtyPerUnit,
                    IsPacket = p.IsPacket,
                    IsMinor = p.IsMinor,
                    LevelID = p.LevelID,
                    MinorDivisor = p.MinorDivisor
                }).FirstOrDefault();

                var qry1 = db.tbl_Stock.Where(p => p.tbl_Product.BarCode == barCode && p.BranchID == BranchID).FirstOrDefault();
                if (qry1 != null)
                {
                    stockQty = (int)(qry1.Qty);
                    if (qry1.CostPrice != null)
                    { cost = (decimal)(qry1.CostPrice); }
                    if (qry1.SalePrice != null)
                    { SalePrice = (decimal)(qry1.SalePrice); }

                }
                if (cost == 0 && qry1 != null)
                {
                    if (qry1.CostPrice == null)
                    {
                        cost = 0;
                    }
                    else
                        cost = (decimal)(qry1.CostPrice);
                }
                SalePrice = db.tbl_Product.Where(p => p.BarCode == barCode).FirstOrDefault().SaleRate ?? 0;
                objectList.Add(new
                {
                    stock = stockQty,
                    costPrice = cost,
                    SalePrice = SalePrice,
                    qry = qry
                });
                return Json(objectList, JsonRequestBehavior.AllowGet);
                //return Json(new { stockData = result, Id = ID });
                //return Json(qry);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }

            // return Json("null data");
        }
        public JsonResult getProductByBarCode(string barCode)
        {
            try
            {               
                var qry = db.tbl_Product.Where(p => p.BarCode == barCode).Select(p => new {

                    
                    ProductID = p.ProductID,  
                    PartNo = p.PartNo,    
                    UnitCode = p.UnitCode,
                    SalePrice = p.SaleRate
                }).FirstOrDefault();
                
               
                return Json(qry, JsonRequestBehavior.AllowGet);
                //return Json(new { stockData = result, Id = ID });
                //return Json(qry);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }

            // return Json("null data");
        }
        #endregion
        public JsonResult getProductDetail(int productId,int? BranchID)
        {
            try
            {
                if (productId > 0)
                {
                    List<object> objectList = new List<object>();
                    int stockQty = 0;
                    decimal cost = 0;
                    decimal SalePrice = 0;
                    int branchId = getBranchID();

                    var qry = db.tbl_Product.Where(p => p.ProductID == productId).Select(p => new {
                        pid = p.ProductID,
                        partno = p.PartNo,
                        Description = p.Description,
                        SubstituteNo = p.Product_Code,
                        Location = p.tbl_Stock.Where(st => st.ProductID == productId).FirstOrDefault().Location,
                        VehicleCode = p.tbl_VehicleCode.VehicleCode,
                        UnitCode = p.tbl_UnitCode == null ?"-":p.tbl_UnitCode.UnitCode,
                        UnitCodeID = p.UnitCodeID,
                        OpenUnitCode = p.OpenUnitCode,
                        LeastUnitCode = p.LeastUnitCode,
                        UnitPerCtn = p.UnitPerCarton,
                        QtyPerUnit = p.QtyPerUnit == 0 ? 1 : p.QtyPerUnit,
                        Sheets = p.TotalSheets == 0 ? 0 : p.TotalSheets,
                        TotalSheets = p.DepartmentID == 1 ?p.TotalSheets:p.DepartmentID == 2?p.TotalRequiredRolls:p.DepartmentID == 3 ?p.TotalM2ReqIncludingWastage:0 ,
                        SalePrice = p.MarkedupPrice,
                        IsPacket = p.IsPacket,
                        DepartmentID = p.DepartmentID,
                        Department = p.DepartmentID == null ?"-":p.tbl_Department.Department,
                        IsMinor = p.IsMinor,
                        LevelID = p.LevelID,
                        MinorDivisor = p.MinorDivisor
                    }).FirstOrDefault();

                    IEnumerable<object> orderMachine = null;
                    if (qry.DepartmentID != null)
                    {
                        orderMachine = db.tbl_OrderMachine.Where(x => x.DeptID == qry.DepartmentID).Select(x=>new { Value = x.ID,Name = x.OrderMachine}).ToList();

                    }

                    var qry1 = db.tbl_Stock.Where(p => p.ProductID == productId).FirstOrDefault();

                    if (qry1 != null)
                    {
                        stockQty = (int)(qry1.Qty);
                        if (qry1.CostPrice != null)
                        {
                            cost = (decimal)(qry1.CostPrice);
                        }
                        if (qry1.SalePrice != null)
                        {
                            //SalePrice = (decimal)(qry1.SalePrice);
                            SalePrice = db.tbl_SaleDetails.Where(x => x.ProductID == productId && (x.IsDeleted != true || x.IsDeleted == null)).OrderByDescending(x => x.OrderID).Select(x => x.SalePrice).FirstOrDefault() ?? 0;
                        }
                    }

                    if (cost == 0 && qry1 != null)
                    {
                        if (qry1.CostPrice == null)
                        {
                            cost = 0;
                        }
                        else
                        {
                            cost = (decimal)(qry1.CostPrice);
                        }
                    }

                    //SalePrice = db.tbl_Product.Where(p => p.ProductID == productId).FirstOrDefault().SaleRate ?? 0;
                     SalePrice = db.tbl_SaleDetails.Where(x => x.ProductID == productId && (x.IsDeleted != true || x.IsDeleted == null)).OrderByDescending(x=>x.OrderID).Select(x => x.SalePrice).FirstOrDefault() ?? 0;
                    objectList.Add(new
                    {
                        stock = stockQty,
                        costPrice = cost,
                        SalePrice = SalePrice,
                        qry = qry,
                        orderMachine = orderMachine
                    });

                    return Json(objectList, JsonRequestBehavior.AllowGet);

                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }

        // Save Purchase
        public JsonResult SaveOrder(tbl_PurchaseOrder model, List<tbl_StockLog> modelStockLog, int? bankAccountId)
        {
            int branchId = getBranchID();
            int userID = GetUserID();

            model.AddBy = userID;
            model.UserID = User.Identity.GetUserId();
            POSOman.Models.BLL.OrderBooking order = new Models.BLL.OrderBooking();
            object result = order.Save(model, modelStockLog, bankAccountId, branchId);
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Purchases Performed".ToString());

            return Json(result);         
        }
        public JsonResult SavePurchaseRequestOrder(tbl_PurchaseOrder model, List<tbl_StockLog> modelStockLog, int? bankAccountId)
        {
            int branchId = getBranchID();
            int userID = GetUserID();

            model.AddBy = userID;
            model.UserID = User.Identity.GetUserId();
            POSOman.Models.BLL.OrderBooking order = new Models.BLL.OrderBooking();
            object result = order.SaveRequest(model, modelStockLog, bankAccountId, branchId);
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Purchase Request Performed".ToString());

            return Json(result);
        }

        //
        public ActionResult GetEditPurchaseInvoice(int? id)
        {
            ViewBag.OrdId = id;
            ViewBag.PurchaseTitle = "Purchase Edit";
            ViewBag.BranchId = db.tbl_Branch.Select(v => new { Value = v.BranchID, Name = v.BranchName }).ToList(); //branchId;

            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.vendor = db.tbl_Vendor.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            //ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            //ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.Product = db.tbl_Product.Where(x => (x.IsOffsetItem == false && x.IsGeneralItem == true) && x.VehicleCodeID > 5 && (x.isActive == null || x.isActive == 1)).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.Desc = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            //  ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            //return View("PurchaseEdit_1");
            return View("EditPurchase");
        }

        //Get edit vendor

        public JsonResult GetEditInvoiceVendor(int? OrderId)
        {
            List<object> objectList = new List<object>();

            // int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (OrderId > 0)
            {
                var PurchaseOrderInv = db.tbl_PurchaseOrder.Where(x => x.OrderID == OrderId).FirstOrDefault();
                var InvNo = PurchaseOrderInv.InvoiceNo;
                var qry = db.tbl_Vendor.Where(p => p.AccountID == PurchaseOrderInv.AccountID).Select(p => new { Value = p.AccountID, Name = p.Name }).FirstOrDefault();
                var qry1 = db.tbl_PurchaseOrder.Where(p => p.BranchID == PurchaseOrderInv.BranchID).Select(p => new { Value = p.BranchID, Name = p.tbl_Branch.BranchName }).FirstOrDefault();
                var qry2 = db.tbl_Vendor.Where(p => p.AccountID == PurchaseOrderInv.AccountID).Select(x => x.OpeningBalance).FirstOrDefault();
                var FinalAmount = (PurchaseOrderInv.VAT == 0 || PurchaseOrderInv.VAT == null) ? PurchaseOrderInv.TotalAmount : PurchaseOrderInv.TotalAmount + PurchaseOrderInv.VAT;
                var Discount = PurchaseOrderInv.DiscountAmount;
                var Expenses = PurchaseOrderInv.Expenses;
                var chequeDate = PurchaseOrderInv.ChequeDate;
                var purchaseDate = PurchaseOrderInv.PurchaseDate;
                var expDescription = PurchaseOrderInv.Description;
                var RefNo = PurchaseOrderInv.ReferenceNo;
                var PurchaseDate = PurchaseOrderInv.PurchaseDate;
                var SubAmount = PurchaseOrderInv.TotalAmount;
                var TotalAmount = PurchaseOrderInv.TotalAmount;// + PurchaseOrderInv.VAT;
                var Tax = PurchaseOrderInv.VAT;
                var AmountPaid = PurchaseOrderInv.AmountPaid;
                var Paytype = db.tbl_PaymentTypes.Where(x => x.ID == PurchaseOrderInv.PaymentTypeID).Select(x => new { Value = x.ID, Name = x.Name }).FirstOrDefault() == null ? null : db.tbl_PaymentTypes.Where(x => x.ID == PurchaseOrderInv.PaymentTypeID).Select(x => new { Value = x.ID, Name = x.Name }).FirstOrDefault();
                var Cheque = PurchaseOrderInv.ChequeNo;
                var ChqDate = PurchaseOrderInv.ChequeDate;
                var ProductsList = db.tbl_PODetails.Where(x => x.OrderID == OrderId && (x.IsDeleted != true || x.IsDeleted == null)).Select(p => new { ProductID = p.ProductID, Cat = p.tbl_Product.tbl_VehicleCode.VehicleCode, PartNo = p.PartNo, Desc = p.tbl_Product.Description, Qty = p.Qty, UnitPerCarton = p.UnitPerCarton, IsMinor = p.IsMinor,IsPack = p.IsPack, LevelID = p.LevelID, MinorDivisor = p.MinorDivisor ,SalePrice = p.SalePrice, UnitPrice = p.UnitPrice,UnitCode = p.UnitCode,PrDisAm=p.DiscountAmount,PrDisPer=p.DiscountPercent,PrGSTAm=p.GSTAmount,PrGSTPer=p.GSTPercent, PTotal = p.Total }).ToList();
                var Banks = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 18).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                int BankId = 0;
                foreach (var i in Banks)
                {
                    if (i.Name.Equals(PurchaseOrderInv.BankName))
                    {
                        BankId = i.Value;
                    }
                }
                var Bank = db.tbl_PurchaseOrder.Where(x => x.OrderID == OrderId).Select(p => new { Value = BankId, Name = p.BankName }).FirstOrDefault();
                var PayStatus = 0;
                if (PurchaseOrderInv.PaymentStatus == "UnPaid")
                {
                    PayStatus = 3;
                }
                else if (PurchaseOrderInv.PaymentStatus == "Paid")
                {
                    PayStatus = 1;
                }
                if (PurchaseOrderInv.PaymentStatus == "Partial Paid")
                {
                    PayStatus = 2;
                }

                objectList.Add(new
                {
                    Qry = qry,
                    Qry1 = qry1,
                    Qry2 = qry2,
                    chequeDate = chequeDate,
                    purchaseDate = purchaseDate,
                    FinalAmount = FinalAmount,
                    Discount = Discount,
                    PurchaseDate = PurchaseDate,
                    SubAmount = SubAmount,
                    TotalAmount = TotalAmount,
                    Tax = Tax,
                    AmountPaid = AmountPaid,
                    Paytype = Paytype,
                    PayStatus = PayStatus,
                    Cheque = Cheque,                    
                    Bank = Bank,
                    InvNo = InvNo,
                    Expenses = Expenses,
                    Description = expDescription,
                    ReferenceNo = RefNo,
                    ProductsList = ProductsList
                });
            }
            return Json(objectList, JsonRequestBehavior.AllowGet);
            
        }
        //Edit Product Detail
        public JsonResult GetEditProductDetail(int OrderId, int ProductID, int BranchID)
        {
            List<object> objectList1 = new List<object>();

            var Q = db.tbl_PODetails.Where(x => x.OrderID == OrderId && x.ProductID == ProductID && x.BranchID == BranchID).Select(p => new { ProductID = p.ProductID, CatID = p.tbl_Product.VehicleCodeID, ArticleNo = p.PartNo, Desc = p.tbl_Product.Description, Qty = p.Qty,Packet = p.Packet,UnitPerCarton = p.UnitPerCarton,LevelID = p.LevelID, IsPack = p.IsPack,DiscAmount = p.DiscountAmount,DiscPercent = p.DiscountPercent,GstPercent = p.GSTPercent,GstAmount = p.GSTAmount,unitPrice = p.UnitPrice, SalePrice = p.SalePrice, PTotal = p.Total }).FirstOrDefault();

            objectList1.Add(new
            {
                ProductsList = Q
            });

            return Json(objectList1, JsonRequestBehavior.AllowGet);
        }
        //Edit Purchase 
        public JsonResult SaveEditOrder(int OrderId, tbl_PurchaseOrder model, List<tbl_StockLog> modelStockLog, int? bankAccountId)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            branchId = model.BranchID ?? 9001;
            POSOman.Models.BLL.OrderBooking order = new Models.BLL.OrderBooking();
            object result = order.EditPurchase(OrderId, model, modelStockLog, bankAccountId, branchId,userID);
            return Json(result);
            //else {
            //    var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
            //}
        }

        public JsonResult SavePurchaseFromPurchaseRequestOrder(int OrderId, tbl_PurchaseOrder model, List<tbl_StockLog> modelStockLog, int? bankAccountId)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            branchId = model.BranchID ?? 9001;
            POSOman.Models.BLL.OrderBooking order = new Models.BLL.OrderBooking();
            model.PurchaseRequestID = OrderId;
            model.PurchaseTypeID = 2;
            
            object result = order.Save( model, modelStockLog, bankAccountId, branchId);
            return Json(result);
            //else {
            //    var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
            //}
        }

    }
    public class FormattedDbEntityValidationException : Exception
    {
        public FormattedDbEntityValidationException(DbEntityValidationException innerException) :
            base(null, innerException)
        {
        }

        public override string Message
        {
            get
            {
                var innerException = InnerException as DbEntityValidationException;
                if (innerException != null)
                {
                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine();
                    sb.AppendLine();
                    foreach (var eve in innerException.EntityValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().FullName, eve.Entry.State));
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sb.AppendLine(string.Format("-- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                                ve.PropertyName,
                                eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                                ve.ErrorMessage));
                        }
                    }
                    sb.AppendLine();

                    return sb.ToString();
                }

                return base.Message;
            }
        }
    }
}
