﻿using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.BLL;
using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class BankTransferController : Controller
    {
        BankTransferEntry transferEntry = new BankTransferEntry();
        dbPOS db = new dbPOS();

        private int getBranchID()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        // GET: Bank Transfer Detail 
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Index()
        {
            int branchId = getBranchID();
            var bankTransfer = db.tbl_JDetail.Where(j => (j.EntryTypeID == 27));
            return View(bankTransfer.ToList());
        }

        // Get to Create Page
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Create()
        {
            try
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                ViewBag.BranchId = 9001;
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.FromBankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 || acd.AccountID == 1).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                ViewBag.ToBankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 || acd.AccountID == 1).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }
        // Save loanEntry Entry to Db
        public JsonResult SaveTransferEntry(BankTransfer model)
        {
            if (ModelState.IsValid)
            {
                //if (Session["BranchID"] != null)
                //{
                //    branchId = Convert.ToInt32(Session["BranchID"]);
                //}
                //else
                //{
                //    var user = User.Identity;
                //    string currentUserId = User.Identity.GetUserId();
                //    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                //    branchId = currentUser.BranchID;
                //}
                if (db.tbl_JDetail.Any(x => x.UserReferenceID == model.UserReferenceID && x.EntryTypeID == 27))
                {
                    return Json("refError");
                }
                else
                {
                    object Result = transferEntry.Save(model, 9001);
                    // UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Bank Transfer Actions".ToString());

                    return Json(Result);
                }

            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                // UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Error Performed Bank Transfer Actions".ToString());

                return Json("formError");
            }
        }
        // Get Balance of transfering from branch 
        public JsonResult getBankBalance(int bankID)
        {
            if (bankID > 0)
            {
                var totalDr = db.tbl_JDetail.Where(j => j.AccountID == bankID).Sum(j => j.Dr);
                var totalCr = db.tbl_JDetail.Where(j => j.AccountID == bankID).Sum(j => j.Cr);
                decimal Balance = totalDr - totalCr;
                return Json(Balance);
            }
            else
            {
                return Json(null);
            }
        }

        // open Loan  receipt
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            List<tbl_JDetail> _PaymentDetail = db.tbl_JDetail.Where(j => j.JEntryID == id).OrderBy(j => j.JDetailID).ToList();
            if (_PaymentDetail == null)
            {
                return HttpNotFound();
            }
            return PartialView("_TransferVoucher", _PaymentDetail);
        }

        [Authorize(Roles = "SuperAdmin")]
        // GET: Jentry/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int branchId = getBranchID();
            tbl_JEntry tbl_Jentry = db.tbl_JEntry.Where(j => j.JEntryId == id && j.BranchID == branchId).FirstOrDefault();
            if (tbl_Jentry == null)
            {
                return HttpNotFound();
            }

            return View(tbl_Jentry);
        }

        [Authorize(Roles = "SuperAdmin")]
        // POST: Entry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int branchId = getBranchID();
                db.DeleteJournalEntry(id);
                return RedirectToAction("Deposit");
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
        }



        //Deposit Index

        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult IndexDeposit()
        {
            int branchId = getBranchID();
            var Deposit = db.tbl_JDetail.Where(j => (j.EntryTypeID == 19)).OrderBy(x => x.JEntryID).Skip(1).Take(1);
            return View(Deposit.ToList());
        }

        //Create Deposit
        public ActionResult CreateDeposit()
        {
            try
            {
                int branchId = getBranchID();
                ViewBag.BranchId = branchId;
                ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.FromBankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 || acd.AccountID == 1).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                ViewBag.ReferenceAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 28).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
                ViewBag.ToBankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 || acd.AccountID == 1).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        //Save Deposit
        public JsonResult SaveDepositEntry(DepositDTO model)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                model.AddBy = userID;
                //if (db.tbl_JDetail.Any(x => x.UserReferenceID == model.UserReferenceID && x.EntryTypeID == 19))
                //{
                //    return Json("refError");
                //}
                //else
                {
                    object Result = transferEntry.SaveDeposit(model, branchId);
                    // UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Bank Transfer Actions".ToString());

                    return Json(Result);
                }

                // UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Bank Transfer Actions".ToString());


            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                // UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Error Performed Bank Transfer Actions".ToString());

                return Json("formError");
            }
        }

        public ActionResult DepositDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            List<tbl_JDetail> _PaymentDetail = db.tbl_JDetail.Where(j => j.JEntryID == id).OrderBy(j => j.JDetailID).ToList();
            if (_PaymentDetail == null)
            {
                return HttpNotFound();
            }
            return PartialView("_DepositVoucher", _PaymentDetail);
        }

        //Get Deposit Filter

        //Deposit Page With Filter
        public ActionResult Deposit(bool? isSearch, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {

            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Account = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 || acd.AccountID == 1).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();

            if (isSearch == true)
            {

                List<GetDepositDetailDateWise_Result> Deposit = db.GetDepositDetailDateWise(AccountID, fromDate, toDate).ToList();
                if (Deposit.Count == 0)
                {
                    return PartialView("_Deposit");
                }
                else
                    return PartialView("_Deposit", Deposit);
            }

            return View("Deposit");
        }

        // Withdraw /Deposit Detail Person Wise
        public ActionResult PersonDeposit(bool? isSearch, int? AccountID, DateTime? fromDate, DateTime? toDate,int? ShareHolderID,int? EntryTypeID)
        {

            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Account = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 || acd.AccountID == 1).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.ShareHolder = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 28).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();

            if (isSearch == true)
            {

                List<GetPersonTransactionDateWise_Result> data = db.GetPersonTransactionDateWise(AccountID, fromDate, toDate,ShareHolderID,EntryTypeID).ToList();
                if (data.Count == 0)
                {
                    return PartialView("_PersonDeposit");
                }
                else
                    return PartialView("_PersonDeposit", data);
            }
            return View("PersonDeposit");
        }
        //WithDraw
        //WithDraw Index

        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult IndexWithDraw()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(9001);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                //branchId = currentUser.BranchID;
                branchId = 9001;
            }
            //var WithDraw = db.tbl_JDetail.Where(j => (j.EntryTypeID == 29));
            var WithDraw = db.tbl_JDetail.Where(j => (j.EntryTypeID == 29)).OrderBy(x => x.JEntryID).Skip(1);
            return View(WithDraw.ToList());
        }

        //Create WithDraw
        public ActionResult CreateWithDraw()
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    //branchId = Convert.ToInt32(Session["BranchID"]);
                    branchId = Convert.ToInt32(9001);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    // branchId = currentUser.BranchID;
                    branchId = 9001;
                }
                ViewBag.BranchId = branchId;
                ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.FromBankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 || acd.AccountID == 1).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                //ViewBag.ToBankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 18 || acd.AccountTypeID == 1 && (acd.AccountName.Equals("Cash"))).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                ViewBag.ReferenceAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 28).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        //Save WithDraw
        public JsonResult SaveWithDrawEntry(DepositDTO model)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                model.AddBy = userID;
                if (db.tbl_JDetail.Any(x => x.UserReferenceID == model.UserReferenceID && x.EntryTypeID == 29))
                {
                    return Json("refError");
                }
                else
                {
                    object Result = transferEntry.SaveDrawing(model, branchId);
                    // UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Bank Transfer Actions".ToString());

                    return Json(Result);
                }
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                // UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Error Performed Bank Transfer Actions".ToString());

                return Json("formError");
            }
        }

        public ActionResult WithDrawDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            List<tbl_JDetail> _PaymentDetail = db.tbl_JDetail.Where(j => j.JEntryID == id).OrderBy(j => j.JDetailID).ToList();
            if (_PaymentDetail == null)
            {
                return HttpNotFound();
            }
            return PartialView("_DepositVoucher", _PaymentDetail);
        }


        //Withdraw Page With Filter
        public ActionResult WithDrawal(bool? isSearch, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {

            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Account = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 || acd.AccountID == 1).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();

            if (isSearch == true)
            {

                List<GetWithDrawDetailDateWise_Result> WithDraw = db.GetWithDrawDetailDateWise(AccountID, fromDate, toDate).ToList();
                if (WithDraw.Count == 0)
                {
                    return PartialView("_WithDraw");
                }
                else
                    return PartialView("_WithDraw", WithDraw);
            }
            return View("WithDraw");
        }


        //Bank Transfer with filter

        public ActionResult BankTransfer(bool? isSearch, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {

            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Account = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 || acd.AccountID == 1).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();

            if (isSearch == true)
            {

                List<GetBankTransferDetailDateWise_Result> BankTransfer = db.GetBankTransferDetailDateWise(AccountID, fromDate, toDate).ToList();
                if (BankTransfer.Count == 0)
                {
                    return PartialView("_BankTransfer");
                }
                else
                    return PartialView("_BankTransfer", BankTransfer);
            }
            return View();
        }
    }
}
