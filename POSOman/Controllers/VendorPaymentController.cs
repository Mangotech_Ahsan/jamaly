﻿using POSOman.Models;
using POSOman.Models.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace POSOman.Controllers
{
    
    public class VendorPaymentController : Controller
    {
        Models.BLL.VendorPayment vendorPay = new VendorPayment();
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();



        public int GetBranchID()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        public JsonResult GetVendorBalance(int accountID)
        {
            if (accountID > 1)
            {
                var bal = db.tbl_JDetail.Where(a => a.AccountID == accountID).Select(a => new { Cr = a.Cr, Dr = a.Dr }).ToList();
                if(bal.Count != 0)
                {
                    var result = bal.Sum(a => a.Cr) - bal.Sum(a => a.Dr);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }


        #region Settle payment without invoices
        public ActionResult SettleAmountWithInvoices(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();

            var JEntryD = db.tbl_JEntry.Where(x => x.JEntryId == id && x.IsSettleEntry == true).FirstOrDefault();
            ViewBag.CustomerID = JEntryD.CustomerRefAccID;
            ViewBag.Settlement = JEntryD.Amount - JEntryD.TotalBalanceSettled;
            ViewBag.JEntryID = id;
            ViewBag.customer = db.tbl_Vendor.Where(x => x.AccountID == JEntryD.CustomerRefAccID).Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Vendor.Where(x => x.AccountID == JEntryD.CustomerRefAccID).Select(c => new { Value = c.AccountID, Name = c.VendorCode }).ToList();
            ViewBag.customerPhone = db.tbl_Vendor.Where(x => x.AccountID == JEntryD.CustomerRefAccID).Select(c => new { Value = c.AccountID, Name = c.Cell }).ToList();

            return View();



        }

        public JsonResult SettleAmountForInvoice(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId, int CustJEntryID)
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            object result = vendorPay.SettleAmount(model, jentryLog, bankAccId, branchId, CustJEntryID);

            if (Convert.ToInt32(result) > 0)
            {

                return Json(result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }

        }


        // Get Invoice Details of Vendor 
        public JsonResult getVendorDetail(int accountId)
        {
            if (accountId > 0)
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                try
                {
                    //var qry = db.tbl_PurchaseOrder.Where(p => p.AccountID == accountId && p.IsPaid == false && p.IsReturned == false
                    //    && (p.IsDeleted == false || p.IsDeleted == null) && p.PurchaseTypeID == 2).Join(
                    //    db.tbl_PurchaseOrder,
                    //    po => po.OrderID,
                    //    p => p.PurchaseRequestID,
                    //    (p, po) => new
                    //    {
                    //        p.OrderID,
                    //        InvoiceNo = p.InvoiceNo ?? "-",
                    //        p.POID,
                    //        PurchaseRequestID = po.POID,//db.tbl_PurchaseOrder.Where(x=>x.OrderID == p.PurchaseRequestID) !=null ? db.tbl_PurchaseOrder.Where(x => x.OrderID == p.PurchaseRequestID).Select(x=>x.POID).FirstOrDefault().ToString():"-",
                    //        p.AccountID,
                    //        p.PaymentTypeID,
                    //        p.tbl_AccountDetails.AccountName,
                    //        PurchaseDate = p.PurchaseDate,
                    //        p.TotalAmount,
                    //        p.ReturnAmount,
                    //        p.DiscountAmount,
                    //        p.VAT,
                    //        p.Adjustment,
                    //        AdjustmentDescription = p.AdjustmentDescription ?? "noDesc",
                    //        p.VATPercent,
                    //        VATType = p.VATType ?? "noType",
                    //        p.AmountPaid,
                    //        p.TotalPaid,
                    //        p.PaymentStatus
                    //    }).DefaultIfEmpty().ToList();

                    var qry = db.GetPurchaseInvoicesForVendorPayment(accountId, null).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }
            return Json("");

        }
        #endregion



        // GET: VendorPayment
        public ActionResult Index()
        {
            int branchId = 0;            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var vendorVoucher = db.tbl_JDetail.Where(j => j.EntryTypeID == 1 && (j.IsDeleted == false || j.IsDeleted == null));// && j.BranchID == branchId);
            return View(vendorVoucher.ToList());            
        }
        // open payment receipt
        public ActionResult Details(bool? isNew, int? id)
        {


            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //tbl_PurchaseOrder purchaseOrder = db.tbl_PurchaseOrder.Find(id);
            if (isNew == true)
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.OrderByDescending(v => v.JDetailID).Where(j => j.AccountID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_PaymentVoucher", _PaymentDetail);
            }
            else
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JDetailID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_PaymentVoucher", _PaymentDetail);
            }
        }
        // open  receipt
        public ActionResult Voucher(bool? isNew, int? id)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (isNew == true)
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.OrderByDescending(v => v.JDetailID).Where(j => j.AccountID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }

                //return PartialView("_PaymentVoucherSmall", _PaymentDetail);
                return PartialView("_PaymentVoucher", _PaymentDetail);
            }
            else
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JDetailID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }

                //return PartialView("_PaymentVoucherSmall", _PaymentDetail);
                return PartialView("_PaymentVoucher", _PaymentDetail);
            }
        }

        //Get: vendor payments filerwise
        public ActionResult GetVendorPaymentReportFilterWise(bool? btn, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            if (btn.HasValue)
            {
                List<GetVendorPayReportFilterWise_Result> data = db.GetVendorPayReportFilterWise(BranchID, fromDate, toDate).ToList();

                if (data.Count == 0)
                {
                    return PartialView("_VendorPayReport");
                }
                else
                {
                    return PartialView("_VendorPayReport", data);
                }

            }
            return View("VendorPayReport");
        }

        public ActionResult MultiPayment()
        {
            try
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }


                ViewBag.BranchID = db.tbl_Branch.Select(x => new { Value = x.BranchID, Name = x.BranchName }).ToList();

                ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode + "|" + v.Name }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }
        public ActionResult InvoicePayments()
        {
            try
            {
                int branchId = GetBranchID();
                ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode+"|"+v.Name }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
            return View();
        }




        public JsonResult SavePaymentMulti(Payment model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId, List<Models.DTO.GetMultiChequePayments> MultiPayments)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            model.BranchID = branchId;
            if (model.BranchID == null)
            {
                return Json("false", JsonRequestBehavior.AllowGet);
            }

            string UserName = User.Identity.GetUserName().ToString();// Session["CUserName"].ToString();
            if (MultiPayments.Count > 0)
            {

                if (MultiPayments.Any(x => x.Amount == null || x.Amount <= 0) || MultiPayments.Sum(x => x.Amount) != model.Amount || model.BranchID == null)
                {
                    return Json("false", JsonRequestBehavior.AllowGet);
                }

                else
                {

                    foreach (var i in MultiPayments)
                    {
                        if (i.PayTypeID == 3 && i.chqDate == null && i.BankID == null)
                        {
                            return Json("chqDate", JsonRequestBehavior.AllowGet);
                        }
                        else if (i.PayTypeID == 3 && string.IsNullOrWhiteSpace(i.chqNumber) && i.BankID == null)
                        {
                            return Json("chqNumber", JsonRequestBehavior.AllowGet);
                        }
                        else if (i.PayTypeID == 2 && i.BankID == null)
                        {
                            return Json("bank", JsonRequestBehavior.AllowGet);
                        }

                        else if (i.PayTypeID == 1)
                        {
                            i.chqNumber = null;
                            i.chqDate = null;
                            i.BankID = 1;
                        }
                    }



                    //if (ModelState.IsValid)
                    //{

                    object result = vendorPay.SaveMulti(model, jentryLog, bankAccId, Convert.ToInt32(model.BranchID), UserName, MultiPayments);
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Customer Installment Payments Action".ToString());

                    if (result.ToString() == "success")
                    {
                        UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Vendor Payment Action".ToString());

                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("false", JsonRequestBehavior.AllowGet);
                    }
                    //  }
                    //else
                    //{
                    //    var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                    // //   UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Customer Payments Action".ToString());

                    //    return Json("formError");
                    //}
                }


                //}
                //else
                //{
                //    var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                //    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Customer Payments Action".ToString());

                //    return Json("", JsonRequestBehavior.AllowGet);
                //}


            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError", JsonRequestBehavior.AllowGet);
            }
        }



        public JsonResult SavePayment(Payment model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                vendorPay.Save(model, jentryLog,bankAccId,branchId);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Vendor Payment Action".ToString());

                return Json("success");
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }
        public ActionResult VendorStatement(int AccountID)
        {            
            List<GetVendorStatementVendorWise_Result> vendor = db.GetVendorStatementVendorWise(AccountID).ToList();
                if (vendor.Count == 0)
                {
                    return View("VendorStatement");
                }
                else
                    return View("VendorStatement", vendor);                       
        }

        [Authorize(Roles = "SuperAdmin")]
        // GET: Jentry/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            Models.DTO.PaymentLog jLog = new Models.DTO.PaymentLog();
            var query = from jlog in db.tbl_JEntryLog
                        join je in db.tbl_JEntry on jlog.JEntryID equals je.JEntryId
                        join jd in db.tbl_JDetail on je.JEntryId equals jd.JEntryID
                        join so in db.tbl_PurchaseOrder on jlog.OrderID equals so.OrderID
                        join acd in db.tbl_AccountDetails on jd.AccountID equals acd.AccountID
                        where jd.Cr > 0 && jlog.JEntryID == id
                        select new Models.DTO.JEntryLogModel
                        {
                            AccountName = acd.AccountName,
                            InvoiceNo = so.POID,
                            Amount = jlog.Amount ?? 0
                        };
            if (query == null)
            {
                return HttpNotFound();
            }
            return View(query.ToList());
        }
        [Authorize(Roles = "SuperAdmin")]
        // POST: Entry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                vendorPay.DeleteEntry(id);
                return RedirectToAction("Index");
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
        }

        ///without invoices scenario
        ///
        // Pay without selecting invoice 
        public ActionResult DirectPayment()
        {
            try
            {
                int branchId = GetBranchID();
                ViewBag.vendor = db.tbl_Vendor.Where(x=>x.IsDeleted!=true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.VendorCode + "|" + v.Name }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        //  Save Payment without invoices 
        public JsonResult SaveSinglePayment(Payment model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                vendorPay.SaveSinglePayment(model, jentryLog, bankAccId, branchId);
                return Json("success");
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }


    }
}
