﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using AutoMapper;
using System.Web.Configuration;
using Microsoft.Reporting.WebForms;
using POSOman.Models.DTO;
using Microsoft.AspNet.Identity;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Syncfusion.JavaScript.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class ReportsController : Controller
    {
        private dbPOS db = new dbPOS();
        Models.BLL.Reports report = new Models.BLL.Reports();
        public ActionResult reportOrder()
        {
            var qry = db.tbl_PurchaseOrder.ToList();
            ReportViewer viewer = new ReportViewer();
            viewer.Width = 1200;
            viewer.Height = 1000;

            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.EnableHyperlinks = true;
            viewer.LocalReport.ReportPath = Server.MapPath("~/Models/Reports/PO.rdlc");
            ReportDataSource RDS = new ReportDataSource("purchaseReport", qry);
            viewer.LocalReport.DataSources.Clear();
            viewer.LocalReport.DataSources.Add(RDS);
            viewer.LocalReport.Refresh();
            ViewBag.ReportViewer = viewer;
            return View("PoReport");
        }

        public ActionResult ProfitLossReportSalePersonWise(bool? btn, int? ItemID, int? SPAccountID, int? OrderID, int? DeptID, DateTime? fromDate, DateTime? toDate)
        {

            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.Item = db.tbl_Product.Where(x => x.IsOffsetItem == false && x.IsGeneralItem == false).Select(b => new { Value = b.ProductID, Name = b.PartNo }).ToList();
            ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(b => new { Value = b.ID, Name = b.Department }).ToList();
            ViewBag.SalePerson = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(b => new { Value = b.AccountID, Name = b.SalePerson }).ToList();
            if (btn.HasValue)
            {
                var data = db.GetProfitLossReportItemWise(ItemID, OrderID, DeptID, SPAccountID, fromDate, toDate).ToList();

                if (data.Count == 0)
                {
                    return PartialView("_ProfitLossReportSalePersonWise");
                }
                else
                {

                    return PartialView("_ProfitLossReportSalePersonWise", data);
                }
            }

            return View();
        }
        public ActionResult ProfitLossReportItemWise(bool? btn, int? ItemID, int? OrderID, int? DeptID, DateTime? fromDate, DateTime? toDate)
        {

            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.Item = db.tbl_Product.Where(x => x.IsOffsetItem == false && x.IsGeneralItem == false).Select(b => new { Value = b.ProductID, Name = b.PartNo }).ToList();
            ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(b => new { Value = b.ID, Name = b.Department }).ToList();
            if (btn.HasValue)
            {
                var data = db.GetProfitLossReportItemWise(ItemID, OrderID, DeptID, null, fromDate, toDate).ToList();

                if (data.Count == 0)
                {
                    return PartialView("_ProfitLossReportItemWise");
                }
                else
                {

                    return PartialView("_ProfitLossReportItemWise", data);
                }
            }

            return View();
        }


        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }




        public ActionResult GetAccountsID(string Id)
        {
            int? BranchID = null;
            int ID = 0;
            DateTime? fromDate = null;
            DateTime? toDate = null;

            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                string val = "0_null";
                string[] lines = Regex.Split(Id, "_");

                ID = Convert.ToInt32(lines[0]);
                BranchID = Convert.ToInt32(lines[1]);
                var a = lines[2];
                var b = lines[3];
                if (a == "null")
                {
                    fromDate = null;

                }
                else
                {
                    fromDate = Convert.ToDateTime(lines[2]);
                }
                if (b == "null")
                {
                    toDate = null;

                }
                else
                {
                    toDate = Convert.ToDateTime(lines[3]);
                }



                List<GetAccountIDTrialBalanceDetails_Result> Details = db.GetAccountIDTrialBalanceDetails(ID, BranchID, fromDate, toDate).ToList();
                return View(Details);
            }
        }

        // Get Net Sales Date Wise 
        public ActionResult NetSales(bool? isSearch, int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchId)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.customer = db.tbl_Customer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            if (isSearch == true)
            {
                List<GetCustomerSalesDateWise_Result> sales = db.GetCustomerSalesDateWise(AccountID, fromDate, toDate, BranchId).ToList();
                if (sales.Count == 0)
                {
                    return PartialView("_NetSalesReport");
                }
                else
                    return PartialView("_NetSalesReport", sales);
            }
            return View("NetSalesReport");
        }
        // get Share Holder Statement 
        public ActionResult ShareHolderStatement(int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.ShareHolder = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 28).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
            if (AccountID > 0)
            {
                List<GetShareHolderTransDateWise_Result> customer = db.GetShareHolderTransDateWise(AccountID, fromDate, toDate, BranchID).ToList();
                if (customer.Count == 0)
                {
                    return PartialView("_ShareHolderStatement");
                }
                else
                    return PartialView("_ShareHolderStatement", customer);
            }
            return View();
        }
        //Get Stock Report 
        public ActionResult getStockReport(bool? btn, int? ProductID, int? VehicleCodeID, int? GroupID, int? ModelID, int? BranchID, string Product_Code, string Description, string PartNo)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();

            if (btn.HasValue)
            {
                List<GetStockReportFilterWise_Result> stock = db.GetStockReportFilterWise(ProductID, GroupID, VehicleCodeID, ModelID, Product_Code, Description, BranchID, PartNo).ToList();
                if (stock.Count == 0)
                {
                    return PartialView("_StockReport");
                }
                else
                    return PartialView("_StockReport", stock);
            }
            return View("StockReport");
        }

        // Get Product Sales Filter Wise 
        //replacing partno to productid
        public ActionResult GetProductSalesFilterWise(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, int? ProductID, int? VehicleCodeID, int? GroupID, int? ModelID, int? BranchID, string Description)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.customer = db.tbl_Customer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            if (btn.HasValue)
            {
                List<GetProductSalesFilterWise_Result> stock = db.GetProductSalesFilterWise(ProductID, BranchID, AccountID, fromDate, toDate, GroupID, VehicleCodeID, ModelID, Description).ToList();
                if (stock.Count == 0)
                {
                    return PartialView("_ProductSalesReport");
                }
                else
                    return PartialView("_ProductSalesReport", stock);
            }
            return View("ProductSalesReport");
        }

        // Get Product Report Filter Wise 
        public ActionResult GetProductReport(bool? btn, DateTime? fromDate, DateTime? toDate, string PartNo, int? ProductID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            if (btn.HasValue)
            {
                List<GetProductLedgerFilterWise_Result> products = db.GetProductLedgerFilterWise(ProductID, PartNo, fromDate, toDate).ToList();
                if (products.Count == 0)
                {
                    return PartialView("_ProductLedger");
                }
                else
                {
                    ViewBag.DateFrom = fromDate;
                    ViewBag.DateTo = toDate;
                    return PartialView("_ProductLedger", products);
                }
            }
            return View("ProductLedger");
        }
        // Get Product Ledger Detail 
        public ActionResult ProductLedgerDetails(DateTime? fromDate, DateTime? toDate, int ProductID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            List<GetProductLedgerDetail_Result> products = db.GetProductLedgerDetail(ProductID, null, fromDate, toDate).ToList();
            if (products.Count > 0)
            {
                return View(products);
            }
            return View();
        }

        // Get Sales Filter Wise 
        public ActionResult GetSalesFilterWise(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, string PartNo, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.customer = db.tbl_Customer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            if (btn.HasValue)
            {
                List<GetSalesFilterWise_Result> stock = db.GetSalesFilterWise(PartNo, BranchID, AccountID, fromDate, toDate).ToList();
                if (stock.Count == 0)
                {
                    return PartialView("_SalesReport");
                }
                else
                    return PartialView("_SalesReport", stock);
            }
            return View("SalesReport");
        }

        // Get USer Sales Filter Wise 
        public ActionResult GetUserSales(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, TimeSpan? fromTime, TimeSpan? toTime, string PartNo, int? BranchID, int? AddBy)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.customer = db.tbl_Customer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.Users = db.AspNetUsers.Select(v => new { Value = v.UserId, Name = v.UserName }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            if (btn.HasValue)
            {
                List<GetUserSalesFilterWise_Result> sales = db.GetUserSalesFilterWise(PartNo, BranchID, AccountID, fromDate, toDate, AddBy, fromTime, toTime).ToList();
                if (sales.Count == 0)
                {
                    return PartialView("_UserSalesOrders");
                }
                else
                {
                    return PartialView("_UserSalesOrders", sales);
                }
            }
            return View("UserSalesOrders");
        }
        // Get  Sales Credit Filter Wise 
        [Authorize]
        public ActionResult GetSalesCreditFilterWise(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.customer = db.tbl_Customer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();

            if (btn.HasValue)
            {
                List<GetSalesCreditFilterWise_Result> stock = db.GetSalesCreditFilterWise(BranchID, AccountID, fromDate, toDate).ToList();
                if (stock.Count == 0)
                {
                    return PartialView("_SalesCreditReport");
                }
                else
                    return PartialView("_SalesCreditReport", stock);
            }

            return View("SalesCreditReport");
        }

        // Payment Performance Report 
        public ActionResult GetPaymentPerformanceReport(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.customer = db.tbl_Customer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            if (btn.HasValue)
            {
                List<GetCustomerPaymentReport_Result> payment = db.GetCustomerPaymentReport(BranchID, AccountID, fromDate, toDate).ToList();
                if (payment.Count == 0)
                {
                    return PartialView("_PaymentPerformanceReport");
                }
                else
                    return PartialView("_PaymentPerformanceReport", payment);
            }
            return View("PaymentPerformanceReport");
        }
        // Get Cheque Date and Bank Wise
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult GetCheques(bool? isSearch, int? isRec, int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            //int branchId = 0;            
            //if (Session["BranchID"] != null)
            //{
            //    branchId = Convert.ToInt32(Session["BranchID"]);
            //}
            //else
            //{
            //    var user = User.Identity;
            //    string currentUserId = User.Identity.GetUserId();
            //    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
            //    branchId = currentUser.BranchID;
            //}
            ViewBag.isReceived = isRec;
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            if (isSearch == true)
            {
                List<GetChequesDateWise_Result> cheques = db.GetChequesDateWise(isRec, fromDate, toDate, BranchID).ToList();
                if (cheques.Count == 0)
                {
                    return PartialView("_Cheques");
                }
                else
                    return PartialView("_Cheques", cheques);
            }

            return View("Cheques");
        }
        // Get Bank Statement Date and Bank Wise
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult GetBankStatement(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchId)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            //int branchId = 0;
            //if (Session["BranchID"] != null)
            //{
            //    branchId = Convert.ToInt32(Session["BranchID"]);
            //}
            //else
            //{
            //    var user = User.Identity;
            //    string currentUserId = User.Identity.GetUserId();
            //    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
            //    branchId = currentUser.BranchID;
            //}
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Bank = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();

            if (btn.HasValue)
            {
                List<GetBanksDetailFilterWise_Result> bankStatement = db.GetBanksDetailFilterWise(AccountID, fromDate, toDate, BranchId).ToList();
                if (bankStatement.Count == 0)
                {
                    return PartialView("_BankStatement");
                }
                else
                    return PartialView("_BankStatement", bankStatement);
            }
            return View("BankStatement");
        }

        // Get Expense Report Date and Expense Wise
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult GetExpenseStatement(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchId)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            //ViewBag.expenses = db.tbl_AccountDetails.Where(a => a.AccountTypeID == 13).Select(a => new { Value = a.AccountID, Name = a.AccountName }).ToList();
            ViewBag.expenses = db.tbl_AccountDetails.Where(a => a.HeadID == 3 && a.AccountID > 109).Select(a => new { Value = a.AccountID, Name = a.AccountName }).ToList();
            if (btn.HasValue)
            {
                List<GetExpensesFilterWise_Result> expenseStatement = db.GetExpensesFilterWise(AccountID, fromDate, toDate, BranchId).ToList();
                if (expenseStatement.Count == 0)
                {
                    return PartialView("_ExpenseReport");
                }
                else
                    return PartialView("_ExpenseReport", expenseStatement);
            }

            return View("ExpenseReport");
        }

        // Get Product Purchase Filter Wise 
        //replacing partno to productid
        //[Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult GetProductPurchaseFilterWise(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, int? ProductID, int? VehicleCodeID, int? GroupID, int? ModelID, int? BranchID, string Description)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode + "|" + v.Name }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            if (btn.HasValue)
            {
                List<GetProductPurchaseFilterWise_Result> stock = db.GetProductPurchaseFilterWise(ProductID, BranchID, AccountID, fromDate, toDate, GroupID, VehicleCodeID, ModelID, Description).ToList();
                if (stock.Count == 0)
                {
                    return PartialView("_ProductPurchaseReport");
                }
                else
                    return PartialView("_ProductPurchaseReport", stock);
            }
            return View("ProductPurchaseReport");
        }

        // Get Purchases Filter Wise 
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult GetPurchaseFilterWise(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, string PartNo, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode + "|" + v.Name }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            if (btn.HasValue)
            {
                List<GetPurchaseFilterWise_Result> stock = db.GetPurchaseFilterWise(null,PartNo, BranchID, AccountID, fromDate, toDate).ToList();
                if (stock.Count == 0)
                {
                    return PartialView("_PurchaseReport");
                }
                else
                    return PartialView("_PurchaseReport", stock);
            }
            return View("PurchaseReport");
        }
        private Models.DTO.ExpensesTrialBalance MaptoModel(GetExpensesTrialBalance_Result expenses)
        {

            Models.DTO.ExpensesTrialBalance ExpensesTB = new Models.DTO.ExpensesTrialBalance()
            {

                AccountName = expenses.AccountName,
                TotalCredit = expenses.TotalCredit,
                TotalDebit = expenses.TotalDebit,
                Balance = expenses.Balance

            };
            return ExpensesTB;


        }
        private Models.DTO.DailyExpenses MaptoModel(GetDailyExpensesList_Result expense)
        {
            Models.DTO.DailyExpenses dailyExpense = new Models.DTO.DailyExpenses()
            {
                AccountName = expense.AccountName,
                Description = expense.Description,
                Amount = expense.Amount,
                Detail = expense.Detail
            };

            return dailyExpense;
        }
        private Models.DTO.DailyReceiving MaptoModel(GetDailyCashPaymentReceived_Result payments)
        {
            Models.DTO.DailyReceiving dailyPayment = new Models.DTO.DailyReceiving()
            {
                AccountName = payments.AccountName,
                Description = payments.Description,
                Amount = payments.Amount,
                Detail = payments.Detail
            };

            return dailyPayment;
        }

        private Models.DTO.AssetsBalance MapAssetstoModel(GetTrialBalanceForBalanceSheet_Result assets)
        {
            Models.DTO.AssetsBalance assetsBalance = new Models.DTO.AssetsBalance()
            {
                AccountName = assets.AccountName,
                TotalCredit = assets.TotalCredit,
                TotalDebit = assets.TotalDebit,
                Balance = assets.Balance
            };
            return assetsBalance;
        }
        private Models.DTO.LiabilityBalance MapLiabtoModel(GetTrialBalanceForBalanceSheet_Result liabilities)
        {

            Models.DTO.LiabilityBalance liabilitiesBalance = new Models.DTO.LiabilityBalance()
            {

                AccountName = liabilities.AccountName,
                TotalCredit = liabilities.TotalCredit,
                TotalDebit = liabilities.TotalDebit,
                Balance = liabilities.Balance

            };
            return liabilitiesBalance;
        }

        private Models.DTO.EquityBalance MapEquitytoModel(GetTrialBalanceForBalanceSheet_Result equity)
        {
            Models.DTO.EquityBalance equityBalance = new Models.DTO.EquityBalance()
            {
                AccountName = equity.AccountName,
                TotalCredit = equity.TotalCredit,
                TotalDebit = equity.TotalDebit,
                Balance = equity.Balance
            };
            return equityBalance;
        }
        // gET POSCashStatement
        // get cash statement
        public ActionResult POSCashStatement(bool? btn, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.Branch = db.tbl_Branch.Where(b => b.IsShop == true).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            if (btn.HasValue)
            {
                List<GetPOSCashDetailFilterWise_Result> CashStatement = db.GetPOSCashDetailFilterWise(fromDate, toDate, BranchID).ToList();

                if (CashStatement.Count == 0)
                {
                    ViewBag.Branch = db.tbl_Branch.Where(b => b.IsShop == true).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
                    return PartialView("_POSCashStatement");
                }
                else
                {
                    return PartialView("_POSCashStatement", CashStatement);
                }

            }
            return View("POSCashStatement");

        }
        // get cash statement
        public ActionResult CashStatement_UC(bool? btn, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.Branch = db.tbl_Branch.Where(b => b.IsShop == true).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();

            if (btn.HasValue)
            {
                List<GetCashDetailFilterWise_Result> CashStatement = db.GetCashDetailFilterWise(fromDate, toDate, BranchID).ToList();

                if (CashStatement.Count == 0)
                {
                    return PartialView("_CashStatement");
                }
                else
                {
                    List<StatementDTO> list = new List<StatementDTO>();
                    foreach (var i in CashStatement)
                    {
                        
                        list.Add(new StatementDTO
                        {
                            JEntryId = i.JEntryID,
                            Date = i.VoucherDate != null ? Convert.ToString(string.Format("{0:dd/MM/yyyy}", i.VoucherDate)) : "-",
                            Description = i.Description ?? "-",
                            Detail = i.Detail ?? "-",
                            Dr = Convert.ToDecimal(i.Dr).ToString("#,##0.00"),
                            Cr = Convert.ToDecimal(i.Cr).ToString("#,##0.00"),
                            Balance = string.IsNullOrWhiteSpace(i.Balance) ? "-" : Convert.ToDecimal(i.Balance).ToString("#,##0.00"),
                            OpeningBalance = i.OpBalance == null ? "-" : Convert.ToDecimal(i.OpBalance).ToString("#,##0.00"),
                        });

                    }
                    return PartialView("_CashStatement", list);
                }

            }
            return View("CashStatement");

        }

        public ActionResult CashStatement(bool? btn, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.Branch = db.tbl_Branch.Where(b => b.IsShop == true).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();

            //if (btn.HasValue)
            //{
            //    List<GetCashDetailFilterWise_Result> CashStatement = db.GetCashDetailFilterWise(fromDate, toDate, BranchID).ToList();

            //    if (CashStatement.Count == 0)
            //    {
            //        return PartialView("_CashStatement");
            //    }
            //    else
            //    {
            //        List<StatementDTO> list = new List<StatementDTO>();
            //        foreach (var i in CashStatement)
            //        {

            //            list.Add(new StatementDTO
            //            {
            //                JEntryId = i.JEntryID,
            //                Date = i.VoucherDate != null ? Convert.ToString(string.Format("{0:dd/MM/yyyy}", i.VoucherDate)) : "-",
            //                Description = i.Description ?? "-",
            //                Detail = i.Detail ?? "-",
            //                Dr = Convert.ToDecimal(i.Dr).ToString("#,##0.00"),
            //                Cr = Convert.ToDecimal(i.Cr).ToString("#,##0.00"),
            //                Balance = string.IsNullOrWhiteSpace(i.Balance) ? "-" : Convert.ToDecimal(i.Balance).ToString("#,##0.00"),
            //                OpeningBalance = i.OpBalance == null ? "-" : Convert.ToDecimal(i.OpBalance).ToString("#,##0.00"),
            //            });

            //        }
            //        return PartialView("_CashStatement", list);
            //    }

            //}
            return View("CashStatement");

        }


        [HttpPost]
        public JsonResult DataList(DTParameters param, DateTime? fromDate, DateTime? toDate)
        {
            int TotalCount = 0;
            decimal OpBal = 0;
            decimal ClBal = 0;
            var filtered = this.GetFilteredData(param.Search.Value, param.SortOrder, param.Start, param.Length, out TotalCount,  fromDate, toDate);

            var dataList = (filtered != null && filtered.Count > 0) ? filtered.Select(i => new StatementDTO()
            {
                JEntryId = i.JEntryId,
                Date = i.Date,
                AccName = i.AccName ?? "-",
                Description = i.Description ?? "-",
                Detail = i.Detail ?? "-",
                PayStatus = i.PayStatus ?? "-",
                Aging = i.Aging != null ? i.Aging.ToString() : "",
                Dr = i.Dr,
                Cr = i.Cr,
                Balance = i.Balance,
                OpeningBalance = i.OpeningBalance,
                detailList = i.detailList

            }):null;
            TotalCount = db.GetCashDetailFilterWise(fromDate, toDate, null).Count();

            //OpBal = db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).FirstOrDefault().OpBalance??0;
            //ClBal = (db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance == "-" || string.IsNullOrWhiteSpace(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance)  ) ? 0 :  Convert.ToDecimal(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance);

            DTResult<StatementDTO> finalresult = new DTResult<StatementDTO>
            {
                draw = param.Draw,
                data = (dataList != null && dataList.Count() > 0) ? dataList.ToList() : new List<StatementDTO>(),
                recordsFiltered = TotalCount,
                recordsTotal = filtered.Count,
                customData = new
                {
                    OpBal = Helper.DigitsWithComma(db.GetCashDetailFilterWise(fromDate, toDate, null).Select(x=>x.OpBalance).FirstOrDefault() ?? 0),
                    ClBal = Helper.DigitsWithComma((db.GetCashDetailFilterWise(fromDate, toDate, null).Select(x => x.Balance).LastOrDefault() == "-" || string.IsNullOrWhiteSpace(db.GetCashDetailFilterWise(fromDate, toDate, null).Select(x => x.Balance).LastOrDefault())) ? 0 : Convert.ToDecimal(db.GetCashDetailFilterWise(fromDate, toDate, null).Select(x => x.Balance).LastOrDefault())),
                    TotalDr = Helper.DigitsWithComma(db.GetCashDetailFilterWise(fromDate, toDate, null).Sum(x => x.Dr) <= 0 ? 0 : Convert.ToDecimal(db.GetCashDetailFilterWise(fromDate, toDate, null).Sum(x => x.Dr))),
                    TotalCr = Helper.DigitsWithComma(db.GetCashDetailFilterWise(fromDate, toDate, null).Sum(x => x.Cr) <= 0 ? 0 : Convert.ToDecimal(db.GetCashDetailFilterWise(fromDate, toDate, null).Sum(x => x.Cr)))
                }

            };

            //ViewBag.OpBal = (dataList != null && dataList.Count() > 0)?dataList.FirstOrDefault().OpeningBalance:"0";
            //ViewBag.ClBal = (dataList != null && dataList.Count() > 0)?dataList.LastOrDefault().Balance:"0";

            return Json(finalresult);

        }

        public List<StatementDTO> GetFilteredData(string search, string sortOrder, int start, int length, out int TotalCount,  DateTime? fromDate, DateTime? toDate)
        {
            //var result = db.tbl_Product.OrderBy(p => p.ProductID).Include(t => t.tbl_VehicleCode).Include(t => t.tbl_Brand).Where(p => (search == null || (p.PartNo != null && p.PartNo.ToLower().Contains(search.ToLower())
            //    || p.BarCode != null && p.BarCode.Contains(search)
            //    || p.tbl_VehicleCode.VehicleCode != null && p.tbl_VehicleCode.VehicleCode.ToLower().Contains(search.ToLower())
            //    || p.tbl_Brand.BrandTitle != null && p.tbl_Brand.BrandTitle.ToLower().Contains(search.ToLower())
            //    || p.Product_Code != null && p.Product_Code.ToLower().Contains(search.ToLower())
            //    || p.UnitCode != null && p.UnitCode.ToLower().Contains(search.ToLower())
            //    || p.SaleRate != null && p.SaleRate.ToString().Contains(search.ToString())
            //    ))).Skip(start).Take(length).ToList();
            //TotalCount = db.tbl_Product.Count();
            var result = db.GetCashDetailFilterWise(fromDate, toDate, null).Where(p => (search == null || (p.Description != null && p.Description.ToLower().Contains(search.ToLower())
                  || p.Detail != null && p.Detail.Contains(search)
                  //|| p.Aging != null && p.Aging.ToLower().Contains(search.ToLower())
                  //|| p.Balance != null && p.Balance.ToLower().Contains(search.ToLower())
                  //|| p.Product_Code != null && p.Product_Code.ToLower().Contains(search.ToLower())
                  //|| p.UnitCode != null && p.UnitCode.ToLower().Contains(search.ToLower())
                  //|| p.SaleRate != null && p.SaleRate.ToString().Contains(search.ToString())
                  ))).Skip(start).Take(length).ToList();
            TotalCount = db.GetCashDetailFilterWise(fromDate, toDate, null).Count();
            List<StatementDTO> dataList = new List<StatementDTO>();
            int counter = 0;
            foreach (var i in result)
            {

                Models.DTO.StatementDTO data = new StatementDTO();

                data.JEntryId = i.JEntryID;
                data.Date = i.VoucherDate != null ? Convert.ToString(string.Format("{0:dd/MM/yyyy}", i.VoucherDate)) : "-";
                //data.AccName = i.VoucherName ?? "-";
                data.Description = i.Description ?? "-";
                data.Detail = i.Detail ?? "-";
                //data.PayStatus = i.PaymentStatus ?? "-";
                //data.Aging = i.Aging != null ? i.Aging.ToString() : "";
                data.Dr = Convert.ToDecimal(i.Dr).ToString("#,##0.00");
                data.Cr = Convert.ToDecimal(i.Cr).ToString("#,##0.00");
                data.Balance = string.IsNullOrWhiteSpace(i.Balance) ? "-" : Convert.ToDecimal(i.Balance).ToString("#,##0.00");
                data.OpeningBalance = i.OpBalance == null ? "-" : Convert.ToDecimal(i.OpBalance).ToString("#,##0.00");
                data.detailList = StatementBLL.GetItemDetailsForStatementJenTryIDWise(i.JEntryID) != null ? StatementBLL.GetItemDetailsForStatementJenTryIDWise(i.JEntryID) : new List<Details>();


                dataList.Add(data);
                counter = counter + 1;
            }
            //Mapper.CreateMap<tbl_Product, Models.DTO.ProductListCategory>();
            //var details = Mapper.Map<ICollection<tbl_Product>, ICollection<Models.DTO.ProductListCategory>>(result);
            return dataList;
        }

        // GET DAily Cash FLow  Sheet 
        public ActionResult CashFlowStatement(bool? btn, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            if (btn.HasValue)
            {
                ViewBag.ToDate = toDate;
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                List<GetCashFlowByEntrytype_Result> cashFlow = db.GetCashFlowByEntrytype(fromDate, toDate, branchId).ToList();
                if (cashFlow == null)
                {
                    return PartialView("_CashFlowStatement");
                }
                else
                    return PartialView("_CashFlowStatement", cashFlow);
            }

            return View("CashFlowStatement");

        }

        //Customer Aging Summary
        public ActionResult CustomerAgingReport(bool? btn, int? AccountID, int? SPAccID, int? OID, DateTime? fromDate, DateTime? toDate, int? BranchID, int? InvoiceStatusID, string PaymentStatusDD, int? InvoiceTypeID, int? interval, int? range, decimal? taxPercent)
        {
            if (btn.HasValue)
            {
                if(range!=null && interval != null)
                {
                    var getData = Reports.agingSummaryData(range??0, interval??0, AccountID, SPAccID, OID, fromDate, toDate, BranchID, InvoiceStatusID, PaymentStatusDD, InvoiceTypeID, taxPercent);
                    if (getData != null && getData.Count>0)
                    {
                        List<string> seq = getData.Select(x => x.rangeAndInterval).Distinct().ToList();
                        List<string> cust = getData.Select(x => x.customerName).Distinct().ToList();

                        Tuple<List<AgingSummaryDTO>, List<string>,List<string>> tuple = new Tuple<List<AgingSummaryDTO>, List<string>,List<string>>(getData, seq, cust)  ;
                        return PartialView("_CustomerAgingReport", tuple);
                    }
                }

                

                return PartialView("_CustomerAgingReport");
            }


            ViewBag.Branch = db.tbl_Branch.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.SalePerson = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.SalePerson }).ToList();
            return View();
        }

        // Employee Pending Report
        public ActionResult EmployeePendingReport(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            if (btn.HasValue)
            {

                List<GetEmployeePendingFilterWise_Result> data = db.GetEmployeePendingFilterWise(AccountID, fromDate, toDate).ToList();

                if (data?.Count>0)
                {
                    return PartialView("_EmployeePendingReport", data);
                    
                }



                return PartialView("_EmployeePendingReport");
            }


            ViewBag.employee = db.tbl_Employee.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            return View();
        }


        // Get Accounts Balance 
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult GetAccountsBalance(int? reportId, bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.ReportID = reportId;
            ViewBag.Branch = db.tbl_Branch.Where(b => b.IsShop == true).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();

            if (btn.HasValue && reportId == 1) // Vendor Balance
            {
                List<GetVendorBalance_Result> balance = db.GetVendorBalance(fromDate, toDate, BranchID).ToList();
                Mapper.CreateMap<GetVendorBalance_Result, Models.DTO.AccountBalances>();
                var details = Mapper.Map<ICollection<GetVendorBalance_Result>, ICollection<Models.DTO.AccountBalances>>(balance);
                if (balance.Count == 0)
                {
                    return PartialView("_Balance");
                }
                else
                    return PartialView("_Balance", details.ToList());
            }
            else if (btn.HasValue && reportId == 2) // Customer Balance
            {
                List<GetCustomerBalance_Result> balance = db.GetCustomerBalance(fromDate, toDate, BranchID).ToList();
                Mapper.CreateMap<GetCustomerBalance_Result, Models.DTO.AccountBalances>();
                var details = Mapper.Map<ICollection<GetCustomerBalance_Result>, ICollection<Models.DTO.AccountBalances>>(balance);
                if (balance.Count == 0)
                {
                    return PartialView("_Balance");
                }
                else
                    return PartialView("_Balance", details.ToList());
            }
            else if (btn.HasValue && reportId == 3) // Expenses Balance
            {
                List<GetExpensesBalance_Result> balance = db.GetExpensesBalance(fromDate, toDate, BranchID).ToList();
                Mapper.CreateMap<GetExpensesBalance_Result, Models.DTO.AccountBalances>();
                var details = Mapper.Map<ICollection<GetExpensesBalance_Result>, ICollection<Models.DTO.AccountBalances>>(balance);
                if (balance.Count == 0)
                {
                    return PartialView("_Balance");
                }
                else
                    return PartialView("_Balance", details.ToList());
            }
            else if (btn.HasValue && reportId == 4) // Bank Balance
            {
                List<GetCashBankBalance_Result> balance = db.GetCashBankBalance(fromDate, toDate, BranchID).ToList();
                Mapper.CreateMap<GetCashBankBalance_Result, Models.DTO.AccountBalances>();
                var details = Mapper.Map<ICollection<GetCashBankBalance_Result>, ICollection<Models.DTO.AccountBalances>>(balance);
                if (balance.Count == 0)
                {
                    return PartialView("_Balance");
                }
                else
                    return PartialView("_Balance", details.ToList());
            }
            return View("Balance");
        }

        // GET Income Statement 
        //[Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        //public ActionResult IncomeStatement(bool? isSearch, DateTime? fromDate, DateTime? toDate, int? BranchID)
        //{
        //    ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
        //    ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

        //    ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
        //    ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
        //    ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
        //    ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

        //    ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
        //    if (isSearch == true)
        //    {
        //        int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
        //        Models.DTO.IncomeStatement income = new Models.DTO.IncomeStatement();
        //        GetRevenueData_Result getRevenue = db.GetRevenueData(fromDate, toDate,null).FirstOrDefault();
        //        List<GetExpensesTrialBalance_Result> Expenses = new List<GetExpensesTrialBalance_Result>();
        //        List<Models.DTO.ExpensesTrialBalance> ExpTB = new List<Models.DTO.ExpensesTrialBalance>();
        //        Expenses = db.GetExpensesTrialBalance(fromDate, toDate, branchId).ToList();
        //        Models.DTO.Revenue rev = new Models.DTO.Revenue();
        //        rev.Sales = getRevenue.Sales;
        //        rev.COGS = getRevenue.COGS;
        //        rev.GrossProfit = rev.Sales - rev.COGS;

        //        foreach (var item in Expenses)
        //        {
        //            {
        //                var exp = MaptoModel(item);
        //                ExpTB.Add(exp);
        //            }
        //        }
        //        rev.Expenses = ExpTB.Sum(exp => exp.Balance);
        //        rev.NetProfit = rev.GrossProfit - rev.Expenses;
        //        income.ExpensesTrialBalance = ExpTB;
        //        income.Revenue = rev;
        //        if (getRevenue == null)
        //        {
        //            return PartialView("_IncomeStatement1");
        //        }
        //        else
        //        {
        //            return PartialView("_IncomeStatement1", income);
        //        }
        //    }

        //    return View();
        //}


        // Get Detailed Income Statement
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult DetailedIncomeStatement(bool? isSearch, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            if (isSearch == true)
            {
                List<GetDetailedIncomeStatement_Result> income = db.GetDetailedIncomeStatement(fromDate, toDate, BranchID).ToList();
                List<DetailedIncome> finalReport = new List<DetailedIncome>();
                decimal totalExpenses = 0;
                decimal creditAdjustment = 0;
                decimal netProfit = 0;
                if (income.Count() <= 0)
                {
                    return PartialView("_DetailIncomeStatement");
                }
                else
                {
                    foreach (var item in income)
                    {
                        DetailedIncome detailIncome = new Models.DTO.DetailedIncome();
                        detailIncome.AccountID = item.AccountID;
                        detailIncome.AccountName = item.AccountName;
                        detailIncome.TotalExpense = item.TotalExpense;
                        //detailIncome.AdjustmentBalance = item.AdjustmentBalance;
                        totalExpenses += item.TotalExpense ?? 0;
                        //if (item.AdjustmentBalance > 0)
                        //{
                        //    totalExpenses += item.AdjustmentBalance ?? 0;
                        //}
                        //else if (item.AdjustmentBalance < 0)
                        //{
                        //    creditAdjustment += item.AdjustmentBalance ?? 0;
                        //}
                        finalReport.Add(detailIncome);
                    }
                    ViewBag.COGS = income[0].COGS;
                    ViewBag.Sales = income[0].Sales;
                    ViewBag.GrossProfit = income[0].GrossProfit;
                    ViewBag.TotalExpenses = totalExpenses;
                    netProfit = income[0].GrossProfit - totalExpenses ?? 0;
                    ViewBag.NetProfit = netProfit;// + creditAdjustment;
                    if (finalReport == null)
                    {
                        return PartialView("_DetailIncomeStatement");
                    }
                    else
                    {
                        return PartialView("_DetailIncomeStatement", finalReport);
                    }
                }

            }
            return View("DetailIncomeStatement");

        }
        //private  Models.DTO.BalanceSheet MaptoModel(GetMonthlyBalanceSheet_Result balance)
        //{
        //    Models.DTO.BalanceSheet balanceSheet = new Models.DTO.BalanceSheet()
        //    {
        //        CashSales = balance.CashSales,
        //        SalesReceiving = balance.SalesReceiving,
        //        GeneralExpenses = balance.GeneralExpenses,
        //        UtilityBills = balance.UtilityBills,
        //        GovtBills = balance.GovtBills,
        //        CustomBills = balance.CustomBills,
        //        FuelExpenses = balance.FuelExpenses,
        //        Discount = balance.Discount,
        //        TotalCash = balance.TotalCash,
        //        TotalExpenses = balance.TotalExpenses

        //    };

        //    return balanceSheet;
        //}

        // Deposit Cash to bank 
        public JsonResult SaveCashDepositEntry(Expenses model)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                object Result = report.Save(model, branchId);
                return Json(Result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }

        #region Replaced by Ahsan  

        public ActionResult BalanceSheet_Old(bool? isSearch, DateTime? fromDate, DateTime? toDate)
        {
            if (isSearch == true)
            {
                int BranchID = 9001;

                Models.DTO.IncomeStatement income = new Models.DTO.IncomeStatement();
                List<GetTrialBalanceForBalanceSheet_Result> gjM = db.GetTrialBalanceForBalanceSheet(fromDate, toDate, BranchID).ToList();
                //        
                List<GetTrialBalance_Result> gj = db.GetTrialBalance(fromDate, toDate, BranchID).ToList();
                GetRevenueData_Result getRevenue = db.GetRevenueData(fromDate, toDate, null).FirstOrDefault();
                List<GetExpensesTrialBalance_Result> Expenses = new List<GetExpensesTrialBalance_Result>();
                List<Models.DTO.ExpensesTrialBalance> ExpTB = new List<Models.DTO.ExpensesTrialBalance>();
                Expenses = db.GetExpensesTrialBalance(fromDate, toDate, BranchID).ToList();
                Models.DTO.Revenue rev = new Models.DTO.Revenue();
                rev.Sales = getRevenue.Sales;
                rev.COGS = getRevenue.COGS;
                rev.GrossProfit = rev.Sales - rev.COGS;

                foreach (var item in Expenses)
                {
                    if (item.AccountTypeID != 6 && item.Balance != 0)
                    {
                        var exp = MaptoModel(item);
                        ExpTB.Add(exp);
                    }
                }

                rev.Expenses = ExpTB.Sum(exp => exp.Balance);
                rev.NetProfit = rev.GrossProfit - rev.Expenses;
                ViewBag.NetProfit = rev.NetProfit;
                income.ExpensesTrialBalance = ExpTB;
                income.Revenue = rev;

                List<GetTrialBalance_Result> Assets = new List<GetTrialBalance_Result>(); // 1
                List<GetTrialBalance_Result> Liabilities = new List<GetTrialBalance_Result>(); // 2
                List<GetTrialBalance_Result> Equity = new List<GetTrialBalance_Result>(); // 3   
                List<GetTrialBalance_Result> ExpPurchase = new List<GetTrialBalance_Result>(); // 4    

                List<Models.DTO.AssetsBalance> assetsBal = new List<Models.DTO.AssetsBalance>();
                List<Models.DTO.LiabilityBalance> liabBal = new List<Models.DTO.LiabilityBalance>();
                List<Models.DTO.EquityBalance> eqBal = new List<Models.DTO.EquityBalance>();

                List<int> AssetsHeads = new List<int>();



                //AssetsHeads.AddRange(db.tbl_AccountType.Where(x=>x.HeadID == 1).Select(x=>x.AccountTypeID).ToList());
                List<int> LiabilitiesHeads = new List<int>() { 2, 12, 13 };
                List<int> EquityHeads = new List<int>() { 4, 17, 18, 19, 20 };

                Assets = gj.Where(a => a.AccountTypeID == 1 || AssetsHeads.Contains(a.HeadID ?? 0)).ToList();
                Liabilities = gj.Where(a => a.AccountTypeID == 2 || LiabilitiesHeads.Contains(a.HeadID ?? 0)).ToList();
                Equity = gj.Where(a => a.AccountTypeID == 4 || EquityHeads.Contains(a.HeadID ?? 0)).ToList();

                Models.DTO.BalanceSheet balSheet = new Models.DTO.BalanceSheet();

                balSheet.AssetsBalance = assetsBal;
                balSheet.LiabilityBalance = liabBal;
                balSheet.EquityBalance = eqBal;

                var openingInventory = gjM.Where(a => a.AccountID == 21).FirstOrDefault();
                decimal openingBalance = 0;
                if (openingInventory != null)
                {
                    openingBalance = openingInventory.Balance ?? 0;
                }

                var Inventory = gjM.Where(a => a.AccountID == 3).FirstOrDefault();
                decimal invBalance = 0;
                if (Inventory != null)
                {
                    invBalance = Inventory.Balance ?? 0;
                }

                ViewBag.InvClosing = (openingBalance) + invBalance - rev.COGS;

                if (balSheet == null)
                {
                    return PartialView("_BalanceSheet1");
                }
                else
                {
                    return PartialView("_BalanceSheet1", balSheet);

                }

            }
            return View("BalanceSheet");

        }


        public ActionResult BalanceSheet(bool? isSearch, DateTime? fromDate, DateTime? toDate)
        {
            if (isSearch == true)
            {


                List<Models.DTO.BalanceSheetFormat> balSheetFormat = new List<Models.DTO.BalanceSheetFormat>();


                return PartialView("_BalanceSheet_New", balSheetFormat);


            }
            return View("BalanceSheet_New");

        }

        public List<BalanceSheetFormat> RecursiveCallToGenerateBalanceSheet()
        {
            try
            {
                List<BalanceSheetFormat> masterList = null;
                List<int> BalanceSheetHeadIDs = new List<int>() { 1, 2, 4 };
                var heads = db.tbl_AccountType.Where(x => x.HeadID == null && BalanceSheetHeadIDs.Contains(x.AccountTypeID))/*.OrderByDescending(x=>x.AccountTypeID)*/.ToList();
                if (heads != null && heads.Count > 0)
                {
                    masterList = new List<BalanceSheetFormat>();
                    foreach (var h in heads)
                    {
                        BalanceSheetFormat head = new BalanceSheetFormat();
                        head.name = h.TypeName;
                        head.number = h.AccountTypeID.ToString();// + "--" + h.Code.ToString();
                        head.assets = RecursionOnHeads(h.AccountTypeID);
                        head.balance = head.assets != null ? head.assets.Sum(x => Convert.ToDecimal(x.balance)).ToString() : "0";//db.GetAccountBalances(null, null, null, h.AccountTypeID, null).Sum(x => x.Balance).ToString();

                        masterList.Add(head);
                    }


                }
                return masterList;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<BalanceSheetFormat> RecursiveCallToGenerateIncomeStatement()
        {
            try
            {
                List<BalanceSheetFormat> masterList = null;
                List<int> BalanceSheetHeadIDs = new List<int>() { 3, 5 };
                var heads = db.tbl_AccountType.Where(x => x.HeadID == null && BalanceSheetHeadIDs.Contains(x.AccountTypeID))/*.OrderByDescending(x=>x.AccountTypeID)*/.ToList();
                if (heads != null && heads.Count > 0)
                {
                    masterList = new List<BalanceSheetFormat>();
                    foreach (var h in heads)
                    {
                        BalanceSheetFormat head = new BalanceSheetFormat();
                        head.name = h.TypeName;
                        head.number = h.AccountTypeID.ToString();// + "--" + h.Code.ToString();
                        head.assets = RecursionOnHeadsPLS(h.AccountTypeID);
                        head.balance = head.assets != null ? head.assets.Sum(x => Convert.ToDecimal(x.balance)).ToString() : "0";//db.GetAccountBalances(null, null, null, h.AccountTypeID, null).Sum(x => x.Balance).ToString();

                        masterList.Add(head);
                    }

                    if (masterList.Count > 0)
                    {
                        BalanceSheetFormat head = new BalanceSheetFormat();
                        head.name = "Net Income";
                        head.number = "5.4";// + "--" + h.Code.ToString();
                        //head.assets = null;
                        head.balance = Convert.ToString(masterList.Where(x => x.number.Equals("5")).Sum(y => Convert.ToDecimal(y.balance)) - masterList.Where(x => x.number.Equals("3")).Sum(y => Convert.ToDecimal(y.balance)));
                        masterList.Add(head);
                    }


                }
                return masterList;

            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public List<Asset> RecursionOnHeads(int ID)
        {
            try
            {
                using (var d = new dbPOS())
                {
                    List<Asset> format = new List<Asset>();

                    if (ID <= 0)
                    {
                        return format;
                    }


                    var data = d.tbl_AccountType.Where(x => x.HeadID == ID).ToList();

                    if (data.Count > 0)
                    {
                        foreach (var i in data)
                        {
                            Asset asset = new Asset();
                            asset.Id = i.AccountTypeID;
                            asset.name = i.TypeName;
                            asset.number = i.AccountTypeID.ToString();// + "--" + i.Code.ToString();
                            //asset.assets = RecursionOnHeads(i.AccountTypeID);

                            if (RecursionOnHeads(i.AccountTypeID).Count > 0)
                            {
                                asset.assets = RecursionOnHeads(i.AccountTypeID);
                            }
                            else
                            {
                                var accDet = d.tbl_AccountDetails.Where(x => x.AccountTypeID == i.AccountTypeID).Select(x => new { Name = x.AccountName, ID = x.AccountID }).ToList();
                                if (accDet != null && accDet.Count > 0)
                                {
                                    asset.assets = new List<Asset>();
                                    foreach (var acc in accDet)
                                    {
                                        asset.assets.Add(new Asset
                                        {
                                            number = acc.ID.ToString(),
                                            name = acc.Name,
                                            balance = d.GetAccountBalancesWithoutRefAccountIDs(null, null, null, null, acc.ID).Sum(x => x.Balance).ToString()
                                        });
                                    }
                                }
                            }
                            //asset.balance = d.GetAccountBalances(null, null, null, i.AccountTypeID, null).Sum(x => x.Balance).ToString();
                            asset.balance = asset.assets != null ? asset.assets.Sum(x => Convert.ToDecimal(x.balance)).ToString() : "0";

                            format.Add(asset);
                        }
                    }


                    return format;
                }

            }
            catch
            {
                return null;
            }
        }

        public List<Asset> RecursionOnHeadsPLS(int ID)
        {
            try
            {
                using (var d = new dbPOS())
                {
                    List<Asset> format = new List<Asset>();

                    if (ID <= 0)
                    {
                        return format;
                    }


                    var data = d.tbl_AccountType.Where(x => x.HeadID == ID).ToList();

                    if (data.Count > 0)
                    {
                        foreach (var i in data)
                        {
                            Asset asset = new Asset();
                            asset.Id = i.AccountTypeID;
                            asset.name = i.TypeName;
                            asset.number = i.AccountTypeID.ToString();// + "--" + i.Code.ToString();
                            //asset.assets = RecursionOnHeads(i.AccountTypeID);

                            if (RecursionOnHeadsPLS(i.AccountTypeID).Count > 0)
                            {
                                asset.assets = RecursionOnHeadsPLS(i.AccountTypeID);
                            }
                            else
                            {
                                var accDet = d.tbl_AccountDetails.Where(x => x.AccountTypeID == i.AccountTypeID).Select(x => new { Name = x.AccountName, ID = x.AccountID }).ToList();
                                if (accDet != null && accDet.Count > 0)
                                {
                                    asset.assets = new List<Asset>();
                                    foreach (var acc in accDet)
                                    {
                                        asset.assets.Add(new Asset
                                        {
                                            number = acc.ID.ToString(),
                                            name = acc.Name,
                                            balance = d.GetAccountBalancesWithoutRefAccountIDs(null, null, null, null, acc.ID).Sum(x => x.Balance).ToString()
                                        });
                                    }
                                }
                            }
                            //asset.balance = d.GetAccountBalances(null, null, null, i.AccountTypeID, null).Sum(x => x.Balance).ToString();
                            asset.balance = asset.assets != null ? asset.assets.Sum(x => Convert.ToDecimal(x.balance)).ToString() : "0";

                            format.Add(asset);
                        }
                    }


                    return format;
                }

            }
            catch
            {
                return null;
            }
        }

        public JsonResult BalanceSheetData()
        {

            List<Models.DTO.BalanceSheetFormat> balSheetFormat = new List<Models.DTO.BalanceSheetFormat>();

            balSheetFormat = RecursiveCallToGenerateBalanceSheet();


            return Json(balSheetFormat, JsonRequestBehavior.AllowGet);
            ///
        }

        public JsonResult PLSData()
        {

            List<Models.DTO.BalanceSheetFormat> incomeStatement = new List<Models.DTO.BalanceSheetFormat>();

            incomeStatement = RecursiveCallToGenerateIncomeStatement();


            return Json(incomeStatement, JsonRequestBehavior.AllowGet);
            ///
        }
        // GET: Reports/GetAccountTypeID/5

        public ActionResult GetAccountTypeID(int? Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                List<GetAccountTypeTrialBalanceDetails_Result> Details = db.GetAccountTypeTrialBalanceDetails(Id).ToList();
                return View(Details);
            }


        }
        public ActionResult GetAccountIDWiseResult(int? AccountID)
        {
            if (AccountID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                // List<GetAccountDetailWiseTrialBalanceDetails_Result> Details = db.GetAccountDetailWiseTrialBalanceDetails(AccountID).ToList();
                return View(/*Details*/);
            }
        }

        //Details According to AccountID
        public ActionResult GetAccountID(int? JentryId)
        {
            if (JentryId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                var Jdetails = db.tbl_JDetail.Where(x => x.JEntryID == JentryId).ToList();

                return View(Jdetails);

            }
        }


        public ActionResult IncomeStatement_Old(bool? btn, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            if (btn.HasValue)
            {
                List<GetProfitLossReport_Result> sales = db.GetProfitLossReport(fromDate, toDate, BranchID).ToList();

                if (sales.Count > 0)
                {
                    return PartialView("_IncomeStatement", sales);
                }

                return PartialView("_IncomeStatement");
            }

            ViewBag.BranchID = db.tbl_Branch.Select(v => new { Value = v.BranchID, Name = v.BranchName }).ToList();
            return View("IncomeStatement_old");
        }

        public ActionResult IncomeStatement(bool? isSearch, DateTime? fromDate, DateTime? toDate)
        {
            if (isSearch == true)
            {
                List<PLSData> report = null;
                List<GetIncomeStatementFilterWise_Result> data = db.GetIncomeStatementFilterWise(fromDate, toDate).ToList();

                if (data != null && data.Count > 0)
                {
                    report = new List<PLSData>();
                    //report.plsReport = new List<PLSData>();

                    foreach (var d in data)
                    {
                        if (!d.Particulars.Equals("COGS (D)")) // Remove COGS 
                        {
                            report.Add(new Models.DTO.PLSData
                            {
                                HeadAccountType = d.HeadAccountType,
                                AccountType = d.AccountType,
                                Particulars = d.Particulars,
                                Total = d.Total ?? 0,
                                TotalInString = Helper.DigitsWithComma(d.Total)
                            });
                        }
                    }
                }

                return PartialView("_IncomeStatementNew", report);


            }
            return View();

        }

        public ActionResult TrialBalance(bool? btn, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            if (btn.HasValue)
            {
                List<GetTrialBalance_Result> sales = db.GetTrialBalance(fromDate, toDate, BranchID).ToList();

                if (sales.Count > 0)
                {
                    return PartialView("_TrialBalance", sales);
                }

                return PartialView("_TrialBalance");
            }

            ViewBag.BranchID = db.tbl_Branch.Select(v => new { Value = v.BranchID, Name = v.BranchName }).ToList();
            return View();
        }

        //public ActionResult TrialBalance(bool? isSearch, DateTime? fromDate, DateTime? toDate)
        //{
        //    List<GetTrialBalanceNewForDetail_Result> TrialBal = db.GetTrialBalanceNewForDetail(null, null, null).ToList();

        //    if (TrialBal.Count > 0)
        //    {
        //        return View(TrialBal);
        //    }
        //    else
        //    {
        //        return View();
        //    }
        //}

        //Get: Trial Balance
        //public ActionResult TrialBalance(bool? isSearch, DateTime? fromDate, DateTime? toDate)
        //{
        //    List<GetTrialBalance_Result> TrialBal = db.GetTrialBalance(null, null, 9001).ToList();
        //    if (TrialBal.Count > 0)
        //    {
        //        return View(TrialBal);
        //    }
        //    else
        //    {
        //        return View();
        //    }
        //}

        private Models.DTO.ExpensesTrialBalance MapExpTrialtoModel(GetExpensesTrialBalance_Result expenses)
        {

            Models.DTO.ExpensesTrialBalance ExpensesTB = new Models.DTO.ExpensesTrialBalance()
            {
                AccountID = expenses.AccountID,
                AccountTypeID = expenses.AccountTypeID,
                AccountName = expenses.AccountName,
                TotalCredit = expenses.TotalCredit,
                TotalDebit = expenses.TotalDebit,
                Balance = expenses.Balance

            };
            return ExpensesTB;


        }
        private Models.DTO.DailyExpenses MapDailyExpensestoModel(GetDailyExpensesList_Result expense)
        {
            Models.DTO.DailyExpenses dailyExpense = new Models.DTO.DailyExpenses()
            {
                AccountName = expense.AccountName,
                Description = expense.Description,
                Amount = expense.Amount,
                Detail = expense.Detail
            };

            return dailyExpense;
        }
        private Models.DTO.DailyReceiving MapDailyReceivingtoModel(GetDailyCashPaymentReceived_Result payments)
        {
            Models.DTO.DailyReceiving dailyPayment = new Models.DTO.DailyReceiving()
            {
                AccountName = payments.AccountName,
                Description = payments.Description,
                Amount = payments.Amount,
                Detail = payments.Detail
            };

            return dailyPayment;
        }

        //private Models.DTO.AssetsBalance MapAssetstoModel(GetTrialBalance_Result assets)//changed from gettrailbalance2 to gettrialbalance
        //{
        //    Models.DTO.AssetsBalance assetsBalance = new Models.DTO.AssetsBalance()
        //    {
        //        // AccountID = assets.AccountID,
        //        AccountTypeID = assets.AccountTypeID,
        //        HeadID = assets.HeadID,
        //        //////new is above included
        //        AccountName = assets.TypeName,
        //        TotalCredit = assets.TotalCredit,
        //        TotalDebit = assets.TotalDebit,
        //        Balance = assets.Balance
        //    };

        //    return assetsBalance;
        //}

        //private Models.DTO.LiabilityBalance MapLiabtoModel(GetTrialBalance_Result liabilities)//changed from gettrailbalance2 to gettrialbalance
        //{

        //    Models.DTO.LiabilityBalance liabilitiesBalance = new Models.DTO.LiabilityBalance()
        //    {
        //        // AccountID = liabilities.AccountID,
        //        AccountTypeID = liabilities.AccountTypeID,
        //        HeadID = liabilities.HeadID,
        //        //////new is above included
        //        //AccountName = liabilities.AccountName,
        //        AccountName = liabilities.TypeName,
        //        TotalCredit = liabilities.TotalCredit,
        //        TotalDebit = liabilities.TotalDebit,
        //        Balance = liabilities.Balance

        //    };
        //    return liabilitiesBalance;
        //}
        //private Models.DTO.EquityBalance MapEquitytoModel(GetTrialBalance_Result equity)//changed from trial balance 2 to trial balance
        //{
        //    Models.DTO.EquityBalance equityBalance = new Models.DTO.EquityBalance()
        //    {
        //        // AccountID = equity.AccountID,
        //        AccountTypeID = equity.AccountTypeID,
        //        HeadID = equity.HeadID,
        //        //////new is above included
        //        //AccountName = equity.AccountName,
        //        AccountName = equity.TypeName,
        //        TotalCredit = equity.TotalCredit,
        //        TotalDebit = equity.TotalDebit,
        //        Balance = equity.Balance
        //    };
        //    return equityBalance;
        //}
        //private Models.DTO.ExpensesTrialBalance MaptoModel1(GetTrialBalance_Result expenses)//Changed from trial balance 2 to trail balance
        //{

        //    Models.DTO.ExpensesTrialBalance ExpensesTB = new Models.DTO.ExpensesTrialBalance()
        //    {

        //        // AccountName = expenses.AccountName,
        //        TotalCredit = expenses.TotalCredit,
        //        TotalDebit = expenses.TotalDebit,
        //        Balance = expenses.Balance

        //    };
        //    return ExpensesTB;
        //}
        #endregion
    }
}
