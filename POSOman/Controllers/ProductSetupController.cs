﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class ProductSetupController : Controller
    {
        private dbPOS db = new dbPOS();
        // Products Image List 
        [Authorize]
        public ActionResult ProductFilterList(int? Filter)
        {           
            #region
            if (Filter == null)
            {
                return View();
            }
            else if (Filter == 1) // ALL
            {
                List<GetFilterProducts_Result> products = db.GetFilterProducts(Filter).ToList();
                return View(products);
            }
            else if (Filter == 2) // With images 
            {
                List<GetFilterProducts_Result> products = db.GetFilterProducts(Filter).ToList();
                return View(products);
            }
            else if (Filter == 3) // Without Images
            {
                List<GetFilterProducts_Result> products = db.GetFilterProducts(Filter).ToList();
                return View(products);
            }
           
            #endregion
          
            return View();
        }
        // GET: ProductSetup
        public ActionResult Index()
        {
            tbl_Product products = null;
            return View(products);
        }
        [HttpPost]
        public JsonResult ProductList(DTParameters param)
        {
            int TotalCount = 0;
            var filtered = this.GetProductFiltered(param.Search.Value, param.SortOrder, param.Start, param.Length, out TotalCount);

            var ProductList = filtered.Select(p => new ProductList()
            {
                ProductID = p.ProductID,
                VehicleCode = (p.VehicleCodeID.HasValue) ? p.tbl_VehicleCode.VehicleCode : "",
                ImageUrl = p.ImageUrl,
                PartNo = p.PartNo,
                Description = p.Description,
                BarCode = p.BarCode,
                UnitCode = p.UnitCode,
                SaleRate = p.SaleRate
            });

            DTResult<ProductList> finalresult = new DTResult<ProductList>
            {
                draw = param.Draw,
                data = ProductList.ToList(),
                recordsFiltered = TotalCount,
                recordsTotal = filtered.Count

            };

            return Json(finalresult);

        }
        public List<ProductList> GetProductFiltered(string search, string sortOrder, int start, int length, out int TotalCount)
        {


            var result = db.tbl_Product.OrderBy(p => p.ProductID).Include(t => t.tbl_VehicleCode).Where(p => (search == null || (p.PartNo != null && p.PartNo.ToLower().Contains(search.ToLower())
                || p.BarCode != null && p.BarCode.Contains(search)
                || p.tbl_VehicleCode.VehicleCode != null && p.tbl_VehicleCode.VehicleCode.ToLower().Contains(search.ToLower())

                || p.Product_Code != null && p.Product_Code.ToLower().Contains(search.ToLower())
                || p.UnitCode != null && p.UnitCode.ToLower().Contains(search.ToLower())
                || p.SaleRate != null && p.SaleRate.ToString().Contains(search.ToString())
                ))


                ).Skip(start).Take(length).ToList();
            TotalCount = db.tbl_Product.Count();
            //TotalCount = result.Count;

            // result = result.ToList();//.Skip(start).Take(length).ToList();
            Mapper.CreateMap<tbl_Product, Models.DTO.ProductList>();
            var details = Mapper.Map<ICollection<tbl_Product>, ICollection<Models.DTO.ProductList>>(result);
            return details.ToList();
        }
        // GET: Product/Create
        public ActionResult UploadPicture()
        {            
            return View();
        }
        // Upload Picture  
        [HttpPost]        
        public ActionResult UploadPicture(tbl_ProductDetail Product)
        {
            var user = User.Identity;
            int userID = 0;
            string currentUserId = User.Identity.GetUserId();
            var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);            
            userID = currentUser.UserId;
            if (Request.Files.Count > 0 && Request.Files["img"].ContentLength > 0)
            {

                HttpPostedFileBase postFile = Request.Files.Get("img");
                // var fileName = Path.GetFileName(postFile.FileName);
                string imgName = Product.ProductID + Path.GetFileName(postFile.FileName);
                string imgPath = "/Content/ProductImages/" + imgName;
                postFile.SaveAs(Server.MapPath(imgPath));
                var product = db.tbl_Product.Where(pr => pr.ProductID == Product.ProductID).FirstOrDefault();
                product.ImageUrl = imgPath;
                product.UpdateBy = userID;
                product.UpdateOn = DateTime.UtcNow.AddHours(5);
                db.Entry(product).State = EntityState.Modified;
                tbl_ProductDetail prodDetail = new tbl_ProductDetail();
                prodDetail.ImageUrl = imgPath;
                prodDetail.ProductID = Product.ProductID;
                db.tbl_ProductDetail.Add(prodDetail);
                //db.Entry(tbl_Product).Property(p => p.Addby).IsModified = false;
                //db.Entry(tbl_Product).Property(p => p.IsPacket).IsModified = false;
                //db.Entry(tbl_Product).Property(p => p.OpeningStock).IsModified = false;
                //db.Entry(tbl_Product).Property(p => p.Expiry).IsModified = false;
                //db.Entry(tbl_Product).Property(p => p.CostPrice).IsModified = false;
                //db.Entry(tbl_Product).Property(p => p.UserID).IsModified = false;
                //db.Entry(tbl_Product).Property(p => p.BranchID).IsModified = false;
                //db.Entry(tbl_Product).Property(p => p.AddOn).IsModified = false;
                //db.Entry(tbl_Product).Property(p => p.rowguid).IsModified = false;
                db.SaveChanges();
                return View();
            }
            else
            {
                return View();
            }
            
        }

    }
}