﻿using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class PreviousSalesReturnController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
       
        // GET: Previous Sales Return
        [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult Index()
        {
            int branchId = 0;
            int userID = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            var tbl_PrevSalesReturn = db.tbl_PrevSalesReturn.Where(so => so.BranchID == branchId);
            return View(tbl_PrevSalesReturn.ToList());
        }       
        
        // GET: SalesOrder/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<tbl_PrevSOReturnDetails> details = db.tbl_PrevSOReturnDetails.Where(p => p.ReturnID == id).ToList();

            if (details == null)
            {
                return HttpNotFound();
            }
            return PartialView(details);
        }        
        // GET: PrevSalesReturn/Create
        [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult Create()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BranchID = branchId;
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            //  ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            return View();
        }  
        public JsonResult SaveOrder(Models.tbl_PrevSalesReturn model, List<Models.DTO.StockLog> modelStockLog,int? bankAccId)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            POSOman.Models.BLL.PrevSalesReturn prSales = new Models.BLL.PrevSalesReturn();
            var orderID = prSales.Save(model, modelStockLog,bankAccId, branchId);
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Previous Sales Returning ".ToString());

            return Json(orderID);            
        }        
    }
}