﻿using POSOman.Models;
using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class AdjustAccountController : Controller
    {
        UserActionsPerformed UserActions = new UserActionsPerformed();
       
        // GET: AdjustAccount
        // Get Receiving Page
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult NewCustomerAdjustment()
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
                ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
                ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
                //ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code + "|" + c.Name }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View("CustomerAdj");
        }
        // Get Invoice Details of Customer 
        public JsonResult getCustomerDetail(int accountId)
        {
            if (accountId > 0)
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                try
                {
                    var qry = db.tbl_SalesOrder.Where(p => p.AccountID == accountId && p.IsPaid == false && p.IsReturned == false)
                        //&& p.BranchID == branchId)
                        .Select(p => new
                        {
                            p.OrderID,
                            p.SOID,
                            p.AccountID,
                            p.PaymentTypeID,
                            p.tbl_AccountDetails.AccountName,
                            p.PONo,
                            p.SalesDate,
                            p.TotalAmount,
                            p.ReturnAmount,
                            p.VAT,
                            p.AmountPaid,
                            p.TotalPaid,
                            p.PaymentStatus
                        }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }
            return Json("");

        }

        // Save Receivings of customer invoices
        public JsonResult SaveCustomerAdjustment(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            if (ModelState.IsValid)
            {
                object result = adEntry.SaveCustomerAdjustment(model,jentryLog,branchId);// customerPay.Save(model, jentryLog, bankAccId, branchId);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Customer Adjustments".ToString());

                return Json(result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Error Performed Customer Adjustments".ToString());

                return Json("formError");
            }
        }
        Models.BLL.AdjustmentEntry adEntry = new Models.BLL.AdjustmentEntry();
        dbPOS db = new dbPOS();
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Index()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);                
                branchId = currentUser.BranchID;
            }
            var adjustments = db.tbl_JDetail.Where(j => j.BranchID == branchId && (j.EntryTypeID == 14));
            return View(adjustments.ToList());
        }
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult CustomerAdjustment()
        {
            try
            {
                int branchId = 0;
                
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                ViewBag.BranchId = branchId;
                ViewBag.customer = db.tbl_Customer.Select(v => new
                {
                    Value = v.AccountID,
                    Name = v.Name
                }).ToList();
                ViewBag.customerCode = db.tbl_Customer.Select(c => new
                {
                    Value = c.AccountID,
                    Name = c.Code
                }).ToList();
                ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }
        // Get to Create Page
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult VendorAdjustment()
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                ViewBag.BranchId = branchId;
                //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
                ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }
        // Save adjustments Entry to Db
        public JsonResult SaveAdjustment(Expenses model, string CustomerName, bool? isVendor)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                object Result = adEntry.SaveEntry(model, CustomerName, isVendor,branchId);
                return Json(Result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            Models.DTO.PaymentLog jLog = new Models.DTO.PaymentLog();
            var query = from jlog in db.tbl_JEntryLog
                        join je in db.tbl_JEntry on jlog.JEntryID equals je.JEntryId
                        join jd in db.tbl_JDetail on je.JEntryId equals jd.JEntryID
                        join so in db.tbl_SalesOrder on jlog.OrderID equals so.OrderID
                        join acd in db.tbl_AccountDetails on jd.AccountID equals acd.AccountID
                        where jd.Cr > 0 && jlog.JEntryID == id
                        select new Models.DTO.JEntryLogModel
                        {
                            AccountName = acd.AccountName,
                            InvoiceNo = so.SOID
                        ,
                            Amount = jlog.Amount ?? 0
                        };
            if (query == null)
            {
                return HttpNotFound();
            }
            return View(query.ToList());
        }
        Models.BLL.CustomerPayment customerPay = new Models.BLL.CustomerPayment();
        [Authorize(Roles = "SuperAdmin")]        
        // POST: Entry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                customerPay.DeleteSpecialDiscount(id);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Customer Adjustments".ToString());

                return RedirectToAction("Index");
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
        }
        //[Authorize(Roles = "SuperAdmin")]
        //// GET: Jentry/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    int branchId = 0;

        //    if (Session["BranchID"] != null)
        //    {
        //        branchId = Convert.ToInt32(Session["BranchID"]);
        //    }
        //    else
        //    {
        //        var user = User.Identity;
        //        string currentUserId = User.Identity.GetUserId();
        //        var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
        //        branchId = currentUser.BranchID;
        //    }
        //    tbl_JEntry tbl_Jentry = db.tbl_JEntry.Where(j => j.JEntryId == id && j.BranchID == branchId).FirstOrDefault();
        //    if (tbl_Jentry == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tbl_Jentry);
        //}
        //[Authorize(Roles = "SuperAdmin")]
        //// POST: Entry/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    try
        //    {
        //        int branchId = 0;

        //        if (Session["BranchID"] != null)
        //        {
        //            branchId = Convert.ToInt32(Session["BranchID"]);
        //        }
        //        else
        //        {
        //            var user = User.Identity;
        //            string currentUserId = User.Identity.GetUserId();
        //            var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
        //            branchId = currentUser.BranchID;
        //        }
        //        db.DeleteJournalEntry(id);
        //        return RedirectToAction("Index");
        //    }
        //    catch (Exception err)
        //    {
        //        ModelState.AddModelError(String.Empty, err.Message);
        //        return View("Index");
        //    }
        //}
    }
}