﻿using POSOman.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models.DTO;
using System.Net;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;
using Microsoft.Reporting.WebForms;

namespace POSOman.Controllers
{
    public class ExpensesController : Controller
    {
        ExpensesEntry exp = new ExpensesEntry();
        UserActionsPerformed UserActions = new UserActionsPerformed();
       
        dbPOS db = new dbPOS();


        private int GetBranchID()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }


        // GET: Expenses
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            int branchId = GetBranchID();
            var customerVoucher = db.tbl_JDetail.Where(j => j.tbl_AccountDetails.tbl_AccountType.AccountTypeID == 3 && j.tbl_AccountDetails.AccountID > 33 && j.BranchID == branchId );
            return View(customerVoucher.ToList());
        }

        // Get to Create Page
        public ActionResult Create()
        {
            try
            {
                int branchId = GetBranchID();
                ViewBag.BranchId = branchId;
                //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
                ViewBag.Department = db.tbl_Department.Where(a => a.IsDeleted != true).Select(a => new { Value = a.ID, Name = a.Department }).ToList();
                //ViewBag.expenses = db.tbl_AccountDetails.Where(a => a.AccountTypeID == 3 && a.AccountID > 33).Select(a => new { Value = a.AccountID, Name = a.AccountName }).ToList();
                ViewBag.expenses = db.tbl_AccountDetails.Where(a => a.HeadID == 3 && a.AccountID > 109 && (a.IsDeleted != true || a.IsDeleted == null)).Select(a => new { Value = a.AccountID, Name = a.AccountName }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        // Save Expense Entry to Db
        public JsonResult SaveExpense(Expenses model, PaymentLog payLog, int? bankAccId)
        {
            if (ModelState.IsValid)
            {
                int branchId = GetBranchID();
                model.UserID = User.Identity.GetUserId();
                object Result = exp.Save(model, payLog,bankAccId,branchId);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Add Expanse Action".ToString());

                return Json(Result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Add Expanse Action".ToString());

                return Json("formError");
            }
        }


        public ActionResult Edit(int id)
        {
            try
            {
                int branchId = GetBranchID();
                ViewBag.BranchId = branchId;
                ViewBag.expenses = db.tbl_AccountDetails.Where(a => a.AccountTypeID == 3 && a.AccountID > 33).Select(a => new { Value = a.AccountID, Name = a.AccountName }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                ViewBag.SalesOrder = db.tbl_SalesOrder1.Where(a => a.IsActive == true && a.IsDeleted != true).Select(a => new { Value = a.OrderID, Name = a.StrCode }).ToList();

                ViewBag.JEntry = db.tbl_JEntry.Where(j => j.JEntryId == id && j.BranchID == branchId).FirstOrDefault();
                ViewBag.JDetail = db.tbl_JDetail.Where(j => j.JEntryID == id && j.BranchID == branchId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        // Save Expense Entry to Db
        public JsonResult EditExpense(Expenses model, PaymentLog payLog, int? bankAccId, int jentryid)
        {
            if (ModelState.IsValid)
            {
                int branchId = GetBranchID();
                db.DeleteJournalEntry(jentryid);
                object Result = exp.Save(model, payLog, bankAccId, branchId);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Edit Expanse Action".ToString());

                return Json(Result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Add Expanse Action".ToString());

                return Json("formError");
            }
        }




        // open Expense receipt
        public ActionResult Details(bool? isNew, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (isNew == true)
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.OrderByDescending(v => v.JDetailID).Where(j => j.AccountID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_ExpenseVoucher", _PaymentDetail);
            }
            else
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JEntryID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_ExpenseVoucher", _PaymentDetail);
            }
        }

        // open Expense receipt small
        public ActionResult Voucher(bool? isNew, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            if (isNew == true)
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.OrderByDescending(v => v.JDetailID).Where(j => j.AccountID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_ExpenseVoucherSmall", _PaymentDetail);
            }
            else
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JDetailID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_ExpenseVoucherSmall", _PaymentDetail);
            }
        }

        [Authorize(Roles = "SuperAdmin")]
        // GET: Jentry/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int branchId = GetBranchID();
            tbl_JEntry tbl_Jentry = db.tbl_JEntry.Where(j => j.JEntryId == id && j.BranchID == branchId).FirstOrDefault();
            if (tbl_Jentry == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Jentry);
        }

        [Authorize(Roles = "SuperAdmin")]
        // POST: Entry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int branchId = GetBranchID();
                db.DeleteJournalEntry(id);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Expanse Paying".ToString());

                return RedirectToAction("Index");
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
        }
    }
}
