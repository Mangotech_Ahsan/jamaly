﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using POSOman.Models.BLL;
using System.Drawing;
using System.IO;
using QRCoder;
using ZXing;
using System.Net.Http;
using System.Text;
using Microsoft.Reporting.WebForms;
using POSOman.Models.DTO;
using System.Threading.Tasks;

namespace POSOman.Controllers
{
    public class SalesOrderController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        ApplicationDbContext context;

        public int getBranchID()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        public FileResult Export(int id, String ReportType)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/saleInvoice.rdlc");


            ReportDataSource rs = new ReportDataSource();
            rs.Name = "DataSet1";
            rs.Value = db.SaleInvoiceNew(id, null, null,null);

            lo.DataSources.Add(rs);


            byte[] renderbyte;

            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            Response.AddHeader("Content-Disposition", "attachment; filename=SaleInvoice.pdf");

            return new FileContentResult(renderedBytes, mimeType);
        }

        public ActionResult ServiceSC()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            //ViewBag.BranchID = branchId;
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName");
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Product = db.tbl_Product.Where(x => x.VehicleCodeID == 1).Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.Desc = db.tbl_Product.Where(x => x.VehicleCodeID == 1).Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();

            ViewBag.VehCode = db.tbl_VehicleCode.Where(x => x.VehicleCodeID == 1).Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.PaymentTypeID = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();

            //    ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            return View();
        }

        // GET: SalesOrder
        // [Authorize(Roles = "SuperAdmin,Admin,BDM")]
        public ActionResult Index(bool? isNew, bool? btn, int? AccountID, int? OID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.customer = db.tbl_Customer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();

            int branchId = getBranchID();
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var s = UserManager.GetRoles(user.GetUserId());
            var userRole = s[0].ToString();

            if (userRole == "SuperAdmin")
            {
                ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            }
            else
            {
                ViewBag.Branch = db.tbl_Branch.Where(br => br.BranchID == branchId).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            }

            List<GetSalesIndexFilterWise_Result> stock = null;
            #region SuperAdmin
            if (userRole == "SuperAdmin" && isNew == null && btn == null)
            {
                return View();
            }
            else if (userRole == "SuperAdmin" && isNew == true) // ALL
            {
                toDate = DateTime.UtcNow.AddHours(5).Date;
                fromDate = DateTime.UtcNow.AddHours(5).Date;
                stock = db.GetSalesIndexFilterWise(BranchID, AccountID, null, OID, fromDate, toDate, null, null, null, null, null, null, null).ToList();
            }
            else if (userRole == "SuperAdmin" && isNew == null && btn.HasValue) // ALL
            {
                stock = db.GetSalesIndexFilterWise(BranchID, AccountID, null, OID, fromDate, toDate, null, null, null, null, null, null, null).ToList();
            }
            #endregion
            #region Other USers

            if (userRole != "SuperAdmin" && isNew == null && btn == null)
            {
                return View();
            }
            else if (userRole != "SuperAdmin" && isNew == true && btn.HasValue) // ALL
            {
                toDate = DateTime.UtcNow.AddHours(5).Date;
                fromDate = DateTime.UtcNow.AddHours(5).Date;
                stock = db.GetSalesIndexFilterWise(branchId, AccountID, null, OID, fromDate, toDate, currentUserId, null, null, null, null, null, null).ToList();
            }
            else if (userRole != "SuperAdmin" && isNew == null && btn.HasValue) // ALL
            {
                stock = db.GetSalesIndexFilterWise(branchId, AccountID, null, OID, fromDate, toDate, currentUserId, null, null, null, null, null, null).ToList();
            }

            #endregion

            if (stock.Count == 0)
            {
                return PartialView("_Index");
            }
            else
                return PartialView("_Index", stock.OrderByDescending(so => so.OrderID));

        }

        [Authorize]
        public ActionResult OldIndex(int? Filter, bool? isNew)
        {
            int branchId = 0;
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var s = UserManager.GetRoles(user.GetUserId());
            var userRole = s[0].ToString();
            #region SuperAdmin or Admin
            if ((userRole == "SuperAdmin" || userRole == "Admin") && isNew == null && Filter == null)
            {
                return View();
            }
            if ((userRole == "SuperAdmin" || userRole == "Admin") && isNew == null && Filter == 1) // ALL
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.SOID > 1000 && so.IsDeleted != true && (so.OnlineOrderID == 0 || so.OnlineOrderID == null)).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            if ((userRole == "SuperAdmin" || userRole == "Admin") && isNew == null && Filter == 2) // Today 
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.SOID > 1000 && so.IsDeleted != true && (so.OnlineOrderID == 0 || so.OnlineOrderID == null) && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month && DbFunctions.TruncateTime(so.SalesDate).Value.Day == DateTime.Now.Day).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            if ((userRole == "SuperAdmin" || userRole == "Admin") && isNew == null && Filter == 3) // This Month
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.SOID > 1000 && so.IsDeleted != true && (so.OnlineOrderID == 0 || so.OnlineOrderID == null) && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            if ((userRole == "SuperAdmin" || userRole == "Admin") && isNew == null && Filter == 4) // This Year
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.SOID > 1000 && so.IsDeleted != true && (so.OnlineOrderID == 0 || so.OnlineOrderID == null) && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            if ((userRole == "SuperAdmin" || userRole == "Admin") && isNew == true)
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && (so.OnlineOrderID == 0 || so.OnlineOrderID == null) && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            #endregion
            #region Other USers

            if ((userRole != "SuperAdmin" || userRole != "Admin") && isNew == null && Filter == null)
            {
                return View();
            }
            else if ((userRole != "SuperAdmin" || userRole != "Admin") && isNew == null && Filter == 1) // ALL
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && (so.OnlineOrderID == 0 || so.OnlineOrderID == null) && so.UserID == currentUserId).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if ((userRole != "SuperAdmin" || userRole != "Admin") && isNew == null && Filter == 2) // Today 
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && (so.OnlineOrderID == 0 || so.OnlineOrderID == null) && so.UserID == currentUserId && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month && DbFunctions.TruncateTime(so.SalesDate).Value.Day == DateTime.Now.Day).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if ((userRole != "SuperAdmin" || userRole != "Admin") && isNew == null && Filter == 3) // This Month
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && (so.OnlineOrderID == 0 || so.OnlineOrderID == null) && so.UserID == currentUserId && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if ((userRole != "SuperAdmin" || userRole != "Admin") && isNew == null && Filter == 4) // This Year
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && (so.OnlineOrderID == 0 || so.OnlineOrderID == null) && so.UserID == currentUserId && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if ((userRole != "SuperAdmin" || userRole != "Admin") && isNew == true)
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && (so.OnlineOrderID == 0 || so.OnlineOrderID == null) && so.UserID == currentUserId && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            #endregion
            return View();
        }

        public ActionResult PosNews()
        {

            int branchId = 0;
            context = new ApplicationDbContext();
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
            branchId = currentUser.BranchID;
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var role = UserManager.GetRoles(user.GetUserId());


            if (role[0].ToString() == "SuperAdmin")
            {
                ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName");
            }
            else
            {


                ViewBag.BranchID = new SelectList(db.tbl_Branch.Where(p => p.BranchID == branchId), "BranchID", "BranchName");
            }
            ViewBag.BranchID = branchId;

            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();

            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            return View("PosNews");
        }

        // [Authorize(Roles = "SuperAdmin,Admin,SalesPerson,Accountant")]
        public ActionResult POSSales()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.AccountID == 33 && so.SOID > 1000 && so.IsDeleted != true && (so.OnlineOrderID == 0 || so.OnlineOrderID == null)).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
            return View(tbl_SalesOrder.ToList());
        }
        //BarCode   

        public void RenderBarcode(string Code, string QRCode)
        {
            Image img = null;
            using (var ms = new MemoryStream())
            {
                var writer = new BarcodeWriter() { Format = BarcodeFormat.CODE_128 };
                writer.Options.Height = 80;
                writer.Options.Width = 280;
                writer.Options.PureBarcode = true;
                img = writer.Write(Code);
                // save image in files
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                Session["BCImage"] = "data:image/jpeg;base64," + Convert.ToBase64String(ms.ToArray());
            }
        }
        // Get Sales Products of Last Invoice 
        public JsonResult getLastSalesItems()
        {
            try
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                int lastOrderID = db.tbl_SalesOrder.Where(s => s.BranchID == branchId).OrderByDescending(s => s.OrderID).Select(s => s.OrderID).FirstOrDefault();
                var qry = db.tbl_SaleDetails.Where(s => s.OrderID == lastOrderID).OrderByDescending(s => s.OrderID)
                    .Select(s => new
                    {
                        s.ProductID,
                        s.tbl_Product.PartNo,
                        s.tbl_Product.tbl_VehicleCode.VehicleCode,
                        s.tbl_Product.Description,
                        s.tbl_Product.UnitCode,
                        s.tbl_Product.OpenUnitCode,
                        s.Qty,
                        s.UnitPerCarton,
                        s.SalePrice,
                        s.UnitPrice,
                        s.Total
                    }).ToList();
                return Json(new { qry }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }
        //
        //[Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult CreateChallanFromSale(int? id)
        {
            ViewBag.OrdId = id;
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            string userRoleId = db.AspNetUserRoles.Where(x => x.UserId.Equals(currentUserId)).Select(x => x.RoleId).FirstOrDefault();
            string userRole = "";

            if (!string.IsNullOrWhiteSpace(userRoleId))
            {
                userRole = db.AspNetRoles.Where(x => x.Id.Equals(userRoleId)).Select(x => x.Name).FirstOrDefault();

            }
            ViewBag.userRole = userRole.ToString();
            ViewBag.BranchId = db.tbl_Branch.Select(v => new { Value = v.BranchID, Name = v.BranchName }).ToList(); //branchId;

            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.Artno = db.tbl_Product.Where(x => x.isActive == null || x.isActive == 1).Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.Desc = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            //  ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();

            if (User.IsInRole("SuperAdmin"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            if (User.IsInRole("Satin Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            if (User.IsInRole("Sticker Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            if (User.IsInRole("Offset Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            return View("CreateChallan");
        }

        public JsonResult GetJobOrderAttachedMachineDetails(int OrderID, int ItemID)
        {
            try
            {
                var data = db.tbl_JobOrderMachinesLogs.Where(x => x.ItemID == ItemID && x.OrderID == OrderID && x.IsDeleted != true).OrderBy(x => x.ID).ToList();
                if (data != null && data.Count > 0)
                {
                    List<object> list = new List<object>();
                    foreach (var i in data)
                    {
                        list.Add(new
                        {
                            RowID = i.ID,
                            MachineName = i.tbl_OrderMachine.OrderMachine ?? "-",
                            MachineType = i.tbl_OrderMachineTypes.TypeName ?? "-",
                            IsCurrentMachine = i.IsCurrentMachine == true ? 1 : 0,
                            AddedOn = Convert.ToDateTime(i.AddedOn).ToShortDateString() + " " + Convert.ToDateTime(i.AddedOn).ToShortTimeString(),
                        });
                    }

                    return Json(list, JsonRequestBehavior.AllowGet);
                }

                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetItemPackageDetails(int OrderID, int ItemID)
        {
            try
            {
                //var data = db.tbl_SOItemWisePackageDetails.Where(x => x.ItemID == ItemID && x.OrderID == OrderID && x.ChallanID == null && x.IsDeleted != true).ToList();
                var data = db.tbl_SOItemWisePackageDetails.Where(x => x.ItemID == ItemID && x.OrderID == OrderID && x.IsDeleted != true).ToList();
                if (data != null && data.Count > 0)
                {
                    List<object> list = new List<object>();
                    foreach (var i in data)
                    {
                        if(i.ProductQty == i.DeliveredQty)
                        {
                            list.Add(new
                            {
                                IsChkBoxEnabled = i.ProductQty == i.DeliveredQty ? false : i.DeliverQty == 0 ? false : true,
                                RowID = i.ID,
                                ItemID = i.ItemID,
                                OrderID = i.OrderID,
                                Description = string.IsNullOrWhiteSpace(i.Description) == true ? "-" : i.Description,
                                Qty = i.ProductQty,
                                DeliverQty = i.DeliverQty,
                                DeliveredQty = i.DeliveredQty,
                                PackageId = i.PackageId,
                                ChallanID = 0//i.ChallanID
                            });
                        }
                        else if(/*i.ChallanID == null &&*/ i.ProductQty> i.DeliveredQty)
                        {
                            list.Add(new
                            {
                                IsChkBoxEnabled = i.ProductQty == i.DeliveredQty ? false : i.DeliverQty == 0 ? false : true,
                                RowID = i.ID,
                                ItemID = i.ItemID,
                                OrderID = i.OrderID,
                                Description = string.IsNullOrWhiteSpace(i.Description) == true ? "-" : i.Description,
                                Qty = i.ProductQty,
                                DeliverQty = i.DeliverQty,
                                DeliveredQty = i.DeliveredQty,
                                PackageId = i.PackageId,
                                ChallanID = 0//i.ChallanID
                            });
                        }
                        
                    }

                    return Json(list, JsonRequestBehavior.AllowGet);
                }

                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetItemPackageDetailsForChallanEdit(int OrderID, int ItemID, int ChallanID)
        {
            try
            {
                var data = db.tbl_SOItemWisePackageDetails.Where(x => x.ItemID == ItemID && x.OrderID == OrderID /*&& x.ChallanID == ChallanID*/ && x.IsDeleted != true).ToList();
                if (data != null && data.Count > 0)
                {
                    List<object> list = new List<object>();
                    foreach (var i in data)
                    {
                        var getChallanDataSoItemPackage = db.tbl_SOItemWisePackageChallanWiseDetails.Where(x => x.ItemID == i.ItemID && x.PackageId.Trim().Equals(i.PackageId.Trim()) && x.ChallanID == ChallanID).FirstOrDefault();

                        if (getChallanDataSoItemPackage != null)
                        {
                            list.Add(new
                            {
                                IsChkBoxEnabled = i.ProductQty == i.DeliveredQty ? false : i.DeliverQty == 0 ? false : true,
                                RowID = i.ID,
                                ItemID = i.ItemID,
                                OrderID = i.OrderID,
                                Description = string.IsNullOrWhiteSpace(i.Description) == true ? "-" : i.Description,
                                Qty = getChallanDataSoItemPackage.Qty,
                                DeliverQty = i.DeliverQty,
                                //DeliveredQty = i.DeliveredQty,
                                DeliveredQty = getChallanDataSoItemPackage.Qty,
                                PackageId = i.PackageId.Trim().ToString(),
                                IsReturned = getChallanDataSoItemPackage.IsReturned,
                                IsDeleted = getChallanDataSoItemPackage.IsDeleted
                            });
                        }

                    }

                    return Json(list, JsonRequestBehavior.AllowGet);
                }

                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        #region Old Logic
        //public JsonResult GetItemPackageDetailsForChallanEdit(int OrderID, int ItemID, int ChallanID)
        //{
        //    try
        //    {
        //        var data = db.tbl_SOItemWisePackageDetails.Where(x => x.ItemID == ItemID && x.OrderID == OrderID /*&& x.ChallanID == ChallanID*/ && x.IsDeleted != true).ToList();
        //        if (data != null && data.Count > 0)
        //        {
        //            List<object> list = new List<object>();
        //            foreach (var i in data)
        //            {
        //                list.Add(new
        //                {
        //                    IsChkBoxEnabled = i.ProductQty == i.DeliveredQty ? false : i.DeliverQty == 0 ? false : true,
        //                    RowID = i.ID,
        //                    ItemID = i.ItemID,
        //                    OrderID = i.OrderID,
        //                    Description = string.IsNullOrWhiteSpace(i.Description) == true ? "-" : i.Description,
        //                    Qty = i.ProductQty,
        //                    DeliverQty = i.DeliverQty,
        //                    DeliveredQty = i.DeliveredQty,
        //                    PackageId = i.PackageId.Trim().ToString()
        //                });
        //            }

        //            return Json(list, JsonRequestBehavior.AllowGet);
        //        }

        //        return Json(null, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        while (e.InnerException != null)
        //        {
        //            e = e.InnerException;
        //        }

        //        return Json(e.Message, JsonRequestBehavior.AllowGet);
        //    }
        //}
        #endregion
        public async Task<JsonResult> UpdateDispatchQty(List<ItemWisePackageDTO> packageModel)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    int c = 0;

                    if (packageModel != null && packageModel.Count > 0)
                    {

                        var OrderID = packageModel.Select(x => new { x.OrderID, x.ItemID }).FirstOrDefault();
                        if (OrderID != null)
                        {
                            var prevData = db.tbl_SOItemWisePackageDetails.Where(x => x.OrderID == OrderID.OrderID && x.ItemID == OrderID.ItemID /*&& x.ChallanID == null*/).ToList();
                            if (prevData != null)
                            {
                                packageModel = packageModel.Where(x => prevData.Select(y => y.PackageId).Contains(x.PackageId)).ToList();

                                if(packageModel!=null && packageModel.Count > 0)
                                {
                                    var detail = db.tbl_SaleDetails.Where(x => x.OrderID == OrderID.OrderID && x.ProductID == OrderID.ItemID).FirstOrDefault();
                                    if (detail != null)
                                    {
                                        int prevReadyQty = detail.ReadyQty;
                                        detail.ReadyQty = packageModel.Sum(x => x.DeliverQty);
                                        db.Entry(detail).State = EntityState.Modified;
                                        c += await db.SaveChangesAsync();


                                    }


                                    List<tbl_SOItemWisePackageDetails> newList = new List<tbl_SOItemWisePackageDetails>();
                                    foreach (var i in packageModel)
                                    {

                                        if(i.ItemID > 0 && i.OrderID > 0 && !string.IsNullOrWhiteSpace(i.PackageId))
                                        {
                                            var soItemPackage = db.tbl_SOItemWisePackageDetails.Where(x => x.ItemID == i.ItemID && x.OrderID == i.OrderID && x.PackageId.Trim().Equals(i.PackageId.Trim())).FirstOrDefault();

                                            if (soItemPackage != null)
                                            {
                                                soItemPackage.DeliverQty = i.DeliverQty;
                                                //soItemPackage.DeliveredQty = i.DeliveredQty;
                                                //soItemPackage.PackageId = i.PackageId.Trim().ToString();

                                                db.Entry(soItemPackage).State = EntityState.Modified;
                                                c += await db.SaveChangesAsync();
                                            }
                                        }
                                        //if (prevData.Any(x => x.ItemID == i.ItemID && x.OrderID == i.OrderID && x.PackageId == i.PackageId))
                                        //{
                                        //    tbl_SOItemWisePackageDetails det = new tbl_SOItemWisePackageDetails();
                                        //    Guid guid = Guid.NewGuid();
                                        //    det.AddedOn = Helper.PST();
                                        //    det.IsDeleted = false;
                                        //    det.DeliverQty = i.DeliverQty;
                                        //    det.ItemID = i.ItemID;
                                        //    det.OrderID = i.OrderID;
                                        //    det.DeliveredQty = i.DeliveredQty;
                                        //    det.Description = i.Description;
                                        //    det.PackageId = string.IsNullOrWhiteSpace(i.PackageId) ? guid.ToString() : i.PackageId.Trim().ToString();
                                        //    det.ProductQty = i.Qty;


                                        //    newList.Add(det);
                                        //}

                                        /// 


                                    }
                                    //db.tbl_SOItemWisePackageDetails.RemoveRange(prevData);

                                    //if (newList.Count > 0)
                                    //{
                                    //    db.tbl_SOItemWisePackageDetails.AddRange(newList);
                                    //    c = await db.SaveChangesAsync();
                                    //}
                                }
                                

                            }

                        }

                    }


                    if (c > 0)
                    {
                        UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Package Dispatch Qty Update".ToString());
                        t.Commit();
                        return Json(200, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        t.Rollback();
                        return Json(-2, JsonRequestBehavior.AllowGet);
                    }

                }

            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }

        public async Task<JsonResult> UpdatePackageDetails(List<ItemWisePackageDTO> packageModel)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    int c = 0;

                    if (packageModel != null && packageModel.Count > 0)
                    {
                        if (packageModel.Any(x => x.DeliverQty <= 0))
                        {
                            t.Rollback();
                            return Json(-6);
                        }
                        var OrderID = packageModel.Select(x => new { x.OrderID, x.ItemID }).FirstOrDefault();
                        if (OrderID != null)
                        {
                            List<tbl_SOItemWisePackageDetails> prevList = new List<tbl_SOItemWisePackageDetails>();

                            var prevData = db.tbl_SOItemWisePackageDetails.Where(x => x.OrderID == OrderID.OrderID && x.ItemID == OrderID.ItemID).ToList();
                            if (prevData != null)
                            {
                                foreach (var io in prevData)
                                {
                                    tbl_SOItemWisePackageDetails det = new tbl_SOItemWisePackageDetails();

                                    det.DeliveredQty = io.DeliveredQty;
                                    det.ItemID = io.ItemID;
                                    det.OrderID = io.OrderID;

                                    prevList.Add(det);
                                }

                                db.tbl_SOItemWisePackageDetails.RemoveRange(prevData);
                                db.SaveChanges();
                            }


                            List<tbl_SOItemWisePackageDetails> newList = new List<tbl_SOItemWisePackageDetails>();

                            foreach (var i in packageModel)
                            {
                                var newData = prevList.Where(x => x.OrderID == i.OrderID && x.ItemID == i.ItemID).FirstOrDefault();
                                if (newData != null)
                                {
                                    tbl_SOItemWisePackageDetails det = new tbl_SOItemWisePackageDetails();
                                    det.AddedOn = Helper.PST();
                                    det.IsDeleted = false;
                                    det.DeliverQty = i.DeliverQty;
                                    det.ItemID = i.ItemID;
                                    det.OrderID = i.OrderID;
                                    det.DeliveredQty = newData.DeliveredQty;
                                    det.Description = i.Description;
                                    det.ProductQty = i.Qty;
                                    newList.Add(det);
                                }

                            }
                            db.tbl_SOItemWisePackageDetails.AddRange(newList);
                            c = await db.SaveChangesAsync();
                        }

                    }


                    if (c > 0)
                    {
                        UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Package Challan Update".ToString());
                        t.Commit();
                        return Json(200);
                    }
                    else
                    {
                        t.Rollback();
                        return Json(-2);
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.InnerException.Message);
                return Json(200);
            }

        }

        public JsonResult GetChallanDetailsFromSales(int? OrderId, string currentUserRole)
        {
            List<object> objectList = new List<object>();
            if (string.IsNullOrWhiteSpace(currentUserRole))
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            // int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (OrderId > 0)
            {
                var SaleOrderInv = db.tbl_SalesOrder.Where(x => x.OrderID == OrderId).FirstOrDefault();
                var CustomerPO = SaleOrderInv.CustomerPO;
                var Remarks = SaleOrderInv.Remarks;
                var qry = db.tbl_Customer.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(p => new { Value = p.AccountID, Name = p.Name }).FirstOrDefault();
                var qry1 = db.tbl_SalesOrder.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(p => new { Value = p.BranchID, Name = p.tbl_Branch.BranchName }).FirstOrDefault();
                var qry2 = db.tbl_Customer.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(x => x.CreditLimit).FirstOrDefault();
                var FinalAmount = (SaleOrderInv.VAT == 0) ? SaleOrderInv.TotalAmount : SaleOrderInv.TotalAmount + SaleOrderInv.VAT;
                var Discount = SaleOrderInv.DiscountAmount;
                //string SalesDate = Convert.ToDateTime(SaleOrderInv.SalesDate).ToShortDateString();
                var SalePersonAccID = SaleOrderInv.SalePersonAccID ?? 0;
                var SubAmount = SaleOrderInv.TotalAmount + SaleOrderInv.DiscountAmount;
                var TotalAmount = SaleOrderInv.TotalAmount;
                var Tax = SaleOrderInv.VAT;
                var CreditDays = SaleOrderInv.CreditDays;
                var AmountPaid = SaleOrderInv.AmountPaid;
                var chequeDate = SaleOrderInv.ChequeDate;
                var salesDate = SaleOrderInv.SalesDate;
                var Paytype = db.tbl_PaymentTypes.Where(x => x.ID == SaleOrderInv.PaymentTypeID).Select(x => new { Value = x.ID, Name = x.Name }).FirstOrDefault() == null ? null : db.tbl_PaymentTypes.Where(x => x.ID == SaleOrderInv.PaymentTypeID).Select(x => new { Value = x.ID, Name = x.Name }).FirstOrDefault();
                var Cheque = SaleOrderInv.ChequeNo;
                var ChqDate = SaleOrderInv.ChequeDate;
                var ProductsList = db.tbl_SaleDetails.Where(x => x.OrderID == OrderId && (x.IsDeleted != true || x.IsDeleted == null)).
                    Select(p => new
                    {
                        ProductID = p.ProductID,
                        Cat = p.tbl_Product.tbl_VehicleCode.VehicleCode,
                        CatName = p.tbl_Product.VehicleCodeID,
                        PartNo = p.PartNo,
                        Desc = p.tbl_Product.Description,
                        UnitCode = p.UnitCode,
                        Qty = p.Qty,
                        ReadyQty = p.ReadyQty,
                        DeliveredQty = p.QtyDelivered,
                        Packet = p.Packet,
                        IsMinor = p.IsMinor,
                        IsPack = p.IsPack,
                        UnitID = p.UnitID,
                        LevelID = p.LevelID,
                        MinorDivisor = p.MinorDivisor,
                        UnitPerCarton = p.UnitPerCarton,
                        CardQuality = p.tbl_Product.CardQuality == null ? "-" : p.tbl_Product.tbl_Card_Quality.CardName,
                        SalePrice = p.SalePrice,
                        UnitPrice = p.UnitPrice ?? 0,
                        PTotal = p.Total,
                        p.MachineImpression,
                        Impression = p.tbl_Product.NOOfImpressions * ((p.tbl_Product.TextColor * p.tbl_Product.TextCost) + (p.tbl_Product.GroundColor * p.tbl_Product.GroundCost)),
                        p.OrderMachineID,
                        OrderMachine = p.tbl_OrderMachine.OrderMachine ?? "-",
                        p.StoreStatusID,
                        StoreStatus = p.tbl_StoreStatus.StoreStatus ?? "-",
                        p.DeliveryStatusID,
                        DeliveryStatus = p.tbl_DeliveryStatus.DeliveryStatus ?? "-",
                        p.NoOfSheets,
                        p.TotalSheets,
                        Upping = p.tbl_Product.Upping,
                        JobSize = (p.tbl_Product.HSheetSize.ToString() + " X " + p.tbl_Product.WSheetSize.ToString()).ToString(),
                        GSM = p.tbl_Product.CardGSM,
                        RawSize = p.tbl_Product.RawSizeSheets,
                        SplitSheets = p.tbl_Product.Split,

                        PrintingMachine = p.tbl_Product.tbl_PrintingMachine.PrintingMachine ?? "-",
                        Cylinder = p.tbl_Product.tbl_Cylinder == null ? "-" : p.tbl_Product.tbl_Cylinder.Cylinder ?? "-",
                        Length = p.tbl_Product.Length,
                        Width = p.tbl_Product.Width,
                        CylinderLength = p.tbl_Product.tbl_Cylinder == null ? 0 : p.tbl_Product.tbl_Cylinder.CylinderLength,
                        AroundUps = p.tbl_Product.AroundUps,
                        NoOfPrintingColor = p.tbl_Product.NoOfPrintingColors,
                        Roll = p.tbl_Product.tbl_Roll == null ? "-" : p.tbl_Product.tbl_Roll.Roll ?? "-",
                        TotalPcInRoll = p.tbl_Product.TotalPcsInRoll,
                        WastagePcInRoll = p.tbl_Product.WastagePcsInRoll,
                        TotalReqRoll = p.tbl_Product.TotalRequiredRolls,
                        FinishingType = p.tbl_Product.tbl_FinishingType == null ? "-" : p.tbl_Product.tbl_FinishingType.FinishingType ?? "-",
                        BlockType = p.tbl_Product.tbl_BlockType == null ? "-" : p.tbl_Product.tbl_BlockType.BlockType ?? "-"





                    }).ToList();

                StringBuilder deliveryData = new StringBuilder();
                var renderDeliveryStatusDD = db.tbl_DeliveryStatus.Where(x => x.IsDeleted != true).Select(x => new { ID = x.ID, Value = x.DeliveryStatus }).ToList();
                //if(renderDeliveryStatusDD.Count>0 && renderDeliveryStatusDD != null)
                //{
                //    foreach(var i in renderDeliveryStatusDD)
                //    {
                //        var PrevDeliveryID = ProductsList.Where(x => x.DeliveryStatusID == i.ID).Select(x => x.DeliveryStatusID).FirstOrDefault() ?? 0;
                //        if (PrevDeliveryID > 0)
                //        {
                //            deliveryData.Append("<option value =" + i.ID + " selected='selected'>" + i.Value + "</option>");
                //        }
                //        else
                //        {
                //            deliveryData.Append("<option value =" + i.ID + ">" + i.Value + "</option>");
                //        }

                //    }
                //}

                StringBuilder storeData = new StringBuilder();
                var renderStoreStatusDD = db.tbl_StoreStatus.Where(x => x.IsDeleted != true).Select(x => new { ID = x.ID, Value = x.StoreStatus }).ToList();
                //if (renderStoreStatusDD.Count > 0 && renderStoreStatusDD != null)
                //{
                //    foreach (var i in renderStoreStatusDD)
                //    {
                //        var PrevStoreID = ProductsList.Where(x => x.StoreStatusID == i.ID).Select(x => x.StoreStatusID).FirstOrDefault() ?? 0;
                //        if (PrevStoreID > 0)
                //        {
                //            storeData.Append("<option value =" + i.ID + " selected='selected'>" + i.Value + "</option>");
                //        }
                //        else
                //        {
                //            storeData.Append("<option value =" + i.ID + ">" + i.Value + "</option>");
                //        }

                //    }
                //}

                StringBuilder orderMachineData = new StringBuilder();
                var renderSlittingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 1).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderPrintingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 2).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderDieCuttingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 3).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderUVMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 4).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderLaminationMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 5).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderFoilMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 6).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderEmbosingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 7).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                //if (renderOrderMachineDD.Count > 0 && renderOrderMachineDD != null)
                //{
                //    foreach (var i in renderOrderMachineDD)
                //    {
                //        var PrevOrderMachineID = ProductsList.Where(x => x.OrderMachineID == i.ID).Select(x => x.OrderMachineID).FirstOrDefault() ?? 0;
                //        if (PrevOrderMachineID > 0)
                //        {
                //            string str = "\"selected\"";
                //            orderMachineData.Append("<option value =" + i.ID + " selected=" + str + ">" + i.Value + "</option>");
                //        }
                //        else
                //        {
                //            orderMachineData.Append("<option value =" + i.ID + ">" + i.Value + "</option>");
                //        }

                //    }
                //}

                //var storeStatusDD = db.tbl_StoreStatus.Where(x=>x.ID == )

                var Banks = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 18).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                int BankId = 0;
                foreach (var i in Banks)
                {
                    if (i.Name.Equals(SaleOrderInv.BankName))
                    {
                        BankId = i.Value;
                    }
                }
                var Bank = db.tbl_SalesOrder.Where(x => x.OrderID == OrderId).Select(p => new { Value = BankId, Name = p.BankName }).FirstOrDefault();
                var PayStatus = 0;

                if (SaleOrderInv.PaymentStatus == "UnPaid")
                {
                    PayStatus = 3;
                }
                else if (SaleOrderInv.PaymentStatus == "Paid")
                {
                    PayStatus = 1;
                }
                if (SaleOrderInv.PaymentStatus == "Partial Paid")
                {
                    PayStatus = 2;
                }



                objectList.Add(new
                {
                    Qry = qry,
                    Qry1 = qry1,
                    Qry2 = qry2,
                    CustomerPO = CustomerPO,
                    Remarks = Remarks,
                    chequeDate = chequeDate,
                    SalePersonAccID = SalePersonAccID,
                    salesDate = salesDate,
                    ExpectedDeliveryDate = Convert.ToDateTime(SaleOrderInv.ExpectedDeliveryDate).ToShortDateString(),
                    FinalAmount = FinalAmount,
                    Discount = Discount,
                    SubAmount = SubAmount,
                    TotalAmount = TotalAmount,
                    CreditDays = CreditDays,
                    Tax = Tax,
                    AmountPaid = AmountPaid,
                    Paytype = Paytype,
                    PayStatus = PayStatus,
                    Cheque = Cheque,
                    Bank = Bank,
                    ProductsList = ProductsList,
                    deliveryDataDD = renderDeliveryStatusDD,
                    storeDataDD = renderStoreStatusDD,

                    slittingMachineDataDD = renderSlittingMDD,
                    printingMachineDataDD = renderPrintingMDD,
                    dieCuttingMachineDataDD = renderDieCuttingMDD,
                    uvMachineDataDD = renderUVMDD,
                    laminationMachineDataDD = renderLaminationMDD,
                    foilMachineDataDD = renderFoilMDD,
                    embosingMachineDataDD = renderEmbosingMDD,
                    userRole = currentUserRole

                });
            }
            return Json(objectList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEditInvoiceCustomer(int? OrderId, string currentUserRole, int DeptID)
        {
            List<object> objectList = new List<object>();
            if (string.IsNullOrWhiteSpace(currentUserRole))
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            // int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (OrderId > 0 && DeptID > 0)
            {
                var SaleOrderInv = db.tbl_SalesOrder.Where(x => x.OrderID == OrderId).FirstOrDefault();
                var qry = db.tbl_Customer.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(p => new { Value = p.AccountID, Name = p.Name }).FirstOrDefault();
                var qry1 = db.tbl_SalesOrder.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(p => new { Value = p.BranchID, Name = p.tbl_Branch.BranchName }).FirstOrDefault();
                var qry2 = db.tbl_Customer.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(x => x.CreditLimit).FirstOrDefault();
                var FinalAmount = (SaleOrderInv.VAT == 0 || SaleOrderInv.VAT == null) ? SaleOrderInv.TotalAmount : SaleOrderInv.TotalAmount + SaleOrderInv.VAT;
                var Discount = SaleOrderInv.DiscountAmount;
                //string SalesDate = Convert.ToDateTime(SaleOrderInv.SalesDate).ToShortDateString();
                var SalePersonAccID = SaleOrderInv.SalePersonAccID ?? 0;
                var SubAmount = SaleOrderInv.TotalAmount + SaleOrderInv.DiscountAmount;
                var TotalAmount = SaleOrderInv.TotalAmount;
                var Tax = SaleOrderInv.VAT;
                var CreditDays = SaleOrderInv.CreditDays;
                var AmountPaid = SaleOrderInv.AmountPaid;
                var chequeDate = SaleOrderInv.ChequeDate;
                var salesDate = SaleOrderInv.SalesDate;
                var Paytype = db.tbl_PaymentTypes.Where(x => x.ID == SaleOrderInv.PaymentTypeID).Select(x => new { Value = x.ID, Name = x.Name }).FirstOrDefault() == null ? null : db.tbl_PaymentTypes.Where(x => x.ID == SaleOrderInv.PaymentTypeID).Select(x => new { Value = x.ID, Name = x.Name }).FirstOrDefault();
                var Cheque = SaleOrderInv.ChequeNo;
                var ChqDate = SaleOrderInv.ChequeDate;
                var ProductsList = db.tbl_SaleDetails.Where(x => x.OrderID == OrderId && (x.IsDeleted != true || x.IsDeleted == null) && x.tbl_Product.DepartmentID == DeptID).
                    Select(p => new
                    {
                        ProductID = p.ProductID,
                        Cat = p.tbl_Product.tbl_VehicleCode.VehicleCode,
                        CatName = p.tbl_Product.VehicleCodeID,
                        PartNo = p.PartNo,
                        Desc = p.tbl_Product.Description,
                        UnitCode = p.UnitCode,
                        Qty = p.Qty,
                        Packet = p.Packet,
                        IsMinor = p.IsMinor,
                        IsPack = p.IsPack,
                        UnitID = p.UnitID,
                        LevelID = p.LevelID,
                        MinorDivisor = p.MinorDivisor,
                        UnitPerCarton = p.UnitPerCarton,
                        CardQuality = p.tbl_Product.CardQuality == null ? "-" : p.tbl_Product.tbl_Card_Quality.CardName,
                        SalePrice = p.SalePrice,
                        UnitPrice = p.UnitPrice ?? 0,
                        PTotal = p.Total,
                        p.MachineImpression,
                        Impression = p.tbl_Product.NOOfImpressions * ((p.tbl_Product.TextColor * p.tbl_Product.TextCost) + (p.tbl_Product.GroundColor * p.tbl_Product.GroundCost)),
                        p.OrderMachineID,
                        OrderMachine = p.tbl_OrderMachine.OrderMachine ?? "-",
                        p.StoreStatusID,
                        StoreStatus = p.tbl_StoreStatus.StoreStatus ?? "-",
                        p.DeliveryStatusID,
                        DeliveryStatus = p.tbl_DeliveryStatus.DeliveryStatus ?? "-",
                        p.NoOfSheets,
                        p.TotalSheets,
                        Upping = p.tbl_Product.Upping,
                        JobSize = (p.tbl_Product.HSheetSize.ToString() + " X " + p.tbl_Product.WSheetSize.ToString()).ToString(),
                        GSM = p.tbl_Product.CardGSM,
                        RawSize = p.tbl_Product.RawSizeSheets,
                        SplitSheets = p.tbl_Product.Split,

                        PrintingMachine = p.tbl_Product.tbl_PrintingMachine.PrintingMachine ?? "-",
                        Cylinder = p.tbl_Product.tbl_Cylinder == null ? "-" : p.tbl_Product.tbl_Cylinder.Cylinder ?? "-",
                        Length = p.tbl_Product.Length,
                        Width = p.tbl_Product.Width,
                        CylinderLength = p.tbl_Product.tbl_Cylinder == null ? 0 : p.tbl_Product.tbl_Cylinder.CylinderLength,
                        AroundUps = p.tbl_Product.AroundUps,
                        NoOfPrintingColor = p.tbl_Product.NoOfPrintingColors,
                        Roll = p.tbl_Product.tbl_Roll == null ? "-" : p.tbl_Product.tbl_Roll.Roll ?? "-",
                        TotalPcInRoll = p.tbl_Product.TotalPcsInRoll,
                        WastagePcInRoll = p.tbl_Product.WastagePcsInRoll,
                        TotalReqRoll = p.tbl_Product.TotalRequiredRolls,
                        FinishingType = p.tbl_Product.tbl_FinishingType == null ? "-" : p.tbl_Product.tbl_FinishingType.FinishingType ?? "-",
                        BlockType = p.tbl_Product.tbl_BlockType == null ? "-" : p.tbl_Product.tbl_BlockType.BlockType ?? "-"





                    }).ToList();

                StringBuilder deliveryData = new StringBuilder();
                var renderDeliveryStatusDD = db.tbl_DeliveryStatus.Where(x => x.IsDeleted != true).Select(x => new { ID = x.ID, Value = x.DeliveryStatus }).ToList();
                //if(renderDeliveryStatusDD.Count>0 && renderDeliveryStatusDD != null)
                //{
                //    foreach(var i in renderDeliveryStatusDD)
                //    {
                //        var PrevDeliveryID = ProductsList.Where(x => x.DeliveryStatusID == i.ID).Select(x => x.DeliveryStatusID).FirstOrDefault() ?? 0;
                //        if (PrevDeliveryID > 0)
                //        {
                //            deliveryData.Append("<option value =" + i.ID + " selected='selected'>" + i.Value + "</option>");
                //        }
                //        else
                //        {
                //            deliveryData.Append("<option value =" + i.ID + ">" + i.Value + "</option>");
                //        }

                //    }
                //}

                StringBuilder storeData = new StringBuilder();
                var renderStoreStatusDD = db.tbl_StoreStatus.Where(x => x.IsDeleted != true).Select(x => new { ID = x.ID, Value = x.StoreStatus }).ToList();
                //if (renderStoreStatusDD.Count > 0 && renderStoreStatusDD != null)
                //{
                //    foreach (var i in renderStoreStatusDD)
                //    {
                //        var PrevStoreID = ProductsList.Where(x => x.StoreStatusID == i.ID).Select(x => x.StoreStatusID).FirstOrDefault() ?? 0;
                //        if (PrevStoreID > 0)
                //        {
                //            storeData.Append("<option value =" + i.ID + " selected='selected'>" + i.Value + "</option>");
                //        }
                //        else
                //        {
                //            storeData.Append("<option value =" + i.ID + ">" + i.Value + "</option>");
                //        }

                //    }
                //}

                StringBuilder orderMachineData = new StringBuilder();
                var renderSlittingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 1).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderPrintingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 2).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderDieCuttingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 3).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderUVMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 4).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderLaminationMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 5).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderFoilMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 6).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderEmbosingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 7).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                //if (renderOrderMachineDD.Count > 0 && renderOrderMachineDD != null)
                //{
                //    foreach (var i in renderOrderMachineDD)
                //    {
                //        var PrevOrderMachineID = ProductsList.Where(x => x.OrderMachineID == i.ID).Select(x => x.OrderMachineID).FirstOrDefault() ?? 0;
                //        if (PrevOrderMachineID > 0)
                //        {
                //            string str = "\"selected\"";
                //            orderMachineData.Append("<option value =" + i.ID + " selected=" + str + ">" + i.Value + "</option>");
                //        }
                //        else
                //        {
                //            orderMachineData.Append("<option value =" + i.ID + ">" + i.Value + "</option>");
                //        }

                //    }
                //}

                //var storeStatusDD = db.tbl_StoreStatus.Where(x=>x.ID == )

                var Banks = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 18).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                int BankId = 0;
                foreach (var i in Banks)
                {
                    if (i.Name.Equals(SaleOrderInv.BankName))
                    {
                        BankId = i.Value;
                    }
                }
                var Bank = db.tbl_SalesOrder.Where(x => x.OrderID == OrderId).Select(p => new { Value = BankId, Name = p.BankName }).FirstOrDefault();
                var PayStatus = 0;

                if (SaleOrderInv.PaymentStatus == "UnPaid")
                {
                    PayStatus = 3;
                }
                else if (SaleOrderInv.PaymentStatus == "Paid")
                {
                    PayStatus = 1;
                }
                if (SaleOrderInv.PaymentStatus == "Partial Paid")
                {
                    PayStatus = 2;
                }



                objectList.Add(new
                {
                    Qry = qry,
                    Qry1 = qry1,
                    Qry2 = qry2,
                    chequeDate = chequeDate,
                    SalePersonAccID = SalePersonAccID,
                    salesDate = salesDate,
                    ExpectedDeliveryDate = Convert.ToDateTime(SaleOrderInv.ExpectedDeliveryDate).ToShortDateString(),
                    FinalAmount = FinalAmount,
                    Discount = Discount,
                    SubAmount = SubAmount,
                    TotalAmount = TotalAmount,
                    CreditDays = CreditDays,
                    Tax = Tax,
                    AmountPaid = AmountPaid,
                    Paytype = Paytype,
                    PayStatus = PayStatus,
                    Cheque = Cheque,
                    Bank = Bank,
                    ProductsList = ProductsList,
                    deliveryDataDD = renderDeliveryStatusDD,
                    storeDataDD = renderStoreStatusDD,

                    slittingMachineDataDD = renderSlittingMDD,
                    printingMachineDataDD = renderPrintingMDD,
                    dieCuttingMachineDataDD = renderDieCuttingMDD,
                    uvMachineDataDD = renderUVMDD,
                    laminationMachineDataDD = renderLaminationMDD,
                    foilMachineDataDD = renderFoilMDD,
                    embosingMachineDataDD = renderEmbosingMDD,
                    userRole = currentUserRole

                });
            }
            return Json(objectList, JsonRequestBehavior.AllowGet);
        }

        //Edit Product Detail
        public JsonResult GetEditProductDetail(int OrderId, int ProductID, int? BranchID)
        {
            List<object> objectList1 = new List<object>();

            var Q = db.tbl_SaleDetails.Where(x => x.OrderID == OrderId && x.ProductID == ProductID).Select(p => new { ProductID = p.ProductID, CatID = p.tbl_Product.VehicleCodeID, PartNo = p.PartNo, Desc = p.tbl_Product.Description, Qty = p.Qty, Packet = p.Packet, UnitPerCarton = p.UnitPerCarton, LevelID = p.LevelID, IsPack = p.IsPack, UnitCode = p.UnitCode, UnitID = p.UnitID, MinorDivisor = p.MinorDivisor, SalePrice = p.SalePrice, PTotal = p.Total }).FirstOrDefault();

            objectList1.Add(new
            {
                ProductsList = Q
            });
            return Json(objectList1, JsonRequestBehavior.AllowGet);
        }

        //Save Edit Order
        public JsonResult SaveEditOrder(int OrderId, Models.DTO.Sales model1, List<Models.DTO.StockLog> modelStockLog1, bool? isQuote, int? QuoteOrderID, int? bankAccId)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            POSOman.Models.BLL.SalesOrder sales = new Models.BLL.SalesOrder();
            branchId = model1.BranchID;
            // var orderID = sales.EditSales(model1, modelStockLog1, isQuote, QuoteOrderID, bankAccId, branchId, OrderId,userID);
            return Json(0);
            //else {
            //    var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
            //}
        }
        // [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult GetEditSaleInvoice(int? id)
        {
            ViewBag.OrdId = id;

            ViewBag.BranchId = db.tbl_Branch.Select(v => new { Value = v.BranchID, Name = v.BranchName }).ToList(); //branchId;

            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.Artno = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.Desc = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            //  ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();

            if (User.IsInRole("SuperAdmin"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            if (User.IsInRole("Satin Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            if (User.IsInRole("Sticker Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            if (User.IsInRole("Offset Manager"))
            {
                ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            }
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            return View("SaleEdit");
        }

        // [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult SODelete(int? id)
        {
            ModelState.AddModelError(string.Empty, "Deleting this record results in deleting all entries and related actions! Proceed to delete?");
            tbl_SalesOrder tbl_SalesOrder = db.tbl_SalesOrder.Find(id);
            return View(tbl_SalesOrder);
            // return RedirectToAction("Index");
        }
        [HttpPost, ActionName("SODelete")]
        [ValidateAntiForgeryToken]
        public ActionResult SODeleteConfirmed(int? id, int i = 0)
        {
            try
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                DeleteEntry DeleteSE = new DeleteEntry();

                bool Check = DeleteSE.DeleteSO(id, Convert.ToInt32(Session["LoginUserID"]), userID);
                if (Check == true)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                    return RedirectToAction("Index");
                }

            }

            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return RedirectToAction("Index");
            }
        }
        // check PONO duplication for selected user 
        public JsonResult checkPONO(int accountID, string PONO)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var row = db.tbl_SalesOrder.Where(s => s.PONo == PONO && s.AccountID == accountID && s.BranchID == branchId).FirstOrDefault();
            if (row != null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            { return Json(false, JsonRequestBehavior.AllowGet); }

        }
        // GEt Last Inserted SOID
        public JsonResult getNewSOID()
        {
            int SOID = 0;
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var tmp = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.isOpening != true && so.IsDeleted != true).OrderByDescending(v => v.OrderID).FirstOrDefault();
            if (tmp != null)
            {
                SOID = tmp.SOID + 1;
            }
            else
            {
                SOID = 10001;
            }

            return Json(SOID, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        private static Byte[] BitmapToBytesCode(Bitmap image)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }
        // GET: SalesOrder/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //List<tbl_SaleDetails> details = db.tbl_SaleDetails.Where(p => p.OrderID == id).ToList();
            List<GetSalesByOrderID_Result> details = db.GetSalesByOrderID(id).ToList();
            details = details.Where(so => so.Qty > 0).ToList();
            var SaleOrder = db.tbl_SalesOrder.Where(x => x.OrderID == id).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(SaleOrder.Barcode))
            {
                RenderBarcode(SaleOrder.Barcode.ToString(), SaleOrder.QRCode);
                ViewBag.BarCodeImg = Session["BCImage"];
                ViewBag.BarCodeID = SaleOrder.Barcode;
                //ViewBag.QRCode = SaleOrder.QRCode.ToString();//Session["QRCode"];
            }
            else
            {
                ViewBag.BarCodeImg = null;
            }
            if (details == null)
            {
                return HttpNotFound();
            }
            else
            {//return PartialView("_SalesInvoice",details);



                string isNegative = "";
                try
                {

                    string number = (details.First().TotalAmount - details.First().DiscountAmount).ToString();

                    number = Convert.ToDouble(number).ToString();

                    if (number.Contains("-"))
                    {
                        isNegative = "Minus ";
                        number = number.Substring(1, number.Length - 1);
                    }
                    if (number == "0")
                    {
                        ViewBag.AmInWords = "Zero Rupees Only";

                    }
                    else
                    {
                        AmountInWordsBLL amInWord = new AmountInWordsBLL();
                        ViewBag.AmInWords = isNegative + amInWord.ConvertToWords(number);
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.AmInWords = "NIL";


                }
                return PartialView("_SaleInvoiceTextileNew", details);
            }



        }
        // Get Last Sale Price To selected Customer  else Get Last Sold Price 
        public JsonResult getLastSalePrice(int ProductID, int AccountID)
        {
            decimal dSalePrice = 0;
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            if (ProductID > 0 && AccountID > 0)
            {
                var qry = db.tbl_SaleDetails.Where(p => p.ProductID == ProductID && p.tbl_SalesOrder.AccountID == AccountID && p.BranchID == branchId).OrderByDescending(v => v.DetailID).FirstOrDefault();
                if (qry != null)
                {
                    dSalePrice = (decimal)(qry.SalePrice);
                }
                else if (qry == null)
                {
                    var poPrice = db.tbl_PODetails.Where(p => p.ProductID == ProductID).OrderByDescending(v => v.DetailID).FirstOrDefault();
                    if (poPrice != null)
                    {
                        if (poPrice.SalePrice != null)
                        {
                            dSalePrice = (decimal)(poPrice.SalePrice);
                        }
                        else
                        { dSalePrice = 0; }
                    }
                }
            }

            else if (ProductID > 0 && AccountID == 0)
            {
                var qry1 = db.tbl_SaleDetails.Where(p => p.ProductID == ProductID && p.BranchID == branchId).OrderByDescending(v => v.DetailID).FirstOrDefault();
                if (qry1 != null)
                {
                    dSalePrice = (decimal)(qry1.SalePrice);
                }
                else
                {
                    var qry2 = db.tbl_Stock.Where(p => p.ProductID == ProductID && p.BranchID == branchId).FirstOrDefault();
                    if (qry2 != null)
                    {
                        if (qry2.SalePrice != null)
                        {
                            dSalePrice = (decimal)(qry2.SalePrice);
                        }
                    }
                }
            }
            return Json(dSalePrice, JsonRequestBehavior.AllowGet);
        }

        // GET: SalesOrder/Create
        // [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult Create()
        {
            int branchId = getBranchID();
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Product = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.Desc = db.tbl_Product.Where(x => x.VehicleCodeID != 1).Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();
            ViewBag.Vat = db.tbl_TAX.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AmountInPercentage, Name = c.Tax + "|" + c.AmountInPercentage.ToString() + "%" }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Where(x => x.VehicleCodeID != 1).Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.PaymentTypeID = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();

            return View();
        }

        // GET: SalesOrder/POS
        [Authorize]
        public ActionResult POS()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BranchID = branchId;

            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();

            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            return View("POS1");
        }
        // Create New Sales 

        // Hold Sale Order
        public JsonResult HoldOrder(Models.DTO.Quotation model)
        {
            int branchId = getBranchID();
            POSOman.Models.BLL.SalesOrder sales = new Models.BLL.SalesOrder();
            //sales.HoldSales(model,false,branchId);

            return Json("success");
        }

        // Print Order 
        public JsonResult HoldPrintOrder(Models.DTO.Quotation model)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            POSOman.Models.BLL.SalesOrder sales = new Models.BLL.SalesOrder();
            //var orderID = sales.HoldSales(model,true,branchId);            
            return Json(0);
            //else {
            //    var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
            //}
        }
        public JsonResult SaveOrder(Models.DTO.Sales model, List<Models.DTO.StockLog> modelStockLog, bool? isQuote, int? QuoteOrderID, int? bankAccId, int? OrderTypeID)
        {

            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null && Session["UserID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            model.AddBy = userID;
            branchId = model.BranchID;
            POSOman.Models.BLL.SalesOrder sales = new Models.BLL.SalesOrder();
            //var orderID = sales.createSales(model, modelStockLog,isQuote, QuoteOrderID,bankAccId,branchId,OrderTypeID);
            //if (orderID != null && model.AccountID != 33)
            //{

            //}
            //UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Sales".ToString());

            return Json(0);
        }

        public string SendSMS(string number, string message)
        {
            try
            {
                String result;
                string url = "http://119.160.92.2:7700/sendsms_url.html?Username=03043953892&Password=123.123&From=Clover&To=" + number + "&Message=" + message + "";
                StreamWriter myWriter = null;
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);
                objRequest.Method = "POST";
                objRequest.ContentLength = Encoding.UTF8.GetByteCount(url);
                objRequest.ContentType = "application/x-www-form-urlencoded";
                try
                {
                    myWriter = new StreamWriter(objRequest.GetRequestStream());
                    myWriter.Write(url);
                }
                catch (Exception e)
                {
                    return (e.Message);
                }
                finally
                {
                    myWriter.Close();
                }
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    // Close and clean up the StreamReader
                    sr.Close();
                }
                return (result);
            }
            catch (Exception d)
            {
                return (d.Message);
            }
        }
        // Dont Save Direct Show Print View 
        public ActionResult printOrder(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            // List<tbl_SaleDetails> details = db.tbl_SaleDetails.Where(p => p.OrderID == id).ToList();
            List<GetSalesByOrderID_Result> details = db.GetSalesByOrderID(id).ToList();
            var SaleOrder = db.tbl_SalesOrder.Where(x => x.OrderID == id).FirstOrDefault();
            details = details.Where(so => so.Qty > 0).ToList();
            if (!string.IsNullOrWhiteSpace(SaleOrder.Barcode))
            {
                RenderBarcode(SaleOrder.Barcode.ToString(), SaleOrder.QRCode);
                ViewBag.BarCodeImg = Session["BCImage"];
                ViewBag.BarCodeID = SaleOrder.Barcode;
                //ViewBag.QRCode = SaleOrder.QRCode.ToString();//Session["QRCode"];
            }
            else
            {
                ViewBag.BarCodeImg = null;
            }
            if (details == null)
            {
                return HttpNotFound();
            }
            //return PartialView("_printOrder", details);
            return PartialView("_POSInvoice", details);
        }
        // GET: SalesOrder/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_SalesOrder tbl_SalesOrder = db.tbl_SalesOrder.Find(id);
            if (tbl_SalesOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccountID = new SelectList(db.tbl_AccountDetails, "AccountID", "AccountName", tbl_SalesOrder.AccountID);
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "Code", tbl_SalesOrder.BranchID);
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name", tbl_SalesOrder.PaymentTypeID);
            return View(tbl_SalesOrder);
        }

        // POST: SalesOrder/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderID,BranchID,AccountID,CustomerCode,InvoiceNo,SalesDate,PaymentStatus,PaymentTypeID,SalesCode,Currency,TotalAmount,FinalAmount,AmountPaid,DiscountPercent,DiscountAmount,AddOn,AddBy,UpdateOn,UpdateBy,IsReturned,IsDeleted,IsPaid,Status")] tbl_SalesOrder tbl_SalesOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_SalesOrder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AccountID = new SelectList(db.tbl_AccountDetails, "AccountID", "AccountName", tbl_SalesOrder.AccountID);
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "Code", tbl_SalesOrder.BranchID);
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name", tbl_SalesOrder.PaymentTypeID);
            return View(tbl_SalesOrder);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
