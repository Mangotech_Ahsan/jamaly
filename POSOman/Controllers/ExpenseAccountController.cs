﻿using AutoMapper;
using POSOman.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Net;
using System.Data.Entity;
using System.Web.Configuration;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class ExpenseAccountController : Controller
    {
        // GET: ExpenseAccount
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
       
        private int GetBranchID()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }


        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();


            int branchId = GetBranchID();
            var bankAccounts = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 3 && acd.AccountID > 33 ).ToList();
            return View(bankAccounts);
        }

        // GET: ExpenseAccount/Create
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Create()
        {
            return View();
        }
        // POST: ExpenseAccount/Create
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.DTO.BankAccount account)
        {
            int branchId = GetBranchID();
            account.BranchID = branchId;
            account.AccountTypeID = 3; // Expense Accounts 
            account.Addby = 1;
            account.AddOn = DateTime.UtcNow.AddHours(5);
            account.IsDeleted = false;
            account.IsActive = true;
            account.UserID = HttpContext.User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                if (db.tbl_Expense.Any(acd => acd.Name == account.AccountName))
                {
                    ModelState.AddModelError("AccountName", "Expense Account Already Exists!");
                    return View("Create");
                }

                else
                {
                    tbl_Expense exp = new tbl_Expense();
                    exp.Name = account.AccountName;

                    int code = 0;
                    int accCode = db.tbl_Expense.OrderByDescending(a => a.ExpenseID).Select(a => a.Code).FirstOrDefault();
                    if (accCode > 0)
                    {
                        code = accCode + 1;
                    }
                    else
                    {
                        code = 1;
                    }

                    exp.Code = code;
                    db.tbl_Expense.Add(exp);
                    if(db.SaveChanges() > 0)
                    {
                        account.AccountCode = "Exp-" + code;

                        Mapper.CreateMap<Models.DTO.BankAccount, tbl_AccountDetails>();
                        var accountModel = Mapper.Map<Models.DTO.BankAccount, tbl_AccountDetails>(account);
                        db.tbl_AccountDetails.Add(accountModel);
                        db.SaveChanges();
                        UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added Expanse Account".ToString());
                    }

                    return RedirectToAction("Index");
                }
            }

            return View(account);
        }

        // GET: ExpenseAccount/Edit/5
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_AccountDetails tbl_AccountDetails = db.tbl_AccountDetails.Find(id);
            if (tbl_AccountDetails == null)
            {
                return HttpNotFound();
            }

            return View(tbl_AccountDetails);
        }

        // POST: ExpenseAccount/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_AccountDetails accountModel)
        {
            accountModel.UpdateOn = DateTime.UtcNow.AddHours(5);
            if (ModelState.IsValid)
            {

                if (db.tbl_AccountDetails.Any(v => v.AccountName == accountModel.AccountName && v.AccountID != accountModel.AccountID && v.AccountCode == accountModel.AccountCode))
                {
                    ModelState.AddModelError("AccountName", "Account Already Exists!");
                    return View("Edit");
                }
                else
                {
                    db.Entry(accountModel).State = EntityState.Modified;
                    db.Entry(accountModel).Property(v => v.AccountTypeID).IsModified = false;
                    db.Entry(accountModel).Property(v => v.AddOn).IsModified = false;
                    db.Entry(accountModel).Property(v => v.Addby).IsModified = false;
                    db.Entry(accountModel).Property(v => v.UserID).IsModified = false;
                    db.Entry(accountModel).Property(v => v.BranchID).IsModified = false;
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edited Expanse Accounts".ToString());

                    return RedirectToAction("Index");
                }
            }

            return View(accountModel);
        }

        // GET: ExpenseAccount/Delete/5
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_AccountDetails tbl_AccountDetails = db.tbl_AccountDetails.Where(v => v.AccountID == id).FirstOrDefault();
            if (tbl_AccountDetails == null)
            {
                return HttpNotFound();
            }
            return View(tbl_AccountDetails);
        }

        // POST: ExpenseAccount/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                bool isExist = db.tbl_JDetail.Any(p => p.AccountID == id);
                if (isExist)
                {
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                    tbl_AccountDetails tbl_AccountDetails = db.tbl_AccountDetails.Where(v => v.AccountID == id).FirstOrDefault();
                    if (tbl_AccountDetails == null)
                    {
                        return HttpNotFound();
                    }
                    return View(tbl_AccountDetails);
                }
                else
                {
                    tbl_AccountDetails tbl_AccountDetails = db.tbl_AccountDetails.Find(id);
                    db.tbl_AccountDetails.Remove(tbl_AccountDetails);
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Expanse Acoounts".ToString());

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}