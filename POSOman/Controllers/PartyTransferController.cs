﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.BLL;
using POSOman.Models.DTO;

namespace POSOman.Controllers
{
    public class PartyTransferController : Controller
    {
        PartyTransferEntry transferEntry = new PartyTransferEntry();
        Models.DTO.VendorPayment vpays = new Models.DTO.VendorPayment();
        private dbPOS db = new dbPOS();

        private int GetBranchID()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        // GET: PartyTransfer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PartyToPartyTransfer()
        {
            try
            {
                int branchId = 0;

                ViewBag.BranchId = GetBranchID();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.FromBankAccount = db.tbl_Customer.Select(p => new { Value = p.AccountID, Name = p.Name + " | " + p.Code }).ToList();
                ViewBag.ToBankAccount = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode + "|" + v.Name }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }


        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public JsonResult SaveTransferEntry(PartyToPartyTransferDTO model, int? bankAccId)
        {
            int branchId = GetBranchID();
            if (db.tbl_JDetail.Any(x => x.UserReferenceID == model.UserReferenceID && x.EntryTypeID == 29))
            {
                return Json("refError");
            }
            else
            {
                object Result = transferEntry.Save(model, bankAccId, branchId);
                return Json(Result);
            }
        }
    }
}