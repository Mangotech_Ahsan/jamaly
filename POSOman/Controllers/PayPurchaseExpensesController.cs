﻿using POSOman.Models;
using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class PayPurchaseExpensesController : Controller
    {
        ExpensesEntry exp = new ExpensesEntry();
        UserActionsPerformed UserActions = new UserActionsPerformed();
       
        dbPOS db = new dbPOS();
        // GET: Expenses
        public ActionResult Index()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var customerVoucher = db.tbl_JDetail.Where(j => j.AccountID == 7 && j.Dr > 0 && j.BranchID == branchId);
            return View(customerVoucher.ToList());
        }
        // Get to Create Page
        public ActionResult Create()
        {
            try
            {
                int branchId = 0;
                
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                ViewBag.BranchId = branchId;
                decimal amount = 0;
                if(db.tbl_JDetail.Where(j => j.AccountID == 7).Sum(j => j.Cr - j.Dr) > 0)
                {  amount =  db.tbl_JDetail.Where(j => j.AccountID == 7).Sum(j => j.Cr - j.Dr);
                     }
                ViewBag.TotalPayable = amount;
                ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }
        // Save Expense Entry to Db
        public JsonResult PayPurchaseExpense(Expenses model, int? bankAccId)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                object Result = exp.PayPurchaseExpenses(model, bankAccId,branchId);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Purchase Expanse Paying Action".ToString());

                return Json(Result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Error Performed Purchase Expanse Paying Action".ToString());

                return Json("formError");
            }
        }
        // open Expense receipt
        public ActionResult Details(bool? isNew, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (isNew == true)
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.OrderByDescending(v => v.JDetailID).Where(j => j.AccountID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_ExpenseVoucher", _PaymentDetail);
            }
            else
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JDetailID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_ExpenseVoucher", _PaymentDetail);
            }
        }
        [Authorize(Roles = "SuperAdmin")]
        // GET: Jentry/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            tbl_JEntry tbl_Jentry = db.tbl_JEntry.Where(j => j.JEntryId == id && j.BranchID == branchId).FirstOrDefault();
            if (tbl_Jentry == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Jentry);
        }
        [Authorize(Roles = "SuperAdmin")]
        // POST: Entry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int branchId = 0;
                
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                db.DeleteJournalEntry(id);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Purchase Payings".ToString());

                return RedirectToAction("Index");
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
        }
    }
}