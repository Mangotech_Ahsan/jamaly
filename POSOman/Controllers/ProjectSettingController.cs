﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.BLL;
using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class ProjectSettingController : Controller
    {
        dbPOS db = new dbPOS();

        // GET: ProjectSetting
        public ActionResult Index()
        {
            var data = db.tbl_AddressDetail.FirstOrDefault();
            if(data == null)
            {
                data = DefaultData();
            }

            return View(data);
        }

        [HttpPost]
        public ActionResult SaveAddressDetails(tbl_AddressDetail adddetail, HttpPostedFileBase file)
        {
            var data = db.tbl_AddressDetail.FirstOrDefault();
            adddetail.Logo = data.Logo;
            if (file != null)
            {
                if (file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var extension = Path.GetExtension(fileName);
                    string imgName = "/Content/images/logo/Logo" + extension;
                    //Console.WriteLine("adsa");
                    //var path = Path.Combine(Server.MapPath("/Content/images/logo"), imgName);
                    file.SaveAs(Server.MapPath(imgName));
                    adddetail.Logo = imgName;
                }
            }


            Mapper.CreateMap<Models.tbl_AddressDetail, tbl_AddressDetail>();
            var list = Mapper.Map<tbl_AddressDetail, tbl_AddressDetail>(adddetail, data);
            db.Entry(list).State = System.Data.Entity.EntityState.Modified;

            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        private tbl_AddressDetail DefaultData()
        {
            tbl_AddressDetail adddata = new tbl_AddressDetail();
            adddata.Title = "MangoTech";
            adddata.FullAddress = "Mangotech Noor Trade Centre Karachi";
            adddata.Contact1 = "0344-3607167";
            adddata.IsActive = true;
            adddata.City = "Karachi";
            adddata.Logo = "32x27.png";
            adddata.DevelopBy = "Mangotech Solutions";

            db.tbl_AddressDetail.Add(adddata);
            db.SaveChanges();

            return adddata;
        }
    }
}