﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Configuration;
using POSOman.Models.BLL;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using POSOman.Models.DTO;

namespace POSOman.Controllers{
   
    public class EmployeeController : Controller
    {
        private dbPOS db = new dbPOS();
        Models.DTO.EmployeePayment epay = new Models.DTO.EmployeePayment();
        ApplicationDbContext context;

        #region Employee Settlements
        [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult EmployeeSettlement()
        {
            return View(db.tbl_EmployeeSettlement.ToList());
        }


        public int GetBranchID()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult SettleDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var list = db.tbl_EmployeeSettlementDetails.Where(x=>x.EmpSettleID == id);
            if (list == null)
            {
                return HttpNotFound();
            }
            return View(list);
        }
        [Authorize(Roles = "SuperAdmin,Admin")]

        public ActionResult CreateSettlement()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 && acd.BranchID == branchId).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.BranchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            ViewBag.employee = db.tbl_Employee.Select(c => new { Value = c.EmployeeID, Name = c.Name }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();            
            return View();
        }
        [Authorize(Roles = "SuperAdmin,Admin")]

        public JsonResult GenerateSettlement(POSOman.Models.DTO.GenerateSalary model, List<Models.DTO.EmployeeLog> modelEmployeeLog, int? bankAccId)
        {
            try
            {
                POSOman.Models.BLL.GenerateSalary genSalary = new Models.BLL.GenerateSalary();
                object result = genSalary.SaveSettlement(model, modelEmployeeLog, bankAccId);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }

        #endregion


        #region generate individual salary scenario
        [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult GenerateIndividualSalary()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 && acd.BranchID == branchId).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.BranchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            ViewBag.employee = db.tbl_Employee.Select(c => new { Value = c.EmployeeID, Name = c.Name }).ToList();
            ViewBag.departments = db.tbl_Department.Where(x=>x.IsDeleted!=true ).Select(c => new { Value = c.ID, Name = c.Department }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();            
            return View();
        }
        [Authorize(Roles = "SuperAdmin,Admin")]

        public JsonResult SaveIndividualSalary(POSOman.Models.DTO.GenerateSalary model, List<Models.DTO.EmployeeLog> modelEmployeeLog, int? bankAccId)
        {
            try
            {
                POSOman.Models.BLL.GenerateSalary genSalary = new Models.BLL.GenerateSalary();
                object result = genSalary.SaveSalary(model, modelEmployeeLog, bankAccId);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }

        #endregion


        public string UserRole()
        {

            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;
                string role = "";
                context = new ApplicationDbContext();
                if (User.Identity.IsAuthenticated)
                {                    
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    var s = UserManager.GetRoles(user.GetUserId());
                    role = s[0].ToString();
                }
                ViewBag.Role = role;

                return role;
            }
            else
            {
                ViewBag.Name = "Not Logged IN";
                return string.Empty;
            }
        }
        public string gerRole()
        {
            context = new ApplicationDbContext();
            string userRole = "";
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                userRole = s[0].ToString();
            }
            return userRole;
        }
        //public FileResult Download(String p, String d)
        //{
        //    return File(Path.Combine(Server.MapPath("~/EmployeeDocs/Document/"), p), System.Net.Mime.MediaTypeNames.Application.Octet, d);
        //}
        [Authorize(Roles = "SuperAdmin,Admin")]

        // GET: Employee
        public ActionResult Index()
        {

            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();


            if (UserRole() == "SuperAdmin" || UserRole() == "Admin")
            {
                return View(db.tbl_Employee.ToList());
            }
            else
                return RedirectToAction("Login", "Account");
        }

        public ActionResult EmployeeStatement(int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.employees = db.tbl_Employee.Where(x=>x.IsDeleted!=true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            

            return View();
        }

        [HttpPost]
        public JsonResult DataList(DTParameters param, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {
            int TotalCount = 0;
            decimal OpBal = 0;
            decimal ClBal = 0;
            var filtered = this.GetFilteredData(param.Search.Value, param.SortOrder, param.Start, param.Length, out TotalCount, AccountID, fromDate, toDate);

            var dataList = (filtered != null && filtered.Count > 0) ? filtered.Select(i => new StatementDTO()
            {
                Date = i.Date,
                AccName = i.AccName ?? "-",
                Description = i.Description ?? "-",
                Detail = i.Detail ?? "-",
                PayStatus = i.PayStatus ?? "-",
                Aging = i.Aging != null ? i.Aging.ToString() : "",
                Dr = i.Dr,
                Cr = i.Cr,
                Balance = i.Balance,
                OpeningBalance = i.OpeningBalance,
                detailList = i.detailList

            }) : null;
            TotalCount = db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Count();

            //OpBal = db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).FirstOrDefault().OpBalance??0;
            //ClBal = (db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance == "-" || string.IsNullOrWhiteSpace(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance)  ) ? 0 :  Convert.ToDecimal(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance);
            DTResult<StatementDTO> finalresult = new DTResult<StatementDTO>
            {
                draw = param.Draw,
                data = (dataList != null && dataList.Count() > 0) ? dataList.ToList() : new List<StatementDTO>(),
                recordsFiltered = TotalCount,
                recordsTotal = filtered.Count,
                customData = new
                {
                    OpBal = epay.GetEmployeePaymentDateWise(AccountID, fromDate, toDate, null).Select(x => x.OpBalance).FirstOrDefault(),
                    ClBal = (epay.GetEmployeePaymentDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault() == "-" || string.IsNullOrWhiteSpace(epay.GetEmployeePaymentDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault())) ? 0 : Convert.ToDecimal(epay.GetEmployeePaymentDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault()),
                    TotalDr = epay.GetEmployeePaymentDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Debit) <= 0 ? 0 : Convert.ToDecimal(epay.GetEmployeePaymentDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Debit)),
                    TotalCr = epay.GetEmployeePaymentDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Credit) <= 0 ? 0 : Convert.ToDecimal(epay.GetEmployeePaymentDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Credit))
                }

            };

            //ViewBag.OpBal = (dataList != null && dataList.Count() > 0)?dataList.FirstOrDefault().OpeningBalance:"0";
            //ViewBag.ClBal = (dataList != null && dataList.Count() > 0)?dataList.LastOrDefault().Balance:"0";

            return Json(finalresult);

        }

        public List<StatementDTO> GetFilteredData(string search, string sortOrder, int start, int length, out int TotalCount, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {
           
            var result = epay.GetEmployeePaymentDateWise(AccountID, fromDate, toDate, null).Where(p => (search == null || (p.VoucherName != null && p.VoucherName.ToLower().Contains(search.ToLower())
                  || p.Detail != null && p.Detail.Contains(search)
                 
                  ))).Skip(start).Take(length).ToList();
            TotalCount = db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Count();
            List<StatementDTO> dataList = new List<StatementDTO>();
            int counter = 0;
            foreach (var i in result)
            {

                Models.DTO.StatementDTO data = new StatementDTO();

                data.Date = i.VoucherDate != null ? Convert.ToString(string.Format("{0:dd/MM/yyyy}", i.VoucherDate)) : "-";
                data.AccName = i.VoucherName ?? "-";
                // data.Description = i.Description ?? "-";
                data.Detail = i.Detail ?? "-";
                // data.PayStatus = i.PaymentStatus ?? "-";
                // data.Aging = i.Aging != null ? i.Aging.ToString() : "";
                data.Dr = Convert.ToDecimal(i.Debit).ToString("#,##0.00");
                data.Cr = Convert.ToDecimal(i.Credit).ToString("#,##0.00");
                data.Balance = string.IsNullOrWhiteSpace(i.Balance) ? "-" : Convert.ToDecimal(i.Balance).ToString("#,##0.00");
                data.OpeningBalance = i.OpBalance <= 0 ? "-" : Convert.ToDecimal(i.OpBalance).ToString("#,##0.00");
                data.detailList = StatementBLL.GetItemDetailsForStatementJenTryIDWise(i.JEntryId) != null ? StatementBLL.GetItemDetailsForStatementJenTryIDWise(i.JEntryId) : new List<Details>();


                dataList.Add(data);
                counter = counter + 1;
            }
            //Mapper.CreateMap<tbl_Product, Models.DTO.ProductListCategory>();
            //var details = Mapper.Map<ICollection<tbl_Product>, ICollection<Models.DTO.ProductListCategory>>(result);
            return dataList;
        }


        //[Authorize(Roles = "SuperAdmin,Admin")]
        //// GET: Employee/Details/5
        //[Authorize(Roles = "SuperAdmin,Admin")]
        //public ActionResult EDetail(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var  tbl_Employee = db.tbl_EmployeeImages.Where(p=>p.EmployeeID == id).ToList();
        //    if (tbl_Employee == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tbl_Employee);
        //}
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Employee tbl_Employee = db.tbl_Employee.Find(id);
            if (tbl_Employee == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Employee);
        }
        [Authorize(Roles = "SuperAdmin,Admin")]
        // GET: Employee/Create
        public ActionResult Create()
        {
            if (UserRole() == "SuperAdmin"  || UserRole() == "Admin")
            {
                return View();
            }
            else
                return RedirectToAction("Login", "Account");            
        }

        [Authorize(Roles = "SuperAdmin,Admin")]

        // POST: Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_Employee tbl_Employee, IEnumerable<HttpPostedFileBase> files, IEnumerable<HttpPostedFileBase> img)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            
                            if (string.IsNullOrWhiteSpace(tbl_Employee.Name))
                            {
                                ModelState.AddModelError("Name", "Required!");
                                t.Rollback();
                                return View("Create");
                            }
                            
                            else if (tbl_Employee.IDExpiry == null)
                            {
                                ModelState.AddModelError("Name", "Required!");
                                t.Rollback();
                                return View("Create");
                            }
                            else if (tbl_Employee.Salary == null || tbl_Employee.Salary<=0)
                            {
                                ModelState.AddModelError("Salary", "Required!");
                                t.Rollback();
                                return View("Create");
                            }
                            else if ((!string.IsNullOrWhiteSpace(tbl_Employee.IDNo) && tbl_Employee.IDNo.All(char.IsDigit) && tbl_Employee.IDNo.Length != 13) || (!string.IsNullOrWhiteSpace(tbl_Employee.IDNo) && !tbl_Employee.IDNo.All(char.IsDigit)))
                            {
                                ModelState.AddModelError("IDNo", "Invalid CNIC # Provided!");
                                t.Rollback();
                                return View("Create");
                            }
                            else if (string.IsNullOrWhiteSpace(tbl_Employee.IDNo))
                            {
                                ModelState.AddModelError("IDNo", "No CNIC # Provided!");
                                t.Rollback();
                                return View("Create");
                            }
                            else if ((!string.IsNullOrWhiteSpace(tbl_Employee.Phone) && tbl_Employee.Phone.All(char.IsDigit) && tbl_Employee.Phone.Length != 11) || (!string.IsNullOrWhiteSpace(tbl_Employee.Phone) && !tbl_Employee.Phone.All(char.IsDigit)))
                            {
                                ModelState.AddModelError("Phone", "Invalid Phone # Provided!");
                                t.Rollback();
                                return View("Create");
                            }
                            else if (string.IsNullOrWhiteSpace(tbl_Employee.Phone))
                            {
                                ModelState.AddModelError("Phone", "No Phone # Provided!");
                                t.Rollback();
                                return View("Create");
                            }
                            else if (db.tbl_Employee.Any(e => e.Phone == tbl_Employee.Phone ))
                            {
                                ModelState.AddModelError("Phone", "Employee Phone Already Exists!");
                                t.Rollback();
                                return View("Create");
                            }
                            else if (db.tbl_Employee.Any(e => e.IDNo == tbl_Employee.IDNo))
                            {
                                ModelState.AddModelError("IDNo", "Employee CNIC # Already Exists!");
                                t.Rollback();
                                return View("Create");
                            }

                            else
                            {


                                if (tbl_Employee.OpeningBalance == null)
                                {
                                    tbl_Employee.OpeningBalance = 0;
                                }

                                tbl_Employee.Addby = 1;
                                tbl_Employee.AddOn = Helper.PST();
                                tbl_Employee.IsDeleted = false;
                                tbl_Employee.IsActive = true;
                                tbl_Employee.BranchID = GetBranchID();


                                if (Request.Files.Count > 0 && Request.Files["img"].ContentLength > 0)
                                {
                                    HttpPostedFileBase postFile = Request.Files.Get("img");
                                    string fileName = tbl_Employee.IDNo /*+ Path.GetFileName(postFile.FileName)*/+ Path.GetExtension(postFile.FileName);
                                    string filePath = "/Content/Records/Employee/Img/" + fileName;

                                    //if (System.IO.File.Exists(Path.Combine(filePath, fileName)))
                                    //{
                                    //    System.IO.File.Delete(Path.Combine(filePath, fileName));
                                    //}
                                    if (System.IO.File.Exists(Server.MapPath(filePath)))
                                    {
                                        // If file found, delete it    
                                        //System.IO.File.Delete(Path.Combine(filePath, fileName));
                                        System.IO.File.Delete(Server.MapPath(filePath));
                                    }
                                    postFile.SaveAs(Server.MapPath(filePath));
                                    tbl_Employee.Picture = filePath;

                                }

                                if (Request.Files.Count > 0 && Request.Files["files"].ContentLength > 0)
                                {
                                    HttpPostedFileBase postFile = Request.Files.Get("files");
                                    string FileExtension = System.IO.Path.GetExtension(postFile.FileName);

                                    if (!FileExtension.ToLower().Equals(".pdf"))
                                    {
                                        ModelState.AddModelError("Name", "Invalid Extension Detected: "+FileExtension.ToString()+". Only PDF allowed!");
                                        t.Rollback();
                                        return View("Create");
                                    }
                                    string fileName = tbl_Employee.IDNo /*+ Path.GetFileName(postFile.FileName)*/+ Path.GetExtension(postFile.FileName);
                                    string filePath = "/Content/Records/Employee/Doc/" + fileName;

                                    if (System.IO.File.Exists(Server.MapPath(filePath)))
                                    {
                                        // If file found, delete it    
                                        //System.IO.File.Delete(Path.Combine(filePath, fileName));
                                        System.IO.File.Delete(Server.MapPath(filePath));
                                    }
                                    postFile.SaveAs(Server.MapPath(filePath));
                                    tbl_Employee.Document = filePath;

                                }

                                db.tbl_Employee.Add(tbl_Employee);
                                db.SaveChanges();
                                t.Commit();


                                return RedirectToAction("Index");
                            }

                        }
                    }
                    catch(Exception e)
                    {
                        while (e.InnerException != null) { e = e.InnerException; }
                        t.Rollback();
                        return Content(e.Message);
                    }
                }
            }
            catch(Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return Content(e.Message);
            }
           
          
            return View(tbl_Employee);
        }


        [Authorize(Roles = "SuperAdmin,Admin")]

        // Get Employee Detail 
        public JsonResult getEmployeeDetail(int employeeID)
        {
            try
            {
                List<object> objectList = new List<object>();
                var qry = db.tbl_Employee.Where(e => e.EmployeeID == employeeID).Select(e => new {

                    IDno = e.IDNo,
                    IDExpiry = e.IDExpiry,
                    PassportID = e.PassportID,
                    PassportExpiry = e.PassportExpiry,
                    Salary = e.Salary
                }).FirstOrDefault();
                var advance = db.tbl_EmployeeLog.Where(e => e.EmployeeID == employeeID && (e.JentryTypeID == 6 || e.JentryTypeID == 11)).Sum(e => e.Dr - e.Cr).GetValueOrDefault();
                //var result = new { data = qry, advance = advance };
                objectList.Add(new
                {
                    qry = qry,
                    adv = advance,
                });
                return Json(objectList, JsonRequestBehavior.AllowGet);
                //return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }

            // return Json("null data");
        }

        [Authorize(Roles = "SuperAdmin,Admin")]
        // GET: Employee/Edit/5
        public ActionResult Edit(int? id)
        {
            if (UserRole() == "SuperAdmin" || UserRole() == "Admin")
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                tbl_Employee tbl_Employee = db.tbl_Employee.Find(id);
               
                if (tbl_Employee == null)
                {
                    return HttpNotFound();
                }
                return View(tbl_Employee);
            }
            else
                return RedirectToAction("Login", "Account");

        }

        // POST: Employee/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "SuperAdmin,Admin")]

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_Employee tbl_Employee, IEnumerable<HttpPostedFileBase> files, IEnumerable<HttpPostedFileBase> img)
        {
            using(var t = db.Database.BeginTransaction()){
                if (ModelState.IsValid && tbl_Employee!=null && tbl_Employee.EmployeeID>0)
                {

                    if (string.IsNullOrWhiteSpace(tbl_Employee.Name))
                    {
                        ModelState.AddModelError("Name", "Required!");
                        t.Rollback();
                        return View(tbl_Employee);
                    }

                    else if (tbl_Employee.IDExpiry == null)
                    {
                        ModelState.AddModelError("Name", "Required!");
                        t.Rollback();
                        return View(tbl_Employee);
                    }
                    else if (tbl_Employee.Salary == null || tbl_Employee.Salary <= 0)
                    {
                        ModelState.AddModelError("Salary", "Required!");
                        t.Rollback();
                        return View(tbl_Employee);
                    }
                    else if ((!string.IsNullOrWhiteSpace(tbl_Employee.IDNo) && tbl_Employee.IDNo.All(char.IsDigit) && tbl_Employee.IDNo.Length != 13) || (!string.IsNullOrWhiteSpace(tbl_Employee.IDNo) && !tbl_Employee.IDNo.All(char.IsDigit)))
                    {
                        ModelState.AddModelError("IDNo", "Invalid CNIC # Provided!");
                        t.Rollback();
                        return View(tbl_Employee);
                    }
                    else if (string.IsNullOrWhiteSpace(tbl_Employee.IDNo))
                    {
                        ModelState.AddModelError("IDNo", "No CNIC # Provided!");
                        t.Rollback();
                        return View(tbl_Employee);
                    }
                    else if ((!string.IsNullOrWhiteSpace(tbl_Employee.Phone) && tbl_Employee.Phone.All(char.IsDigit) && tbl_Employee.Phone.Length != 11) || (!string.IsNullOrWhiteSpace(tbl_Employee.Phone) && !tbl_Employee.Phone.All(char.IsDigit)))
                    {
                        ModelState.AddModelError("Phone", "Invalid Phone # Provided!");
                        t.Rollback();
                        return View(tbl_Employee);
                    }
                    else if (string.IsNullOrWhiteSpace(tbl_Employee.Phone))
                    {
                        ModelState.AddModelError("Phone", "No Phone # Provided!");
                        t.Rollback();
                        return View(tbl_Employee);
                    }
                    else if (db.tbl_Employee.Any(e => e.Phone == tbl_Employee.Phone && e.EmployeeID != tbl_Employee.EmployeeID))
                    {
                        ModelState.AddModelError("Phone", "Employee Phone Already Exists!");
                        t.Rollback();
                        return View(tbl_Employee);
                    }
                    else if (db.tbl_Employee.Any(e => e.IDNo == tbl_Employee.IDNo && e.EmployeeID!=tbl_Employee.EmployeeID))
                    {
                        ModelState.AddModelError("IDNo", "Employee CNIC # Already Exists!");
                        t.Rollback();
                        return View(tbl_Employee);
                    }

                    db.Entry(tbl_Employee).State = EntityState.Modified;

                    if (Request.Files.Count > 0 && Request.Files["img"].ContentLength > 0)
                    {
                        HttpPostedFileBase postFile = Request.Files.Get("img");
                        string fileName = tbl_Employee.IDNo /*+ Path.GetFileName(postFile.FileName)*/+ Path.GetExtension(postFile.FileName);
                        string filePath = "/Content/Records/Employee/Img/" + fileName;

                        //if (System.IO.File.Exists(Path.Combine(filePath, fileName)))
                        //{
                        //    System.IO.File.Delete(Path.Combine(filePath, fileName));
                        //}
                        if (System.IO.File.Exists(Server.MapPath(filePath)))
                        {
                            // If file found, delete it    
                            //System.IO.File.Delete(Path.Combine(filePath, fileName));
                            System.IO.File.Delete(Server.MapPath(filePath));
                        }
                        postFile.SaveAs(Server.MapPath(filePath));
                        tbl_Employee.Picture = filePath;

                    }
                    else
                    {
                        db.Entry(tbl_Employee).Property(e => e.Picture).IsModified = false;
                    }

                    if (Request.Files.Count > 0 && Request.Files["files"].ContentLength > 0)
                    {
                        HttpPostedFileBase postFile = Request.Files.Get("files");
                        string FileExtension = System.IO.Path.GetExtension(postFile.FileName);

                        if (!FileExtension.ToLower().Equals(".pdf"))
                        {
                            ModelState.AddModelError("Name", "Invalid Extension Detected: " + FileExtension.ToString() + ". Only PDF allowed!");
                            t.Rollback();
                            return View("Create");
                        }
                        string fileName = tbl_Employee.IDNo /*+ Path.GetFileName(postFile.FileName)*/+ Path.GetExtension(postFile.FileName);
                        string filePath = "/Content/Records/Employee/Doc/" + fileName;

                        if (System.IO.File.Exists(Server.MapPath(filePath)))
                        {
                            // If file found, delete it    
                            //System.IO.File.Delete(Path.Combine(filePath, fileName));
                            System.IO.File.Delete(Server.MapPath(filePath));
                        }
                        postFile.SaveAs(Server.MapPath(filePath));
                        tbl_Employee.Document = filePath;

                    }

                    else 
                    { 
                        db.Entry(tbl_Employee).Property(e => e.Document).IsModified = false; 
                    }

                    if (tbl_Employee.EmployeeID > 0)
                    {

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("Name", "Something went wrong! Please Try Again!");
                    }

                    db.Entry(tbl_Employee).Property(e => e.Addby).IsModified = false;
                    db.Entry(tbl_Employee).Property(e => e.AddOn).IsModified = false;
                    db.SaveChanges();
                    t.Commit();
                    return RedirectToAction("Index");
                }
            }
            
            return View(tbl_Employee);
        }

        [Authorize(Roles = "SuperAdmin,Admin")]
        // GET: Employee/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Employee tbl_Employee = db.tbl_Employee.Find(id);
            if (tbl_Employee == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Employee);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Employee tbl_Employee = db.tbl_Employee.Find(id);
            db.tbl_Employee.Remove(tbl_Employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
