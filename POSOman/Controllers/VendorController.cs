﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using POSOman.Models;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;
using System.Threading.Tasks;

namespace POSOman.Controllers
{
    public class VendorController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        
        // GET: Vendor
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();


            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            var tbl_Vendor = db.tbl_Vendor;
            
            return View(tbl_Vendor.ToList());
        }


        // Get Last Vendor code and return it to js and increment in it +1 
        public JsonResult getLastCode()
        {
            string tempID = "";
            
            var tmp = db.tbl_Vendor.OrderByDescending(v => v.VendorCode).FirstOrDefault();
            if (tmp != null)
            {
                tempID = tmp.VendorCode;
                if (tempID == null)
                {
                    tempID = "V-100";
                }
            }
            else
            {
                tempID = "V-100";
            }

            return Json(tempID, JsonRequestBehavior.AllowGet);
        }

        //get deatil of vendor in edit invoice
        public JsonResult getDetail(int accountID)
        {
            if (accountID > 1)
            {
                var vendorDetail = db.tbl_Vendor.Where(x => x.AccountID == accountID).Select(x => new { OpenBal = x.OpeningBalance }).FirstOrDefault();
                if (vendorDetail.OpenBal.HasValue)
                {
                    var result = new { Balance = vendorDetail.OpenBal };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("null", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("null", JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Vendor/Details/5
        public ActionResult Details(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Vendor tbl_Vendor = db.tbl_Vendor.Find(id);
            if (tbl_Vendor == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Vendor);
        }
        // Get Vendor Purchases Date Wise 
        public ActionResult VendorPurchases(bool? isSearch,int? AccountID, DateTime? fromDate, DateTime? toDate,int? BranchID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode + "|" + v.Name }).ToList();

            if (isSearch == true)
            {
                List<GetVendorPurchasesDateWise_Result> customer = db.GetVendorPurchasesDateWise(AccountID, fromDate, toDate,BranchID).ToList();
                customer = customer.Where(a => a.isOpening != true).ToList();
                if (customer.Count == 0)
                {
                    return PartialView("_VendorPurchases");
                }
                else
                {
                    return PartialView("_VendorPurchases", customer);
                }
            }

            return View("VendorPurchaseReport");
        }

        // GET: Vendor/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vendor/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_Vendor tbl_Vendor)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            if (tbl_Vendor.OpeningBalance == null)
            {
                tbl_Vendor.OpeningBalance = 0;
            }
            if(branchId == 0)
            {
                branchId = 9001;
            }
            tbl_Vendor.Addby = userID;
            tbl_Vendor.AddOn = DateTime.UtcNow.AddHours(5);
            tbl_Vendor.IsDeleted = false;
            tbl_Vendor.IsActive = true;
            tbl_Vendor.UserID = HttpContext.User.Identity.GetUserId();
            tbl_Vendor.BranchID = branchId;
            if (ModelState.IsValid)
            {
                if (db.tbl_Vendor.Any(v => v.VendorCode == tbl_Vendor.VendorCode))
                {
                    ModelState.AddModelError("VendorCode", "Vendor Already Exists!");
                    return View("Create");
                }
                else if (tbl_Vendor.Cell != null && db.tbl_Vendor.Any(v => v.Cell == tbl_Vendor.Cell))
                {
                    ModelState.AddModelError("Cell", "Vendor Cell Already Exists!");
                    return View("Create");
                }
                else if (tbl_Vendor.Landline != null && db.tbl_Vendor.Any(v => v.Landline == tbl_Vendor.Landline))
                {
                    ModelState.AddModelError("Landline", "Vendor Phone Already Exists!");
                    return View("Create");
                }
                
                else
                {
                    db.tbl_Vendor.Add(tbl_Vendor);
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added Vendors".ToString());

                    return RedirectToAction("Index");
                }
            }

            //ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Vendor.Addby);
            return View(tbl_Vendor);
        }

        // GET: Vendor/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Vendor tbl_Vendor = db.tbl_Vendor.Find(id);
            if (tbl_Vendor == null)
            {
                return HttpNotFound();
            }
            //ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Vendor.Addby);
            return View(tbl_Vendor);
        }

        // POST: Vendor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VendorID,AccountID,VendorCode,Name,Address,ContactPerson,Landline,Cell,Email,OpeningBalance", Exclude = "IsActive,AddOn,Addby,UpdateOn,UpdateBy,DeleteOn,DeleteBy,IsDeleted")] tbl_Vendor tbl_Vendor)
        {
            tbl_Vendor.UpdateOn = DateTime.UtcNow.AddHours(5);
            if (ModelState.IsValid)
            {
                int branchId = 0;
                
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                if (db.tbl_Vendor.Any(v => v.VendorCode == tbl_Vendor.VendorCode && v.VendorID != tbl_Vendor.VendorID))
                {
                    ModelState.AddModelError("VendorCode", "Vendor Already Exists!");
                    return View("Edit");
                }
                else if (tbl_Vendor.Cell != null && db.tbl_Vendor.Any(v => v.Cell == tbl_Vendor.Cell && v.VendorID != tbl_Vendor.VendorID))
                {
                    ModelState.AddModelError("Cell", "Vendor Cell Already Exists!");
                    return View("Edit");
                }
                else if (tbl_Vendor.Landline != null && db.tbl_Vendor.Any(v => v.Landline == tbl_Vendor.Landline && v.VendorID != tbl_Vendor.VendorID))
                {
                    ModelState.AddModelError("Landline", "Vendor Phone Already Exists!");
                    return View("Edit");
                }
                else
                {
                    db.Entry(tbl_Vendor).State = EntityState.Modified;
                    db.Entry(tbl_Vendor).Property(v => v.AddOn).IsModified = false;
                    db.Entry(tbl_Vendor).Property(v => v.AccountID).IsModified = false;
                    db.Entry(tbl_Vendor).Property(v => v.Addby).IsModified = false;
                    db.Entry(tbl_Vendor).Property(v => v.BranchID).IsModified = false;
                    db.Entry(tbl_Vendor).Property(v => v.rowguid).IsModified = false;
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edited Vendors".ToString());

                    return RedirectToAction("Index");
                }
            }
               // ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Vendor.Addby);
                return View(tbl_Vendor);            
        }

        // GET: Vendor/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Vendor tbl_Vendor = db.tbl_Vendor.Where(v => v.AccountID == id).FirstOrDefault();
            if (tbl_Vendor == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Vendor);
        }

        // POST: Vendor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                bool isExist = db.tbl_PurchaseOrder.Any(p => p.AccountID == id && p.isOpening == false);
                bool isPaymentExist = db.tbl_JDetail.Any(p => p.AccountID == id && p.EntryTypeID != 24);
                if (isExist || isPaymentExist)
                {
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                    tbl_Vendor tbl_Vendor = db.tbl_Vendor.Where(v => v.AccountID == id).FirstOrDefault();
                    if (tbl_Vendor == null)
                    {
                        return HttpNotFound();
                    }
                    return View(tbl_Vendor);
                }
                else
                {
                    int VendorID = db.tbl_Vendor.Where(v=>v.AccountID==id).Select(v=> v.VendorID).FirstOrDefault();
                    tbl_Vendor tbl_Vendor = db.tbl_Vendor.Find(VendorID);                    
                    db.tbl_Vendor.Remove(tbl_Vendor);                                        
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Vendors".ToString());

                    return RedirectToAction("Index");
                }
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
