﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.DTO;
using System.Web.Configuration;
using System.Net;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;
using Microsoft.Reporting.WebForms;

namespace POSOman.Controllers
{
    public class SalaryController : Controller
    {
        private dbPOS db = new dbPOS();

        public int getBranchID()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        [Authorize(Roles = "SuperAdmin,Admin")]
        // GET : General Journal Entry of Salary and Advance for deletion
        public ActionResult EmployeeVouchers()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            int branchId = getBranchID();
            var employeeVoucher = db.tbl_JDetail.Where(j => j.BranchID == branchId && j.Dr > 0 && (j.EntryTypeID == 5 || j.EntryTypeID == 6 || j.EntryTypeID == 12));
            return View(employeeVoucher.ToList());
        }

        // GET: Salary
        public ActionResult Index(bool? btn, int? employeeID, DateTime? fromDate, DateTime? toDate, string EmployeeName)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.employee = db.tbl_Employee.Select(c => new { Value = c.EmployeeID, Name = c.Name }).ToList();
            if (btn.HasValue)
            {
                List<GetEmployeeSalaryFilterWise_Result> empLedger = db.GetEmployeeSalaryFilterWise(fromDate,toDate, employeeID, EmployeeName).ToList();
                if (empLedger.Count == 0)
                {
                    return PartialView("_EmployeeLedger");
                }
                else
                    return PartialView("_EmployeeLedger", empLedger);
            }

            return View();
        }

        public FileResult Export(int id, String ReportType)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/SalaryReport.rdlc");


            ReportDataSource rs = new ReportDataSource();
            rs.Name = "DataSet1234";
            rs.Value = db.SalaryReport(id);

            lo.DataSources.Add(rs);


            byte[] renderbyte;

            string deviceInfo = "<DeviceInfo>" +
    "<OutputFormat>PDF</OutputFormat>" +
    "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            Response.AddHeader("Content-Disposition",
             "attachment; filename=Salary.pdf");

            return new FileContentResult(renderedBytes, mimeType);

        }

        // Get  Salary receipt 

        public ActionResult SalaryReceipt(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }            
                var _PaymentDetail = db.tbl_EmployeeLog.Where(j => j.JEntryID == id).ToList();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
            else
            {
                string isNegative = "";
                try
                {
                    var Dr = _PaymentDetail.Sum(x => x.Dr);
                    var Cr = _PaymentDetail.Sum(x => x.Cr);
                    string number = (Convert.ToDecimal(Dr+Cr)).ToString();
                    ViewBag.Tot = number;
                    number = Convert.ToDouble(number).ToString();

                    if (number.Contains("-"))
                    {
                        isNegative = "Minus ";
                        number = number.Substring(1, number.Length - 1);
                    }
                    if (number == "0")
                    {
                        ViewBag.AmInWord = "Zero Rupees Only";

                    }
                    else
                    {
                        AmountInWordsBLL amInWord = new AmountInWordsBLL();
                        ViewBag.AmInWord = isNegative + amInWord.ConvertToWords(number);
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.AmInWord = "NIL";

                }
                return PartialView("_SalaryVoucherNew", _PaymentDetail);

            }
                
            
            
        }
        // Get  Voucher receipt 
        public ActionResult Details(bool? isNew, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }            
            if (isNew == true)
            {
                LocalReport lo = new LocalReport();
                lo.ReportPath = Server.MapPath("~/Models/Reports/AdvanceSalary.rdlc");


                ReportDataSource rs = new ReportDataSource();
                rs.Name = "DataSet1";
                rs.Value = db.AdvanceSalary(id);

                lo.DataSources.Add(rs);


                byte[] renderbyte;

                string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
                Warning[] warnings;
                string[] streams;
                string mimeType;
                byte[] renderedBytes;
                string encoding;
                string fileNameExtension;
                renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


                Response.AddHeader("Content-Disposition", "attachment; filename=LoanInvoice.pdf");

                return new FileContentResult(renderedBytes, mimeType);

            }
            else
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JDetailID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_VoucherPrint", _PaymentDetail);
            }
        }


        [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult Create()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 && acd.BranchID == branchId).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.BranchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            ViewBag.employee = db.tbl_Employee.Select(c => new { Value = c.EmployeeID, Name = c.Name }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();            
            return View();
        }
       [Authorize(Roles = "SuperAdmin,Admin")]
        // Generate Salary 
        public JsonResult GenerateSalary(POSOman.Models.DTO.GenerateSalary model, List<Models.DTO.EmployeeLog> modelEmployeeLog,int? bankAccId)
        {
            try
            {
                POSOman.Models.BLL.GenerateSalary genSalary = new Models.BLL.GenerateSalary();
                object result = genSalary.Save(model, modelEmployeeLog,bankAccId);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }

        [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult CreateAdvance()
        {
            try
            {
                int branchId = getBranchID();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 && acd.BranchID == branchId).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                ViewBag.BranchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
                ViewBag.employee = db.tbl_Employee.Select(c => new { Value = c.EmployeeID, Name = c.Name }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View("EmployeeAdvance");
        }

        //  Advance / Loan Payment to Employee
        public JsonResult SaveAdvance(Expenses model, POSOman.Models.DTO.EmployeeLog employeeLog, int? bankAccId)
        {
            if (ModelState.IsValid)
            {
                POSOman.Models.BLL.GenerateSalary genSalary = new Models.BLL.GenerateSalary();
                object Result = genSalary.SaveAdvance(model, employeeLog,bankAccId);
                return Json(Result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }

       [Authorize(Roles = "SuperAdmin,Admin")]
        // GET: Jentry/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var tbl_Employee = db.tbl_EmployeeLog.Where(emp => emp.JEntryID == id);
            if (tbl_Employee == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Employee.ToList());
        }
       [Authorize(Roles = "SuperAdmin,Admin")]
        // POST: Entry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                db.DeleteSalaryEntry(id);
                return RedirectToAction("EmployeeVouchers");
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
        }
    }
}
