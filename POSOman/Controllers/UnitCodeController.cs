﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class UnitCodeController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: UnitCode
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            return View(db.tbl_UnitCode.Where(x=>x.IsDeleted!=true).ToList());
        }

        // GET: UnitCode/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_UnitCode tbl_UnitCode = db.tbl_UnitCode.Find(id);
            if (tbl_UnitCode == null)
            {
                return HttpNotFound();
            }
            return View(tbl_UnitCode);
        }

        public class UnitCode
        {
            public int Value { get; set; }
            public string Name { get; set; }
        }

        // GET: UnitCode/Create
        public ActionResult Create()
        {
            List<UnitCode> list = new List<UnitCode>{new
           UnitCode {
                Value = 1,
                Name = "Raw Products"
            },
            new UnitCode
            {
                Value = 2,
                Name = "Items"
            } };
            ViewBag.TypeID = list.Select(x => new { Value = x.Value, Name = x.Name }).ToList();
            return View();
        }

        // POST: UnitCode/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_UnitCode tbl_UnitCode)
        {

            if (ModelState.IsValid)
            {
                if (db.tbl_UnitCode.Any(p => p.UnitCode == tbl_UnitCode.UnitCode && p.TypeID == tbl_UnitCode.TypeID && tbl_UnitCode.TypeID != null))
                {
                    ModelState.AddModelError("UnitCode", "Unit Already Exists!");
                    List<UnitCode> list = new List<UnitCode>{
                        new UnitCode {
                Value = 1,
                Name = "Raw Products"
            },
            new UnitCode
            {
                Value = 2,
                Name = "Items"
            } };
                    ViewBag.TypeID = list.Select(x => new { Value = x.Value, Name = x.Name }).ToList();
                    return View("Create");
                }
                else if (string.IsNullOrWhiteSpace(tbl_UnitCode.UnitCode) || tbl_UnitCode.TypeID == null)
                {
                    ModelState.AddModelError("UnitCode", "Required");
                    List<UnitCode> list = new List<UnitCode>{new
           UnitCode {
                Value = 1,
                Name = "Raw Products"
            },
            new UnitCode
            {
                Value = 2,
                Name = "Items"
            } };
                    ViewBag.TypeID = list.Select(x => new { Value = x.Value, Name = x.Name }).ToList();
                    return View("Create");
                }
                else
                {
                    db.tbl_UnitCode.Add(tbl_UnitCode);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(tbl_UnitCode);
        }

        // GET: UnitCode/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_UnitCode tbl_UnitCode = db.tbl_UnitCode.Find(id);
            if (tbl_UnitCode == null)
            {
                return HttpNotFound();
            }
            ViewBag.UnitCodeID = new SelectList(db.tbl_UnitCode, "UnitCodeID", "UnitCode", tbl_UnitCode.UnitCodeID);
            List<UnitCode> list = new List<UnitCode>{new
           UnitCode {
                Value = 1,
                Name = "Raw Products"
            },
            new UnitCode
            {
                Value = 2,
                Name = "Items"
            } };
            ViewBag.TypeID = list.Select(x => new { Value = x.Value, Name = x.Name }).ToList();
            return View(tbl_UnitCode);
        }

        // POST: UnitCode/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_UnitCode tbl_UnitCode)
        {
            if (ModelState.IsValid)
            {
                if (db.tbl_UnitCode.Any(p => p.UnitCode == tbl_UnitCode.UnitCode && p.TypeID == tbl_UnitCode.TypeID && tbl_UnitCode.TypeID != null))
                {
                    ModelState.AddModelError("UnitCode", "Unit Already Exists!");
                    List<UnitCode> list = new List<UnitCode>{
                        new UnitCode {
                Value = 1,
                Name = "Raw Products"
            },
            new UnitCode
            {
                Value = 2,
                Name = "Items"
            } };
                    ViewBag.TypeID = list.Select(x => new { Value = x.Value, Name = x.Name }).ToList();
                    return View(tbl_UnitCode);
                }
                else if (string.IsNullOrWhiteSpace(tbl_UnitCode.UnitCode) )
                {
                    ModelState.AddModelError("UnitCode", "Required");
                    List<UnitCode> list = new List<UnitCode>{new
           UnitCode {
                Value = 1,
                Name = "Raw Products"
            },
            new UnitCode
            {
                Value = 2,
                Name = "Items"
            } };
                    ViewBag.TypeID = list.Select(x => new { Value = x.Value, Name = x.Name }).ToList();
                    return View(tbl_UnitCode);
                }
                else
                {
                    tbl_UnitCode.IsDeleted = false;
                    tbl_UnitCode.UpdateOn = Helper.PST();
                    db.Entry(tbl_UnitCode).State = EntityState.Modified;
                    db.Entry(tbl_UnitCode).Property(p => p.AddOn).IsModified = false;
                    db.Entry(tbl_UnitCode).Property(p => p.TypeID).IsModified = false;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(tbl_UnitCode);
        }

        // GET: UnitCode/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_UnitCode tbl_UnitCode = db.tbl_UnitCode.Find(id);
            if (tbl_UnitCode == null)
            {
                return HttpNotFound();
            }
            return View(tbl_UnitCode);
        }

        // POST: UnitCode/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var tbl_UnitCode = db.tbl_UnitCode.Where(x=>x.UnitCodeID == id).FirstOrDefault();
            if (tbl_UnitCode != null)
            {
                tbl_UnitCode.IsDeleted = true;
                db.Entry(tbl_UnitCode).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return HttpNotFound();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
