﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;
using Microsoft.Reporting.WebForms;

namespace POSOman.Controllers
{
    public class QuotationController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();

        private int getBranchID()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        public FileResult Export(int id, String ReportType)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/Quto.rdlc");


            ReportDataSource rs = new ReportDataSource();
            rs.Name = "DataSet2";
            rs.Value = db.QuotationInvoiceNew(id);

            lo.DataSources.Add(rs);


            byte[] renderbyte;

            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            Response.AddHeader("Content-Disposition", "attachment; filename=UnAssignedLevels.pdf");

            return new FileContentResult(renderedBytes, mimeType);

        }

        // GET: Quotation
        [Authorize(Roles = "SuperAdmin,Admin,BDM")]
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            int branchId = getBranchID();            
            var tbl_Quotation = db.tbl_Quotation.Where(q => q.IsQuote == true && q.IsDeleted != true && q.BranchID == branchId).Include(t => t.tbl_AccountDetails).OrderByDescending(q => q.QuoteID);
       
            return View(tbl_Quotation.ToList());
        }

        // Get Hold Orders
        public ActionResult HoldOrders()
        {
            int branchId = getBranchID();
            var tbl_Quotation = db.tbl_Quotation.Where(q => q.IsHold == true && q.BranchID == branchId).Include(t => t.tbl_AccountDetails).OrderByDescending(q => q.QuoteID).ToList();
            return View(tbl_Quotation);
        }

        // Get POS Hold Orders
        public ActionResult POSHoldOrders()
        {
            int branchId = 0;
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var tbl_Quotation = db.tbl_Quotation.Where(q => q.IsHold == true && q.UserID == currentUserId).Include(t => t.tbl_AccountDetails).OrderByDescending(q => q.QuoteID);
            return View(tbl_Quotation.ToList());
        }
        // GET: Quotation/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //tbl_PurchaseOrder purchaseOrder = db.tbl_PurchaseOrder.Find(id);
            /*List<tbl_QuoteDetails>*/var _QDetails = db.tbl_QuoteDetails.Where(p => p.OrderID == id).ToList();
            if (_QDetails == null)
            {
                return HttpNotFound();
            }
            else
            {
                string isNegative = "";
                try
                {

                    string number = (Convert.ToDecimal(_QDetails.First().tbl_Quotation.TotalAmount) - Convert.ToDecimal(_QDetails.First().tbl_Quotation.DiscountAmount)).ToString();

                    number = Convert.ToDouble(number).ToString();

                    if (number.Contains("-"))
                    {
                        isNegative = "Minus ";
                        number = number.Substring(1, number.Length - 1);
                    }
                    if (number == "0")
                    {
                        ViewBag.AmInWord = "Zero Rupees Only";

                    }
                    else
                    {
                        AmountInWordsBLL amInWord = new AmountInWordsBLL();
                        ViewBag.AmInWord = isNegative + amInWord.ConvertToWords(number);
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.AmInWord = "NIL";


                }

                //return PartialView("_QuotationPrintNew", _QDetails);
                return PartialView("_QuotationNewInvoice", _QDetails);
            }
        }

        public JsonResult getDetail(int vatid)
        {
            if (vatid > 0)
            {
                int? branchId = null;

                var customerDetail = db.tbl_TAX.Where(x => x.AmountInPercentage == vatid     ).Select(p=>p.TaxDescription).FirstOrDefault();
                if (customerDetail !=null)
                {
                   
                    return Json(customerDetail, JsonRequestBehavior.AllowGet);
                }
                else
                { return Json("null", JsonRequestBehavior.AllowGet); }
            }
            else
            { return Json("null", JsonRequestBehavior.AllowGet); }

        }
        // GET: Quotation/Create
        [Authorize(Roles = "SuperAdmin,Admin,BDM")]
        public ActionResult Create()
        {
            // TODO:: 
            //var user = Microsoft.AspNet.Identity.UserManager.FindById(User.Identity.GetUserId());
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BranchID = branchId;
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            //ViewBag.Product = db.tbl_Product.Where(x=>x.VehicleCodeID!=1).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();

            ViewBag.Product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            //ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();

            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Vat = db.tbl_TAX.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AmountInPercentage, Name = c.Tax + "|" + c.AmountInPercentage.ToString() + "%" }).ToList();

            ViewBag.Vatgs = db.tbl_TAX.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AmountInPercentage, Name = c.TaxDescription }).ToList();

            return View();
        }
        // Get Quotation by Qoute ID and Submit 
        public ActionResult getQuotationByID(string QuoteID)
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }            
            var OrderID = db.tbl_Quotation.Where(q => q.QuoteID == QuoteID && q.IsSubmit == false && q.BranchID == branchId).Select(q => q.OrderID).FirstOrDefault();
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Product = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Where(p=>p.VehicleCodeID !=1).Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            if (OrderID > 0)
            {
                ViewBag.OrderID = OrderID;
                return View("SubmitOrders");
            }
            else
                return View("Quotation");
        }
        public JsonResult getQuotationOrderID(string QuoteID)
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var OrderID = db.tbl_Quotation.Where(q => q.QuoteID == QuoteID && q.IsSubmit == false && q.BranchID == branchId).Select(q => q.OrderID).FirstOrDefault();
            if (OrderID > 0)
            {
                return Json(OrderID, JsonRequestBehavior.AllowGet);
            }
            else
            { return Json(false, JsonRequestBehavior.AllowGet); }

        }
        // Submit  Hold, Unsaved and Quotations
        public ActionResult SubmitOrders(int OrderID,bool? isPOS)
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.UserBranchID = branchId;
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Where(x=>x.ID<4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Product = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.Vat = db.tbl_TAX.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AmountInPercentage, Name = c.Tax + "|" + c.AmountInPercentage.ToString() + "%" }).ToList();

            // ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes.Where(x=>x.ID<4), "ID", "Name");
            ViewBag.OrderID = OrderID;
            if (isPOS == true)
            {
                return View("SubmitPOS");
            }
            else
            {
                return View();
            }
        }
        // // Get Last Quotation ID and return it to js and increment in it +1 
        public JsonResult getLastQuotationID()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            string QID = "";
            var tmp = db.tbl_Quotation.Where(q => q.IsQuote == true && q.BranchID == branchId).OrderByDescending(v => v.OrderID).FirstOrDefault();
            if (tmp != null)
            {
                QID = tmp.QuoteID;
                if (QID == null)
                {
                    QID = "Q-1200";
                }
            }
            else
            {
                QID = "Q-1200";
            }

            return Json(QID, JsonRequestBehavior.AllowGet);
        }
        // // Get Last Hold ID and return it to js and increment in it +1 
        public JsonResult getLastHoldID()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            string HID = "";
            var tmp = db.tbl_Quotation.Where(q => q.IsHold == true && q.BranchID == branchId).OrderByDescending(v => v.OrderID).FirstOrDefault();
            if (tmp != null)
            {
                HID = tmp.HoldID;
                if (HID == null)
                {
                    HID = "H-1000";
                }
            }
            else
            {
                HID = "H-1000";
            }

            return Json(HID, JsonRequestBehavior.AllowGet);
        }
        // Add New Quotation 
        public JsonResult SaveQuotation(Models.DTO.Quotation model)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            POSOman.Models.BLL.SalesOrder sales = new Models.BLL.SalesOrder();
            //var orderID = sales.SaveQuotation(model,branchId);
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Quotation Actions".ToString());

            return Json(0);  
                      
        }
        // Get Quotation or Hold Orders for Submission 
        public JsonResult getOrderDetails(int OrderID) // orderID=pk, tOrderId= Holdid or QouteID
        {
            if (OrderID > 0)
            {
                try
                {
                    int branchId = 0;
                    
                    if (Session["BranchID"] != null)
                    {
                        branchId = Convert.ToInt32(Session["BranchID"]);
                    }
                    else
                    {
                        var user = User.Identity;
                        string currentUserId = User.Identity.GetUserId();
                        var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                        branchId = currentUser.BranchID;
                    }
                    
                    var qry = db.tbl_QuoteDetails.Where(p => p.OrderID == OrderID)
                        .Select(p => new
                        {
                            p.tbl_Quotation.QuoteID,
                            p.tbl_Quotation.BranchID,
                            p.OrderID,
                           
                            p.tbl_Quotation.CreditDays,
                            p.tbl_Quotation.VehicleNo,
                            p.tbl_Quotation.PONo,
                            p.ProductID,
                            p.tbl_Product.PartNo,
                            p.tbl_Product.Description,
                            p.tbl_Product.tbl_VehicleCode.VehicleCode,
                            p.tbl_Product.Product_Code,
                            p.tbl_Product.Location,
                            p.tbl_Quotation.AccountID,
                            p.tbl_Quotation.PaymentStatus,
                            p.tbl_Quotation.PaymentTypeID,
                            p.tbl_Quotation.SalesDate,
                            p.tbl_Quotation.Currency,
                            p.tbl_Quotation.AmountPaid,
                            p.tbl_Quotation.TotalAmount,
                            p.tbl_Quotation.VAT,
                            p.tbl_Quotation.InvoiceNo,                            
                            p.Qty,
                            p.UnitPerCarton,
                            p.UnitPrice,
                            p.tbl_Product.UnitCode,
                            p.tbl_Product.OpenUnitCode,
                            p.ExchangeRate,
                            p.SalePrice,
                            p.Total
                        }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }

            }

            return Json("");
        }
        // GET: Quotation/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Quotation tbl_Quotation = db.tbl_Quotation.Find(id);
            if (tbl_Quotation == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccountID = new SelectList(db.tbl_AccountDetails, "AccountID", "AccountName", tbl_Quotation.AccountID);
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "Code", tbl_Quotation.BranchID);
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name", tbl_Quotation.PaymentTypeID);
            return View(tbl_Quotation);
        }

        
        // GET: Quotation/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Quotation tbl_Quotation = db.tbl_Quotation.Find(id);
            if (tbl_Quotation == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Quotation);
        }

        // POST: Quotation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            List<tbl_QuoteDetails> tbl = db.tbl_QuoteDetails.Where(p => p.OrderID == id).ToList();

            foreach (var item in tbl)
            {
                if (item != null)
                {
                    item.IsDeleted = "1" ;
                    db.Entry(item).State = EntityState.Modified;
                }
            }

           tbl_Quotation tbl_Quotation = db.tbl_Quotation.Find(id);
            tbl_Quotation.IsDeleted = true;
            db.Entry(tbl_Quotation).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
