﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.BLL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class ShareHolderController : Controller
    {

        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();

        [Authorize]
        // GET: BankAccount
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Index()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var shareHolders = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 28 && acd.BranchID == branchId).ToList();
            return View(shareHolders);
        }
        [Authorize]
        // GET: Bank Account /Create
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Create()
        {
            return View();
        }
        // POST: bank Account/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.DTO.BankAccount account)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null && Session["UserID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            account.BranchID = branchId;
            account.AccountTypeID = 28; // Share Holders 
            account.Addby = userID;
            account.AddOn = DateTime.UtcNow.AddHours(5);
            account.IsDeleted = false;
            account.IsActive = true;
            account.UserID = HttpContext.User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                if (db.tbl_AccountDetails.Any(acd => acd.AccountName == account.AccountName && acd.Bank == account.Bank))
                {
                    ModelState.AddModelError("AccountName", "Account Already Exists!");
                    return View("Create");
                }

                else
                {
                    Mapper.CreateMap<Models.DTO.BankAccount, tbl_AccountDetails>();
                    var accountModel = Mapper.Map<Models.DTO.BankAccount, tbl_AccountDetails>(account);
                    db.tbl_AccountDetails.Add(accountModel);
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added ShareHolder Account".ToString());

                    return RedirectToAction("Index");
                }
            }

            return View(account);
        }

        // GET: BankAccount/Edit/5
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_AccountDetails tbl_AccountDetails = db.tbl_AccountDetails.Find(id);
            if (tbl_AccountDetails == null)
            {
                return HttpNotFound();
            }

            return View(tbl_AccountDetails);
        }

        // POST: Vendor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_AccountDetails accountModel)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null && Session["UserID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            accountModel.UpdateBy = userID;
            accountModel.UpdateOn = DateTime.UtcNow.AddHours(5);
            if (ModelState.IsValid)
            {

                if (db.tbl_AccountDetails.Any(v => v.AccountName == accountModel.AccountName && v.AccountID != accountModel.AccountID))
                {
                    ModelState.AddModelError("AccountName", "Account Already Exists!");
                    return View("Edit");
                }
                else
                {
                    db.Entry(accountModel).State = EntityState.Modified;
                    db.Entry(accountModel).Property(v => v.AccountTypeID).IsModified = false;
                    db.Entry(accountModel).Property(v => v.AddOn).IsModified = false;
                    db.Entry(accountModel).Property(v => v.Addby).IsModified = false;
                    db.Entry(accountModel).Property(v => v.UserID).IsModified = false;
                    db.Entry(accountModel).Property(v => v.BranchID).IsModified = false;
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edited Bank Accounts".ToString());

                    return RedirectToAction("Index");
                }
            }

            return View(accountModel);
        }

        // GET: BankAccount/Delete/5
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_AccountDetails tbl_AccountDetails = db.tbl_AccountDetails.Where(v => v.AccountID == id).FirstOrDefault();
            if (tbl_AccountDetails == null)
            {
                return HttpNotFound();
            }
            return View(tbl_AccountDetails);
        }

        // POST: BankAccount/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                bool isExist = db.tbl_JDetail.Any(p => p.AccountID == id);
                if (isExist)
                {
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                    tbl_AccountDetails tbl_AccountDetails = db.tbl_AccountDetails.Where(v => v.AccountID == id).FirstOrDefault();
                    if (tbl_AccountDetails == null)
                    {
                        return HttpNotFound();
                    }
                    return View(tbl_AccountDetails);
                }
                else
                {
                    tbl_AccountDetails tbl_AccountDetails = db.tbl_AccountDetails.Find(id);
                    db.tbl_AccountDetails.Remove(tbl_AccountDetails);
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted ShareHolder Account".ToString());

                    return RedirectToAction("Index");
                }
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}