﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using AutoMapper;
using System.Threading.Tasks;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;
using Microsoft.Reporting.WebForms;

namespace POSOman.Controllers
{
    public class CustomerController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();


        private int GetBranchID()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        public ActionResult PaymentsChallanWise(bool? btn, int? AccountID, int? OID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            if (btn.HasValue)
            {
                List<GetDeliveryChallanList_Result> data = db.GetDeliveryChallanList(BranchID, AccountID, OID, fromDate, toDate, null,null,null).ToList();
                if (data.Count > 0)
                {
                    return PartialView("_PaymentsChallanWise", data.OrderByDescending(so => so.ChallanID));
                }

                return PartialView("_PaymentsChallanWise");
            }

            ViewBag.customer = db.tbl_Customer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();

            return View();
        }

        public JsonResult GetCustomerDetail(int accID)
        {
            var data = db.tbl_Customer.Where(a => a.AccountID == accID).Select(a=> new {
                // CustomerID = a.CustomerID,
                AccountID = a.AccountID,
                SalePersonAccID = a.SalePersonAccID,
                // CodeInt = a.CodeInt,
                // Code = a.Code,
                // Name = a.Name,
                // NameUr = a.NameUR,
                // Phone = a.Phone,
                // Phone1 = a.Phone1,
                // Phone2 = a.Phone2,
                // Cell = a.Cell,
                // Email = a.Email,
                // Address = a.Address,
                // Company = a.Company,
                CreditLimit = a.CreditLimit??0,
                GST = a.GST??"0",
                InvoiceType = a.InvoiceType == 1 ? "Jamaly": a.InvoiceType == 2?"Jemely":"No-Type",
                OpeningBalance = a.OpeningBalance
            }).FirstOrDefault();

            if (data != null)
            {
               // Console.WriteLine("dasdasdasd");
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public FileResult Export(String ReportType)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/saleInvoice.rdlc");


            ReportDataSource rs = new ReportDataSource();
            rs.Name = "DataSet1";
            rs.Value = db.SaleInvoiceNew(1002,null,null,null);

            lo.DataSources.Add(rs);

            byte[] renderbyte;
            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
            
            Response.AddHeader("Content-Disposition",
             "attachment; filename=UnAssignedLevels.pdf");
            
            return new FileContentResult(renderedBytes, mimeType);

        }

        // GET: Customer
        public ActionResult Index()
        {

            //if (Session["BranchID"] != null)
            //{
            //    branchId = Convert.ToInt32(Session["BranchID"]);
            //}
            //else
            //{
            //    var user = User.Identity;
            //    string currentUserId = User.Identity.GetUserId();
            //    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
            //    branchId = currentUser.BranchID;
            //}        ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            var customers = db.tbl_Customer.Where(x => x.IsDeleted != true).ToList();
            return View(customers);
        }
        // Get Last Vendor code and return it to js and increment in it +1 
        public JsonResult getLastCode()
        {
            string tempID = "";

            var tmp = db.tbl_Customer.OrderByDescending(v => v.Code).FirstOrDefault();
            if (tmp != null)
            {
                tempID = tmp.Code;
                if (tempID == null)
                {
                    tempID = "C-1000";
                }
            }
            else
            {
                tempID = "C-1000";
            }

            return Json(tempID, JsonRequestBehavior.AllowGet);
        }
        // Get Credit Limit and Balance 
        public JsonResult getDetail(int accountID)
        {
            if (accountID > 1)
            {
                int? branchId = null;
                
                var customerDetail = db.GetCustomerDetails(accountID, branchId).FirstOrDefault();
                if (customerDetail.Balance.HasValue || customerDetail.creditLimit.HasValue)
                {
                    var result = new { Balance = customerDetail.Balance, creditLimit = customerDetail.creditLimit };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                { return Json("null", JsonRequestBehavior.AllowGet); }
            }
            else
            { return Json("null", JsonRequestBehavior.AllowGet); }

        }
        // Get Credit Limit and Balance 
        public JsonResult getInfo(int accountID)
        {
            if (accountID > 1)
            {
                var customer = db.tbl_Customer.Where(cst => cst.AccountID == accountID).FirstOrDefault();                
                if (customer.LoyaltyCardID > 0)
                {
                    var result = customer.LoyaltyCardID;
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            else
            { return Json("null", JsonRequestBehavior.AllowGet); }

        }
        // GET: Customer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Customer tbl_Customer = db.tbl_Customer.Find(id);
            if (tbl_Customer == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Customer);
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            List<object> list = new List<object>();
            list.Add(new
            {
                Name = "JAMALY",
                Value = 1
            });
            list.Add(new
            {
                Name = "JEMELY",
                Value = 2
            });
            ViewBag.InvoiceType = list.ToList();
            ViewBag.SalePerson = db.tbl_SalePerson.Select(c => new { Value = c.AccountID, Name = c.SalePerson }).ToList();


            return View();
        }

        // POST: Customer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_Customer tbl_Customer)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            if (tbl_Customer.OpeningBalance == null)
            {
                tbl_Customer.OpeningBalance = 0;
            }
            
            
            tbl_Customer.UserID = HttpContext.User.Identity.GetUserId();
            tbl_Customer.BranchID = branchId;
            tbl_Customer.Addby = userID;
            tbl_Customer.AddOn = DateTime.UtcNow.AddHours(5);
            tbl_Customer.IsDeleted = false;
            tbl_Customer.IsActive = true;
            if (ModelState.IsValid)
            {
                if (db.tbl_Customer.Any(c => c.Name == tbl_Customer.Name))
                {
                    ModelState.AddModelError("Name", "Customer Already Exists!");
                    ViewBag.SalePerson = db.tbl_SalePerson.Select(c => new { Value = c.AccountID, Name = c.SalePerson }).ToList();

                    List<object> list = new List<object>();
                    list.Add(new
                    {
                        Name = "JAMALY",
                        Value = 1
                    });
                    list.Add(new
                    {
                        Name = "JEMELY",
                        Value = 2
                    });
                    ViewBag.InvoiceType = list.ToList();
                    return View("Create");
                }
                else
                {
                    if (tbl_Customer.InvoiceType == null)
                    {
                        tbl_Customer.InvoiceType = 1;// Jamaly Invoice Customer
                    }
                    db.tbl_Customer.Add(tbl_Customer);
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added Customers".ToString());

                    return RedirectToAction("Index");
                }
                
            }

            return View(tbl_Customer);
        }
        public ActionResult CreatePartial()
        {            
            return View("_AddCustomer");
        }
        // Create Customer Modal , Sales 
        public async Task<ActionResult> PartialCustomer(Models.DTO.Customer customer)
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                                      .Where(y => y.Count > 0)
                                      .ToList();
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return View("_AddCustomer", customer);

            }
            bool isValid = false;
            if (db.tbl_Customer.Any(v => v.Phone == customer.Phone))
            {
                isValid = true;
            }
            else
            {
                isValid = false;                
                customer.UserID = HttpContext.User.Identity.GetUserId();
                customer.BranchID = branchId;
                Mapper.CreateMap<Models.DTO.Customer, tbl_Customer>();
                var model = Mapper.Map<Models.DTO.Customer, tbl_Customer>(customer);

                db.tbl_Customer.Add(model);
                var task = db.SaveChangesAsync();
                await task;

                if (task.Exception != null)
                {
                    ModelState.AddModelError("", "Unable to add the Customer");
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Adding Partial Customers Failed".ToString());

                    return View("_AddCustomer", customer);
                }
                else
                {
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added Partial Customers".ToString());

                }
            }
            return Json(isValid);

        }
        // Get Customers to refresh dropdowns after Adding Customer on Sales
        public JsonResult getCustomers()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var qry = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name}).ToList();
            return Json(qry);
        }
        [Authorize(Roles = "SuperAdmin,Admin")]
        // GET: Customer/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Customer tbl_Customer = db.tbl_Customer.Find(id);
            if (tbl_Customer == null)
            {
                return HttpNotFound();
            }
            List<object> list = new List<object>();
            list.Add(new
            {
                Name = "JAMALY",
                Value = 1
            });
            list.Add(new
            {
                Name = "JEMELY",
                Value = 2
            });
            ViewBag.InvoiceType = list.ToList();
         
            ViewBag.SalePerson = db.tbl_SalePerson.Select(c => new { Value = c.AccountID, Name = c.SalePerson }).ToList();

            return View(tbl_Customer);
        }
        // Get Customer Sales Date Wise 
        public ActionResult CustomerSales(bool? isSearch,int? AccountID, DateTime? fromDate, DateTime? toDate,int? BranchId)
        {
            int branchId = GetBranchID();
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.customer = db.tbl_Customer.Select(v => new { Value = v.AccountID, Name =v.Name }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();

            if (isSearch == true)
            {
                List<GetCustomerSalesDateWise_Result> customer = db.GetCustomerSalesDateWise(AccountID, fromDate, toDate,BranchId).ToList();
                customer = customer.Where(a => a.isOpening != true).ToList();
                if (customer.Count == 0)
                {
                    return PartialView("_CustomerSales");
                }
                else
                    return PartialView("_CustomerSales", customer);
            }
            return View("CustomerSalesReport");
        }

        [Authorize(Roles = "SuperAdmin,Admin")]
        // POST: Customer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_Customer tbl_Customer)
        {
            tbl_Customer.UpdateOn = DateTime.UtcNow.AddHours(5);
            if (ModelState.IsValid)
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                if (db.tbl_Customer.Any(v => v.CustomerID != tbl_Customer.CustomerID && v.Name == tbl_Customer.Name))
                {
                    ModelState.AddModelError("Name", "Customer Already Exists!");
                    List<object> list = new List<object>();
                    list.Add(new
                    {
                        Name = "JAMALY",
                        Value = 1
                    });
                    list.Add(new
                    {
                        Name = "JEMELY",
                        Value = 2
                    });
                    ViewBag.InvoiceType = list.ToList();
                   
                    ViewBag.SalePerson = db.tbl_SalePerson.Select(c => new { Value = c.AccountID, Name = c.SalePerson }).ToList();

                    return View("Edit");
                }
                else
                {

                    tbl_Customer.InvoiceType = db.tbl_Customer.Where(x => x.AccountID == tbl_Customer.AccountID).Select(x => x.InvoiceType).FirstOrDefault();

                    tbl_Customer.UpdateOn = Helper.PST();
                    tbl_Customer.UpdateBy = userID;
                    db.Entry(tbl_Customer).State = EntityState.Modified;
                    db.Entry(tbl_Customer).Property(v => v.LoyaltyCardID).IsModified = false;
                    //db.Entry(tbl_Customer).Property(v => v.InvoiceType).IsModified = false;
                    db.Entry(tbl_Customer).Property(v => v.AddOn).IsModified = false;
                    db.Entry(tbl_Customer).Property(v => v.AccountID).IsModified = false;
                    db.Entry(tbl_Customer).Property(v => v.Addby).IsModified = false;
                    db.Entry(tbl_Customer).Property(v => v.UserID).IsModified = false;
                    db.Entry(tbl_Customer).Property(v => v.BranchID).IsModified = false;
                    db.Entry(tbl_Customer).Property(v => v.rowguid).IsModified = false;
                    
                       
                    
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edited Customers".ToString());

                    return RedirectToAction("Index");
                }
            }
            
            return View(tbl_Customer);
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Customer tbl_Customer = db.tbl_Customer.Where(v => v.AccountID == id).FirstOrDefault();
            if (tbl_Customer == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Customer);
        }

        // POST: Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                bool isExist = db.tbl_SalesOrder.Any(p => p.AccountID == id && p.IsDeleted!=true);
                bool isChallanExist = db.tbl_Challan.Any(p => p.AccountID == id && p.IsDeleted!=true);
                if (isExist && isChallanExist)
                {
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                    tbl_Customer tbl_Customer = db.tbl_Customer.Where(v => v.AccountID == id).FirstOrDefault();
                    if (tbl_Customer == null)
                    {
                        return HttpNotFound();
                    }
                    return View(tbl_Customer);
                }
                else
                {
                    int CustomerID = db.tbl_Customer.Where(v => v.AccountID == id).Select(v => v.CustomerID).FirstOrDefault();
                    tbl_Customer tbl_Customer = db.tbl_Customer.Find(CustomerID);
                    if (tbl_Customer != null)
                    {
                        var cust = db.tbl_Customer.Where(x => x.AccountID == id).FirstOrDefault();
                        if (cust != null)
                        {
                            db.DeleteCustomer(id, CustomerID);
                            
                            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Customer with AccID: " + id.ToString());
                            return RedirectToAction("Index");

                        }


                    }
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                    return View(tbl_Customer);
                    //db.tbl_Customer.Remove(tbl_Customer);
                }
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
