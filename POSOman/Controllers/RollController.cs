﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class RollController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: Roll
        public ActionResult Index()
        {
            return View(db.tbl_Roll.Where(x=>x.IsDeleted!=true).ToList());
        }

        // GET: Roll/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Roll tbl_Roll = db.tbl_Roll.Find(id);
            if (tbl_Roll == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Roll);
        }

        // GET: Roll/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Roll/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_Roll model)
        {


            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            model.ForOffsetTypeID = 1;
                            if (string.IsNullOrWhiteSpace(model.Roll))
                            {
                                ModelState.AddModelError("Roll", "Name Required");
                                return View(model);
                            }
                            else if (model.RollCost <= 0)
                            {
                                ModelState.AddModelError("RollCost", "Cost Required");
                                return View(model);
                            }
                            else if (model.ForOffsetTypeID == null)
                            {
                                ModelState.AddModelError("ForOffsetTypeID", "Type Required");
                                return View(model);
                            }
                            else if (model.RollWidth <= 0)
                            {
                                ModelState.AddModelError("RollWidth", "Width Required");
                                return View(model);
                            }
                            else if (model.RollMeters_Length <= 0)
                            {
                                ModelState.AddModelError("RollMeters_Length", "Length Required");
                                return View(model);
                            }
                            model.AddedOn = Helper.PST();
                            db.tbl_Roll.Add(model);
                            db.SaveChanges();

                            tbl_Product p = new tbl_Product();
                            p.VehicleCodeID = 3;// Tafata Roll Category
                            p.PartNo = model.Roll;
                            p.isActive = 1;
                            p.IsGeneralItem = false;
                            p.IsOffsetItem = true;
                            p.AddOn = Helper.PST();
                            p.MarkedupPrice = model.RollCost;
                            p.RawProductID = model.ID;
                            db.tbl_Product.Add(p);
                            db.SaveChanges();
                            t.Commit();

                            return RedirectToAction("Index");
                        }

                        t.Rollback();
                        return View(model);
                    }
                    catch (Exception d)
                    {
                        t.Rollback();
                        while (d.InnerException != null)
                        {
                            d = d.InnerException;
                        }
                        ModelState.AddModelError("Roll", d.Message);
                        return View(model);
                    }

                }

            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                ModelState.AddModelError("Roll", e.Message);
                return View(model);
            }

            
        }

        // GET: Roll/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Roll model = db.tbl_Roll.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        // POST: Roll/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( tbl_Roll model)
        {
            if (ModelState.IsValid)
            {
                model.ForOffsetTypeID = 1;
                if (string.IsNullOrWhiteSpace(model.Roll))
                {
                    ModelState.AddModelError("Roll", "Name Required");
                    return View(model);
                }
                else if (model.RollCost <= 0)
                {
                    ModelState.AddModelError("RollCost", "Cost Required");
                    return View(model);
                }
                else if (model.ForOffsetTypeID == null)
                {
                    ModelState.AddModelError("ForOffsetTypeID", "Type Required");
                    return View(model);
                }
                else if (model.RollWidth <= 0)
                {
                    ModelState.AddModelError("RollWidth", "Width Required");
                    return View(model);
                }
                else if (model.RollMeters_Length <= 0)
                {
                    ModelState.AddModelError("RollMeters_Length", "Length Required");
                    return View(model);
                }

               
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Roll/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Roll tbl_Roll = db.tbl_Roll.Find(id);
            if (tbl_Roll == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Roll);
        }

        // POST: Roll/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Roll tbl_Roll = db.tbl_Roll.Find(id);

            if (tbl_Roll != null)
            {
                tbl_Roll.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("Roll", "Record not found");
            return View(tbl_Roll);
           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
