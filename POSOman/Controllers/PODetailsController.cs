﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace POSOman.Controllers
{
    public class PODetailsController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: PODetails
        public ActionResult Index()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var tbl_PODetails = db.tbl_PODetails.Where(p => p.BranchID == branchId).Include(t => t.tbl_Product).Include(t => t.tbl_PurchaseOrder);
            return View(tbl_PODetails.ToList());
        }

        // GET: PODetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_PODetails tbl_PODetails = db.tbl_PODetails.Find(id);
            if (tbl_PODetails == null)
            {
                return HttpNotFound();
            }
            return View(tbl_PODetails);
        }

        // GET: PODetails/Create
        public ActionResult Create()
        {
            ViewBag.ProductID = new SelectList(db.tbl_Product, "ProductID", "PartNo");
            ViewBag.OrderID = new SelectList(db.tbl_PurchaseOrder, "OrderID", "InvoiceNo");
            return View();
        }

        // POST: PODetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DetailID,OrderID,PartNo,ProductID,Qty,UnitPrice,SalePrice,Total,ExchangeRate,DiscountPercent,DiscountAmount,UpdateOn,UpdateBy,ReturnedQty,IsReturned,IsDeleted")] tbl_PODetails tbl_PODetails)
        {
            if (ModelState.IsValid)
            {
                db.tbl_PODetails.Add(tbl_PODetails);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductID = new SelectList(db.tbl_Product, "ProductID", "PartNo", tbl_PODetails.ProductID);
            ViewBag.OrderID = new SelectList(db.tbl_PurchaseOrder, "OrderID", "InvoiceNo", tbl_PODetails.OrderID);
            return View(tbl_PODetails);
        }

        // GET: PODetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_PODetails tbl_PODetails = db.tbl_PODetails.Find(id);
            if (tbl_PODetails == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductID = new SelectList(db.tbl_Product, "ProductID", "PartNo", tbl_PODetails.ProductID);
            ViewBag.OrderID = new SelectList(db.tbl_PurchaseOrder, "OrderID", "InvoiceNo", tbl_PODetails.OrderID);
            return View(tbl_PODetails);
        }

        // POST: PODetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DetailID,OrderID,PartNo,ProductID,Qty,UnitPrice,SalePrice,Total,ExchangeRate,DiscountPercent,DiscountAmount,UpdateOn,UpdateBy,ReturnedQty,IsReturned,IsDeleted")] tbl_PODetails tbl_PODetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_PODetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductID = new SelectList(db.tbl_Product, "ProductID", "PartNo", tbl_PODetails.ProductID);
            ViewBag.OrderID = new SelectList(db.tbl_PurchaseOrder, "OrderID", "InvoiceNo", tbl_PODetails.OrderID);
            return View(tbl_PODetails);
        }

        // GET: PODetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_PODetails tbl_PODetails = db.tbl_PODetails.Find(id);
            if (tbl_PODetails == null)
            {
                return HttpNotFound();
            }
            return View(tbl_PODetails);
        }

        // POST: PODetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_PODetails tbl_PODetails = db.tbl_PODetails.Find(id);
            db.tbl_PODetails.Remove(tbl_PODetails);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
