﻿using AutoMapper;
using Microsoft.Reporting.WebForms;
using POSOman.Models;
using POSOman.Models.BLL;
using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class StockIssuanceController : Controller
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        Common com = new Common();
        SalesOrder custom = new SalesOrder();
        // GET: StockIssuance

        //[Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult Index(bool? btn, int? ItemID, int? ProductID, int? OrderID,int? DeptID,int?MachineID,DateTime? fromDate,DateTime? toDate)
        {
            
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            ViewBag.OrderMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
            ViewBag.Item = db.tbl_Product.Where(x=>x.IsOffsetItem == false && x.IsGeneralItem == false).Select(b => new { Value = b.ProductID, Name = b.PartNo }).ToList();
            ViewBag.Department = db.tbl_Department.Where(x=>x.IsDeleted !=true).Select(b => new { Value = b.ID, Name = b.Department }).ToList();
            ViewBag.RawProduct = db.tbl_Product.Where(x=>x.IsOffsetItem != false || x.IsGeneralItem != false).Select(b => new { Value = b.ProductID, Name = b.PartNo }).ToList();
           if (btn.HasValue)
            {
                var stock = db.GetMixedStockIssuanceListFilterWise(ProductID,ItemID,OrderID,DeptID,fromDate,toDate,MachineID).Select(x=>new {x.OrderID,x.ItemID,x.Note,x.ItemName ,x.StkIssuanceDate,x.Department,x.RawProducts,x.RawProductsUnitCode,x.ID,
                    x.TotalCost,x.ProductCost,x.OrderMachine,x.TotIssuedQty,x.UnitCode,x.Category
                }).Distinct().ToList();

                if (stock.Count == 0)
                {
                    return PartialView("_Index");
                }
                else
                {
                    List<StkIssuance> list = new List<StkIssuance>();
                   foreach(var i in stock)
                    {
                        list.Add(new StkIssuance
                        {
                            OrderID = i.OrderID??0,
                            ID = i.ID,
                            ItemID = i.ItemID,
                            RawProductList = i.RawProducts,
                            RawProductUnitCodeList = i.RawProductsUnitCode,
                            ItemName = i.ItemName,
                            Department = i.Department,
                            StkIssuanceDate = i.StkIssuanceDate,
                            TotalCostPrice = i.TotalCost??0,
                            CostPrice = i.ProductCost??0,
                            UnitCode = i.UnitCode,
                            Category = i.Category,
                            Qty = i.TotIssuedQty??0,
                            Machine = i.OrderMachine,
                            Note = i.Note,
                        });
                    }
                    return PartialView("_Index", list.Distinct().ToList());
                }
            }

            return View();
        }


        //public ActionResult StkIssuanceDetails (int OrderID,int ItemID)
        //{
        //    try
        //    {
        //        List<GetStockIssuanceFilterWise_Result> list = db.GetStockIssuanceFilterWise(null,ItemID,OrderID,null,null).ToList();
        //        if (list.Count > 0)
        //        {

        //            StkIssuanceDetailsDTO data = new StkIssuanceDetailsDTO();

        //            data.ItemID = ItemID;
        //            data.ItemName = list.Where(x => x.ItemID == ItemID).Select(x => x.ItemName).FirstOrDefault() ?? "-";
        //            data.OrderID = OrderID;

        //            foreach(var s in list)
        //            {
        //                data.stkIssuedProducts.Add(new StkIssuance { RawProductID = s.ProductID, RawProductQty = s.TotalIssuedQty });
        //            }

        //            var p = db.tbl_Product.Where(x => x.ProductID == ItemID).FirstOrDefault();
        //            if (p != null)
        //            {
        //                List<int> IDs = new List<int>();
        //                if (p.DepartmentID == 1)//Offset
        //                {
        //                    var id = db.tbl_Roll.Where(x => x.ID == p.RollID).Select(x =>new { x.ID ,x.Roll,x.RollMeters_Length}).FirstOrDefault();
        //                    if (id !=null)
        //                    {
        //                        data.rawProducts.Add(new StkIssuance { RawProduct = id.Roll, RawProductQty = (int)id.RollMeters_Length });

        //                        //IDs.Add(id);

        //                    }
        //                    else
        //                    {
        //                        var card_id = db.tbl_Card_Quality.Where(x => x.ID == p.CardQuality).Select(x => new { x.ID ,x.CardName,x.Rate}).FirstOrDefault();
        //                        if (card_id !=null)
        //                        {
        //                            data.rawProducts.Add(new StkIssuance { RawProduct = card_id.CardName, RawProductQty = (int)card_id.Rate });

        //                            // IDs.Add(id);

        //                        }
        //                    }

        //                }
        //                else if (p.DepartmentID == 2)//Satin
        //                {
        //                    var ribbon_id = db.tbl_Roll.Where(x => x.ID == p.RollID).Select(x => new { x.ID ,x.Roll,x.RollMeters_Length}).FirstOrDefault();
        //                    if (ribbon_id !=null)
        //                    {
        //                        data.rawProducts.Add(new StkIssuance { RawProduct = ribbon_id.Roll, RawProductQty = (int)ribbon_id.RollMeters_Length });

        //                        //IDs.Add(ribbon_id);

        //                    }

        //                }
        //                else if (p.DepartmentID == 3)//Sticker
        //                {
        //                    var sq = db.tbl_StickerQuality.Where(x => x.ID == p.StickerQualityID).Select(x => new { x.ID,x.Quality, x.GSM}).FirstOrDefault();
        //                    if (sq != null)
        //                    {
        //                        data.rawProducts.Add(new StkIssuance { RawProduct = sq.Quality, RawProductQty = (int)sq.GSM });
        //                      //  IDs.Add(sq);

        //                    }
        //                    var f_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_F && x.RibbonTypeID == 1).Select(x => new { x.ID ,x.Ribbon,x.RollLengthInM}).FirstOrDefault();
        //                    if (f_id !=null)
        //                    {
        //                        data.rawProducts.Add(new StkIssuance { RawProduct = f_id.Ribbon, RawProductQty = (int)f_id.RollLengthInM });
        //                        //IDs.Add(f_id);

        //                    }
        //                    var t_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_T && x.RibbonTypeID == 2).Select(x => new { x.ID, x.Ribbon, x.RollLengthInM }).FirstOrDefault();
        //                    if (t_id != null)
        //                    {
        //                        data.rawProducts.Add(new StkIssuance { RawProduct = t_id.Ribbon, RawProductQty = (int)t_id.RollLengthInM });
        //                        //IDs.Add(t_id);

        //                    }
        //                    var s_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_S && x.RibbonTypeID == 3).Select(x => new { x.ID, x.Ribbon, x.RollLengthInM }).FirstOrDefault();
        //                    if (s_id != null)
        //                    {
        //                        data.rawProducts.Add(new StkIssuance { RawProduct = s_id.Ribbon, RawProductQty = (int)s_id.RollLengthInM });
        //                        //IDs.Add(s_id);

        //                    }
        //                    var rf_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_RFID && x.RibbonTypeID == 4).Select(x => new { x.ID, x.Ribbon, x.RollLengthInM }).FirstOrDefault();
        //                    if (rf_id != null)
        //                    {
        //                        data.rawProducts.Add(new StkIssuance { RawProduct = rf_id.Ribbon, RawProductQty = (int)rf_id.RollLengthInM });
        //                        //IDs.Add(rf_id);

        //                    }

        //                }
        //                return PartialView("_StkIssuanceDetails", data);
        //            }
        //            else
        //            {
        //                return PartialView("_StkIssuanceDetails");
        //            }

                    
        //        }

        //        return View();
        //    }
        //    catch(Exception c)
        //    {
        //        return View(c.Message);

        //    }
        //}


        public FileResult StkIssuanceCostDetails(int OrderID, int ItemID)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/StockIssuanceCostReport.rdlc");
            ReportDataSource rs1 = new ReportDataSource();
            ReportDataSource rs2 = new ReportDataSource();
            ReportDataSource masterData = new ReportDataSource();

            rs1.Name = "IssuedProductDataset";
            rs2.Name = "DefaultProductDataset";
            masterData.Name = "OverAllStkIssuanceDataSet";

            #region Implemented Data source
            try
            {
                List<GetStockIssuanceFilterWise_Result> list = db.GetStockIssuanceFilterWise(null, null, ItemID, OrderID,null, null, null).ToList();
                if (list.Count > 0)
                {

                    StkIssuanceDetailsDTO data = new StkIssuanceDetailsDTO();
                    decimal totalRawProductPrice = 0;
                    List<object> newData = new List<object>();
                    data.ItemID = ItemID;
                    data.ItemName = list.Where(x => x.ItemID == ItemID).Select(x => x.ItemName).FirstOrDefault() ?? "-";
                    data.CustomerName = list.Where(x => x.ItemID == ItemID && x.OrderID == OrderID).Select(x => x.CustomerName).FirstOrDefault() ?? "-";
                    data.ItemQty = Convert.ToDecimal(list.Where(x => x.ItemID == ItemID).Select(x => x.ItemQty).FirstOrDefault());
                    data.ItemSalePrice = Convert.ToDecimal(list.Where(x => x.ItemID == ItemID).Select(x => x.ProductCost).FirstOrDefault());
                    data.OrderID = OrderID;
                    data.IssueDate = list.Where(x => x.OrderID == OrderID && x.ItemID == ItemID).Select(x => x.StkIssuanceDate).FirstOrDefault() ;
                   
                    List<StkIssuance> ip = new List<StkIssuance>();
                    var newList = list.Select(x => new { x.RawProduct, x.TotalIssuedQty, x.StkCostPrice, x.StkIssuanceDate }).Distinct().ToList();
                    foreach (var s in newList)
                    {
                        
                       ip.Add(new StkIssuance { RawProduct = s.RawProduct, RawProductQty = s.TotalIssuedQty , RawProductPrice = (decimal)s.StkCostPrice, RawStkIssuanceDate = Convert.ToDateTime(s.StkIssuanceDate).ToShortDateString() });
                        totalRawProductPrice += s.TotalIssuedQty * (decimal)s.StkCostPrice;
                    }
                    
                    data.stkIssuedProducts = ip;//Issued Products

                    //Default Products
                    var p = db.tbl_Product.Where(x => x.ProductID == ItemID).FirstOrDefault();

                    if (p != null)
                    {
                       

                        List<StkIssuance> dp = new List<StkIssuance>();
                        List<int> IDs = new List<int>();
                       
                        if (p.DepartmentID == 1)//Offset
                        {
                            var id = db.tbl_Roll.Where(x => x.ID == p.RollID).Select(x => new { x.ID, x.Roll, x.RollMeters_Length }).FirstOrDefault();
                            if (id != null)
                            {
                               dp.Add(new StkIssuance { RawProduct = id.Roll, RawProductQty = (int)id.RollMeters_Length , RawProductPrice = (decimal)p.PerSheetCost });

                                
                                //IDs.Add(id);

                            }
                            else
                            {
                                var card_id = db.tbl_Card_Quality.Where(x => x.ID == p.CardQuality).Select(x => new { x.ID, x.CardName, x.Rate }).FirstOrDefault();
                                if (card_id != null)
                                {
                                    dp.Add(new StkIssuance { RawProduct = card_id.CardName, RawProductQty = (int)0, RawProductPrice = (decimal)p.MaterialCostPerMeter});
                                  
                                    // IDs.Add(id);

                                }
                            }

                            data.rawProducts = dp;

                        }
                        else if (p.DepartmentID == 2)//Satin
                        {
                            var ribbon_id = db.tbl_Roll.Where(x => x.ID == p.RollID).Select(x => new { x.ID, x.Roll, x.RollMeters_Length }).FirstOrDefault();
                            if (ribbon_id != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = ribbon_id.Roll, RawProductQty = (int)ribbon_id.RollMeters_Length, RawProductPrice = (decimal)p.MaterialCostPerMeter });
                                //IDs.Add(ribbon_id);

                            }

                            data.rawProducts = dp;

                        }
                        else if (p.DepartmentID == 3)//Sticker
                        {
                            var sq = db.tbl_StickerQuality.Where(x => x.ID == p.StickerQualityID).Select(x => new { x.ID, x.Quality, x.GSM }).FirstOrDefault();
                            if (sq != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = sq.Quality, RawProductQty = (int)sq.GSM, RawProductPrice = (decimal)p.TotalM2Cost });
                                //  IDs.Add(sq);
                              
                            }
                            var f_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_F && x.RibbonTypeID == 1).Select(x => new { x.ID, x.Ribbon, x.RollLengthInM }).FirstOrDefault();
                            if (f_id != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = f_id.Ribbon, RawProductQty = (int)f_id.RollLengthInM, RawProductPrice = (decimal)p.TotalM2Cost });
                                //IDs.Add(f_id);

                            }
                            var t_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_T && x.RibbonTypeID == 2).Select(x => new { x.ID, x.Ribbon, x.RollLengthInM }).FirstOrDefault();
                            if (t_id != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = t_id.Ribbon, RawProductQty = (int)t_id.RollLengthInM, RawProductPrice = (decimal)p.TotalM2Cost });
                                //IDs.Add(t_id);

                              
                            }
                            var s_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_S && x.RibbonTypeID == 3).Select(x => new { x.ID, x.Ribbon, x.RollLengthInM }).FirstOrDefault();
                            if (s_id != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = s_id.Ribbon, RawProductQty = (int)s_id.RollLengthInM, RawProductPrice = (decimal)p.TotalM2Cost });
                                //IDs.Add(s_id);

                               
                            }
                            var rf_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_RFID && x.RibbonTypeID == 4).Select(x => new { x.ID, x.Ribbon, x.RollLengthInM }).FirstOrDefault();
                            if (rf_id != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = rf_id.Ribbon, RawProductQty = (int)rf_id.RollLengthInM, RawProductPrice = (decimal)p.TotalM2Cost });
                                //IDs.Add(rf_id);

                             
                            }

                            data.rawProducts = dp;

                        }

                        newData.Add(new
                        {
                            ItemID = ItemID,
                            ItemName = data.ItemName,
                            Customer = data.CustomerName,
                            ItemQty = data.ItemQty,
                            ItemSalePrice = data.ItemSalePrice,
                            OrderID = OrderID,
                            IssueDate = data.IssueDate,
                            ActualPrice = list.Sum(x => (decimal)x.ItemSalePrice * (decimal)x.ItemQty),
                            EstimatedPrice = totalRawProductPrice // p.MarkedupPrice
                        });

                        masterData.Value = newData;
                        rs1.Value = data.stkIssuedProducts;
                        rs2.Value = data.rawProducts;
                    }
                    else
                    {
                        masterData.Value = null;
                        rs1.Value = null;
                        rs2.Value = null;
                    }


                }
                //masterData.Value = null;
                //rs1.Value = null;
                //rs2.Value = null;

            }
            catch (Exception c)
            {
                masterData.Value = null;
                rs1.Value = null;
                rs2.Value = null;

            }
            #endregion

           lo.DataSources.Add(rs1);
           lo.DataSources.Add(rs2);
           lo.DataSources.Add(masterData);


            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            //Response.AddHeader("Content-Disposition", "attachment; filename=SaleInvoice.pdf");

            return new FileContentResult(renderedBytes, mimeType);
        }

        public FileResult StkIssuanceDetails(int? OrderID, int? ItemID, int? StkOrderID)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/StockIssuanceDetailReport.rdlc");
            ReportDataSource rs1 = new ReportDataSource();
            ReportDataSource rs2 = new ReportDataSource();
            ReportDataSource masterData = new ReportDataSource();

            rs1.Name = "IssuedProductDataset";
            rs2.Name = "DefaultProductDataset";
            masterData.Name = "OverAllStkIssuanceDataSet";

            #region Implemented Data source
            try
            {
                List<GetStockIssuanceFilterWise_Result> list = db.GetStockIssuanceFilterWise(StkOrderID,null, ItemID, OrderID,null, null, null).ToList();
                if (list.Count > 0)
                {

                    StkIssuanceDetailsDTO data = new StkIssuanceDetailsDTO();
                    decimal totalRawProductPrice = 0;
                    List<object> newData = new List<object>();
                    data.ItemID = ItemID??0;
                    data.ItemName = list.Where(x => x.ItemID == ItemID).Select(x => x.ItemName).FirstOrDefault() ?? "-";
                    data.CustomerName = list.Where(x => x.ItemID == ItemID && x.OrderID == OrderID).Select(x => x.CustomerName).FirstOrDefault() ?? "-";
                    data.ItemQty = Convert.ToDecimal(list.Where(x => x.ItemID == ItemID).Select(x => x.ItemQty).FirstOrDefault());
                    data.ItemSalePrice = Convert.ToDecimal(list.Where(x => x.ItemID == ItemID).Select(x => x.ItemSalePrice).FirstOrDefault());
                    data.OrderID = OrderID??0;
                    data.IssueDate = StkOrderID!=null ? list.Where(x => x.ID ==StkOrderID).Select(x => x.StkIssuanceDate).FirstOrDefault(): list.Where(x => x.OrderID == OrderID && x.ItemID == ItemID).Select(x => x.StkIssuanceDate).FirstOrDefault();

                    List<StkIssuance> ip = new List<StkIssuance>();
                    var newList = list.Select(x => new { x.RawProduct, x.TotalIssuedQty, x.StkCostPrice, x.StkIssuanceDate }).Distinct().ToList();

                    foreach (var s in newList)
                    {

                        ip.Add(new StkIssuance { RawProduct = s.RawProduct, RawProductQty = s.TotalIssuedQty, RawProductPrice = (decimal)s.StkCostPrice, RawStkIssuanceDate = Convert.ToDateTime(s.StkIssuanceDate).ToShortDateString() });
                        totalRawProductPrice += s.TotalIssuedQty * (decimal)s.StkCostPrice;
                    }

                    data.stkIssuedProducts = ip;//Issued Products

                    //Default Products
                    var p = db.tbl_Product.Where(x => x.ProductID == ItemID).FirstOrDefault();

                    if (p != null)
                    {


                        List<StkIssuance> dp = new List<StkIssuance>();
                        List<int> IDs = new List<int>();

                        if (p.DepartmentID == 1)//Offset
                        {
                            var id = db.tbl_Roll.Where(x => x.ID == p.RollID).Select(x => new { x.ID, x.Roll, x.RollMeters_Length }).FirstOrDefault();
                            if (id != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = id.Roll, RawProductQty = (int)id.RollMeters_Length, RawProductPrice = (decimal)p.MarkedupPrice });


                                //IDs.Add(id);

                            }
                            else
                            {
                                var card_id = db.tbl_Card_Quality.Where(x => x.ID == p.CardQuality).Select(x => new { x.ID, x.CardName, x.Rate }).FirstOrDefault();
                                if (card_id != null)
                                {
                                    dp.Add(new StkIssuance { RawProduct = card_id.CardName, RawProductQty = (int)0, RawProductPrice = (decimal)p.MarkedupPrice });

                                    // IDs.Add(id);

                                }
                            }

                            data.rawProducts = dp;

                        }
                        else if (p.DepartmentID == 2)//Satin
                        {
                            var ribbon_id = db.tbl_Roll.Where(x => x.ID == p.RollID).Select(x => new { x.ID, x.Roll, x.RollMeters_Length }).FirstOrDefault();
                            if (ribbon_id != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = ribbon_id.Roll, RawProductQty = (int)ribbon_id.RollMeters_Length, RawProductPrice = (decimal)p.MarkedupPrice });
                                //IDs.Add(ribbon_id);

                            }

                            data.rawProducts = dp;

                        }
                        else if (p.DepartmentID == 3)//Sticker
                        {
                            var sq = db.tbl_StickerQuality.Where(x => x.ID == p.StickerQualityID).Select(x => new { x.ID, x.Quality, x.GSM }).FirstOrDefault();
                            if (sq != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = sq.Quality, RawProductQty = (int)sq.GSM, RawProductPrice = (decimal)p.MarkedupPrice });
                                //  IDs.Add(sq);

                            }
                            var f_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_F && x.RibbonTypeID == 1).Select(x => new { x.ID, x.Ribbon, x.RollLengthInM }).FirstOrDefault();
                            if (f_id != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = f_id.Ribbon, RawProductQty = (int)f_id.RollLengthInM, RawProductPrice = (decimal)p.MarkedupPrice });
                                //IDs.Add(f_id);


                            }
                            var t_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_T && x.RibbonTypeID == 2).Select(x => new { x.ID, x.Ribbon, x.RollLengthInM }).FirstOrDefault();
                            if (t_id != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = t_id.Ribbon, RawProductQty = (int)t_id.RollLengthInM, RawProductPrice = (decimal)p.MarkedupPrice });
                                //IDs.Add(t_id);


                            }
                            var s_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_S && x.RibbonTypeID == 3).Select(x => new { x.ID, x.Ribbon, x.RollLengthInM }).FirstOrDefault();
                            if (s_id != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = s_id.Ribbon, RawProductQty = (int)s_id.RollLengthInM, RawProductPrice = (decimal)p.MarkedupPrice });
                                //IDs.Add(s_id);


                            }
                            var rf_id = db.tbl_StickerRibbon.Where(x => x.ID == p.StickerRibbonID_RFID && x.RibbonTypeID == 4).Select(x => new { x.ID, x.Ribbon, x.RollLengthInM }).FirstOrDefault();
                            if (rf_id != null)
                            {
                                dp.Add(new StkIssuance { RawProduct = rf_id.Ribbon, RawProductQty = (int)rf_id.RollLengthInM, RawProductPrice = (decimal)p.MarkedupPrice });
                                //IDs.Add(rf_id);


                            }

                            data.rawProducts = dp;

                        }

                        
                    }
                    else
                    {
                        data.rawProducts = data.stkIssuedProducts;
                    }
                    newData.Add(new
                    {
                        ItemID = ItemID??0,
                        ItemName = data.ItemName,
                        Customer = data.CustomerName,
                        ItemQty = data.ItemQty,
                        ItemSalePrice = data.ItemSalePrice,
                        OrderID = OrderID??0,
                        IssueDate = data.IssueDate,
                        ActualPrice = list.Sum(x => Convert.ToDecimal(x.ItemSalePrice) * Convert.ToDecimal(x.ItemQty)),
                        EstimatedPrice = totalRawProductPrice // p.MarkedupPrice
                    });

                    masterData.Value = newData;
                    rs1.Value = data.stkIssuedProducts;
                    rs2.Value = data.rawProducts;
                    //else
                    //{
                    //    masterData.Value = null;
                    //    rs1.Value = null;
                    //    rs2.Value = null;
                    //}

                    lo.DataSources.Add(rs1);
                    lo.DataSources.Add(rs2);
                    lo.DataSources.Add(masterData);


                    string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
                    Warning[] warnings;
                    string[] streams;
                    string mimeType;
                    byte[] renderedBytes;
                    string encoding;
                    string fileNameExtension;
                    renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


                    //Response.AddHeader("Content-Disposition", "attachment; filename=SaleInvoice.pdf");

                    return new FileContentResult(renderedBytes, mimeType);
                }
                //masterData.Value = null;
                //rs1.Value = null;
                //rs2.Value = null;

            }
            catch (Exception c)
            {
                masterData.Value = null;
                rs1.Value = null;
                rs2.Value = null;

              
            }
            #endregion
            return new FileContentResult(null, null);

        }

        // GET: StockIssuance/Create
        public ActionResult Create()
        {
            GetViewBagValues();
            return View();
        }

        private void GetViewBagValues()
        {
            List<object> Item = new List<object>();
            Item.Add(new { Value = 0, Name = "-" });
            ViewBag.Item = Item.ToList();
            ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.OrderMachine = db.tbl_OrderMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.OrderMachine }).ToList();
            ViewBag.JobOrder = db.tbl_SalesOrder.Where(x => x.IsDeleted != true && x.IsSaleOrder == true).Select(x => new { Value = x.OrderID, Name = x.OrderID }).Distinct().ToList();
            ViewBag.Category = db.tbl_VehicleCode.Where(x => x.IsDeleted != true && x.VehicleCodeID > 1).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode }).Distinct().ToList();
            //ViewBag.Product = db.tbl_Product.Where(x => ((x.isActive == null || x.isActive == 1) && x.IsGeneralItem == true && x.IsOffsetItem == false) ).Select(x => new { Value = x.ProductID, Name = x.PartNo }).Distinct().ToList();
            ViewBag.Product = db.tbl_Product.Where(x => ((x.isActive == null || x.isActive == 1) && x.IsGeneralItem == true && x.IsOffsetItem == false) ).Select(x => new { Value = x.ProductID, Name = x.PartNo }).Distinct().ToList();
        }

        public JsonResult getItemsOrderIDWise(int OrderID)
        {
            try
            {
                if (OrderID > 0)
                {
                    var data = db.tbl_SaleDetails.Where(x => x.IsDeleted != true && x.OrderID == OrderID).
                        Select(x => new { Value = x.ProductID, Name = x.tbl_Product.PartNo }).ToList();

                    return Json(data, JsonRequestBehavior.AllowGet);
                }

                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        
        public JsonResult getDepartmentIDItemsIDWise(int ItemID)
        {
            try
            {
                if (ItemID > 0)
                {
                    var data = db.tbl_Product.Where(x => x.ProductID == ItemID).Select(x => x.DepartmentID).FirstOrDefault();
                    if(data>0 && data != null)
                    {
                        return Json(data, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult getRawMaterialDepartmentIDWise()
        {
            try
            {

                var data = db.tbl_Product.Where(x => (x.isActive == null || x.isActive == 1) && x.DepartmentID == null).
                    Select(x => new { Value = x.ProductID, Name = x.PartNo }).ToList();

                return Json(data, JsonRequestBehavior.AllowGet);


            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        //public JsonResult getRawMaterialItemIDWise(int OrderID, int ItemID)
        //{
        //    try
        //    {
        //        if (OrderID>0 && ItemID > 0)
        //        {
        //            List<object> genericList = new List<object>();

        //               // var data = db.tbl_SaleDetails.Where(x => x.IsDeleted != true && x.OrderID == OrderID && x.ProductID == ItemID && x.tbl_Product.DepartmentID == DepartmentID).
        //               //Select(x => new { x.CardQuality, x.RollID}).FirstOrDefault();
        //               // if (data != null)
        //               // {
        //                    var rawMaterials = db.tbl_StockIssuance.Where(x => x.ItemID == ItemID && x.OrderID == OrderID).ToList();
        //                    if (rawMaterials != null)
        //                    {
        //                        foreach(var i in rawMaterials)
        //                        {
        //                            var stkQty = Convert.ToInt32(db.tbl_Stock.Where(x => x.ProductID == i.ProductID ).Select(x => x.Qty).FirstOrDefault());
        //                            genericList.Add(new
        //                            {
        //                                ProductID = i.ProductID,
        //                                OrderID = i.OrderID,
        //                                Product = i.tbl_Product.PartNo,
        //                                TotalIssuedQty = i.TotalIssuedQty,
        //                                Department = i.tbl_Department.Department??"-",
        //                                DepartmentID = i.DepartmentID,
        //                                StockQty = stkQty
        //                            });

        //                        }

        //                    }

        //               // }

        //            return Json(genericList, JsonRequestBehavior.AllowGet);
        //        }

        //        return Json(null, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(e.Message, JsonRequestBehavior.AllowGet);
        //    }
        //}

        public JsonResult Save(tbl_StockIssuance model)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    if (model != null)
                    {
                        //if(model.OrderID != null && model.ItemID <=0)
                        //{
                        //    t.Rollback();
                        //    return Json(-3);
                        //}
                        if (model.OrderID == null && model.ItemID > 0)
                        {
                            t.Rollback();
                            return Json(-3);
                        }
                        else if(model.DepartmentID == null)
                        {
                            t.Rollback();
                            return Json(-4);
                        }

                        //if(db.tbl_StockIssuance.Any(x=>x.OrderID == model.OrderID && x.ItemID == model.ItemID))
                        //{
                            //t.Rollback();
                            //return Json(-2);

                            
                        //}
                        if(model.AddedOn == null)
                        {
                            t.Rollback();
                            return Json(-5);
                        }
                        //model.AddedOn = Helper.PST();
                        if (model.OrderID <= 0)
                        {
                            model.OrderID = null;
                        }
                        List<tbl_StockLog> logList = new List<tbl_StockLog>();

                        foreach (var i in model.tbl_StockIssuanceDetails)
                        {

                            if (i.CurrentIssuedQty > 0)
                            {
                                i.TotalIssuedQty = i.CurrentIssuedQty;
                                i.IssuanceDate = model.AddedOn;
                                i.AddedOn = Helper.PST();

                                var stk = db.tbl_Stock.Where(x => x.ProductID == i.ProductID).FirstOrDefault();
                                if (stk != null)
                                {
                                    decimal stkCostPrice = stk.CostPrice??0;
                                    if ((stk.Qty - i.CurrentIssuedQty) >= 0)
                                    {
                                        stk.Qty -= i.CurrentIssuedQty;
                                        i.CostPrice = stk.CostPrice??0;
                                        if (stk.Qty <= 0) // to cater the decimal qty
                                        {
                                            stk.CostPrice = 0;
                                        }
                                        db.Entry(stk).State = System.Data.Entity.EntityState.Modified;

                                        tbl_StockLog stkLog = new tbl_StockLog();

                                        stkLog.AddOn = Helper.PST();
                                        stkLog.BranchID = 9001;
                                        stkLog.CostPrice = stkCostPrice;
                                        stkLog.Detail = model.Note ?? "-";
                                        stkLog.InvoiceDate = Helper.PST();
                                        stkLog.IsActive = true;
                                        stkLog.OutReference = "Stock Issued";
                                        stkLog.ProductID = i.ProductID;
                                        stkLog.SalePrice = stk.SalePrice??0;
                                        stkLog.StockOut = i.CurrentIssuedQty;
                                        stkLog.StockIN = 0;
                                        stkLog.OrderTypeID = 13; //Issued/Consumed
                                        stkLog.AddBy = 1;
                                        stkLog.UnitID = db.tbl_Product.Where(x => x.ProductID == i.ProductID).Select(x=>x.UnitCodeID).FirstOrDefault();
                                        stkLog.UnitCode = db.tbl_Product.Where(x => x.ProductID == i.ProductID).Select(x=>x.UnitCode).FirstOrDefault();
                                        stkLog.OrderID = model.OrderID!=null ? model.OrderID: null;
                                        stkLog.AccountID = db.tbl_SalesOrder.Where(x=>x.OrderID == model.OrderID).Select(y=>y.AccountID).FirstOrDefault();
                                        
                                        logList.Add(stkLog);
                                    
                                    }
                                    else
                                    {
                                        t.Rollback();
                                        return Json(-3);
                                    }
                                   
                                }
                                else
                                {
                                    t.Rollback();
                                    return Json(-3);
                                }
                            }

                        }
                        
                        

                        Mapper.CreateMap<tbl_StockIssuance, tbl_StockIssuance>();
                        Mapper.CreateMap<tbl_StockIssuanceDetails, tbl_StockIssuanceDetails>();
                        var details = Mapper.Map<ICollection<tbl_StockIssuanceDetails>, ICollection<tbl_StockIssuanceDetails>>(model.tbl_StockIssuanceDetails);
                        var master = Mapper.Map<tbl_StockIssuance, tbl_StockIssuance>(model);
                       // master.tbl_StockIssuanceDetails = details;
                        db.tbl_StockIssuance.Add(master);
                        db.SaveChanges();

                        if (logList != null && logList.Count > 0)
                        {
                            logList.ForEach(x=>x.OrderID = master.ID);
                            db.tbl_StockLog.AddRange(logList);
                        }

                        db.SaveChanges();
                        t.Commit();
                        return Json(1);
                    }
                    t.Rollback();
                    return Json(-1);
                }

            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message);
            }
        }

        //[Authorize(Roles = "SuperAdmin, Admin")]
        // GET: Product/Delete/5
        public ActionResult Delete(int? ID)
        {
            if (ID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StockIssuance data = db.tbl_StockIssuance.Find(ID);
            if (data == null)
            {
                return HttpNotFound();
            }
            return View(data);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int ID)
        {
            try
            {
                var record = db.tbl_StockIssuance.Where(x => x.ID == ID).FirstOrDefault();
                if (record != null)
                {
                    bool Exist = db.tbl_SaleDetails.Any(x => record.ItemID == x.ProductID && x.OrderID == record.OrderID && x.tbl_SalesOrder.IsSaleOrder == false);
                    if (Exist)
                    {
                        ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                        tbl_StockIssuance data = db.tbl_StockIssuance.Find(ID);
                        return View(data);
                    }
                    else
                    {

                        //var data = db.tbl_StockIssuance.Where(x => x.ID == ID).FirstOrDefault();
                        //if (data != null)
                        //{
                            foreach(var i in record.tbl_StockIssuanceDetails)
                            {
                                var stk = db.tbl_Stock.Where(x => x.ProductID == i.ProductID).FirstOrDefault();
                                if (stk != null)
                                {
                                    
                                        stk.Qty += i.CurrentIssuedQty;
                                        db.Entry(stk).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                        i.IsDeleted = true;
                                }
                            }

                            record.IsDeleted = true;
                            db.Entry(record).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();

                            return RedirectToAction("Index");
                       // }
                        
                    }
                    
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Some error occured");
                    tbl_StockIssuance tbl_StockIssuance = db.tbl_StockIssuance.Find(ID);
                    return View(tbl_StockIssuance);
                }
             
               
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                tbl_StockIssuance tbl_StockIssuance = db.tbl_StockIssuance.Find(ID);
                return View(tbl_StockIssuance);
            }
        }

    }
}
