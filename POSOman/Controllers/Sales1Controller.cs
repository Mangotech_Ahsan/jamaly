﻿
using POSOman.Models;
using POSOman.Models.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using POSOman.Models.DTO;
using System.Net;
using System.Data.Entity;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace POSOman.Controllers
{
    public class Sales1Controller : Controller
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        Common com = new Common();

        private void GetViewBagValues()
        {
            List<object> chBag = new List<object>();
            chBag.Add(new
            {
                Value = 0,
                Name = "No Data"
            });
            var ChallanIDs = db.tbl_ChallanIDs.Where(x => x.isDeleted != true).Select(x => x.ChallanID).Distinct().ToList();
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Product = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.Vat = db.tbl_TAX.Where(x => x.IsDeleted != true && x.IsActive == true).Select(c => new { Value = (int)c.AmountInPercentage, Name = c.TaxDescription + " | " + c.AmountInPercentage.ToString() + "%" }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Where(x => x.VehicleCodeID != 1).Select(v => new { Value = v.VehicleCodeID, Name = v.tbl_VehicleCode2.VehicleCode + "->" + v.VehicleCode }).ToList();
            ViewBag.DeliveryChallan = chBag.ToList();// db.tbl_Challan.Where(x => !ChallanIDs.Contains(x.ChallanID) && x.IsDeleted != true).Select(a => new { Value = 0, Name = "No Data" }).FirstOrDefault();
            ViewBag.PaymentTypeID = db.tbl_PaymentTypes.Where(p => p.ID < 3).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.saleperson = db.tbl_SalePerson.Select(c => new { Value = c.AccountID, Name = c.SalePerson }).ToList();
        }

        public JsonResult SalePersonCommissionDetails(int AccID)
        {
            if (AccID > 0)
            {
                var data = db.tbl_SalePerson.Where(x => x.AccountID == AccID).Select(x => x.CommissionPercent).FirstOrDefault();
                if (data != null)
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        // GET: Sales1
        public ActionResult Index(bool? btn, int? AccountID, int? SPAccID, int? OID, DateTime? fromDate, DateTime? toDate, int? BranchID, int? InvoiceStatusID,string PaymentStatusDD , int? InvoiceTypeID, int? startDay, int? endDay,decimal? taxPercent)
        {
            if (btn.HasValue)
            {
                List<GetSalesIndexFilterWise_Result> data = db.GetSalesIndexFilterWise(BranchID, AccountID, SPAccID, OID, fromDate, toDate, null, InvoiceStatusID, PaymentStatusDD, InvoiceTypeID, startDay, endDay,taxPercent).ToList();
                if (data != null)
                {
                    if (InvoiceStatusID >= 0)
                    {
                        data = data.Where(x => x.IsInvoiceReceived == (InvoiceStatusID == 0 ? false : true)).ToList();
                    }

                    return PartialView("_Index", data.OrderByDescending(so => so.OrderID));
                }

                return PartialView("_Index");
            }


            ViewBag.Branch = db.tbl_Branch.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.SalePerson = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.SalePerson }).ToList();
            return View();
        }

        public ActionResult DeletedInvoices(bool? btn, int? AccountID, int? SPAccID, int? OID, DateTime? fromDate, DateTime? toDate, int? BranchID, int? InvoiceStatusID, string PaymentStatusDD, int? InvoiceTypeID, int? startDay, int? endDay)
        {
            if (btn.HasValue)
            {
                List<GetDeletedSalesFilterWise_Result> data = db.GetDeletedSalesFilterWise(BranchID, AccountID, SPAccID, OID, fromDate, toDate, null, InvoiceStatusID, PaymentStatusDD, InvoiceTypeID, startDay, endDay).ToList();
                if (data != null)
                {
                    if (InvoiceStatusID >= 0)
                    {
                        data = data.Where(x => x.IsInvoiceReceived == (InvoiceStatusID == 0 ? false : true)).ToList();
                    }

                    return PartialView("_DeletedInvoices", data.OrderByDescending(so => so.OrderID));
                }

                return PartialView("_DeletedInvoices");
            }


            ViewBag.Branch = db.tbl_Branch.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x=>x.IsDeleted!=true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.SalePerson = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.SalePerson }).ToList();
            return View();
        }

        public JsonResult ChangeInvoiceStatus(int OrderID, int Status)
        {
            if (OrderID > 0)
            {
                using (var t = db.Database.BeginTransaction())
                {
                    var invoice = db.tbl_SalesOrder.Where(x => x.OrderID == OrderID && x.IsDeleted != true).FirstOrDefault();
                    if (invoice != null)
                    {
                        if (Status == 1)
                        {
                            invoice.IsInvoiceReceived = true;

                        }
                        else
                        {
                            invoice.IsInvoiceReceived = false;

                        }

                        db.Entry(invoice).State = EntityState.Modified;
                        db.SaveChanges();
                        t.Commit();
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                }
            }


            return Json(-1, JsonRequestBehavior.AllowGet);

        }

        public JsonResult RevertInvoicePayment(int OrderID)
        {
            try
            {
                if (OrderID > 0)
                {

                    using (var t = db.Database.BeginTransaction())
                    {

                        var invoice = db.tbl_SalesOrder.Where(x => x.OrderID == OrderID && x.PaymentStatus.ToLower().Equals("paid") && x.IsPaid == true && x.IsDeleted != true).FirstOrDefault();
                        if (invoice != null)
                        {

                            var c = db.RevertInvoicePayment(invoice.OrderID, 3, com.GetUserID());//3 = Sales


                            invoice.PaymentStatus = "UnPaid";
                            invoice.IsPaid = false;
                            invoice.TotalPaid = 0;

                            db.Entry(invoice).State = EntityState.Modified;

                            UserActions.MapActions(com.GetUserID(), "Reverted Invoice Payment For Sales Invoice: " + OrderID.ToString());

                            db.SaveChanges();
                            t.Commit();
                            return Json(1, JsonRequestBehavior.AllowGet);

                        }
                    }
                }


                return Json(-1, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
          

        }
        public ActionResult EditSaleInvoice(int id, string InvoiceNo)
        {

            ViewBag.InvoiceNo = InvoiceNo;
            ViewBag.OrderID = id;
            GetViewBagValues();
            return View();
        }

        public JsonResult GetEditInvoiceData(int orderID)
        {
            dynamic data = null;

            var sale = db.tbl_SalesOrder.Where(x => x.OrderID == orderID && x.IsDeleted != true).FirstOrDefault();
            if (sale != null)
            {
                var challansDD = db.GetDeliveryChallanCustomerWise(sale.AccountID).Select(a => new
                {
                    Value = a.ChallanID,
                    Name = "DC-" + a.SOID
                }).ToList();

                var chIDs = db.tbl_ChallanIDs.Where(x=>x.OrderID== orderID && x.isDeleted!= true).Select(y=>new { ChallanID = y.ChallanID ,Challan ="DC-"+ db.tbl_Challan.Where(t=>t.ChallanID == y.ChallanID).Select(x=>x.SOID).FirstOrDefault()}).ToList();
              
                if(chIDs!=null && chIDs.Count > 0)
                {
                    foreach(var i in chIDs)
                    {
                        challansDD.Add(new
                        {
                            Value = i.ChallanID??0,
                            Name = i.Challan
                        });
                    }
                }
                data = new
                {
                    challansDD = challansDD,
                    chIDs = chIDs,
                    taxAmount = sale.VAT,
                    taxPercent = sale.TaxPercent,
                    discAmount = sale.DiscountAmount,
                    discPercent = sale.DiscountPercent,
                    AccountID = sale.AccountID,
                    SalePersonAccID = sale.SalePersonAccID,
                    OrderID = orderID,
                    SalesDate = sale.SalesDate,
                    payType = sale.PaymentTypeID,
                    payStatus = sale.PaymentStatus,
                    fsc_cert = sale.FSC_Cert??"",
                    grs_cert = sale.GRS_Cert??"",
                    oeko_tex_cert = sale.OEKO_TEX_Cert??"",
                    HSCode = sale.HSCode??string.Empty,
                    Commission = sale.SalePersonCommissionAmount,
                };
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Create()
        {
            GetViewBagValues();
            return View();
        }

        public JsonResult SaveOrder(tbl_SalesOrder model, List<Models.DTO.StockLog> modelStockLog, bool? isQuote, int? QuoteOrderID, int? bankAccId, int? OrderTypeID, bool fsc_Cert, bool grs_Cert, bool oeko_tex_Cert)
        {

            int branchId = model.BranchID;
            int userID = com.GetUserID();
            model.AddBy = userID;
            branchId = model.BranchID;

            POSOman.Models.BLL.SalesOrder sales = new Models.BLL.SalesOrder();
            var orderID = sales.createSales(model, modelStockLog, isQuote, QuoteOrderID, bankAccId, branchId, OrderTypeID, model.ChallanIDs, fsc_Cert, grs_Cert, oeko_tex_Cert,userID);
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Sales".ToString());

            //if (model.ChallanIDs != null)
            //{
            //    foreach (var item in model.ChallanIDs)
            //    {
            //        tbl_ChallanIDs ch = new tbl_ChallanIDs();

            //        ch.OrderID = Convert.ToInt32(orderID);
            //        ch.ChallanID = item;
            //        ch.AddOn = Helper.PST();
            //        ch.isDeleted = false;

            //        db.tbl_ChallanIDs.Add(ch);
            //    }

            //    db.SaveChanges();
            //}

            var custInvoiceType = db.tbl_Customer.Where(x => x.AccountID == model.AccountID).Select(x => x.InvoiceType).FirstOrDefault();
            var obj = new { OrderID = Convert.ToInt32(orderID), InvoiceType = Convert.ToInt32(custInvoiceType) };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveEditedSaleInvoiceOrder(tbl_SalesOrder model, List<Models.DTO.StockLog> modelStockLog, bool? isQuote, int? QuoteOrderID, int? bankAccId, int? OrderTypeID, bool fsc_Cert, bool grs_Cert, bool oeko_tex_Cert)
        {

            int branchId = model.BranchID;
            int userID = com.GetUserID();
            model.AddBy = userID;
            branchId = model.BranchID;

            POSOman.Models.BLL.SalesOrder sales = new Models.BLL.SalesOrder();
            var orderID = sales.editSales(model, modelStockLog, isQuote, QuoteOrderID, bankAccId, branchId, OrderTypeID, model.ChallanIDs, fsc_Cert, grs_Cert, oeko_tex_Cert, userID);
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Edit Sales".ToString());

            var custInvoiceType = db.tbl_Customer.Where(x => x.AccountID == model.AccountID).Select(x => x.InvoiceType).FirstOrDefault();
            var obj = new { OrderID = Convert.ToInt32(orderID), InvoiceType = Convert.ToInt32(custInvoiceType) };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult SaleInvoiceDelete(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_SalesOrder model = db.tbl_SalesOrder.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);


        }

        [HttpPost, ActionName("SaleInvoiceDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                //bool isExist = db.tbl_ChallanIDs.Any(p => p.OrderID == id && p.isDeleted !=true);
                //if (isExist)
                //{
                //    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                //    tbl_SalesOrder model = db.tbl_SalesOrder.Find(id);
                //    return View(model);
                //}
                //else
                //{
                DeleteOrders deleteInvoice = new DeleteOrders();

                int status = deleteInvoice.DeleteSaleInvoice(id, Convert.ToInt32(Session["LoginUserID"]));
                if (status > 0)
                {
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError(String.Empty, "Some error occured");
                tbl_SalesOrder model = db.tbl_SalesOrder.Find(id);
                return View(model);
                // }
            }
            catch (Exception err)
            {
                while (err.InnerException != null)
                {
                    err = err.InnerException;
                }
                ModelState.AddModelError(String.Empty, err.Message);
                tbl_SalesOrder model = db.tbl_SalesOrder.Find(id);
                return View(model);
            }
        }

        public FileResult Export_Old(int id, int InvoiceType, String ReportType)
        {
            LocalReport lo = new LocalReport();
            if (InvoiceType == 1)
            {
                lo.ReportPath = Server.MapPath("~/Models/Reports/saleInvoice.rdlc");
            }
            else
            {
                lo.ReportPath = Server.MapPath("~/Models/Reports/SaleInvoiceJemely.rdlc");
            }

            //string fscCert = string.Empty;// db.tbl_Certificates.Where(x => x.ID == 3).Select(x => x.CertificateInfo).FirstOrDefault();
            //ReportParameter FSC_Cert = new ReportParameter("FSC_Cert", fscCert);
            //lo.SetParameters(FSC_Cert);

            //string grsCert = string.Empty;// db.tbl_Certificates.Where(x => x.ID == 4).Select(x => x.CertificateInfo).FirstOrDefault();
            //ReportParameter GRS_Cert = new ReportParameter("GRS_Cert",grsCert);
            //lo.SetParameters(GRS_Cert);

            //string oeko_tex_Cert = string.Empty;// db.tbl_Certificates.Where(x => x.ID == 5).Select(x => x.CertificateInfo).FirstOrDefault();
            //ReportParameter OEKO_TEX_Cert = new ReportParameter("OEKO_TEX_Cert", oeko_tex_Cert);
            //lo.SetParameters(OEKO_TEX_Cert);

            ReportDataSource rs = new ReportDataSource();
            rs.Name = "DataSet1";
            List<SaleInvoiceNew_Result> data = new List<SaleInvoiceNew_Result>();

            //List<tbl_SaleDetails> getSaleDetailIDs = db.tbl_SaleDetails.Where(x => x.OrderID == id && x.Qty > 0).ToList();
            List<tbl_SaleDetails> getSaleDetailIDs = new List<tbl_SaleDetails>();
            var getChallanIDs = db.tbl_ChallanIDs.Where(x => x.OrderID == id).OrderBy(y => y.ID).Select(x => x.ChallanID).ToList();
            
            Stack<int> chids = new Stack<int>();
            if(getChallanIDs != null && getChallanIDs.Count > 0)
            {
                foreach (var chid in getChallanIDs)
                {
                    if (chid != null && chid > 0)
                    {
                        var itemChallanIds = db.tbl_ChallanDetails.Where(x => x.ChallanID == chid && x.Qty>0).OrderBy(x=>x.ProductID).Select(y=>new { y.ProductID,y.ChallanID,y.Qty}).ToList();
                        if (itemChallanIds.Count > 0)
                        {
                            foreach(var i in itemChallanIds)
                            {
                                var JoDetails = db.tbl_SaleDetails.Where(x => x.OrderID == id && x.ProductID == i.ProductID && x.Qty == i.Qty).ToList();
                                if(JoDetails!= null && JoDetails.Count > 0)
                                {
                                    getSaleDetailIDs.AddRange(JoDetails);
                                }
                                chids.Push(chid ?? 0);
                            }
                        }
                    }
                }
            }
            
            if (getSaleDetailIDs!=null && getSaleDetailIDs.Count > 0 && chids!=null && chids.Count>0 && chids.Count == getSaleDetailIDs.Count)
            {

                //foreach (tbl_SaleDetails did in getSaleDetailIDs.ToList())
                for(int i =0;i<getSaleDetailIDs.ToList().Count();i++)
                {
                    int? challanID = null;

                    if (chids.Count > 0)
                    {
                        challanID = chids.Pop();
                    }
 
                    var getSaleData = db.SaleInvoiceNew(id, challanID, getSaleDetailIDs[i].DetailID, getSaleDetailIDs[i].ProductID).FirstOrDefault();
                    if(getSaleData == null && challanID!=null)//&& !getSaleDetailIDs.Contains(getSaleDetailIDs[i]))
                    {
                        chids.Push(challanID??0);
                        getSaleDetailIDs.Add(getSaleDetailIDs[i]);
                        continue;
                    }
                    data.Add(getSaleData);
                }

            }

            else if (getSaleDetailIDs != null && getSaleDetailIDs.Count > 0 && chids != null && chids.Count > 0 && chids.Count != getSaleDetailIDs.Count)
            {

                for (int i = 0; i < getSaleDetailIDs.ToList().Count(); i++)
                {
                    int? challanID = null;

                    if (chids.Count > 0)
                    {
                        challanID = chids.Pop();
                    }

                    var getSaleData = db.SaleInvoiceNew(id, challanID, getSaleDetailIDs[i].DetailID, getSaleDetailIDs[i].ProductID).FirstOrDefault();
                    if (getSaleData == null && challanID != null)//&& !getSaleDetailIDs.Contains(getSaleDetailIDs[i]))
                    {
                       
                        chids.Push(challanID ?? 0);
                        getSaleDetailIDs.Add(getSaleDetailIDs[i]);
                        continue;
                    }
                    data.Add(getSaleData);
                }

            }

            rs.Value = data!=null ? data:null;
            lo.DataSources.Add(rs);

            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            //Response.AddHeader("Content-Disposition", "attachment; filename=SaleInvoice.pdf");

            return new FileContentResult(renderedBytes, mimeType);
        }

        public FileResult Export(int id, int InvoiceType, String ReportType)
        {
            LocalReport lo = new LocalReport();
            if (InvoiceType == 1)
            {
                lo.ReportPath = Server.MapPath("~/Models/Reports/saleInvoice.rdlc");
            }
            else
            {
                lo.ReportPath = Server.MapPath("~/Models/Reports/SaleInvoiceJemely.rdlc");
            }

            ReportDataSource rs = new ReportDataSource();
            rs.Name = "DataSet1";
            List<SaleInvoiceNew_Result> data = new List<SaleInvoiceNew_Result>();

            List<int> ExcludedJoDetailIDs = new List<int>();

            List<tbl_SaleDetails> JoDetailsList = db.tbl_SaleDetails.Where(x => x.OrderID == id && x.Qty > 0).ToList();
            List<tbl_ChallanDetails> ChDetailsList = new List<tbl_ChallanDetails>();
            var getChallanIDs = db.tbl_ChallanIDs.Where(x => x.OrderID == id).OrderBy(y => y.ID).Select(x => x.ChallanID).ToList();

            if (getChallanIDs != null && getChallanIDs.Count > 0)
            {
                ChDetailsList = db.tbl_ChallanDetails.Where(x => getChallanIDs.Contains(x.ChallanID) && x.Qty>0).OrderBy(x=>x.ChallanID).ToList();
                
                foreach(var c in ChDetailsList)
                {
                    var getJoData = JoDetailsList.Where(x => x.ProductID == c.ProductID && x.Qty == c.Qty).FirstOrDefault();
                    if (getJoData != null)
                    {
                        if (!ExcludedJoDetailIDs.Contains(getJoData.DetailID))
                        {
                            var getSaleData = db.SaleInvoiceNew(id, c.ChallanID, getJoData.DetailID, c.ProductID).FirstOrDefault();
                            if (getSaleData != null)
                            {
                                data.Add(getSaleData);
                            }
                            ExcludedJoDetailIDs.Add(getJoData.DetailID);
                        }
                        else
                        {
                            int ExcludedJoDetailId = getJoData.DetailID;
                            getJoData = JoDetailsList.Where(x => x.ProductID == c.ProductID && x.Qty == c.Qty && x.DetailID!= ExcludedJoDetailId).FirstOrDefault();
                            if (getJoData != null)
                            {
                                    var getSaleData = db.SaleInvoiceNew(id, c.ChallanID, getJoData.DetailID, c.ProductID).FirstOrDefault();
                                    if (getSaleData != null)
                                    {
                                        data.Add(getSaleData);
                                    }
                                    ExcludedJoDetailIDs.Add(getJoData.DetailID);
                            }
                        }
                    }
                }
            }

           
            rs.Value = data != null ? data : null;
            lo.DataSources.Add(rs);

            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            //Response.AddHeader("Content-Disposition", "attachment; filename=SaleInvoice.pdf");

            return new FileContentResult(renderedBytes, mimeType);
        }


    }
}