﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class SalePersonController : Controller
    {
        private dbPOS db = new dbPOS();
        Models.BLL.VendorPayment vendorPay = new VendorPayment();

        // GET: SalePerson
        public ActionResult Index()
        {
            return View(db.tbl_SalePerson.ToList());
        }

        public ActionResult Statement(int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.saleperson = db.tbl_SalePerson.Select(v => new { Value = v.AccountID, Name = v.SalePerson }).ToList();
           if (AccountID > 0)
            {
                List<GetSalePersonPaymentDateWise_Result> list = db.GetSalePersonPaymentDateWise(AccountID, fromDate, toDate, BranchID).ToList();
                if (list.Count == 0)
                {
                    return PartialView("_Statement");
                }
                else
                    return PartialView("_Statement", list);
            }

            return View();
        }

         public ActionResult DirectPayment()
        {
            try
            {
               
                ViewBag.vendor = db.tbl_SalePerson.Select(v => new { Value = v.AccountID, Name = v.SalePerson }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        public JsonResult SaveSinglePayment(Payment model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                vendorPay.SaveSalePersonSinglePayment(model, jentryLog, bankAccId, branchId);
                return Json("success");
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }

        public ActionResult Details(bool? isNew, int? id)
        {


            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //tbl_PurchaseOrder purchaseOrder = db.tbl_PurchaseOrder.Find(id);
            if (isNew == true)
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.OrderByDescending(v => v.JDetailID).Where(j => j.AccountID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_PaymentVoucher", _PaymentDetail);
            }
            else
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JDetailID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_PaymentVoucher", _PaymentDetail);
            }
        }
        
        public ActionResult GetSalePersonPaymentReportFilterWise(bool? btn, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            if (btn.HasValue)
            {
                List<GetSalePersonPayReportFilterWise_Result> stock = db.GetSalePersonPayReportFilterWise(BranchID, fromDate, toDate).ToList();

                if (stock.Count == 0)
                {
                    return PartialView("_SalePersonPayReport");
                }
                else
                {
                    return PartialView("_SalePersonPayReport", stock);
                }

            }
            return View();
        }

        // GET: SalePerson/Details/5
        public JsonResult GetSalePersonBalance(int accountID)
        {
            if (accountID > 1)
            {
                var bal = db.tbl_JDetail.Where(a => a.AccountID == accountID).Select(a => new { Cr = a.Cr, Dr = a.Dr }).ToList();
                if (bal.Count != 0)
                {
                    var result = bal.Sum(a => a.Cr) - bal.Sum(a => a.Dr);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }
       
        // GET: SalePerson/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SalePerson/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( tbl_SalePerson model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (string.IsNullOrWhiteSpace(model.SalePerson))
                    {
                        ModelState.AddModelError("SalePerson", "Required");
                        return View(model);

                    }
                    if (string.IsNullOrWhiteSpace(model.CNIC))
                    {
                        ModelState.AddModelError("CNIC", "Required");
                        return View(model);
                    }
                    if (string.IsNullOrWhiteSpace(model.Cell))
                    {
                        ModelState.AddModelError("Cell", "Required");
                        return View(model);

                    }
                    if (string.IsNullOrWhiteSpace(model.UserName))
                    {
                        ModelState.AddModelError("UserName", "Required");
                        return View(model);

                    }

                    if (!db.AspNetUsers.Any(x => x.UserName.Equals(model.UserName)))
                    {
                        Common c = new Common();
                        RegisterBindingViewModel m = new RegisterBindingViewModel();
                        m.UserName = model.UserName;
                        m.Name = model.SalePerson;
                        m.Cell = model.Cell;
                        m.Password = "123456";
                        m.ConfirmPassword = "123456";
                       var id = new AccountController().RegisterUser(m);
                        int accId = 0;
                        if (Int32.TryParse(id.ToString(),out accId) == true)
                        {
                            model.AccountID = accId;
                            model.AddedOn = Helper.PST();

                            db.tbl_SalePerson.Add(model);
                            db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ModelState.AddModelError("SalePerson", "Critical Error Occured");
                            return View();
                        }

                    }
                    else
                    {
                        ModelState.AddModelError("SalePerson", "Some Error Occured/UserName Already Exist!");
                        return View();
                    }
                    
                }

                return View(model);
            }
            catch(Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                ModelState.AddModelError("SalePerson", e.Message);
                return View(model);
            }

            
        }

        // GET: SalePerson/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_SalePerson tbl_SalePerson = db.tbl_SalePerson.Find(id);
            if (tbl_SalePerson == null)
            {
                return HttpNotFound();
            }
            return View(tbl_SalePerson);
        }

        // POST: SalePerson/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( tbl_SalePerson tbl_SalePerson)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_SalePerson).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_SalePerson);
        }

        // GET: SalePerson/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_SalePerson tbl_SalePerson = db.tbl_SalePerson.Find(id);
            if (tbl_SalePerson == null)
            {
                return HttpNotFound();
            }
            return View(tbl_SalePerson);
        }

        // POST: SalePerson/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_SalePerson tbl_SalePerson = db.tbl_SalePerson.Find(id);
            db.tbl_SalePerson.Remove(tbl_SalePerson);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
