﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class StockManagementController : Controller
    {
        private dbPOS db = new dbPOS();
        ApplicationDbContext context;
        AdjustStock adjust = new AdjustStock();
        // GET: Stock
        [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult Index(bool? btn, int? ProductID, int? VehicleCodeID, int? BranchID, string Description, string PartNo,int? OrderTypeID)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            // ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name =  v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            if (btn.HasValue)
            {
                List<GetStockAdjustmentSumFilterWise_Result> stock = db.GetStockAdjustmentSumFilterWise(ProductID, VehicleCodeID,  Description, BranchID, PartNo,OrderTypeID).ToList();

                //var data = ProductList(param, stock);
                if (stock.Count == 0)
                {
                    return PartialView("_StockFilter");
                }
                else
                    return PartialView("_StockFilter", stock);
            }
            return View();
        }
        public ActionResult StockAdjustment()
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            ViewBag.BranchID = branchId;
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            return View();
        }
        // Get Stock detail for Sock Movement
        [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public JsonResult getStockDetail(int? iProductID)
        {
            context = new ApplicationDbContext();
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
            int branchId = currentUser.BranchID;
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var role = UserManager.GetRoles(user.GetUserId());
            if (iProductID > 0)
            {
                try
                {
                    if (role[0].ToString() == "SuperAdmin")
                    {
                        var qry = db.GetStockForAdjustment(iProductID, null, null, null).ToList();
                        
                        return Json(new { qry }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var qry = db.GetStockForAdjustment(iProductID, null, null,branchId);
                       
                        return Json(new { qry }, JsonRequestBehavior.AllowGet);
                    }

                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }
            return Json("");
        }
        // Save Stock Adjustment Data
        // Receive Stock and Update Stock 
        public JsonResult SaveStockAdjustment(List<Models.DTO.StockMoveOut> modelStockLogOut)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            POSOman.Models.BLL.MoveStock moveStock = new Models.BLL.MoveStock();
            object result = adjust.Save(modelStockLogOut,userID);
            return Json(result);
        }
    }
}
