﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;

namespace POSOman.Controllers
{
    public class StickerCylinderController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: StickerCylinder
        public ActionResult Index()
        {
            var tbl_StickerCylinder = db.tbl_StickerCylinder.Where(x=>x.IsDeleted!=true).Include(t => t.tbl_StickerMachine);
            return View(tbl_StickerCylinder.ToList());
        }

        // GET: StickerCylinder/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerCylinder tbl_StickerCylinder = db.tbl_StickerCylinder.Find(id);
            if (tbl_StickerCylinder == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerCylinder);
        }

        // GET: StickerCylinder/Create
        public ActionResult Create()
        {
            ViewBag.StickerCylinderTypeMachineID = new SelectList(db.tbl_StickerMachine, "ID", "MachineName");
            return View();
        }

        // POST: StickerCylinder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,StickerCylinderTypeMachineID,CylinderType,CylinderTeeth,NoOfCylinderPresent")] tbl_StickerCylinder tbl_StickerCylinder)
        {
            if (ModelState.IsValid)
            {
                db.tbl_StickerCylinder.Add(tbl_StickerCylinder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.StickerCylinderTypeMachineID = new SelectList(db.tbl_StickerMachine, "ID", "MachineName", tbl_StickerCylinder.StickerCylinderTypeMachineID);
            return View(tbl_StickerCylinder);
        }

        // GET: StickerCylinder/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerCylinder tbl_StickerCylinder = db.tbl_StickerCylinder.Find(id);
            if (tbl_StickerCylinder == null)
            {
                return HttpNotFound();
            }
            ViewBag.StickerCylinderTypeMachineID = new SelectList(db.tbl_StickerMachine, "ID", "MachineName", tbl_StickerCylinder.StickerCylinderTypeMachineID);
            return View(tbl_StickerCylinder);
        }

        // POST: StickerCylinder/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( tbl_StickerCylinder tbl_StickerCylinder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_StickerCylinder).State = EntityState.Modified;
                db.Entry(tbl_StickerCylinder).Property(p => p.AddedOn).IsModified = false;
                //db.Entry(tbl_StickerCylinder).Property(p => p.StickerCylinderTypeMachineID).IsModified = false;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.StickerCylinderTypeMachineID = new SelectList(db.tbl_StickerMachine, "ID", "MachineName", tbl_StickerCylinder.StickerCylinderTypeMachineID);
            return View(tbl_StickerCylinder);
        }

        // GET: StickerCylinder/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerCylinder tbl_StickerCylinder = db.tbl_StickerCylinder.Find(id);
            if (tbl_StickerCylinder == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerCylinder);
        }

        // POST: StickerCylinder/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_StickerCylinder model = db.tbl_StickerCylinder.Find(id);
            if (model != null)
            {
                model.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("CylinderType", "Record not found");
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
