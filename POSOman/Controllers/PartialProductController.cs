﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Threading.Tasks;
using System.Net;
using System.Data.Entity;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace POSOman.Controllers
{
    public class PartialProductController : Controller
    {
        // GET: PartialProduct          
        public ActionResult Index()
        {
            return View();
        }
        dbPOS db = new dbPOS();
        // check Duplicates
        [HttpPost]
        public JsonResult CheckPartNO(string PartNo)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            bool isValid = false;
            if (db.tbl_Product.Any(p => p.PartNo == PartNo && p.BranchID ==  branchId))
            {
                isValid = true;
            }
            return Json(isValid);
        }
        // GET: PartialProduct/Create
        public ActionResult Create()
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            //ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName");
            //List<SelectListItem> items = new SelectList(db.tbl_VehicleModel, "VehicleID", "VehicleName").ToList();
            //items.Insert(0, (new SelectListItem { Text = "[None]", Value = "0" }));           
            
            ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode, "VehicleCodeID", "VehicleCode");
                              
            return View("_PartialProduct");
        }

        // POST: Product/Create
        [HttpPost]
        public async Task<ActionResult> Create(Models.DTO.Product prod)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
                currentUserId = User.Identity.GetUserId();
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;                
            }
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                                      .Where(y => y.Count > 0)
                                      .ToList();
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return View("_CreatePartial", prod);
                
            }
            bool isValid = false;
            
           
            var newProdID = 1;
            var tmp = db.tbl_Product.OrderByDescending(v => v.ProductID).FirstOrDefault();
            if (tmp != null)
            {
                newProdID = tmp.ProductID + 1;
            }

            if (string.IsNullOrWhiteSpace(prod.BarCode))
            {
                string source = prod.PartNo;
                string result = string.Concat(source.Where(c => !char.IsWhiteSpace(c)));
                Random ran = new Random();
                var randomNo = ran.Next();
                string barCode = (newProdID).ToString() + randomNo.ToString();
                if (barCode.Length > 13)
                {
                    barCode = barCode.Substring(0, 13);
                }
                else if (barCode.Length < 13)
                {
                    Random ran1 = new Random();
                    var randomNo1 = ran1.Next();
                    barCode = (barCode + randomNo1.ToString()).Substring(0, 13);
                }
                prod.BarCode = barCode;
            }
            if (ModelState.IsValid)
            {

                if (string.IsNullOrWhiteSpace(prod.PartNo))
                {
                    isValid = true;

                }

                else if (db.tbl_Product.Any(p => p.PartNo == prod.PartNo && p.VehicleCodeID == prod.VehicleCodeID))
                {
                    isValid = true;
                }
                else if (db.tbl_Product.Any(p => p.BarCode == prod.BarCode))
                {
                    isValid = true;
                }
                else
                {
                    isValid = false;
                    tbl_Product product = MaptoModel(prod);
                    product.Addby = userID;
                    product.AddOn = DateTime.UtcNow.AddHours(5);
                    product.UserID = currentUserId;
                    product.MinorDivisor = 1000;
                    product.BranchID = branchId;
                    product.UserID = User.Identity.GetUserId();
                    product.QtyPerUnit = 1;
                    product.UnitPerCarton = 1;
                    product.Addby = userID;
                    product.AddOn = DateTime.UtcNow.AddHours(5);
                    product.isActive = 1;
                    db.tbl_Product.Add(product);
                    var task = db.SaveChangesAsync();
                    await task;

                    if (task.Exception != null)
                    {
                        ModelState.AddModelError("", "Unable to add the Product");
                        return View("_CreatePartial", product);
                    }
                }
            }                
            return Json(isValid);            
        }
        private tbl_Product MaptoModel(Models.DTO.Product product)
        {
            tbl_Product prod = new tbl_Product()
            {
                PartNo = product.PartNo,
                Description = product.Description,
                BarCode = product.BarCode,               
                VehicleCodeID = product.VehicleCodeID,
                SaleRate = product.SaleRate               
            };

            return prod;
        }
       
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Product tbl_Product = db.tbl_Product.Find(id);            
            if (tbl_Product == null)
            {
                return HttpNotFound();
            }
            //ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Product.Addby);
            
            ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode, "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
           
            return PartialView("_EditProduct",tbl_Product);
        }
        [HttpPost]
        public async Task<ActionResult> Edit(tbl_Product prod)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }            
            if (!ModelState.IsValid)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return View(Request.IsAjaxRequest() ? "_EditPartial" : "Edit", prod);
            }
            bool isValid = false;
            if (db.tbl_Product.Any(p => p.PartNo == prod.PartNo && p.BranchID== branchId && p.ProductID != prod.ProductID && p.VehicleCodeID == prod.VehicleCodeID))
            {
                isValid = true;
            }
            else
            {
                bool isExist = db.tbl_Stock.Any(p => p.ProductID == prod.ProductID);
                bool isExist1 = db.tbl_PODetails.Any(p => p.ProductID == prod.ProductID);
                if (isExist || isExist1)
                {
                    isValid = false;
                    prod.UpdateOn = DateTime.UtcNow.AddHours(5);
                    prod.UpdateBy = userID;
                    db.Entry(prod).State = EntityState.Modified;
                    db.Entry(prod).Property(p => p.PartNo).IsModified = false;
                    db.Entry(prod).Property(p => p.VehicleCodeID).IsModified = false;
                    db.Entry(prod).Property(p => p.Addby).IsModified = false;
                    db.Entry(prod).Property(p => p.UserID).IsModified = false;
                    db.Entry(prod).Property(p => p.BranchID).IsModified = false;
                    db.Entry(prod).Property(p => p.AddOn).IsModified = false;
                    db.Entry(prod).Property(p => p.rowguid).IsModified = false;
                }
                else
                {
                    isValid = false;
                    prod.UpdateOn = DateTime.UtcNow.AddHours(5).Date;
                    db.Entry(prod).State = EntityState.Modified;
                    db.Entry(prod).Property(p => p.Addby).IsModified = false;
                    db.Entry(prod).Property(p => p.UserID).IsModified = false;
                    db.Entry(prod).Property(p => p.BranchID).IsModified = false;
                    db.Entry(prod).Property(p => p.AddOn).IsModified = false;
                    db.Entry(prod).Property(p => p.rowguid).IsModified = false;
                }
                var task = db.SaveChangesAsync();
                await task;

                if (task.Exception != null)
                {
                    ModelState.AddModelError("", "Unable to update the Product!");
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return View(Request.IsAjaxRequest() ? "_EditPartial" : "Edit", prod);
                }
            }
            //if (Request.IsAjaxRequest())
            //{
            //    return Content("success");
            //}
            return Json(isValid);
            //return Content("success");

        }        
    }
}
