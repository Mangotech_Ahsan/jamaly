﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.BLL;
using POSOman.Models.DTO;

namespace POSOman.Controllers
{
    public class MachinePlateController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        Common com = new Common();

        #region Machine Region

        public JsonResult GetImpression(decimal Sheets)
        {
            try
            {
                var getImpressions = db.tbl_Impression.Select(x => new { x.ID, x.ImpressionUnit, x.StartRange, x.EndRange }).ToList();
                if (getImpressions.Count > 0)
                {
                    foreach (var i in getImpressions)
                    {
                        if (Sheets >= i.StartRange && Sheets <= i.EndRange)
                        {
                            return Json(i.ImpressionUnit, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json("Impression Does Not Exist For The Given Sheets", JsonRequestBehavior.AllowGet);
                }
                return Json("Not Found", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);

            }
        }
        // GET: MachinePlate
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            var tbl_Card_Quality = db.tbl_Card_Quality.Include(t => t.tbl_CardType);
            return View(db.tbl_MachinePlate.Where(x => x.IsDeleted != true).ToList());
        }

        // GET: MachinePlate/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_MachinePlate tbl_MachinePlate = db.tbl_MachinePlate.Find(id);
            if (tbl_MachinePlate == null)
            {
                return HttpNotFound();
            }
            return View(tbl_MachinePlate);
        }

        // GET: MachinePlate/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MachinePlate/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_MachinePlate model)
        {
            if (ModelState.IsValid)
            {
                model.AddedOn = Helper.PST();
                model.IsDeleted = false;
                db.tbl_MachinePlate.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: MachinePlate/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_MachinePlate model = db.tbl_MachinePlate.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: MachinePlate/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_MachinePlate model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: MachinePlate/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_MachinePlate model = db.tbl_MachinePlate.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: MachinePlate/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_MachinePlate model = db.tbl_MachinePlate.Find(id);
            if (model != null)
            {
                model.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("MachinePlate", "Record not found");
            return View(model);

        }

        #endregion

        #region Update offset costing through modal
        [HttpPost]
        public void UpdateOffsetCosting(tbl_Product model)
        {
            try
            {
                UpdateCostingModelBLL bll = new UpdateCostingModelBLL();
                bool chk = bll.updateCosting(model);

                //if(chk == true)
                //    return Json(1, JsonRequestBehavior.AllowGet);

                //return Json(-1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                //return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Offset Calculation
        public JsonResult SaveOffsetOrder(Sales model, List<Models.DTO.StockLog> modelStockLog, bool? isQuote, int? QuoteOrderID, int? bankAccId, int? OrderTypeID)
        {

            int branchId = model.BranchID;
            int userID = com.GetUserID();
            model.AddBy = userID;
            branchId = model.BranchID;

            SalesOrder sales = new SalesOrder();
            var orderID = sales.SaveOffsetSales(model, modelStockLog, isQuote, QuoteOrderID, bankAccId, branchId, OrderTypeID);
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Offset Order".ToString());

            return Json(orderID);
        }
        public ActionResult CalculateOffSet()
        {
            ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
            ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
            ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
            ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

            return View();
        }

        [HttpPost]
        public ActionResult CalculateOffSet(tbl_Product model)
        {

            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    if (string.IsNullOrWhiteSpace(model.PartNo))
                    {
                        ModelState.AddModelError("PartNo", "Description Required");
                        ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
                        ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                        ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                        ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if ((model.ProductID > 0 && db.tbl_Product.Any(x => x.PartNo.ToLower().Trim().Equals(model.PartNo.ToLower().Trim()) && x.ProductID != model.ProductID)) || (model.ProductID <= 0 && db.tbl_Product.Any(x => x.PartNo.ToLower().Trim().Equals(model.PartNo.ToLower().Trim()))))
                    {
                        ModelState.AddModelError("PartNo", "Duplication Occured");
                        ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
                        ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                        ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                        ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if (model.UnitCodeID <= 0 || model.UnitCodeID == null)
                    {
                        ModelState.AddModelError("UnitCodeID", "Not Valid");
                        ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
                        ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                        ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                        ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if (model.MarkedupPrice <= 0)
                    {
                        ModelState.AddModelError("PartNo", "Cost Not Valid");
                        ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
                        ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                        ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                        ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if (model.QtyPerUnit <= 0)
                    {
                        ModelState.AddModelError("PartNo", "Qty Not Valid");
                        ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
                        ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                        ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                        ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }

                    
                    model.AddOn = Helper.PST();
                    //model.MarkedupPrice = model.CostPerPc;
                    //if (model.LaminationCost <= 0)
                    //{
                    model.OffsetTypeID = 1;
                    //}
                    model.VehicleCodeID = 1;
                    model.DepartmentID = 1;
                    model.UnitCode = db.tbl_UnitCode.Where(x => x.UnitCodeID == model.UnitCodeID).Select(x => x.UnitCode).FirstOrDefault();
                    model.UnitPerCarton = 1;
                    model.PartNo = model.PartNo.Trim();

                    if (model.CardQuality != null && model.CardQuality > 0)
                    {
                        var getCardQuality = db.tbl_Card_Quality.Where(x => x.ID == model.CardQuality).FirstOrDefault();
                        if (getCardQuality != null && getCardQuality.Rate > 0 && model.RatePerKG > 0 && getCardQuality.Rate != model.RatePerKG)
                        {
                            getCardQuality.Rate = model.RatePerKG;
                            getCardQuality.UpdatedOn = Helper.PST();
                            db.Entry(getCardQuality).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    //model.UnitCode = "PCS";
                    if (model.ProductID <= 0)
                    {
                        model.PartNo = model.PartNo.Trim();
                        db.tbl_Product.Add(model);
                        db.SaveChanges();
                    }
                    else
                    {
                        db.Entry(model).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    //var id = db.tbl_Product.Where(x => x.RawProductID == model.CardQuality).Select(x => x.ProductID).FirstOrDefault();
                    //if (id > 0)
                    //{
                    //    tbl_StockIssuance si = new tbl_StockIssuance();
                    //    si.ItemID = model.ProductID;
                    //    si.DepartmentID = model.DepartmentID;
                    //    si.ProductID = id;// Raw Material
                    //    si.AddedOn = Helper.PST();
                    //    db.tbl_StockIssuance.Add(si);
                    //    db.SaveChanges();
                    //}
                    t.Commit();

                    return RedirectToAction("PackageReport", "Product", new { target = "_blank", OffsetTypeID = 1, deptID = 1, ProductID = model.ProductID, ReportType = "PDF" });

                }

            }
            catch (Exception e)
            {
                ModelState.AddModelError("PartNo", e.Message);
                ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
                ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                return View(model);
            }
        }

        public ActionResult EditCalculateOffSet(int ItemID, bool? IsView)
        {
            ViewBag.ItemID = ItemID;
            var data = db.tbl_Product.Where(x => x.ProductID == ItemID && (x.isActive != 0 || x.isActive == null)).FirstOrDefault();
            ViewBag.CardQlty = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
            ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
            ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
            ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

            if (IsView == true)
            {
                return View("ViewCalculateOffSet", data);
            }
            return View(data);
        }

        public ActionResult OffsetOrders()
        {
            var data = db.tbl_SalesOrder.Where(x => (x.IsDeleted != true || x.IsDeleted == null) && x.IsOffsetOrder == true).ToList();
            return View(data);
        }
        public ActionResult OffsetDetails(int id)
        {
            var data = db.tbl_SaleDetails.Where(x => x.OrderID == id).FirstOrDefault();
            int AccountID = data.tbl_SalesOrder.AccountID;
            int SalePersonAccID = data.tbl_SalesOrder.SalePersonAccID ?? 0;
            int CardQuality = data.CardQuality ?? 0;
            int MachineID = data.MachineID ?? 0;

            ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true && x.ID == CardQuality).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
            ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true && x.AccountID == AccountID).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
            ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true && x.AccountID == SalePersonAccID).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
            ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true && x.ID == MachineID).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();

            return View(data);
        }


        public ActionResult CreateEditOffsetAdditionalCostings()
        {
            return View();
        }
        public ActionResult CreateEditStickerAdditionalCostings()
        {
            return View();
        }
        public ActionResult CreateEditTafataAdditionalCostings()
        {
            return View();
        }

        public JsonResult SaveAdditionalCostings(tbl_AdditionalCostings model)
        {
            try
            {
                if (model != null)
                {
                    if (model.ID <= 0)
                    {
                        model.AddedOn = Helper.PST();
                        db.tbl_AdditionalCostings.Add(model);
                        db.SaveChanges();
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    else if (model.ID > 0)
                    {
                        var data = db.tbl_AdditionalCostings.Where(x => x.ID == model.ID).FirstOrDefault();
                        if (data != null)
                        {
                            data.GlossLaminationCost = model.GlossLaminationCost;
                            data.MatLaminationCost = model.MatLaminationCost;
                            data.HotLaminationCost = model.HotLaminationCost;
                            data.UVCost = model.UVCost;
                            data.SlittingCost = model.SlittingCost;
                            data.UVGlossVarnish = model.UVGlossVarnish;
                            data.UVMattVarnish = model.UVMattVarnish;
                            data.AQGlossVarnish = model.AQGlossVarnish;
                            data.AQMattVarnish = model.AQMattVarnish;
                            data.EmbosingCost = model.EmbosingCost;
                            data.OverheadExpense = model.OverheadExpense;
                            data.tr2Cost = model.tr2Cost;
                            db.Entry(data).State = EntityState.Modified;
                            db.SaveChanges();
                            return Json(1, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult FetchLaminationCosts(int? OffsetTypeID)
        {
            try
            {
                var data = db.tbl_AdditionalCostings.Where(x => x.IsDeleted != true && x.OffsetTypeID == OffsetTypeID).Select(x => new { x.ID, x.GlossLaminationCost, x.MatLaminationCost, x.SlittingCost, x.UVCost, x.HotLaminationCost, x.UVGlossVarnish, x.UVMattVarnish, x.AQGlossVarnish, x.AQMattVarnish, x.OverheadExpense }).FirstOrDefault();
                if (data != null)
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult FetchCardQualityDetails(int ID)
        {
            try
            {

                var data = db.tbl_Card_Quality.Where(x => x.ID == ID).Select(x => new { x.Rate, x.CardName, x.CardTypeID, x.UpdatedOn  }).FirstOrDefault();
                if (data != null)
                {
                    var list = new
                    {
                        Rate = data.Rate,
                        CardName = data.CardName,
                        CardTypeID = data.CardTypeID,
                        UpdatedOn = data.UpdatedOn != null ? "LU: " + Helper.GetNumberOfDays(data.UpdatedOn , Helper.PST())  : null
                    };
                    return Json(list, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult FetchRollQualityDetails(int ID)
        {
            try
            {
                var data = db.tbl_Roll.Where(x => x.ID == ID).Select(x => new { x.RollCost, RollLength = x.RollWidth, RollMeters = x.RollMeters_Length }).FirstOrDefault();
                if (data != null)
                {
                    var new_data = new { data.RollMeters, data.RollCost, data.RollLength, MaterialCostPerMeter = (data.RollCost / data.RollMeters).ToString("N" + 2) };
                    return Json(new_data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult FetchMachineDetails(int ID, int? OffsetTypeID)
        {
            try
            {
                List<object> list = new List<object>();

                var data = db.tbl_MachinePlate.Where(x => x.ID == ID).Select(x => new { x.DieCuttingCost, x.GroundCost, x.PlateCost, x.TextCost }).FirstOrDefault();
                if (data != null)
                {
                    var getData = db.tbl_AdditionalCostings.Where(x => x.IsDeleted != true && x.OffsetTypeID == OffsetTypeID).Select(x => new { x.GlossLaminationCost, x.HotLaminationCost, x.MatLaminationCost, x.SlittingCost, x.UVCost }).FirstOrDefault();
                    if (getData != null)
                    {
                        list.Add(new
                        {
                            DieCuttingCost = data.DieCuttingCost,
                            GroundCost = data.GroundCost,
                            PlateCost = data.PlateCost,
                            TextCost = data.TextCost,
                            GlossLaminationCost = getData.GlossLaminationCost,
                            MatLaminationCost = getData.MatLaminationCost,
                            HotLaminationCost = getData.HotLaminationCost,
                            UVCost = getData.UVCost,
                            SlittingCost = getData.SlittingCost
                        });

                    }
                    else
                    {
                        list.Add(new
                        {
                            DieCuttingCost = data.DieCuttingCost,
                            GroundCost = data.GroundCost,
                            PlateCost = data.PlateCost,
                            TextCost = data.TextCost,
                            GlossLaminationCost = 1,
                            MatLaminationCost = 1,
                            HotLaminationCost = 1,
                            UVCost = 1,
                            SlittingCost = 1
                        });

                    }

                    return Json(list.FirstOrDefault(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    list.Add(new
                    {
                        DieCuttingCost = 0,
                        GroundCost = 0,
                        PlateCost = 0,
                        TextCost = 0,
                        GlossLaminationCost = 0,
                        MatLaminationCost = 0,
                        HotLaminationCost = 0,
                        UVCost = 0,
                        SlittingCost = 0
                    });
                    return Json(list.FirstOrDefault(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region Cloth Offset Calculation
        public ActionResult CalculateClothOffSet()
        {
            ViewBag.RollQuality = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 1).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
            ViewBag.Splinter = db.tbl_SplinterSize.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.SplinterSize }).ToList();
            ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
            ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
            ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
            ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

            return View();
        }
        [HttpPost]
        public ActionResult CalculateClothOffSet(tbl_Product model)
        {

            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    if (string.IsNullOrWhiteSpace(model.PartNo))
                    {
                        ModelState.AddModelError("PartNo", "Description Required");
                        ViewBag.RollQuality = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 1).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
                        ViewBag.Splinter = db.tbl_SplinterSize.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.SplinterSize }).ToList();
                        ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                        ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                        ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if ((model.ProductID > 0 && db.tbl_Product.Any(x => x.PartNo.ToLower().Trim().Equals(model.PartNo.ToLower().Trim()) && x.ProductID != model.ProductID)) || (model.ProductID <= 0 && db.tbl_Product.Any(x => x.PartNo.ToLower().Trim().Equals(model.PartNo.ToLower().Trim()))))
                    {
                        ModelState.AddModelError("PartNo", "Duplication Occured");
                        ViewBag.RollQuality = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 1).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
                        ViewBag.Splinter = db.tbl_SplinterSize.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.SplinterSize }).ToList();
                        ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                        ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                        ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if (model.UnitCodeID <= 0 || model.UnitCodeID == null)
                    {
                        ModelState.AddModelError("UnitCodeID", "Not Valid");
                        ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
                        ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                        ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                        ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if (model.MarkedupPrice <= 0)
                    {
                        ModelState.AddModelError("PartNo", "Cost Not Valid");
                        ViewBag.RollQuality = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 1).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
                        ViewBag.Splinter = db.tbl_SplinterSize.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.SplinterSize }).ToList();
                        ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                        ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                        ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if (model.QtyPerUnit <= 0)
                    {
                        ModelState.AddModelError("PartNo", "Qty Not Valid");
                        ViewBag.RollQuality = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 1).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
                        ViewBag.Splinter = db.tbl_SplinterSize.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.SplinterSize }).ToList();
                        ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                        ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                        ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }

                    model.AddOn = Helper.PST();
                    //model.MarkedupPrice = model.CostPerPc;
                    //if(model.LaminationCost <= 0)
                    //{
                    model.OffsetTypeID = 3;
                    model.DepartmentID = 1;
                    //}
                    model.VehicleCodeID = 1;
                    model.PartNo = model.PartNo.Trim();
                    model.UnitPerCarton = 1;
                    //model.UnitCode = "PCS";
                    model.UnitCode = db.tbl_UnitCode.Where(x => x.UnitCodeID == model.UnitCodeID).Select(x => x.UnitCode).FirstOrDefault();
                    if (model.ProductID <= 0)
                    {

                        db.tbl_Product.Add(model);
                        db.SaveChanges();
                    }
                    else
                    {
                        db.Entry(model).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    //var id = db.tbl_Product.Where(x => x.RawProductID == model.RollID).Select(x => x.ProductID).FirstOrDefault();
                    //if (id > 0)
                    //{
                    //    tbl_StockIssuance si = new tbl_StockIssuance();
                    //    si.ItemID = model.ProductID;
                    //    si.DepartmentID = model.DepartmentID;
                    //    si.ProductID = id;// Raw Material
                    //    si.AddedOn = Helper.PST();
                    //    db.tbl_StockIssuance.Add(si);
                    //    db.SaveChanges();
                    //}

                    t.Commit();
                    //return RedirectToAction("Index", "Product");
                    return RedirectToAction("PackageReport", "Product", new { OffsetTypeID = 3, deptID = 1, ProductID = model.ProductID, ReportType = "PDF" });

                }

            }
            catch (Exception e)
            {
                ModelState.AddModelError("PartNo", e.Message);
                ViewBag.RollQuality = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 1).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
                ViewBag.Splinter = db.tbl_SplinterSize.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.SplinterSize }).ToList();
                ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                return View(model);
            }
        }

        public ActionResult EditCalculateClothOffSet(int ItemID, bool? IsView)
        {
            ViewBag.ItemID = ItemID;
            var data = db.tbl_Product.Where(x => x.ProductID == ItemID && (x.isActive != 0 || x.isActive == null)).FirstOrDefault();
            ViewBag.RollQuality = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 1).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
            ViewBag.Splinter = db.tbl_SplinterSize.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.SplinterSize }).ToList();
            ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
            ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
            ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
            ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

            if (IsView == true)
            {
                return View("ViewCalculateClothOffSet", data);
            }
            return View(data);
        }


        public ActionResult ClothOffsetDetails(int id)
        {
            var data = db.tbl_SaleDetails.Where(x => x.OrderID == id).FirstOrDefault();
            int AccountID = data.tbl_SalesOrder.AccountID;
            int CardQuality = data.CardQuality ?? 0;
            int SalePersonAccID = data.tbl_SalesOrder.SalePersonAccID ?? 0;
            int MachineID = data.MachineID ?? 0;
            int RollID = data.RollID ?? 0;
            int SplinterID = data.SplinterID ?? 0;

            ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true && x.ID == CardQuality).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
            ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true && x.AccountID == SalePersonAccID).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
            ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true && x.AccountID == AccountID).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
            ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true && x.ID == MachineID).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
            ViewBag.RollQuality = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ID == RollID).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
            ViewBag.Splinter = db.tbl_SplinterSize.Where(x => x.IsDeleted != true && x.ID == SplinterID).Select(x => new { Value = x.ID, Name = x.SplinterSize }).ToList();

            return View(data);
        }

        public ActionResult AddSplinterSize()
        {
            return View();

        }

        [HttpPost]
        public ActionResult AddSplinterSize(tbl_SplinterSize model)
        {
            try
            {
                if (model.SplinterSize <= 0)
                {
                    ModelState.AddModelError("SplinterSize", "Invalid/Required");
                    return View(model);

                }
                model.AddedOn = Helper.PST();
                db.tbl_SplinterSize.Add(model);
                db.SaveChanges();
                return RedirectToAction("SplinterIndex");

            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;

                }

                ModelState.AddModelError("SplinterSize", e.Message);
                return View(model);
            }

        }

        public ActionResult SplinterIndex()
        {
            var data = db.tbl_SplinterSize.Where(x => x.IsDeleted != true).ToList();
            return View(data);
        }

        // GET: MachinePlate/Edit/5
        public ActionResult EditSplinter(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_SplinterSize model = db.tbl_SplinterSize.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: MachinePlate/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSplinter(tbl_SplinterSize model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("SplinterIndex");
            }
            return View(model);
        }

        // GET: MachinePlate/Delete/5
        public ActionResult DeleteSplinter(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_SplinterSize model = db.tbl_SplinterSize.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: MachinePlate/Delete/5
        [HttpPost, ActionName("DeleteSplinter")]
        [ValidateAntiForgeryToken]
        public ActionResult SplinterDeleteConfirmed(int id)
        {
            tbl_SplinterSize model = db.tbl_SplinterSize.Find(id);
            if (model != null)
            {
                model.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("SplinterIndex");
            }

            ModelState.AddModelError("SplinterSize", "Record not found");
            return View(model);

        }
        #endregion

        #region Satin Calculation

        public ActionResult CalculateSatinOffSet()
        {
            ViewBag.Cylinder = db.tbl_Cylinder.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Cylinder }).ToList();
            ViewBag.FinishingType = db.tbl_FinishingType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.FinishingType }).ToList();
            ViewBag.Roll = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 3).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
            ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
            ViewBag.PrintingMachine = db.tbl_PrintingMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.PrintingMachine }).ToList();
            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

            return View();
        }
        [HttpPost]
        public ActionResult CalculateSatinOffSet(tbl_Product model)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    if (string.IsNullOrWhiteSpace(model.PartNo))
                    {
                        ModelState.AddModelError("PartNo", "Description Required");
                        ViewBag.Cylinder = db.tbl_Cylinder.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Cylinder }).ToList();
                        ViewBag.FinishingType = db.tbl_FinishingType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.FinishingType }).ToList();
                        ViewBag.Roll = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 3).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
                        ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
                        ViewBag.PrintingMachine = db.tbl_PrintingMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.PrintingMachine }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if ((model.ProductID > 0 && db.tbl_Product.Any(x => x.PartNo.ToLower().Trim().Equals(model.PartNo.ToLower().Trim()) && x.ProductID != model.ProductID)) || (model.ProductID <= 0 && db.tbl_Product.Any(x => x.PartNo.ToLower().Trim().Equals(model.PartNo.ToLower().Trim()))))
                    {
                        ModelState.AddModelError("PartNo", "Duplication Occured");
                        ViewBag.Cylinder = db.tbl_Cylinder.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Cylinder }).ToList();
                        ViewBag.FinishingType = db.tbl_FinishingType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.FinishingType }).ToList();
                        ViewBag.Roll = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 3).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
                        ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
                        ViewBag.PrintingMachine = db.tbl_PrintingMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.PrintingMachine }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if (model.UnitCodeID <= 0 || model.UnitCodeID == null)
                    {
                        ModelState.AddModelError("UnitCodeID", "Not Valid");
                        ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
                        ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                        ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                        ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if (model.CostPerPc <= 0)
                    {
                        ModelState.AddModelError("PartNo", "Cost Not Valid");
                        ViewBag.Cylinder = db.tbl_Cylinder.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Cylinder }).ToList();
                        ViewBag.FinishingType = db.tbl_FinishingType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.FinishingType }).ToList();
                        ViewBag.Roll = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 3).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
                        ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
                        ViewBag.PrintingMachine = db.tbl_PrintingMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.PrintingMachine }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }
                    else if (model.QtyPerUnit <= 0)
                    {
                        ModelState.AddModelError("PartNo", "Qty Not Valid");
                        ViewBag.Cylinder = db.tbl_Cylinder.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Cylinder }).ToList();
                        ViewBag.FinishingType = db.tbl_FinishingType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.FinishingType }).ToList();
                        ViewBag.Roll = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 3).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
                        ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
                        ViewBag.PrintingMachine = db.tbl_PrintingMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.PrintingMachine }).ToList();
                        ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                        ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                        return View(model);
                    }

                    model.OffsetTypeID = 4;
                    model.DepartmentID = 2;
                    model.AddOn = Helper.PST();
                    model.MarkedupPrice = model.CostPerPc;
                    model.VehicleCodeID = 1;
                    model.UnitPerCarton = 1;
                    model.UnitCode = "PCS";
                    model.PartNo = model.PartNo.Trim();
                    model.UnitCode = db.tbl_UnitCode.Where(x => x.UnitCodeID == model.UnitCodeID).Select(x => x.UnitCode).FirstOrDefault();
                    if (model.ProductID <= 0)
                    {

                        db.tbl_Product.Add(model);
                        db.SaveChanges();
                    }
                    else
                    {
                        db.Entry(model).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    t.Commit();
                    //return RedirectToAction("Index", "Product");
                    return RedirectToAction("PackageReport", "Product", new { OffsetTypeID = 4, deptID = 2, ProductID = model.ProductID, ReportType = "PDF" });

                }


            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                ModelState.AddModelError("PartNo", e.Message);
                ViewBag.Cylinder = db.tbl_Cylinder.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Cylinder }).ToList();
                ViewBag.FinishingType = db.tbl_FinishingType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.FinishingType }).ToList();
                ViewBag.Roll = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 3).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
                ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
                ViewBag.PrintingMachine = db.tbl_PrintingMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.PrintingMachine }).ToList();
                ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                return View(model);
            }

        }
        public ActionResult EditCalculateSatinOffSet(int ItemID, bool? IsView)
        {
            ViewBag.ItemID = ItemID;
            var data = db.tbl_Product.Where(x => x.ProductID == ItemID && (x.isActive != 0 || x.isActive == null)).FirstOrDefault();
            ViewBag.Cylinder = db.tbl_Cylinder.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Cylinder }).ToList();
            ViewBag.FinishingType = db.tbl_FinishingType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.FinishingType }).ToList();
            ViewBag.Roll = db.tbl_Roll.Where(x => x.IsDeleted != true && x.ForOffsetTypeID == 3).Select(x => new { Value = x.ID, Name = x.Roll }).ToList();
            ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 2).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
            ViewBag.PrintingMachine = db.tbl_PrintingMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.PrintingMachine }).ToList();
            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

            if (IsView == true)
            {
                return View("ViewCalculateSatinOffSet", data);
            }
            return View(data);
        }

        [HttpGet]
        public JsonResult FetchCylinderData(int ID)
        {
            try
            {
                var data = db.tbl_Cylinder.Where(x => x.ID == ID).Select(x => new { x.CylinderLength, x.NoOfCylinders, x.PrintingMachineID }).FirstOrDefault();
                if (data != null)
                {
                    //var new_data = new { data.RollMeters, data.RollCost, data.RollLength, MaterialCostPerMeter = (data.RollCost / data.RollMeters).ToString("N" + 2) };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult FetchPrintingMachineData(int ID)
        {
            try
            {
                var data = db.tbl_PrintingMachine.Where(x => x.ID == ID).Select(x => new { x.RatesPerRoll }).FirstOrDefault();
                if (data != null)
                {
                    var cylinderDD = db.tbl_Cylinder.Where(x => x.PrintingMachineID == ID).Select(x => new { Value = x.ID, Name = x.Cylinder }).ToList();
                    List<object> list = new List<object>();
                    list.Add(new
                    {
                        data = data,
                        cylinderDD = cylinderDD
                    });
                    //var new_data = new { data.RollMeters, data.RollCost, data.RollLength, MaterialCostPerMeter = (data.RollCost / data.RollMeters).ToString("N" + 2) };
                    return Json(list, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult FetchFinishingData(int ID)
        {
            try
            {

                var data = db.tbl_FinishingType.Where(x => x.ID == ID).Select(x => new { x.RatesPerPC }).FirstOrDefault();
                if (data != null)
                {
                    //var new_data = new { data.RollMeters, data.RollCost, data.RollLength, MaterialCostPerMeter = (data.RollCost / data.RollMeters).ToString("N" + 2) };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult FetchBlockData(int ID)
        {
            try
            {

                var data = db.tbl_BlockType.Where(x => x.ID == ID).Select(x => new { x.RatesPerSqInch }).FirstOrDefault();
                if (data != null)
                {
                    //var new_data = new { data.RollMeters, data.RollCost, data.RollLength, MaterialCostPerMeter = (data.RollCost / data.RollMeters).ToString("N" + 2) };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult FetchRollQualityData(int ID)
        {
            try
            {
                var data = db.tbl_Roll.Where(x => x.ID == ID).Select(x => new { x.RollCost, RollLength = x.RollWidth, RollMeters = x.RollMeters_Length, SatinRibbonCost = x.SatinRibbonCost }).FirstOrDefault();
                if (data != null)
                {
                    //var new_data = new { data.RollMeters, data.RollCost, data.RollLength, MaterialCostPerMeter = (data.RollCost / data.RollMeters).ToString("N" + 2) };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }


        public ActionResult SatinOffsetOrders()
        {
            var data = db.tbl_SalesOrder.Where(x => (x.IsDeleted != true || x.IsDeleted == null) && x.IsOffsetOrder == true).ToList();
            return View(data);
        }
        public ActionResult SatinOffsetDetails(int id)
        {
            var data = db.tbl_SaleDetails.Where(x => x.OrderID == id).FirstOrDefault();
            int AccountID = data.tbl_SalesOrder.AccountID;
            int SalePersonAccID = data.tbl_SalesOrder.SalePersonAccID ?? 0;
            int CardQuality = data.CardQuality ?? 0;
            int MachineID = data.MachineID ?? 0;

            ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true && x.ID == CardQuality).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
            ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true && x.AccountID == AccountID).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
            ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true && x.AccountID == SalePersonAccID).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
            ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true && x.ID == MachineID).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();

            return View(data);
        }

        #endregion

        #region Sticker Calculation
        public JsonResult GetStickerMachineDetails(int id)
        {
            try
            {
                if (id > 0)
                {
                    var data = db.tbl_StickerMachine.Where(x => x.ID == id).Select(x => new { x.ID, x.TextColorRate, x.GroundColorRate, x.LaborCost, x.StickerMachineTypeID }).FirstOrDefault();
                    if (data != null)
                    {
                        List<object> cylList = new List<object>();
                        cylList.Add(new
                        {
                            Value = "",
                            Name = "Select"
                        });
                        var cylinders = db.tbl_StickerCylinder.Where(x => x.tbl_StickerMachine.ID == data.ID && x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CylinderType }).ToList();

                        if (cylinders.Count > 0)
                        {
                            foreach (var i in cylinders)
                            {
                                cylList.Add(new
                                {
                                    Value = i.Value,
                                    Name = i.Name
                                });
                            }
                        }
                        List<object> list = new List<object>();
                        list.Add(new
                        {
                            data = data,
                            cylinders = cylList
                        });
                        return Json(list, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetStickerDieCuttingDetails(int id)
        {
            try
            {
                if (id > 0)
                {
                    var data = db.tbl_StickerDieCutting.Where(x => x.ID == id).Select(x => new { x.ID, x.Rate }).FirstOrDefault();
                    if (data != null)
                    {
                        return Json(data, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetStickerRibbonDetails(int id)
        {
            try
            {
                if (id > 0)
                {
                    var data = db.tbl_StickerRibbon.Where(x => x.ID == id).Select(x => new { x.ID, x.Rate, x.RibbonTypeID, x.RollCost, x.RollLengthInM, x.RollWidthInMM }).FirstOrDefault();
                    if (data != null)
                    {
                        return Json(data, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetAdditionalCostingDetails(int? OffsetTypeID)
        {
            try
            {

                var data = db.tbl_AdditionalCostings.Where(x => x.IsDeleted != true && x.OffsetTypeID == OffsetTypeID).Select(x => new { x.ID, x.OverheadExpense, x.GlossLaminationCost, x.MatLaminationCost, x.HotLaminationCost, x.UVMattVarnish, x.UVGlossVarnish, x.AQGlossVarnish, x.SlittingCost, x.UVCost, x.AQMattVarnish, x.EmbosingCost, x.tr2Cost }).FirstOrDefault();
                if (data != null)
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }

                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetStickerCylinderDetails(int id)
        {
            try
            {
                if (id > 0)
                {
                    var data = db.tbl_StickerCylinder.Where(x => x.ID == id).Select(x => new { x.ID, x.NoOfCylinderPresent, x.CylinderTeeth }).FirstOrDefault();
                    if (data != null)
                    {
                        return Json(data, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetStickerQualityDetails(int id)
        {
            try
            {
                if (id > 0)
                {
                    var data = db.tbl_StickerQuality.Where(x => x.ID == id).Select(x => new { x.ID, x.GSM, x.Rate,x.UpdatedOn }).FirstOrDefault();
                    if (data != null)
                    {
                        var list = new
                        {
                            Rate = data.Rate,
                            GSM = data.GSM,
                            ID = data.ID,
                            UpdatedOn = data.UpdatedOn != null ? "LU: " + Helper.GetNumberOfDays(data.UpdatedOn, Helper.PST()) : null
                        };
                        return Json(list, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult CalculateStickerOffSet()
        {
            ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.MachineType = db.tbl_StickerMachineType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Type }).ToList();
            ViewBag.StickerMachine = db.tbl_StickerMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachineName }).ToList();
            ViewBag.StickerCylinder = db.tbl_StickerCylinder.Select(x => new { Value = x.ID, Name = x.CylinderType }).ToList();
            ViewBag.StickerQuality = db.tbl_StickerQuality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Quality }).ToList();
            ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
            ViewBag.DieCutting = db.tbl_StickerDieCutting.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.DieCutting }).ToList();
            ViewBag.StickerRibbonFoil = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 1).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            ViewBag.StickerRibbonThermal = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 2).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            ViewBag.StickerRibbonSatin = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 3).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            ViewBag.StickerRibbonRFID = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 4).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

            return View();
        }

        [HttpPost]
        public ActionResult CalculateStickerOffSet(tbl_Product model)
        {

            try
            {
                if (string.IsNullOrWhiteSpace(model.PartNo))
                {
                    ModelState.AddModelError("PartNo", "Description Required");
                    ViewBag.MachineType = db.tbl_StickerMachineType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Type }).ToList();
                    ViewBag.StickerMachine = db.tbl_StickerMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachineName }).ToList();
                    ViewBag.StickerCylinder = db.tbl_StickerCylinder.Select(x => new { Value = x.ID, Name = x.CylinderType }).ToList();
                    ViewBag.StickerQuality = db.tbl_StickerQuality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Quality }).ToList();
                    ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
                    ViewBag.DieCutting = db.tbl_StickerDieCutting.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.DieCutting }).ToList();
                    ViewBag.StickerRibbonFoil = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 1).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonThermal = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 2).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonSatin = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 3).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonRFID = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 4).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                    ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                    return View(model);
                }
                else if ((model.ProductID > 0 && db.tbl_Product.Any(x => x.PartNo.ToLower().Trim().Equals(model.PartNo.ToLower().Trim()) && x.ProductID != model.ProductID)) || (model.ProductID <= 0 && db.tbl_Product.Any(x => x.PartNo.ToLower().Trim().Equals(model.PartNo.ToLower().Trim()))))
                {
                    ModelState.AddModelError("PartNo", "Duplication Occured");
                    ViewBag.MachineType = db.tbl_StickerMachineType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Type }).ToList();
                    ViewBag.StickerMachine = db.tbl_StickerMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachineName }).ToList();
                    ViewBag.StickerCylinder = db.tbl_StickerCylinder.Select(x => new { Value = x.ID, Name = x.CylinderType }).ToList();
                    ViewBag.StickerQuality = db.tbl_StickerQuality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Quality }).ToList();
                    ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
                    ViewBag.DieCutting = db.tbl_StickerDieCutting.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.DieCutting }).ToList();
                    ViewBag.StickerRibbonFoil = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 1).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonThermal = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 2).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonSatin = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 3).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonRFID = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 4).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                    ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                    return View(model);
                }
                else if (model.UnitCodeID <= 0 || model.UnitCodeID == null)
                {
                    ModelState.AddModelError("UnitCodeID", "Not Valid");
                    ViewBag.CardQuality = db.tbl_Card_Quality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.CardName }).ToList();
                    ViewBag.AccountID = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.Name }).ToList();
                    ViewBag.Machine = db.tbl_MachinePlate.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachinePlate }).ToList();
                    ViewBag.SalePersonAccountID = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(x => new { Value = x.AccountID, Name = x.SalePerson }).ToList();
                    ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 1).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                    ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                    return View(model);
                }
                else if (model.MarkedupPrice <= 0)
                {
                    ModelState.AddModelError("PartNo", "Cost Not Valid");
                    ViewBag.MachineType = db.tbl_StickerMachineType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Type }).ToList();
                    ViewBag.StickerMachine = db.tbl_StickerMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachineName }).ToList();
                    ViewBag.StickerCylinder = db.tbl_StickerCylinder.Select(x => new { Value = x.ID, Name = x.CylinderType }).ToList();
                    ViewBag.StickerQuality = db.tbl_StickerQuality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Quality }).ToList();
                    ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
                    ViewBag.DieCutting = db.tbl_StickerDieCutting.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.DieCutting }).ToList();
                    ViewBag.StickerRibbonFoil = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 1).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonThermal = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 2).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonSatin = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 3).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonRFID = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 4).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                    ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                    return View(model);
                }
                else if (model.QtyPerUnit <= 0)
                {
                    ModelState.AddModelError("PartNo", "Qty Not Valid");
                    ViewBag.MachineType = db.tbl_StickerMachineType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Type }).ToList();
                    ViewBag.StickerMachine = db.tbl_StickerMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachineName }).ToList();
                    ViewBag.StickerCylinder = db.tbl_StickerCylinder.Select(x => new { Value = x.ID, Name = x.CylinderType }).ToList();
                    ViewBag.StickerQuality = db.tbl_StickerQuality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Quality }).ToList();
                    ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
                    ViewBag.DieCutting = db.tbl_StickerDieCutting.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.DieCutting }).ToList();
                    ViewBag.StickerRibbonFoil = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 1).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonThermal = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 2).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonSatin = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 3).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.StickerRibbonRFID = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 4).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                    ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                    ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                    return View(model);
                }


                using (var t = db.Database.BeginTransaction())
                {
                    try
                    {

                        model.AddOn = Helper.PST();
                        //model.MarkedupPrice = model.CostPerPc;
                        //if (model.LaminationCost <= 0)
                        //{
                        model.OffsetTypeID = 5;
                        model.DepartmentID = 3;
                        //}
                        model.VehicleCodeID = 1;
                        model.PartNo = model.PartNo.Trim();
                        model.UnitPerCarton = 1;
                        model.UnitCode = db.tbl_UnitCode.Where(x => x.UnitCodeID == model.UnitCodeID).Select(x => x.UnitCode).FirstOrDefault();

                        if (model.StickerQualityID != null && model.StickerQualityID > 0)
                        {
                            var getSticker = db.tbl_StickerQuality.Where(x => x.ID == model.StickerQualityID).FirstOrDefault();
                            if (getSticker != null && getSticker.Rate > 0 && model.MaterialCostPerMeter > 0 && getSticker.Rate != model.MaterialCostPerMeter)
                            {
                                getSticker.Rate = model.MaterialCostPerMeter;
                                getSticker.UpdatedOn = Helper.PST();
                                db.Entry(getSticker).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }


                        if (model.ProductID <= 0)
                        {
                            db.tbl_Product.Add(model);
                            db.SaveChanges();
                        }
                        else
                        {
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        t.Commit();
                        return RedirectToAction("PackageReport", "Product", new { OffsetTypeID = 5, deptID = 3, ProductID = model.ProductID, ReportType = "PDF" });

                    }
                    catch (Exception e)
                    {
                        while (e.InnerException != null)
                        {
                            e = e.InnerException;
                        }
                        t.Rollback();
                        ModelState.AddModelError("PartNo", e.Message);
                        return View(model);
                    }
                }

                //List<int> rawIDs = new List<int>();
                //var st_id = db.tbl_Product.Where(x => x.RawProductID == model.StickerQualityID).Select(x => x.ProductID).FirstOrDefault();
                //if (st_id > 0)
                //{
                //    rawIDs.Add(st_id); 
                //}
                // var s_id = db.tbl_Product.Where(x => x.RawProductID == model.StickerRibbonID_S).Select(x => x.ProductID).FirstOrDefault();
                //if (s_id > 0)
                //{
                //    rawIDs.Add(s_id); 
                //}
                //var f_id = db.tbl_Product.Where(x => x.RawProductID == model.StickerRibbonID_F).Select(x => x.ProductID).FirstOrDefault();
                //if (f_id > 0)
                //{
                //    rawIDs.Add(f_id);
                //}
                //var t_id = db.tbl_Product.Where(x => x.RawProductID == model.StickerRibbonID_T).Select(x => x.ProductID).FirstOrDefault();
                //if (t_id > 0)
                //{
                //    rawIDs.Add(t_id);
                //}
                //var rf_id = db.tbl_Product.Where(x => x.RawProductID == model.StickerRibbonID_RFID).Select(x => x.ProductID).FirstOrDefault();
                //if (rf_id > 0)
                //{
                //    rawIDs.Add(rf_id);
                //}

                //if (rawIDs.Count > 0)
                //{
                //    foreach(var i in rawIDs)
                //    {
                //        tbl_StockIssuance si = new tbl_StockIssuance();
                //        si.ItemID = model.ProductID;
                //        si.DepartmentID = model.DepartmentID;
                //        si.ProductID = i;// Raw Material
                //        si.AddedOn = Helper.PST();
                //        db.tbl_StockIssuance.Add(si);
                //        db.SaveChanges();
                //    }
                //}


                //return RedirectToAction("Index", "Product");

            }
            catch (Exception e)
            {
                ModelState.AddModelError("PartNo", e.Message);
                ViewBag.MachineType = db.tbl_StickerMachineType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Type }).ToList();
                ViewBag.StickerMachine = db.tbl_StickerMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachineName }).ToList();
                ViewBag.StickerCylinder = db.tbl_StickerCylinder.Select(x => new { Value = x.ID, Name = x.CylinderType }).ToList();
                ViewBag.StickerQuality = db.tbl_StickerQuality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Quality }).ToList();
                ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
                ViewBag.DieCutting = db.tbl_StickerDieCutting.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.DieCutting }).ToList();
                ViewBag.StickerRibbonFoil = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 1).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                ViewBag.StickerRibbonThermal = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 2).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                ViewBag.StickerRibbonSatin = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 3).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                ViewBag.StickerRibbonRFID = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 4).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
                ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
                ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                return View(model);
            }
        }

        public ActionResult EditCalculateStickerOffSet(int ItemID, bool? IsView)
        {
            ViewBag.ItemID = ItemID;
            var data = db.tbl_Product.Where(x => x.ProductID == ItemID && (x.isActive != 0 || x.isActive == null)).FirstOrDefault();
            ViewBag.DepartmentID = db.tbl_Department.Where(x => x.IsDeleted != true && x.ID == 3).Select(x => new { Value = x.ID, Name = x.Department }).ToList();
            ViewBag.MachineType = db.tbl_StickerMachineType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Type }).ToList();
            ViewBag.StickerMachine = db.tbl_StickerMachine.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.MachineName }).ToList();
            ViewBag.StickerCylinder = db.tbl_StickerCylinder.Select(x => new { Value = x.ID, Name = x.CylinderType }).ToList();
            ViewBag.StickerQuality = db.tbl_StickerQuality.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Quality }).ToList();
            ViewBag.BlockType = db.tbl_BlockType.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.BlockType }).ToList();
            ViewBag.DieCutting = db.tbl_StickerDieCutting.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.DieCutting }).ToList();
            ViewBag.StickerRibbonFoil = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 1).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            ViewBag.StickerRibbonThermal = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 2).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            ViewBag.StickerRibbonSatin = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 3).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            ViewBag.StickerRibbonRFID = db.tbl_StickerRibbon.Where(x => x.IsDeleted != true && x.RibbonTypeID == 4).Select(x => new { Value = x.ID, Name = x.Ribbon }).ToList();
            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 2 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

            if (IsView == true)
            {
                return View("ViewCalculateStickerOffSet", data);
            }
            return View(data);
        }
        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
