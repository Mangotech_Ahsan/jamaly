﻿using POSOman.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models.BLL;
using Microsoft.AspNet.Identity;
using System.Net.Http;
using System.Net;

namespace POSOman.Controllers
{
    public class InquiryController : Controller
    {
        private dbPOS db = new dbPOS();
        Common c = new Common();

        public JsonResult UpdateInquiry(int InquiryID, int? StatusID, decimal? Chances, DateTime? followupdate)
        {
            Console.WriteLine("sadsadsad");
            var data = db.tbl_Inquiry.Where(a => a.InquiryID == InquiryID).FirstOrDefault();
            if(StatusID != null) { data.StatusID = StatusID; }
            if (Chances != null) { data.ChancesPercent = Chances; }
            if (followupdate != null) { data.FollowUpDate = followupdate; }
            
            db.Entry(data).State = System.Data.Entity.EntityState.Modified;

            if (db.SaveChanges() > 0)
            {
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Failed", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult LatestInquiryNo()
        {
            int code = 0;
            int data = db.tbl_Inquiry.OrderByDescending(a => a.InquiryID).Select(a => a.Code).FirstOrDefault() ?? 0;
            code = data + 1;

            return Json(code, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult InquiryDetail(int? id)
        {
            var data = db.tbl_Inquiry.Where(a => a.InquiryID == id).Select(a => new {
                InquiryID = a.InquiryID,
                InquiryDate = a.InquiryDate,
                InquiryNumber = a.InquiryNumber,
                AccountID = a.tbl_Customer.AccountID,
                Name = a.tbl_Customer.Name,
                ContactPerson = a.ContactPerson,
                CompanyName = a.CompanyName,
                PhoneNo = a.PhoneNo,
                Email = a.Email,
                Sources = a.Sources,
                FollowUpDate = a.FollowUpDate,
                FollowUp = a.FollowUp,
                StatusTitle = a.tbl_OrderStatus.StatusTitle,
                Description = a.Description,
                Remarks = a.Remarks,
                ChancesPercent = a.ChancesPercent,
                SalePersonID = a.SalePersonID,
                SalesRevenue = a.SalesRevenue
            }).FirstOrDefault();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void GetViewBagValues()
        {
            ViewBag.Status = db.tbl_OrderStatus.Select(c => new { Value = c.StatusID, Name = c.StatusTitle }).ToList();
            ViewBag.Employee = db.tbl_Employee.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.Customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
        }

        // GET: Inquiry
        public ActionResult Index(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, int? StatusID)
        {
            GetViewBagValues();
            if (btn.HasValue)
            {
                List<GetInquiry_Result> data = db.GetInquiry(StatusID, AccountID, fromDate, toDate, null).ToList();
                if(data.Count > 0)
                {
                    return PartialView("_Index", data.OrderByDescending(so => so.InquiryID));
                }
                else
                {
                    return PartialView("_Index");
                }
            }
            
            return View();
        }

        public ActionResult Create()
        {
            GetViewBagValues();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_Inquiry inquiry)
        {
            try
            {
                if (db.tbl_Inquiry.Any(v => v.InquiryNumber == inquiry.InquiryNumber))
                {
                    GetViewBagValues();
                    ModelState.AddModelError("Inquiry Exist!", "This Inquiry Already Exists!");
                    return View("Create");
                }

                if(inquiry.StatusID == null)
                {
                    inquiry.StatusID = 9;
                }
                inquiry.isActive = true;
                inquiry.AddOn = Helper.PST();
                inquiry.AddBy = User.Identity.GetUserId();
                db.tbl_Inquiry.Add(inquiry);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                GetViewBagValues();
                return View(inquiry);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            tbl_Inquiry data = db.tbl_Inquiry.Find(id);
            
            if (data == null)
            {
                return HttpNotFound();
            }
            

            GetViewBagValues();
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_Inquiry inquiry)
        {

            try
            {
                var data = db.tbl_Inquiry.Where(a => a.InquiryID == inquiry.InquiryID).FirstOrDefault();
                data.CompanyName = inquiry.CompanyName;
                data.ContactPerson = inquiry.ContactPerson;
                data.PhoneNo = inquiry.PhoneNo;
                data.Email = inquiry.Email;
                data.Sources = inquiry.Sources;
                data.FollowUp = inquiry.FollowUp;
                data.StatusID = inquiry.StatusID;
                data.SalePersonID = inquiry.SalePersonID;
                data.Remarks = inquiry.Remarks;
                data.SalesRevenue = inquiry.SalesRevenue;
                data.Description = inquiry.Description;
                data.UpdatedOn = Helper.PST();
                data.UpdatedBy = User.Identity.GetUserId();
                data.CustomerID = inquiry.CustomerID;
                data.FollowUpDate = inquiry.FollowUpDate;
                data.ChancesPercent = inquiry.ChancesPercent;
                db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                GetViewBagValues();
                return View(inquiry);
            }
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            tbl_Inquiry data = db.tbl_Inquiry.Find(id);

            if (data == null)
            {
                return HttpNotFound();
            }


            ModelState.AddModelError(string.Empty, "Deleting this record results in deleting all entries and related actions! Proceed to delete?");
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int? id, int extra = 0)
        {
            var data = db.tbl_Inquiry.Where(a => a.InquiryID == id).FirstOrDefault();
            //var inqQuot = db.tbl_InquiryQuotation.Where(a => a.InquiryID == id).ToList();
            data.isActive = false;
            db.Entry(data).State = System.Data.Entity.EntityState.Modified;
            
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            tbl_Inquiry data = db.tbl_Inquiry.Find(id);

            if (data == null)
            {
                return HttpNotFound();
            }


            return View(data);
        }

    }
}