﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using Microsoft.AspNet.Identity;
using System.Web.Configuration;
using POSOman.Models.BLL;
using AutoMapper;

namespace POSOman.Controllers
{
    public class VehicleCodeController : Controller
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();


        private int GetBranchID()
        {
            int branchId = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        private int GetUserID()
        {
            int userID = 0;
            string currentUserId = "";
            if (Session["UserID"] != null)
            {
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                userID = currentUser.UserId;
            }

            return userID;
        }

        public JsonResult getCategory(int? vehCodeID)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);

            var qry = db.tbl_VehicleCode.Where(p=>p.HeadID !=null).Select(p => new { Value = p.VehicleCodeID, Name = p.VehicleCode }).ToList();

            return Json(qry);

        }

        public JsonResult getProduct(int? vehCodeID)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);

            var qry = db.tbl_Product.Where(p=>p.VehicleCodeID== vehCodeID).Select(p => new { Value = p.ProductID, Name = p.PartNo }).ToList();

            return Json(qry);

        }

        // GET: VehicleCode
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            return View(db.tbl_VehicleCode.Where(veh => veh.LevelID == 0 &&(veh.IsDeleted!=true || veh.IsDeleted == null)).ToList());
        }

        // GET: VehicleCode/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_VehicleCode tbl_VehicleCode = db.tbl_VehicleCode.Find(id);
            if (tbl_VehicleCode == null)
            {
                return HttpNotFound();
            }
            return View(tbl_VehicleCode);
        }

        public ActionResult IndexSubService()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            return View(db.tbl_VehicleCode.Where(ve =>ve.HeadID==1).ToList());
        }

        #region  Sub Category
        public ActionResult IndexSub()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            return View(db.tbl_VehicleCode.Where(veh =>  veh.HeadID != 1 && veh.VehicleCodeID!=1 && (veh.IsDeleted != true || veh.IsDeleted == null)).ToList());
        }


        public ActionResult CreateSub()
        {
            ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0 && x.VehicleCodeID > 5 && (x.IsDeleted!=true || x.IsDeleted == null)), "VehicleCodeID", "VehicleCode");
            return View();
        }
        public ActionResult CreateSubService()
        {
            ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.VehicleCodeID==1), "VehicleCodeID", "VehicleCode", 1);
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSubService(Models.DTO.VehicleCodeDTO tbl_VehicleCode)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (ModelState.IsValid)
            {
                if (db.tbl_VehicleCode.Any(v => v.VehicleCode == tbl_VehicleCode.VehicleCode && v.BranchID == branchId))
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Service Already Exists!");
                    return View("IndexSubService");
                }
                else if (string.IsNullOrWhiteSpace(tbl_VehicleCode.VehicleCode))
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Please Enter Service!");
                    return View("IndexSubService");
                }


                else if (tbl_VehicleCode.HeadID == null)
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Please Select Head Service!");
                    return View("IndexSubService");
                }

                else
                {
                    tbl_VehicleCode.LevelID = 1;

                    var user = User.Identity;
                    tbl_VehicleCode.UserID = user.GetUserId();
                    tbl_VehicleCode.AddOn = DateTime.UtcNow.AddHours(5).Date;
                    tbl_VehicleCode.BranchID = branchId;
                    tbl_VehicleCode.HeadID = 1;

                    tbl_VehicleCode.IsActive = true;
                    tbl_VehicleCode.IsDeleted = false;

                    Mapper.CreateMap<Models.DTO.VehicleCodeDTO, tbl_VehicleCode>();

                    var master = Mapper.Map<Models.DTO.VehicleCodeDTO, tbl_VehicleCode>(tbl_VehicleCode);
                    db.tbl_VehicleCode.Add(master);


                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added Sub Category(Veh Code)".ToString());

                    return RedirectToAction("IndexSubService");
                }
            }

            return View(tbl_VehicleCode);
        }


        public ActionResult EditSubService(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            tbl_VehicleCode tbl_VehicleCode = db.tbl_VehicleCode.Find(id);
            if (tbl_VehicleCode == null)
            {
                return HttpNotFound();
            }

            ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.VehicleCodeID == 1), "VehicleCodeID", "VehicleCode", 1);
            return View(tbl_VehicleCode);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSubService(tbl_VehicleCode tbl_VehicleCode)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (ModelState.IsValid)
            {
                if (db.tbl_VehicleCode.Any(v => v.VehicleCode == tbl_VehicleCode.VehicleCode && v.BranchID == branchId))
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Service Already Exists!");
                    return View("EditSubService");
                }
                else if (string.IsNullOrWhiteSpace(tbl_VehicleCode.VehicleCode))
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Please Enter Service!");
                    return View("IndexSubService");
                }
                else if (tbl_VehicleCode.HeadID == null)
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Please Select Head Service!");
                    return View("IndexSubService");
                }
                else
                {
                    var data = db.tbl_VehicleCode.Where(a => a.VehicleCodeID == tbl_VehicleCode.VehicleCodeID).FirstOrDefault();
                    data.VehicleCode = tbl_VehicleCode.VehicleCode;

                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();

                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edit Sub Services(Veh Code)".ToString());

                    return RedirectToAction("IndexSubService");
                }
            }

            return View(tbl_VehicleCode);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSub(Models.DTO.VehicleCodeDTO tbl_VehicleCode)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (ModelState.IsValid)
            {
                if (db.tbl_VehicleCode.Any(v => v.VehicleCode == tbl_VehicleCode.VehicleCode && v.BranchID == branchId))
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Category Already Exists!");
                    return View("CreateSub");
                }
                else if (string.IsNullOrWhiteSpace(tbl_VehicleCode.VehicleCode))
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Please Enter Category!");
                    return View("CreateSub");
                }


                else if ( tbl_VehicleCode.HeadID == null)
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Please Select Head Category!");
                    return View("CreateSub");
                }

                else
                {
                    tbl_VehicleCode.LevelID = 1;

                    var user = User.Identity;
                    tbl_VehicleCode.UserID = user.GetUserId();
                    tbl_VehicleCode.AddOn = DateTime.UtcNow.AddHours(5).Date;
                    tbl_VehicleCode.BranchID = branchId;
                    tbl_VehicleCode.IsActive = true;
                    tbl_VehicleCode.IsDeleted = false;

                    Mapper.CreateMap<Models.DTO.VehicleCodeDTO, tbl_VehicleCode>();

                    //   var details = Mapper.Map<ICollection<Models.DTO.SaleDetails>, ICollection<tbl_SaleDetails>>(modelSales.SaleDetails);
                    var master = Mapper.Map<Models.DTO.VehicleCodeDTO, tbl_VehicleCode>(tbl_VehicleCode);
                    // master.tbl_SaleDetails = details;
                    db.tbl_VehicleCode.Add(master);


                    //  db.tbl_VehicleCode.Add(tbl_VehicleCode);
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added Sub Category(Veh Code)".ToString());

                    return RedirectToAction("IndexSub");
                }
            }

            return View(tbl_VehicleCode);
        }



        // GET: VehicleCode/Edit/5
        public ActionResult EditSub(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_VehicleCode tbl_VehicleCode = db.tbl_VehicleCode.Find(id);
            if (tbl_VehicleCode == null)
            {
                return HttpNotFound();
            }
            // ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x =>  x.LevelID == 0), "VehicleCodeID", "VehicleCode", tbl_VehicleCode.HeadID);
            ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0 && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)), "VehicleCodeID", "VehicleCode");

            return View(tbl_VehicleCode);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSub(tbl_VehicleCode tbl_VehicleCode)
        {
            if (ModelState.IsValid)
            {
                int branchId = GetBranchID();
                int userID = GetUserID();

                if (db.tbl_VehicleCode.Any(c => c.VehicleCode == tbl_VehicleCode.VehicleCode && c.VehicleCodeID != tbl_VehicleCode.VehicleCodeID && c.BranchID == branchId))
                {
                    ModelState.AddModelError("VehicleCode", "Sub-Category Already Exists!");
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    return View("EditSub");
                }
                else if (tbl_VehicleCode.HeadID == null)
                {
                    ModelState.AddModelError("VehicleCode", "Please select head-category!");
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    return View("EditSub");
                }
                else if (string.IsNullOrWhiteSpace(tbl_VehicleCode.VehicleCode))
                {
                    ModelState.AddModelError("VehicleCode", "cannot enter null values!");
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && x.LevelID == 0), "VehicleCodeID", "VehicleCode");
                    return View("EditSub");
                }
                else
                {
                    tbl_VehicleCode.LevelID = 1;
                    tbl_VehicleCode.UpdateOn = DateTime.UtcNow.AddHours(5);
                    tbl_VehicleCode.UpdateBy = userID;
                    db.Entry(tbl_VehicleCode).State = EntityState.Modified;
                    db.Entry(tbl_VehicleCode).Property(x => x.ImageUrl).IsModified = false;

                    db.Entry(tbl_VehicleCode).Property(x => x.LevelID).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.IsActive).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.IsDeleted).IsModified = false;

                    db.Entry(tbl_VehicleCode).Property(x => x.AddOn).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.Addby).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.UserID).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.BranchID).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.rowguid).IsModified = false;
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edited Sub-Category(Veh Code)".ToString());

                    return RedirectToAction("IndexSub");
                }
            }
            return View(tbl_VehicleCode);
        }

        #endregion

        #region  Section of category

        public ActionResult IndexSection()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            return View(db.tbl_VehicleCode.Where(veh => veh.LevelID == 2).ToList());
        }
        public ActionResult CreateCategorySec()
        {
            ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID !=null && x.LevelID == 1), "VehicleCodeID", "VehicleCode");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCategorySec(Models.DTO.VehicleCodeDTO tbl_VehicleCode)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (ModelState.IsValid)
            {
                if (db.tbl_VehicleCode.Any(v => v.VehicleCode == tbl_VehicleCode.VehicleCode && v.BranchID == branchId))
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID != null && x.LevelID == 1), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Category Already Exists!");
                    return View("CreateCategorySec");
                }
                else if (string.IsNullOrWhiteSpace(tbl_VehicleCode.VehicleCode))
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID != null && x.LevelID == 1), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Please Enter Category!");
                    return View("CreateCategorySec");
                }


                else if ( tbl_VehicleCode.HeadID == null)
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID != null && x.LevelID == 1), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Please Select Sub Category!");
                    return View("CreateCategorySec");
                }

                else
                {
                    tbl_VehicleCode.LevelID = 2;

                    var user = User.Identity;
                    tbl_VehicleCode.UserID = user.GetUserId();
                    tbl_VehicleCode.AddOn = DateTime.UtcNow.AddHours(5).Date;
                    tbl_VehicleCode.BranchID = branchId;
                    tbl_VehicleCode.IsActive = true;
                    tbl_VehicleCode.IsDeleted = false;

                    Mapper.CreateMap<Models.DTO.VehicleCodeDTO, tbl_VehicleCode>();

                    //   var details = Mapper.Map<ICollection<Models.DTO.SaleDetails>, ICollection<tbl_SaleDetails>>(modelSales.SaleDetails);
                    var master = Mapper.Map<Models.DTO.VehicleCodeDTO, tbl_VehicleCode>(tbl_VehicleCode);
                    // master.tbl_SaleDetails = details;
                    db.tbl_VehicleCode.Add(master);


                    //  db.tbl_VehicleCode.Add(tbl_VehicleCode);
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added category section(Veh Code)".ToString());

                    return RedirectToAction("IndexSection");
                }
            }

            return View(tbl_VehicleCode);
        }



        // GET: VehicleCode/Edit/5
        public ActionResult EditSection(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_VehicleCode tbl_VehicleCode = db.tbl_VehicleCode.Find(id);
            if (tbl_VehicleCode == null)
            {
                return HttpNotFound();
            }
            ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.LevelID == 1), "VehicleCodeID", "VehicleCode", tbl_VehicleCode.HeadID);

            return View(tbl_VehicleCode);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSection(tbl_VehicleCode tbl_VehicleCode)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                if (db.tbl_VehicleCode.Any(c => c.VehicleCode == tbl_VehicleCode.VehicleCode && c.VehicleCodeID != tbl_VehicleCode.VehicleCodeID && c.BranchID == branchId))
                {
                    ModelState.AddModelError("VehicleCode", "Section Already Exists!");
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x =>  x.LevelID == 1), "VehicleCodeID", "VehicleCode");

                    return View("EditSection");
                }
                else if (tbl_VehicleCode.HeadID == null)
                {
                    ModelState.AddModelError("VehicleCode", "Please select sub-category!");
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x =>  x.LevelID == 1), "VehicleCodeID", "VehicleCode");

                    return View("EditSection");
                }
                else if (string.IsNullOrWhiteSpace(tbl_VehicleCode.VehicleCode))
                {
                    ModelState.AddModelError("VehicleCode", "cannot enter null values!");
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x =>  x.LevelID ==1), "VehicleCodeID", "VehicleCode");

                    return View("EditSection");
                }
                else
                {
                    tbl_VehicleCode.UpdateBy = userID;
                    tbl_VehicleCode.UpdateOn = DateTime.UtcNow.AddHours(5);
                    tbl_VehicleCode.LevelID = 2;
                    db.Entry(tbl_VehicleCode).State = EntityState.Modified;
                    db.Entry(tbl_VehicleCode).Property(x => x.ImageUrl).IsModified = false;

                    //db.Entry(tbl_VehicleCode).Property(x => x.LevelID).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.IsActive).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.IsDeleted).IsModified = false;

                    db.Entry(tbl_VehicleCode).Property(x => x.AddOn).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.Addby).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.UserID).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.BranchID).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.rowguid).IsModified = false;
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edited Section(Veh Code)".ToString());

                    return RedirectToAction("IndexSection");
                }
            }
            return View(tbl_VehicleCode);
        }



        #endregion

        #region  Master Category
        // GET: VehicleCode/Create
        public ActionResult Create()
        {
            ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x=>x.HeadID == null && (x.IsDeleted!=true || x.IsDeleted == null)), "VehicleCodeID", "VehicleCode");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.DTO.VehicleCodeDTO tbl_VehicleCode)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (ModelState.IsValid)
            {
                if (db.tbl_VehicleCode.Any(v => v.VehicleCode == tbl_VehicleCode.VehicleCode && v.BranchID == branchId))
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && (x.IsDeleted != true || x.IsDeleted == null)), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Category Already Exists!");
                    return View("Create");
                }
                else if (string.IsNullOrWhiteSpace(tbl_VehicleCode.VehicleCode))
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && (x.IsDeleted != true || x.IsDeleted == null)), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Please Enter Category!");
                    return View("Create");
                }


                else if (tbl_VehicleCode.IsSubCategory == true && tbl_VehicleCode.HeadID == null)
                {
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && (x.IsDeleted != true || x.IsDeleted == null)), "VehicleCodeID", "VehicleCode");
                    ModelState.AddModelError("VehicleCode", "Please Select Head Category!");
                    return View("Create");
                }

                else
                {
                    //if (tbl_VehicleCode.IsSubCategory == false && tbl_VehicleCode.HeadID != null)
                    //{
                        tbl_VehicleCode.HeadID = null;
                    //}
                    tbl_VehicleCode.LevelID = 0;

                    var user = User.Identity;
                    tbl_VehicleCode.UserID = user.GetUserId();
                    tbl_VehicleCode.AddOn = DateTime.UtcNow.AddHours(5).Date;
                    tbl_VehicleCode.BranchID = branchId;
                    tbl_VehicleCode.IsActive = true;
                    tbl_VehicleCode.IsDeleted = false;

                    Mapper.CreateMap<Models.DTO.VehicleCodeDTO, tbl_VehicleCode>();

                    //   var details = Mapper.Map<ICollection<Models.DTO.SaleDetails>, ICollection<tbl_SaleDetails>>(modelSales.SaleDetails);
                    var master = Mapper.Map<Models.DTO.VehicleCodeDTO, tbl_VehicleCode>(tbl_VehicleCode);
                    // master.tbl_SaleDetails = details;
                    db.tbl_VehicleCode.Add(master);


                    //  db.tbl_VehicleCode.Add(tbl_VehicleCode);
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added Category(Veh Code)".ToString());

                    return RedirectToAction("Index");
                }
            }

            return View(tbl_VehicleCode);
        }

        #endregion
        public JsonResult getProduct_VehCode()
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            var qry = db.tbl_VehicleCode.Where(veh => veh.BranchID == branchId).Select(p => new { Value = p.VehicleCodeID, Name = p.VehicleCode }).ToList();
            return Json(qry);
        }
        // POST: VehicleCode/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
      

        // GET: VehicleCode/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_VehicleCode tbl_VehicleCode = db.tbl_VehicleCode.Find(id);
            if (tbl_VehicleCode == null)
            {
                return HttpNotFound();
            }
            return View(tbl_VehicleCode);
        }

        // POST: VehicleCode/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VehicleCodeID,VehicleCode,IsActive,AddOn,Addby,UpdateOn,UpdateBy,DeleteOn,DeleteBy,IsDeleted")] tbl_VehicleCode tbl_VehicleCode)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                if (db.tbl_VehicleCode.Any(c => c.VehicleCode == tbl_VehicleCode.VehicleCode && c.VehicleCodeID != tbl_VehicleCode.VehicleCodeID && c.BranchID == branchId))
                {
                    ModelState.AddModelError("VehicleCode", "Code Already Exists!");
                    ViewBag.HeadID = new SelectList(db.tbl_VehicleCode.Where(x => x.HeadID == null && (x.IsDeleted != true || x.IsDeleted == null)), "VehicleCodeID", "VehicleCode");

                    return View("Edit");
                }
                else
                {
                    tbl_VehicleCode.UpdateOn = DateTime.UtcNow.AddHours(5);
                    tbl_VehicleCode.UpdateBy = userID;
                    tbl_VehicleCode.LevelID = 0;
                    db.Entry(tbl_VehicleCode).State = EntityState.Modified;
                    db.Entry(tbl_VehicleCode).Property(x => x.ImageUrl).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.AddOn).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.Addby).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.UserID).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.BranchID).IsModified = false;
                    db.Entry(tbl_VehicleCode).Property(x => x.rowguid).IsModified = false;                    
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edited Category(Veh Code)".ToString());

                    return RedirectToAction("Index");
                }
            }
            return View(tbl_VehicleCode);
        }

        
        // GET: VehicleCode/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_VehicleCode tbl_VehicleCode = db.tbl_VehicleCode.Find(id);
            if (tbl_VehicleCode == null)
            {
                return HttpNotFound();
            }
            return View(tbl_VehicleCode);
        }

        // POST: VehicleCode/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_VehicleCode tbl_VehicleCode = db.tbl_VehicleCode.Find(id);

            try
            {
                bool isExist = db.tbl_Product.Any(p => p.VehicleCodeID == id && (p.isActive!=0 || p.isActive == null)) == true ? true :
                    db.tbl_VehicleCode.Any(v=>v.HeadID == id && (v.IsDeleted!=true || v.IsDeleted == null)) == true ? true : false;
                if (isExist)
                {
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted Due To Product/Sub Category Association!");
                    if (tbl_VehicleCode == null)
                    {
                        return HttpNotFound();
                    }
                    return View(tbl_VehicleCode);
                }
                else
                {
                    
                    var data = db.tbl_VehicleCode.Where(x => x.VehicleCodeID == id).FirstOrDefault();
                    if (data != null)
                    {
                        data.IsActive = false;
                        data.IsDeleted = true;
                        data.DeleteOn = Helper.PST();
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();

                    }
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Category(Veh Code)".ToString());

                    return RedirectToAction("Index");
                }
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View(tbl_VehicleCode);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
