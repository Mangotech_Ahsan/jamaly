﻿using POSOman.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Net;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class JournalEntryController : Controller
    {
        private dbPOS db = new dbPOS();

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult CreateNewAccount(tbl_AccountDetails modelAccount)
        {
            using(var t = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        modelAccount.OpeningDate = Helper.PST();
                        modelAccount.UserID = User.Identity.GetUserId();
                        modelAccount.AddOn = Helper.PST();
                        modelAccount.IsActive = true;

                        if (modelAccount.CategoryType == 1)
                        {

                            if (db.tbl_AccountType.Any(act => act.TypeName == modelAccount.AccountName))
                            {
                                ModelState.AddModelError("AccountName", "Account Already Exists!");
                                getCreateData();
                                //return PartialView("CreateNewAccount");
                                return Json("Account Already Exists!", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                var AccCode = db.tbl_AccountType.Where(a => a.AccountTypeID == modelAccount.HeadID).Select(a => new { a.ATCode, a.Level }).FirstOrDefault();
                                var code = db.tbl_AccountType.Where(a => a.HeadID == modelAccount.HeadID).OrderByDescending(a => a.AccountTypeID).Select(a => a.Code).FirstOrDefault();
                                tbl_AccountType acctype = new tbl_AccountType();
                                acctype.TypeName = modelAccount.AccountName;
                                acctype.HeadID = modelAccount.HeadID;
                                acctype.Code = (code ?? 0) + 1;
                                acctype.Level = AccCode.Level + 1;
                                acctype.ATCode = AccCode.ATCode + "-" + acctype.Code;
                                acctype.AddOn = Helper.PST();
                                acctype.Addby = 4001;
                                acctype.IsActive = true;
                                acctype.MainHeadID = modelAccount.AccountTypeID;
                                db.tbl_AccountType.Add(acctype);
                                db.SaveChanges();
                                t.Commit();

                                //return RedirectToAction("Create");
                                return Json(modelAccount.AccountID, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            if (db.tbl_AccountDetails.Any(act => act.AccountTypeID == modelAccount.AccountTypeID && act.AccountName == modelAccount.AccountName))
                            {
                                ModelState.AddModelError("AccountName", "Account Already Exists!");
                                getCreateData();
                                // return PartialView("CreateNewAccount");
                                return Json("Account Already Exists!", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                int maxCode = 0;

                                var accType = db.tbl_AccountType.Where(a => a.AccountTypeID == modelAccount.HeadID).Select(a => new { a.ATCode, a.Level }).FirstOrDefault();
                                maxCode = db.tbl_AccountDetails.Where(a => a.AccountTypeID == modelAccount.HeadID).OrderByDescending(a => a.AccountID).Select(a => a.Code).FirstOrDefault() ?? 0;
                                modelAccount.Levels = (accType.Level ?? 0) + 1;
                                modelAccount.Code = maxCode + 1;
                                int HeadID = modelAccount.AccountTypeID;
                                modelAccount.AccountTypeID = modelAccount.HeadID ?? 0;
                                if (modelAccount.HeadID == 27 || modelAccount.HeadID == 15)
                                {
                                    HeadID = 7;
                                }
                                modelAccount.HeadID = HeadID;
                                //modelAccount.HeadID = modelAccount.AccountTypeID;
                                modelAccount.AccountCode = accType.ATCode + "-" + modelAccount.Code;
                                modelAccount.isCustom = true;
                                db.tbl_AccountDetails.Add(modelAccount);
                                db.SaveChanges();
                                t.Commit();

                                //return RedirectToAction("Create");
                                return Json(modelAccount.AccountID, JsonRequestBehavior.AllowGet);
                            }
                        }

                    }

                    //return PartialView(modelAccount);
                    return Json("Something went wrong", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    while(ex.InnerException!=null)
                    {
                        ex = ex.InnerException;
                    }
                    t.Rollback();
                    //return Content(ex.Message);
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            
        }

        private void getCreateData()
        {
            ViewBag.BranchId = db.tbl_Branch.Select(v => new { Value = v.BranchID, Name = v.BranchName }).ToList();
            List<int> AccountTypeIDs = new List<int> { 3, 4, 5, 6, 7, 12, 13 };
            ViewBag.AccountTypeID = new SelectList(db.tbl_AccountType.Where(x => x.HeadID == null), "AccountTypeID", "TypeName");
        }
        private int GetBranchID()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }


        public ActionResult Details(int? id)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            //ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);

            ViewBag.User = db.AspNetUsers.Where(x => x.Id == currentUserId).Select(x => x.UserName).FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            List<tbl_JDetail> _JDDetails = db.tbl_JDetail.Where(p => p.JEntryID == id).ToList();
            if (_JDDetails == null)
            {
                return HttpNotFound();
            }

            return PartialView("_GJInvoice", _JDDetails);
        }

        public ActionResult NewIndex(bool? isSearch, int? AccountTypeID, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            ViewBag.BranchID = db.tbl_Branch.Select(x => new { Value = x.BranchID, Name = x.BranchName }).ToList();
            ViewBag.Account = db.tbl_AccountDetails.Where(acd => (acd.AccountID == 1 || acd.AccountID == 2 || acd.AccountID > 33)).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
            List<int> AccTypeID = new List<int> { 3, 4, 5, 6, 7, 12, 13 };
            ViewBag.AccountType = db.tbl_AccountType.Where(acd => AccTypeID.Contains(acd.AccountTypeID)).Select(p => new { Value = p.AccountTypeID, Name = p.TypeName }).ToList();

            if (isSearch.HasValue)
            {
                List<GetGeneralJournalFilterWise_Result> gj = db.GetGeneralJournalFilterWise(AccountTypeID, AccountID, fromDate, toDate, null).ToList();
                if (gj.Count == 0)
                {
                    return PartialView("_Index");
                }
                else
                    return PartialView("_Index", gj);
            }

            return View();
        }

        //GET Trial Balance 
        public ActionResult getTrialBalance(DateTime? fromDate, DateTime? toDate)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            ViewBag.BranchID = branchId;
            List<GetTrialBalance2_Result> gj = db.GetTrialBalance2(fromDate, toDate, branchId).ToList();
            GetRevenueData_Result getRevenue = db.GetRevenueData(fromDate, toDate, branchId).FirstOrDefault();

            List<GetTrialBalance2_Result> Assets = new List<GetTrialBalance2_Result>(); // 1
            List<GetTrialBalance2_Result> Liabilities = new List<GetTrialBalance2_Result>(); // 2
            List<GetTrialBalance2_Result> Equity = new List<GetTrialBalance2_Result>(); // 3
            List<GetTrialBalance2_Result> Revenue = new List<GetTrialBalance2_Result>(); // 4
            List<GetTrialBalance2_Result> Expenses = new List<GetTrialBalance2_Result>(); // 5
            List<Models.DTO.AssetsBalance> assetsBal = new List<Models.DTO.AssetsBalance>();
            List<Models.DTO.LiabilityBalance> liabBal = new List<Models.DTO.LiabilityBalance>();
            List<Models.DTO.EquityBalance> eqBal = new List<Models.DTO.EquityBalance>();
            Assets = gj.Where(a => a.AccountTypeID == 1 || a.HeadID == 1).ToList();
            Liabilities = gj.Where(a => a.AccountTypeID == 2 || a.HeadID == 2).ToList();
            Equity = gj.Where(a => a.AccountTypeID == 3 || a.HeadID == 3).ToList();
            Revenue = gj.Where(a => a.AccountTypeID == 4 || a.HeadID == 4).ToList();
            Expenses = gj.Where(a => a.AccountTypeID == 5 || a.HeadID == 5).ToList();
            Models.DTO.Revenue rev = new Models.DTO.Revenue();
            rev.Sales = getRevenue.Sales;
            rev.COGS = getRevenue.COGS;
            rev.GrossProfit = rev.Sales - rev.COGS;

            foreach (var item in Assets)
            {
                ///if (item.AccountTypeID != 6)
                {
                    var ast = MaptoModel(item);
                    assetsBal.Add(ast);
                }
            }
            foreach (var item in Liabilities)
            {
                //if (item.AccountTypeID != 6)
                {
                    var liab = MapLiabtoModel(item);
                    liabBal.Add(liab);
                }
            }
            foreach (var item in Equity)
            {
                if (item.AccountID != 4)
                {
                    var reven = MapEquitytoModel(item);
                    eqBal.Add(reven);
                }
            }
            rev.Expenses = assetsBal.Sum(exp => exp.Balance);
            rev.NetProfit = rev.GrossProfit - rev.Expenses;
            if (gj.Count == 0)
            {
                return View("TrialBalance");
            }
            else
                return View("TrialBalance", gj);
        }
        private Models.DTO.AssetsBalance MaptoModel(GetTrialBalance2_Result assets)
        {
            Models.DTO.AssetsBalance assetsBalance = new Models.DTO.AssetsBalance()
            {
                AccountName = assets.AccountName,
                TotalCredit = assets.TotalCredit,
                TotalDebit = assets.TotalDebit,
                Balance = assets.Balance
            };
            return assetsBalance;
        }
        private Models.DTO.LiabilityBalance MapLiabtoModel(GetTrialBalance2_Result liabilities)
        {

            Models.DTO.LiabilityBalance liabilitiesBalance = new Models.DTO.LiabilityBalance()
            {

                AccountName = liabilities.AccountName,
                TotalCredit = liabilities.TotalCredit,
                TotalDebit = liabilities.TotalDebit,
                Balance = liabilities.Balance

            };
            return liabilitiesBalance;
        }
        private Models.DTO.EquityBalance MapEquitytoModel(GetTrialBalance2_Result equity)
        {
            Models.DTO.EquityBalance equityBalance = new Models.DTO.EquityBalance()
            {
                AccountName = equity.AccountName,
                TotalCredit = equity.TotalCredit,
                TotalDebit = equity.TotalDebit,
                Balance = equity.Balance
            };
            return equityBalance;
        }
        // GET: JournalEntry/Create
        public ActionResult Create()
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            // ViewBag.BranchID = branchId;
            ViewBag.BranchID = db.tbl_Branch.Select(x => new { Value = x.BranchID, Name = x.BranchName }).ToList();
            //ViewBag.Account = db.tbl_AccountDetails.Where(acd => acd.AccountID > 24  && acd.BranchID == branchId).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
            //ViewBag.Account = db.tbl_AccountDetails.Where(acd => (acd.AccountID == 1 || acd.AccountID == 7 || acd.AccountID > 24) && acd.BranchID == branchId).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
            //ViewBag.Account = db.tbl_AccountDetails.Where(acd => acd.AccountID == 1 || acd.AccountID > 33).Select(p => new { Value = p.AccountID, Name = p.AccountCode + "-" + p.AccountName  }).ToList();
            ViewBag.Account = db.tbl_AccountDetails.Where(acd => (acd.AccountID == 1 || acd.AccountID > 33 ) && (acd.IsDeleted!=true || acd.IsDeleted == null)).Select(p => new { Value = p.AccountID, Name = p.tbl_AccountType.TypeName + "-|-" + p.AccountCode + "-|-" + p.AccountName  }).ToList();

            return View();
        }

        public ActionResult NewIndexManual(bool? isSearch, int? AccountTypeID, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {
            ViewBag.BranchID = db.tbl_Branch.Select(x => new { Value = x.BranchID, Name = x.BranchName }).ToList();
            ViewBag.Account = db.tbl_AccountDetails.Where(acd => (acd.AccountID == 1 || acd.AccountID == 2 || acd.AccountID > 33)).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
            List<int> AccTypeID = new List<int> { 3, 4, 5, 6, 7, 12, 13 };
            ViewBag.AccountType = db.tbl_AccountType.Where(acd => AccTypeID.Contains(acd.AccountTypeID)).Select(p => new { Value = p.AccountTypeID, Name = p.TypeName }).ToList();
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            if (isSearch.HasValue)
            {
                List<GetGeneralJournalFilterWiseManual_Result> gj = db.GetGeneralJournalFilterWiseManual(AccountTypeID, AccountID, fromDate, toDate).ToList();
                if (gj.Count == 0)
                {
                    return PartialView("_IndexManual");
                }
                else
                    return PartialView("_IndexManual", gj);
            }
            return View();
        }


        public JsonResult getEditGJEntryData(int JEntryID)
        {
            List<object> obList = new List<object>();
            var JDetail = db.tbl_JDetail.Where(x => x.JEntryID == JEntryID).Select(x => new { JEntryID = x.JEntryID, AccountID = x.AccountID, AccountName = x.tbl_AccountDetails.AccountName, Memo = x.Memo, Debit = x.Dr, Credit = x.Cr, BranchID = x.BranchID, MultiEntryDate = x.MultiEntryDate }).ToList();
            var Description = db.tbl_JEntry.Where(x => x.JEntryId == JEntryID).Select(x => new { Description = x.Description, VoucherName = x.VoucherName }).FirstOrDefault();

            if (JDetail != null)
            {
                obList.Add(new { JDetail = JDetail, Description = Description, JEntryID = JEntryID });
            }

            return Json(obList);
        }

        //Edit GJ Entry
        public ActionResult EditGJEntry(int id)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            //  ViewBag.BranchId = db.tbl_Branch.Select(x => new { Value = x.BranchID, Name = x.BranchName }).ToList();// branchId;
            ViewBag.JEntryID = id;
            ViewBag.BranchID = db.tbl_Branch.Select(x => new { Value = x.BranchID, Name = x.BranchName }).ToList();

            //ViewBag.Account = db.tbl_AccountDetails.Where(acd => acd.AccountID > 24  && acd.BranchID == branchId).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
            //ViewBag.Account = db.tbl_AccountDetails.Where(acd => (acd.AccountID == 1 || acd.AccountID == 7 || acd.AccountID > 24) && acd.BranchID == branchId).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
            ViewBag.Account = db.tbl_AccountDetails.Where(acd => (acd.AccountID == 1 || acd.AccountID > 33) && (acd.IsDeleted != true || acd.IsDeleted == null)).Select(p => new { Value = p.AccountID, Name = p.tbl_AccountType.TypeName + "-|-" + p.AccountCode + "-|-" + p.AccountName }).ToList();

            return View();
        }
        // Save Edited Journal General Entry 
        public JsonResult SaveEditGJEntry(Models.DTO.JournalEntry model)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            foreach (var i in model.JDetail)
            {
                if ((i.Dr <= 0 || i.Dr == null) && (i.Cr <= 0 || i.Cr == null))
                {
                    return Json("");
                }
            }
            // User.Identity.GetUserName();
            string UserName = User.Identity.GetUserName().ToString(); // Session["CUserName"].ToString();

            POSOman.Models.BLL.SaveJournalEntry gjEntry = new Models.BLL.SaveJournalEntry();
            var orderID = gjEntry.SaveEditGJEntry(model, UserName);
            return Json(orderID);

        }
        //public JsonResult AssignEntryDate(Models.DTO.JournalEntry model)
        //{
        //    int branchId = 0;
        //    if (Session["BranchID"] != null)
        //    {
        //        branchId = Convert.ToInt32(Session["BranchID"]);
        //    }
        //    else
        //    {
        //        var user = User.Identity;
        //        string currentUserId = User.Identity.GetUserId();
        //        var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
        //        branchId = currentUser.BranchID;
        //    }

        //    string UserName = User.Identity.GetUserName().ToString();//Session["CUserName"].ToString();

        //    POSOman.Models.BLL.SaveJournalEntry gjEntry = new Models.BLL.SaveJournalEntry();
        //    var orderID = gjEntry.SaveEntry(model, UserName);
        //    return Json(orderID);
        //    //else {
        //    //    var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
        //    //}
        //}
        public JsonResult AssignEntryDate(Models.DTO.JournalEntry model)
        {
            List<object> objectList = new List<object>();
            List<object> NewEntryList = new List<object>();
            // int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (model.JDetail.Count > 0 && model.EntryDate != null)
            {
                foreach (var i in model.JDetail)
                {
                    if (i.MultiEntryDate == null)
                    {
                        i.MultiEntryDate = model.EntryDate;
                        i.IsMultiDateEntry = true;
                    }
                    i.Detail = db.tbl_AccountDetails.Where(x => x.AccountID == i.AccountID).Select(x => x.AccountName).FirstOrDefault();

                }

                var EntryList = model.JDetail.Select(x => new { accId = x.AccountID, accountName = x.Detail, memo = x.Memo, drAmount = x.Dr, crAmount = x.Cr, MultiEntryDate = x.MultiEntryDate }).ToList();



                return Json(EntryList);

            }
            else
            {
                return Json(null);
            }


        }
        public JsonResult getNewID()
        {
            long? PLOID = 0;
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var tmp = db.tbl_JEntry.Where(so => so.IsDeleted != true).OrderByDescending(v => v.JEntryId).FirstOrDefault();
            if (tmp != null)
            {
                if (tmp.CodeInt != null)
                {
                    PLOID = tmp.CodeInt + 1;
                }
                else
                {
                    PLOID = 950001;
                }
            }
            else
            {
                PLOID = 950001;
            }

            return Json(PLOID, JsonRequestBehavior.AllowGet);
        }
        // Save Journal General Entry 
        public JsonResult SaveEntry(Models.DTO.JournalEntry model)
        {
            int branchId = GetBranchID();
            string UserName = User.Identity.GetUserId().ToString();
            model.UserID = User.Identity.GetUserId();

            POSOman.Models.BLL.SaveJournalEntry gjEntry = new Models.BLL.SaveJournalEntry();
            var orderID = gjEntry.SaveEntry(model, UserName);
            return Json(orderID);
            //else {
            //    var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
            //}
        }
        // [Authorize(Roles = "SuperAdmin")]
        // GET: Jentry/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            tbl_JEntry tbl_Jentry = db.tbl_JEntry.Where(j => j.JEntryId == id).FirstOrDefault();
            if (tbl_Jentry == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Jentry);
        }
        // [Authorize(Roles = "SuperAdmin")]
        // POST: Entry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                //db.DeleteJournalEntry(id);
                POSOman.Models.BLL.SaveJournalEntry gjEntry = new Models.BLL.SaveJournalEntry();
                var orderID = gjEntry.DeleteGJEntry(id);
                return RedirectToAction("NewIndex");
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("NewIndex");
            }
        }
    }
}
