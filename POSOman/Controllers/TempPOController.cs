﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace POSOman.Controllers
{
    public class TempPOController : Controller
    {
        private dbPOS db = new dbPOS();

        private int GetBranchID()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        // GET: TempPO
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            int branchId = GetBranchID();

            var tmp_PO = db.tmp_PO.Where(t => t.IsDeleted == false || t.IsDeleted == null && t.IsPO == false).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_PaymentTypes).OrderByDescending(t => t.tmpID);
            return View(tmp_PO.ToList());            
        }

        public JsonResult getTmpOrderID()
        {
            int? tempID = 0;
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var tmp = db.tmp_PO.Where(po => po.BranchID == branchId).OrderByDescending(v => v.OrderID).FirstOrDefault();
            if (tmp != null)
            {
                tempID = tmp.tmpID + 1;
            }          
            else
            {
                tempID = 1;
            }
            return Json(tempID, JsonRequestBehavior.AllowGet);
        }        
        // make isDeleted = true when user submit unsaved Orders
        public JsonResult falseTempOrder(int tmpOrderID,int tOrderID)
        {
            var result = db.tmp_PO.SingleOrDefault(p => p.OrderID == tOrderID && p.tmpID == tmpOrderID);
            if (result != null)
            {
                result.IsDeleted = true;
                db.SaveChanges();
            }            
            return Json("Success");

        }
        // Delete Temp when submited immediately
        public JsonResult deleteTempOrder(int tmpOrderID)
        {
            db.tmp_OrderDetails.RemoveRange(db.tmp_OrderDetails.Where(d => d.tmpID == tmpOrderID));
            db.tmp_PO.RemoveRange(db.tmp_PO.Where(p => p.tmpID == tmpOrderID));
            db.SaveChanges();
            return Json("Success");
                
        }
        public ActionResult DeleteUnsavedPurchase(int OrderID)
        {
            db.tmp_OrderDetails.RemoveRange(db.tmp_OrderDetails.Where(d => d.OrderID == OrderID));
            db.tmp_PO.RemoveRange(db.tmp_PO.Where(p => p.OrderID == OrderID));
            db.SaveChanges();
            return RedirectToAction("Index");

        }
        // GET: TempPO/Details/5
        // Get Temp Order Details and Bind it for submission 
        public JsonResult getInvoiceDetails(int tOrderID, int tmpOrderID)
        {
            if (tOrderID > 0 && tmpOrderID > 0)
            {
                try
                {
                   
                    var qry = db.tmp_OrderDetails.Where(p => p.OrderID == tOrderID && p.tmpID == tmpOrderID)
                        .Select(p => new
                        {
                            p.tmp_PO.BranchID,
                            p.OrderID,                            
                            p.ProductID,
                            p.PartNo,
                            p.tbl_Product.Description,
                            p.tbl_Product.VehicleCodeID,
                            p.tbl_Product.tbl_VehicleCode.VehicleCode,
                            p.tbl_Product.Product_Code,
                            p.tbl_Product.Location,                            
                            p.tmp_PO.AccountID,
                            p.tmp_PO.PaymentStatus,
                            p.tmp_PO.PaymentTypeID,
                            p.tmp_PO.PurchaseDate,
                            p.tmp_PO.Currency,
                            p.tmp_PO.AmountPaid,
                            p.tmp_PO.TotalAmount,
                            p.tmp_PO.VAT,
                            p.tmp_PO.DiscountAmount,
                            p.tmp_PO.InvoiceNo,
                            p.tmp_PO.Expenses,
                            CurrencyRate = p.tmp_PO.ExchangeRate,   // Actual Exchange rate
                            p.Qty,
                            PrDiscAm = p.DiscountAmount,
                            PrDiscPer = p.DiscountPercent,
                            PrGSTAm = p.GSTAmount,
                            PrGSTPer = p.GSTPercent,
                            p.UnitPrice,
                            p.tbl_Product.UnitCode,
                            p.tbl_Product.OpenUnitCode,
                            p.ExpiryDate,
                            //p.ExchangeRate, // Exchange UnitPrice 
                            p.SalePrice,
                            Total =  p.Total 
                        }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }

            }

            return Json("");
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //tbl_PurchaseOrder purchaseOrder = db.tbl_PurchaseOrder.Find(id);
            List<tmp_OrderDetails> _tmpPODetails = db.tmp_OrderDetails.Where(p => p.OrderID == id).ToList();
            if (_tmpPODetails == null)
            {
                return HttpNotFound();
            }
            return View(_tmpPODetails);
        }
        public JsonResult tmpSaveOrder(tmp_PO model)
        {
            try
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                branchId = model.BranchID ?? 9001;
                model.AddBy = userID;
                POSOman.Models.BLL.tmpOrder tmpOrder = new Models.BLL.tmpOrder();                
                tmpOrder.Save(model,branchId);
                return Json("success");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

            
        }
        // GET: TempPO/Create
        public ActionResult Create()
        {
            ViewBag.BranchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            ViewBag.AccountID = new SelectList(db.tbl_AccountDetails, "AccountID", "AccountName");
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            //ViewBag.AddBy = new SelectList(db.tbl_User, "ID", "UserName");
            return View();
        }
        public ActionResult tempOrder(int tmpOrderID, int tOrderID)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            //ViewBag.BranchID = branchId;
            ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.vendorCode = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode }).ToList();
            ViewBag.Desc = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();
            ViewBag.Product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.tmpOrderID = tmpOrderID;
            ViewBag.tOrderID = tOrderID;
            ViewBag.isUnsaved = 1;        
            return View("SaveTempOrders");
        }
        // POST: TempPO/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrderID,AccountID,InvoiceNo,PurchaseDate,PaymentStatus,PaymentTypeID,VAT,Expenses,PurchaseCode,Currency,TotalAmount,DiscountPercent,DiscountAmount,AddOn,AddBy,UpdateOn,UpdateBy,IsReturned,IsDeleted,IsPaid,Status,AmountPaid")] tmp_PO tmp_PO)
        {
            if (ModelState.IsValid)
            {
                db.tmp_PO.Add(tmp_PO);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AccountID = new SelectList(db.tbl_AccountDetails, "AccountID", "AccountName", tmp_PO.AccountID);
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name", tmp_PO.PaymentTypeID);
           // ViewBag.AddBy = new SelectList(db.tbl_User, "ID", "UserName", tmp_PO.AddBy);
            return View(tmp_PO);
        }
        // GET: TempPO/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tmp_PO tmp_PO = db.tmp_PO.Find(id);
            if (tmp_PO == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccountID = new SelectList(db.tbl_AccountDetails, "AccountID", "AccountName", tmp_PO.AccountID);
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name", tmp_PO.PaymentTypeID);
           // ViewBag.AddBy = new SelectList(db.tbl_User, "ID", "UserName", tmp_PO.AddBy);
            return View(tmp_PO);
        }

        // POST: TempPO/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderID,AccountID,InvoiceNo,PurchaseDate,PaymentStatus,PaymentTypeID,VAT,Expenses,PurchaseCode,Currency,TotalAmount,DiscountPercent,DiscountAmount,AddOn,AddBy,UpdateOn,UpdateBy,IsReturned,IsDeleted,IsPaid,Status,AmountPaid")] tmp_PO tmp_PO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tmp_PO).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AccountID = new SelectList(db.tbl_AccountDetails, "AccountID", "AccountName", tmp_PO.AccountID);
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name", tmp_PO.PaymentTypeID);
            //ViewBag.AddBy = new SelectList(db.tbl_User, "ID", "UserName", tmp_PO.AddBy);
            return View(tmp_PO);
        }

       
    }
}
