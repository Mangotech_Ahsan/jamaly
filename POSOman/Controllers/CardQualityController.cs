﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class CardQualityController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();

        // GET: CardQuality
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            var tbl_Card_Quality = db.tbl_Card_Quality.Include(t => t.tbl_CardType);
            return View(tbl_Card_Quality.Where(x=>x.IsDeleted!=true).ToList());
        }

        // GET: CardQuality/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Card_Quality tbl_Card_Quality = db.tbl_Card_Quality.Find(id);
            if (tbl_Card_Quality == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Card_Quality);
        }

        // GET: CardQuality/Create
        public ActionResult Create()
        {
            ViewBag.CardTypeID = new SelectList(db.tbl_CardType, "ID", "CardType");
            return View();
        }

        // POST: CardQuality/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_Card_Quality model)
        {
            try
            {
                using(var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            model.AddedOn = Helper.PST();
                            model.IsDeleted = false;
                            db.tbl_Card_Quality.Add(model);
                            db.SaveChanges();

                            tbl_Product p = new tbl_Product();
                            p.VehicleCodeID = 2;// Card Category
                            p.PartNo = model.CardName;
                            p.isActive = 1;
                            p.IsGeneralItem = false;
                            p.IsOffsetItem = true;
                            p.AddOn = Helper.PST();
                            p.MarkedupPrice = model.Rate;
                            p.RawProductID = model.ID;
                            p.CardQuality = model.ID;
                            db.tbl_Product.Add(p);
                            db.SaveChanges();
                            t.Commit();

                            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Created Card Quality/Product With Id: " + model.ID.ToString());


                            return RedirectToAction("Index");
                        }

                        t.Rollback();
                        ViewBag.CardTypeID = new SelectList(db.tbl_CardType, "ID", "CardType", model.CardTypeID);
                        return View(model);
                    }
                    catch(Exception d)
                    {
                        while (d.InnerException != null)
                        {
                            d = d.InnerException;
                        }
                        t.Rollback();
                        ModelState.AddModelError("CardTypeID", d.Message);
                        ViewBag.CardTypeID = new SelectList(db.tbl_CardType, "ID", "CardType", model.CardTypeID);
                        return View(model);
                    }
                 
                }
                
            }
            catch(Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                ModelState.AddModelError("CardTypeID", e.Message);
                ViewBag.CardTypeID = new SelectList(db.tbl_CardType, "ID", "CardType", model.CardTypeID);
                return View(model);
            }
            
        }

        // GET: CardQuality/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Card_Quality model = db.tbl_Card_Quality.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            ViewBag.CardType = new SelectList(db.tbl_CardType, "ID", "CardType", model.CardTypeID);
            return View(model);
        }

        // POST: CardQuality/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( tbl_Card_Quality model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;

                var prod = db.tbl_Product.Where(x => x.CardQuality == model.ID).FirstOrDefault();
                if (prod != null)
                {
                    prod.PartNo = model.CardName;
                    prod.MarkedupPrice = model.Rate;

                    db.Entry(prod).State = EntityState.Modified;
                }
                db.SaveChanges();
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edited Card Quality/Product With Id: " + model.ID.ToString());

                return RedirectToAction("Index");
            }
            ViewBag.CardType = new SelectList(db.tbl_CardType, "ID", "CardType", model.CardTypeID);
            return View(model);
        }

        // GET: CardQuality/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Card_Quality model = db.tbl_Card_Quality.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: CardQuality/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Card_Quality model = db.tbl_Card_Quality.Find(id);
            
            if (model != null)
            {
                using(var t = db.Database.BeginTransaction())
                {
                    if (db.tbl_Product.Any(x => x.CardQuality == id) || db.tbl_Product.Any(x => x.RawProductID == id))
                    {
                        var prodV1 = db.tbl_Product.Where(x => x.CardQuality == id && x.VehicleCodeID == 2).FirstOrDefault();
                        if (prodV1 != null)
                        {
                            //prodV1.PartNo = Helper.RemoveCharactersFromProductName(model.CardName);
                            prodV1.isActive = 0;
                            prodV1.DeleteOn = Helper.PST();
                            db.Entry(prodV1).State = EntityState.Modified;

                            db.Entry(prodV1).Property(p => p.Addby).IsModified = false;
                            db.Entry(prodV1).Property(p => p.isActive).IsModified = false;
                            db.Entry(prodV1).Property(p => p.IsGeneralItem).IsModified = false;
                            db.Entry(prodV1).Property(p => p.IsPacket).IsModified = false;
                            db.Entry(prodV1).Property(p => p.OpeningStock).IsModified = false;
                            db.Entry(prodV1).Property(p => p.Description).IsModified = false;
                            db.Entry(prodV1).Property(p => p.ImageUrl).IsModified = false;
                            db.Entry(prodV1).Property(p => p.Expiry).IsModified = false;
                            db.Entry(prodV1).Property(p => p.CostPrice).IsModified = false;
                            db.Entry(prodV1).Property(p => p.UserID).IsModified = false;
                            db.Entry(prodV1).Property(p => p.BranchID).IsModified = false;
                            db.Entry(prodV1).Property(p => p.AddOn).IsModified = false;
                            db.Entry(prodV1).Property(p => p.rowguid).IsModified = false;
                            db.Entry(prodV1).Property(p => p.VehicleCodeID).IsModified = false;
                            db.Entry(prodV1).Property(p => p.BrandID).IsModified = false;
                            db.Entry(prodV1).Property(p => p.DepartmentID).IsModified = false;
                            db.SaveChanges();

                        }
                        else
                        {
                            var rawProduct = db.tbl_Product.Where(x => x.RawProductID == id && x.VehicleCodeID == 2).FirstOrDefault();
                            if (rawProduct != null)
                            {
                                //rawProduct.PartNo = Helper.RemoveCharactersFromProductName(model.CardName);
                                rawProduct.isActive = 0;
                                rawProduct.DeleteOn = Helper.PST();
                                db.Entry(rawProduct).State = EntityState.Modified;

                                db.Entry(rawProduct).Property(p => p.Addby).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.isActive).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.IsGeneralItem).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.IsPacket).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.OpeningStock).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.Description).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.ImageUrl).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.Expiry).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.CostPrice).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.UserID).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.BranchID).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.AddOn).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.rowguid).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.VehicleCodeID).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.BrandID).IsModified = false;
                                db.Entry(rawProduct).Property(p => p.DepartmentID).IsModified = false;
                                db.SaveChanges();

                            }
                        }

                        
                            model.IsDeleted = true;
                            model.DeletedOn = Helper.PST();

                            db.SaveChanges();
                            t.Commit();
                            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Card Quality/Product With Id #: " + model.ID.ToString());

                            return RedirectToAction("Index");
                       
                        
                    }

                    ModelState.AddModelError("CardTypeID", "Record cannot be deleted. It's Product Not Found!");
                    return View(model);
                }
                
            }

            ModelState.AddModelError("CardTypeID", "Record not found");
            return View(model);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
