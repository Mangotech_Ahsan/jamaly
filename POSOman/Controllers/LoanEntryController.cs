﻿using POSOman.Models;
using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class LoanEntryController : Controller
    {
        LoanEntry loanEntry = new LoanEntry();
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
       
        // GET: Loan 
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Index()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var customerVoucher = db.tbl_JDetail.Where(j => (j.EntryTypeID == 17 || j.EntryTypeID == 18) && j.BranchID == branchId);
            return View(customerVoucher.ToList());
        }
        // Get to Create Page
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult Create()
        {
            try
            {
                int branchId = 0;
                
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                ViewBag.BranchId = branchId;
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }
        // Save loanEntry Entry to Db
        public JsonResult SaveLoanEntry(Expenses model, int? bankAccId)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                object Result = loanEntry.Save(model,bankAccId,branchId);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Loan Entry Action".ToString());

                return Json(Result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Error Performed Loan Entry Action".ToString());

                return Json("formError");
            }
        }
        // open Loan  receipt
        public ActionResult Details(bool? isNew, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (isNew == true)
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.OrderByDescending(v => v.JDetailID).Where(j => j.AccountID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return View("_ExpenseVoucher", _PaymentDetail);
            }
            else
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JDetailID == id).FirstOrDefault();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return View("_ExpenseVoucher", _PaymentDetail);
            }
        }
        [Authorize(Roles = "SuperAdmin")]
        // GET: Jentry/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            tbl_JEntry tbl_Jentry = db.tbl_JEntry.Where(j => j.JEntryId == id && j.BranchID == branchId).FirstOrDefault();
            if (tbl_Jentry == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Jentry);
        }
        [Authorize(Roles = "SuperAdmin")]
        // POST: Entry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int branchId = 0;
                
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                db.DeleteJournalEntry(id);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Loan Entry Action".ToString());

                return RedirectToAction("Index");
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
        }

    }
}