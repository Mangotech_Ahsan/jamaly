﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class DesktopController : Controller
    {
        public static string CONNECTION_STRING = ConfigurationManager.ConnectionStrings["DB_POSConnectionString"].ConnectionString;
        public static string COMMAND_TIMEOUT = ConfigurationManager.AppSettings["CommandTimeout"];
        // GET: Desktop
        public ActionResult Index()
        {
           
            return View();
        }
        public static DataSet GetDataSetUsingStoredProcedure(string procedure, List<SqlParameter> parameters)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(procedure, conn))
                    {
                        //command.CommandTimeout = ConversionHelper.SafeConvertToInt32(COMMAND_TIMEOUT);
                        command.CommandType = CommandType.StoredProcedure;

                        if (parameters != null)
                        {
                            foreach (SqlParameter parameter in parameters)
                            {
                                command.Parameters.Add(parameter);
                            }
                        }

                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataSet dataset = new DataSet();
                            adapter.Fill(dataset);
                            return dataset;
                        }
                    }
                }
            }
            catch (SqlException sqlException)
            {
                ThrowSQLException(sqlException);
            }

            return null;
        }

        private static void ThrowSQLException(SqlException sqlException)
        {
            throw new NotImplementedException();
        }
    }
}