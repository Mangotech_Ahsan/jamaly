﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;

namespace POSOman.Controllers
{
    public class BlockTypeController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: BlockType
        public ActionResult Index()
        {
            return View(db.tbl_BlockType.Where(x=>x.IsDeleted!=true).ToList());
        }

        // GET: BlockType/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_BlockType tbl_BlockType = db.tbl_BlockType.Find(id);
            if (tbl_BlockType == null)
            {
                return HttpNotFound();
            }
            return View(tbl_BlockType);
        }

        // GET: BlockType/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BlockType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_BlockType tbl_BlockType)
        {
            if (ModelState.IsValid)
            {
                db.tbl_BlockType.Add(tbl_BlockType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_BlockType);
        }

        // GET: BlockType/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_BlockType tbl_BlockType = db.tbl_BlockType.Find(id);
            if (tbl_BlockType == null)
            {
                return HttpNotFound();
            }
            return View(tbl_BlockType);
        }

        // POST: BlockType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_BlockType tbl_BlockType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_BlockType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_BlockType);
        }

        // GET: BlockType/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_BlockType tbl_BlockType = db.tbl_BlockType.Find(id);
            if (tbl_BlockType == null)
            {
                return HttpNotFound();
            }
            return View(tbl_BlockType);
        }

        // POST: BlockType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_BlockType model = db.tbl_BlockType.Find(id);
            if (model != null)
            {
                model.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("BlockType", "Record not found");
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
