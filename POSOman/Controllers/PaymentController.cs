﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using POSOman.Models.DTO;
using POSOman.Models.BLL;
using Microsoft.Reporting.WebForms;
using System.Windows;
using System.Net;

namespace POSOman.Controllers
{

    public class PaymentController : Controller
    {
        dbPOS db = new dbPOS();
        // GET: Payment
        Models.DTO.VendorPayment vpay = new Models.DTO.VendorPayment();

        public int getBranchID()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        public ActionResult CreateEmployeePurchases()
        {
            try
            {
                List<object> list = new List<object>();
                list.Add(new
                {
                    Value = -1,
                    Name = "Nil"
                });
                ViewBag.Product = db.tbl_Product.Where(x => (x.IsOffsetItem == true || x.IsGeneralItem == true) && x.VehicleCodeID != 1 && (x.isActive == null || x.isActive == 1)).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
                ViewBag.Expense = db.tbl_AccountDetails.Where(a => a.HeadID == 3 && a.AccountID > 109 && (a.IsDeleted != true || a.IsDeleted == null)).Select(a => new { Value = a.AccountID, Name = a.AccountName }).ToList();

                ViewBag.employee = db.tbl_Employee.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.JentryId_DD = list;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        public ActionResult ApproveEmployeePurchase(int id)
        {
            try
            {
                if (id > 0)
                {
                    ViewBag.OrderID = id;
                    List<object> list = new List<object>();
                    list.Add(new
                    {
                        Value = -1,
                        Name = "Nil"
                    });
                    ViewBag.Product = db.tbl_Product.Where(x => (x.IsOffsetItem == true || x.IsGeneralItem == true) && x.VehicleCodeID != 1 && (x.isActive == null || x.isActive == 1)).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
                    ViewBag.Expense = db.tbl_AccountDetails.Where(a => a.HeadID == 3 && a.AccountID > 109 && (a.IsDeleted != true || a.IsDeleted == null)).Select(a => new { Value = a.AccountID, Name = a.AccountName }).ToList();

                    ViewBag.employee = db.tbl_Employee.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
                    ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                    ViewBag.JentryId_DD = list;
                }
                else
                {
                    return Content("No Data Found");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        public ActionResult EditApprovedEmployeePurchase(int id)
        {
            try
            {
                if (id > 0)
                {
                    ViewBag.OrderID = id;
                    List<object> list = new List<object>();
                    list.Add(new
                    {
                        Value = -1,
                        Name = "Nil"
                    });
                    ViewBag.Product = db.tbl_Product.Where(x => (x.IsOffsetItem == true || x.IsGeneralItem == true) && x.VehicleCodeID != 1 && (x.isActive == null || x.isActive == 1)).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
                    ViewBag.Expense = db.tbl_AccountDetails.Where(a => a.HeadID == 3 && a.AccountID > 109 && (a.IsDeleted != true || a.IsDeleted == null)).Select(a => new { Value = a.AccountID, Name = a.AccountName }).ToList();

                    ViewBag.employee = db.tbl_Employee.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
                    ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                    ViewBag.JentryId_DD = list;
                }
                else
                {
                    return Content("No Data Found");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }
        public ActionResult Index_UC(int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.vendorList = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode + " | " + v.Name }).ToList();
            if (AccountID > 0)
            {
                List<GetVendorPaymentDateWise_Result> vendor = vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, BranchID);
                if (vendor.Count == 0)
                {
                    return PartialView("_VendorStatement");
                }
                else
                {
                    List<StatementDTO> list = new List<StatementDTO>();
                    foreach (var i in vendor)
                    {
                        list.Add(new StatementDTO
                        {
                            Date = i.VoucherDate != null ? Convert.ToString(string.Format("{0:dd/MM/yyyy}", i.VoucherDate)) : "-",
                            Description = i.VoucherName ?? "-",
                            Detail = i.Detail ?? "-",
                            Dr = Convert.ToDecimal(i.Debit).ToString("#,##0.0"),
                            Cr = Convert.ToDecimal(i.Credit).ToString("#,##0.0"),
                            Balance = string.IsNullOrWhiteSpace(i.Balance) ? "-" : Convert.ToDecimal(i.Balance).ToString("#,##0.0"),
                            OpeningBalance = Convert.ToDecimal(i.OpBalance).ToString("#,##0.0")
                        });

                    }
                    return PartialView("_VendorStatement", list);
                }
            }

            return View();
        }

        public ActionResult Index(int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.vendorList = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode + " | " + v.Name }).ToList();

            return View();
        }

        public ActionResult CashPayment()
        {
            try
            {
                List<object> list = new List<object>();
                list.Add(new
                {
                    Value = -1,
                    Name = "Nil"
                });
                ViewBag.Product = db.tbl_Product.Where(x => (x.IsOffsetItem == true || x.IsGeneralItem == true) && x.VehicleCodeID != 1 && (x.isActive == null || x.isActive == 1)).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
                ViewBag.Expense = db.tbl_AccountDetails.Where(a => a.HeadID == 3 && a.AccountID > 109 && (a.IsDeleted != true || a.IsDeleted == null)).Select(a => new { Value = a.AccountID, Name = a.AccountName }).ToList();

                ViewBag.employee = db.tbl_Employee.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.JentryId_DD = list;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        public ActionResult EditCashPayment(int id)
        {
            try
            {
                ViewBag.JentryId = id;
                List<object> list = new List<object>();
                list.Add(new
                {
                    Value = -1,
                    Name = "Nil"
                });
                ViewBag.Product = db.tbl_Product.Where(x => (x.IsOffsetItem == true || x.IsGeneralItem == true) && x.VehicleCodeID != 1 && (x.isActive == null || x.isActive == 1)).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
                ViewBag.Expense = db.tbl_AccountDetails.Where(a => a.HeadID == 3 && a.AccountID > 109 && (a.IsDeleted != true || a.IsDeleted == null)).Select(a => new { Value = a.AccountID, Name = a.AccountName }).ToList();

                ViewBag.employee = db.tbl_Employee.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.JentryId_DD = list;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        public ActionResult DeleteCashPayment(int? id)
        {
            ModelState.AddModelError(string.Empty, "Deleting this record results in deleting all related entries! Proceed to delete?");
            tbl_JEntry data = db.tbl_JEntry.Find(id);
            return View(data);
        }

        [HttpPost, ActionName("DeleteCashPayment")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteEmployeeCashPayment(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_JEntry data = db.tbl_JEntry.Find(id);
            int? jentryId = db.DeleteEmployeeCashPayments(null, id).FirstOrDefault();

            if (jentryId != null && jentryId > 0)
            {
                UserActionsPerformed UserActions = new UserActionsPerformed();
                Common com = new Common();
                UserActions.MapActions(com.GetUserID(), "Performed Delete Employee Cash Payments".ToString());
                return RedirectToAction("GetEmployeePaymentReportFilterWise");

            }
            else
            {
                ModelState.AddModelError(string.Empty, "Record Can not be deleted!");
                return RedirectToAction("DeleteCashPayment",data);
            }
        }

        public ActionResult DeleteEmployeePurchase(int? id)
        {
            ModelState.AddModelError(string.Empty, "Deleting this record results in deleting all related entries! Proceed to delete?");
            tbl_EmployeePurchases data = db.tbl_EmployeePurchases.Find(id);
            return View(data);
        }

        [HttpPost, ActionName("DeleteEmployeePurchase")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteEmployeePurchaseConfirmed(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var p = db.tbl_EmployeePurchases.Find(id);

            try
            {
                if (id!=null && id > 0)
                {
                    bool isDeleted = AdjustmentEntry.DeleteEmployeePendingPurchase(id ?? 0);

                    if (isDeleted)
                    {
                        UserActionsPerformed UserActions = new UserActionsPerformed();
                        Common com = new Common();
                        UserActions.MapActions(com.GetUserID(), "Performed Delete Employee Purchase".ToString());
                        return RedirectToAction("EmployeePurchases");

                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Record Can not be deleted!");
                        return View("DeleteEmployeePurchase", p);
                    }
                }
                ModelState.AddModelError(string.Empty, "Something Went Wrong!");
                return View("DeleteEmployeePurchase", p);
            }
            catch(Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }

                ModelState.AddModelError(string.Empty, e.Message);
                return View("DeleteEmployeePurchase", p);
            }
            
        }

        #region unused for now 20/12/23
        //public JsonResult GetJentryEmployeeWise(int EmpID)
        //{
        //    try
        //    {
        //        List<object> jEntryList = new List<object>();
        //        List<object> list = new List<object>();
        //        decimal? previousBalA = 0;
        //        decimal? previousBalB = 0;
        //        if (EmpID > 0)
        //        {
        //            var data = db.GetJEntryListEmployeeIDWise(EmpID).Select(x => new { Value = x.JEntryId, Name = x.JEntryId + " | " + x.Description ?? "-" }).ToList();

        //            var manualData = db.GetJEntryListEmployeeIDWiseForManualSettlement(EmpID).Select(x => new { Value = x.JEntryId, Name = x.JEntryId + " | " + x.Description ?? "-" }).ToList();

        //            if (manualData != null && manualData.Count > 0)
        //            {
        //                var jentryIds = manualData.Select(z => z.Value).ToList();
        //                previousBalA = db.tbl_JEntry.Where(x => jentryIds.Contains(x.JEntryId) && (x.IsDeleted != true || x.IsDeleted == null)).ToList().Sum(x => x.Amount);
        //            }

        //            if (data != null && data.Count > 0)
        //            {

        //                jEntryList.AddRange(data);
        //                var jentryIds = data.Select(z => z.Value).ToList();
        //                previousBalB = db.tbl_JEntry.Where(x => jentryIds.Contains(x.JEntryId) && (x.IsDeleted != true || x.IsDeleted == null)).ToList().Sum(x => x.Amount);
        //                list.Add(new
        //                {
        //                    jentryList = jEntryList,
        //                    prevBal = Convert.ToDecimal(previousBalB) - Convert.ToDecimal(previousBalA),
        //                    jentryIds = jentryIds
        //                });
        //            }
        //        }

        //        return Json(list, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(-2, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public JsonResult GetJentryEmployeeWiseForBalanceOnEmployeePurchaseCreate(int EmpID)
        //{
        //    try
        //    {
        //        List<object> jEntryList = new List<object>();
        //        List<object> list = new List<object>();
        //        decimal? previousBal = 0;
        //        if (EmpID > 0)
        //        {
        //            var data = db.GetJEntryListEmployeeIDWise(EmpID).Select(x => new { Value = x.JEntryId, Name = x.JEntryId + " | " + x.Description ?? "-" }).ToList();

        //            var pending_Purchases = db.tbl_EmployeePurchases.Where(x => x.EmployeeAccID == EmpID && x.StatusID == 1 && x.IsApproved != true && x.IsDeleted != true).ToList();



        //            if (data != null && data.Count > 0)
        //            {

        //                jEntryList.AddRange(data);
        //                var jentryIds = data.Select(z => z.Value).ToList();
        //                previousBal = db.tbl_JEntry.Where(x => jentryIds.Contains(x.JEntryId) && x.IsClearedFromEmployee != true && (x.IsDeleted != true || x.IsDeleted == null)).ToList().Sum(x => x.Amount);
        //                list.Add(new
        //                {
        //                    jentryList = jEntryList,
        //                    prevBal = Convert.ToDecimal(previousBal) - Convert.ToDecimal(pending_Purchases?.Sum(x => x.TotalAmount) ?? 0),
        //                    jentryIds = jentryIds
        //                });
        //            }
        //        }

        //        return Json(list, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(-2, JsonRequestBehavior.AllowGet);
        //    }
        //}
        #endregion

        public JsonResult GetJentryEmployeeWise(int EmpID)
        {
            try
            {
                List<object> jEntryList = new List<object>();
                List<object> list = new List<object>();
                decimal? previousBalA = 0;
                decimal? previousBalB = 0;
                if (EmpID > 0)
                {
                    decimal empPettyCashBalance = db.tbl_Employee.Where(x => x.AccountID == EmpID).Select(x => x.EmployeePettyCashBalance).FirstOrDefault();// db.GetJEntryListEmployeeIDWise(EmpID).Select(x => new { Value = x.JEntryId, Name = x.JEntryId + " | " + x.Description ?? "-" }).ToList();

                    var pending_Purchases = db.tbl_EmployeePurchases.Where(x => x.EmployeeAccID == EmpID && x.StatusID == 1 && x.IsApproved != true && x.IsDeleted != true).ToList();

                    list.Add(new
                    {
                        //jentryList = jEntryList,
                        prevBal = empPettyCashBalance - Convert.ToDecimal(pending_Purchases?.Sum(x => x.TotalAmount) ?? 0),
                        //jentryIds = jentryIds
                    });

                }

                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(-2, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetJentryEmployeeWiseForBalanceOnEmployeePurchaseCreate(int EmpID)
        {
            try
            {
                List<object> jEntryList = new List<object>();
                List<object> list = new List<object>();
                //decimal? previousBal = 0;
                if (EmpID > 0)
                {
                    decimal empPettyCashBalance = db.tbl_Employee.Where(x => x.AccountID == EmpID).Select(x => x.EmployeePettyCashBalance).FirstOrDefault();// db.GetJEntryListEmployeeIDWise(EmpID).Select(x => new { Value = x.JEntryId, Name = x.JEntryId + " | " + x.Description ?? "-" }).ToList();

                    var pending_Purchases = db.tbl_EmployeePurchases.Where(x => x.EmployeeAccID == EmpID && x.StatusID == 1 && x.IsApproved != true && x.IsDeleted != true).ToList();


                    //jEntryList.AddRange(data);
                    //var jentryIds = data.Select(z => z.Value).ToList();
                    //previousBal = db.tbl_JEntry.Where(x => jentryIds.Contains(x.JEntryId) && x.IsClearedFromEmployee != true && (x.IsDeleted != true || x.IsDeleted == null)).ToList().Sum(x => x.Amount);
                    list.Add(new
                    {
                        //jentryList = jEntryList,
                        prevBal = empPettyCashBalance - Convert.ToDecimal(pending_Purchases?.Sum(x => x.TotalAmount) ?? 0),
                        //jentryIds = jentryIds
                    });

                }

                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(-2, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetJentryAmountIdWise(int JEntryID)
        {
            try
            {
                if (JEntryID > 0)
                {
                    var data = db.tbl_JEntry.Where(x => x.JEntryId == JEntryID && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => x.Amount).FirstOrDefault();
                    if (data != null && data > 0)
                    {
                        return Json(data, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(-1, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(-2, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetCashBalance()
        {
            try
            {
                decimal Balance = 0;
                var data = db.GetCashDetailFilterWise(null, null, null).ToList();
                if (data != null && data.Count > 0)
                {
                    if (!data.Select(x => x.Balance).LastOrDefault().Equals("-"))
                    {
                        Balance = Convert.ToDecimal(data.Select(x => x.Balance).LastOrDefault());
                    }

                }
                return Json(Balance, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(-2, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetApprovedPurchaseDataForEdit(int OrderID)
        {
            try
            {
                List<object> list = null;
                if (OrderID > 0)
                {
                    var data = db.tbl_EmployeePurchases.Where(x => x.OrderID == OrderID && x.IsDeleted != true && x.IsApproved == true).FirstOrDefault();
                    if (data != null)
                    {
                        list = new List<object>();

                        list.Add(new
                        {
                            EmployeeID = data.EmployeeAccID,
                            OrderDate = data.Date,
                            Description = data.Description,
                            TotalAmount = data.TotalAmount,
                            JEntryID = data.JEntryID,
                            JEntryVoucher = data.JEntryID != null ? db.tbl_JEntry.Where(t => t.JEntryId == data.JEntryID).Select(d => d.JEntryId.ToString() + " | " + d.Description).FirstOrDefault() : "-",
                            EmployeeSettledAmount = data.JEntryID != null ? db.tbl_JEntry.Where(q => q.JEntryId == data.JEntryID).Select(s => s.Amount).FirstOrDefault() : 0,
                            rows = data.tbl_EmployeePurchaseDetails.Select(x => new
                            {
                                productID = x.ProductID,
                                qty = x.Qty,
                                expenseID = x.ExpenseID,
                                expenseName = x.ExpenseID != null ? db.tbl_AccountDetails.Where(z => z.AccountID == x.ExpenseID).Select(p => p.AccountName).FirstOrDefault() : "-",

                                cost = x.UnitPrice,
                                total = x.Total,
                                prodDesc = x.Description
                            }).ToList()
                        });
                    }
                }
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetPurchaseDataForApproval(int OrderID)
        {
            try
            {
                List<object> list = null;
                if (OrderID > 0)
                {
                    var data = db.tbl_EmployeePurchases.Where(x => x.OrderID == OrderID && x.IsDeleted != true && x.IsApproved == false).FirstOrDefault();
                    if (data != null)
                    {
                        list = new List<object>();

                        list.Add(new
                        {
                            EmployeeID = data.EmployeeAccID,
                            OrderDate = data.Date,
                            Description = data.Description,
                            TotalAmount = data.TotalAmount,
                            JEntryID = data.JEntryID,
                            JEntryVoucher = data.JEntryID != null ? db.tbl_JEntry.Where(t => t.JEntryId == data.JEntryID).Select(d => d.JEntryId.ToString() + " | " + d.Description).FirstOrDefault() : "-",
                            EmployeeSettledAmount = data.JEntryID != null ? db.tbl_JEntry.Where(q => q.JEntryId == data.JEntryID).Select(s => s.Amount).FirstOrDefault() : 0,
                            rows = data.tbl_EmployeePurchaseDetails.Select(x => new
                            {
                                productID = x.ProductID,
                                qty = x.Qty,
                                expenseID = x.ExpenseID,
                                expenseName = x.ExpenseID != null ? db.tbl_AccountDetails.Where(z => z.AccountID == x.ExpenseID).Select(p => p.AccountName).FirstOrDefault() : "-",

                                cost = x.UnitPrice,
                                total = x.Total,
                                prodDesc = x.Description
                            }).ToList()
                        });
                    }
                }
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EmployeePurchases(bool? btn, int? AccountID, DateTime? fromDate, DateTime? toDate, int? InvoiceStatus)
        {
            //ViewBag.RedirectLink = "/Sales";
            if (btn.HasValue)
            {
                List<GetEmployeePurchases_Result> data = db.GetEmployeePurchases(null, AccountID, fromDate, toDate, InvoiceStatus).ToList();
                if (data.Count > 0)
                {
                    return PartialView("_EmployeePurchases", data.OrderByDescending(so => so.OrderID));
                }

                return PartialView("_EmployeePurchases");
            }

            //var data = db.tbl_SalesOrder1.Where(a => a.IsActive == true && a.IsDeleted == false).ToList();
            ViewBag.employee = db.tbl_Employee.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            return View();
        }

        public FileResult Export(int id)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/EmployeePurchaseInvoice.rdlc");


            ReportDataSource rs = new ReportDataSource();
            rs.Name = "EmployeePurchaseInvoice";
            rs.Name = "EmployeePurchaseInvoiceData";
            rs.Value = db.EmpPurchaseInvoiceNew(id);

            lo.DataSources.Add(rs);


            byte[] renderbyte;

            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            //Response.AddHeader("Content-Disposition", "attachment; filename=PurchaseInvoice.pdf");

            return new FileContentResult(renderedBytes, mimeType);

        }

        public JsonResult SaveApproveEmployeePurchase(int EmployeePurchaseID, Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, Models.DTO.JEntryLog jentryLog, bool isChecked)
        {
            if (ModelState.IsValid)
            {

                return Json(AdjustmentEntry.ApproveEmployeePurchase(EmployeePurchaseID, model, detailList, jentryLog), JsonRequestBehavior.AllowGet);


            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }
        public JsonResult SaveEditedEmployeePurchase(int EmployeePurchaseID, Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, Models.DTO.JEntryLog jentryLog, bool isChecked)
        {
            if (ModelState.IsValid)
            {

                return Json(AdjustmentEntry.EditApprovedEmployeePurchase(EmployeePurchaseID, model, detailList, jentryLog), JsonRequestBehavior.AllowGet);


            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }

        public JsonResult SaveEmployeePurchase(Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, List<Models.DTO.JEntryLog> jentryLog, bool isChecked)
        {
            if (ModelState.IsValid)
            {

                return Json(AdjustmentEntry.CreateEmployeePurchase(model, detailList, jentryLog), JsonRequestBehavior.AllowGet);


            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }

        //  Save Payment without invoices 
        public JsonResult SaveCashPayment(Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, Models.DTO.JEntryLog jentryLog, bool isChecked)
        {
            if (ModelState.IsValid)
            {
                if (isChecked)
                {
                    return Json(AdjustmentEntry.SaveCashPayment(model, jentryLog), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(AdjustmentEntry.SaveEmployeePurchase(model, detailList, jentryLog), JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }

        public JsonResult EditEmployeeCashPayment(Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, Models.DTO.JEntryLog jentryLog, bool isChecked)
        {
            if (ModelState.IsValid)
            {

                return Json(AdjustmentEntry.EditCashPayment(model, jentryLog), JsonRequestBehavior.AllowGet);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }

        public JsonResult GetEmployeeEditCashPaymentData(int? JEntryId)
        {
            if (JEntryId != null && JEntryId > 0)
            {
                var data = db.tbl_JEntry.Where(x => x.JEntryId == JEntryId).FirstOrDefault();
                if (data != null)
                {
                    var list = new
                    {
                        Desc = data.Description,
                        Amount = data.Amount,
                        Date = data.VoucherDate.Value.ToShortDateString().ToString(),
                        AccountId = data.RefID
                    };
                    return Json(list, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        // Rec cash from emp
        public JsonResult SaveSettleEmpolyeePayments(Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, Models.DTO.JEntryLog jentryLog, bool isChecked)
        {
            if (ModelState.IsValid)
            {

                return Json(AdjustmentEntry.SaveSettleEmpolyeePayments(model, jentryLog), JsonRequestBehavior.AllowGet);


            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }

        public ActionResult GetEmployeePaymentReportFilterWise(bool? btn, DateTime? fromDate, DateTime? toDate, int? BranchID,int? AccountID)
        {
            if (btn.HasValue)
            {
                List<GetEmployeePayReportFilterWise_Result> data = db.GetEmployeePayReportFilterWise(BranchID, fromDate, toDate,AccountID).ToList();

                if (data.Count == 0)
                {
                    return PartialView("_EmployeePayReport");
                }
                else
                {
                    return PartialView("_EmployeePayReport", data);
                }

            }
            ViewBag.employee = db.tbl_Employee.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();

            return View();
        }


        [HttpPost]
        public JsonResult DataList(DTParameters param, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {
            int TotalCount = 0;
            decimal OpBal = 0;
            decimal ClBal = 0;
            var filtered = this.GetFilteredData(param.Search.Value, param.SortOrder, param.Start, param.Length, out TotalCount, AccountID, fromDate, toDate);

            var dataList = (filtered != null && filtered.Count > 0) ? filtered.Select(i => new StatementDTO()
            {
                Date = i.Date,
                AccName = i.AccName ?? "-",
                Description = i.Description ?? "-",
                Detail = i.Detail ?? "-",
                PayStatus = i.PayStatus ?? "-",
                Aging = i.Aging != null ? i.Aging.ToString() : "",
                Dr = i.Dr,
                Cr = i.Cr,
                Balance = i.Balance,
                OpeningBalance = i.OpeningBalance,
                detailList = i.detailList

            }) : null;
            TotalCount = db.GetVendorPaymentDateWise(AccountID, fromDate, toDate, null).Count();

            //OpBal = db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).FirstOrDefault().OpBalance??0;
            //ClBal = (db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance == "-" || string.IsNullOrWhiteSpace(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance)  ) ? 0 :  Convert.ToDecimal(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance);
            DTResult<StatementDTO> finalresult = new DTResult<StatementDTO>
            {
                draw = param.Draw,
                data = (dataList != null && dataList.Count() > 0) ? dataList.ToList() : new List<StatementDTO>(),
                recordsFiltered = TotalCount,
                recordsTotal = filtered.Count,
                customData = new
                {
                    //OpBal = vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Select(x => x.OpBalance).FirstOrDefault(),
                    //ClBal = (vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault() == "-" || string.IsNullOrWhiteSpace(vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault())) ? 0 : Convert.ToDecimal(vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault()),
                    //TotalDr = vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Debit) <= 0 ? 0 : Convert.ToDecimal(vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Debit)),
                    //TotalCr = vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Credit) <= 0 ? 0 : Convert.ToDecimal(vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Credit))
                    OpBal = Helper.DigitsWithComma(vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Select(x => x.OpBalance).FirstOrDefault()),
                    ClBal = (vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault() == "-" || string.IsNullOrWhiteSpace(vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault())) ? "0" : Helper.DigitsWithComma(Convert.ToDecimal(vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault())),
                    TotalDr = vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Debit) <= 0 ? "0" : Helper.DigitsWithComma(Convert.ToDecimal(vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Debit))),
                    TotalCr = vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Credit) <= 0 ? "0" : Helper.DigitsWithComma(Convert.ToDecimal(vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Credit)))
                }

            };

            //ViewBag.OpBal = (dataList != null && dataList.Count() > 0)?dataList.FirstOrDefault().OpeningBalance:"0";
            //ViewBag.ClBal = (dataList != null && dataList.Count() > 0)?dataList.LastOrDefault().Balance:"0";

            return Json(finalresult);

        }

        public List<StatementDTO> GetFilteredData(string search, string sortOrder, int start, int length, out int TotalCount, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {
            //var result = db.tbl_Product.OrderBy(p => p.ProductID).Include(t => t.tbl_VehicleCode).Include(t => t.tbl_Brand).Where(p => (search == null || (p.PartNo != null && p.PartNo.ToLower().Contains(search.ToLower())
            //    || p.BarCode != null && p.BarCode.Contains(search)
            //    || p.tbl_VehicleCode.VehicleCode != null && p.tbl_VehicleCode.VehicleCode.ToLower().Contains(search.ToLower())
            //    || p.tbl_Brand.BrandTitle != null && p.tbl_Brand.BrandTitle.ToLower().Contains(search.ToLower())
            //    || p.Product_Code != null && p.Product_Code.ToLower().Contains(search.ToLower())
            //    || p.UnitCode != null && p.UnitCode.ToLower().Contains(search.ToLower())
            //    || p.SaleRate != null && p.SaleRate.ToString().Contains(search.ToString())
            //    ))).Skip(start).Take(length).ToList();
            //TotalCount = db.tbl_Product.Count();
            var result = vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, null).Where(p => (search == null || (p.VoucherName != null && p.VoucherName.ToLower().Contains(search.ToLower())
                  || p.Detail != null && p.Detail.Contains(search)
                  //|| p.Aging != null && p.Aging.ToLower().Contains(search.ToLower())
                  //|| p.Balance != null && p.Balance.ToLower().Contains(search.ToLower())
                  //|| p.Product_Code != null && p.Product_Code.ToLower().Contains(search.ToLower())
                  //|| p.UnitCode != null && p.UnitCode.ToLower().Contains(search.ToLower())
                  //|| p.SaleRate != null && p.SaleRate.ToString().Contains(search.ToString())
                  ))).Skip(start).Take(length).ToList();
            TotalCount = db.GetVendorPaymentDateWise(AccountID, fromDate, toDate, null).Count();
            List<StatementDTO> dataList = new List<StatementDTO>();
            int counter = 0;
            foreach (var i in result)
            {

                Models.DTO.StatementDTO data = new StatementDTO();

                data.Date = i.VoucherDate != null ? Convert.ToString(string.Format("{0:dd/MM/yyyy}", i.VoucherDate)) : "-";
                data.AccName = i.VoucherName ?? "-";
                // data.Description = i.Description ?? "-";
                data.Detail = i.Detail ?? "-";
                // data.PayStatus = i.PaymentStatus ?? "-";
                // data.Aging = i.Aging != null ? i.Aging.ToString() : "";
                data.Dr = Helper.DigitsWithComma(i.Debit);//Convert.ToDecimal(i.Debit).ToString("#,##0.00");
                data.Cr = Helper.DigitsWithComma(i.Credit);//Convert.ToDecimal(i.Credit).ToString("#,##0.00");
                data.Balance = string.IsNullOrWhiteSpace(i.Balance) ? "-" : Helper.DigitsWithComma(Convert.ToDecimal(i.Balance));//Convert.ToDecimal(i.Balance).ToString("#,##0.00");
                data.OpeningBalance = i.OpBalance <= 0 ? "-" : Helper.DigitsWithComma(Convert.ToDecimal(i.OpBalance)); ; //Convert.ToDecimal(i.OpBalance).ToString("#,##0.00");
                data.detailList = StatementBLL.GetItemDetailsForStatementJenTryIDWise(i.JEntryId) != null ? StatementBLL.GetItemDetailsForStatementJenTryIDWise(i.JEntryId) : new List<Details>();


                dataList.Add(data);
                counter = counter + 1;
            }
            //Mapper.CreateMap<tbl_Product, Models.DTO.ProductListCategory>();
            //var details = Mapper.Map<ICollection<tbl_Product>, ICollection<Models.DTO.ProductListCategory>>(result);
            return dataList;
        }

        public ActionResult Statement(int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            //int branchId = 0;

            //if (Session["BranchID"] != null)
            //{
            //    branchId = Convert.ToInt32(Session["BranchID"]);
            //}
            //else
            //{
            //    var user = User.Identity;
            //    string currentUserId = User.Identity.GetUserId();
            //    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
            //    branchId = currentUser.BranchID;
            //}
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.vendorList = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode + "|" + v.Name }).ToList();
            if (AccountID > 0)
            {
                List<GetVendorPaymentDateWise_Result> vendor = vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, BranchID);
                if (vendor.Count == 0)
                {
                    return View("Index");
                }
                else
                    return PartialView("_CustomVendorStatement", vendor);
            }
            return View("Index");
        }

        public JsonResult Search(int? AccountID, DateTime? fromDate, DateTime? toDate)
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.vendorList = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode + "|" + v.Name }).ToList();
            if (AccountID > 0 && fromDate != null && toDate != null)
            {
                List<GetVendorPaymentDateWise_Result> vendor = vpay.GetVendorPaymentsDateWise(AccountID, fromDate, toDate, branchId);
                return Json(vendor);
            }
            return Json("");
        }

    }
}
