﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;

namespace POSOman.Controllers
{
    public class CylinderController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: Cylinder
        public ActionResult Index()
        {
            var tbl_Cylinder = db.tbl_Cylinder.Where(x=>x.IsDeleted!=true).Include(t => t.tbl_PrintingMachine);
            return View(tbl_Cylinder.ToList());
        }

        // GET: Cylinder/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Cylinder tbl_Cylinder = db.tbl_Cylinder.Find(id);
            if (tbl_Cylinder == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Cylinder);
        }

        // GET: Cylinder/Create
        public ActionResult Create()
        {
            ViewBag.PrintingMachineID = new SelectList(db.tbl_PrintingMachine.Where(x=> x.IsDeleted != true), "ID", "PrintingMachine");
            return View();
        }

        // POST: Cylinder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_Cylinder tbl_Cylinder)
        {
            if (ModelState.IsValid)
            { 
                db.tbl_Cylinder.Add(tbl_Cylinder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PrintingMachineID = new SelectList(db.tbl_PrintingMachine.Where(x=> x.IsDeleted != true), "ID", "PrintingMachine", tbl_Cylinder.PrintingMachineID);
            return View(tbl_Cylinder);
        }

        // GET: Cylinder/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Cylinder tbl_Cylinder = db.tbl_Cylinder.Find(id);
            if (tbl_Cylinder == null)
            {
                return HttpNotFound();
            }
            ViewBag.PrintingMachineID = new SelectList(db.tbl_PrintingMachine.Where(x=> x.IsDeleted != true), "ID", "PrintingMachine", tbl_Cylinder.PrintingMachineID);
            return View(tbl_Cylinder);
        }

        // POST: Cylinder/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_Cylinder tbl_Cylinder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Cylinder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PrintingMachineID = new SelectList(db.tbl_PrintingMachine.Where(x=> x.IsDeleted != true), "ID", "PrintingMachine", tbl_Cylinder.PrintingMachineID);
            return View(tbl_Cylinder);
        }

        // GET: Cylinder/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Cylinder tbl_Cylinder = db.tbl_Cylinder.Find(id);
            if (tbl_Cylinder == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Cylinder);
        }

        // POST: Cylinder/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Cylinder model = db.tbl_Cylinder.Find(id);
            if (model != null)
            {
                model.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("Cylinder", "Record not found");
            return View(model);
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
