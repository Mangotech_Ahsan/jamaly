﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using AutoMapper;
using POSOman.Models.DTO;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class StockController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
       
        ApplicationDbContext context;

        //Stock Consumption
       // [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult StockConsumption()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            //ViewBag.BranchID = branchId;
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName");
          ViewBag.Product = db.tbl_Product.Where(x => x.VehicleCodeID != 1).Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.Desc = db.tbl_Product.Where(x => x.VehicleCodeID != 1).Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();

            ViewBag.VehCode = db.tbl_VehicleCode.Where(x => x.VehicleCodeID != 1).Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
           
            return View();
        }

        public JsonResult SaveStkConsumption(List<StockLog> order)
        {
            using (var t = db.Database.BeginTransaction())
            {
                try
                {
                    if (order.Count > 0 && order.All(x => x.BranchID > 0) && order.All(x=>x.ProductID>0 && x.StockOut>0))
                    {
                        int? BranchID = order.First().BranchID;
                        List<int> lstProdIds = order.Select(p => p.ProductID).ToList();
                        // get all stock where productid contains lstProdIds
                        var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == BranchID).ToList();
                        List<tbl_Stock> existingStock = lstStock.ToList();
                        foreach (var item in order)
                        {
                            var rStockOut = item.StockOut ?? 0;
                            var rowCount = 0;
                            var stockBatchID = 0;
                            var CostPrice = 0m;
                            decimal COGS = 0;
                            decimal stLogQty = 0;

                            var VehID = db.tbl_Product.Where(x => x.ProductID == item.ProductID).Select(x => x.VehicleCodeID).FirstOrDefault();


                            #region FIFO 
                            var stockBatch = db.GetProductFIFO(item.ProductID, item.StockOut, item.BranchID).ToList();
                            //foreach (var st in stockBatch)
                            //if(stockBatch.Count > 0)                            
                            while (rStockOut > 0)
                            {
                                if (stockBatch.Count > 0 && rowCount < stockBatch.Count)
                                {
                                    var StockBatch = stockBatch[rowCount];
                                    var sbRow = db.tbl_StockBatch.Where(b => b.StockBatchID == StockBatch.StockBatchID).FirstOrDefault();
                                    var Order = db.tbl_StockLog.Where(sl => sl.OrderTypeID == 1 && sl.StockBatchID == sbRow.StockBatchID).FirstOrDefault();

                                    if (rStockOut > StockBatch.Qty)
                                    {
                                        stLogQty = sbRow.Qty;
                                        COGS += sbRow.Qty * sbRow.CostPrice;
                                        rStockOut -= sbRow.Qty;
                                        sbRow.Qty = 0;
                                        db.Entry(sbRow).State = EntityState.Modified;

                                    }
                                    else if (rStockOut < StockBatch.Qty)
                                    {
                                        stLogQty = rStockOut;
                                        COGS += rStockOut * sbRow.CostPrice;
                                        sbRow.Qty -= rStockOut;
                                        db.Entry(sbRow).State = EntityState.Modified;
                                        rStockOut = 0;
                                    }
                                    else if (rStockOut == StockBatch.Qty)
                                    {
                                        stLogQty = rStockOut;
                                        COGS += rStockOut * sbRow.CostPrice;
                                        sbRow.Qty = 0;
                                        db.Entry(sbRow).State = EntityState.Modified;
                                        rStockOut = 0;
                                    }
                                    stockBatchID = sbRow.StockBatchID;
                                    CostPrice = sbRow.CostPrice;
                                    if (Order != null)
                                    {
                                        var PurchaseOrder = db.tbl_PurchaseOrder.Where(po => po.OrderID == Order.OrderID).FirstOrDefault();
                                        PurchaseOrder.Can_Modify = false;
                                        db.Entry(PurchaseOrder).State = EntityState.Modified;
                                    }

                                    db.SaveChanges();
                                }
                                else
                                {
                                    stLogQty = rStockOut;
                                    rStockOut = 0;
                                }
                                if (stLogQty > 0)
                                {
                                    tbl_StockLog stLog = new tbl_StockLog();
                                    stLog.AccountID = item.AccountID;
                                    stLog.StockBatchID = stockBatchID;
                                    stLog.AddBy = 1;
                                    stLog.AddOn = DateTime.UtcNow.AddHours(5);
                                    stLog.BranchID = item.BranchID;
                                    stLog.CostPrice = CostPrice;
                                    stLog.InvoiceDate = Helper.PST();
                                    stLog.IsActive = true;
                                    // stLog.IsMinor = item.IsMinor;
                                    // stLog.IsOpen = item.IsOpen;
                                    // stLog.IsPack = item.IsPack;
                                    // stLog.LevelID = item.LevelID;
                                    // stLog.MinorDivisor = item.MinorDivisor;
                                    // stLog.OrderID = iOrderID;
                                    stLog.OrderTypeID = 13;
                                    stLog.OutReference = "Consumed Stock";
                                    // stLog.OutReferenceID = ;
                                    stLog.ProductID = item.ProductID;
                                    stLog.SalePrice = 1;
                                    stLog.StockIN = 0;
                                    stLog.ReturnedQty = 0;
                                    stLog.StockOut = stLogQty;
                                    // stLog.UnitCode = item.UnitCode;
                                    //stLog.UnitID = item.UnitID;
                                    stLog.UnitPerCarton = 1;
                                    //stLog.UserReferenceID = item.UserReferenceID;
                                    db.tbl_StockLog.Add(stLog);
                                    rowCount += 1;
                                }
                            }

                            #endregion
                            #region Update CostPrice in Stock 
                            decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == BranchID).Sum(p => (decimal?)p.CostPrice * p.Qty) ?? 0m);
                            int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == BranchID).Sum(p => (decimal?)p.Qty) ?? 0m);

                            decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;

                            #endregion
                            if (VehID != 1)
                            {
                                //  if already exists stock

                                var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
                                if (row != null)
                                {
                                    if (avgCost > 0)
                                    { row.CostPrice = avgCost; }
                                    row.Qty -= Convert.ToInt32(item.StockOut);
                                    row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                    row.BranchID = item.BranchID;
                                    row.Location = item.Location;
                                    db.Entry(row).State = EntityState.Modified;
                                }
                                else
                                {
                                    // if new item is not In Stock and User is Selling (As Discussed with Boss) 
                                    tbl_Stock newStock = new tbl_Stock();
                                    newStock.ProductID = item.ProductID;
                                    newStock.Qty = Convert.ToDecimal(item.StockOut * (-1));
                                    newStock.SalePrice = item.SalePrice;
                                    newStock.Addon = DateTime.UtcNow.AddHours(5);
                                    newStock.Location = item.Location;
                                    newStock.BranchID = item.BranchID;
                                    newStock.CostPrice = item.CostPrice;
                                    newStock.OnMove = 0;
                                    db.tbl_Stock.Add(newStock);
                                }

                            }
                        }
                        int ID = db.SaveChanges();
                        t.Commit();
                        return Json(ID, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        t.Rollback();


                        return Json("false", JsonRequestBehavior.AllowGet);
                    }
                   


                   
                }
                catch(Exception c)
                {
                    t.Rollback();


                    return Json(c.Message, JsonRequestBehavior.AllowGet);
                }
            }
          
        }

       [Authorize]
        public ActionResult StockConsumpionList(bool? btn,  DateTime? fromDate, DateTime? toDate, int? ProductID, int? VehicleCodeID, int? BranchID, string Description)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            //  ViewBag.customer = db.tbl_Customer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            // ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            //  ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.Product = db.tbl_Product.Where(x=>x.VehicleCodeID!=1).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            if (btn.HasValue)
            {
                List<GetConsumedStockFilterWise_Result> stock = db.GetConsumedStockFilterWise(ProductID, BranchID,null, fromDate, toDate,null).ToList();
                if (stock.Count == 0)
                {
                    return PartialView("_StockConsumpionList");
                }
                else
                    return PartialView("_StockConsumpionList", stock);
            }
            return View();
        }

        // GET: Stock
       // [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult Index(bool? btn,int? ProductID,int? VehicleCodeID, int? GroupID,int? ModelID, int? BranchID, int? DepartmentID, string Product_Code, string Description, string PartNo)
        {
            if(Product_Code =="")
            { Product_Code = null; }
            if (Description == "")
            { PartNo = null; }
            if (PartNo == "")
            { Description = null; }

            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PartNo = db.tbl_Product.Where(x=>x.IsGeneralItem == true).Select(v => new { Value = v.ProductID, Name = "C-"+v.ProductID }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.Department = db.tbl_Department.Select(v => new { Value = v.ID, Name = v.Department }).ToList();
            if (btn.HasValue)
            {
                 List<GetStockFilterWise_Result> stock = db.GetStockFilterWise(ProductID, GroupID, VehicleCodeID, ModelID, Product_Code,Description, BranchID,PartNo, DepartmentID).Where(x=>x.Qty>0).ToList(); 
                
                if (stock.Count == 0)
                {
                    return PartialView("_StockFilter");
                }
                else
                {
                    return PartialView("_StockFilter", stock.Where(x=>x.IsItem == false));
                }
            }

            return View();
        }

        public ActionResult StockItemIndex(bool? btn, int? ProductID, int? VehicleCodeID, int? GroupID, int? ModelID, int? BranchID, int? DepartmentID, string Product_Code, string Description, string PartNo)
        {
            if (Product_Code == "")
            { Product_Code = null; }
            if (Description == "")
            { PartNo = null; }
            if (PartNo == "")
            { Description = null; }

            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PartNo = db.tbl_Product.Where(x=>x.IsOffsetItem == false && x.IsGeneralItem == false).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.Department = db.tbl_Department.Select(v => new { Value = v.ID, Name = v.Department }).ToList();
            if (btn.HasValue)
            {
                List<GetStockFilterWise_Result> stock = db.GetStockFilterWise(ProductID, GroupID, VehicleCodeID, ModelID, Product_Code, Description, BranchID, PartNo, DepartmentID).ToList();

                if (stock.Count == 0)
                {
                    return PartialView("_StockItemIndex");
                }
                else
                {
                    return PartialView("_StockItemIndex", stock.Where(x => x.IsItem == true));
                }
            }

            return View();
        }

        // get Stock By Expiry 
        // [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult StockExpiry(bool? btn, int? ProductID, int? VehicleCodeID, int? BrandID, int? BranchID, string BarCode, DateTime? fromDate, DateTime? toDate)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            if (BarCode == "")
            { BarCode = null; }
            
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.Brand = db.tbl_Brand.Select(v => new { Value = v.BrandID, Name = v.BrandTitle }).ToList();
            if (btn.HasValue)
            {
                List<GetStockExpiryFilterWise_Result> stock = db.GetStockExpiryFilterWise(ProductID, BrandID, VehicleCodeID, BarCode,  BranchID,fromDate,toDate).ToList();

                //var data = ProductList(param, stock);
                if (stock.Count == 0)
                {
                    return PartialView("_StockExpiry");
                }
                else
                    return PartialView("_StockExpiry", stock);
            }
            return View();
        }

        // Get Packets Stock Report date Wise 
        //public ActionResult GetPacketStockReport(bool? btn, int? VehicleCodeID, DateTime? fromDate, DateTime? toDate)
        //{
        //    ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
        //    if (btn.HasValue)
        //    {
        //        List<GetPacketStockReportDateWise_Result> products = db.GetPacketStockReportDateWise(VehicleCodeID, fromDate, toDate).ToList();
        //        if (products.Count == 0)
        //        {
        //            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
        //            return PartialView("_PacketStockReport");
        //        }
        //        else
        //            return PartialView("_PacketStockReport", products);
        //    }
        //    return View("PacketStockReport");
        //}
        // Get Stock Report date Wise 
        public ActionResult GetStockReport(bool? btn,int? VehicleCodeID, DateTime? fromDate, DateTime? toDate)
        {
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            if (btn.HasValue)
            {
                List<GetStockReportDateWise_Result> products = db.GetStockReportDateWise(VehicleCodeID,fromDate, toDate).ToList();
                if (products.Count == 0)
                {
                    ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
                    return PartialView("_StockReport");
                }
                else
                    return PartialView("_StockReport", products);
            }
            return View("StockReport");
        }
        //get Products on behalf of category
        public JsonResult GetProductsList(int? VehicleCodeID)
        {
            List<object> objectList = new List<object>();

            if (VehicleCodeID > 0)
            {
                var qry = db.tbl_Product.Where(p => p.VehicleCodeID == VehicleCodeID).Select(x => new { Value = x.ProductID, Name = x.PartNo }).ToList();
                // var qry = db.tbl_Maker.Select(x => new { Value = x.MakeID, Name = x.MakeName }).ToList();
                //var qry1 = db.tbl_Stock.Where(p => p.ProductID == ProductID).FirstOrDefault();
                if (qry != null)
                {
                    objectList.Add(new
                    {
                        qry = qry
                    });
                }
            }
            else
            {
                var qry = db.tbl_Product.Select(x => new { Value = x.ProductID, Name = x.PartNo }).ToList();
                // var qry = db.tbl_Maker.Select(x => new { Value = x.MakeID, Name = x.MakeName }).ToList();
                //var qry1 = db.tbl_Stock.Where(p => p.ProductID == ProductID).FirstOrDefault();
                if (qry != null)
                {
                    objectList.Add(new
                    {
                        qry = qry
                    });

                }
            }
            return Json(objectList, JsonRequestBehavior.AllowGet);
        }
        #region Quick Stock Pagination
        public ActionResult FullStock()
        {           
            return View();
        }
        [HttpPost]
        public JsonResult ProductList(DTParameters param)
        {
            List<GetStockFilterWise_Result> stock = db.GetStockFilterWise(null,null,null,null,"","",null,"",null).ToList();

            int TotalCount = 0;
            TotalCount = stock.Count();
            //TotalCount = result.Count;

            stock = stock.Skip(param.Start).Take(param.Length).ToList();

           
            DTResult<GetStockFilterWise_Result> finalresult = new DTResult<GetStockFilterWise_Result>
            {
                draw = param.Draw,
                data = stock,
                recordsFiltered = TotalCount,
                recordsTotal = stock.Count

            };                       
            return Json(finalresult);

        }
        #region Quick Stock with Search

        public JsonResult StockList(DTParameters param)
        {

            var filtered = new List<QuickStock>();
            var result = new List<tbl_Stock>();
            int TotalCount = 0;
            //if (param.Length == -1)
            //{
            //    result = db.tbl_Stock.OrderBy(p => p.ProductID).Include(t => t.tbl_Product.tbl_VehicleModel).Include(t => t.tbl_Product.tbl_Group).Include(t => t.tbl_Product.tbl_VehicleCode).ToList();                

            //    TotalCount = result.Count();

            //    var StockList = result.Select(p => new QuickStock()
            //    {
            //        ProductID = p.ProductID,
            //        VehicleCode = (p.tbl_Product.VehicleCodeID.HasValue) ? p.tbl_Product.tbl_VehicleCode.VehicleCode : "",
            //        GroupName = (p.tbl_Product.GroupID.HasValue) ? p.tbl_Product.tbl_Group.GroupName : "",
            //        VehicleModel = (p.tbl_Product.VehicleModelID.HasValue) ? p.tbl_Product.tbl_VehicleModel.VehicleName : "",
            //        PartNo = p.tbl_Product.PartNo,
            //        Description = p.tbl_Product.Description,
            //        Location = p.Location,
            //        BranchName = p.tbl_Branch.BranchName,
            //        Qty = p.Qty,
            //        CostPrice = p.CostPrice,
            //        SalePrice = p.SalePrice
            //    });
            //    DTResult<QuickStock> finalresult = new DTResult<QuickStock>
            //    {
            //        draw = param.Draw,
            //        data = StockList.ToList(),
            //        recordsFiltered = TotalCount,
            //        recordsTotal = result.Count

            //    };
            //    return Json(finalresult);
            //}
            //else
            {
                filtered = this.GetStockFiltered(param.Search.Value, param.SortOrder, param.Start, param.Length, out TotalCount);
                var StockList = filtered.Select(p => new QuickStock()
                {
                    ProductID = p.ProductID,
                    VehicleCode = (p.tbl_Product.VehicleCodeID.HasValue) ? p.tbl_Product.tbl_VehicleCode.VehicleCode : "",
                    PartNo = p.tbl_Product.PartNo,
                    Description = p.tbl_Product.Description,
                    Location = p.Location,
                    BranchName = p.tbl_Branch.BranchName,
                    Qty = p.Qty,
                    CostPrice = p.CostPrice,
                    SalePrice = p.SalePrice
                });
                DTResult<QuickStock> finalresult = new DTResult<QuickStock>
                {
                    draw = param.Draw,
                    data = StockList.ToList(),
                    recordsFiltered = TotalCount,
                    recordsTotal = filtered.Count

                };

                return Json(finalresult);
            }        
        }
        public List<QuickStock> GetStockFiltered(string search, string sortOrder, int start, int length, out int TotalCount)
        {            
            
                var result = db.tbl_Stock.OrderBy(p => p.ProductID).Include(t => t.tbl_Product.tbl_VehicleCode).Where(p => (search == null || (p.tbl_Product.PartNo != null && p.tbl_Product.PartNo.ToLower().Contains(search.ToLower())
               || p.tbl_Product.Description != null && p.tbl_Product.Description.ToLower().Contains(search.ToLower())
               || p.tbl_Product.tbl_VehicleCode.VehicleCode != null && p.tbl_Product.tbl_VehicleCode.VehicleCode.ToLower().Contains(search.ToLower())
                            
                || p.Location != null && p.Location.ToLower().Contains(search.ToLower())
               || p.Qty.ToString().Equals(search.ToString())
               ))
               ).Skip(start).Take(length).ToList();
                TotalCount = db.tbl_Stock.Count();

                result = result.ToList();//.Skip(start).Take(length).ToList();


                Mapper.CreateMap<tbl_Stock, Models.DTO.QuickStock>();
                var details = Mapper.Map<ICollection<tbl_Stock>, ICollection<Models.DTO.QuickStock>>(result);
                return details.ToList();
            
        }
        #endregion
        public List<ProductList> GetProductFiltered(string search, string sortOrder, int start, int length, out int TotalCount)
        {


            var result = db.tbl_Product.OrderBy(p => p.ProductID).Skip(start).Take(length).Include(t => t.tbl_VehicleCode).Where(p => (search == null || (p.PartNo != null && p.PartNo.ToLower().Contains(search.ToLower())
                || p.Description != null && p.Description.ToLower().Contains(search.ToLower())
                || p.tbl_VehicleCode.VehicleCode != null && p.tbl_VehicleCode.VehicleCode.ToLower().Contains(search.ToLower())
                || p.Product_Code != null && p.Product_Code.ToLower().Contains(search.ToLower())
                || p.UnitCode != null && p.UnitCode.ToLower().Contains(search.ToLower())
                || p.SaleRate != null && p.SaleRate.ToString().Contains(search.ToString())
                ))
                ).ToList();
            TotalCount = db.tbl_Product.Count();
            //TotalCount = result.Count;

            result = result.ToList();//.Skip(start).Take(length).ToList();
            
            Mapper.CreateMap<tbl_Product, Models.DTO.ProductList>();
            var details = Mapper.Map<ICollection<tbl_Product>, ICollection<Models.DTO.ProductList>>(result);
            return details.ToList();
        }
        #endregion
        // GET: Stock/Details/5
        public ActionResult Details(int? id, int? BranchID)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var _StockLog = new List<Models.DTO.StockLogDetails>();
            //var qry = (from st in db.tbl_StockLog
            //           join pro in db.tbl_Product on st.ProductID equals pro.ProductID
            //           join ort in db.tbl_OrderType on st.OrderTypeID equals ort.OrderTypeID
            //           join acd in db.tbl_AccountDetails on st.AccountID equals acd.AccountID
            //           into gj
            //           from v in gj.DefaultIfEmpty() // Use Composite key Table in Future 
            //           where st.ProductID == id && st.BranchID == BranchID && st.IsActive != false
            //           select new
            //           {
            //               Name = (v == null ? String.Empty : v.AccountName),
            //               //Name        = st.Name,
            //               InvoiceDate = st.InvoiceDate,
            //               PartNo = pro.PartNo,
            //               StockIN = st.StockIN,
            //               StockOut = st.StockOut,
            //               CostPrice = st.CostPrice,
            //               SalePrice = st.SalePrice,
            //               Location = st.Location,
            //               OrderType = ort.OrderType,
            //               OrderTypeID = st.OrderTypeID,
            //               OrderID = st.OrderID,
            //               Description = pro.Description,
            //               UnitCode = st.UnitCode,
            //               UnitID = st.UnitID,
            //               LevelID = st.LevelID,
            //               UnitPerCarton = st.UnitPerCarton

            //           }).ToList();

            var qry = db.GetStockProductDetailsFilterWise(id, BranchID).ToList();
            // Map list to class
            foreach (var s in qry)
            {
                _StockLog.Add(new Models.DTO.StockLogDetails
                {
                    Name = s.AccountName,
                    InvoiceDate = s.InvoiceDate,
                    Location = s.Location,
                    PartNo = s.PartNo,
                    CostPrice = s.CostPrice,
                    Description = s.Description,
                    OrderType = s.OrderType,
                    SalePrice = s.SalePrice,
                    StockIN = s.StockIN,
                    QtyBalance = s.QtyBalance??"-",
                    StockOut = s.StockOut,
                    OrderTypeID = s.OrderTypeID,
                    OrderID = s.OrderID,
                    UnitCode = s.UnitCode,
                    UnitID = s.UnitID,
                    LevelID = s.LevelID,
                    UnitPerCarton = s.UnitPerCarton
                });
            }

            //List<tbl_StockLog> _StockLog = db.tbl_StockLog.Where(p => p.ProductID == id).ToList();
            if (_StockLog == null)
            {
                return HttpNotFound();
            }
            return View(_StockLog);
        }
        //public ActionResult Details(int? id,int? BranchID)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var _StockLog = new List<Models.DTO.StockLogDetails>();
        //    var qry = (from st in db.tbl_StockLog
        //                    join pro in db.tbl_Product on st.ProductID equals pro.ProductID
        //                    join ort in db.tbl_OrderType on st.OrderTypeID equals ort.OrderTypeID
        //               join acd in db.tbl_AccountDetails on st.AccountID equals acd.AccountID
        //               into gj from v in gj.DefaultIfEmpty() // Use Composite key Table in Future 
        //               where st.ProductID == id && st.BranchID == BranchID && st.IsActive != false
        //               select new
        //                      {                                  
        //                           Name = (v == null ? String.Empty : v.AccountName),                                  
        //                          //Name        = st.Name,
        //                          InvoiceDate = st.InvoiceDate,
        //                          PartNo      = pro.PartNo,
        //                          StockIN     = st.StockIN,
        //                          StockOut    = st.StockOut,
        //                          CostPrice   = st.CostPrice,
        //                          SalePrice   = st.SalePrice,
        //                          Location    = st.Location,
        //                          OrderType   = ort.OrderType,
        //                          OrderTypeID = st.OrderTypeID,
        //                          OrderID = st.OrderID,
        //                          Description = pro.Description,
        //                          UnitCode = st.UnitCode,
        //                          UnitID = st.UnitID,
        //                          LevelID = st.LevelID,
        //                          UnitPerCarton = st.UnitPerCarton

        //                      }).ToList();
        //    // Map list to class
        //    foreach (var s in qry)
        //    {
        //        _StockLog.Add(new Models.DTO.StockLogDetails { Name = s.Name, InvoiceDate = s.InvoiceDate,
        //        Location = s.Location,
        //        PartNo =    s.PartNo,
        //        CostPrice = s.CostPrice,
        //        Description = s.Description,
        //        OrderType = s.OrderType,
        //        SalePrice = s.SalePrice,
        //        StockIN =   s.StockIN,
        //        StockOut =  s.StockOut,
        //        OrderTypeID = s.OrderTypeID,
        //        OrderID = s.OrderID,
        //            UnitCode = s.UnitCode,
        //            UnitID = s.UnitID,
        //            LevelID = s.LevelID,
        //            UnitPerCarton = s.UnitPerCarton
        //        });


        //    }

        //    //List<tbl_StockLog> _StockLog = db.tbl_StockLog.Where(p => p.ProductID == id).ToList();
        //    if ( _StockLog == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(_StockLog);
        //}
        // GET: Stock/Details/5
        public ActionResult FIFOStock(int? id, int? BranchID)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<tbl_StockBatch> _StockBatch = db.tbl_StockBatch.Where(p => p.ProductID == id && p.Qty > 0 && p.BranchID == BranchID).ToList();
            if (_StockBatch == null)
            {
                return HttpNotFound();
            }
            return View(_StockBatch);
        }
        #region Select2 pagination
        [HttpGet]
        public ActionResult GetStockProducts(string searchTerm, int pageSize, int pageNum, string countyId)
        {
            if (countyId != "" && countyId != null && countyId != string.Empty)
            {
                int vehCodeID = Convert.ToInt32(countyId);
                List<tbl_Stock> ObjList = null;
                if (searchTerm != null && searchTerm != "")
                {
                    ObjList = db.tbl_Stock.Where(p => p.tbl_Product.PartNo.StartsWith(searchTerm) && p.tbl_Product.VehicleCodeID == vehCodeID).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ObjList = db.tbl_Stock.Where(p => p.tbl_Product.VehicleCodeID == vehCodeID).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                //Getting items
                //var itemList = db.tbl_Product.Where(p => p.PartNo == (searchTerm)).ToList();
                var itemList = (from p in ObjList
                                    //where p.PartNo.StartsWith(searchTerm)
                                select new { PartNo = p.tbl_Product.PartNo, ProductID = p.ProductID });
                //Creating new object for return.
                var result = new
                {
                    Total = itemList.Count(),
                    //Results = itemList.Skip((pageNum * pageSize) - 100).Take(pageSize)
                    Results = itemList//.Skip((pageNum * pageSize) - 100).Take(pageSize)
                };

                return new JsonResult
                {
                    Data = result,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                List<tbl_Stock> ObjList = null;
                if (searchTerm != null && searchTerm != "")
                {
                    ObjList = db.tbl_Stock.Where(p => p.tbl_Product.PartNo.StartsWith(searchTerm)).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ObjList = db.tbl_Stock.OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                //Getting items
                //var itemList = db.tbl_Product.Where(p => p.PartNo == (searchTerm)).ToList();
                var itemList = (from p in ObjList
                                    //where p.PartNo.StartsWith(searchTerm)
                                select new { PartNo = p.tbl_Product.PartNo, ProductID = p.ProductID });
                //Creating new object for return.
                var result = new
                {
                    Total = itemList.Count(),
                    //Results = itemList.Skip((pageNum * pageSize) - 100).Take(pageSize)
                    Results = itemList//.Skip((pageNum * pageSize) - 100).Take(pageSize)
                };

                return new JsonResult
                {
                    Data = result,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }

        }
        #endregion

        // Get Stock for provided Product and Branch 
        public JsonResult getStock(int ProductID, int BranchID)
        {
            List<object> objectList = new List<object>();
            int stockQty = 0;
            decimal cost = 0;
            decimal SalePrice = 0;
            if (ProductID > 0 && BranchID > 0)
            {
                var qry = db.tbl_Stock.Where(p => p.ProductID == ProductID && p.BranchID == BranchID).FirstOrDefault();
                var qry1 = db.tbl_Stock.Where(p => p.ProductID == ProductID).FirstOrDefault();
                if (qry != null)
                {
                    stockQty = (int)(qry.Qty);
                    if (qry.CostPrice != null)
                    { cost = (decimal)(qry.CostPrice); }
                    if (qry.SalePrice != null)
                    { SalePrice = (decimal)(qry.SalePrice); }

                }
                if (cost == 0 && qry1!=null)
                {
                    if (qry1.CostPrice == null)
                    {
                        cost = 0;
                    }
                    else
                        cost = (decimal)(qry1.CostPrice);
                }
            }
            objectList.Add(new
            {
                stock = stockQty,
                        costPrice = cost,
                        SalePrice = SalePrice
                    });     
            return Json(objectList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getBranches()
        {
            var qry = db.tbl_Branch.Select(p => new { Value = p.BranchID, Name = p.BranchName }).ToList();
            return Json(qry);
        }
        // Get Stock detail for Stock Movement
       // [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public JsonResult getStockForUpdatePrice(int? iProductID)
        {
            context = new ApplicationDbContext();
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
            int branchId = currentUser.BranchID;
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var role = UserManager.GetRoles(user.GetUserId());

            try
            {
                if (iProductID > 0)
                {
                    if (role[0].ToString() == "SuperAdmin")
                    {
                        var qry = db.tbl_Stock.Where(s => s.ProductID == iProductID) // for branch && s.BranchID == branchId)
                        .Select(s => new
                        {
                            s.StockID,
                            s.Location,
                            s.ProductID,
                            s.tbl_Product.PartNo,
                            s.tbl_Product.tbl_VehicleCode.VehicleCode,
                            s.tbl_Product.UnitCode,
                            s.tbl_Product.UnitPerCarton,
                            s.tbl_Product.LevelID,
                            s.tbl_Product.MinorDivisor,
                            s.tbl_Product.Description,
                            s.tbl_Branch.BranchName,
                            s.BranchID,
                            s.OnMove,
                            s.Qty,
                            s.SalePrice,
                            s.CostPrice
                        }).ToList();
                        return Json(new { qry }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var qry = db.tbl_Stock.Where(s => s.ProductID == iProductID && s.BranchID == branchId)
                        .Select(s => new
                        {
                            s.StockID,
                            s.Location,
                            s.ProductID,
                            s.tbl_Product.PartNo,
                            s.tbl_Product.tbl_VehicleCode.VehicleCode,
                            s.tbl_Product.UnitCode,
                            s.tbl_Product.UnitPerCarton,
                            s.tbl_Product.LevelID,
                            s.tbl_Product.MinorDivisor,
                            s.tbl_Product.Description,
                            s.tbl_Branch.BranchName,
                            s.BranchID,
                            s.OnMove,
                            s.Qty,
                            s.SalePrice,
                            s.CostPrice
                        }).ToList();
                        return Json(new { qry }, JsonRequestBehavior.AllowGet);
                    }
                }
                else if(iProductID == -1)
                {
                    {
                        if (role[0].ToString() == "SuperAdmin")
                        {
                            var qry = db.tbl_Stock // for branch && s.BranchID == branchId)
                            .Select(s => new
                            {
                                s.StockID,
                                s.Location,
                                s.ProductID,
                                s.tbl_Product.PartNo,
                                s.tbl_Product.tbl_VehicleCode.VehicleCode,
                                s.tbl_Product.UnitCode,
                                s.tbl_Product.UnitPerCarton,
                                s.tbl_Product.LevelID,
                                s.tbl_Product.MinorDivisor,
                                s.tbl_Product.Description,
                                s.tbl_Branch.BranchName,
                                s.BranchID,
                                s.OnMove,
                                s.Qty,
                                s.SalePrice,
                                s.CostPrice
                            }).ToList();
                            return Json(new { qry }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var qry = db.tbl_Stock.Where(s => s.BranchID == branchId)
                            .Select(s => new
                            {
                                s.StockID,
                                s.Location,
                                s.ProductID,
                                s.tbl_Product.PartNo,
                                s.tbl_Product.tbl_VehicleCode.VehicleCode,
                                s.tbl_Product.UnitCode,
                                s.tbl_Product.UnitPerCarton,
                                s.tbl_Product.LevelID,
                                s.tbl_Product.MinorDivisor,
                                s.tbl_Product.Description,
                                s.tbl_Branch.BranchName,
                                s.BranchID,
                                s.OnMove,
                                s.Qty,
                                s.SalePrice,
                                s.CostPrice
                            }).ToList();
                            return Json(new { qry }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
            
            return Json("");
        }
        // Get Stock detail for Stock Movement
      //  [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public JsonResult getStockDetail(int iProductID)
        {
            context = new ApplicationDbContext();
            var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                int branchId = currentUser.BranchID;
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var role = UserManager.GetRoles(user.GetUserId());
            if (iProductID > 0)
            {
                try
                {
                    if (role[0].ToString() == "SuperAdmin" || role[0].ToString() == "Admin")
                    {
                        var qry = db.tbl_Stock.Where(s => s.ProductID == iProductID) // for branch && s.BranchID == branchId)
                        .Select(s => new
                        {
                            s.StockID,
                            s.Location,
                            s.ProductID,
                            s.tbl_Product.PartNo,
                            s.tbl_Product.tbl_VehicleCode.VehicleCode,
                            s.tbl_Product.IsPacket,
                            s.tbl_Product.UnitCode,
                            s.tbl_Product.UnitPerCarton,
                            s.tbl_Product.LevelID,
                            s.tbl_Product.MinorDivisor,
                            s.tbl_Product.Description,
                            s.tbl_Product.OpenUnitCode,
                            s.tbl_Product.LeastUnitCode,
                            s.tbl_Branch.BranchName,
                            s.BranchID,
                            s.OnMove,
                            s.Qty,
                            s.SalePrice,
                            s.CostPrice
                        }).ToList();
                        return Json(new { qry }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var qry = db.tbl_Stock.Where(s => s.ProductID == iProductID && s.BranchID == branchId)
                        .Select(s => new
                        {
                            s.StockID,
                            s.Location,
                            s.ProductID,
                            s.tbl_Product.PartNo,
                            s.tbl_Product.tbl_VehicleCode.VehicleCode,
                            s.tbl_Product.IsPacket,
                            s.tbl_Product.UnitCode,
                            s.tbl_Product.UnitPerCarton,
                            s.tbl_Product.LevelID,
                            s.tbl_Product.MinorDivisor,
                            s.tbl_Product.Description,
                            s.tbl_Product.OpenUnitCode,
                            s.tbl_Product.LeastUnitCode,
                            s.tbl_Branch.BranchName,
                            s.BranchID,
                            s.OnMove,
                            s.Qty,
                            s.SalePrice,
                            s.CostPrice
                        }).ToList();
                        return Json(new { qry }, JsonRequestBehavior.AllowGet);
                    }
                    
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }
            return Json("");
        }
        // get Pending Stock Movement 
      //  [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public JsonResult getPendingStock(int iBranchID)
        {
            context = new ApplicationDbContext();
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
            int branchId = currentUser.BranchID;
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var role = UserManager.GetRoles(user.GetUserId());
            if (iBranchID > 0)
            {
                try
                {
                    if (role[0].ToString() == "SuperAdmin" || role[0].ToString() == "Admin")
                    {
                        var qry = db.tbl_StockMoving.Where(s => s.isReceived == false)                        
                        .Select(s => new
                        {
                            s.ID,
                            s.Location,
                            s.ProductID,
                            s.tbl_Product.PartNo,
                            s.tbl_Product.tbl_VehicleCode.VehicleCode,
                            s.tbl_Product.Description,
                            s.tbl_Product.IsPacket,
                            s.tbl_Product.UnitCode,
                            s.tbl_Product.UnitPerCarton,
                            s.tbl_Product.LevelID,
                            s.tbl_Product.MinorDivisor,
                            s.tbl_Product.OpenUnitCode,
                            s.tbl_Product.LeastUnitCode,
                            FromBranchName = s.tbl_Branch.BranchName,
                            ToBranchName = s.tbl_Branch1.BranchName,
                            s.FromBranchID,
                            s.StockMoved,
                            s.StockReceived,
                            s.ToBranchID,
                            s.CostPrice,
                            s.SalePrice
                        }).ToList();
                        return Json(new { qry }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var qry = db.tbl_StockMoving.Where(s => s.isReceived == false && s.ToBranchID == branchId)
                        .Select(s => new
                        {
                            s.ID,
                            s.Location,
                            s.ProductID,
                            s.tbl_Product.PartNo,
                            s.tbl_Product.tbl_VehicleCode.VehicleCode,
                            s.tbl_Product.Description,
                            s.tbl_Product.IsPacket,
                            s.tbl_Product.UnitCode,
                            s.tbl_Product.UnitPerCarton,
                            s.tbl_Product.LevelID,
                            s.tbl_Product.MinorDivisor,
                            s.tbl_Product.OpenUnitCode,
                            s.tbl_Product.LeastUnitCode,
                            FromBranchName = s.tbl_Branch.BranchName,
                            ToBranchName = s.tbl_Branch1.BranchName,
                            s.FromBranchID,
                            s.StockMoved,
                            s.StockReceived,
                            s.ToBranchID,
                            s.CostPrice,
                            s.SalePrice

                        }).ToList();
                        return Json(new { qry }, JsonRequestBehavior.AllowGet);
                    }                    
                    
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }
            return Json("");
        }
        // GET: Stock/Create
        public ActionResult Create()
        {            
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "Code");
            ViewBag.ProductID = new SelectList(db.tbl_Product, "ProductID", "PartNo");
            return View();
        }

        // POST: Stock/Create
        
       
        // Go to Receiving Qty Moved from Other Branches
      //  [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult ReceiveStock()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BranchID = branchId;
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
           // ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            return View();
        }
        public ActionResult StockMovement()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BranchID = branchId;
            ViewBag.VehCode = db.tbl_VehicleCode.Where(x=>x.VehicleCodeID != 1).Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            //ViewBag.PartNo = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            return View();
        }
        // Update Price 
        public ActionResult UpdateStockPrice()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BranchID = branchId;
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            return View();
        }
        // Move Stock , Add in Moving Branch to and Less from Moving Branch From 
        public JsonResult MoveStock(List<Models.DTO.StockMoving> modelStockMoving)
        {
            POSOman.Models.BLL.MoveStock moveStock = new Models.BLL.MoveStock();            
            object result = moveStock.SaveStockMovement(modelStockMoving);
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Stock Moving".ToString());
            return Json(result);
        }
        // Update Sale Price 
        public JsonResult UpdateSalePrice(List<Models.DTO.StockPriceDTO> modelStockPrice)
        {
            context = new ApplicationDbContext();
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
            int userID = currentUser.UserId;
            POSOman.Models.BLL.MoveStock moveStock = new Models.BLL.MoveStock();
            object result = moveStock.UpdateSalePrice(modelStockPrice,userID);
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Sale Price Update".ToString());
            return Json(result);
        }
        // Receive Stock and Update Stock 
        public JsonResult SaveStockReceiving(List<Models.DTO.StockMoveIn> modelStockLogIn, List<Models.DTO.StockMoveOut> modelStockLogOut, List<Models.DTO.StockMoving> modelStockMoving)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            modelStockMoving.FirstOrDefault().ReceivedBy = userID;
            POSOman.Models.BLL.MoveStock moveStock = new Models.BLL.MoveStock();
            object result = moveStock.SaveStockReceiving(modelStockLogIn, modelStockMoving);
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Stock Recieving".ToString());

            return Json(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
