﻿using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.BLL;
using System;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Drawing;
using ZXing;

namespace POSOman.Controllers
{
    public class OnlineOrdersController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        ApplicationDbContext context;
        // GET: SalesOrder
        [Authorize]
        public ActionResult ProcessedOrders(int? Filter, bool? isNew)
        {
            int branchId = 0;
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var s = UserManager.GetRoles(user.GetUserId());
            var userRole = s[0].ToString();
            #region SuperAdmin
            if (userRole == "SuperAdmin" && isNew == null && Filter == null)
            {
                return View();
            }
            else if (userRole == "SuperAdmin" && isNew == null && Filter == 1) // ALL
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.SOID > 1000 && so.IsDeleted != true && so.OnlineOrderID > 0).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if (userRole == "SuperAdmin" && isNew == null && Filter == 2) // Today 
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.SOID > 1000 && so.IsDeleted != true && so.OnlineOrderID > 0 && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month && DbFunctions.TruncateTime(so.SalesDate).Value.Day == DateTime.Now.Day).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if (userRole == "SuperAdmin" && isNew == null && Filter == 3) // This Month
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.SOID > 1000 && so.IsDeleted != true && so.OnlineOrderID > 0 && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if (userRole == "SuperAdmin" && isNew == null && Filter == 4) // This Year
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.SOID > 1000 && so.IsDeleted != true && so.OnlineOrderID > 0 && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if (userRole == "SuperAdmin" && isNew == true)
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && so.OnlineOrderID > 0 && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            #endregion
            #region Other USers

            if (userRole != "SuperAdmin" && isNew == null && Filter == null)
            {
                return View();
            }
            else if (userRole != "SuperAdmin" && isNew == null && Filter == 1) // ALL
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && so.OnlineOrderID > 0 && so.UserID == currentUserId).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if (userRole != "SuperAdmin" && isNew == null && Filter == 2) // Today 
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && so.OnlineOrderID > 0 && so.UserID == currentUserId && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month && DbFunctions.TruncateTime(so.SalesDate).Value.Day == DateTime.Now.Day).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if (userRole != "SuperAdmin" && isNew == null && Filter == 3) // This Month
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && so.OnlineOrderID > 0 && so.UserID == currentUserId && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if (userRole != "SuperAdmin" && isNew == null && Filter == 4) // This Year
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && so.OnlineOrderID > 0 && so.UserID == currentUserId && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            else if (userRole != "SuperAdmin" && isNew == true)
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.BranchID == branchId && so.SOID > 1000 && so.IsDeleted != true && so.OnlineOrderID > 0 && so.UserID == currentUserId && DbFunctions.TruncateTime(so.SalesDate).Value.Year == DateTime.Now.Year && DbFunctions.TruncateTime(so.SalesDate).Value.Month == DateTime.Now.Month).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
            #endregion
            return View();
        }
       
        [Authorize(Roles = "SuperAdmin,Admin,OnlineOrders")]
        public ActionResult Index()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var onlineOrders = db.tbl_OnlineOrder.Where(q => q.IsDelivered == false && q.PaymentMode != "Pending").Include(t => t.tbl_AccountDetails).OrderByDescending(q => q.OrderID);
            return View(onlineOrders.ToList());
        }
        public ActionResult printOrder(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<GetOnlineSalesByOrderID_Result> details = db.GetOnlineSalesByOrderID(id).ToList();
            var SaleOrder = db.tbl_SalesOrder.Where(x => x.OrderID == id).FirstOrDefault();
            details = details.Where(so => so.Qty > 0).ToList();
            if (!string.IsNullOrWhiteSpace(SaleOrder.Barcode))
            {
                RenderBarcode(SaleOrder.Barcode.ToString(), SaleOrder.QRCode);
                ViewBag.BarCodeImg = Session["BCImage"];
                ViewBag.BarCodeID = SaleOrder.Barcode;
            }
            else
            {
                ViewBag.BarCodeImg = null;
            }
            if (details == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Invoice", details);
        }
        //BarCode   

        public void RenderBarcode(string Code, string QRCode)
        {
            Image img = null;
            using (var ms = new MemoryStream())
            {
                var writer = new BarcodeWriter() { Format = BarcodeFormat.CODE_128 };
                writer.Options.Height = 80;
                writer.Options.Width = 280;
                writer.Options.PureBarcode = true;
                img = writer.Write(Code);
                // save image in files
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                Session["BCImage"] = "data:image/jpeg;base64," + Convert.ToBase64String(ms.ToArray());
            }
        }
        // GET: Quotation/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            List<tbl_OnlineOrderDetail> _Details = db.tbl_OnlineOrderDetail.Where(p => p.OrderID == id).ToList();
            if (_Details == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Print", _Details);
        }
       
        // Submit orders in inventory and forward it for delivery 
        public ActionResult SubmitOrders(int OrderID)
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName");
            ViewBag.UserBranchID = branchId;
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_OnlineCustomer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();            
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Product = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            ViewBag.OrderID = OrderID;
           
                return View();
        }
        // Get Online Orders for Submission 
        public JsonResult getOrderDetails(int OrderID)
        {
            if (OrderID > 0)
            {
                try
                {
                    int branchId = 0;

                    if (Session["BranchID"] != null)
                    {
                        branchId = Convert.ToInt32(Session["BranchID"]);
                    }
                    else
                    {
                        var user = User.Identity;
                        string currentUserId = User.Identity.GetUserId();
                        var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                        branchId = currentUser.BranchID;
                    }

                    var qry = db.tbl_OnlineOrderDetail.Where(p => p.OrderID == OrderID)
                        .Select(p => new
                        {
                            p.tbl_OnlineOrder.SOID,
                            p.tbl_OnlineOrder.BranchID,
                            p.OrderID,
                            p.ProductID,
                            p.tbl_Product.PartNo,
                            p.tbl_Product.Description,
                            p.tbl_Product.tbl_VehicleCode.VehicleCode,
                            p.tbl_Product.Product_Code,
                            p.tbl_Product.Location,
                            p.tbl_OnlineOrder.AccountID,
                            p.tbl_OnlineOrder.OrderDate,
                            p.tbl_OnlineOrder.AmountPaid,
                            p.tbl_OnlineOrder.TotalAmount,
                            p.Qty,                            
                            p.UnitPrice,
                            p.tbl_Product.UnitCode,                            
                            p.SalePrice,
                            p.Total
                        }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }

            }

            return Json("");
        }

    }
}