﻿using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.BLL;
using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class DailyPOSCashDepositController : Controller
    {
        POSCashDepositBLL deposit = new POSCashDepositBLL();
        dbPOS db = new dbPOS();

        // GET: DailyPOSCashDeposit
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult DepositList()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();


            var Deposit = db.tbl_JDetail.Where(j => (j.EntryTypeID == 37) && j.Dr > 0).OrderBy(x => x.JEntryID);
            return View(Deposit.ToList());
        }

        //Create Deposit
        public ActionResult Deposit()
        {
            try
            {
                ViewBag.BranchID = new SelectList(db.tbl_Branch.Where(br => br.IsShop == true), "BranchID", "BranchName");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        //Save Deposit
        public JsonResult SaveDepositEntry(DepositDTO model)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                model.AddBy = userID;
                {
                    object Result = deposit.SaveDeposit(model, branchId);                   
                    return Json(Result);
                }
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }

        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult WithDrawList()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            var WithDraw = db.tbl_JDetail.Where(j => (j.EntryTypeID == 38) && j.Cr > 0).OrderBy(x => x.JEntryID);
            return View(WithDraw.ToList());
        }

        //Create WithDraw
        public ActionResult WithDraw()
        {
            try
            {
                ViewBag.BranchID = new SelectList(db.tbl_Branch.Where(br => br.IsShop == true), "BranchID", "BranchName");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        //Save WithDraw
        public JsonResult SaveWithDrawEntry(DepositDTO model)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                model.AddBy = userID;
               
              
                    object Result = deposit.SaveDrawing(model, branchId);
                    return Json(Result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }
        }
    }
}