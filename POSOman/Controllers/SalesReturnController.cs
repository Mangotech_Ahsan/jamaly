﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using AutoMapper;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Reporting.WebForms;

namespace POSOman.Controllers
{
    public class SalesReturnController : Controller
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        

        private int GetBranchID()
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        // GET: SalesReturn
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            int branchId = GetBranchID();

            return View(db.tbl_SalesReturn.Where(sr => sr.BranchID == branchId).ToList().OrderByDescending(p => p.SalesReturnID));
        }

        // Get Sales Orders to return
        public ActionResult SalesReturn()
        {
            ApplicationDbContext context;
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BranchID = branchId;
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            //ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();  
            context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var s = UserManager.GetRoles(user.GetUserId());
            var userRole = s[0].ToString();
            if (userRole == "SuperAdmin" || userRole == "Admin")
            {

                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.IsReturned == false && so.SOID > 1000 && so.IsDeleted != true).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.OrderByDescending(a => a.OrderID).ToList());
            }
            else
            {
                var tbl_SalesOrder = db.tbl_SalesOrder.Where(so => so.IsReturned == false && so.SOID > 1000 && so.IsDeleted != true && so.UserID == currentUserId).Include(t => t.tbl_AccountDetails).Include(t => t.tbl_Branch).Include(t => t.tbl_PaymentTypes);
                return View(tbl_SalesOrder.ToList());
            }
        }
        // Get Return Order Details 
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //  //tbl_PurchaseOrder purchaseOrder = db.tbl_PurchaseOrder.Find(id);
            ////  List<tbl_SOReturnDetails> _SORDetails = db.tbl_SOReturnDetails.Where(p => p.ReturnID == id).ToList();




            //  if (_SORDetails == null)
            //  {
            //      return HttpNotFound();
            //  }
            //  return PartialView("_Print",_SORDetails);

            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/SaleReturnReport.rdlc");


            ReportDataSource rs = new ReportDataSource();
            rs.Name = "DataSaleReturn";
            rs.Value = db.SaleReturnDetail(id);

            lo.DataSources.Add(rs);


            byte[] renderbyte;

            string deviceInfo = "<DeviceInfo>" +
    "<OutputFormat>PDF</OutputFormat>" +
    "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            Response.AddHeader("Content-Disposition",
             "attachment; filename=UnAssignedLevels.pdf");

            return new FileContentResult(renderedBytes, mimeType);

        }
        // Get Sales Detail of selected invoice 
        public JsonResult getInvoiceDetails(int orderID)
        {
            if (orderID > 0)
            {
                try
                {
                    int branchId = 0;
                    if (Session["BranchID"] != null)
                    {
                        branchId = Convert.ToInt32(Session["BranchID"]);
                    }
                    else
                    {
                        var user = User.Identity;
                        string currentUserId = User.Identity.GetUserId();
                        var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                        branchId = currentUser.BranchID;
                    }
                    var qry = db.tbl_SaleDetails.Where(s => s.OrderID == orderID && s.IsReturned != true)
                        .Select(s => new
                        {
                            s.OrderID,
                            s.tbl_SalesOrder.tbl_Branch.BranchName,
                            s.tbl_SalesOrder.SOID,
                            s.ProductID,
                            s.tbl_Product.PartNo,
                            s.tbl_SalesOrder.AccountID,
                            s.tbl_SalesOrder.BranchID,
                            s.tbl_SalesOrder.PONo,
                            s.tbl_SalesOrder.IsPaid,
                            s.tbl_SalesOrder.PaymentTypeID,
                            s.tbl_SalesOrder.PaymentStatus,
                            s.tbl_SalesOrder.VAT,
                            s.tbl_SalesOrder.TotalAmount,
                            s.tbl_SalesOrder.DiscountAmount,
                            s.tbl_SalesOrder.ReturnAmount,
                            s.tbl_SalesOrder.AmountPaid,
                            s.Qty,
                            s.Packet,
                            s.UnitCode,
                            s.UnitID,
                            s.LevelID,
                            s.IsPack,
                            s.IsOpen,
                            s.MinorDivisor,
                            s.IsMinor,
                            s.UnitPerCarton,
                            s.SalePrice,
                            s.UnitPrice,    // CostPrice
                            s.Total,
                            s.ReturnedQty
                        }).ToList();
                    var totalQty = qry.Sum(s => s.Qty);
                    var discountPerItem = qry[0].DiscountAmount/totalQty;                    
                    return Json(new { qry , discountPerItem}, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }
            return Json("");
        }
        public JsonResult ReturnOrder(Models.DTO.SalesReturn model, List<Models.DTO.StockLog> modelStockLog,int? bankAccId)
        {
            try
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                model.AddBy = userID;
                POSOman.Models.BLL.SalesReturn returnOrder = new Models.BLL.SalesReturn();
                object result = returnOrder.Save(model, modelStockLog,bankAccId,model.BranchID);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Sales Return Action".ToString());

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }
    }
}
