﻿using POSOman.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class CancelOrdersController : Controller
    {
        private dbPOS db = new dbPOS();
        // GET: CancelOrders
        public ActionResult Index()
        {
            return View();
        }
        // GET: CancelDeliveryOrders
        public ActionResult DOCancel()
        {
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            return View();
        }
        // GET: CancelSalesOrders
        public ActionResult SOCancel()
        {
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            return View();
        }
        public JsonResult getDODetails(string DOID,int BranchID)
        {
            if (DOID != "" && BranchID > 0)
            {
                try
                {                    
                    var qry = db.tbl_DODetails.Where(s => s.tbl_DeliveryOrder.DOID == DOID && (db.tbl_DOReturn.Any(dr => dr.tbl_DeliveryOrder.DOID == DOID && dr.BranchID == BranchID) == false) && s.BranchID == BranchID && s.tbl_DeliveryOrder.IsPaid != true)
                        .Select(s => new
                        {
                            s.OrderID,
                            s.tbl_DeliveryOrder.DOID,                            
                            s.ProductID,
                            s.tbl_Product.PartNo,
                            s.tbl_Product.Description,
                            s.tbl_DeliveryOrder.AccountID,
                            s.tbl_DeliveryOrder.tbl_AccountDetails.AccountName,
                            s.tbl_DeliveryOrder.BranchID,
                            s.tbl_DeliveryOrder.tbl_Branch.BranchName,
                            s.tbl_DeliveryOrder.TotalAmount,
                            s.tbl_DeliveryOrder.SalesDate,
                            s.Qty,
                            s.SalePrice,
                            s.Total
                        }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }
            return Json("");
        }
        public JsonResult getSODetails(int SOID, int BranchID)
        {
            if (SOID > 0 && BranchID > 0)
            {
                int salesOrderID = 0;
                int AccountID = 0;
                try
                {                   
                    
                    var qry = db.tbl_SaleDetails.Where(s => s.tbl_SalesOrder.SOID == SOID &&(db.tbl_SalesReturn.Any(sr => sr.tbl_SalesOrder.SOID == SOID && sr.BranchID == BranchID) == false) && s.BranchID == BranchID)
                        .Select(s => new
                        {
                            s.OrderID,
                            s.tbl_SalesOrder.SOID,
                            s.ProductID,
                            s.tbl_Product.PartNo,
                            s.tbl_Product.Description,
                            s.tbl_SalesOrder.AccountID,
                            s.tbl_SalesOrder.tbl_AccountDetails.AccountName,
                            s.tbl_SalesOrder.BranchID,
                            s.tbl_SalesOrder.tbl_Branch.BranchName,
                            s.tbl_SalesOrder.TotalAmount,
                            s.tbl_SalesOrder.SalesDate,
                            s.Qty,
                            s.SalePrice,
                            s.Total
                        }).ToList();
                    if (qry.Count > 0)
                    {
                        salesOrderID = qry[0].OrderID;
                        AccountID = qry[0].AccountID;
                    }
                    var jentryLog = db.tbl_JEntryLog.Where(jl => jl.OrderID == salesOrderID && jl.OrderTypeID == 3).ToList().Count;
                    var jentry = db.tbl_JEntryLog.Where(jl => jl.OrderTypeID == 3 && jl.BranchID == BranchID && jl.OrderID == salesOrderID).Select(jl => jl.JEntryID).FirstOrDefault();
                    var unpaidJentry = db.tbl_JDetail.Where(jd => jd.tbl_JEntry.RefID == salesOrderID && jd.BranchID == BranchID && jd.tbl_JEntry.VoucherName == "Sales" && jd.AccountID == AccountID).Select(s => new
                    {
                        s.tbl_AccountDetails.AccountName,                        
                        //s.tbl_JEntry.tbl_Branch.BranchName,
                        s.JEntryID,
                        s.tbl_JEntry.VoucherDate,
                        s.Detail,
                        s.AccountID,
                        s.Dr,
                        s.Cr                        
                    }).ToList();
                    var paidJentry = db.tbl_JDetail.Where(jd => jd.JEntryID == jentry && jd.AccountID == AccountID).Select(s => new
                    {
                        s.tbl_AccountDetails.AccountName,
                        //s.tbl_JEntry.tbl_Branch.BranchName,
                        s.JEntryID,
                        s.tbl_JEntry.VoucherDate,
                        s.Detail,
                        s.AccountID,
                        s.Dr,
                        s.Cr
                    }).ToList();
                    var cheque = db.tbl_Cheques.Where(chq => chq.JEntryId == jentry && chq.BranchID == BranchID && chq.ChequeTypeID == 
                    1).Select(s => new
                    {
                        s.tbl_Branch.BranchName,
                        s.ChequeNumber,
                        s.ChequeID,
                        s.ChequeDate,
                        s.tbl_JEntry.VoucherDate,
                        s.Description,
                        s.Amount                        
                    }).ToList();
                    if (jentryLog > 1)
                    {
                        qry = null;
                        unpaidJentry = null;
                        paidJentry = null;
                        cheque = null;
                    }
                    return Json(new { qry = qry, JentryUP = unpaidJentry, JentryPaid = paidJentry , Cheque = cheque }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }
            return Json("");
        }
        public JsonResult DeleteOrder(Models.DTO.DOReturn model)
        {
            try
            {                
                POSOman.Models.BLL.DeleteOrders deleteOrder = new Models.BLL.DeleteOrders();
                object result = deleteOrder.DeleteDO(model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }
        //Delete SalesOrder
        public JsonResult DeleteSalesOrder(Models.DTO.SalesReturn model, int SalesOrderID, int SOID, int BranchID, int? ChequeID, int UPJEntryID, int? PaidJEntryID)
        {
            try
            {
                POSOman.Models.BLL.DeleteOrders deleteOrder = new Models.BLL.DeleteOrders();
                object result = deleteOrder.DeleteSO(model, SalesOrderID, SOID, BranchID, ChequeID, UPJEntryID, PaidJEntryID);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }

    }
}