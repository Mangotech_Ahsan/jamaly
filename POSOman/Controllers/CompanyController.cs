﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;

namespace POSOman.Controllers
{
    public class CompanyController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: Company
        public ActionResult Index()
        {
            var tbl_Company = db.tbl_Company.Include(t => t.AspNetUser);
            return View(tbl_Company.ToList());
        }

        // GET: Company/Details/5
        public ActionResult Details(int? id)
        {


            int cmp = db.tbl_Company.Select(p => p.CompanyID).FirstOrDefault();



            id = new int?(cmp); // Just to be clear :)

            // These are equivalent
            int ids = (int)id;
            cmp = id.Value;


            if (id == null)
            {
                // return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                return RedirectToAction("Create");

            }
            tbl_Company tbl_Company = db.tbl_Company.Find(id);
            if (tbl_Company == null)
            {
                return RedirectToAction("Create");
            }
            return View(tbl_Company);
        }

        // GET: Company/Create
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email");

           var data=  db.tbl_Company.Select(p => p.CompanyID).ToList();
            if(data.Count == 0) { 
            return View();
            }
            return View("Details");
        }

        // POST: Company/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CompanyID,Name,Address,WebSite,Landline,Description,Cell,Capital,Email,CompanyCode,IDNo,Picture,isActive,AddOn,Addby,UpdateOn,UpdateBy,DeleteOn,DeleteBy,IsDeleted,UserID,rowguid")] tbl_Company tbl_Company)
        {
            if (ModelState.IsValid)
            {

                if (Request.Files.Count > 0 && Request.Files["img"].ContentLength > 0)
                {
                    HttpPostedFileBase postFile = Request.Files.Get("img");
                    // var fileName = Path.GetFileName(postFile.FileName);
                    string imgName = tbl_Company.IDNo + Path.GetFileName(postFile.FileName);
                    string imgPath = "/Content/Logo/" + imgName;
                    postFile.SaveAs(Server.MapPath(imgPath));
                    tbl_Company.Picture = imgPath;

                }


                db.tbl_Company.Add(tbl_Company);

                int poId = db.SaveChanges();
                int iOrderID = tbl_Company.CompanyID;

                ViewBag.Picture = db.tbl_Company.Where(p => p.CompanyID == iOrderID).Select(p => p.Picture).FirstOrDefault();






                return RedirectToAction("Details/" + iOrderID);


            }

            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", tbl_Company.UserID);
            return View(tbl_Company);
        }

        // GET: Company/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Company tbl_Company = db.tbl_Company.Find(id);
            if (tbl_Company == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", tbl_Company.UserID);
            return View(tbl_Company);
        }

        // POST: Company/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CompanyID,Name,Address,WebSite,Landline,Description,Cell,Capital,Email,CompanyCode,IDNo,Picture,isActive,AddOn,Addby,UpdateOn,UpdateBy,DeleteOn,DeleteBy,IsDeleted,UserID,rowguid")] tbl_Company tbl_Company)
        {

            if (ModelState.IsValid)
            {
                db.Entry(tbl_Company).State = EntityState.Modified;
                if (Request.Files.Count > 0 && Request.Files["img"].ContentLength > 0)
                {
                    HttpPostedFileBase postFile = Request.Files.Get("img");
                    // var fileName = Path.GetFileName(postFile.FileName);
                    string imgName = tbl_Company.IDNo + Path.GetFileName(postFile.FileName);
                    string imgPath = "/Content/Logo/" + imgName;
                    postFile.SaveAs(Server.MapPath(imgPath));
                    tbl_Company.Picture = imgPath;
                }
                else { db.Entry(tbl_Company).Property(e => e.Picture).IsModified = false; }
                db.Entry(tbl_Company).Property(e => e.Addby).IsModified = false;
                db.Entry(tbl_Company).Property(e => e.AddOn).IsModified = false;
              


                int poId = db.SaveChanges();
                int iOrderID = tbl_Company.CompanyID;



                return RedirectToAction("Details/" + iOrderID);
            }


            if (ModelState.IsValid)
            {
                db.Entry(tbl_Company).State = EntityState.Modified;
              
                int poId = db.SaveChanges();
                int iOrderID = tbl_Company.CompanyID;



                return RedirectToAction("Details/" + iOrderID);
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", tbl_Company.UserID);
            return View(tbl_Company);
        }

        // GET: Company/Delete/5
        public ActionResult Delete(int? id)
        {
         


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Company tbl_Company = db.tbl_Company.Find(id);
            if (tbl_Company == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Company);
        }

        // POST: Company/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Company tbl_Company = db.tbl_Company.Find(id);
            db.tbl_Company.Remove(tbl_Company);
            int poId = db.SaveChanges();
            int iOrderID = tbl_Company.CompanyID;



            return RedirectToAction("Details/" + iOrderID);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}