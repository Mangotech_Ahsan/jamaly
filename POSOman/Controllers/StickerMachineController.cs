﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class StickerMachineController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: StickerMachine
        public ActionResult Index()
        {
            return View(db.tbl_StickerMachine.Where(x=>x.IsDeleted!=true).ToList());
        }

        // GET: StickerMachine/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerMachine tbl_StickerMachine = db.tbl_StickerMachine.Find(id);
            if (tbl_StickerMachine == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerMachine);
        }

        // GET: StickerMachine/Create
        public ActionResult Create()
        {
            ViewBag.StickerMachineType = db.tbl_StickerMachineType.Select(x => new { ID = x.ID, Name = x.Type }).ToList();
            return View();
        }

        // POST: StickerMachine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_StickerMachine tbl_StickerMachine)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(tbl_StickerMachine.MachineName))
                {
                    ViewBag.StickerMachineType = db.tbl_StickerMachineType.Select(x => new { ID = x.ID, Name = x.Type }).ToList();
                    ModelState.AddModelError("MachineName", "Required");
                    return View(tbl_StickerMachine);
                }
                if (tbl_StickerMachine.StickerMachineTypeID == null)
                {
                    ViewBag.StickerMachineType = db.tbl_StickerMachineType.Select(x => new { ID = x.ID, Name = x.Type }).ToList();
                    ModelState.AddModelError("StickerMachineTypeID", "Required");
                    return View(tbl_StickerMachine);
                }
                if (tbl_StickerMachine.TextColorRate<0 )
                {
                    ViewBag.StickerMachineType = db.tbl_StickerMachineType.Select(x => new { ID = x.ID, Name = x.Type }).ToList();
                    ModelState.AddModelError("TextColorRate", "Required");
                    return View(tbl_StickerMachine);
                }
                if (tbl_StickerMachine.GroundColorRate < 0)
                {
                    ViewBag.StickerMachineType = db.tbl_StickerMachineType.Select(x => new { ID = x.ID, Name = x.Type }).ToList();
                    ModelState.AddModelError("GroundColorRate", "Required");
                    return View(tbl_StickerMachine);
                }
                if (tbl_StickerMachine.LaborCost < 0)
                {
                    ViewBag.StickerMachineType = db.tbl_StickerMachineType.Select(x => new { ID = x.ID, Name = x.Type }).ToList();
                    ModelState.AddModelError("LaborCost", "Required");
                    return View(tbl_StickerMachine);
                }
                tbl_StickerMachine.AddedOn = Helper.PST();
                db.tbl_StickerMachine.Add(tbl_StickerMachine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.StickerMachineType = db.tbl_StickerMachineType.Select(x => new { ID = x.ID, Name = x.Type }).ToList();

            return View(tbl_StickerMachine);
        } 

        // GET: StickerMachine/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerMachine tbl_StickerMachine = db.tbl_StickerMachine.Find(id);
            if (tbl_StickerMachine == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerMachine);
        }

        // POST: StickerMachine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( tbl_StickerMachine tbl_StickerMachine)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(tbl_StickerMachine.MachineName) || db.tbl_StickerMachine.Any(x => x.MachineName.Equals(tbl_StickerMachine.MachineName) && x.ID != tbl_StickerMachine.ID))
                {
                    ModelState.AddModelError("MachineName", "Required/Exist");
                    return View(tbl_StickerMachine);
                }
                if (tbl_StickerMachine.TextColorRate <= 0 )
                {
                    ModelState.AddModelError("TextColorRate", "Required");
                    return View(tbl_StickerMachine);
                }
                if (tbl_StickerMachine.GroundColorRate <= 0)
                {
                    ModelState.AddModelError("GroundColorRate", "Required");
                    return View(tbl_StickerMachine);
                }
                if (tbl_StickerMachine.LaborCost <= 0)
                {
                    ModelState.AddModelError("LaborCost", "Required");
                    return View(tbl_StickerMachine);
                }
                db.Entry(tbl_StickerMachine).State = EntityState.Modified;
                db.Entry(tbl_StickerMachine).Property(p => p.StickerMachineTypeID).IsModified = false;
                db.Entry(tbl_StickerMachine).Property(p => p.AddedOn).IsModified = false;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_StickerMachine);
        }

        // GET: StickerMachine/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerMachine tbl_StickerMachine = db.tbl_StickerMachine.Find(id);
            if (tbl_StickerMachine == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerMachine);
        }

        // POST: StickerMachine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_StickerMachine model = db.tbl_StickerMachine.Find(id);
            if (model != null)
            {
                model.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("MachineName", "Record not found");
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
