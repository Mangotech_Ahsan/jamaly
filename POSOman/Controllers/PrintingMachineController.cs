﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;

namespace POSOman.Controllers
{
    public class PrintingMachineController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: PrintingMachine
        public ActionResult Index()
        {
            return View(db.tbl_PrintingMachine.Where(x=>x.IsDeleted!=true).ToList());
        }

        // GET: PrintingMachine/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_PrintingMachine tbl_PrintingMachine = db.tbl_PrintingMachine.Find(id);
            if (tbl_PrintingMachine == null)
            {
                return HttpNotFound();
            }
            return View(tbl_PrintingMachine);
        }

        // GET: PrintingMachine/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PrintingMachine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_PrintingMachine tbl_PrintingMachine)
        {
            if (ModelState.IsValid)
            {
                db.tbl_PrintingMachine.Add(tbl_PrintingMachine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_PrintingMachine);
        }

        // GET: PrintingMachine/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_PrintingMachine tbl_PrintingMachine = db.tbl_PrintingMachine.Find(id);
            if (tbl_PrintingMachine == null)
            {
                return HttpNotFound();
            }
            return View(tbl_PrintingMachine);
        }

        // POST: PrintingMachine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_PrintingMachine tbl_PrintingMachine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_PrintingMachine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_PrintingMachine);
        }

        // GET: PrintingMachine/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_PrintingMachine tbl_PrintingMachine = db.tbl_PrintingMachine.Find(id);
            if (tbl_PrintingMachine == null)
            {
                return HttpNotFound();
            }
            return View(tbl_PrintingMachine);
        }

        // POST: PrintingMachine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_PrintingMachine model = db.tbl_PrintingMachine.Find(id);
            if (model != null)
            {
                model.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("PrintingMachine", "Record not found");
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
