﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;

namespace POSOman.Controllers
{
    public class CertificatesController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: Certificates
        public ActionResult Index()
        {
            return View(db.tbl_Certificates.ToList());
        }

        // GET: Certificates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Certificates tbl_Certificates = db.tbl_Certificates.Find(id);
            if (tbl_Certificates == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Certificates);
        }

        // GET: Certificates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Certificates/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, Authorize(Roles = "Admin,SuperAdmin,Accountant")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Certificate,CertificateInfo,Description,IsDeleted,AddedOn")] tbl_Certificates tbl_Certificates)
        {
            if (ModelState.IsValid)
            {
                db.tbl_Certificates.Add(tbl_Certificates);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_Certificates);
        }
        [HttpGet, Authorize(Roles = "Admin,SuperAdmin,Accountant")]
        // GET: Certificates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Certificates tbl_Certificates = db.tbl_Certificates.Find(id);
            if (tbl_Certificates == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Certificates);
        }

        // POST: Certificates/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, Authorize(Roles ="Admin,SuperAdmin,Accountant")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( tbl_Certificates tbl_Certificates)
        {
            if (ModelState.IsValid)
            {
                if(tbl_Certificates == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
                if (string.IsNullOrWhiteSpace(tbl_Certificates.CertificateInfo)) { ModelState.AddModelError("CertificateInfo", "Required"); return View(tbl_Certificates); }
                if (string.IsNullOrWhiteSpace(tbl_Certificates.Certificate)) { ModelState.AddModelError("Certificate", "Required"); return View(tbl_Certificates); }
                db.Entry(tbl_Certificates).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_Certificates);
        }

        // GET: Certificates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Certificates tbl_Certificates = db.tbl_Certificates.Find(id);
            if (tbl_Certificates == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Certificates);
        }

        // POST: Certificates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Certificates tbl_Certificates = db.tbl_Certificates.Find(id);
            db.tbl_Certificates.Remove(tbl_Certificates);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
