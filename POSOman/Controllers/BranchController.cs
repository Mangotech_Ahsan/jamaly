﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Configuration;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class BranchController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        ApplicationDbContext context;
        public string UserRole()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;
                string role = gerRole();
                ViewBag.Role = role;
                //{
                //    ViewBag.displayMenu = "Yes";
                //}
                return role;

            }
            else
            {
                ViewBag.Name = "Not Logged IN";
                return string.Empty;
            }            
        }
        public string gerRole()
        {
            context = new ApplicationDbContext();
            string userRole = "";
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                userRole = s[0].ToString();
            }
            return userRole;
        }
        [Authorize(Roles = "SuperAdmin")]
        // GET: Branch
        public ActionResult Index()
        {

            //if (UserRole() == "SuperAdmin")
            {
                var tbl_Branch = db.tbl_Branch;
                //List<tbl_Branch> list = tbl_Branch.ToList();
                //list.
                return View(tbl_Branch.ToList());
            }
            //else
              //  return RedirectToAction("Login", "Account");
        }
        [Authorize(Roles = "SuperAdmin")]
        // GET: Branch/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Branch tbl_Branch = db.tbl_Branch.Find(id);
            if (tbl_Branch == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Branch);
        }
        [Authorize(Roles = "SuperAdmin")]
        // GET: Branch/Create
        public ActionResult Create()
        {            
           // ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName");
            return View();
        }

        // POST: Branch/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_Branch tbl_Branch)
        {
            if (ModelState.IsValid)
            {
                db.tbl_Branch.Add(tbl_Branch);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Branch.Addby);
            return View(tbl_Branch);
        }
        [Authorize(Roles = "SuperAdmin")]
        // GET: Branch/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Branch tbl_Branch = db.tbl_Branch.Find(id);
            if (tbl_Branch == null)
            {
                return HttpNotFound();
            }
           // ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Branch.Addby);
            return View(tbl_Branch);
        }

        // POST: Branch/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_Branch tbl_Branch)
        {
            tbl_Branch.UpdateOn = DateTime.UtcNow.AddHours(5).Date;
            if (ModelState.IsValid)
            {
                if (db.tbl_Branch.Any(b => b.BranchName == tbl_Branch.BranchName && b.BranchID != tbl_Branch.BranchID))
                {
                    ModelState.AddModelError("BranchName", "Branch Already Exists!");
                    return View("Edit");
                }
                else
                {
                    db.Entry(tbl_Branch).State = EntityState.Modified;
                    db.Entry(tbl_Branch).Property(x => x.AddOn).IsModified = false;
                    db.Entry(tbl_Branch).Property(x => x.Addby).IsModified = false;
                    db.Entry(tbl_Branch).Property(x => x.UserID).IsModified = false;                    
                    db.Entry(tbl_Branch).Property(x => x.rowguid).IsModified = false;
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edited Branch".ToString());
                    return RedirectToAction("Index");
                }
            }
            //ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Branch.Addby);
            return View(tbl_Branch);            
        }

        // GET: Branch/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            bool isExist = db.tbl_Stock.Any(p => p.BranchID == id);
            bool isExist1 = db.tbl_PODetails.Any(p => p.BranchID == id);
            bool isExist2 = db.tbl_SaleDetails.Any(p => p.BranchID == id);
            tbl_Branch tbl_Branch = db.tbl_Branch.Find(id);
            if (isExist || isExist1 || isExist2)
            {
                ModelState.AddModelError(string.Empty, "Certain Actions have been performed on this branch! Proceed to delete ?");
                
                return View(tbl_Branch);
            }
           // tbl_Branch tbl_Branch = db.tbl_Branch.Find(id);
            if (tbl_Branch == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Branch);
        }

        // POST: Branch/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //tbl_Branch tbl_Branch = db.tbl_Branch.Find(id);
            //db.tbl_Branch.Remove(tbl_Branch);
            //db.SaveChanges();
            //return RedirectToAction("Index");
            try
            {
                bool isExist = db.tbl_Stock.Any(p => p.BranchID == id);
                bool isExist1 = db.tbl_PODetails.Any(p => p.BranchID == id);
                bool isExist2 = db.tbl_SaleDetails.Any(p => p.BranchID == id);
                if (isExist || isExist1 || isExist2)
                {
                    ModelState.AddModelError(string.Empty, "Branch Cannot Be Deleted!");
                    tbl_Branch tbl_Branch = db.tbl_Branch.Find(id);
                    return View(tbl_Branch);
                }
                else
                {
                    tbl_Branch tbl_Branch = db.tbl_Branch.Find(id);
                    db.tbl_Branch.Remove(tbl_Branch);
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Branch".ToString());

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
