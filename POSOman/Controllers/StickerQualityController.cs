﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class StickerQualityController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: StickerQuality
        public ActionResult Index()
        {
            return View(db.tbl_StickerQuality.Where(x=>x.IsDeleted!=true).ToList());
        }

        // GET: StickerQuality/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerQuality tbl_StickerQuality = db.tbl_StickerQuality.Find(id);
            if (tbl_StickerQuality == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerQuality);
        }

        // GET: StickerQuality/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StickerQuality/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_StickerQuality model)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            if (string.IsNullOrWhiteSpace(model.Quality))
                            {
                                ModelState.AddModelError("Quality", "Required");
                                return View(model);
                            }
                            if (model.Rate <= 0)
                            {
                                ModelState.AddModelError("Rate", "Required");
                                return View(model);
                            }
                            if (model.GSM <= 0)
                            {
                                ModelState.AddModelError("GSM", "Required");
                                return View(model);
                            }
                            model.AddedOn = Helper.PST();
                            db.tbl_StickerQuality.Add(model);
                            db.SaveChanges();
                           
                            tbl_Product p = new tbl_Product();
                            p.VehicleCodeID = 4;// Sticker Category
                            p.PartNo = model.Quality;
                            p.isActive = 1;
                            p.IsGeneralItem = false;
                            p.IsOffsetItem = true;
                            p.AddOn = Helper.PST();
                            p.MarkedupPrice = model.Rate;
                            p.RawProductID = model.ID;
                            db.tbl_Product.Add(p);
                            db.SaveChanges();
                            t.Commit();

                            return RedirectToAction("Index");
                        }

                        t.Rollback();
                        return View(model);
                    }
                    catch (Exception d)
                    {
                        t.Rollback();
                        ModelState.AddModelError("Quality", d.Message);
                        return View(model);
                    }

                }

            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                ModelState.AddModelError("Quality", e.Message);
                return View(model);
            }

        }

        // GET: StickerQuality/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerQuality tbl_StickerQuality = db.tbl_StickerQuality.Find(id);
            if (tbl_StickerQuality == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerQuality);
        }

        // POST: StickerQuality/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_StickerQuality model)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(model.Quality))
                {
                    ModelState.AddModelError("Quality", "Required");
                    return View(model);
                }
                if (model.Rate <= 0)
                {
                    ModelState.AddModelError("Rate", "Required");
                    return View(model);
                }
                if (model.GSM <= 0)
                {
                    ModelState.AddModelError("GSM", "Required");
                    return View(model);
                }
                db.Entry(model).State = EntityState.Modified;
                db.Entry(model).Property(p => p.AddedOn).IsModified = false;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: StickerQuality/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerQuality tbl_StickerQuality = db.tbl_StickerQuality.Find(id);
            if (tbl_StickerQuality == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerQuality);
        }

        // POST: StickerQuality/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_StickerQuality model = db.tbl_StickerQuality.Find(id);
            if (model != null)
            {
                model.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("Quality", "Record not found");
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
