﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using POSOman.Models.DTO;
using AutoMapper;
using POSOman.Models.BLL;
using System.Drawing;
using System.IO;
using ZXing;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WebForms;

namespace POSOman.Controllers
{
    public class ProductController : Controller
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();

        public JsonResult GetProductsList(int TypeID)
        {
            if (TypeID != 3 && TypeID > 0)
            {
                var qry = db.tbl_Product.Where(p => p.ProductTypeID == TypeID).Select(p => new { Value = p.ProductID, Name = p.PartNo ,Type = p.ProductTypeID}).ToList();
                return Json(qry, JsonRequestBehavior.AllowGet);
            }

            var data = db.tbl_Product.Select(p => new { Value = p.ProductID, Name = p.PartNo, Type = p.ProductTypeID }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Product
        public ActionResult Index(int? deptId)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            ViewBag.Department = string.Empty;
            List<tbl_Product> data = new List<tbl_Product>();
            if (deptId!=null && deptId > 0)
            {
                data = db.tbl_Product.Where(x => x.DepartmentID == deptId && x.IsGeneralItem == false && x.IsOffsetItem == false && (x.isActive != 0 || x.isActive == null)).ToList();
                ViewBag.Department = data.Select(x => x.tbl_Department.Department).FirstOrDefault();
            }
            else
            {
                data = db.tbl_Product.Where(x => x.IsGeneralItem == false && x.IsOffsetItem == false && (x.isActive != 0 || x.isActive == null)).ToList();

            }
            return View(data);
        }


        public FileResult PackageReport( int OffsetTypeID, int deptID, int ProductID, String ReportType)
        {
            try
            {
                LocalReport lo = new LocalReport();
                if (deptID == 1 && OffsetTypeID != 3)
                {
                    ReportDataSource rs = new ReportDataSource();
                    rs.Name = "ItemViewDataset";
                    rs.Value = db.GetViewDetailSummaryProductDepartmentWiseSP(ProductID,deptID);
                    lo.DataSources.Add(rs);
                    lo.ReportPath = Server.MapPath("~/Models/Reports/OffsetItem.rdlc");

                }
                else if (deptID == 1 && OffsetTypeID == 3)
                {
                    ReportDataSource rs = new ReportDataSource();
                    rs.Name = "ItemViewDataset";
                    rs.Value = db.GetViewDetailSummaryProductDepartmentWiseSP(ProductID, deptID);
                    lo.DataSources.Add(rs);
                    lo.ReportPath = Server.MapPath("~/Models/Reports/TafataItem.rdlc");

                }
                else if (deptID == 2)
                {
                    ReportDataSource rs = new ReportDataSource();
                    rs.Name = "ItemViewDataset";
                    rs.Value = db.GetViewDetailSummaryProductDepartmentWiseSP(ProductID, deptID);
                    lo.DataSources.Add(rs);
                    lo.ReportPath = Server.MapPath("~/Models/Reports/SatinItem.rdlc");

                }
                else if (deptID == 3)
                {
                    ReportDataSource rs = new ReportDataSource();
                    rs.Name = "ItemViewDataset";
                    rs.Value = db.GetViewDetailSummaryProductDepartmentWiseSP(ProductID, deptID);
                    lo.DataSources.Add(rs);
                    lo.ReportPath = Server.MapPath("~/Models/Reports/StickerItem.rdlc");

                }

                string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
                Warning[] warnings;
                string[] streams;
                string mimeType;
                byte[] renderedBytes;
                string encoding;
                string fileNameExtension;
                renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

                return new FileContentResult(renderedBytes, mimeType);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return null;
            }
        }

        public ActionResult GeneralProductIndex()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();
            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            var data = db.tbl_Product.Where(x => x.IsGeneralItem == true && (x.isActive !=0)).ToList();
            return View(data);
        }

        public ActionResult  justProduct()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            var tbl_Product = db.tbl_Product.Where(p => p.tbl_VehicleCode.HeadID != 1).ToList();

            if (tbl_Product.Count > 0)
            {

            }
            return View(tbl_Product);
        }

        public ActionResult ServiceList()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();
            var tbl_Product = db.tbl_Product.Where(p => p.tbl_VehicleCode.HeadID == 1 && p.tbl_VehicleCode.LevelID == 1).ToList() ;

            if (tbl_Product.Count > 0)
            {

            }
            return View(tbl_Product);
        }
        public ActionResult Index_All()
        {
            var tbl_Product = db.tbl_Product.Include(t => t.tbl_VehicleCode);
            return View(tbl_Product);
        }
        #region Print Bar Code 
        ///Print BarCode
        public ActionResult PrintBarCode(int? id)
        {
            Session["BCImage"] = null;
            var BCode = db.tbl_Product.Where(x => x.ProductID == id).Select(x => new { x.BarCode, x.PartNo, x.Code, x.SaleRate }).FirstOrDefault();
            var Price = BCode.SaleRate;
            if (Price == null)
            {
                ViewBag.Price = "undefined";
            }
            else
            {
                ViewBag.Price = Price;
            }
            if (BCode == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                RenderBarcode(BCode.BarCode.ToString());
                ViewBag.PrintBCode = Session["BCImage"];
                ViewBag.PartNo = BCode.PartNo.ToString();
                if (BCode.PartNo.Length > 25)
                {
                    ViewBag.PartNo = BCode.PartNo.ToString().Substring(0,25);
                }
                
                ViewBag.BarCode = BCode.BarCode.ToString();
                return PartialView("_PrintBarCode");
            }
        }
        public void RenderBarcode(string Code)
        {

            Image img = null;
            using (var ms = new MemoryStream())
            {
                var writer = new ZXing.BarcodeWriter() { Format = BarcodeFormat.CODE_128 };
                writer.Options.Height = 80;
                writer.Options.Width = 280;
                writer.Options.PureBarcode = true;
                img = writer.Write(Code);
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                Session["BCImage"] = "data:image/jpeg;base64," + Convert.ToBase64String(ms.ToArray());
            }
        }
        #endregion       

        [HttpPost]
        public ActionResult LoadData()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            using (db)
            {
                // dc.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
                var v = (from a in db.tbl_Product.OrderBy(p => p.ProductID).Skip(skip).Take(pageSize) select a);

                //SORT
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    v = v.OrderBy(p => p.ProductID);
                }

                recordsTotal = v.Count();
                var data = v;//.Skip(skip).Take(pageSize).ToList();
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);
            }
        }
        #region Select2 pagination

        [HttpGet]
        public ActionResult GetValuesStkMove(string searchTerm, int pageSize, int pageNum, string countyId)
        {
            if (countyId != "" && countyId != null && countyId != string.Empty)
            {
                int vehCodeID = Convert.ToInt32(countyId);
                List<tbl_Product> ObjList = null;
                if (searchTerm != null && searchTerm != "")
                {
                    //ObjList = db.tbl_Product.Where(p => p.PartNo.StartsWith(searchTerm) && p.VehicleCodeID == vehCodeID).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                    ObjList = db.tbl_Product.Where(p => p.PartNo.Contains(searchTerm) && p.VehicleCodeID == vehCodeID && p.VehicleCodeID !=1).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ObjList = db.tbl_Product.Where(p => p.VehicleCodeID == vehCodeID && p.VehicleCodeID != 1).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                //Getting items
                //var itemList = db.tbl_Product.Where(p => p.PartNo == (searchTerm)).ToList();
                var itemList = (from p in ObjList
                                    //where p.PartNo.StartsWith(searchTerm)
                                select new { PartNo = p.PartNo, ProductID = p.ProductID });
                //Creating new object for return.
                var result = new
                {
                    Total = itemList.Count(),
                    //Results = itemList.Skip((pageNum * pageSize) - 100).Take(pageSize)
                    Results = itemList//.Skip((pageNum * pageSize) - 100).Take(pageSize)
                };

                return new JsonResult
                {
                    Data = result,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                List<tbl_Product> ObjList = null;
                if (searchTerm != null && searchTerm != "")
                {
                    //ObjList = db.tbl_Product.Where(p => p.PartNo.StartsWith(searchTerm)).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                    ObjList = db.tbl_Product.Where(p => p.PartNo.Contains(searchTerm) && p.VehicleCodeID != 1).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ObjList = db.tbl_Product.Where(p=> p.VehicleCodeID != 1).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                //Getting items
                //var itemList = db.tbl_Product.Where(p => p.PartNo == (searchTerm)).ToList();
                var itemList = (from p in ObjList
                                    //where p.PartNo.StartsWith(searchTerm)
                                select new { PartNo = p.PartNo, ProductID = p.ProductID });
                //Creating new object for return.
                var result = new
                {
                    Total = itemList.Count(),
                    //Results = itemList.Skip((pageNum * pageSize) - 100).Take(pageSize)
                    Results = itemList//.Skip((pageNum * pageSize) - 100).Take(pageSize)
                };

                return new JsonResult
                {
                    Data = result,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }

        }

        [HttpGet]
        public ActionResult GetValues(string searchTerm, int pageSize, int pageNum, string countyId)
        {
            if(countyId != "" && countyId != null && countyId != string.Empty)
            {
                int vehCodeID = Convert.ToInt32(countyId);
                List<tbl_Product> ObjList = null;
                if (searchTerm != null && searchTerm != "")
                {
                    //ObjList = db.tbl_Product.Where(p => p.PartNo.StartsWith(searchTerm) && p.VehicleCodeID == vehCodeID).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                    ObjList = db.tbl_Product.Where(p => p.PartNo.Contains(searchTerm) && p.VehicleCodeID == vehCodeID).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ObjList = db.tbl_Product.Where(p => p.VehicleCodeID == vehCodeID).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                //Getting items
                //var itemList = db.tbl_Product.Where(p => p.PartNo == (searchTerm)).ToList();
                var itemList = (from p in ObjList
                                    //where p.PartNo.StartsWith(searchTerm)
                                select new { PartNo = p.PartNo, ProductID = p.ProductID });
                //Creating new object for return.
                var result = new
                {
                    Total = itemList.Count(),
                    //Results = itemList.Skip((pageNum * pageSize) - 100).Take(pageSize)
                    Results = itemList//.Skip((pageNum * pageSize) - 100).Take(pageSize)
                };

                return new JsonResult
                {
                    Data = result,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                List<tbl_Product> ObjList = null;
                if (searchTerm != null && searchTerm != "")
                {
                    //ObjList = db.tbl_Product.Where(p => p.PartNo.StartsWith(searchTerm)).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                    ObjList = db.tbl_Product.Where(p => p.PartNo.Contains(searchTerm)).OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ObjList = db.tbl_Product.OrderBy(p => p.ProductID).Skip((pageNum * pageSize) - pageSize).Take(pageSize).ToList();
                }
                //Getting items
                //var itemList = db.tbl_Product.Where(p => p.PartNo == (searchTerm)).ToList();
                var itemList = (from p in ObjList
                                     //where p.PartNo.StartsWith(searchTerm)
                                select new { PartNo = p.PartNo, ProductID = p.ProductID });
                //Creating new object for return.
                var result = new
                {
                    Total = itemList.Count(),
                    //Results = itemList.Skip((pageNum * pageSize) - 100).Take(pageSize)
                    Results = itemList//.Skip((pageNum * pageSize) - 100).Take(pageSize)
                };

                return new JsonResult
                {
                    Data = result,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
           
        }
        #endregion
        [HttpPost]
        public JsonResult  ProductList(DTParameters param)
        {
            int TotalCount = 0;
            var filtered = this.GetProductFiltered(param.Search.Value, param.SortOrder, param.Start, param.Length, out TotalCount);

            var ProductList = filtered.Select(p => new ProductListCategory()
            {
                ProductID = p.ProductID,
                //VehicleCode = (p.VehicleCodeID.HasValue) ? p.tbl_VehicleCode.VehicleCode : "",
                Brand = p.Brand,
                Head = p.Head,
                SubCategory = p.SubCategory,
                Section = p.Section,
                PartNo = p.PartNo,
                Description = p.Description,
                BarCode = p.BarCode,
                UnitCode = p.UnitCode,
                SaleRate = p.SaleRate
            });

            DTResult<ProductListCategory> finalresult = new DTResult<ProductListCategory>
            {
                draw = param.Draw,
                data = ProductList.ToList(),
                recordsFiltered = TotalCount,
                recordsTotal = filtered.Count

            };

            return Json(finalresult);

        }

        [HttpPost]
        public JsonResult ProductListService(DTParameters param)
        {
            int TotalCount = 0;
            var filtered = this.GetProductFilteredService(param.Search.Value, param.SortOrder, param.Start, param.Length, out TotalCount);

            var ProductList = filtered.Select(p => new ProductListCategory()
            {
                ProductID = p.ProductID,
                //VehicleCode = (p.VehicleCodeID.HasValue) ? p.tbl_VehicleCode.VehicleCode : "",
                Brand = p.Brand,
                Head = p.Head,
                SubCategory = p.SubCategory,
                Section = p.Section,
                PartNo = p.PartNo,
                Description = p.Description,
                BarCode = p.BarCode,
                UnitCode = p.UnitCode,
                SaleRate = p.SaleRate
            });

            DTResult<ProductListCategory> finalresult = new DTResult<ProductListCategory>
            {
                draw = param.Draw,
                data = ProductList.ToList(),
                recordsFiltered = TotalCount,
                recordsTotal = filtered.Count

            };

            return Json(finalresult);

        }
        public List<ProductListCategory> GetProductFilteredService(string search, string sortOrder, int start, int length, out int TotalCount)
        {
            var result = db.tbl_Product.OrderBy(p => p.ProductID).Where(t=>t.VehicleCodeID ==1).Include(t => t.tbl_VehicleCode).Include(t => t.tbl_Brand).Where(p => (search == null || (p.PartNo != null && p.PartNo.ToLower().Contains(search.ToLower())
                || p.BarCode != null && p.BarCode.Contains(search)
                || p.tbl_VehicleCode.VehicleCode != null && p.tbl_VehicleCode.VehicleCode.ToLower().Contains(search.ToLower())
                || p.tbl_Brand.BrandTitle != null && p.tbl_Brand.BrandTitle.ToLower().Contains(search.ToLower())
                || p.Product_Code != null && p.Product_Code.ToLower().Contains(search.ToLower())
                || p.UnitCode != null && p.UnitCode.ToLower().Contains(search.ToLower())
                || p.SaleRate != null && p.SaleRate.ToString().Contains(search.ToString())
                ))).Skip(start).Take(length).ToList();
            TotalCount = db.tbl_Product.Count();
            List<Models.DTO.ProductListCategory> prodList = new List<ProductListCategory>();
            int counter = 0;
            foreach (var item in result)
            {
                Models.DTO.ProductListCategory prod = new ProductListCategory();
                prod.ImageUrl = item.ImageUrl;
                prod.BarCode = item.BarCode;
                prod.PartNo = item.PartNo;
                prod.Brand = item.tbl_Brand.BrandTitle;
                prod.ProductID = item.ProductID;
                prod.SaleRate = item.SaleRate;
                prod.UnitCode = item.UnitCode;
                int levelID = result[counter].tbl_VehicleCode.LevelID ?? 0;
                var category = result[counter].tbl_VehicleCode;
                if (levelID == 0)
                {
                    prod.Head = category.VehicleCode;
                }
                else if (levelID == 1)
                {
                    var head = db.tbl_VehicleCode.Where(v => v.HeadID == category.HeadID).FirstOrDefault().VehicleCode;
                    prod.Head = head;
                    prod.SubCategory = category.VehicleCode;
                }
                else if (levelID == 2)
                {
                    var sub = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == category.HeadID).FirstOrDefault();
                    prod.SubCategory = sub.VehicleCode;
                    var head = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == sub.HeadID).FirstOrDefault().VehicleCode;
                    prod.Head = head;
                    prod.Section = category.VehicleCode;
                }
                prodList.Add(prod);
                counter = counter + 1;
            }
            //Mapper.CreateMap<tbl_Product, Models.DTO.ProductListCategory>();
            //var details = Mapper.Map<ICollection<tbl_Product>, ICollection<Models.DTO.ProductListCategory>>(result);
            return prodList;
        }
        public List<ProductListCategory> GetProductFiltered(string search, string sortOrder, int start, int length, out int TotalCount)
        {
            var result = db.tbl_Product.OrderBy(p => p.ProductID).Include(t => t.tbl_VehicleCode).Include(t => t.tbl_Brand).Where(p => (search == null || (p.PartNo != null && p.PartNo.ToLower().Contains(search.ToLower())
                || p.BarCode != null && p.BarCode.Contains(search)
                || p.tbl_VehicleCode.VehicleCode != null && p.tbl_VehicleCode.VehicleCode.ToLower().Contains(search.ToLower())
                || p.tbl_Brand.BrandTitle != null && p.tbl_Brand.BrandTitle.ToLower().Contains(search.ToLower())
                || p.Product_Code != null && p.Product_Code.ToLower().Contains(search.ToLower())
                || p.UnitCode != null && p.UnitCode.ToLower().Contains(search.ToLower())
                || p.SaleRate != null && p.SaleRate.ToString().Contains(search.ToString())
                ))).Skip(start).Take(length).ToList();
            TotalCount = db.tbl_Product.Count();
            List<Models.DTO.ProductListCategory> prodList = new List<ProductListCategory>();
            int counter = 0;
            foreach (var item in result)
            {
                //var tbl_Product = db.tbl_Product.Where(p => p.tbl_VehicleCode.HeadID == 1 && p.tbl_VehicleCode.LevelID == 1).ToList();
                string type = "Product";
                if (item.tbl_VehicleCode.HeadID == 1 && item.tbl_VehicleCode.LevelID == 1)
                {
                    type = "Service";
                }
                Models.DTO.ProductListCategory prod = new ProductListCategory();
                prod.ImageUrl = item.ImageUrl;
                prod.BarCode = item.BarCode;
                prod.PartNo = item.PartNo;
                prod.Description = type;
                prod.Brand = item.tbl_Brand.BrandTitle;
                prod.ProductID = item.ProductID;
                prod.SaleRate = item.SaleRate;
                prod.UnitCode = item.UnitCode;
                int levelID = result[counter].tbl_VehicleCode.LevelID ?? 0;
                var category = result[counter].tbl_VehicleCode;
                if (levelID == 0)
                {
                    prod.Head = category.VehicleCode;
                }
                else if(levelID == 1)
                {
                    var head = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == category.HeadID).FirstOrDefault().VehicleCode;
                    prod.Head = head;
                    prod.SubCategory = category.VehicleCode;
                }
                else if (levelID == 2)
                {
                    var sub = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == category.HeadID).FirstOrDefault();
                    prod.SubCategory = sub.VehicleCode;
                    var head = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == sub.HeadID).FirstOrDefault().VehicleCode;
                    prod.Head = head;                    
                    prod.Section = category.VehicleCode;
                }
                prodList.Add(prod);
                counter = counter + 1;
            }
            //Mapper.CreateMap<tbl_Product, Models.DTO.ProductListCategory>();
            //var details = Mapper.Map<ICollection<tbl_Product>, ICollection<Models.DTO.ProductListCategory>>(result);
            return prodList;
        }
        
    // GET: Product/Details/5
    public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Product tbl_Product = db.tbl_Product.Find(id);
            if (tbl_Product == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Product);
        }
        public JsonResult GetSubCat(int HeadCat)
        {
            if (HeadCat > 0)
            {
                var qry = db.tbl_VehicleCode.Where(p => p.HeadID == HeadCat && p.LevelID == 1).Select(p => new { Value = p.VehicleCodeID, Name = p.VehicleCode }).ToList();
                return Json(qry);
            }
            else
            {
                var qry = db.tbl_VehicleCode.Where(p => p.HeadID == HeadCat).Select(p => new { Value = p.VehicleCodeID, Name = p.VehicleCode }).ToList();
                return Json(qry);
            }
        }
        public JsonResult GetSection(int SubCat)
        {
            if (SubCat > 0)
            {
                var qry = db.tbl_VehicleCode.Where(p => p.HeadID == SubCat && p.LevelID == 2).Select(p => new { Value = p.VehicleCodeID, Name = p.VehicleCode }).ToList();
                return Json(qry);
            }
            else
            {
                var qry = db.tbl_VehicleCode.Where(p => p.HeadID == SubCat).Select(p => new { Value = p.VehicleCodeID, Name = p.VehicleCode }).ToList();
                return Json(qry);
            }
        }

        public ActionResult CreateService()
        {
            Random ran = new Random();
            var randomNo = ran.Next();
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            // ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName");
            //List<SelectListItem> items = new SelectList(db.tbl_VehicleModel, "VehicleID", "VehicleName").ToList();
            //items.Insert(0, (new SelectListItem { Text = "[None]", Value = "0" }));            
            //ViewBag.Category = new SelectList(db.tbl_VehicleCode.Where(x=>x.LevelID == 0 && x.HeadID == null), "VehicleCodeID", "VehicleCode");
            //ViewBag.SubCategory = new SelectList(db.tbl_VehicleCode.Where(x => x.LevelID == 1 ), "VehicleCodeID", "VehicleCode");
            //ViewBag.Section = new SelectList(db.tbl_VehicleCode.Where(x => x.LevelID == 2 ), "VehicleCodeID", "VehicleCode");

            ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null && x.VehicleCodeID == 1).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode  });
///            ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1  ).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
            ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1 && x.HeadID == 1).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });

            ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });



            ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
            ViewBag.UnitCodeID = new SelectList(db.tbl_UnitCode, "UnitCodeID", "UnitCode");
            return View();
        }
        // GET: Product/Create
        public ActionResult Create()
        {
            Random ran = new Random();
            var randomNo = ran.Next();            
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            // ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName");
            //List<SelectListItem> items = new SelectList(db.tbl_VehicleModel, "VehicleID", "VehicleName").ToList();
            //items.Insert(0, (new SelectListItem { Text = "[None]", Value = "0" }));            
            //ViewBag.Category = new SelectList(db.tbl_VehicleCode.Where(x=>x.LevelID == 0 && x.HeadID == null), "VehicleCodeID", "VehicleCode");
            //ViewBag.SubCategory = new SelectList(db.tbl_VehicleCode.Where(x => x.LevelID == 1 ), "VehicleCodeID", "VehicleCode");
            //ViewBag.Section = new SelectList(db.tbl_VehicleCode.Where(x => x.LevelID == 2 ), "VehicleCodeID", "VehicleCode");
            //ViewBag.Category = new SelectList(db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null && x.VehicleCodeID != 1), "VehicleCodeID", "VehicleCode");

            ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null && x.VehicleCodeID>5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x=> new { Value = x.VehicleCodeID,Name = x.VehicleCode});
            ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1 && x.HeadID != 1).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
            ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2 ).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
            ViewBag.ProductTypeID = db.tbl_ProductType.Where(x => x.ProductTypeID != 3).Select(x => new { Value = x.ProductTypeID, Name = x.TypeName });
            ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted !=true).Select(x => new { Value = x.ID, Name = x.Department });
            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 1 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });
            ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
            //ViewBag.UnitCodeID = new SelectList(db.tbl_UnitCode, "UnitCodeID", "UnitCode");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateService(Models.DTO.ProductDTO tbl_Product)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }

            tbl_Product.MinorDivisor = 1000;
            //tbl_Product.UnitCode = db.tbl_UnitCode.Find(tbl_Product.UnitCodeID).UnitCode;
            tbl_Product.BranchID = branchId;
            tbl_Product.UserID = User.Identity.GetUserId();
            tbl_Product.QtyPerUnit = 1;
            tbl_Product.Addby = userID;
            tbl_Product.AddOn = DateTime.UtcNow.AddHours(5);
            tbl_Product.isActive = 1;
            if (tbl_Product.IsPacket == false)
            {
                tbl_Product.UnitPerCarton = 1;
            }
            if (tbl_Product.BrandID == null)
            {
                tbl_Product.BrandID = 1;
            }
            var newProdID = 1;
            var tmp = db.tbl_Product.OrderByDescending(v => v.ProductID).FirstOrDefault();
            if (tmp != null)
            {
                newProdID = tmp.ProductID + 1;
            }

            if (string.IsNullOrWhiteSpace(tbl_Product.BarCode))
            {
                string source = tbl_Product.PartNo;
                string result = string.Concat(source.Where(c => !char.IsWhiteSpace(c)));
                Random ran = new Random();
                var randomNo = ran.Next();
                string barCode = (newProdID).ToString() + randomNo.ToString();
                if (barCode.Length > 13)
                {
                    barCode = barCode.Substring(0, 13);
                }
                else if (barCode.Length < 13)
                {
                    Random ran1 = new Random();
                    var randomNo1 = ran1.Next();
                    barCode = (barCode + randomNo1.ToString()).Substring(0, 13);
                }
                tbl_Product.BarCode = barCode;
            }
            if (ModelState.IsValid)
            {

                if (string.IsNullOrWhiteSpace(tbl_Product.PartNo))
                {
                    ModelState.AddModelError("PartNo", "Cannot Enter Null Entries!");
                    //  ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Product.Addby);

                    // ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode, "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
                    ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });



                    ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
                    return View("ServiceList");
                }

                else if (db.tbl_Product.Any(p => p.PartNo == tbl_Product.PartNo && p.VehicleCodeID == tbl_Product.VehicleCodeID))
                {
                    ModelState.AddModelError("PartNo", "Product Already Exists!");
                    ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
                    ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });

                    return View("ServiceList");
                }
                else if (tbl_Product.SubCategoryID == null && tbl_Product.SectionID == null && tbl_Product.VehicleCodeID == null)
                {
                    ModelState.AddModelError("PartNo", "Please select atleast one option from above!");
                    ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
                    ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });

                    return View("ServiceList");
                }
                else if (db.tbl_Product.Any(p => p.BarCode == tbl_Product.BarCode))
                {
                    ModelState.AddModelError("BarCode", "BarCode Already Exists!");
                    ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
                    ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });

                    return View("ServiceList");
                }
                else
                {

                    if (tbl_Product.SectionID != null)
                    {
                        tbl_Product.VehicleCodeID = tbl_Product.SectionID;

                    }
                    else if (tbl_Product.SubCategoryID != null && tbl_Product.SectionID == null)
                    {
                        tbl_Product.VehicleCodeID = tbl_Product.SubCategoryID;
                    }
                    else if (tbl_Product.VehicleCodeID != null && tbl_Product.SubCategoryID == null && tbl_Product.SectionID == null)
                    {
                        tbl_Product.VehicleCodeID = tbl_Product.VehicleCodeID;
                    }

                    Mapper.CreateMap<Models.DTO.ProductDTO, tbl_Product>();
                    var master = Mapper.Map<Models.DTO.ProductDTO, tbl_Product>(tbl_Product);
                    db.tbl_Product.Add(master);
                    db.SaveChanges();
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added Products".ToString());
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
            ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
            ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
            ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
            ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });

            return View(tbl_Product);
        }



        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for         
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.DTO.ProductDTO tbl_Product)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    int branchId = 0;
                    int userID = 0;
                    string currentUserId = "";
                    if (Session["BranchID"] != null)
                    {
                        branchId = Convert.ToInt32(Session["BranchID"]);
                        userID = Convert.ToInt32(Session["UserID"]);
                    }
                    else
                    {
                        var user = User.Identity;
                        currentUserId = User.Identity.GetUserId();
                        var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                        branchId = currentUser.BranchID;
                        userID = currentUser.UserId;
                    }

                    tbl_Product.MinorDivisor = 1000;
                    //tbl_Product.UnitCode = db.tbl_UnitCode.Find(tbl_Product.UnitCodeID).UnitCode;
                    tbl_Product.IsGeneralItem = true;
                    tbl_Product.IsOffsetItem = false;
                    tbl_Product.BranchID = branchId;
                    tbl_Product.UserID = User.Identity.GetUserId();
                    tbl_Product.QtyPerUnit = 1;
                    tbl_Product.Addby = userID;
                    tbl_Product.AddOn = DateTime.UtcNow.AddHours(5);
                    tbl_Product.isActive = 1;
                    tbl_Product.PartNo = tbl_Product.PartNo.Trim();
                    if (tbl_Product.IsPacket == false)
                    {
                        tbl_Product.UnitPerCarton = 1;
                    }
                    if (tbl_Product.BrandID == null)
                    {
                        tbl_Product.BrandID = 1;
                    }
                    var newProdID = 1;
                    var tmp = db.tbl_Product.OrderByDescending(v => v.ProductID).FirstOrDefault();
                    if (tmp != null)
                    {
                        newProdID = tmp.ProductID + 1;
                    }

                    if (string.IsNullOrWhiteSpace(tbl_Product.BarCode))
                    {
                        string source = tbl_Product.PartNo;
                        string result = string.Concat(source.Where(c => !char.IsWhiteSpace(c)));
                        Random ran = new Random();
                        var randomNo = ran.Next();
                        string barCode = (newProdID).ToString() + randomNo.ToString();
                        if (barCode.Length > 13)
                        {
                            barCode = barCode.Substring(0, 13);
                        }
                        else if (barCode.Length < 13)
                        {
                            Random ran1 = new Random();
                            var randomNo1 = ran1.Next();
                            barCode = (barCode + randomNo1.ToString()).Substring(0, 13);
                        }
                        tbl_Product.BarCode = barCode;
                    }
                    if (ModelState.IsValid)
                    {

                        if (tbl_Product.UnitCodeID == null || tbl_Product.UnitCodeID <= 0)
                        {
                            ModelState.AddModelError("UnitCodeID", "Cannot Enter Null Entries!");
                            //  ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Product.Addby);

                            // ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode, "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
                            ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1 && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });
                            ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 1 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });



                            ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
                            return View("Create",tbl_Product);
                        }

                        if (string.IsNullOrWhiteSpace(tbl_Product.PartNo))
                        {
                            ModelState.AddModelError("PartNo", "Cannot Enter Null Entries!");
                            //  ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Product.Addby);

                            // ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode, "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
                            ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1 && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });
                            ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 1 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });



                            ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
                            return View("Create", tbl_Product);
                        }

                        else if (db.tbl_Product.Any(p => p.PartNo == tbl_Product.PartNo && p.VehicleCodeID == tbl_Product.VehicleCodeID))
                        {
                            ModelState.AddModelError("PartNo", "Product Already Exists!");
                            ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1 && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });
                            ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
                            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 1 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                            return View("Create", tbl_Product);
                        }
                        else if (tbl_Product.SubCategoryID == null && tbl_Product.SectionID == null && tbl_Product.VehicleCodeID == null)
                        {
                            ModelState.AddModelError("PartNo", "Please select atleast one option from above!");
                            ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1 && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });
                            ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
                            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 1 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                            return View("Create", tbl_Product);
                        }
                        else if (db.tbl_Product.Any(p => p.BarCode == tbl_Product.BarCode))
                        {
                            ModelState.AddModelError("BarCode", "BarCode Already Exists!");
                            ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1 && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });
                            ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                            ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
                            ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 1 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                            return View("Create", tbl_Product);
                        }
                        else
                        {

                            if (tbl_Product.SectionID != null)
                            {
                                tbl_Product.VehicleCodeID = tbl_Product.SectionID;

                            }
                            else if (tbl_Product.SubCategoryID != null && tbl_Product.SectionID == null)
                            {
                                tbl_Product.VehicleCodeID = tbl_Product.SubCategoryID;
                            }
                            else if (tbl_Product.VehicleCodeID != null && tbl_Product.SubCategoryID == null && tbl_Product.SectionID == null)
                            {
                                tbl_Product.VehicleCodeID = tbl_Product.VehicleCodeID;
                            }
                            tbl_Product.UnitCode = db.tbl_UnitCode.Where(x => x.UnitCodeID == tbl_Product.UnitCodeID).Select(x => x.UnitCode).FirstOrDefault();

                            Mapper.CreateMap<Models.DTO.ProductDTO, tbl_Product>();
                            var master = Mapper.Map<Models.DTO.ProductDTO, tbl_Product>(tbl_Product);
                            db.tbl_Product.Add(master);
                            db.SaveChanges();


                            tbl_AccountDetails ad = new tbl_AccountDetails();
                            ad.AccountCode = "GP-"+master.ProductID.ToString();
                            ad.AccountName = master.PartNo.ToString();
                            ad.AccountTypeID = 30;//COGS
                            ad.AddOn = Helper.PST();
                            ad.BranchID = 9001;
                            ad.IsActive = true;
                            ad.IsDeleted = false;
                            db.tbl_AccountDetails.Add(ad);
                            db.SaveChanges();

                            t.Commit();

                            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Added Products".ToString());
                            return RedirectToAction("GeneralProductIndex");
                        }
                    }
                    ViewBag.Category = db.tbl_VehicleCode.Where(x => x.LevelID == 0 && x.HeadID == null && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.SubCategory = db.tbl_VehicleCode.Where(x => x.LevelID == 1 && x.VehicleCodeID > 5 && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.Department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });
                    ViewBag.Section = db.tbl_VehicleCode.Where(x => x.LevelID == 2).Select(x => new { Value = x.VehicleCodeID, Name = x.VehicleCode });
                    ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
                    ViewBag.UnitCode = db.tbl_UnitCode.Where(x => x.TypeID == 1 && x.IsDeleted != true).Select(x => new { Value = x.UnitCodeID, Name = x.UnitCode });

                    return View(tbl_Product);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return Content(e.Message.ToString());
            }
          
        }

        public JsonResult ProductEditDetails(int? PID)
        {

            List<object> objectList = new List<object>();
           
            
            if (PID != null)
            {
               

                var CatID = db.tbl_Product.Where(x => x.ProductID == PID).Select(x=>x.VehicleCodeID).FirstOrDefault();
                if (CatID != null)
                {
                    var Cat = db.tbl_VehicleCode.Where(x => x.VehicleCodeID == CatID).FirstOrDefault();
                    
                    objectList.Add(new
                    {
                       
                    });
                }

               

            }
            return Json(objectList);

        }

        public ActionResult EditService(int? id)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Product tbl_Product = db.tbl_Product.Find(id);
            if (tbl_Product == null)
            {
                return HttpNotFound();
            }
            int levelID = tbl_Product.tbl_VehicleCode.LevelID ?? 0;
            var category = tbl_Product.tbl_VehicleCode;

            
            if (levelID == 0)
            {
                ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0 && v.VehicleCodeID == 1), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
                ViewBag.SubCategoryID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 1 && v.HeadID == 1), "VehicleCodeID", "VehicleCode");
                ViewBag.SectionID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 2), "VehicleCodeID", "VehicleCode");
            }
            else if (levelID == 1)
            {
                var head = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == category.HeadID && v.VehicleCodeID !=1).FirstOrDefault().VehicleCodeID;
                ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0 && v.VehicleCodeID !=1), "VehicleCodeID", "VehicleCode", head);
                ViewBag.SubCategoryID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 1 && v.HeadID != 1), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
                ViewBag.SectionID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 2), "VehicleCodeID", "VehicleCode");
            }
            else if (levelID == 2)
            {
                var sub = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == category.HeadID).FirstOrDefault();
                var head = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == sub.HeadID && v.VehicleCodeID !=1).FirstOrDefault().VehicleCodeID;
                ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0 && v.VehicleCodeID !=1), "VehicleCodeID", "VehicleCode", head);
                ViewBag.SubCategoryID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 1 && v.HeadID!=1), "VehicleCodeID", "VehicleCode", sub.VehicleCodeID);
                ViewBag.SectionID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 2), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
            }

            ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle", tbl_Product.BrandID);
            ViewBag.UnitCodeID = new SelectList(db.tbl_UnitCode, "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);
            ViewBag.IsPacket = new SelectList(new List<SelectListItem>{
                                                   new SelectListItem{ Text="Packet", Value = "1" },
                                                   new SelectListItem{ Text="Item", Value = "0"},
                                               }, "ID", "IsPacket", tbl_Product.IsPacket);
            Mapper.CreateMap<tbl_Product, Models.DTO.ProductDTO>();
            var master = Mapper.Map<tbl_Product, Models.DTO.ProductDTO>(tbl_Product);
            return View(master);
        }
        // GET: Product/Edit/5
        public ActionResult Edit(int? id)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Product tbl_Product = db.tbl_Product.Find(id);
            if (tbl_Product == null)
            {
                return HttpNotFound();
            }
            int levelID = tbl_Product.tbl_VehicleCode== null ?0: tbl_Product.tbl_VehicleCode.LevelID??0 ;
            var category = tbl_Product.tbl_VehicleCode;
            if (category.VehicleCodeID == 1)
            {
                return RedirectToActionPermanent("EditService", new { id = id });
            }
            if (levelID == 0)
            {
                ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0 && v.VehicleCodeID>5 && (v.IsDeleted != true || v.IsDeleted == null)), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
                ViewBag.SubCategoryID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 1  && v.VehicleCodeID > 5 && (v.IsDeleted != true || v.IsDeleted == null)), "VehicleCodeID", "VehicleCode");
                ViewBag.SectionID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 2), "VehicleCodeID", "VehicleCode");
                ViewBag.DepartmentID = new SelectList(db.tbl_Department.Where(v => v.IsDeleted!=true), "ID", "Department",tbl_Product.DepartmentID);
                ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted!=true), "UnitCodeID", "UnitCode",tbl_Product.UnitCodeID);
            }
            else if (levelID == 1)
            {
                var head = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == category.HeadID).FirstOrDefault().VehicleCodeID;
                ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0 && v.VehicleCodeID > 5 && (v.IsDeleted != true || v.IsDeleted == null)), "VehicleCodeID", "VehicleCode", head);
                ViewBag.SubCategoryID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 1 && v.VehicleCodeID > 5 && (v.IsDeleted != true || v.IsDeleted == null)), "VehicleCodeID", "VehicleCode",tbl_Product.VehicleCodeID);
                ViewBag.SectionID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 2), "VehicleCodeID", "VehicleCode");
                ViewBag.DepartmentID = new SelectList(db.tbl_Department.Where(v => v.IsDeleted != true), "ID", "Department", tbl_Product.DepartmentID);
                ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);
            }
            else if (levelID == 2)
            {
                var sub = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == category.HeadID).FirstOrDefault();                
                var head = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == sub.HeadID).FirstOrDefault().VehicleCodeID;
                ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0 && v.VehicleCodeID > 5 && (v.IsDeleted != true || v.IsDeleted == null)), "VehicleCodeID", "VehicleCode", head);
                ViewBag.SubCategoryID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 1 && v.VehicleCodeID > 5 && (v.IsDeleted != true || v.IsDeleted == null)), "VehicleCodeID", "VehicleCode", sub.VehicleCodeID);
                ViewBag.SectionID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 2), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
                ViewBag.DepartmentID = new SelectList(db.tbl_Department.Where(v => v.IsDeleted != true), "ID", "Department", tbl_Product.DepartmentID);
                ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);
            }
            
            ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle",tbl_Product.BrandID);
            ViewBag.UnitCodeID = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode",tbl_Product.UnitCodeID);
            ViewBag.IsPacket = new SelectList(new List<SelectListItem>{
                                                   new SelectListItem{ Text="Packet", Value = "1" },
                                                   new SelectListItem{ Text="Item", Value = "0"},
                                               }, "ID", "IsPacket", tbl_Product.IsPacket);
            ViewBag.DepartmentID = new SelectList(db.tbl_Department.Where(v => v.IsDeleted != true), "ID", "Department", tbl_Product.DepartmentID);
     //       Mapper.CreateMap<tbl_Product, Models.DTO.ProductDTO>();
            ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);
     //       var master = Mapper.Map<tbl_Product, Models.DTO.ProductDTO>(tbl_Product);
            return View(tbl_Product);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for         
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_Product tbl_Product)
        {
            //var data = db.tbl_Product.Where(x => x.ProductID == tbl_Product.ProductID).FirstOrDefault();
            //if (data != null)
            //{
                //Mapper.CreateMap<tbl_Product, tbl_Product>();
                //var mapData = Mapper.Map<tbl_Product, tbl_Product>(data);
                //tbl_Product = data

            //}
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            //if (tbl_Product.SectionID != null)
            //{
            //    tbl_Product.VehicleCodeID = tbl_Product.SectionID;

            //}
            //else if (tbl_Product.SubCategoryID != null && tbl_Product.SectionID == null)
            //{
            //    tbl_Product.VehicleCodeID = tbl_Product.SubCategoryID;
            //}
            //else if (tbl_Product.VehicleCodeID != null && tbl_Product.SubCategoryID == null && tbl_Product.SectionID == null)
            //{
            //    tbl_Product.VehicleCodeID = tbl_Product.VehicleCodeID;
            //}
            var vehCode = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == tbl_Product.VehicleCodeID).FirstOrDefault();
           if(vehCode!=null)tbl_Product.tbl_VehicleCode = vehCode;

            var uCode = db.tbl_UnitCode.Where(v => v.UnitCodeID == tbl_Product.UnitCodeID).FirstOrDefault();
            if (uCode != null)
            {
                tbl_Product.tbl_UnitCode = uCode;
            }
           
            int levelID = vehCode.LevelID ?? 0;
            var category = vehCode;
            //if (levelID == 0)
            //{
            //    ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
            //    ViewBag.SubCategory = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 1), "VehicleCodeID", "VehicleCode");
            //    ViewBag.Section = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 2), "VehicleCodeID", "VehicleCode");
            //    ViewBag.DepartmentID = new SelectList(db.tbl_Department.Where(v => v.IsDeleted != true), "ID", "Department", tbl_Product.DepartmentID);
            //    ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);
            //}
            //else if (levelID == 1)
            //{
            //    var head = db.tbl_VehicleCode.Where(v => v.HeadID == category.HeadID).FirstOrDefault().VehicleCodeID;
            //    ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0), "VehicleCodeID", "VehicleCode", head);
            //    ViewBag.SubCategoryID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 1), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
            //    ViewBag.SectionID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 2), "VehicleCodeID", "VehicleCode");
            //    ViewBag.DepartmentID = new SelectList(db.tbl_Department.Where(v => v.IsDeleted != true), "ID", "Department", tbl_Product.DepartmentID);
            //    ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);
            //}
            //else if (levelID == 2)
            //{
            //    var sub = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == category.HeadID).FirstOrDefault();
            //    var head = db.tbl_VehicleCode.Where(v => v.VehicleCodeID == sub.HeadID).FirstOrDefault().VehicleCode;
            //    ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0), "VehicleCodeID", "VehicleCode", head);
            //    ViewBag.SubCategoryID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 1), "VehicleCodeID", "VehicleCode", sub.VehicleCodeID);
            //    ViewBag.SectionID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 2), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
            //    ViewBag.DepartmentID = new SelectList(db.tbl_Department.Where(v => v.IsDeleted != true), "ID", "Department", tbl_Product.DepartmentID);
            //    ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);

            //}
            if (string.IsNullOrWhiteSpace(tbl_Product.BarCode))
            {
                string source = tbl_Product.PartNo;
                string result = string.Concat(source.Where(c => !char.IsWhiteSpace(c)));
                Random ran = new Random();
                var randomNo = ran.Next();
                string barCode = (tbl_Product.ProductID).ToString() + randomNo.ToString();
                
                if (barCode.Length > 13)
                {
                    barCode = barCode.Substring(0, 13);
                }
                else if (barCode.Length < 13)
                {
                    Random ran1 = new Random();
                    var randomNo1 = ran1.Next();
                    barCode = (barCode + randomNo1.ToString()).Substring(0, 13);
                }
                tbl_Product.BarCode = barCode;
            }
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(tbl_Product.PartNo))
                {
                    ModelState.AddModelError("PartNo", "Cannot Enter Null Entries!");
                    ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0 && v.VehicleCodeID > 5 && (v.IsDeleted != true || v.IsDeleted == null)), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
                    ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle", tbl_Product.BrandID);
                    ViewBag.DepartmentID = new SelectList(db.tbl_Department.Where(v => v.IsDeleted != true), "ID", "Department", tbl_Product.DepartmentID);
                    ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);
                    return View("Edit", tbl_Product);
                }
                else if (db.tbl_Product.Any(p => p.PartNo == tbl_Product.PartNo && p.ProductID != tbl_Product.ProductID && p.VehicleCodeID == tbl_Product.VehicleCodeID))
                {
                    ModelState.AddModelError("PartNo", "Product Already Exists!");
                    ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0 && v.VehicleCodeID > 5 && (v.IsDeleted != true || v.IsDeleted == null)), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
                    ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle", tbl_Product.BrandID);
                    ViewBag.Department = new SelectList(db.tbl_Department.Where(v => v.IsDeleted != true), "ID", "Department", tbl_Product.DepartmentID);
                    ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);
                    return View("Edit", tbl_Product);
                }
                //else if (tbl_Product.SubCategoryID == null  && tbl_Product.VehicleCodeID == null)
                //{
                //    ModelState.AddModelError("PartNo", "Please select atleast one option from above!");
                //    ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
                //    ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle");
                //    ViewBag.DepartmentID = new SelectList(db.tbl_Department.Where(v => v.IsDeleted != true), "ID", "Department", tbl_Product.DepartmentID);
                //    ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);
                //    return View("Edit", tbl_Product);
                //}
                //else if (db.tbl_Product.Any(p => p.BarCode == tbl_Product.BarCode && p.ProductID != tbl_Product.ProductID))
                //{
                //    ModelState.AddModelError("BarCode", "BarCode Already Exists!");
                //    ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v => v.LevelID == 0), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
                //    ViewBag.BrandID = new SelectList(db.tbl_Brand, "BrandID", "BrandTitle", tbl_Product.BrandID);
                //    ViewBag.DepartmentID = new SelectList(db.tbl_Department.Where(v => v.IsDeleted != true), "ID", "Department", tbl_Product.DepartmentID);
                //    ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);
                //    return View("Edit", tbl_Product);
                //}
                else
                {
                    //if (tbl_Product.SectionID != null)
                    //{
                    //    tbl_Product.VehicleCodeID = tbl_Product.SectionID;

                    //}
                    //else if (tbl_Product.SubCategoryID != null && tbl_Product.SectionID == null)
                    //{
                    //    tbl_Product.VehicleCodeID = tbl_Product.SubCategoryID;
                    //}
                    //else if (tbl_Product.VehicleCodeID != null && tbl_Product.SubCategoryID == null && tbl_Product.SectionID == null)
                    //{
                    //    tbl_Product.VehicleCodeID = tbl_Product.VehicleCodeID;
                    //}
                    tbl_Product.MinorDivisor = 1000;
                    tbl_Product.UpdateBy = userID;
                    tbl_Product.UpdateOn = Helper.PST();
                    //Mapper.CreateMap<Models.DTO.ProductDTO, tbl_Product>();
                    //var master = Mapper.Map<Models.DTO.ProductDTO, tbl_Product>(tbl_Product);
                    db.Entry(tbl_Product).State = EntityState.Modified;
                    db.Entry(tbl_Product).Property(p => p.Addby).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.isActive).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.IsGeneralItem).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.IsPacket).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.OpeningStock).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.Description).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.ImageUrl).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.Expiry).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.CostPrice).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.UserID).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.BranchID).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.AddOn).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.rowguid).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.VehicleCodeID).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.BrandID).IsModified = false;
                    db.Entry(tbl_Product).Property(p => p.DepartmentID).IsModified = false;
                    //}
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Edited Products".ToString());

                    db.SaveChanges();
                    return RedirectToAction("GeneralProductIndex");
                }
            }
            //  ViewBag.Addby = new SelectList(db.tbl_User, "ID", "UserName", tbl_Product.Addby);

              ViewBag.VehicleCodeID = new SelectList(db.tbl_VehicleCode.Where(v=>v.LevelID == 0 && v.VehicleCodeID > 5 && (v.IsDeleted != true || v.IsDeleted == null)), "VehicleCodeID", "VehicleCode", tbl_Product.VehicleCodeID);
            ViewBag.DepartmentID = new SelectList(db.tbl_Department.Where(v => v.IsDeleted != true), "ID", "Department", tbl_Product.DepartmentID);
            ViewBag.UnitCode = new SelectList(db.tbl_UnitCode.Where(v => v.TypeID == 1 && v.IsDeleted != true), "UnitCodeID", "UnitCode", tbl_Product.UnitCodeID);
            return View(tbl_Product);
        }
        [Authorize(Roles = "SuperAdmin, Admin")]
        // GET: Product/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Product tbl_Product = db.tbl_Product.Find(id);
            if (tbl_Product == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Product);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                bool isExist = db.tbl_Stock.Any(p => p.ProductID == id);
                bool isExist1 = db.tbl_PODetails.Any(p => p.ProductID == id);
                if (isExist || isExist1)
                {
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                    tbl_Product tbl_Product = db.tbl_Product.Find(id);
                    return View(tbl_Product);
                }
                else
                {
                    var product = db.tbl_Product.Where(x=>x.ProductID == id).FirstOrDefault();
                    if (product != null)
                    {
                        product.isActive = 0;
                        db.Entry(product).State = EntityState.Modified;
                        db.SaveChanges();
                        //db.tbl_Product.Remove(tbl_Product);
                        UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Products".ToString());
                        return RedirectToAction("GeneralProductIndex");
                    }

                    ModelState.AddModelError(string.Empty, "Some error occured");
                    tbl_Product tbl_Product = db.tbl_Product.Find(id);
                    return View(tbl_Product);

                }
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("GeneralProductIndex");
            }            
        }

        [Authorize(Roles = "SuperAdmin, Admin")]
        // GET: Product/Delete/5
        public ActionResult DeleteItem(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Product tbl_Product = db.tbl_Product.Find(id);
            if (tbl_Product == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Product);
        }

        // POST: Product/DeleteItem/5
        [HttpPost, ActionName("DeleteItem")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteItemConfirmed(int id)
        {
            try
            {
                bool isExist = db.tbl_StockIssuance.Any(p => p.ItemID == id);
                bool isExist1 = db.tbl_SaleDetails.Any(p => p.ProductID == id);
                bool isExist2 = db.tbl_DODetails.Any(p => p.ProductID == id);
                if (isExist || isExist1 || isExist2)
                {
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                    tbl_Product tbl_Product = db.tbl_Product.Find(id);
                    return View(tbl_Product);
                }
                else
                {
                    var product = db.tbl_Product.Where(x => x.ProductID == id).FirstOrDefault();
                    if (product != null)
                    {
                        product.isActive = 0;
                        db.Entry(product).State = EntityState.Modified;
                        db.SaveChanges();
                        //db.tbl_Product.Remove(tbl_Product);
                        UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Products".ToString());
                        return RedirectToAction("GeneralProductIndex");
                    }

                    ModelState.AddModelError(string.Empty, "Some error occured");
                    tbl_Product tbl_Product = db.tbl_Product.Find(id);
                    return View(tbl_Product);

                }
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("GeneralProductIndex");
            }
        }
        // Get Products by Vehicle COde Id
        public JsonResult getProducts(int vehCodeID)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (vehCodeID > 0)
            {
                var qry = db.tbl_Product.Where(p => p.VehicleCodeID == vehCodeID).Select(p => new { Value = p.ProductID, Name = p.PartNo }).ToList();
                return Json(qry);
            }
            else
            {
                var qry = db.tbl_Product.Select(p => new { Value = p.ProductID, Name = p.PartNo }).ToList();
                return Json(qry);
            }
        }

        public JsonResult getServiceProducts(int vehCodeID)
        {
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (vehCodeID > 0)
            {
                var qry = db.tbl_Product.Where(p => p.VehicleCodeID == vehCodeID /*&& p.VehicleCodeID == 1*/).Select(p => new { Value = p.ProductID, Name = p.PartNo }).ToList();
                return Json(qry);
            }
            else
            {
                var qry = db.tbl_Product.Where(p => p.VehicleCodeID == 1).Select(p => new { Value = p.ProductID, Name = p.PartNo }).ToList();
                return Json(qry);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }      
    }
}
