﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class DeliveryOrderController : Controller
    {
        private dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();

        
       // GET: DeliveryOrder
        [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult Index(bool? isSearch, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.customer = db.tbl_Customer.Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            if (isSearch==true)
            {
                List<GetDOCustomerWise_Result> customer = db.GetDOCustomerWise(AccountID,fromDate,toDate, branchId).ToList();
                if (customer.Count == 0)
                {
                    return PartialView("_CustomerDO");
                }
                else
                    return PartialView("_CustomerDO", customer);
            }
            return View();
        }
        // // Get Last Delivery Order ID and return it to js and increment in it +1 
        public JsonResult getLastDOID()
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            string DOID = "";
            var tmp = db.tbl_DeliveryOrder.Where(d => d.BranchID == branchId).OrderByDescending(v => v.OrderID).FirstOrDefault();
            if (tmp != null)
            {
                DOID = tmp.DOID;
                if (DOID == null)
                {
                    DOID = "DO-100";
                }
            }
            else
            {
                DOID = "DO-100";
            }

            return Json(DOID, JsonRequestBehavior.AllowGet);
        }
        // check PONO duplication for selected user 
        public JsonResult checkPONO(int accountID, string PONO)
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var row = db.tbl_DeliveryOrder.Where(s => s.PONo == PONO && s.AccountID == accountID && s.BranchID == branchId).FirstOrDefault();
            if (row != null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            { return Json(false, JsonRequestBehavior.AllowGet); }

        }
        // GET: DeliveryOrder/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<tbl_DODetails> tbl_DODetails =  db.tbl_DODetails.Where(p => p.OrderID == id).ToList();
            if (tbl_DODetails == null)
            {
                return HttpNotFound();
            }
            return PartialView(tbl_DODetails);
        }

        // GET: DeliveryOrder/Create
        [Authorize(Roles = "SuperAdmin,Admin,SalesPerson")]
        public ActionResult Create()
        {
            int branchId = 0;            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            //ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();            
            ViewBag.BranchId = branchId;
            return View();
        }
        // Create Delivery Order, Also change Stock  as Discussed by Boss
        public JsonResult SaveOrder(Models.DTO.DeliveryOrder model, List<Models.DTO.StockLog> modelStockLog)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            POSOman.Models.BLL.DeliveryOrder dOrder = new Models.BLL.DeliveryOrder();
            dOrder.createSales(model, modelStockLog,branchId);
            UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Delivery Orders Action ".ToString());

            return Json("success");            
        }
        // Create invoice of DO
        public ActionResult SubmitDeliveryOrders(int[] OrderID)
        {
            //To get what you have stored to a session
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.BranchId = branchId;
            var orderIDs = Session["orderIDs"];
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
           // ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            ViewBag.OrderID = orderIDs;
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27 ).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            return View();
        }
        public JsonResult saveOrderIDs(int[] OrderIDs) // orderID=pk, tOrderId= Holdid or QouteID
        {
            //Store the products to a session
            Session["orderIDs"] = OrderIDs;
            
            return Json("Success");
        }
        // Get Delivery Orders for creating Invoice
        public JsonResult getOrdersData(int[] OrderIDs) // orderID=pk, tOrderId= Holdid or QouteID
        {
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            if (OrderIDs != null)
            {
                try
                {
                    var qry = db.tbl_DODetails.Where(p => OrderIDs.Contains(p.OrderID) && p.BranchID == branchId)
                        .Select(p => new
                        {
                            p.tbl_DeliveryOrder.DOID,
                            p.tbl_DeliveryOrder.VehicleNo,
                            p.tbl_DeliveryOrder.PONo,
                            p.tbl_DeliveryOrder.BranchID,
                            p.OrderID,
                            p.ProductID,
                            p.tbl_Product.PartNo,
                            p.tbl_Product.Description,
                            p.tbl_Product.tbl_VehicleCode.VehicleCode,                           
                            p.tbl_Product.UnitCode,
                            p.tbl_Product.OpenUnitCode,
                            p.tbl_Product.Location,
                            p.tbl_DeliveryOrder.AccountID,                            
                            p.tbl_DeliveryOrder.SalesDate,
                            p.tbl_DeliveryOrder.TotalAmount,                            
                            p.tbl_DeliveryOrder.InvoiceNo,
                            p.Qty,
                            p.ReturnedQty,
                            p.UnitPrice,                                                        
                            p.SalePrice,
                            p.Total
                        }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }

            }

            return Json("");
        }
        // Create Invoice
        public JsonResult CreateInvoice(Models.DTO.Sales model, int? bankAccId)
        {

            int[] orderIDs = Session["orderIDs"] as int[];
            POSOman.Models.BLL.DeliveryOrder dOrder = new Models.BLL.DeliveryOrder();
            var result = dOrder.createInvoice(model, orderIDs,bankAccId);
            Session.Remove("orderIDs");
            return Json(result);
            
        }
        // POST: DeliveryOrder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrderID,DOID,PurchaseOrderID,BranchID,AccountID,TmpID,PONo,VehicleNo,InvoiceNo,SalesDate,PaymentStatus,PaymentTypeID,TotalAmount,AddOn,AddBy,UpdateOn,UpdateBy,IsReturned,IsDeleted,IsPaid,Status")] tbl_DeliveryOrder tbl_DeliveryOrder)
        {
            if (ModelState.IsValid)
            {
                db.tbl_DeliveryOrder.Add(tbl_DeliveryOrder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            int branchId = 0;
            
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.AccountID = new SelectList(db.tbl_AccountDetails.Where(acd => acd.BranchID == branchId), "AccountID", "AccountName", tbl_DeliveryOrder.AccountID);
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name", tbl_DeliveryOrder.PaymentTypeID);            
            return View(tbl_DeliveryOrder);
        }

        // GET: DeliveryOrder/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_DeliveryOrder tbl_DeliveryOrder = db.tbl_DeliveryOrder.Find(id);
            if (tbl_DeliveryOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccountID = new SelectList(db.tbl_AccountDetails, "AccountID", "AccountName", tbl_DeliveryOrder.AccountID);
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name", tbl_DeliveryOrder.PaymentTypeID);            
            return View(tbl_DeliveryOrder);
        }

        // POST: DeliveryOrder/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderID,DOID,PurchaseOrderID,BranchID,AccountID,TmpID,PONo,VehicleNo,InvoiceNo,SalesDate,PaymentStatus,PaymentTypeID,TotalAmount,AddOn,AddBy,UpdateOn,UpdateBy,IsReturned,IsDeleted,IsPaid,Status")] tbl_DeliveryOrder tbl_DeliveryOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_DeliveryOrder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AccountID = new SelectList(db.tbl_AccountDetails, "AccountID", "AccountName", tbl_DeliveryOrder.AccountID);
            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name", tbl_DeliveryOrder.PaymentTypeID);
            
            return View(tbl_DeliveryOrder);
        }

        // GET: DeliveryOrder/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_DeliveryOrder tbl_DeliveryOrder = db.tbl_DeliveryOrder.Find(id);
            if (tbl_DeliveryOrder == null)
            {
                return HttpNotFound();
            }
            return View(tbl_DeliveryOrder);
        }

        // POST: DeliveryOrder/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_DeliveryOrder tbl_DeliveryOrder = db.tbl_DeliveryOrder.Find(id);
            db.tbl_DeliveryOrder.Remove(tbl_DeliveryOrder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
