﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class TAXController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: TAX
        public ActionResult Index()
        {
            return View(db.tbl_TAX.ToList());
        }

        // GET: TAX/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_TAX tbl_TAX = db.tbl_TAX.Find(id);
            if (tbl_TAX == null)
            {
                return HttpNotFound();
            }
            return View(tbl_TAX);
        }

        // GET: TAX/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TAX/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( tbl_TAX tbl_TAX)
        {
            if (ModelState.IsValid)
            {
                using(var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        decimal parsedValue;
                        if (db.tbl_TAX.Any(x=>x.Tax == tbl_TAX.Tax))
                        {
                            ModelState.AddModelError("Tax", "Duplicate Tax Name!");
                            return View(tbl_TAX);
                        }
                        else if (tbl_TAX.TaxTypeID<=0)
                        {
                            ModelState.AddModelError("TaxTypeID", "Please Select Tax Type!");
                            return View(tbl_TAX);
                        }
                        else if (string.IsNullOrWhiteSpace(tbl_TAX.Tax))
                        {
                            ModelState.AddModelError("Tax", "Please Enter Tax Name!");
                            return View(tbl_TAX);
                        }
                       
                        else if (!decimal.TryParse(tbl_TAX.AmountInPercentage.ToString(), out parsedValue))
                        {
                            ModelState.AddModelError("AmountInPercentage", "Please Enter Numeric Value!");
                            return View(tbl_TAX);
                        }
                        else if (tbl_TAX.AmountInPercentage<=0 || tbl_TAX.AmountInPercentage>100)
                        {
                            ModelState.AddModelError("AmountInPercentage", "Please Enter Valid Amount!");
                            return View(tbl_TAX);
                        }
                        else
                        {
                            tbl_TAX.IsActive = true;
                            tbl_TAX.IsDeleted = false;
                            tbl_TAX.AddedOn = Helper.PST();
                            db.tbl_TAX.Add(tbl_TAX);
                            db.SaveChanges();
                            t.Commit();
                            return RedirectToAction("Index");
                        }
                       
                    }
                    catch(Exception d)
                    {
                        t.Rollback();
                        ModelState.AddModelError("Tax", "Something went wrong!");
                        return View(tbl_TAX);
                    }
                }
              
            }
            else
            {
                ModelState.AddModelError("Tax", "Undefined Entries!");
                return View(tbl_TAX);
            }

           
        }

        // GET: TAX/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_TAX tbl_TAX = db.tbl_TAX.Find(id);
            if (tbl_TAX == null)
            {
                return HttpNotFound();
            }
            return View(tbl_TAX);
        }

        // POST: TAX/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( tbl_TAX tbl_TAX)
        {
            if (ModelState.IsValid)
            {
                using (var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        decimal parsedValue;
                        if (db.tbl_TAX.Any(x => x.Tax == tbl_TAX.Tax && tbl_TAX.TaxID!=x.TaxID))
                        {
                            ModelState.AddModelError("Tax", "Duplicate Tax Name!");
                            return View(tbl_TAX);
                        }
                       
                        else if (string.IsNullOrWhiteSpace(tbl_TAX.Tax))
                        {
                            ModelState.AddModelError("Tax", "Please Enter Tax Name!");
                            return View(tbl_TAX);
                        }

                        else if (!decimal.TryParse(tbl_TAX.AmountInPercentage.ToString(), out parsedValue))
                        {
                            ModelState.AddModelError("AmountInPercentage", "Please Enter Numeric Value!");
                            return View(tbl_TAX);
                        }
                        else if (tbl_TAX.AmountInPercentage <= 0 || tbl_TAX.AmountInPercentage > 100)
                        {
                            ModelState.AddModelError("AmountInPercentage", "Please Enter Valid Amount!");
                            return View(tbl_TAX);
                        }
                        else
                        {
                            var Update = db.tbl_TAX.Where(x => x.TaxID == tbl_TAX.TaxID).FirstOrDefault();
                            if (Update != null)
                            {
                                Update.Tax = tbl_TAX.Tax;
                                Update.TaxDescription = tbl_TAX.TaxDescription;
                                Update.AmountInPercentage = tbl_TAX.AmountInPercentage;

                                db.Entry(Update).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                           
                           
                            t.Commit();
                            return RedirectToAction("Index");
                        }

                    }
                    catch (Exception d)
                    {
                        t.Rollback();
                        ModelState.AddModelError("Tax", "Something went wrong!");
                        return View(tbl_TAX);
                    }
                }

            }
            else
            {
                ModelState.AddModelError("Tax", "Undefined Entries!");
                return View(tbl_TAX);
            }
        }

        // GET: TAX/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_TAX tbl_TAX = db.tbl_TAX.Find(id);
            if (tbl_TAX == null)
            {
                return HttpNotFound();
            }
            return View(tbl_TAX);
        }

        // POST: TAX/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_TAX tbl_TAX = db.tbl_TAX.Find(id);
            db.tbl_TAX.Remove(tbl_TAX);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
