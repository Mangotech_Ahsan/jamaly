﻿using Microsoft.AspNet.Identity;
using POSOman.Models;
using POSOman.Models.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class VendorAdjustmentController : Controller
    {
        VendorPayment vendorPay = new VendorPayment();
        AdjustmentEntry adEntry = new AdjustmentEntry();
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
       
        // GET: VendorAdjustment
        public ActionResult Index()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            var adjustments = db.tbl_JDetail.Where(j => j.BranchID == branchId && (j.EntryTypeID == 13));
            return View(adjustments.ToList());
        }
        public ActionResult Create()
        {
            try
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                ViewBag.vendor = db.tbl_Vendor.Select(v => new { Value = v.AccountID, Name = v.VendorCode + "|" + v.Name }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        public JsonResult getVendorDetail(int accountId)
        {
            if (accountId > 0)
            {
                try
                {
                    int branchId = 0;
                    if (Session["BranchID"] != null)
                    {
                        branchId = Convert.ToInt32(Session["BranchID"]);
                    }
                    else
                    {
                        var user = User.Identity;
                        string currentUserId = User.Identity.GetUserId();
                        var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                        branchId = currentUser.BranchID;
                    }
                    var qry = db.tbl_PurchaseOrder.Where(p => p.AccountID == accountId && p.IsPaid == false && p.IsReturned == false)
                        //&& p.BranchID ==branchId)
                        .Select(p => new
                        {
                            p.OrderID,
                            p.POID,
                            p.AccountID,
                            p.tbl_AccountDetails.AccountName,
                            p.InvoiceNo,
                            p.PurchaseDate,
                            p.TotalAmount,
                            p.VAT,
                            p.AmountPaid,
                            p.TotalPaid,
                            p.ReturnAmount,
                            p.PaymentStatus
                        }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }

            }

            return Json("");

        }

        public JsonResult SavePayment(Payment model, List<Models.DTO.JEntryLog> jentryLog)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                object jentryID = adEntry.SaveVendorAdjustment(model, jentryLog, branchId);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Vendor Adjustments".ToString());

                return Json(jentryID);                
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Error Performed Vendor Adjustments".ToString());

                return Json("formError");
            }
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            Models.DTO.PaymentLog jLog = new Models.DTO.PaymentLog();
            var query = from jlog in db.tbl_JEntryLog
                        join je in db.tbl_JEntry on jlog.JEntryID equals je.JEntryId
                        join jd in db.tbl_JDetail on je.JEntryId equals jd.JEntryID
                        join so in db.tbl_PurchaseOrder on jlog.OrderID equals so.OrderID
                        join acd in db.tbl_AccountDetails on jd.AccountID equals acd.AccountID
                        where jd.Cr > 0 && jlog.JEntryID == id
                        select new Models.DTO.JEntryLogModel
                        {
                            AccountName = acd.AccountName,
                            InvoiceNo = so.POID,
                            Amount = jlog.Amount ?? 0
                        };
            if (query == null)
            {
                return HttpNotFound();
            }
            return View(query.ToList());
        }
        [Authorize(Roles = "SuperAdmin")]
        // POST: Entry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                adEntry.DeleteVendorAdjustment(id);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Deleted Vendor Adjustments".ToString());

                return RedirectToAction("Index");
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
        }
    }
}