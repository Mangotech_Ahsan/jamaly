﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using POSOman.Models;
using POSOman.Models.BLL;
using POSOman.Models.DTO;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext context;
        Models.dbPOS db = new Models.dbPOS();
        public ActionResult IndexOld()
        {
            if (User.Identity.IsAuthenticated)
            {
                context = new ApplicationDbContext();
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var BranchID = 0;
                var userID = 0;
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                if (currentUser != null)
                {
                    BranchID = currentUser.BranchID;
                    userID = currentUser.UserId;
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    var s = UserManager.GetRoles(currentUserId);
                    ViewBag.userRole = s[0].ToString();
                    Session["userRole"] = s[0].ToString();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
                var temp = Session["BranchID"];
                Session["BranchID"] = BranchID;
                Session["UserID"] = userID;
                ViewBag.purchases = db.tbl_PurchaseOrder.Where(p => p.BranchID == BranchID).Sum(p => p.TotalAmount).GetValueOrDefault();
                ViewBag.TotalSales = db.tbl_SalesOrder.Where(so => so.BranchID == BranchID).Sum(p => p.TotalAmount).GetValueOrDefault();
                var raPA = db.GetAPAR().FirstOrDefault();
                ViewBag.payable = raPA.PA;
                ViewBag.receiveable =raPA.RA;
                var expiryBranches = db.GetExpiryBranchesData().ToList();
                var expiryCount = db.GetExpiryData().FirstOrDefault();
                int? branches = 0;
                int? employee = 0;
                List<String> br = new List<string>();
                foreach (var item in expiryBranches)
                {
                    br.Add(item.BranchName);
                }

                ViewBag.Branches = 8;


                if (expiryCount.Branch > 0)
                {
                    branches = expiryCount.Branch;
                }
                if (expiryCount.Employee > 0)
                {
                    employee = expiryCount.Employee;
                    ViewBag.employee = employee + " Employee(s) ID/Passport will expire in 15 Days";
                }
                ViewBag.expCount = db.GetNotificationCount().FirstOrDefault().SalesCreditCount;
                ViewBag.expIssuedPDCCount = db.GetNotificationCount().FirstOrDefault().IssuedPDCCount;
                ViewBag.expRecdPDCCount = db.GetNotificationCount().FirstOrDefault().RecdPDCCount;
                ViewBag.TotalCount = db.GetNotificationCount().FirstOrDefault().SalesCreditCount + db.GetNotificationCount().FirstOrDefault().IssuedPDCCount  + db.GetNotificationCount().FirstOrDefault().RecdPDCCount;
                ViewBag.Sales = db.GetDashBoardSales().ToList();
                ViewBag.TopSoldProduct = db.GetTopMonthlyProduct().ToList();
                ViewBag.TopPayables = db.GetTop5Payables().ToList();
                ViewBag.TopReceiveables = db.GetTop5ReceiveAbles().ToList();
                return View("IndexNew");
            }
            else
                return RedirectToAction("Login", "Account");
        }

        
        public JsonResult GetNotification(int typeId) 
        {
            NotificationModel m = null;
            try
            {
                m = new NotificationModel();
                if (typeId == 1) // Pending Cheques Notification
                {
                    bool isPendingCheques = db.tbl_Cheques.Any(x => x.ChequeDate <= DateTime.Now && (x.IsCleared !=true || x.IsCleared == null) && x.IsPDC == true && (x.IsDeleted!=true || x.IsDeleted == null));

                    if (isPendingCheques)
                    {
                        m.msg = "<strong>You have over dues PDC.<br> Please clear them.<br> Go to <a style='color:black;' href='../Reports/GetCheques?isRec=1' target='_blank'><text style='font-weight:bold;'>Received PDC</text></a> Page</strong>";
                        m.msgTitle = "Pending PDCs";
                        m.msgType = "warning";
                    }
                }
                
                return Json(m, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return Json(m, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BulkDeleteJO()
        {
            
            try
            {
                db.BulkDeleteJobOrders_SP();

                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult RestSharpClient(int ClientID)
        {
           return Json(Convert.ToBoolean(RSharp.RestSharpClient(ClientID)), JsonRequestBehavior.AllowGet);

        }

        [Authorize]
        public ActionResult Index()
        {
            var response = RSharp.RestSharpClient(1);
            if (response == true)
            {
                return RedirectToAction("CustomLogOff", "Account");
            }
            var getSystemData = Helper.GetHostNameAndIPAddress();
            List<tbl_GenericDataTable> dataList = new List<tbl_GenericDataTable>();
            if (getSystemData != null)
            {
                if(!db.tbl_GenericDataTable.Any(x => x.DataValue.Equals(getSystemData.HostName)) && !db.tbl_GenericDataTable.Any(x => x.DataValue.Equals(getSystemData.IpAddress)))
                {
                    tbl_GenericDataTable host_data = new tbl_GenericDataTable();
                    host_data.AddedOn = Helper.PST();
                    host_data.DataKey = "HostName";
                    host_data.DataValue = getSystemData.HostName;
                    dataList.Add(host_data);

                    tbl_GenericDataTable ip_data = new tbl_GenericDataTable();
                    ip_data.AddedOn = Helper.PST();
                    ip_data.DataKey = "Ip_Address";
                    ip_data.DataValue = getSystemData.IpAddress;


                    dataList.Add(ip_data);

                    db.tbl_GenericDataTable.AddRange(dataList);
                    db.SaveChanges();
                }
                
            }
            if (User.IsInRole("SuperAdmin") || User.IsInRole("Admin"))
            {
                ViewBag.SatinSales = db.GetDashBoardSalesDepartmentWise(2).ToList();
                ViewBag.OffsetSales = db.GetDashBoardSalesDepartmentWise(1).ToList();
                ViewBag.StickerSales = db.GetDashBoardSalesDepartmentWise(3).ToList();
                ViewBag.purchases = db.tbl_PurchaseOrder.Where(p => p.IsDeleted == null || p.IsDeleted !=true).Sum(p => p.TotalAmount).GetValueOrDefault();
                ViewBag.TotalSales = db.tbl_SalesOrder.Where(so => so.IsDeleted != true).Sum(p => p.TotalAmount).GetValueOrDefault();
                var acc_payable_recieveables = db.GetAPAR().FirstOrDefault();
                ViewBag.payable = acc_payable_recieveables.PA;
                ViewBag.receiveable = acc_payable_recieveables.RA;
                return View("AdminIndex");
            }
            else if (User.IsInRole("Offset Manager"))
            {
                return RedirectToActionPermanent("OffsetIndex");
            }
            else if (User.IsInRole("Sticker Manager"))
            {
                return RedirectToActionPermanent("StickerIndex");
            }
            else if (User.IsInRole("Satin Manager"))
            {
                return RedirectToActionPermanent("SatinIndex");
            }
            return View("NotFound");
        }
        public ActionResult SatinIndex()
        {
            ViewBag.Sales = db.GetDashBoardSalesDepartmentWise(2).ToList();
            return View();
        }
        public ActionResult OffsetIndex()
        {
            ViewBag.Sales = db.GetDashBoardSalesDepartmentWise(1).ToList();
            return View();
        }

        public ActionResult StickerIndex()
        {
            ViewBag.Sales = db.GetDashBoardSalesDepartmentWise(3).ToList();
            return View();
        }

        [HttpGet]
        public JsonResult GetSalesGraphData(int DeptID)
        {
            List<object> data = new List<object>();
            var getDeptWiseOrderID = db.tbl_SaleDetails.Where(x => x.tbl_Product.DepartmentID == DeptID).Select(x => x.OrderID).Distinct().ToList();

            var orders = db.tbl_SalesOrder.Where(x => (x.IsDeleted != true || x.IsDeleted == null) && getDeptWiseOrderID.Contains(x.OrderID)).ToList();

            for (int m = 1; m <= 12; m++)
            {
                int totalDays = DateTime.DaysInMonth(2021, m);
                decimal monthWiseTotalAmount = 0;
                string saleMonth = new DateTime(2021, m, 1).ToString("MMM", CultureInfo.InvariantCulture);
                for (int d = 1; d <= totalDays; d++)
                {
                    foreach (var s in orders)
                    {
                        if (Convert.ToDateTime(s.SalesDate).Day == d && Convert.ToDateTime(s.SalesDate).Month == m)
                        {
                            monthWiseTotalAmount += s.TotalAmount ?? 0;
                        }
                    }
                }

                data.Add(new { x = saleMonth, y = monthWiseTotalAmount });
            }

            return Json(data, JsonRequestBehavior.AllowGet);

        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
