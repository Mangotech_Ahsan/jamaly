﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using Microsoft.AspNet.Identity;

namespace POSOman.Controllers
{
    public class BrandController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: Brand
        public ActionResult Index()
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture=db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            return View(db.tbl_Brand.ToList());
        }

        // GET: Brand/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Brand tbl_Brand = db.tbl_Brand.Find(id);
            if (tbl_Brand == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Brand);
        }

        // GET: Brand/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Brand/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BrandID,BrandTitle,IsActive,AddOn,Addby,UpdateOn,UpdateBy,DeleteOn,DeleteBy,ImageUrl,Description")] tbl_Brand tbl_Brand)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            if (ModelState.IsValid)
            {
                if (db.tbl_Brand.Any(v => v.BrandTitle == tbl_Brand.BrandTitle))
                {
                    ModelState.AddModelError("BrandTitle", "Already Exists!");
                    return View("Create");
                }
                else
                {
                    var user = User.Identity;
                    tbl_Brand.AddOn = DateTime.UtcNow.AddHours(5).Date;
                    tbl_Brand.Addby = userID;                    
                    tbl_Brand.IsActive = true;
                    db.tbl_Brand.Add(tbl_Brand);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }                
            }
            return View(tbl_Brand);
        }

        // GET: Brand/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Brand tbl_Brand = db.tbl_Brand.Find(id);
            if (tbl_Brand == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Brand);
        }

        // POST: Brand/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BrandID,BrandTitle,IsActive,AddOn,Addby,UpdateOn,UpdateBy,DeleteOn,DeleteBy,ImageUrl,Description")] tbl_Brand tbl_Brand)
        {
            if (ModelState.IsValid)
            {
                int branchId = 0;
                int userID = 0;
                string currentUserId = "";
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                    userID = Convert.ToInt32(Session["UserID"]);
                }
                else
                {
                    var user = User.Identity;
                    currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                    userID = currentUser.UserId;
                }
                if (db.tbl_Brand.Any(c => c.BrandTitle == tbl_Brand.BrandTitle && c.BrandID != tbl_Brand.BrandID))
                {
                    ModelState.AddModelError("BrandTitle", "Already Exists!");
                    return View("Edit");
                }
                else
                {
                    db.Entry(tbl_Brand).State = EntityState.Modified;
                    tbl_Brand.UpdateBy = userID;
                    tbl_Brand.UpdateOn = DateTime.UtcNow.AddHours(5);
                    db.Entry(tbl_Brand).Property(x => x.ImageUrl).IsModified = false;
                    db.Entry(tbl_Brand).Property(x => x.AddOn).IsModified = false;
                    db.Entry(tbl_Brand).Property(x => x.Addby).IsModified = false;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }               
            }
            return View(tbl_Brand);
        }

        // GET: Brand/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Brand tbl_Brand = db.tbl_Brand.Find(id);
            if (tbl_Brand == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Brand);
        }

        // POST: Brand/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                bool isExist = db.tbl_Product.Any(p => p.BrandID == id);
                if (isExist)
                {
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                    tbl_Brand Brand = db.tbl_Brand.Find(id);
                    if (Brand == null)
                    {
                        return HttpNotFound();
                    }
                    return View(Brand);
                }
                else
                {
                    tbl_Brand tbl_Brand = db.tbl_Brand.Find(id);
                    db.tbl_Brand.Remove(tbl_Brand);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("Index");
            }
           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
