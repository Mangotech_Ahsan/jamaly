﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class OrderMachineController : Controller
    {
        private dbPOS db = new dbPOS();

        public FileResult OrderMachineWiseProductionReport(int OrderMachineID)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/OrderMachineProductionReport.rdlc");
            ReportDataSource masterData = new ReportDataSource();

            masterData.Name = "OrderMachineProductionDataSet";

            #region Implemented Data source
            try
            {
                List<GetOrderMachineWiseProductionReportFilterWise_Result> list = db.GetOrderMachineWiseProductionReportFilterWise(null, OrderMachineID, null, null).ToList();
                if (list.Count > 0)
                {
                    masterData.Value = list;

                }

            }
            catch (Exception c)
            {
                masterData.Value = null;

            }
            #endregion

            lo.DataSources.Add(masterData);


            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            //Response.AddHeader("Content-Disposition", "attachment; filename=SaleInvoice.pdf");

            return new FileContentResult(renderedBytes, mimeType);
        }

        // GET: OrderMachine
        public ActionResult Index()
        {
            return View(db.tbl_OrderMachine.Where(x=>x.IsDeleted!=true).ToList());
        }

        [HttpGet]
        public async Task<JsonResult> getMachineDetails(int OrderMachineID,int ProdID)
        {
            decimal impression = 0;
            var getProd = await db.tbl_Product.Where(x => x.ProductID == ProdID).Select(x => new { x.NOOfImpressions, x.GroundColor, x.GroundCost, x.TextColor, x.TextCost }).FirstOrDefaultAsync();
            if (getProd != null)
            {
                impression = getProd.NOOfImpressions * ((getProd.TextColor * getProd.TextCost) + (getProd.GroundColor * getProd.GroundCost));
            }
            //var data = await db.tbl_OrderMachine.Where(x => x.ID == OrderMachineID).Select(x => new { x.ID, x.Impressions }).FirstOrDefaultAsync();
            if (impression >0)
            {
                return Json(impression.ToString("0.##"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }

        }

        // GET: OrderMachine/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_OrderMachine tbl_OrderMachine = db.tbl_OrderMachine.Find(id);
            if (tbl_OrderMachine == null)
            {
                return HttpNotFound();
            }
            return View(tbl_OrderMachine);
        }

        // GET: OrderMachine/Create
        public ActionResult Create()
        {
            ViewBag.DeptID = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });
            ViewBag.MachineTypeID = db.tbl_OrderMachineTypes.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.TypeName });
            return View();
        }

        // POST: OrderMachine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_OrderMachine model)
        {
            if (ModelState.IsValid)
            {
                ViewBag.DeptID = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });
                ViewBag.MachineTypeID = db.tbl_OrderMachineTypes.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.TypeName });

                if (string.IsNullOrWhiteSpace(model.OrderMachine))
                {
                    ModelState.AddModelError("OrderMachine", "Required");
                    return View(model);
                }
                if (model.MachineTypeID <=0 || model.MachineTypeID == null)
                {
                    ModelState.AddModelError("MachineTypeID", "Required");
                    return View(model);
                }
                if (model.DeptID <= 0 || model.DeptID == null)
                {
                    ModelState.AddModelError("DeptID", "Required");
                    return View(model);
                }

                model.AddedOn = Helper.PST();
                db.tbl_OrderMachine.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: OrderMachine/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            tbl_OrderMachine tbl_OrderMachine = db.tbl_OrderMachine.Find(id);
            if (tbl_OrderMachine == null)
            {
                return HttpNotFound();
            }
            ViewBag.Dept = new SelectList(db.tbl_Department, "ID", "Department", tbl_OrderMachine.DeptID);
            //ViewBag.DeptID = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });
            //ViewBag.MachineTypeID = db.tbl_OrderMachineTypes.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.TypeName });
            ViewBag.MachineType = new SelectList(db.tbl_OrderMachineTypes, "ID", "TypeName", tbl_OrderMachine.MachineTypeID);

            return View(tbl_OrderMachine);
        }

        // POST: OrderMachine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_OrderMachine model)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Dept = db.tbl_Department.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.Department });
                ViewBag.MachineType = db.tbl_OrderMachineTypes.Where(x => x.IsDeleted != true).Select(x => new { Value = x.ID, Name = x.TypeName });

                if (string.IsNullOrWhiteSpace(model.OrderMachine))
                {
                    ModelState.AddModelError("OrderMachine", "Required");
                    return View(model);
                }
                if (model.MachineTypeID <= 0 || model.MachineTypeID == null)
                {
                    ModelState.AddModelError("MachineTypeID", "Required");
                    return View(model);
                }
                if (model.DeptID <= 0 || model.DeptID == null)
                {
                    ModelState.AddModelError("DeptID", "Required");
                    return View(model);
                }
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: OrderMachine/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_OrderMachine tbl_OrderMachine = db.tbl_OrderMachine.Find(id);
            if (tbl_OrderMachine == null)
            {
                return HttpNotFound();
            }
            return View(tbl_OrderMachine);
        }

        // POST: OrderMachine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_OrderMachine tbl_OrderMachine = db.tbl_OrderMachine.Find(id);

            if(db.tbl_Product.Any(x => x.MachineID == id))
            {
                ModelState.AddModelError("OrderMachine", "Record cannot be deleted");
                return View(tbl_OrderMachine);
            }

            tbl_OrderMachine.IsDeleted = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
