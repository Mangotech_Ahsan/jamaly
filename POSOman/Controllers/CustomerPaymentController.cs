﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Net;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using POSOman.Models.BLL;
using System.Configuration;
using POSOman.Models.DTO;
using System.Data.Entity;
using System.Threading.Tasks;

namespace POSOman.Controllers
{
    public class CustomerPaymentController : Controller
    {
        dbPOS db = new dbPOS();
        CustomerPayment customerPay = new CustomerPayment();
        UserActionsPerformed UserActions = new UserActionsPerformed();
       
        public int getBranchID()
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        // GET: CustomerPayment
        public ActionResult Index()
        {
            int branchId = getBranchID();
            var customerVoucher = db.tbl_JDetail.Where(j => j.EntryTypeID == 2 && (j.IsDeleted == false || j.IsDeleted == null));// && j.BranchID == branchId);
            return View(customerVoucher.ToList());
        }
        
        public ActionResult Statement_UnChanged(int? AccountID, DateTime? fromDate, DateTime? toDate,int? BranchID)
        {
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            if (AccountID > 0)
            {
                List<GetCustomerPaymentDateWise_Result> customer = db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate,BranchID).ToList();
                if (customer.Count == 0)
                {
                    return PartialView("_Statement");
                }
                else
                {
                    List<StatementDTO> list = new List<StatementDTO>();
                    foreach(var i in customer)
                    {
                        list.Add(new StatementDTO
                        {
                            Date = i.VoucherDate != null ? Convert.ToString(string.Format("{0:dd/MM/yyyy}", i.VoucherDate)) : "-",
                            AccName = i.VoucherName ?? "-",
                            Description = i.Description ?? "-",
                            Detail = i.Detail ?? "-",
                            PayStatus = i.PaymentStatus ?? "-",
                            Aging = i.Aging!=null ? i.Aging.ToString():"",
                            Dr = Convert.ToDecimal(i.Debit).ToString("#,##0.00"),
                            Cr = Convert.ToDecimal(i.Credit).ToString("#,##0.00"),
                            Balance = string.IsNullOrWhiteSpace(i.Balance)?"-": Convert.ToDecimal(i.Balance).ToString("#,##0.00"),
                            OpeningBalance = i.OpBalance == null ?"-": Convert.ToDecimal(i.OpBalance).ToString("#,##0.00")
                        });

                    }
                    return PartialView("_Statement", list);
                }
            }

            return View("CustomerStatement");            
        }


        public ActionResult Statement()//int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
           
            return View("CustomerStatement");
        }


        [HttpPost]
        public JsonResult DataList(DTParameters param, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {
            int TotalCount = 0;
            decimal OpBal = 0;
            decimal ClBal = 0;
            var filtered = this.GetFilteredData(param.Search.Value, param.SortOrder, param.Start, param.Length, out TotalCount,AccountID, fromDate, toDate);

            var dataList = (filtered != null && filtered.Count > 0) ? filtered.Select(i => new StatementDTO()
            {
                Date = i.Date,
                AccName = i.AccName ?? "-",
                Description = i.Description ?? "-",
                Detail = i.Detail ?? "-",
                PayStatus = i.PayStatus ?? "-",
                Aging = i.Aging != null ? i.Aging.ToString() : "",
                Dr = i.Dr,
                Cr = i.Cr,
                Balance = i.Balance,
                OpeningBalance = i.OpeningBalance,
                detailList = i.detailList

            }) : null;
            TotalCount = db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Count();

            //OpBal = db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).FirstOrDefault().OpBalance??0;
            //ClBal = (db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance == "-" || string.IsNullOrWhiteSpace(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance)  ) ? 0 :  Convert.ToDecimal(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).LastOrDefault().Balance);

            DTResult<StatementDTO> finalresult = new DTResult<StatementDTO>
            {
                draw = param.Draw,
                data = (dataList != null && dataList.Count() > 0) ? dataList.ToList() : new List<StatementDTO>(),
                recordsFiltered = TotalCount,
                recordsTotal = filtered.Count,
                customData = new
                {
                    OpBal = Helper.DigitsWithComma(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Select(x => x.OpBalance).FirstOrDefault()),
                    ClBal = (db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault() == "-" || string.IsNullOrWhiteSpace(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault())) ? "0" : Helper.DigitsWithComma(Convert.ToDecimal(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault())),// Convert.ToDecimal(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Select(x => x.Balance).LastOrDefault()),
                    TotalDr = db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Debit) <=0 ? "0" :  Helper.DigitsWithComma(Convert.ToDecimal(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Sum(x=>x.Debit))),
                    TotalCr = db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Credit) <=0  ? "0" :  Helper.DigitsWithComma(Convert.ToDecimal(db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Sum(x => x.Credit)))
                }

            };

            //ViewBag.OpBal = (dataList != null && dataList.Count() > 0)?dataList.FirstOrDefault().OpeningBalance:"0";
            //ViewBag.ClBal = (dataList != null && dataList.Count() > 0)?dataList.LastOrDefault().Balance:"0";

            return Json(finalresult);

        }

        public List<StatementDTO> GetFilteredData(string search, string sortOrder, int start, int length, out int TotalCount, int? AccountID, DateTime? fromDate, DateTime? toDate)
        {
            //var result = db.tbl_Product.OrderBy(p => p.ProductID).Include(t => t.tbl_VehicleCode).Include(t => t.tbl_Brand).Where(p => (search == null || (p.PartNo != null && p.PartNo.ToLower().Contains(search.ToLower())
            //    || p.BarCode != null && p.BarCode.Contains(search)
            //    || p.tbl_VehicleCode.VehicleCode != null && p.tbl_VehicleCode.VehicleCode.ToLower().Contains(search.ToLower())
            //    || p.tbl_Brand.BrandTitle != null && p.tbl_Brand.BrandTitle.ToLower().Contains(search.ToLower())
            //    || p.Product_Code != null && p.Product_Code.ToLower().Contains(search.ToLower())
            //    || p.UnitCode != null && p.UnitCode.ToLower().Contains(search.ToLower())
            //    || p.SaleRate != null && p.SaleRate.ToString().Contains(search.ToString())
            //    ))).Skip(start).Take(length).ToList();
            //TotalCount = db.tbl_Product.Count();
            var result = db.GetCustomerPaymentDateWise(AccountID,fromDate,toDate, null).Where(p => (search == null || (p.VoucherName != null && p.VoucherName.ToLower().Contains(search.ToLower())
                || p.Description != null && p.Description.Contains(search)
                //|| p.Aging != null && p.Aging.ToLower().Contains(search.ToLower())
                //|| p.Balance != null && p.Balance.ToLower().Contains(search.ToLower())
                //|| p.Product_Code != null && p.Product_Code.ToLower().Contains(search.ToLower())
                //|| p.UnitCode != null && p.UnitCode.ToLower().Contains(search.ToLower())
                //|| p.SaleRate != null && p.SaleRate.ToString().Contains(search.ToString())
                ))).Skip(start).Take(length).ToList();
            TotalCount = db.GetCustomerPaymentDateWise(AccountID, fromDate, toDate, null).Count();
            List<StatementDTO> dataList = new List<StatementDTO>();
            int counter = 0;
            foreach (var i in result)
            {
                
                Models.DTO.StatementDTO data = new StatementDTO();

                data.Date = i.VoucherDate != null ? Convert.ToString(string.Format("{0:dd/MM/yyyy}", i.VoucherDate)) : "-";
                data.AccName = i.VoucherName ?? "-";
                data.Description = i.Description ?? "-";
                data.Detail = i.Detail ?? "-";
                data.PayStatus = i.PaymentStatus ?? "-";
                data.Aging = i.Aging != null ? i.Aging.ToString() : "";
                data.Dr = Helper.DigitsWithComma(i.Debit);// Convert.ToDecimal(i.Debit).ToString("#,##0.00");
                data.Cr = Helper.DigitsWithComma(i.Credit);// Convert.ToDecimal(i.Credit).ToString("#,##0.00");
                data.Balance = string.IsNullOrWhiteSpace(i.Balance) ? "-" : Helper.DigitsWithComma(Convert.ToDecimal(i.Balance));// Convert.ToDecimal(i.Balance).ToString("#,##0.00");
                data.OpeningBalance = i.OpBalance == null ? "-" : Helper.DigitsWithComma(i.OpBalance);// Convert.ToDecimal(i.OpBalance).ToString("#,##0.00");
                data.detailList = StatementBLL.GetItemDetailsForStatementJenTryIDWise(i.JEntryId) !=null ? StatementBLL.GetItemDetailsForStatementJenTryIDWise(i.JEntryId): new List<Details>();


                dataList.Add(data);
                counter = counter + 1;
            }
            //Mapper.CreateMap<tbl_Product, Models.DTO.ProductListCategory>();
            //var details = Mapper.Map<ICollection<tbl_Product>, ICollection<Models.DTO.ProductListCategory>>(result);
            return dataList;
        }

        // Get Receving Voucher receipt 
        public ActionResult Details(bool? isNew, int? id)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (isNew == true)
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JEntryID == id).OrderBy(j => j.JEntryID).Skip(1).Take(1).First();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_ReceiptVoucher", _PaymentDetail);
            }
            else
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JEntryID == id).OrderBy(j => j.JEntryID).Skip(1).Take(1).First();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_ReceiptVoucher", _PaymentDetail);
            }                    
        }

        // Get Receving Voucher receipt  small
        public ActionResult Voucher(bool? isNew, int? id)
        {
            ViewBag.CompanyName = db.tbl_Company.Select(v => v.Name).FirstOrDefault();
            ViewBag.CompanyAddress = db.tbl_Company.Select(v => v.Address).FirstOrDefault();

            ViewBag.CompanyLandline = db.tbl_Company.Select(v => v.Landline).FirstOrDefault();
            ViewBag.CompanyCell = db.tbl_Company.Select(v => v.Cell).FirstOrDefault();
            ViewBag.CompanyEmail = db.tbl_Company.Select(v => v.Email).FirstOrDefault();
            ViewBag.Picture = db.tbl_Company.Select(v => v.Picture).FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //tbl_PurchaseOrder purchaseOrder = db.tbl_PurchaseOrder.Find(id);
            if (isNew == true)
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JEntryID == id).OrderBy(j => j.JEntryID).Skip(1).Take(1).First();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_ReceiptVoucher", _PaymentDetail);
            }
            else
            {
                tbl_JDetail _PaymentDetail = db.tbl_JDetail.Where(j => j.JEntryID == id).OrderBy(j => j.JEntryID).Skip(1).Take(1).First();
                if (_PaymentDetail == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_ReceiptVoucher", _PaymentDetail);
            }
        }
        // Get Receiving Page
        public ActionResult Receiving()
        {
            try
            {
                int branchId = 0;
                
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                ViewBag.settleAmounts = db.tbl_JEntry.Where(x=>x.IsSettleEntry == true && (x.IsDeleted!=true || x.IsDeleted == null)).Select(c => new { Value = c.JEntryId, Name = c.Amount }).ToList();
                ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
                ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
                ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
                //ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code + "|" + c.Name }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View("CustomerPayment");
        }
        // Get Invoice Details of Customer 
        //public JsonResult getCustomerDetail(int accountId)
        //{
        //    if (accountId > 0)
        //    {
        //        int branchId = 0;                
        //        if (Session["BranchID"] != null)
        //        {
        //            branchId = Convert.ToInt32(Session["BranchID"]);
        //        }
        //        else
        //        {
        //            var user = User.Identity;
        //            string currentUserId = User.Identity.GetUserId();
        //            var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
        //            branchId = currentUser.BranchID;
        //        }
        //        try
        //        {
        //            var qry = db.tbl_SalesOrder.Where(p => p.AccountID == accountId && p.IsPaid == false && p.IsReturned == false && p.IsDeleted !=true)
        //                //&& p.BranchID == branchId)
        //                .Select(p => new
        //                {
        //                    p.OrderID,
        //                    p.SOID,
        //                    p.AccountID,
        //                    p.PaymentTypeID,               
        //                    p.tbl_AccountDetails.AccountName,
        //                    p.PONo,
        //                    p.SalesDate,
        //                    p.TotalAmount,
        //                    p.ReturnAmount,
        //                    p.VAT,
        //                    p.AmountPaid,
        //                    p.TotalPaid,
        //                    p.PaymentStatus
        //                }).ToList();
        //            return Json(new { qry }, JsonRequestBehavior.AllowGet);
        //        }
        //        catch (Exception ex)
        //        {
        //            return Json(ex.Message.ToString());
        //        }
        //    }
        //    return Json("");

        //}

        public JsonResult GetSettlementAmountIDWise(int AccountID, string type = "")
        {
            dynamic data = null;
            if(!string.IsNullOrWhiteSpace(type) && AccountID > 0)
            {
                switch (type)
                {
                    case "customer":
                        data = db.tbl_JEntry.Where(x => x.CustomerRefAccID == AccountID && x.IsSettleEntry == true && (x.IsDeleted!=true || x.IsDeleted == null)).Select(x => new { Value = x.JEntryId, Name = x.Amount }).ToList();
                        break;
                    case "vendor":
                        data = db.tbl_JEntry.Where(x => x.RefID == AccountID && x.IsSettleEntry == true && (x.IsDeleted != true || x.IsDeleted == null)).Select(x => new { Value = x.JEntryId, Name = x.Amount }).ToList();
                        break;
                    default:
                        break;
                }
            }
            return Json(data,JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveChallanPayment(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            if (ModelState.IsValid)
            {
                object result = customerPay.SaveChallanPayment(model, jentryLog, bankAccId, branchId);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Customer Payments Action".ToString());

                return Json(result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Customer Payments Action".ToString());

                return Json("formError");
            }
        }


        // Save Receivings of customer invoices
        public JsonResult SavePayment(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }            
            if (ModelState.IsValid)
            {
                object result = customerPay.Save(model,jentryLog,bankAccId,branchId);
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Customer Payments Action".ToString());

                return Json(result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Customer Payments Action".ToString());

                return Json("formError");
            }
        }
        [Authorize(Roles = "SuperAdmin")]
        // GET: Jentry/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            Models.DTO.PaymentLog jLog = new Models.DTO.PaymentLog();
            var query = from jlog in db.tbl_JEntryLog
                        join je in db.tbl_JEntry on  jlog.JEntryID equals je.JEntryId
                        join jd in db.tbl_JDetail on  je.JEntryId equals jd.JEntryID
                        join so in db.tbl_SalesOrder on jlog.OrderID equals so.OrderID 
                        join acd in db.tbl_Customer on  jd.AccountID equals acd.AccountID 
                        where jd.Cr > 0 && jlog.JEntryID == id && jlog.OrderID>0
                        select new Models.DTO.JEntryLogModel
                        { AccountName = acd.Name, InvoiceNo = so.SOID
                        , Amount = jlog.Amount ?? 0
                        };
            if (query == null)
            {
                return HttpNotFound();
            }
            return View(query.ToList());
        }
        [Authorize(Roles = "SuperAdmin")]
        // POST: Entry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                customerPay.DeleteEntry(id);
                return RedirectToAction("GetCustomerPaymentReportFilterWise");
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return View("GetCustomerPaymentReportFilterWise");
            }
        }



        public ActionResult SettleAmountWithInvoices(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();

            var JEntryD = db.tbl_JEntry.Where(x => x.JEntryId == id && x.IsSettleEntry == true).FirstOrDefault();
            ViewBag.CustomerID = JEntryD.CustomerRefAccID;
            ViewBag.Settlement = JEntryD.Amount - JEntryD.TotalBalanceSettled;
            ViewBag.JEntryID = id;
            ViewBag.customer = db.tbl_Customer.Where(x=>x.AccountID == JEntryD.CustomerRefAccID).Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Where(x => x.AccountID == JEntryD.CustomerRefAccID).Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Where(x => x.AccountID == JEntryD.CustomerRefAccID).Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();

            return View();
           
           

        }

        public JsonResult SettleAmountForInvoice(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId,int CustJEntryID)
        {
            int branchId = 0;

            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            object result = customerPay.SettleAmount(model, jentryLog, bankAccId, branchId, CustJEntryID);

            if (Convert.ToInt32(result) > 0)
            {

                return Json(result);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError");
            }

        }

        //Get: customer payreport filterwise
        public ActionResult GetCustomerPaymentReportFilterWise(bool? btn, DateTime? fromDate, DateTime? toDate, int? BranchID,int? AccountID)
        {
            ViewBag.AccountID = db.tbl_AccountDetails.Where(x=>x.AccountTypeID == 25).Select(c => new { Value = c.AccountID, Name = c.AccountName }).ToList();

            if (btn.HasValue)
            {
                List<GetCustomerPayReportFilterWise_Result> stock = db.GetCustomerPayReportFilterWise(AccountID,BranchID, fromDate, toDate).ToList();

                if (stock.Count == 0)
                {
                    return PartialView("_CustomerPayReport");
                }
                else
                {
                    return PartialView("_CustomerPayReport", stock);
                }

            }
            return View("CustomerPayReport");
        }

        ///Without invoices scenarios
        ///
          // Get Receiving Page Without Invoices
        public ActionResult CustomerReceiving()
        {
            try
            {
                int branchId = 0;

                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
                ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
                ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
                //ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code + "|" + c.Name }).ToList();
                ViewBag.payType = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View("GetPayment");
        }
        // Get Invoice Details of Customer 
        public JsonResult getCustomerDetail(int accountId)
        {
            if (accountId > 0)
            {
                int branchId = 0;
                if (Session["BranchID"] != null)
                {
                    branchId = Convert.ToInt32(Session["BranchID"]);
                }
                else
                {
                    var user = User.Identity;
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                    branchId = currentUser.BranchID;
                }
                try
                {
                    var qry = db.tbl_SalesOrder.Where(p => p.AccountID == accountId && p.IsPaid == false && p.IsReturned == false
                        && (p.IsDeleted == false || p.IsDeleted == null))
                        .Select(p => new
                        {
                            p.OrderID,
                            p.SOID,
                            PONo = p.PONo,
                            p.AccountID,
                            p.PaymentTypeID,
                            p.tbl_AccountDetails.AccountName,
                            p.SalesDate,
                            p.TotalAmount,
                            p.ReturnAmount,
                            p.VAT,
                            p.AmountPaid,
                            p.TotalPaid,
                            p.PaymentStatus,
                            p.Adjustment,
                            AdjustmentDescription = p.AdjustmentDescription ?? "noDesc",
                            p.VATPercent,
                            p.TaxPercent,
                            VATType = p.VATType ?? "noType",
                        }).ToList();
                    //var qry = db.tbl_Challan.Where(p => p.AccountID == accountId && p.IsPaid == false
                    //    && (p.IsDeleted == false || p.IsDeleted == null))
                    //    .Select(p => new
                    //    {
                    //        OrderID = p.ChallanID,
                    //        p.SOID,
                    //        p.AccountID,
                    //        p.PaymentTypeID,
                    //        p.tbl_AccountDetails.AccountName,
                    //        p.SalesDate,
                    //        p.TotalAmount,
                    //        ReturnAmount = 0,//p.ReturnAmount,
                    //        VAT = 0,//p.VAT,
                    //        p.AmountPaid,
                    //        p.TotalPaid,
                    //        p.PaymentStatus
                    //    }).ToList();
                    return Json(new { qry }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message.ToString());
                }
            }
            return Json("");

        }
        // Save Receivings of customer without invoices
        public JsonResult SaveReceiving(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId)
        {
            int branchId = 0;
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
            }
            else
            {
                var user = User.Identity;
                string currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
            }
            if (ModelState.IsValid)
            {
                object result = customerPay.SaveSinglePayment(model, jentryLog, bankAccId, branchId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json("formError", JsonRequestBehavior.AllowGet);
            }
        }

    }
}
