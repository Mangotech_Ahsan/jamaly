﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class StickerRibbonController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: StickerRibbon
        public ActionResult Index()
        {
            var tbl_StickerRibbon = db.tbl_StickerRibbon.Where(x=>x.IsDeleted!=true).Include(t => t.tbl_StickerRibbonType);
            return View(tbl_StickerRibbon.ToList());
        }

        // GET: StickerRibbon/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerRibbon tbl_StickerRibbon = db.tbl_StickerRibbon.Find(id);
            if (tbl_StickerRibbon == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerRibbon);
        }

        // GET: StickerRibbon/Create
        public ActionResult Create()
        {
            ViewBag.RibbonTypeID = new SelectList(db.tbl_StickerRibbonType, "ID", "RibbonType");
            return View();
        }

        // POST: StickerRibbon/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_StickerRibbon model)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            if (string.IsNullOrWhiteSpace(model.Ribbon))
                            {
                                ModelState.AddModelError("Ribbon", "Required");
                                return View();
                            }
                            if (model.RollCost <= 0)
                            {
                                ModelState.AddModelError("RollCost", "Required");
                                return View();
                            }
                            if (model.RollLengthInM <= 0)
                            {
                                ModelState.AddModelError("RollLengthInM", "Required");
                                return View();
                            }
                            if (model.RollWidthInMM <= 0)
                            {
                                ModelState.AddModelError("RollWidthInMM", "Required");
                                return View();
                            }
                            if (model.RibbonTypeID <= 0)
                            {
                                ModelState.AddModelError("RibbonTypeID", "Required");
                                return View();
                            }

                            if (model.RibbonTypeID == 3)
                            {
                                
                                model.AddedOn = Helper.PST();
                                db.tbl_StickerRibbon.Add(model);
                                db.SaveChanges();

                                tbl_Roll r = new tbl_Roll();
                                r.Roll = model.Ribbon;
                                r.RollCost = model.RollCost;
                                r.ForOffsetTypeID = 3;//Satin
                                r.RollWidth = model.RollWidthInMM;
                                r.RollMeters_Length = model.RollLengthInM;
                                r.AddedOn = Helper.PST();
                                r.SatinRibbonCost = model.Rate;
                                r.StickerRibbonID = model.ID;
                                db.tbl_Roll.Add(r);
                                db.SaveChanges();


                                tbl_Product p = new tbl_Product();
                                p.VehicleCodeID = 5;// Ribbon Category
                                p.PartNo = model.Ribbon;
                                p.isActive = 1;
                                p.IsGeneralItem = false;
                                p.IsOffsetItem = true;
                                p.MarkedupPrice = model.Rate;
                                p.RawProductID = model.ID;
                                p.RollID = r.ID;
                                p.AddOn = Helper.PST();
                                db.tbl_Product.Add(p);
                                db.SaveChanges();
                                t.Commit();

                                return RedirectToAction("Index");

                            }
                            else
                            {
                                model.AddedOn = Helper.PST();
                                db.tbl_StickerRibbon.Add(model);
                                db.SaveChanges();

                                tbl_Product p = new tbl_Product();
                                p.VehicleCodeID = 5;// Ribbon Category
                                p.PartNo = model.Ribbon;
                                p.isActive = 1;
                                p.IsGeneralItem = false;
                                p.IsOffsetItem = true;
                                p.MarkedupPrice = model.Rate;
                                p.RawProductID = model.ID;
                                p.AddOn = Helper.PST();
                                db.tbl_Product.Add(p);
                                db.SaveChanges();
                                t.Commit();

                                return RedirectToAction("Index");
                            }

                            
                        }

                        t.Rollback();
                        ViewBag.RibbonTypeID = new SelectList(db.tbl_StickerRibbonType, "ID", "RibbonType", model.RibbonTypeID);
                        return View(model);
                    }
                    catch (Exception d)
                    {
                        while (d.InnerException != null)
                        {
                            d = d.InnerException;
                        }
                        t.Rollback();
                        ModelState.AddModelError("Ribbon", d.Message);
                        ViewBag.RibbonTypeID = new SelectList(db.tbl_StickerRibbonType, "ID", "RibbonType", model.RibbonTypeID);
                        return View(model);
                    }

                }

            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                ModelState.AddModelError("Ribbon", e.Message);
                ViewBag.RibbonTypeID = new SelectList(db.tbl_StickerRibbonType, "ID", "RibbonType", model.RibbonTypeID);
                return View(model);
            }

        }

        // GET: StickerRibbon/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerRibbon tbl_StickerRibbon = db.tbl_StickerRibbon.Find(id);
            if (tbl_StickerRibbon == null)
            {
                return HttpNotFound();
            }
            ViewBag.RibbonTypeID = new SelectList(db.tbl_StickerRibbonType, "ID", "RibbonType", tbl_StickerRibbon.RibbonTypeID);
            return View(tbl_StickerRibbon);
        }

        // POST: StickerRibbon/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Ribbon,RibbonTypeID,RollLengthInM,RollWidthInMM,RollCost,Rate")] tbl_StickerRibbon model)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(model.Ribbon))
                {
                    ModelState.AddModelError("Ribbon", "Required");
                    return View();
                }
                if (model.RollCost <= 0)
                {
                    ModelState.AddModelError("RollCost", "Required");
                    return View();
                }
                if (model.RollLengthInM <= 0)
                {
                    ModelState.AddModelError("RollLengthInM", "Required");
                    return View();
                }
                if (model.RollWidthInMM <= 0)
                {
                    ModelState.AddModelError("RollWidthInMM", "Required");
                    return View();
                }
                if (model.RibbonTypeID <= 0)
                {
                    ModelState.AddModelError("RibbonTypeID", "Required");
                    return View();
                }

                if (model.RibbonTypeID == 3)
                {
                    var product = db.tbl_Product.Where(x => x.RawProductID == model.ID).FirstOrDefault();
                    if (product != null)
                    {
                        product.PartNo = model.Ribbon;
                        product.MarkedupPrice = model.Rate;
                        db.Entry(product).State = EntityState.Modified;
                        db.SaveChanges();

                        var roll = db.tbl_Roll.Where(x => x.StickerRibbonID == model.ID).FirstOrDefault();
                        if (roll != null)
                        {
                            roll.Roll = model.Ribbon;
                            roll.RollCost = model.RollCost;
                            roll.RollWidth = model.RollWidthInMM;
                            roll.RollMeters_Length = model.RollLengthInM;
                            roll.SatinRibbonCost = model.Rate;
                            db.Entry(roll).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RibbonTypeID = new SelectList(db.tbl_StickerRibbonType, "ID", "RibbonType", model.RibbonTypeID);
            return View(model);
        }

        // GET: StickerRibbon/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerRibbon tbl_StickerRibbon = db.tbl_StickerRibbon.Find(id);
            if (tbl_StickerRibbon == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerRibbon);
        }

        // POST: StickerRibbon/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_StickerRibbon model = db.tbl_StickerRibbon.Find(id);
            if (model != null)
            {
                model.IsDeleted = true;
                db.SaveChanges();
                if(model.RibbonTypeID == 3)
                {
                    var product = db.tbl_Product.Where(x => x.RawProductID == model.ID).FirstOrDefault();
                    if (product != null)
                    {
                        product.isActive = 0;
                        db.Entry(product).State = EntityState.Modified;
                        db.SaveChanges();

                        var roll = db.tbl_Roll.Where(x => x.StickerRibbonID == model.ID).FirstOrDefault();
                        if (roll != null)
                        {
                            roll.IsDeleted = true;
                            db.Entry(roll).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("Ribbon", "Record not found");
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
