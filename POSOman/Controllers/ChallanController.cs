﻿using Microsoft.AspNet.Identity;
using Microsoft.Reporting.WebForms;
using POSOman.Models;
using POSOman.Models.BLL;
using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class ChallanController : Controller
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        Common com = new Common();


        [HttpGet]
        public JsonResult ChallanDetail(int? id,int? orderID = null)
        {
            var data = db.tbl_ChallanDetails.Where(a => a.ChallanID == id).Select(a => new
            {
                AccountID = a.tbl_Challan.AccountID,
                SalePersonAccID = a.tbl_Challan.tbl_SalesOrder.SalePersonAccID,
                CommissionPercent = a.tbl_Challan.tbl_SalesOrder.tbl_AccountDetails1.tbl_SalePerson.Select(x => x.CommissionPercent).FirstOrDefault(),
                PaymentTerm = a.tbl_Challan.tbl_AccountDetails.tbl_Customer.Select(x => x.PaymentTerm).FirstOrDefault(),
                CustomerPO = a.tbl_Challan.tbl_SalesOrder.CustomerPO,
                CategoryID = a.tbl_Product.VehicleCodeID,
                CategoryName = a.tbl_Product.tbl_VehicleCode.VehicleCode,
                ProductID = a.ProductID,
                ProductName = a.tbl_Product.PartNo,
                UnitCode = a.UnitCode,
                UnitPrice = (orderID!=null && orderID>0) ? db.tbl_SaleDetails.Where(x=>x.OrderID == orderID && x.ProductID == a.ProductID).Select(x=>x.SalePrice).FirstOrDefault()??0: a.SalePrice,
                Qty = a.Qty,
                Total = (orderID != null && orderID > 0) ? db.tbl_SaleDetails.Where(x => x.OrderID == orderID && x.ProductID == a.ProductID).Select(x => x.Total).FirstOrDefault() ?? 0 : a.Total,
            }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getChallansCustomerWise(int? AccID)
        {
            using(var d = new dbPOS())
            {
                //var chIDs = await d.tbl_ChallanIDs.Where(x => x.isDeleted != true).Select(x => x.ChallanID).ToListAsync();
                //var data = await d.tbl_Challan.Where(a => a.AccountID == AccID && !chIDs.Contains(a.ChallanID) && (a.IsDeleted != true || a.IsDeleted == null)).Select(a => new
                //{
                //    Value = a.ChallanID,
                //    Name = "DC-" + a.SOID
                //}).Distinct().ToListAsync();

                var data = d.GetDeliveryChallanCustomerWise(AccID).Select(a => new
                {
                    Value = a.ChallanID,
                    Name = a.ChallanCode??"-"//"DC-" + a.SOID
                }).ToList();
                var InvoiceType = db.tbl_Customer.Where(x => x.AccountID == AccID).Select(x => x.InvoiceType).FirstOrDefault();
                var hsCode = db.tbl_SalesOrder.Where(x => x.AccountID == AccID).OrderByDescending(x=>x.OrderID).Select(x => x.HSCode).FirstOrDefault();
                var list = new
                {
                    Challans = data,
                    HSCode = hsCode,
                    InvoiceType = InvoiceType == 1 ? "Jamaly" : InvoiceType == 2 ? "Jemely" : "No-Type",
                };
                return Json(list, JsonRequestBehavior.AllowGet);
            }
           
        }


        private int GetCode()
        {
            int basecode = 1001;
            var data = db.tbl_Challan.OrderByDescending(a => a.SOID).FirstOrDefault();
            if (data != null)
            {
                return (data.SOID + 1);
            }

            return basecode;
        }

        private void GetViewBagValues()
        {
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Product = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.Desc = db.tbl_Product.Where(x => x.VehicleCodeID != 1).Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();
            ViewBag.Vat = db.tbl_TAX.Where(x => x.IsDeleted != true).Select(c => new { Value = c.AmountInPercentage, Name = c.TaxDescription + " | " + c.AmountInPercentage.ToString() + "%" }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Where(x => x.VehicleCodeID != 1).Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.Inquiry = db.tbl_Inquiry.Where(a => a.isActive == true).Select(a => new { Value = a.InquiryID, Name = a.InquiryNumber }).ToList();
            ViewBag.Quotation = db.tbl_InquiryQuotation.Where(a => a.isActive == true && a.isDelete != true).Select(a => new { Value = a.QuotationID, Name = a.StrCode }).ToList();
            ViewBag.SalesOrder = db.tbl_SalesOrder1.Select(a => new { Value = a.OrderID, Name = a.StrCode }).ToList();
            ViewBag.PaymentTypeID = db.tbl_PaymentTypes.Where(p => p.ID < 4).Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            ViewBag.Inquiry = db.tbl_Inquiry.Where(a => a.isActive == true).Select(a => new { Value = a.InquiryID, Name = a.InquiryNumber }).ToList();

        }

        // GET: Challan
        public ActionResult Index(bool? btn, int? AccountID, int? OID, DateTime? fromDate, DateTime? toDate, int? BranchID, int? ChallanStatusID, int? SPAccountID)
        {
            if (btn.HasValue)
            {
                List<GetDeliveryChallanList_Result> data = db.GetDeliveryChallanList(BranchID, AccountID, OID, fromDate, toDate, null, ChallanStatusID, SPAccountID).ToList();
                if (data.Count > 0)
                {
                    return PartialView("_Index", data.OrderByDescending(so => so.ChallanID));
                }

                return PartialView("_Index");
            }

            ViewBag.customer = db.tbl_Customer.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.SalePerson = db.tbl_SalePerson.Where(x => x.IsDeleted != true).Select(v => new { Value = v.AccountID, Name = v.SalePerson }).ToList();
            ViewBag.ChallanStatus = db.tbl_OrderStatus.Where(x => x.IsDeleted != true).Select(v => new { Value = v.StatusID, Name = v.StatusTitle }).ToList();
            ViewBag.Branch = db.tbl_Branch.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            return View();
        }


        public ActionResult DeletedChallans(bool? btn, int? AccountID, int? OID, DateTime? fromDate, DateTime? toDate, int? BranchID, int? ChallanStatusID, int? SPAccountID)
        {
            if (btn.HasValue)
            {
                List<GetDeletedDeliveryChallanList_Result> data = db.GetDeletedDeliveryChallanList(BranchID, AccountID, OID, fromDate, toDate, null, ChallanStatusID, SPAccountID).ToList();
                if (data.Count > 0)
                {
                    return PartialView("_DeletedChallans", data.OrderByDescending(so => so.ChallanID));
                }

                return PartialView("_DeletedChallans");
            }

            ViewBag.customer = db.tbl_Customer.Where(x=>x.IsDeleted!=true || x.IsDeleted == null).Select(v => new { Value = v.AccountID, Name = v.Name }).ToList();
            ViewBag.SalePerson = db.tbl_SalePerson.Where(x => x.IsDeleted != true ).Select(v => new { Value = v.AccountID, Name = v.SalePerson }).ToList();
            ViewBag.ChallanStatus = db.tbl_OrderStatus.Where(x => x.IsDeleted != true).Select(v => new { Value = v.StatusID, Name = v.StatusTitle }).ToList();
            ViewBag.Branch = db.tbl_Branch.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            return View();
        }



        public ActionResult Create()
        {
            GetViewBagValues();
            return View();
        }

        #region Old Logic
        //public JsonResult ReturnQtyForEditChallan(int OrderId, int ChallanId,int RowId, int ItemId, int deliveredQty, int returnedQty, string PkgId)
        //{
        //    try
        //    {
        //        int statusCode = -1;

        //        if(OrderId>0 && ChallanId>0 && RowId>0 && ItemId>0 && deliveredQty>0 && returnedQty>0 &&
        //            !string.IsNullOrWhiteSpace(PkgId))
        //        {
        //            using(var t = db.Database.BeginTransaction())
        //            {
        //                var displayPackage = db.tbl_DisplayChallanItemsDescription.Where(x => x.ChallanID == ChallanId &&
        //                x.OrderID == OrderId && x.ItemID == ItemId && x.PackageId.ToLower().Trim().Equals(PkgId.ToLower().Trim().ToString())).FirstOrDefault();

        //                var SODescription = db.tbl_SOItemWisePackageDetails.Where(x => x.OrderID == OrderId &&
        //                    x.ItemID == ItemId && x.PackageId.ToLower().Trim().Equals(PkgId.ToLower().Trim().ToString()) && x.ID == RowId /*&& x.ChallanID == ChallanId*/ && x.IsDeleted !=true).FirstOrDefault();

        //                var SODetails = db.tbl_SaleDetails.Where(x => x.OrderID == OrderId && x.ProductID == ItemId && x.IsDeleted != true).FirstOrDefault();
        //                var challan = db.tbl_Challan.Where(x => x.ChallanID == ChallanId && x.IsDeleted != true).FirstOrDefault();
        //                var sOrder = db.tbl_SalesOrder.Where(x => x.OrderID == OrderId && x.IsDeleted != true).FirstOrDefault();
        //                if (displayPackage != null && SODescription!=null && SODetails!=null && challan!=null && sOrder!=null)
        //                {

        //                    var previousData = db.tbl_SOItemWisePackageDetails.Where(x => x.OrderID == OrderId &&
        //                    x.ItemID == ItemId && x.PackageId.ToLower().Trim().Equals(PkgId.ToLower().Trim().ToString()) /*&& x.ChallanID == null*/ && x.IsDeleted != true).ToList();

        //                    if(previousData!=null && previousData.Count > 0)
        //                    {
        //                        db.tbl_SOItemWisePackageDetails.RemoveRange(previousData);
        //                        statusCode += db.SaveChanges();
        //                    }


        //                    displayPackage.Qty -= returnedQty;
        //                    displayPackage.ReturnedQty += returnedQty;
        //                    db.Entry(displayPackage).State = EntityState.Modified;

        //                    SODescription.DeliveredQty -= returnedQty;
        //                    SODescription.DeliverQty += returnedQty;
        //                    db.Entry(SODescription).State = EntityState.Modified;
        //                    statusCode += db.SaveChanges();

        //                    //////

        //                    tbl_SOItemWisePackageDetails addReturnedQtyForReuse = new tbl_SOItemWisePackageDetails();
        //                    //SODescription.ChallanID = null;
        //                    addReturnedQtyForReuse = SODescription;

        //                    db.tbl_SOItemWisePackageDetails.Add(addReturnedQtyForReuse);
        //                    statusCode += db.SaveChanges();
        //                    ///


        //                    SODetails.ReadyQty += returnedQty;
        //                    SODetails.QtyDelivered -= returnedQty;
        //                    SODetails.QtyBalanced += returnedQty;
        //                    SODetails.DeliveryStatusID = SODetails.DeliveryStatusID != 1 ? 1 : SODetails.DeliveryStatusID;
        //                    SODetails.isCompleted = false;
        //                    db.Entry(SODetails).State = EntityState.Modified;

        //                    if(challan.tbl_ChallanDetails!=null && challan.tbl_ChallanDetails.Count > 0)
        //                    {
        //                        decimal TotalAm = 0;
        //                        foreach(var i in challan.tbl_ChallanDetails)
        //                        {
        //                            if(i.ProductID == ItemId)
        //                            {
        //                                i.Qty -= returnedQty;
        //                                i.Total = i.Qty * i.SalePrice;
        //                                TotalAm += i.Total??0;
        //                                db.Entry(i).State = EntityState.Modified;
        //                            }
        //                        }

        //                        challan.ChallanStatusID = challan.tbl_ChallanDetails.Sum(x => x.Qty) <= 0 ? 3 : challan.ChallanStatusID; // 3  = Disposed
        //                        challan.TotalAmount = TotalAm>0?TotalAm:challan.TotalAmount ;
        //                        db.Entry(challan).State = EntityState.Modified;
        //                    }

        //                    sOrder.isCompleted = false;
        //                    db.Entry(sOrder).State = EntityState.Modified;

        //                    statusCode = db.SaveChanges();
        //                    t.Commit();
        //                }
        //            }
        //        }

        //        return Json(statusCode, JsonRequestBehavior.AllowGet);
        //    }
        //    catch(Exception e)
        //    {
        //        while (e.InnerException != null)
        //        {
        //            e = e.InnerException;
        //        }
        //        return Json(e.Message, JsonRequestBehavior.AllowGet);
        //    }

        //}
        #endregion

        public JsonResult ReturnQtyForEditChallan(int OrderId, int ChallanId, int RowId, int ItemId, int deliveredQty, int returnedQty, string PkgId)
        {
            try
            {
                int statusCode = -1;

                if (OrderId > 0 && ChallanId > 0 && RowId > 0 && ItemId > 0 && deliveredQty > 0 && returnedQty > 0 &&
                    !string.IsNullOrWhiteSpace(PkgId))
                {
                    using (var t = db.Database.BeginTransaction())
                    {
                        var displayPackage = db.tbl_DisplayChallanItemsDescription.Where(x => x.ChallanID == ChallanId &&
                        x.OrderID == OrderId && x.ItemID == ItemId && x.PackageId.ToLower().Trim().Equals(PkgId.ToLower().Trim().ToString())).FirstOrDefault();

                        var SODescription = db.tbl_SOItemWisePackageDetails.Where(x => x.OrderID == OrderId &&
                            x.ItemID == ItemId && x.PackageId.ToLower().Trim().Equals(PkgId.ToLower().Trim().ToString()) && x.ID == RowId /*&& x.ChallanID == ChallanId*/ && x.IsDeleted != true).FirstOrDefault();

                        var SODetails = db.tbl_SaleDetails.Where(x => x.OrderID == OrderId && x.ProductID == ItemId && x.IsDeleted != true).FirstOrDefault();
                        var challan = db.tbl_Challan.Where(x => x.ChallanID == ChallanId && x.IsDeleted != true).FirstOrDefault();
                        var sOrder = db.tbl_SalesOrder.Where(x => x.OrderID == OrderId && x.IsDeleted != true).FirstOrDefault();
                        if (displayPackage != null && SODescription != null && SODetails != null && challan != null && sOrder != null)
                        {

                            //var previousData = db.tbl_SOItemWisePackageDetails.Where(x => x.OrderID == OrderId &&
                            //x.ItemID == ItemId && x.PackageId.ToLower().Trim().Equals(PkgId.ToLower().Trim().ToString()) /*&& x.ChallanID == null*/ && x.IsDeleted != true).ToList();

                            //if (previousData != null && previousData.Count > 0)
                            //{
                            //    db.tbl_SOItemWisePackageDetails.RemoveRange(previousData);
                            //    statusCode += db.SaveChanges();
                            //}


                            displayPackage.Qty -= returnedQty;
                            displayPackage.ReturnedQty += returnedQty;
                            db.Entry(displayPackage).State = EntityState.Modified;

                            SODescription.DeliveredQty -= returnedQty;
                            SODescription.DeliverQty += returnedQty;

                            foreach(var d in SODescription.tbl_SOItemWisePackageChallanWiseDetails)
                            {
                                if(d.ChallanID == ChallanId)
                                {
                                    
                                    d.ReturnQty += returnedQty;
                                    if (d.ReturnQty == d.Qty)
                                    {
                                        d.IsReturned = true; // use if fully returned
                                    }
                                    d.Qty -= returnedQty;
                                    
                                    //d.IsReturned = true; 
                                    d.ReturnedOn = Helper.PST();
                                    db.Entry(d).State = EntityState.Modified;
                                }
                            }
                            db.Entry(SODescription).State = EntityState.Modified;
                            statusCode += db.SaveChanges();

                            //////

                            //tbl_SOItemWisePackageDetails addReturnedQtyForReuse = new tbl_SOItemWisePackageDetails();
                            ////SODescription.ChallanID = null;
                            //addReturnedQtyForReuse = SODescription;

                            //db.tbl_SOItemWisePackageDetails.Add(addReturnedQtyForReuse);
                            //statusCode += db.SaveChanges();
                            ///


                            SODetails.ReadyQty += returnedQty;
                            SODetails.QtyDelivered -= returnedQty;
                            SODetails.QtyBalanced += returnedQty;
                            SODetails.DeliveryStatusID = SODetails.DeliveryStatusID != 1 ? 1 : SODetails.DeliveryStatusID;
                            SODetails.isCompleted = false;
                            db.Entry(SODetails).State = EntityState.Modified;

                            if (challan.tbl_ChallanDetails != null && challan.tbl_ChallanDetails.Count > 0)
                            {
                                decimal TotalAm = 0;
                                foreach (var i in challan.tbl_ChallanDetails)
                                {
                                    if (i.ProductID == ItemId)
                                    {
                                        i.Qty -= returnedQty;
                                        i.Total = i.Qty * i.SalePrice;
                                        TotalAm += i.Total ?? 0;
                                        db.Entry(i).State = EntityState.Modified;
                                        statusCode += db.SaveChanges();

                                        
                                            var stock = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && (x.IsActive != false || x.IsActive == null)).FirstOrDefault();
                                            if (stock != null)
                                            {
                                                stock.Qty += i.Qty;
                                                db.Entry(stock).State = EntityState.Modified;
                                                statusCode += db.SaveChanges();
                                            }
                                        
                                    }
                                }

                                challan.ChallanStatusID = challan.tbl_ChallanDetails.Sum(x => x.Qty) <= 0 ? 3 : challan.ChallanStatusID; // 3  = Disposed
                                challan.TotalAmount = TotalAm > 0 ? TotalAm : challan.TotalAmount;
                                db.Entry(challan).State = EntityState.Modified;
                            }

                            sOrder.isCompleted = false;
                            db.Entry(sOrder).State = EntityState.Modified;

                            statusCode += db.SaveChanges();

                            
                            t.Commit();
                        }
                    }
                }

                return Json(statusCode, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }


        public JsonResult EditChallan(int ChallanID, string Driver, string Transport, string DriverVehicleNo,string custPONumber,string GrossWeight,string NetWeight,DateTime? ChallanDate)
        {
            int branchId = 0;
            int userID = 0;
            string currentUserId = "";
            if (Session["BranchID"] != null)
            {
                branchId = Convert.ToInt32(Session["BranchID"]);
                userID = Convert.ToInt32(Session["UserID"]);
            }
            else
            {
                var user = User.Identity;
                currentUserId = User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == currentUserId);
                branchId = currentUser.BranchID;
                userID = currentUser.UserId;
            }
            POSOman.Models.BLL.SalesOrder sales = new Models.BLL.SalesOrder();
           
            var status = sales.EditChallan(ChallanID, Driver, Transport, DriverVehicleNo, custPONumber,GrossWeight,NetWeight,ChallanDate);
            if (Convert.ToInt32(status) > 0)
            {
                var obj = new { JOID = db.tbl_Challan.Where(x=>x.ChallanID == ChallanID).Select(x=>x.JOID).FirstOrDefault(), ChallanID = Convert.ToInt32(ChallanID) };
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
            //else {
            //    var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
            //}
        }


        public JsonResult GetChallanDetailsForEdit(int OrderId, int ChallanID, string currentUserRole)
        {
            List<object> objectList = new List<object>();
            if (string.IsNullOrWhiteSpace(currentUserRole))
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            // int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            if (OrderId > 0 && ChallanID > 0)
            {
                var SaleOrderInv = db.tbl_Challan.Where(x => x.ChallanID == ChallanID && x.JOID == OrderId).FirstOrDefault();
                var CustomerPO = SaleOrderInv.CustomerPO;
                var Driver = SaleOrderInv.Driver;
                var Transport = SaleOrderInv.Transport;
                var VehicleNo = SaleOrderInv.DriverVehicleNo;
                var GrossWeight = SaleOrderInv.GrossWeight;
                var NetWeight = SaleOrderInv.NetWeight;
                var qry = db.tbl_Customer.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(p => new { Value = p.AccountID, Name = p.Name }).FirstOrDefault();
                var qry1 = db.tbl_SalesOrder.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(p => new { Value = p.BranchID, Name = p.tbl_Branch.BranchName }).FirstOrDefault();
                var qry2 = db.tbl_Customer.Where(p => p.AccountID == SaleOrderInv.AccountID).Select(x => x.CreditLimit).FirstOrDefault();
                var FinalAmount =  SaleOrderInv.TotalAmount ;
                var Discount = SaleOrderInv.DiscountAmount;
                //string SalesDate = Convert.ToDateTime(SaleOrderInv.SalesDate).ToShortDateString();
                var SalePersonAccID = SaleOrderInv.tbl_SalesOrder.SalePersonAccID ?? 0;
                var SubAmount = SaleOrderInv.TotalAmount + SaleOrderInv.DiscountAmount;
                var TotalAmount = SaleOrderInv.TotalAmount;
                var Tax = SaleOrderInv.tbl_SalesOrder.VAT;
                var CreditDays = SaleOrderInv.tbl_SalesOrder.CreditDays;
                var AmountPaid = SaleOrderInv.AmountPaid;
                var chequeDate = SaleOrderInv.ChequeDate;
                var salesDate = SaleOrderInv.SalesDate;
                var Paytype = db.tbl_PaymentTypes.Where(x => x.ID == SaleOrderInv.PaymentTypeID).Select(x => new { Value = x.ID, Name = x.Name }).FirstOrDefault() == null ? null : db.tbl_PaymentTypes.Where(x => x.ID == SaleOrderInv.PaymentTypeID).Select(x => new { Value = x.ID, Name = x.Name }).FirstOrDefault();
                var Cheque = SaleOrderInv.ChequeNo;
                var ChqDate = SaleOrderInv.ChequeDate;
                var ProductsList = db.tbl_ChallanDetails.Where(x => x.tbl_Challan.JOID == OrderId && x.ChallanID == ChallanID && (x.tbl_Challan.IsDeleted != true || x.tbl_Challan.IsDeleted == null)).
                    Select(p => new
                    {
                        
                        ProductID = p.ProductID,
                        Cat = p.tbl_Product.tbl_VehicleCode.VehicleCode,
                        CatName = p.tbl_Product.VehicleCodeID,
                        PartNo = p.tbl_Product.PartNo,
                        Desc = p.tbl_Product.Description,
                        UnitCode = p.UnitCode,
                        Qty = p.Qty,
                        ReadyQty = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x=>x.ProductID == p.ProductID && x.OrderID == OrderId).Select( x=>x.ReadyQty).FirstOrDefault(),
                        //DeliveredQty = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.QtyDelivered).FirstOrDefault(),
                        DeliveredQty = db.tbl_DisplayChallanItemsDescription.Where(x => x.ItemID == p.ProductID && x.OrderID == OrderId && x.ChallanID == ChallanID).Select(x => x.Qty).FirstOrDefault(),
                        ReturningQty = db.tbl_DisplayChallanItemsDescription.Where(x => x.ItemID == p.ProductID && x.OrderID == OrderId && x.ChallanID == ChallanID).Select(x => x.ReturnedQty).FirstOrDefault(),
                        Packet = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.Packet).FirstOrDefault(),
                        IsMinor = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.IsMinor).FirstOrDefault(),
                        IsPack = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.IsPack).FirstOrDefault(),
                        UnitID = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.UnitID).FirstOrDefault(),
                        LevelID = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.LevelID).FirstOrDefault(),
                        MinorDivisor = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.MinorDivisor).FirstOrDefault(),
                        UnitPerCarton = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.UnitPerCarton).FirstOrDefault(),
                        CardQuality = p.tbl_Product.CardQuality == null ? "-" : p.tbl_Product.tbl_Card_Quality.CardName,
                        SalePrice = p.SalePrice,
                        UnitPrice = p.UnitPrice ?? 0,
                        PTotal = p.Total,
                        MachineImpression = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.MachineImpression).FirstOrDefault(),
                        Impression = p.tbl_Product.NOOfImpressions * ((p.tbl_Product.TextColor * p.tbl_Product.TextCost) + (p.tbl_Product.GroundColor * p.tbl_Product.GroundCost)),
                        OrderMachineID = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.OrderMachineID).FirstOrDefault(),
                        OrderMachine = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.tbl_OrderMachine.OrderMachine).FirstOrDefault(),
                        StoreStatusID = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.StoreStatusID).FirstOrDefault(),
                        StoreStatus = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.tbl_StoreStatus.StoreStatus).FirstOrDefault(),
                        DeliveryStatusID = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.DeliveryStatusID).FirstOrDefault(),
                        DeliveryStatus = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.tbl_DeliveryStatus.DeliveryStatus).FirstOrDefault(),
                        NoOfSheets = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.NoOfSheets).FirstOrDefault(),
                        TotalSheets = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.TotalSheets).FirstOrDefault(),
                        Upping = p.tbl_Challan.tbl_SalesOrder.tbl_SaleDetails.Where(x => x.ProductID == p.ProductID && x.OrderID == OrderId).Select(x => x.tbl_Product.Upping).FirstOrDefault(),
                        JobSize = (p.tbl_Product.HSheetSize.ToString() + " X " + p.tbl_Product.WSheetSize.ToString()).ToString(),
                        GSM = p.tbl_Product.CardGSM,
                        RawSize = p.tbl_Product.RawSizeSheets,
                        SplitSheets = p.tbl_Product.Split,

                        PrintingMachine = p.tbl_Product.tbl_PrintingMachine.PrintingMachine ?? "-",
                        Cylinder = p.tbl_Product.tbl_Cylinder == null ? "-" : p.tbl_Product.tbl_Cylinder.Cylinder ?? "-",
                        Length = p.tbl_Product.Length,
                        Width = p.tbl_Product.Width,
                        CylinderLength = p.tbl_Product.tbl_Cylinder == null ? 0 : p.tbl_Product.tbl_Cylinder.CylinderLength,
                        AroundUps = p.tbl_Product.AroundUps,
                        NoOfPrintingColor = p.tbl_Product.NoOfPrintingColors,
                        Roll = p.tbl_Product.tbl_Roll == null ? "-" : p.tbl_Product.tbl_Roll.Roll ?? "-",
                        TotalPcInRoll = p.tbl_Product.TotalPcsInRoll,
                        WastagePcInRoll = p.tbl_Product.WastagePcsInRoll,
                        TotalReqRoll = p.tbl_Product.TotalRequiredRolls,
                        FinishingType = p.tbl_Product.tbl_FinishingType == null ? "-" : p.tbl_Product.tbl_FinishingType.FinishingType ?? "-",
                        BlockType = p.tbl_Product.tbl_BlockType == null ? "-" : p.tbl_Product.tbl_BlockType.BlockType ?? "-"





                    }).ToList();

                StringBuilder deliveryData = new StringBuilder();
                var renderDeliveryStatusDD = db.tbl_DeliveryStatus.Where(x => x.IsDeleted != true).Select(x => new { ID = x.ID, Value = x.DeliveryStatus }).ToList();

                StringBuilder storeData = new StringBuilder();
                var renderStoreStatusDD = db.tbl_StoreStatus.Where(x => x.IsDeleted != true).Select(x => new { ID = x.ID, Value = x.StoreStatus }).ToList();

                StringBuilder orderMachineData = new StringBuilder();
                var renderSlittingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 1).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderPrintingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 2).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderDieCuttingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 3).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderUVMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 4).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderLaminationMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 5).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderFoilMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 6).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();
                var renderEmbosingMDD = db.tbl_OrderMachine.Where(x => x.IsDeleted != true && x.MachineTypeID == 7).Select(x => new { ID = x.ID, Value = x.OrderMachine }).ToList();

                var Banks = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 18).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                int BankId = 0;
                foreach (var i in Banks)
                {
                    if (i.Name.Equals(SaleOrderInv.BankName))
                    {
                        BankId = i.Value;
                    }
                }
                var Bank = db.tbl_SalesOrder.Where(x => x.OrderID == OrderId).Select(p => new { Value = BankId, Name = p.BankName }).FirstOrDefault();
                var PayStatus = 0;

                if (SaleOrderInv.PaymentStatus == "UnPaid")
                {
                    PayStatus = 3;
                }
                else if (SaleOrderInv.PaymentStatus == "Paid")
                {
                    PayStatus = 1;
                }
                if (SaleOrderInv.PaymentStatus == "Partial Paid")
                {
                    PayStatus = 2;
                }



                objectList.Add(new
                {
                    Qry = qry,
                    Qry1 = qry1,
                    Qry2 = qry2,
                    CustomerPO = CustomerPO,
                    chequeDate = chequeDate,
                    SalePersonAccID = SalePersonAccID,
                    salesDate = salesDate,
                    ExpectedDeliveryDate = Convert.ToDateTime(SaleOrderInv.tbl_SalesOrder.ExpectedDeliveryDate).ToShortDateString(),
                    FinalAmount = FinalAmount,
                    Driver = Driver,
                    Transport = Transport,
                    VehicleNo = VehicleNo,
                    NetWeight = NetWeight,
                    GrossWeight = GrossWeight,
                    Discount = Discount,
                    SubAmount = SubAmount,
                    TotalAmount = TotalAmount,
                    CreditDays = CreditDays,
                    Tax = Tax,
                    AmountPaid = AmountPaid,
                    Paytype = Paytype,
                    PayStatus = PayStatus,
                    Cheque = Cheque,
                    Bank = Bank,
                    ProductsList = ProductsList,
                    deliveryDataDD = renderDeliveryStatusDD,
                    storeDataDD = renderStoreStatusDD,

                    slittingMachineDataDD = renderSlittingMDD,
                    printingMachineDataDD = renderPrintingMDD,
                    dieCuttingMachineDataDD = renderDieCuttingMDD,
                    uvMachineDataDD = renderUVMDD,
                    laminationMachineDataDD = renderLaminationMDD,
                    foilMachineDataDD = renderFoilMDD,
                    embosingMachineDataDD = renderEmbosingMDD,
                    userRole = currentUserRole

                });
            }
            return Json(objectList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int ChallanID, int JOID)
        {
            ViewBag.ChallanID = ChallanID;
            ViewBag.JOID = JOID;
            var user = User.Identity;
            string currentUserId = User.Identity.GetUserId();
            string userRoleId = db.AspNetUserRoles.Where(x => x.UserId.Equals(currentUserId)).Select(x => x.RoleId).FirstOrDefault();
            string userRole = "";

            if (!string.IsNullOrWhiteSpace(userRoleId))
            {
                userRole = db.AspNetRoles.Where(x => x.Id.Equals(userRoleId)).Select(x => x.Name).FirstOrDefault();

            }
            ViewBag.userRole = userRole.ToString();
            ViewBag.BranchId = db.tbl_Branch.Select(v => new { Value = v.BranchID, Name = v.BranchName }).ToList(); //branchId;

            ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
            ViewBag.customer = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Name }).ToList();
            ViewBag.customerCode = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Code }).ToList();
            ViewBag.customerPhone = db.tbl_Customer.Select(c => new { Value = c.AccountID, Name = c.Phone }).ToList();
            ViewBag.Artno = db.tbl_Product.Where(x => x.isActive == null || x.isActive == 1).Select(c => new { Value = c.ProductID, Name = c.PartNo }).ToList();
            ViewBag.Desc = db.tbl_Product.Select(c => new { Value = c.ProductID, Name = c.Description }).ToList();
            ViewBag.payType = db.tbl_PaymentTypes.Select(p => new { Value = p.ID, Name = p.Name }).ToList();
            //  ViewBag.product = db.tbl_Product.Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            //ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();

            ViewBag.department = db.tbl_Department.Where(x => x.IsDeleted != true).Select(v => new { Value = v.ID, Name = v.Department }).ToList();

            ViewBag.PaymentTypeID = new SelectList(db.tbl_PaymentTypes, "ID", "Name");
            return View();
        }

        public JsonResult SaveOrder(tbl_Challan model)
        {
            try
            {
                model.AddBy = com.GetUserID();
                model.AddOn = Helper.PST();
                model.IsActive = true;
                model.SOID = GetCode();
                model.Code = model.SOID;
                model.StrCode = "CH-" + model.SOID;
                db.tbl_Challan.Add(model);
                db.SaveChanges();
                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "Performed Delivery Challan".ToString());

                return Json(200);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.InnerException.Message);
                return Json(200);
            }

        }

        public FileResult Export(int id,int InvoiceType, int? JOID, String ReportType)
        {
            LocalReport lo = new LocalReport();
            if(InvoiceType == 1)
            {
                lo.ReportPath = Server.MapPath("~/Models/Reports/DeliveryChallanNewReport.rdlc");
            }
            else
            {
                lo.ReportPath = Server.MapPath("~/Models/Reports/DeliveryChallanNewReportJemely.rdlc");
            }
            //lo.ReportPath = Server.MapPath("~/Models/Reports/DeliveryChallan.rdlc");
            //string fscCert = string.Empty;// db.tbl_Certificates.Where(x => x.ID == 3).Select(x => x.CertificateInfo).FirstOrDefault();
            //ReportParameter FSC_Cert = new ReportParameter("FSC_Cert", fscCert);
            //lo.SetParameters(FSC_Cert);

            //string grsCert = string.Empty;// db.tbl_Certificates.Where(x => x.ID == 4).Select(x => x.CertificateInfo).FirstOrDefault();
            //ReportParameter GRS_Cert = new ReportParameter("GRS_Cert", grsCert);
            //lo.SetParameters(GRS_Cert);

            //string oeko_tex_Cert = string.Empty;// db.tbl_Certificates.Where(x => x.ID == 5).Select(x => x.CertificateInfo).FirstOrDefault();
            //ReportParameter OEKO_TEX_Cert = new ReportParameter("OEKO_TEX_Cert", oeko_tex_Cert);
            //lo.SetParameters(OEKO_TEX_Cert);

            ReportDataSource rs = new ReportDataSource();
            ReportDataSource packageRs = new ReportDataSource();
            rs.Name = "DeliveryChallanNewReport";
            //rs.Name = "DeliveryChallan";
            // rs.Name = "DeliveryChallan";
            // packageRs.Name = "PackageDetails";
            //rs.Value = db.GetDeliveryChallanPackageDetailsItemAndOrderIDWiseSP(JOID,id).Where(x=>x.DeliveredQty>0);
            rs.Value = db.GetDeliveryChallanPackageDetailsItemAndOrderIDWiseForDisplaySP(JOID, id).Where(x => x.DeliveredQty > 0).ToList();
            // rs.Value = db.DeliveryChallanNew(id);
            // packageRs.Value = db.GetPackageDetailsOrderIDWiseSP(JOID, null).Where(x=>x.DeliveredQty>0);
            lo.DataSources.Add(rs);
            //  lo.DataSources.Add(packageRs);


            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            //Response.AddHeader("Content-Disposition", "attachment; filename=SaleInvoice.pdf");

            return new FileContentResult(renderedBytes, mimeType);
        }

        public FileResult DeletedChallanExport(int id, int InvoiceType, int? JOID, String ReportType)
        {
            LocalReport lo = new LocalReport();
            if (InvoiceType == 1)
            {
                lo.ReportPath = Server.MapPath("~/Models/Reports/DeliveryChallanNewReport.rdlc");
            }
            else
            {
                lo.ReportPath = Server.MapPath("~/Models/Reports/DeliveryChallanNewReportJemely.rdlc");
            }
            //lo.ReportPath = Server.MapPath("~/Models/Reports/DeliveryChallan.rdlc");
            //string fscCert = string.Empty;// db.tbl_Certificates.Where(x => x.ID == 3).Select(x => x.CertificateInfo).FirstOrDefault();
            //ReportParameter FSC_Cert = new ReportParameter("FSC_Cert", fscCert);
            //lo.SetParameters(FSC_Cert);

            //string grsCert = string.Empty;// db.tbl_Certificates.Where(x => x.ID == 4).Select(x => x.CertificateInfo).FirstOrDefault();
            //ReportParameter GRS_Cert = new ReportParameter("GRS_Cert", grsCert);
            //lo.SetParameters(GRS_Cert);

            //string oeko_tex_Cert = string.Empty;// db.tbl_Certificates.Where(x => x.ID == 5).Select(x => x.CertificateInfo).FirstOrDefault();
            //ReportParameter OEKO_TEX_Cert = new ReportParameter("OEKO_TEX_Cert", oeko_tex_Cert);
            //lo.SetParameters(OEKO_TEX_Cert);

            ReportDataSource rs = new ReportDataSource();
            ReportDataSource packageRs = new ReportDataSource();
            rs.Name = "DeliveryChallanNewReport";
            //rs.Name = "DeliveryChallan";
            // rs.Name = "DeliveryChallan";
            // packageRs.Name = "PackageDetails";
            //rs.Value = db.GetDeliveryChallanPackageDetailsItemAndOrderIDWiseSP(JOID,id).Where(x=>x.DeliveredQty>0);
            rs.Value = db.GetDeletedDeliveryChallanPackageDetailsItemAndOrderIDWiseForDisplaySP(JOID, id).Where(x => x.DeliveredQty > 0).ToList();
            // rs.Value = db.DeliveryChallanNew(id);
            // packageRs.Value = db.GetPackageDetailsOrderIDWiseSP(JOID, null).Where(x=>x.DeliveredQty>0);
            lo.DataSources.Add(rs);
            //  lo.DataSources.Add(packageRs);


            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            //Response.AddHeader("Content-Disposition", "attachment; filename=SaleInvoice.pdf");

            return new FileContentResult(renderedBytes, mimeType);
        }

        [Authorize(Roles = "SuperAdmin,Admin,Accountant")]
        public ActionResult ChallanDelete(int id, int JOID)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Challan model = db.tbl_Challan.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);


        }

        [HttpPost, ActionName("ChallanDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int JOID)
        {
            try
            {
                bool exist = db.tbl_ChallanIDs.Any(x => x.ChallanID == id && x.isDeleted != true);
                tbl_Challan model = db.tbl_Challan.Find(id);
                //bool isExist = model.An(p => p.ChallanID == id && p.JOID == JOID);
                if (exist)
                {
                    ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted! Invoice already created");
                    return View(model);
                }

                DeleteOrders deleteChallan = new DeleteOrders();
                var data = deleteChallan.DeleteChallan(id, JOID);
                if (data == 1)
                {
                    UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), $"Deleted Challan #: {model.StrCode.ToString()}".ToString());
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, "Some error occured");
                return View(model);

            }
            catch (Exception err)
            {
                while (err.InnerException != null)
                {
                    err = err.InnerException;
                }
                ModelState.AddModelError(String.Empty, err.Message);
                tbl_Challan model = db.tbl_Challan.Find(id);
                return View(model);
            }
        }
    }
}