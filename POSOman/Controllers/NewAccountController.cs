﻿using Microsoft.AspNet.Identity;
using POSOman.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using POSOman.Models.BLL;
using AutoMapper;
using POSOman.Models.DTO;

namespace POSOman.Controllers
{
    public class NewAccountController : Controller
    {
        dbPOS db = new dbPOS();
        Common com = new Common();

        [HttpGet]
        public JsonResult HeadAccounts(int AccTypeID)
        {
            var data = db.tbl_AccountType.Where(a => a.MainHeadID == AccTypeID).Select(p => new { Value = p.AccountTypeID, Name = p.TypeName }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DetailedAccounts(int? HeadAccID)
        {
            var data = db.tbl_AccountDetails.Where(a => a.AccountTypeID == HeadAccID).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Accounts
        public ActionResult Index()
        {
            List<GetAccountList_Result> data = db.GetAccountList().ToList();
            return View(data);
        }

        // GET: Accounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Product tbl_Product = db.tbl_Product.Find(id);
            if (tbl_Product == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Product);
        }

        private void getCreateData()
        {
            ViewBag.BranchId = db.tbl_Branch.Select(v => new { Value = v.BranchID, Name = v.BranchName }).ToList();
            List<int> AccountTypeIDs = new List<int> { 3, 4, 5, 6, 7, 12, 13 };
            ViewBag.AccountTypeID = new SelectList(db.tbl_AccountType.Where(x => x.HeadID == null), "AccountTypeID", "TypeName");
        }

        // GET: Product/Create
        public ActionResult CreateSubAcc()
        {
            getCreateData();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_AccountDetails modelAccount)
        {
            modelAccount.OpeningDate = DateTime.Now.Date;
            modelAccount.UserID = User.Identity.GetUserId();
            modelAccount.AddOn = Helper.PST();
            modelAccount.IsActive = true;

            if (ModelState.IsValid)
            {
                if (modelAccount.CategoryType == 1)
                {

                    if (db.tbl_AccountType.Any(act => act.TypeName == modelAccount.AccountName))
                    {
                        ModelState.AddModelError("AccountName", "Account Already Exists!");
                        getCreateData();
                        return View("CreateSubAcc");
                    }
                    else
                    {
                        var AccCode = db.tbl_AccountType.Where(a => a.AccountTypeID == modelAccount.HeadID).Select(a => new { a.ATCode, a.Level }).FirstOrDefault();
                        var code = db.tbl_AccountType.Where(a => a.HeadID == modelAccount.HeadID).OrderByDescending(a => a.AccountTypeID).Select(a => a.Code).FirstOrDefault();
                        tbl_AccountType acctype = new tbl_AccountType();
                        acctype.TypeName = modelAccount.AccountName;
                        acctype.HeadID = modelAccount.HeadID;
                        acctype.Code = (code ?? 0) + 1;
                        acctype.Level = AccCode.Level + 1;
                        acctype.ATCode = AccCode.ATCode + "-" + acctype.Code;
                        acctype.AddOn = Helper.PST();
                        acctype.Addby = 4001;
                        acctype.IsActive = true;
                        acctype.MainHeadID = modelAccount.AccountTypeID;
                        db.tbl_AccountType.Add(acctype);
                        db.SaveChanges();

                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    if (db.tbl_AccountDetails.Any(act => act.AccountTypeID == modelAccount.AccountTypeID && act.AccountName == modelAccount.AccountName))
                    {
                        ModelState.AddModelError("AccountName", "Account Already Exists!");
                        getCreateData();
                        return View("CreateSubAcc");
                    }
                    else
                    {
                        int maxCode = 0;

                        var accType = db.tbl_AccountType.Where(a => a.AccountTypeID == modelAccount.HeadID).Select(a => new { a.ATCode, a.Level }).FirstOrDefault();
                        maxCode = db.tbl_AccountDetails.Where(a => a.AccountTypeID == modelAccount.HeadID).OrderByDescending(a => a.AccountID).Select(a => a.Code).FirstOrDefault() ?? 0;
                        modelAccount.Levels = (accType.Level ?? 0) + 1;
                        modelAccount.Code = maxCode + 1;
                        int HeadID = modelAccount.AccountTypeID;
                        modelAccount.AccountTypeID = modelAccount.HeadID ?? 0;
                        if (modelAccount.HeadID == 27 || modelAccount.HeadID == 15)
                        {
                            HeadID = 7;
                        }
                        modelAccount.HeadID = HeadID;
                        //modelAccount.HeadID = modelAccount.AccountTypeID;
                        modelAccount.AccountCode = accType.ATCode + "-" + modelAccount.Code;
                        modelAccount.isCustom = true;
                        db.tbl_AccountDetails.Add(modelAccount);
                        db.SaveChanges();

                        return RedirectToAction("Index");
                    }
                }

            }

            return View(modelAccount);
        }


        // GET: Product/Edit/5
        public ActionResult Edit(int? id, int? CategoryType)
        {
            tbl_AccountDetails modelAccount = new tbl_AccountDetails();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            tbl_AccountDetails tbl_AccountDetails = new tbl_AccountDetails();
            if (CategoryType == 1)
            {
                var data = db.tbl_AccountType.Where(a => a.AccountTypeID == id).Select(a => new { AccountID = a.AccountTypeID, AccountName = a.TypeName, HeadID = a.HeadID, AccountTypeID = a.MainHeadID, CategoryType }).FirstOrDefault();
                tbl_AccountDetails.CategoryType = data.CategoryType;
                tbl_AccountDetails.AccountTypeID = data.AccountTypeID ?? 0;
                tbl_AccountDetails.HeadID = data.HeadID;
                tbl_AccountDetails.AccountID = data.AccountID;
                tbl_AccountDetails.AccountName = data.AccountName;
            }
            else
            {
                var data = db.tbl_AccountDetails.Where(a => a.AccountID == id).Select(a => new { AccountID = a.AccountID, AccountName = a.AccountName, HeadID = a.AccountTypeID, AccountTypeID = a.HeadID, CategoryType }).FirstOrDefault();
                tbl_AccountDetails.CategoryType = data.CategoryType;
                tbl_AccountDetails.AccountTypeID = data.AccountTypeID ?? 0;
                tbl_AccountDetails.HeadID = data.HeadID;
                tbl_AccountDetails.AccountID = data.AccountID;
                tbl_AccountDetails.AccountName = data.AccountName;
            }

            if (tbl_AccountDetails == null)
            {
                return HttpNotFound();
            }

            return View(tbl_AccountDetails);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_AccountDetails modelAccount)
        {
            modelAccount.UpdateOn = DateTime.UtcNow.AddHours(5);
            modelAccount.UpdateBy = com.GetUserID();

            if (modelAccount.CategoryType == 1)
            {
                if (db.tbl_AccountType.Any(act => act.TypeName == modelAccount.AccountName && act.AccountTypeID != modelAccount.AccountID))
                {
                    ModelState.AddModelError("AccountName", "Account Already Exists!");
                    getCreateData();
                    return View("Edit");
                }
                else
                {
                    var data = db.tbl_AccountType.Where(a => a.AccountTypeID == modelAccount.AccountID).FirstOrDefault();
                    data.TypeName = modelAccount.AccountName;
                    data.HeadID = modelAccount.HeadID;
                    data.MainHeadID = modelAccount.AccountTypeID;

                    db.Entry(data).State = EntityState.Modified;
                }

            }
            else
            {
                if (db.tbl_AccountDetails.Any(act => act.AccountName == modelAccount.AccountName && act.AccountID != modelAccount.AccountID))
                {
                    ModelState.AddModelError("AccountName", "Account Already Exists!");
                    getCreateData();
                    return View("Edit");
                }
                else
                {
                    var data = db.tbl_AccountDetails.Where(a => a.AccountID == modelAccount.AccountID).FirstOrDefault();
                    data.AccountName = modelAccount.AccountName;
                    int HeadID = modelAccount.AccountTypeID;
                    modelAccount.AccountTypeID = modelAccount.HeadID ?? 0;
                    data.AccountTypeID = modelAccount.AccountTypeID;
                    if (modelAccount.HeadID == 27 || modelAccount.HeadID == 15)
                    {
                        HeadID = 7;
                    }
                    modelAccount.HeadID = HeadID;
                    data.HeadID = modelAccount.HeadID;

                    db.Entry(data).State = EntityState.Modified;
                }
            }

            db.SaveChanges();
            return RedirectToAction("Index");

        }

        // GET: Product/Delete/5
        public ActionResult Delete(int? id, int? CategoryType)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            tbl_AccountDetails tbl_AccountDetails = new tbl_AccountDetails();
            if (CategoryType == 1)
            {
                var data = db.tbl_AccountType.Where(a => a.AccountTypeID == id).Select(a => new { AccountID = a.AccountTypeID, AccountName = a.TypeName, HeadID = a.HeadID, AccountTypeID = a.MainHeadID, CategoryType }).FirstOrDefault();
                tbl_AccountDetails.CategoryType = data.CategoryType;
                tbl_AccountDetails.AccountTypeID = data.AccountTypeID ?? 0;
                tbl_AccountDetails.HeadID = data.HeadID;
                tbl_AccountDetails.AccountID = data.AccountID;
                tbl_AccountDetails.AccountName = data.AccountName;
            }
            else
            {
                var data = db.tbl_AccountDetails.Where(a => a.AccountID == id).Select(a => new { AccountID = a.AccountID, AccountName = a.AccountName, HeadID = a.AccountTypeID, AccountTypeID = a.HeadID, CategoryType }).FirstOrDefault();
                tbl_AccountDetails.CategoryType = data.CategoryType;
                tbl_AccountDetails.AccountTypeID = data.AccountTypeID ?? 0;
                tbl_AccountDetails.HeadID = data.HeadID;
                tbl_AccountDetails.AccountID = data.AccountID;
                tbl_AccountDetails.AccountName = data.AccountName;
            }


            if (tbl_AccountDetails == null)
            {
                return HttpNotFound();
            }

            return View(tbl_AccountDetails);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int? CategoryType)
        {
            try
            {
                if (CategoryType == 1)
                {
                    if (db.tbl_AccountDetails.Any(act => act.AccountTypeID == id))
                    {
                        ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                        return View("Delete");
                    }
                    else
                    {
                        var data = db.tbl_AccountType.Where(a => a.AccountTypeID == id).FirstOrDefault();
                        db.tbl_AccountType.Remove(data);
                    }
                }
                else
                {
                    if (db.tbl_JDetail.Any(jd => jd.AccountID == id))
                    {
                        ModelState.AddModelError(string.Empty, "Record Cannot Be Deleted!");
                        return View("Delete");
                    }
                    else
                    {
                        var data = db.tbl_AccountDetails.Where(a => a.AccountID == id).FirstOrDefault();
                        db.tbl_AccountDetails.Remove(data);
                    }
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception err)
            {
                ModelState.AddModelError(String.Empty, err.Message);
                return RedirectToAction("Delete");
            }
        }

        //not used
        public ActionResult ChartOfAccount_Old(bool? btn, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            int branchId = com.GetBranchID();

            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            if (btn.HasValue)
            {
                ViewBag.ToDate = toDate;
                ViewBag.BankAccount = db.tbl_AccountDetails.Where(acd => acd.AccountTypeID == 27).Select(p => new { Value = p.AccountID, Name = p.AccountName + " | " + p.Bank }).ToList();
                List<GetChartOfAccounts_Result> COA = db.GetChartOfAccounts(fromDate, toDate, BranchID).ToList();
                if (COA == null)
                {
                    return PartialView("_ChartOfAccountNew");
                }
                else
                    return PartialView("_ChartOfAccountNew", COA);
            }

            return View("ChartOfAccount");
        }

        public ActionResult ChartOfAccount(bool? isSearch, DateTime? fromDate, DateTime? toDate, int? BranchID)
        {
            int branchId = com.GetBranchID();

            if (isSearch == true)
            {


                List<Models.DTO.BalanceSheetFormat> COAFormat = new List<Models.DTO.BalanceSheetFormat>();


                return PartialView("_COA_New", COAFormat);


            }
            return View("ChartOfAccount_New");
        }



        public List<BalanceSheetFormat> RecursiveCallToGenerateBalanceSheet()
        {
            try
            {
                List<BalanceSheetFormat> masterList = null;
                List<int> BalanceSheetHeadIDs = new List<int>() { 1, 2,3, 4,5 };
                var heads = db.tbl_AccountType.Where(x => x.HeadID == null && BalanceSheetHeadIDs.Contains(x.AccountTypeID))/*.OrderByDescending(x=>x.AccountTypeID)*/.ToList();
                if (heads != null && heads.Count > 0)
                {
                    masterList = new List<BalanceSheetFormat>();
                    foreach (var h in heads)
                    {
                        BalanceSheetFormat head = new BalanceSheetFormat();
                        head.name = h.TypeName;
                        head.number = h.AccountTypeID.ToString();// + "--" + h.Code.ToString();
                        head.assets = RecursionOnHeads(h.AccountTypeID);
                        head.balance = head.assets != null ? head.assets.Sum(x => Convert.ToDecimal(x.balance)).ToString() : "0";//db.GetAccountBalances(null, null, null, h.AccountTypeID, null).Sum(x => x.Balance).ToString();

                        masterList.Add(head);
                    }


                }
                return masterList;

            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public List<Asset> RecursionOnHeads(int ID)
        {
            try
            {
                using (var d = new dbPOS())
                {
                    List<Asset> format = new List<Asset>();

                    if (ID <= 0)
                    {
                        return format;
                    }


                    var data = d.tbl_AccountType.Where(x => x.HeadID == ID).ToList();

                    if (data.Count > 0)
                    {
                        foreach (var i in data)
                        {
                            Asset asset = new Asset();
                            asset.Id = i.AccountTypeID;
                            asset.name = i.TypeName;
                            asset.number = i.AccountTypeID.ToString();// + "--" + i.Code.ToString();
                            //asset.assets = RecursionOnHeads(i.AccountTypeID);

                            if (RecursionOnHeads(i.AccountTypeID).Count > 0)
                            {
                                asset.assets = RecursionOnHeads(i.AccountTypeID);
                            }
                            else
                            {
                                var accDet = d.tbl_AccountDetails.Where(x => x.AccountTypeID == i.AccountTypeID).Select(x => new { Name = x.AccountName, ID = x.AccountID }).ToList();
                                if (accDet != null && accDet.Count > 0)
                                {
                                    asset.assets = new List<Asset>();
                                    foreach (var acc in accDet)
                                    {
                                        asset.assets.Add(new Asset
                                        {
                                            number = acc.ID.ToString(),
                                            name = acc.Name,
                                            balance = d.GetAccountBalancesWithoutRefAccountIDs(null, null, null, null, acc.ID).Sum(x => x.Balance).ToString()
                                        });
                                    }
                                }
                            }
                            //asset.balance = d.GetAccountBalances(null, null, null, i.AccountTypeID, null).Sum(x => x.Balance).ToString();
                            asset.balance = asset.assets != null ? asset.assets.Sum(x => Convert.ToDecimal(x.balance)).ToString() : "0";

                            format.Add(asset);
                        }
                    }


                    return format;
                }

            }
            catch
            {
                return null;
            }
        }

        public JsonResult COA_Data()
        {

            List<Models.DTO.BalanceSheetFormat> balSheetFormat = new List<Models.DTO.BalanceSheetFormat>();

            balSheetFormat = RecursiveCallToGenerateBalanceSheet();


            return Json(balSheetFormat, JsonRequestBehavior.AllowGet);
            ///
        }


        public ActionResult AccountLedger(bool? btn, int? BranchID, int? AccTypeID, int? AccID, DateTime? fromDate, DateTime? toDate)
        {
            if (btn.HasValue)
            {
                List<GetAccountsLedger_Result> data = db.GetAccountsLedger(AccID, fromDate, toDate, BranchID).ToList();

                if (data != null)
                {
                    return PartialView("_AccountLedger", data);
                }

                return PartialView("_AccountLedger");
            }

            ViewBag.AccType = AccTypeID;
            ViewBag.Acc = AccID;
            ViewBag.Branch = db.tbl_Branch.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.AccountTypeID = db.tbl_AccountType.Where(x=>x.IsDeleted!=true || x.IsDeleted == null).Select(b => new { Value = b.AccountTypeID, Name = b.TypeName }).ToList();
            ViewBag.Account = db.tbl_AccountDetails.Where(x => x.IsDeleted != true || x.IsDeleted == null).Select(b => new { Value = b.AccountID, Name = b.AccountName }).ToList();

            return View();
        }

        public ActionResult AccountBalances(bool? btn, int? BranchID, int? AccTypeID, int? AccID, DateTime? fromDate, DateTime? toDate)
        {
            if (btn.HasValue)
            {
                List<GetAccountBalances_Result> data = db.GetAccountBalances(fromDate, toDate, BranchID, AccTypeID, AccID).ToList();

                if (data.Count > 0)
                {
                    return PartialView("_AccountBalances", data);
                }

                return PartialView("_AccountBalances");
            }

            ViewBag.AccType = AccTypeID;
            ViewBag.Acc = AccID;
            ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
            ViewBag.AccountTypeID = db.tbl_AccountType.Select(b => new { Value = b.AccountTypeID, Name = b.TypeName }).ToList();
            ViewBag.Account = db.tbl_AccountDetails.Select(b => new { Value = b.AccountID, Name = b.AccountName }).ToList();

            return View();
        }

        public ActionResult GetAccountsDetails(int id)
        {
            if (id > 0)
            {
                List<GetAccountIDTrialBalanceDetails_Result> Details = db.GetAccountIDTrialBalanceDetails(id, null, null, null).ToList();
                return View(Details);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult GetAccountID(int? id)
        {
            if (id != null)
            {
                var Jdetails = db.tbl_JDetail.Where(x => x.JEntryID == id).ToList();
                return View(Jdetails);

            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // Get Accounts by TypeId
        public JsonResult getAccounts(int accountTypeId)
        {
            if (accountTypeId > 0)
            {
                var qry = db.tbl_AccountDetails.Where(p => p.AccountTypeID == accountTypeId /*&& p.AccountID > 24*/).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
                return Json(qry);
            }
            else if(accountTypeId == -2) // populate for gj page
            {
                var qry = db.tbl_AccountDetails.Where(acd => acd.AccountID == 1 || acd.AccountID > 33).Select(p => new { Value = p.AccountID, Name = p.tbl_AccountType.TypeName + "-|-" + p.AccountName }).ToList();
                return Json(qry);
            }
            else
            {
                var qry = db.tbl_AccountDetails/*.Where(acd => acd.AccountID > 24)*/.Where(x=>x.IsDeleted!=true || x.IsDeleted == null).Select(p => new { Value = p.AccountID, Name = p.AccountName }).ToList();
                return Json(qry);
            }
        }
    }
}