﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class FinishingTypeController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: FinishingType
        public ActionResult Index()
        {
            return View(db.tbl_FinishingType.Where(x=>x.IsDeleted!=true).ToList());
        }

        // GET: FinishingType/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_FinishingType tbl_FinishingType = db.tbl_FinishingType.Find(id);
            if (tbl_FinishingType == null)
            {
                return HttpNotFound();
            }
            return View(tbl_FinishingType);
        }

        // GET: FinishingType/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FinishingType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_FinishingType tbl_FinishingType)
        {
            if (ModelState.IsValid)
            {
                tbl_FinishingType.AddedOn = Helper.PST();
                db.tbl_FinishingType.Add(tbl_FinishingType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_FinishingType);
        }

        // GET: FinishingType/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_FinishingType tbl_FinishingType = db.tbl_FinishingType.Find(id);
            if (tbl_FinishingType == null)
            {
                return HttpNotFound();
            }
            return View(tbl_FinishingType);
        }

        // POST: FinishingType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_FinishingType tbl_FinishingType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_FinishingType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_FinishingType);
        }

        // GET: FinishingType/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_FinishingType tbl_FinishingType = db.tbl_FinishingType.Find(id);
            if (tbl_FinishingType == null)
            {
                return HttpNotFound();
            }
            return View(tbl_FinishingType);
        }

        // POST: FinishingType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_FinishingType model = db.tbl_FinishingType.Find(id);
            if (model != null)
            {
                model.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("FinishingType", "Record not found");
            return View(model);
           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
