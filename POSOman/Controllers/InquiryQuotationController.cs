﻿using POSOman.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models.BLL;
using Microsoft.AspNet.Identity;
using Microsoft.Reporting.WebForms;

namespace POSOman.Controllers
{
    public class InquiryQuotationController : Controller
    {
        dbPOS db = new dbPOS();
        Common com = new Common();

        [HttpGet]
        public JsonResult QuotationDetail(int? id)
        {
            var data = db.tbl_InquiryQuotationDetails.Where(a => a.QuotationID == id).Select(a => new {
                InquiryID = a.tbl_InquiryQuotation.InquiryID,
                AccountID = a.tbl_InquiryQuotation.tbl_Inquiry.tbl_Customer.AccountID,
                CategoryID = a.tbl_Product.VehicleCodeID,
                CategoryName = a.tbl_Product.tbl_VehicleCode.VehicleCode,
                ProductID = a.ProductID,
                ProductName = a.tbl_Product.PartNo,
                UnitCode = a.Unit,
                UnitPrice = a.UnitPrice,
                Qty = a.Qty,
                Total = a.Total,
            }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        // GET: InquiryQuotation
        public ActionResult Index()
        {
            var data = db.tbl_InquiryQuotation.Where(a => a.isActive == true && a.isDelete == false).OrderByDescending(a => a.QuotationID).ToList();
            return View(data);
        }

        private int GetCode()
        {
            int baseCode = 2020;
            var data = db.tbl_InquiryQuotation.OrderByDescending(a => a.QuotationID).Select(a=>a.Code).FirstOrDefault();
            //if(data == null)
            //{
            //    baseCode += 1;
            //}
            //else
            //{
            //    baseCode = data + 1;
            //}

            return (data + 1);
        }

        private void GetViewBagData(int? id)
        {
            ViewBag.ProductType = db.tbl_ProductType.Select(v => new { Value = v.ProductTypeID, Name = v.TypeName }).ToList();
            ViewBag.Product = db.tbl_Product.Where(x => x.VehicleCodeID != 1).Select(v => new { Value = v.ProductID, Name = v.PartNo }).ToList();
            ViewBag.VehCode = db.tbl_VehicleCode.Where(x => x.VehicleCodeID != 1).Select(v => new { Value = v.VehicleCodeID, Name = v.VehicleCode }).ToList();
            ViewBag.BranchID = new SelectList(db.tbl_Branch, "BranchID", "BranchName", 9001);
            ViewBag.InquiryList = db.tbl_Inquiry.Where(a=>a.isActive == true).Select(v => new { Value = v.InquiryID, Name = v.InquiryNumber }).ToList();
            ViewBag.Code = GetCode();
            ViewBag.StrCode = "Q-202" + ViewBag.Code;
            var tax = db.tbl_TAX.ToList();
            ViewBag.TaxList = db.tbl_TAX.Where(a => a.IsActive == true && a.IsDeleted != true).Select(a => new { Value = a.AmountInPercentage, Name = a.TaxDescription + " | " + a.AmountInPercentage.ToString() + "%" }).ToList();
            ViewBag.GSTPercent = tax.Where(a => a.TaxID == 2).Select(a => a.AmountInPercentage).FirstOrDefault();
            ViewBag.SSTPercent = tax.Where(a => a.TaxID == 3).Select(a => a.AmountInPercentage).FirstOrDefault();
        }

        public ActionResult Create(int? id)
        {
            GetViewBagData(id);
            if (id != null)
            {
                ViewBag.InquiryID = id;
            }
            return View();
        }

        public JsonResult SaveQuotation(tbl_InquiryQuotation model)
        {
            model.isActive = true;
            model.isDelete = false;
            model.AddOn = Helper.PST();
            model.AddBy = User.Identity.GetUserId();
            db.tbl_InquiryQuotation.Add(model);

            if(db.SaveChanges() > 0)
            {
                return Json("Success");
            }
            else
            {
                return Json("Failed");
            }
        }

        public JsonResult GetQuotationDetails(int? Qid)
        {
            var data = db.tbl_InquiryQuotationDetails.Where(a => a.QuotationID == Qid).Select(a=>new {
                ProductTypeID = a.tbl_Product.ProductTypeID,
                ProductID = a.ProductID,
                Product = a.tbl_Product.PartNo,
                Description = a.Description,
                unitCode = a.Unit,
                UnitPrice = a.UnitPrice,
                Qty = a.Qty,
                Total = a.Total
            }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(int? id)
        {
            GetViewBagData(id);
            if (id != null)
            {
                ViewBag.InquiryID = id;
            }

            
            var data = db.tbl_InquiryQuotation.Where(a => a.QuotationID == id).FirstOrDefault();
            ViewBag.StrCode = data.StrCode;
            return View(data);
        }

        public JsonResult UpdateQuotation(tbl_InquiryQuotation model)
        {
            var detail = db.tbl_InquiryQuotationDetails.Where(a => a.QuotationID == model.QuotationID).ToList();
            db.tbl_InquiryQuotationDetails.RemoveRange(detail);
            var data = db.tbl_InquiryQuotation.Where(a => a.QuotationID == model.QuotationID).FirstOrDefault();
            data.QuotationDate = model.QuotationDate;
            data.Amount = model.Amount;
            data.TaxPercnt = model.TaxPercnt;
            data.TaxAmount = model.TaxAmount;
            data.DiscPercnt = model.DiscPercnt;
            data.DiscAmount = model.DiscAmount;
            data.TotalAmount = model.TotalAmount;
            data.UpdatedOn = Helper.PST();
            data.UpdatedBy = User.Identity.GetUserId();
            data.GSTPercnt = model.GSTPercnt;
            data.GSTAmount = model.GSTAmount;
            data.OfferFor = model.OfferFor;
            data.QuotationType = model.QuotationType;
            data.TaxType = model.TaxType;
            data.OnAdvance = model.OnAdvance;
            data.OnDelivery = model.OnDelivery;
            data.OnCompletion = model.OnCompletion;
            data.ValidUpto = model.ValidUpto;
            db.Entry(data).State = System.Data.Entity.EntityState.Modified;

            db.tbl_InquiryQuotationDetails.AddRange(model.tbl_InquiryQuotationDetails);
            //db.SaveChanges();
            if (db.SaveChanges() > 0)
            {
                return Json("Success");
            }
            else
            {
                return Json("Failed");
            }
        }



        public FileResult Details(int id)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/InquiryQuotation.rdlc");


            ReportDataSource rs = new ReportDataSource();
            rs.Name = "InquiryQuotation";
            rs.Value = db.InquiryQuotation(id);
            lo.DataSources.Add(rs);

            var data = db.InquiryQuotation(id).ToList();
            List<object> o = new List<object>();
            foreach(var item in data)
            {
                object obj = new
                {
                    OfferFor = item.OfferFor,
                    Amount = item.Amount,
                    TaxPercnt = item.TaxPercnt,
                    TaxAmount = item.TaxAmount,
                    TaxType = item.TaxType,
                    GSTPercnt = item.GSTPercnt,
                    GSTAmount = item.GSTAmount,
                    TotalAmount = item.TotalAmount
                };

                o.Add(obj);
                break;
            }

            ReportDataSource rs1 = new ReportDataSource();
            rs1.Name = "InquiryQuotation1";
            rs1.Value = o;
            lo.DataSources.Add(rs1);

            ReportDataSource rs2 = new ReportDataSource();
            rs2.Name = "AddressDetail";
            rs2.Value = db.GetAddressDetail(0);
            lo.DataSources.Add(rs2);


            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            Response.AddHeader("Content-Disposition", "attachment; filename=EnquiryQuotation.pdf");

            return new FileContentResult(renderedBytes, mimeType);
        }

        public FileResult Details1(int id)
        {
            LocalReport lo = new LocalReport();
            lo.ReportPath = Server.MapPath("~/Models/Reports/InquiryQuotationBoth.rdlc");

            var quoteData1 = db.InquiryQuotation(id).Where(a => a.ProductTypeID == 1);
            var quoteData2 = db.InquiryQuotation(id).Where(a => a.ProductTypeID == 2);

            ReportDataSource rs = new ReportDataSource();
            rs.Name = "InquiryQuotation";
            rs.Value = quoteData1;
            lo.DataSources.Add(rs);

            ReportDataSource rs4 = new ReportDataSource();
            rs4.Name = "InquiryQuotation3";
            rs4.Value = quoteData2;
            lo.DataSources.Add(rs4);

            var data = db.InquiryQuotation(id).Where(a => a.ProductTypeID == 1).ToList();
            List<object> o = new List<object>();
            foreach (var item in data)
            {
                object obj = new
                {
                    OfferFor = item.OfferFor,
                    Amount = data.Sum(a=>a.Total), //item.Amount,
                    TaxPercnt = item.TaxPercnt,
                    TaxAmount = item.TaxAmount,
                    TaxType = item.TaxType,
                    GSTPercnt = item.GSTPercnt,
                    GSTAmount = item.GSTAmount,
                    TotalAmount = data.Sum(a => a.Total) + item.TaxAmount //item.TotalAmount
                };

                o.Add(obj);
                break;
            }

            var data1 = db.InquiryQuotation(id).Where(a => a.ProductTypeID == 2).ToList();
            List<object> o1 = new List<object>();
            foreach (var item in data1)
            {
                object obj = new
                {
                    OfferFor = item.OfferFor,
                    Amount = data1.Sum(a => a.Total), //item.Amount,
                    TaxPercnt = item.TaxPercnt,
                    TaxAmount = item.TaxAmount,
                    TaxType = item.TaxType,
                    GSTPercnt = item.GSTPercnt,
                    GSTAmount = item.GSTAmount,
                    TotalAmount = data1.Sum(a => a.Total) + item.GSTAmount //item.TotalAmount
                };

                o1.Add(obj);
                break;
            }


            ReportDataSource rs1 = new ReportDataSource();
            rs1.Name = "InquiryQuotation1";
            rs1.Value = o;
            lo.DataSources.Add(rs1);

            ReportDataSource rs3 = new ReportDataSource();
            rs3.Name = "InquiryQuotation2";
            rs3.Value = o1;
            lo.DataSources.Add(rs3);

            ReportDataSource rs2 = new ReportDataSource();
            rs2.Name = "AddressDetail";
            rs2.Value = db.GetAddressDetail(0);
            lo.DataSources.Add(rs2);


            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>PDF</OutputFormat>" + "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            renderedBytes = lo.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            Response.AddHeader("Content-Disposition", "attachment; filename=EnquiryQuotation.pdf");

            return new FileContentResult(renderedBytes, mimeType);
        }

        public ActionResult Edit(int? id)
        {
            GetViewBagData(id);
            var data = db.tbl_InquiryQuotation.Where(a => a.InquiryID == id).FirstOrDefault();

            return View(data);
        }
    }
}