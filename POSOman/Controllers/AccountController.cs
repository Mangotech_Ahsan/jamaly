﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using POSOman.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using POSOman.Models.BLL;
using Syncfusion.XPS;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Data.Entity;

namespace POSOman.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        ApplicationDbContext context;

        public AccountController()
        {
            context = new ApplicationDbContext();
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
       
        [AllowAnonymous]
        public ActionResult Login(string msg,string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            if (!string.IsNullOrWhiteSpace(msg))
            {
                ModelState.AddModelError("UserName", msg.ToString());

            }
            else if (returnUrl.Equals("/Account/CustomLogOff"))
            {
                ModelState.AddModelError("UserName", "Software has been locked! Please contact Administrator");

            }

            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            var response = RSharp.RestSharpClient(1);
            if (response == true)
            {
                return RedirectToAction("CustomLogOff", "Account");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var User = db.AspNetUsers.Where(x => x.UserName == model.UserName).Select(x => new { x.Id, x.UserId, x.IsAccountDisabled }).FirstOrDefault();
            if (User == null)
            {
                ModelState.AddModelError("", "No User Found. Please contact Administrator.");
                return View(model);
            }
            else if (User != null && User.IsAccountDisabled)
            {
                ModelState.AddModelError("", "Account has been locked. Please contact Administrator.");
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    {
                        
                        Session["User"] = User.Id.ToString();
                        Session["CurrentUserID"] = User.UserId;
                        Users_Log InsertUser = new Users_Log();
                        UserAction_Log UserActions = new UserAction_Log();
                        //var UpdateUser = db.Users_Log.Where(x => x.UserId == User && x.IsLogin == true && x.LogoutTime == null).FirstOrDefault();
                        var UpdateUser = db.Users_Log.Where(x => x.UserId == User.Id && x.IsLogin == true && x.LogoutTime == null).ToList();

                        if (UpdateUser.Count()>0)
                        {
                            foreach (var i in UpdateUser)
                            {
                                i.LogoutTime = DateTime.UtcNow.AddHours(5);
                                i.IsLogin = false;
                                db.Entry(i).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();

                            }
                            InsertUser.IsLogin = true;
                            InsertUser.LoginTime = DateTime.UtcNow.AddHours(5);
                            InsertUser.UserId = User.Id.ToString();
                            db.Users_Log.Add(InsertUser);
                            db.SaveChanges();

                            Session["LoginUserID"] = InsertUser.ID;

                            UserActions.ActionDate = DateTime.UtcNow.AddHours(5);
                            UserActions.UserAction = "LoggedIn".ToString();
                            InsertUser.UserAction_Log.Add(UserActions);
                            db.SaveChanges();
                            return RedirectToLocal(returnUrl);
                            //UpdateUser.LogoutTime = DateTime.UtcNow.AddHours(5);
                            //UpdateUser.IsLogin = false;
                            //db.Entry(UpdateUser).State = System.Data.Entity.EntityState.Modified;
                            //db.SaveChanges();
                            //return View(model);
                        }
                        else
                        {
                            InsertUser.IsLogin = true;
                            InsertUser.LoginTime = DateTime.UtcNow.AddHours(5);
                            InsertUser.UserId = User.Id.ToString();
                            db.Users_Log.Add(InsertUser);
                            db.SaveChanges();
                            Session["LoginUserID"] = InsertUser.ID;

                            UserActions.ActionDate = DateTime.UtcNow.AddHours(5);
                            UserActions.UserAction = "LoggedIn".ToString();
                            InsertUser.UserAction_Log.Add(UserActions);
                            db.SaveChanges();
                            return RedirectToLocal(returnUrl);
                        }
                        //Users_Log InsertUser = new Users_Log();


                        //if (db.Users_Log.Where(x=>x.UserId == User).Any(x => x.IsLogin == true))
                        //{
                        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        //    ModelState.AddModelError("", "Same User Already Logged In!");
                        //    return View(model);
                        //}
                        //else
                        //{
                        //    InsertUser.IsLogin = true;
                        //    InsertUser.LoginTime = DateTime.UtcNow.AddHours(5);
                        //    InsertUser.UserId = User.ToString();
                        //    db.Users_Log.Add(InsertUser);
                        //    db.SaveChanges();
                        //    return RedirectToLocal(returnUrl);
                        //}

                    }
                    
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        public ActionResult UserList(bool? btn, string uId) {

            var list = db.AspNetUsers.ToList();
            if (btn.HasValue)
            {
                if (list != null && list.Count>0)
                {
                    if (!string.IsNullOrWhiteSpace(uId))
                    {
                        return PartialView("_UserList", list.Where(x=>x.Id.Equals(uId)).OrderByDescending(so => so.UserId));

                    }
                    else
                    {
                        return PartialView("_UserList", list.OrderByDescending(so => so.UserId));

                    }
                }

                return PartialView("_UserList");
            }


            ViewBag.Users = db.AspNetUsers.Select(b => new { Value = b.Id, Name = b.UserName }).ToList();
            return View();

        }

        public JsonResult ChangeAccountStatus(string UserID,int Status)
        {
            if(!string.IsNullOrWhiteSpace(UserID) && Status >= 0)
            {
                using(var t = db.Database.BeginTransaction())
                {
                    var user = db.AspNetUsers.Where(x => x.Id.Equals(UserID) && !x.Id.Equals("18aea5a1-e74d-446f-bbdf-0220c34bc120")).FirstOrDefault();
                    if (user != null)
                    {
                        user.IsAccountDisabled = Status == 1 ? true : Status == 0 ? false : false;
                        db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        t.Commit();
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    t.Rollback();
                    return Json(-1, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ResetUserTempPassword(string UserID)
        {
            if (!string.IsNullOrWhiteSpace(UserID))
            {
                using (var t = db.Database.BeginTransaction())
                {
                    var user = db.AspNetUsers.Where(x => x.Id.Equals(UserID) && !x.Id.Equals("18aea5a1-e74d-446f-bbdf-0220c34bc120")).FirstOrDefault();
                    if (user != null)
                    {
                        user.PasswordHash = "ADE+l+hI/Qgfe2B+reunzPgaAVBLaaKfayrxK8nyrG++7NvMIPUs/Jpi2e2qN7DdTQ==";
                        user.SecurityStamp = "f79391f2-1bc9-4bff-8651-75ad7b4ea9ce";
                        db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        t.Commit();
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    t.Rollback();
                    return Json(-1, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        public  object RegisterUser(RegisterBindingViewModel model)
        {
            try
            {
                dbPOS dbase = new dbPOS();
                using (var t = dbase.Database.BeginTransaction())
                {
                    try
                    {
                            if (!dbase.AspNetUsers.Any(x => x.UserName.Equals(model.UserName)))
                            {
                            var _context = new ApplicationDbContext();
                            var UManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));
                            var user = new ApplicationUser { UserName = model.UserName, Email = model.UserName.ToString()+"@gmail.com", BranchID = 9001 };
                            var result = UManager.Create(user, model.Password);
                            if (result.Succeeded)
                            {

                                //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                                 UManager.AddToRoleAsync(user.Id, "SalePerson");
                               
                            }
                            else
                            {
                                return -2;
                            }
                           
                                tbl_AccountDetails vehUsr = new tbl_AccountDetails();

                                vehUsr.AccountName = model.Name;
                                vehUsr.AccountTypeID = 26;//Payables
                                vehUsr.IsActive = true;
                                vehUsr.IsDeleted = false;
                                vehUsr.UserID = user.Id;
                                vehUsr.AddOn = Helper.PST();
                                dbase.tbl_AccountDetails.Add(vehUsr);
                                if (dbase.SaveChanges() > 0)
                                {
                                
                                    t.Commit();
                                    return vehUsr.AccountID;
                                }
                                else
                                {
                                t.Rollback();
                                    return -3;
                                }
                            }

                            else
                            {

                            t.Rollback();
                            return -4;
                            }

                       
                    }
                    catch (Exception e)
                    {
                        t.Rollback();
                        while (e.InnerException != null)
                        {
                            e = e.InnerException;
                        }
                        return -5;
                    }
                }

            }
            catch (Exception d)
            {
                while (d.InnerException != null)
                {
                    d = d.InnerException;
                }
                return -5;
            }


        }
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [Authorize]
        public ActionResult Register()
        {
            dbPOS db = new dbPOS();
            if (UserRole() == "SuperAdmin")
            {

                ViewBag.Name = new SelectList(context.Roles.Where(u => !u.Name.Contains("SuperAdmin") && u.Id !="8" )
                                            .ToList(), "Name", "Name");
                ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
                return View();
            }
            else {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public string UserRole()
        {

            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;
                string role = "";
                context = new ApplicationDbContext();
                if (User.Identity.IsAuthenticated)
                {
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    var s = UserManager.GetRoles(user.GetUserId());
                    role = s[0].ToString();
                }
                ViewBag.Role = role;

                return role;
            }
            else
            {
                ViewBag.Name = "Not Logged IN";
                return string.Empty;
            }
        }
        //
        // POST: /Account/Register
        [HttpPost]
       // [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                dbPOS db = new dbPOS();
                
                if (UserRole() == "SuperAdmin")
                {
                    model.BranchID = 9001;
                    var user = new ApplicationUser { UserName = model.UserName, Email = model.Email ,BranchID = model.BranchID };
                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);                        
                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                        await this.UserManager.AddToRoleAsync(user.Id, model.UserRoles);

                        if(model.UserRoles == "SalePerson")
                        {
                            using(var t = db.Database.BeginTransaction())
                            {
                                tbl_AccountDetails ad = new tbl_AccountDetails();
                                ad.AccountName = model.UserName;
                                ad.AddOn = Helper.PST();
                                ad.BranchID = 9001;
                                ad.IsActive = true;
                                ad.AccountTypeID = 6;//NCA
                                db.tbl_AccountDetails.Add(ad);
                                db.SaveChanges();

                                tbl_SalePerson sp = new tbl_SalePerson();

                                sp.AccountID = ad.AccountID;
                                sp.Cell = "-";
                                sp.CNIC = "-";
                                sp.SalePerson = model.UserName;
                                sp.AddedOn = Helper.PST();
                                db.tbl_SalePerson.Add(sp);
                                db.SaveChanges();

                                t.Commit();
                            }
                          

                        }

                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        Session["BranchID"] = null;
                        Session["User"] = null;
                        Session["LoginUserID"] = null;
                        Session["CurrentUserID"] = null;
                        return RedirectToAction("Index", "Home");
                    }
                    ViewBag.Name = new SelectList(context.Roles.Where(u => !u.Name.Contains("SuperAdmin"))
                                              .ToList(), "Name", "Name");                    
                    ViewBag.Branch = db.tbl_Branch.Select(b => new { Value = b.BranchID, Name = b.BranchName }).ToList();
                    AddErrors(result);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }


            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null )//|| !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");

                return RedirectToAction("ForgotPasswordConfirmation", "Account");
                // Send an email with this link
                //string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                //var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                //await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                //return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LogOff()
        {
            try
            {
                using(var d = new dbPOS())
                {
                    
                        string UserID = User.Identity.GetUserId();
                        int c = 0;
                        if (!string.IsNullOrWhiteSpace(UserID))
                        {
                            var UpdateUser = await d.Users_Log.Where(x => x.UserId == UserID && x.IsLogin == true && x.LogoutTime == null).OrderByDescending(x => x.ID).FirstOrDefaultAsync();

                            if (UpdateUser != null)
                            {

                                UpdateUser.LogoutTime = Helper.PST();
                                UpdateUser.IsLogin = false;
                                d.Entry(UpdateUser).State = System.Data.Entity.EntityState.Modified;
                                c += await d.SaveChangesAsync();
                                UserActions.MapActions(Convert.ToInt32(Session["LoginUserID"]), "LoggedOut");

                            }
                            else
                            {
                                var LockUser = await d.Users_Log.Where(x => x.LogoutTime == null && x.ID != 0).ToListAsync();

                                if (LockUser.Count > 0)
                                {
                                    LockUser.ForEach(x =>
                                    {
                                        x.LogoutTime = Helper.PST();
                                        x.IsLogin = false;
                                        d.Entry(x).State = System.Data.Entity.EntityState.Modified;
                                    });
                                    c += await d.SaveChangesAsync();

                                    UserActions.MapActions(null, "LoggedOut");

                                }
                                //foreach (var i in LockUser)
                                //{
                                //    //i.LogoutTime = DateTime.UtcNow.AddHours(5);
                                //    i.IsLogin = false;
                                //    db.Entry(i).State = System.Data.Entity.EntityState.Modified;
                                //    db.SaveChanges();
                                //}

                            }
                        }

                        var getSystemData = Helper.GetHostNameAndIPAddress();
                        if (getSystemData != null)
                        {
                            var usrHost = d.tbl_GenericDataTable.Where(x => x.DataValue.Equals(getSystemData.HostName)).FirstOrDefault();
                            var usrIp = d.tbl_GenericDataTable.Where(x => x.DataValue.Equals(getSystemData.IpAddress)).FirstOrDefault();
                            if (usrHost != null && usrIp != null)
                            {
                                usrHost.IsSignedOut = true;
                                usrHost.SignedOutOn = Helper.PST();
                                d.Entry(usrHost).State = EntityState.Modified;

                                usrIp.IsSignedOut = true;
                                usrIp.SignedOutOn = Helper.PST();
                                d.Entry(usrIp).State = EntityState.Modified;

                                c += await d.SaveChangesAsync();
                            }
                        }

                }
                
               
            }
            catch(Exception ex) 
            {

            }
            
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                Session["BranchID"] = null;
                Session["User"] = null;
                Session["LoginUserID"] = null;
                Session["CurrentUserID"] = null;
              
            return RedirectToAction("Index", "Home");


        }


        public void ThreadFunc()
        {
            System.Timers.Timer t = new System.Timers.Timer();
            t.Elapsed += new System.Timers.ElapsedEventHandler(TimerWorker);
            //t.Interval = 1800000;
            t.Interval = 30000;
            t.Enabled = true;
            t.AutoReset = true;
            t.Start();
        }

        public void TimerWorker(object sender, System.Timers.ElapsedEventArgs e)
        {
            
            var response = RSharp.RestSharpClient(1);
            if (response == true)
            {
                
                //Response.Redirect("~/Account/CustomLogOff");
                //Response.Redirect("www.facebook.com");
                Redirect("~/Account/CustomLogOff");
                //RedirectToAction("CustomLogOff").ExecuteResult(context);
            }
            //return null;
        }


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult CustomLogOff()
        {
            try
            {
                Session["BranchID"] = null;
                Session["User"] = null;
                Session["LoginUserID"] = null;
                Session["CurrentUserID"] = null;
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                
            }
            catch(Exception e)
            {
                //while (e.InnerException != null)
                //{
                //    e = e.InnerException;
                //}
            }
            ModelState.AddModelError("UserName", "Software has been locked! Please contact Administrator.");
            return View();
          //  return RedirectToAction("Login", "Account",new { msg = "Software has been locked! Please contact Administrator.",returnUrl = "/"});
        }



        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}
