﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using POSOman.Models.BLL;

namespace POSOman.Controllers
{
    public class StickerDieCuttingController : Controller
    {
        private dbPOS db = new dbPOS();

        // GET: StickerDieCutting
        public ActionResult Index()
        {
            return View(db.tbl_StickerDieCutting.Where(x=>x.IsDeleted!=true).ToList());
        }

        // GET: StickerDieCutting/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerDieCutting tbl_StickerDieCutting = db.tbl_StickerDieCutting.Find(id);
            if (tbl_StickerDieCutting == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerDieCutting);
        }

        // GET: StickerDieCutting/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StickerDieCutting/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,DieCutting,Rate,IsDeleted,AddedOn")] tbl_StickerDieCutting model)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(model.DieCutting))
                {
                    ModelState.AddModelError("DieCutting", "Required");
                    return View(model);
                }
                if (model.Rate<=0)
                {
                    ModelState.AddModelError("Rate", "Required");
                    return View(model);
                }
                model.AddedOn = Helper.PST();
                db.tbl_StickerDieCutting.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: StickerDieCutting/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerDieCutting tbl_StickerDieCutting = db.tbl_StickerDieCutting.Find(id);
            if (tbl_StickerDieCutting == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerDieCutting);
        }

        // POST: StickerDieCutting/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( tbl_StickerDieCutting model)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(model.DieCutting))
                {
                    ModelState.AddModelError("DieCutting", "Required");
                    return View(model);
                }
                if (model.Rate <= 0)
                {
                    ModelState.AddModelError("Rate", "Required");
                    return View(model);
                }
                db.Entry(model).State = EntityState.Modified;
                db.Entry(model).Property(p=>p.AddedOn).IsModified = false;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: StickerDieCutting/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_StickerDieCutting tbl_StickerDieCutting = db.tbl_StickerDieCutting.Find(id);
            if (tbl_StickerDieCutting == null)
            {
                return HttpNotFound();
            }
            return View(tbl_StickerDieCutting);
        }

        // POST: StickerDieCutting/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_StickerDieCutting model = db.tbl_StickerDieCutting.Find(id);
            if (model != null)
            {
                model.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("DieCutting", "Record not found");
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
