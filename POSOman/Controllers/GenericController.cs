﻿using OfficeOpenXml;
using POSOman.Models;
using POSOman.Models.BLL;
using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Controllers
{
    public class GenericController : Controller
    {

        public ActionResult UploadProductExcel()
        {
            return View();
        }
        // GET: Generic
        public JsonResult UploadBulkProductViaExcel(FormCollection formCollection)
        {

            var prodList = new List<ExcelProductDTO>();
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files[0];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    var epoch = new DateTime(1970, 1, 1, 5, 0, 0); //--5 for GMT 5
                    long time = Convert.ToInt64((DateTime.Now - epoch).TotalSeconds);
                    #region Save File for future record 
                    string docName = time + Path.GetFileName(file.FileName);
                    string docPath = "/Content/Excels/Products/" + docName;
                    //file.SaveAs(Server.MapPath(docPath));
                    #endregion
                    string fileName = file.FileName;
                    string ext = Path.GetExtension(docPath);
                    if (ext == ".xlsx")
                    {
                        string fileContentType = file.ContentType;
                        byte[] fileBytes = new byte[file.ContentLength];
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                        using (var package = new ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                try
                                {
                                    var Current_Name = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 1].Value, null);
                                    // var catName = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 2].Value, null);
                                    //  var packaging = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 3].Value, null);
                                    var Qty = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 2].Value, null);
                                    //var Qty = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 3].Value, null);
                                    //var vendor = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 3].Value, null);
                                    //var prodSize = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 4].Value, null);
                                    //var packaging = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 5].Value, null);
                                    //var partNo = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 6].Value, null);
                                    //var uniqueNumber = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 7].Value, null);
                                    int uniqueNumber = rowIterator;


                                    // var vendor = (workSheet.Cells[rowIterator, 3].Text);
                                    if (!string.IsNullOrWhiteSpace(Current_Name))
                                    {
                                        //var getID = Id.Split('-');
                                        //if (getID.Count() > 1)
                                        //{
                                        //    Id = getID[1];
                                        //}
                                        //var product = new ExcelProductNameDTO();
                                        var product = new ExcelProductDTO();
                                        //product.Id = Id;
                                        product.Qty = Convert.ToDecimal(Qty);
                                        product.Current_Name = Current_Name;
                                        product.RowID = uniqueNumber;
                                        prodList.Add(product);
                                    }
                                    else
                                    {
                                        return Json("Invalid Column Defined", JsonRequestBehavior.AllowGet);
                                    }
                                }
                                catch (Exception ex)
                                {

                                    while (ex.InnerException != null)
                                    {
                                        ex = ex.InnerException;
                                    }
                                    return Json(ex.Message, JsonRequestBehavior.AllowGet);

                                }

                            }

                            if (prodList != null && prodList.Count > 0)
                            {

                                using (var dbs = new dbPOS())
                                {
                                    using (var d = dbs.Database.BeginTransaction())
                                    {
                                        int c = 0;
                                        //foreach(var p in prodList)
                                        //{
                                        //    int id = Convert.ToInt32(p.Id);
                                        //    var prod = dbs.tbl_Product.Where(x => x.ProductID == id).FirstOrDefault();
                                        //    if (prod != null)
                                        //    {
                                        //        prod.PartNo = p.Current_Name.Trim();
                                        //        dbs.Entry(prod).State = System.Data.Entity.EntityState.Modified;
                                        //        c += dbs.SaveChanges();
                                        //    }
                                        //}

                                        List<tbl_Product> productList = new List<tbl_Product>();
                                        foreach (var p in prodList)
                                        {

                                            tbl_Product prod = new tbl_Product();
                                            prod.PartNo = p.Current_Name.Trim();
                                            prod.Addby = 1;
                                            prod.AddOn = Helper.PST();
                                            prod.BranchID = 9001;
                                            prod.BrandID = 1;
                                            prod.CostPrice = 0;
                                            prod.DepartmentID = 5;
                                            prod.Description = "Bulk Insertion";
                                            prod.GenericName = prod.PartNo;
                                            prod.rowguid = Guid.NewGuid();
                                            prod.isActive = 1;
                                            prod.UnitCodeID = 4;
                                            prod.UnitCode = "pcs";
                                            prod.IsGeneralItem = false;
                                            prod.IsOffsetItem = false;
                                            prod.ItemDescription = prod.Description;
                                            prod.SaleRate = 0;
                                            prod.VehicleCodeID = 1;
                                            productList.Add(prod);

                                            //int id = Convert.ToInt32(p.Id);
                                            //var prod = dbs.tbl_Stock.Where(x => x.ProductID == id).FirstOrDefault();
                                            //if (prod != null)
                                            //{
                                            //    prod.Qty = p.Qty;
                                            //    dbs.Entry(prod).State = System.Data.Entity.EntityState.Modified;
                                            //    c += dbs.SaveChanges();
                                            //}
                                        }

                                        if (productList.Count > 0)
                                        {
                                            dbs.tbl_Product.AddRange(productList);
                                            c += dbs.SaveChanges();
                                        }

                                        if (c > 0)
                                        {
                                            d.Commit();
                                            return Json("1", JsonRequestBehavior.AllowGet);
                                        }
                                        else
                                        {
                                            d.Rollback();
                                        }
                                    }
                                }

                                return Json(null, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        return Json("File type is incorrect.", JsonRequestBehavior.AllowGet);

                    }
                }
            }

            return Json(-1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UploadCustomerExcel()
        {
            return View();
        }

        public JsonResult UploadBulkCustomerViaExcel(FormCollection formCollection)
        {

            var excelList = new List<ExcelCustomerDTO>();
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files[0];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    var epoch = new DateTime(1970, 1, 1, 5, 0, 0); //--5 for GMT 5
                    long time = Convert.ToInt64((DateTime.Now - epoch).TotalSeconds);
                    #region Save File for future record 
                    string docName = time + Path.GetFileName(file.FileName);
                    string docPath = "/Content/Excels/Customers/" + docName;
                    //file.SaveAs(Server.MapPath(docPath));
                    #endregion
                    string fileName = file.FileName;
                    string ext = Path.GetExtension(docPath);
                    if (ext == ".xlsx")
                    {
                        string fileContentType = file.ContentType;
                        byte[] fileBytes = new byte[file.ContentLength];
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                        using (var package = new ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                try
                                {
                                    var Name = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 1].Value, null);
                                    // var catName = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 2].Value, null);
                                    //  var packaging = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 3].Value, null);
                                    var Address = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 2].Value, null);
                                    var Phone = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 3].Value, null);
                                    var NTN = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 4].Value, null);
                                    var STN = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 5].Value, null);
                                    //var vendor = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 3].Value, null);
                                    //var prodSize = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 4].Value, null);
                                    //var packaging = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 5].Value, null);
                                    //var partNo = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 6].Value, null);
                                    //var uniqueNumber = ConversionClass.SafeConvertToString(workSheet.Cells[rowIterator, 7].Value, null);
                                    int uniqueNumber = rowIterator;


                                    // var vendor = (workSheet.Cells[rowIterator, 3].Text);
                                    if (!string.IsNullOrWhiteSpace(Name))
                                    {

                                        //var product = new ExcelProductNameDTO();
                                        var customer = new ExcelCustomerDTO();
                                        customer.Name = Name;
                                        customer.Address = Address;
                                        customer.Phone = Phone;
                                        customer.NTN = NTN;
                                        customer.STN = STN;
                                        excelList.Add(customer);
                                    }
                                    else
                                    {
                                        return Json("Invalid Column Defined", JsonRequestBehavior.AllowGet);
                                    }
                                }
                                catch (Exception ex)
                                {

                                    while (ex.InnerException != null)
                                    {
                                        ex = ex.InnerException;
                                    }
                                    return Json(ex.Message, JsonRequestBehavior.AllowGet);

                                }

                            }

                            if (excelList != null && excelList.Count > 0)
                            {

                                using (var dbs = new dbPOS())
                                {
                                    using (var d = dbs.Database.BeginTransaction())
                                    {
                                        int c = 0;
                                        double codeInt = 0;

                                        List<tbl_AccountDetails> adList = new List<tbl_AccountDetails>();
                                        List<tbl_Customer> custList = new List<tbl_Customer>();

                                        foreach (var e in excelList)
                                        {
                                            if (codeInt == 0)
                                            {
                                                string code = dbs.tbl_Customer.OrderByDescending(v => v.Code).Select(v => v.Code).FirstOrDefault();
                                                if (!string.IsNullOrWhiteSpace(code))
                                                {
                                                    string[] splitCode = code.Split('-');

                                                    code = (Convert.ToDouble(splitCode[1]) + 1).ToString();
                                                    codeInt = Convert.ToDouble(splitCode[1]);
                                                }
                                                else
                                                {
                                                    code = "C-1000";
                                                    codeInt = 1000;
                                                }
                                            }
                                            else
                                            {
                                                codeInt++;
                                            }

                                            dbs.InsertBulkCustomer(e.Name.Trim(), "C-" + codeInt.ToString(), e.Address, null, null, null, e.Name.Trim(), null, null, null, 0, 0, 4001, 9001, null, e.NTN, "18", e.STN, 1, null, null);
                                            dbs.SaveChanges();
                                            c++;
                                            //tbl_AccountDetails ad = new tbl_AccountDetails();
                                            //ad.AccountName = e.Name.Trim();
                                            //ad.Bank = e.NTN + "|" + e.STN + "|" + e.Address;
                                            //ad.AccountCode = "C-" + codeInt.ToString();
                                            //ad.Code = Convert.ToInt32(codeInt);
                                            //ad.IsActive = true;
                                            //ad.rowguid = Guid.NewGuid();
                                            //ad.AccountTypeID = 25;
                                            //ad.Addby = 1;
                                            //ad.AddOn = Helper.PST();
                                            //ad.OpeningBalance = 0;
                                            //ad.UserID = "18aea5a1-e74d-446f-bbdf-0220c34bc120";
                                            //ad.BranchID = 9001;
                                            //ad.OpeningDate = Helper.PST();

                                            //adList.Add(ad);

                                            //tbl_Customer cust = new tbl_Customer();

                                            //cust.Addby = 4001;
                                            //cust.AddOn = Helper.PST();
                                            //cust.Address = e.Address;
                                            //cust.STN = e.STN;
                                            //cust.NTN = e.NTN;
                                            //cust.Name = e.Name.Trim();
                                            //cust.Code = "C"+codeInt.ToString();
                                            //cust.Company = e.Name.Trim();
                                            //cust.GST = "18";
                                            //cust.InvoiceType = 1;
                                            //cust.IsActive = true;
                                            //cust.IsDeleted = false;
                                            //cust.OpeningBalance = 0;
                                            //cust.CreditLimit = 0;
                                            //cust.rowguid = Guid.NewGuid();

                                            //custList.Add(cust);
                                            //dbs.tbl_Customer.Add(cust);
                                            //c += dbs.SaveChanges();
                                        }

                                        //if(adList!=null && adList.Count > 0)
                                        //{
                                        //    //dbs.tbl_AccountDetails.AddRange(adList);
                                        //    //c += dbs.SaveChanges();

                                        //    foreach(var a in adList)
                                        //    {
                                        //        string[] splitCode = a.Bank.Split('|');
                                        //        //tbl_Customer cust = new tbl_Customer();

                                        //        //cust.AccountID = a.AccountID;
                                        //        //cust.Addby = 4001;
                                        //        //cust.AddOn = Helper.PST();
                                        //        //cust.Address = splitCode[2];
                                        //        //cust.STN = splitCode[1];
                                        //        //cust.NTN = splitCode[0];
                                        //        //cust.Name = a.AccountName.Trim();
                                        //        //cust.Code = a.AccountCode;
                                        //        //cust.Company = a.AccountName;
                                        //        //cust.GST = "18";
                                        //        //cust.InvoiceType = 1;
                                        //        //cust.IsActive = true;
                                        //        //cust.IsDeleted = false;
                                        //        //cust.OpeningBalance = 0;
                                        //        //cust.CreditLimit = 0;
                                        //        //cust.rowguid = Guid.NewGuid();

                                        //        //custList.Add(cust);
                                        //        //dbs.tbl_Customer.Add(cust);
                                        //        //c += dbs.SaveChanges();
                                        //    }

                                        //    if(custList!=null && custList.Count > 0)
                                        //    {
                                        //        dbs.tbl_Customer.AddRange(custList);
                                        //        c += dbs.SaveChanges();
                                        //    }
                                        //}

                                        if (c > 0)
                                        {
                                            d.Commit();
                                            return Json("1", JsonRequestBehavior.AllowGet);
                                        }
                                        else
                                        {
                                            d.Rollback();
                                        }
                                    }
                                }

                                return Json(null, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        return Json("File type is incorrect.", JsonRequestBehavior.AllowGet);

                    }
                }
            }

            return Json(-1, JsonRequestBehavior.AllowGet);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Result = RedirectToAction("Index", "Home");
        }
    }
}