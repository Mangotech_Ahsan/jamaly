USE [db_Clover_Medical_6]
GO
DELETE FROM [dbo].[tbl_StockLog]
GO
DELETE FROM [dbo].[tbl_StockBatch]
GO
DELETE FROM [dbo].[tbl_Stock_Recd]
GO
DELETE FROM [dbo].[tbl_Stock]
GO
DELETE FROM [dbo].[tbl_DOReturn]
GO
DELETE FROM [dbo].[tbl_DODetails]
GO
DELETE FROM [dbo].[tbl_SOReturnDetails]
GO
DELETE FROM [dbo].[tbl_SalesReturn]
GO
DELETE FROM [dbo].[tbl_SaleDetails]
GO
DELETE FROM [dbo].[tbl_SalesOrder]
GO
DELETE FROM [dbo].[tbl_QuoteDetails]
GO
DELETE FROM [dbo].[tbl_Quotation]
GO
DELETE FROM [dbo].[tbl_PODetails]
GO
DELETE FROM [dbo].[tbl_PurchaseOrder]
GO
DELETE FROM [dbo].[tbl_JEntryLog]
GO
DELETE FROM [dbo].[tbl_JDetail]
GO
DELETE FROM [dbo].[tbl_JEntry]
GO

DELETE FROM tmp_OrderDetails

DELETE FROM tmp_PO