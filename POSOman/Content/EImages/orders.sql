-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2020 at 08:55 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medify_wholesaler`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_code` varchar(20) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `delivery_charges` double DEFAULT 0,
  `status` enum('Pending','Complete') DEFAULT 'Pending',
  `order_date` date DEFAULT NULL,
  `customer_type` enum('Normal Customer','Wholesaler Customer') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `customer_id`, `delivery_charges`, `status`, `order_date`, `customer_type`, `created_at`, `updated_at`) VALUES
(1, '00001', 12, 0, 'Pending', '2019-08-05', 'Wholesaler Customer', '2019-08-06 18:09:17', '2019-08-06 18:09:17'),
(2, '00002', 7, 0, 'Pending', '2019-08-05', 'Wholesaler Customer', '2019-08-06 18:57:05', '2019-08-06 18:57:05'),
(3, '00003', 7, 0, 'Pending', '2019-08-05', 'Wholesaler Customer', '2019-08-06 19:10:50', '2019-08-06 19:10:50'),
(4, '00004', 7, 0, 'Pending', '2019-08-08', 'Wholesaler Customer', '2019-08-09 16:51:20', '2019-08-09 16:51:20'),
(5, '00005', 18, 0, 'Pending', '2019-08-08', 'Wholesaler Customer', '2019-08-09 17:18:20', '2019-08-09 17:18:20'),
(6, '00006', 7, 0, 'Pending', '2019-08-08', 'Wholesaler Customer', '2019-08-09 17:57:06', '2019-08-09 17:57:06'),
(7, '00007', 7, 0, 'Pending', '2019-08-08', 'Wholesaler Customer', '2019-08-09 19:27:08', '2019-08-09 19:27:08'),
(8, '00008', 21, 0, 'Pending', '2019-08-09', 'Wholesaler Customer', '2019-08-10 17:12:16', '2019-08-10 17:12:16'),
(9, '00009', 7, 0, 'Pending', '2019-08-09', 'Wholesaler Customer', '2019-08-10 18:04:23', '2019-08-10 18:04:23'),
(10, '00010', 7, 0, 'Pending', '2019-08-09', 'Wholesaler Customer', '2019-08-10 19:15:39', '2019-08-10 19:15:39'),
(11, '00011', 7, 0, 'Pending', '2019-08-15', 'Wholesaler Customer', '2019-08-16 20:20:07', '2019-08-16 20:20:07'),
(12, '00012', 23, 0, 'Pending', '2019-08-16', 'Wholesaler Customer', '2019-08-17 16:24:11', '2019-08-17 16:24:11'),
(13, '00013', 4, 0, 'Pending', '2019-08-16', 'Normal Customer', '2019-08-17 17:17:51', '2019-08-17 17:17:51'),
(14, '00014', 23, 0, 'Pending', '2019-08-16', 'Wholesaler Customer', '2019-08-17 22:55:19', '2019-08-17 22:55:19'),
(15, '00015', 31, 0, 'Pending', '2019-08-19', 'Normal Customer', '2019-08-20 23:15:56', '2019-08-20 23:15:56'),
(16, '00016', 31, 0, 'Pending', '2019-08-21', 'Normal Customer', '2019-08-22 23:10:44', '2019-08-22 23:10:44'),
(17, '00017', 33, 0, 'Pending', '2019-08-22', 'Normal Customer', '2019-08-23 21:15:38', '2019-08-23 21:15:38'),
(18, '00018', 33, 0, 'Pending', '2019-08-22', 'Normal Customer', '2019-08-23 21:29:21', '2019-08-23 21:29:21'),
(19, '00019', 33, 0, 'Pending', '2019-08-22', 'Normal Customer', '2019-08-23 21:45:03', '2019-08-23 21:45:03'),
(20, '00020', 33, 0, 'Pending', '2019-08-22', 'Normal Customer', '2019-08-23 22:05:13', '2019-08-23 22:05:13'),
(21, '00021', 48, 0, 'Pending', '2019-08-23', 'Normal Customer', '2019-08-24 19:15:15', '2019-08-24 19:15:15'),
(22, '00022', 50, 0, 'Pending', '2019-08-23', 'Normal Customer', '2019-08-24 19:53:36', '2019-08-24 19:53:36'),
(23, '00023', 16, 0, 'Pending', '2019-09-07', 'Normal Customer', '2019-09-08 14:02:38', '2019-09-08 14:02:38'),
(24, '00024', 16, 0, 'Pending', '2019-09-08', 'Normal Customer', '2019-09-09 16:03:12', '2019-09-09 16:03:12'),
(25, '00025', 31, 0, 'Pending', '2019-09-12', 'Normal Customer', '2019-09-13 07:09:25', '2019-09-13 07:09:25'),
(26, '00026', 31, 1000, 'Pending', '2019-09-12', 'Normal Customer', '2019-09-30 13:41:00', '2019-09-30 13:41:00'),
(27, '00027', 48, 150, 'Pending', '2019-10-04', 'Normal Customer', '2019-10-12 00:33:18', '2019-10-12 10:33:18'),
(28, '00028', 93, 150, 'Pending', '2019-10-11', 'Normal Customer', '2019-10-12 00:39:40', '2019-10-12 10:39:40'),
(29, '00029', 95, 0, 'Pending', '2019-10-12', 'Normal Customer', '2019-10-12 17:02:51', '2019-10-12 17:02:51'),
(30, '00030', 52, 0, 'Pending', '2019-10-12', 'Normal Customer', '2019-10-13 12:24:26', '2019-10-13 12:24:26'),
(31, '00031', 87, 0, 'Pending', '2019-10-14', 'Normal Customer', '2019-10-15 07:26:40', '2019-10-15 07:26:40'),
(32, '00032', 100, 0, 'Pending', '2019-10-15', 'Normal Customer', '2019-10-15 18:26:22', '2019-10-15 18:26:22'),
(33, '00033', 100, 0, 'Pending', '2019-10-16', 'Normal Customer', '2019-10-16 23:54:59', '2019-10-16 23:54:59'),
(34, '00034', 100, 0, 'Pending', '2019-10-16', 'Normal Customer', '2019-10-17 04:24:20', '2019-10-17 04:24:20'),
(35, '00035', 100, 0, 'Pending', '2019-10-16', 'Normal Customer', '2019-10-17 04:54:32', '2019-10-17 04:54:32'),
(36, '00036', 40, 250, 'Pending', '2019-10-18', 'Normal Customer', '2019-10-18 14:24:16', '2019-10-19 00:24:16'),
(37, '00037', 29, 0, 'Pending', '2019-10-19', 'Normal Customer', '2019-10-19 22:43:41', '2019-10-19 22:43:41'),
(38, '00038', 48, 0, 'Pending', '2019-10-25', 'Normal Customer', '2019-10-25 20:28:24', '2019-10-25 20:28:24'),
(39, '00039', 127, 250, 'Pending', '2019-11-01', 'Normal Customer', '2019-11-05 14:14:16', '2019-11-06 01:14:16'),
(40, '00040', 40, 0, 'Pending', '2019-11-04', 'Normal Customer', '2019-11-05 03:00:57', '2019-11-05 03:00:57'),
(41, '00041', 174, 200, 'Pending', '2019-11-08', 'Normal Customer', '2019-11-09 12:20:59', '2019-11-09 23:20:59'),
(42, '00042', 174, 250, 'Pending', '2019-11-12', 'Normal Customer', '2019-11-12 12:49:06', '2019-11-12 23:49:06'),
(43, '00043', 46, 0, 'Pending', '2019-11-17', 'Normal Customer', '2019-11-17 11:44:33', '2019-11-17 11:44:33'),
(44, '00044', 216, 0, 'Pending', '2019-11-23', 'Normal Customer', '2019-11-24 00:40:19', '2019-11-24 00:40:19'),
(45, '00045', 48, 0, 'Pending', '2019-11-25', 'Normal Customer', '2019-11-26 03:40:06', '2019-11-26 03:40:06'),
(46, '00046', 122, 250, 'Pending', '2019-11-25', 'Normal Customer', '2019-11-27 15:29:40', '2019-11-28 02:29:40'),
(47, '00047', 111, 300, 'Pending', '2019-11-26', 'Normal Customer', '2019-11-28 12:23:40', '2019-11-28 23:23:40'),
(48, '00048', 259, 0, 'Pending', '2019-12-11', 'Normal Customer', '2019-12-12 02:55:32', '2019-12-12 02:55:32'),
(49, '00049', 229, 0, 'Pending', '2019-12-16', 'Normal Customer', '2019-12-16 20:21:12', '2019-12-16 20:21:12'),
(50, '00050', 229, 0, 'Pending', '2019-12-23', 'Normal Customer', '2019-12-23 20:54:05', '2019-12-23 20:54:05'),
(51, '00051', 108, 250, 'Pending', '2019-12-24', 'Normal Customer', '2019-12-24 14:23:54', '2019-12-25 01:23:54'),
(52, '00052', 229, 0, 'Pending', '2019-12-26', 'Normal Customer', '2019-12-26 16:15:28', '2019-12-26 16:15:28'),
(53, '00053', 264, 0, 'Pending', '2019-12-29', 'Normal Customer', '2019-12-29 18:51:26', '2019-12-29 18:51:26'),
(54, '00054', 48, 0, 'Pending', '2019-12-30', 'Normal Customer', '2019-12-30 20:35:48', '2019-12-30 20:35:48'),
(55, '00055', 229, 0, 'Pending', '2020-01-07', 'Normal Customer', '2020-01-07 16:22:50', '2020-01-07 16:22:50'),
(56, '00056', 108, 0, 'Pending', '2020-01-17', 'Normal Customer', '2020-01-17 20:55:45', '2020-01-17 20:55:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`,`order_code`,`customer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
