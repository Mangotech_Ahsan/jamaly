﻿using Microsoft.Owin;
using Owin;
using POSOman.Controllers;
using System.Threading;

[assembly: OwinStartupAttribute(typeof(POSOman.Startup))]
namespace POSOman
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            
            //Thread thread = new Thread(new ThreadStart(new AccountController().ThreadFunc));
            //thread.IsBackground = true;
            //thread.Name = "JamalyClient";
            //thread.Start();
        }
    }
}
