//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    
    public partial class GetExpensesFilterWise_Result
    {
        public Nullable<decimal> Total { get; set; }
        public int JEntryID { get; set; }
        public string CodeString { get; set; }
        public string AccountName { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public Nullable<System.DateTime> VoucherDate { get; set; }
        public string BranchName { get; set; }
        public decimal Amount { get; set; }
    }
}
