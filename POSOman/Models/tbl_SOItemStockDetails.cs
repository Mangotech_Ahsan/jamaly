//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_SOItemStockDetails
    {
        public int ID { get; set; }
        public int OrderID { get; set; }
        public int ItemID { get; set; }
        public int DepartmentID { get; set; }
        public decimal Qty { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime AddedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
