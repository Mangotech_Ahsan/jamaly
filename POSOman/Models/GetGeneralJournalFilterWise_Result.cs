//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    
    public partial class GetGeneralJournalFilterWise_Result
    {
        public Nullable<decimal> TotalDebit { get; set; }
        public Nullable<decimal> TotalCredit { get; set; }
        public Nullable<long> CodeInt { get; set; }
        public string CodeString { get; set; }
        public int JentryID { get; set; }
        public Nullable<int> EntryTypeID { get; set; }
        public int JDetailID { get; set; }
        public Nullable<int> AccountID { get; set; }
        public Nullable<System.DateTime> VoucherDate { get; set; }
        public string AccountName { get; set; }
        public string Description { get; set; }
        public decimal Dr { get; set; }
        public decimal Cr { get; set; }
        public string Memo { get; set; }
        public Nullable<bool> ThorughGJ { get; set; }
        public string BranchName { get; set; }
        public string UserName { get; set; }
        public string VoucherName { get; set; }
        public Nullable<int> RefID { get; set; }
    }
}
