//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    
    public partial class GetIssuedPDCDateWise_Result
    {
        public string BranchName { get; set; }
        public string VoucherName { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string vendorCode { get; set; }
        public string ReferenceDetail { get; set; }
        public string BankName { get; set; }
        public string ChequeDate { get; set; }
        public string ChequeNumber { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public int ChequeTypeID { get; set; }
        public int JEntryId { get; set; }
        public Nullable<bool> IsBounced { get; set; }
        public Nullable<bool> IsCleared { get; set; }
    }
}
