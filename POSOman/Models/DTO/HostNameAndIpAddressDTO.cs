﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class HostNameAndIpAddressDTO
    {
        public string HostName { get; set; }
        public string IpAddress { get; set; }
    }
}