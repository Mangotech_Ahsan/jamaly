﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class PurchaseReturn
    {
        public int PurchaseReturnID { get; set; }
        public int AccountID { get; set; }
        public int PurchaseID { get; set; }
        public string Barcode { get; set; }
        public string InvoiceNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ReturnDate { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<int> PaymentTypeID { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> Expenses { get; set; }
        public string PurchaseCode { get; set; }
        public string Currency { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<decimal> DiscountPercent { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> AddBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<System.DateTime> UpdateBy { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsPaid { get; set; }
        public string Status { get; set; }
        public Nullable<int> BranchID { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public string UserID { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }

        public virtual tbl_AccountDetails tbl_AccountDetails { get; set; }
        
        public virtual ICollection<tbl_POReturnDetails> tbl_POReturnDetails { get; set; }
        public virtual tbl_Branch tbl_Branch { get; set; }
        public virtual tbl_PurchaseOrder tbl_PurchaseOrder { get; set; }

    }       
}