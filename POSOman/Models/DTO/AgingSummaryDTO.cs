﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class AgingSummaryDTO
    {
        public int orderID { get; set; }
        public string customerName { get; set; }
        public decimal totalAmount { get; set; }
        public string rangeAndInterval { get; set; }
        public List<IntervalAndRangeBasedStartAndEndDayDTO> rangeAndIntervalDataList { get; set; }
    }

    public class IntervalAndRangeBasedStartAndEndDayDTO
    {
        public int? startDay { get; set; }
        public int? endDay { get; set; }
    }
}