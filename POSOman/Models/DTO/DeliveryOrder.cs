﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class DeliveryOrder
    {
        public int OrderID { get; set; }
        public string DOID { get; set; }
        public Nullable<int> SaleOrderID { get; set; }
        public int BranchID { get; set; }
        public int AccountID { get; set; }
        public string TmpID { get; set; }
        public string PONo { get; set; }
        public string VehicleNo { get; set; }
        public string InvoiceNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> SalesDate { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<int> PaymentTypeID { get; set; }
        public Nullable<decimal> ReturnAmount { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> AddBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<System.DateTime> UpdateBy { get; set; }
        public Nullable<bool> IsReturned { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsPaid { get; set; }
        public string Status { get; set; }
        public string UserID { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }

        public virtual tbl_AccountDetails tbl_AccountDetails { get; set; }
        public virtual tbl_Branch tbl_Branch { get; set; }
        public virtual tbl_PaymentTypes tbl_PaymentTypes { get; set; }
        public virtual tbl_SalesOrder tbl_SalesOrder { get; set; }                      
        public virtual ICollection<tbl_DOReturn> tbl_DOReturn { get; set; }
        public virtual ICollection<DODetails> DODetails { get; set; }
        
    }
    public partial class DODetails
    {
        public int DetailID { get; set; }
        public int OrderID { get; set; }
        public int ProductID { get; set; }
        public int Qty { get; set; }
        public Nullable<int> ReturnedQty { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public string IsDeleted { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<bool> IsReturned { get; set; }

        public virtual tbl_Product tbl_Product { get; set; }
        public virtual DeliveryOrder DeliveryOrder { get; set; }
    }
}