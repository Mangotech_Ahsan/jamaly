﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class NotificationModel
    {
        public string msgTitle { get; set; }
        public string msg { get; set; }
        public string msgType { get; set; }
    }
}