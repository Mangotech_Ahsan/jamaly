﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class Product
    {
        public int ProductID { get; set; }
        public string PartNo { get; set; }
        public string BarCode { get; set; }
        public string Description { get; set; }
        public Nullable<int> VehicleCodeID { get; set; }
        public Nullable<decimal> SaleRate { get; set; }
    }
}