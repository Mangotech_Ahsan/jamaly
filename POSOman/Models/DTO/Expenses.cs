﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class Expenses
    {
        public int BranchID { get; set; }
        public int? DepartmentID { get; set; }
        public string VoucherName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public int RefId { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime VoucherDate { get; set; }
        public int PaymentTypeID { get; set; }
        public string BankName { get; set; }
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ChequeDate { get; set; }
        public string ChequeNumber { get; set; }
        public int AccountID { get; set; }
        public string UserID { get; set; }   
        public int SaleOrderID { get; set; }
    }
}