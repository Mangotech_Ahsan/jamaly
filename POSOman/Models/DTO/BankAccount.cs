﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class BankAccount
    {
        public int AccountTypeID { get; set; }
        public string AccountName { get; set; }
        public string AccountCode { get; set; }
        public string BankAccountNo { get; set; }
        public string Bank { get; set; }
        public Nullable<decimal> OpeningBalance { get; set; }
        public Nullable<System.DateTime> ClosingDate { get; set; }
        public Nullable<System.DateTime> OpeningDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> Addby { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> DeleteOn { get; set; }
        public Nullable<int> DeleteBy { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsOperating { get; set; }
        public Nullable<int> BranchID { get; set; }
        public string UserID { get; set; }

        public virtual tbl_AccountType tbl_AccountType { get; set; }
    }
}