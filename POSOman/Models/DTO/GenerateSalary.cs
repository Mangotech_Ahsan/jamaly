﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class GenerateSalary
    {
        public Nullable<int> PaymentAccountID { get; set; }
        public Nullable<int> EmployeeID { get; set; }
        public Nullable<int> BranchID { get; set; }        
        public Nullable<int> Addby { get; set; }
        [NotMapped]
        public int? NoOfDays { get; set; }
        public Nullable<decimal> TotalDeduction { get; set; }
        public Nullable<decimal> TotalOvertime { get; set; }
        public Nullable<decimal> TotalSalary { get; set; }
        public Nullable<System.DateTime> VoucherDate { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public string UserID { get; set; }
        public Nullable<System.DateTime> SalaryMonth { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
    }
}