﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class PaymentLog
    {
        public int LogID { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<decimal> Received { get; set; }
        public Nullable<decimal> Paid { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> JEntryID { get; set; }
        public Nullable<int> OrderID { get; set; }
        public Nullable<int> JEntryTypeID { get; set; }
        public string UserReferenceID { get; set; }
        public string Detail { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> AddBy { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual tbl_Branch tbl_Branch { get; set; }
        public virtual tbl_JEntry tbl_JEntry { get; set; }
        public virtual tbl_JEntryType tbl_JEntryType { get; set; }
    }
}