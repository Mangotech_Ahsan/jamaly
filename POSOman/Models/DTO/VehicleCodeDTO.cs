﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class VehicleCodeDTO
    {
        public int VehicleCodeID { get; set; }
        public int? LevelID { get; set; }
        public string VehicleCode { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int? HeadID { get; set; }
        [NotMapped]
        public bool IsSubCategory { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> Addby { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> DeleteOn { get; set; }
        public Nullable<int> DeleteBy { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> BranchID { get; set; }
        public string UserID { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }
        public Nullable<int> Code { get; set; }
        public string VCode { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual ICollection<tbl_Product> tbl_Product { get; set; }
    }
}