﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class DragDropDTO
    {
        public int OrderID { get; set; }
        public string CustomerPO { get; set; }
        public string Customer { get; set; }
        public int? ProductID { get; set; }
        public int Qty { get; set; }
        public string ProductName { get; set; }
        public int? MachineId { get; set; }
        public int? SaleDetailId { get; set; }
        public int? ItemRank { get; set; }
        public string MachineName { get; set; }
    }

    public class MachinesDTO
    {
        public int? MachineId { get; set; }
        public string MachineName { get; set; }
    }

}