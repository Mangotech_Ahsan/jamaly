﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class PurchaseOrder
    {
        public int OrderID { get; set; }
        public int POID { get; set; }
        public string Barcode { get; set; }
        public string QRCode { get; set; }
        public int AccountID { get; set; }
        public string ReferenceNo { get; set; }
        public string InvoiceNo { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> PurchaseDate { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<int> PaymentTypeID { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> Expenses { get; set; }
        public string PurchaseCode { get; set; }
        public string Currency { get; set; }
        public Nullable<decimal> ExchangeRate { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<decimal> DiscountPercent { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> AddBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<bool> IsReturned { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public Nullable<bool> IsPaid { get; set; }
        public string Status { get; set; }
        public Nullable<decimal> AmountPaid { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public Nullable<decimal> ReturnAmount { get; set; }
        public Nullable<int> BranchID { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public string UserID { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }
        public Nullable<bool> isOpening { get; set; }
        public Nullable<decimal> Adjustment { get; set; }
        public Nullable<bool> Can_Modify { get; set; }
        public virtual tbl_PaymentTypes tbl_PaymentTypes { get; set; }        
        public virtual ICollection<PODetails> PODetails { get; set; }
    }
    public partial class PODetails
    {
        public int DetailID { get; set; }
        public int OrderID { get; set; }
        public string PartNo { get; set; }
        public int ProductID { get; set; }
        public decimal Qty { get; set; }
        public Nullable<int> UnitPerCarton { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public string UnitCode { get; set; }
        public Nullable<bool> IsPack { get; set; }
        public Nullable<int> LevelID { get; set; }
        public Nullable<bool> IsOpen { get; set; }
        public Nullable<bool> IsMinor { get; set; }
        public Nullable<decimal> MinorDivisor { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<decimal> ExchangeRate { get; set; }
        public Nullable<decimal> DiscountPercent { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<int> ReturnedQty { get; set; }
        public Nullable<bool> IsReturned { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }
    }
}
