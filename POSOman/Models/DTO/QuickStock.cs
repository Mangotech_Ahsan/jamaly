﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class QuickStock
    {
        public int StockID { get; set; }
        public int ProductID { get; set; }
        public Nullable<int> BranchID { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public string VehicleCode { get; set; }
        public string VehicleModel { get; set; }
        public string GroupName { get; set; }
        public string BranchName { get; set; }
        public string Location { get; set; }
        public int Qty { get; set; }
        public Nullable<decimal> CostPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public string Cabinet { get; set; }
        public string RackNO { get; set; }
        public Nullable<System.DateTime> Addon { get; set; }
        public Nullable<int> AddBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<int> OnMove { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }

        public virtual tbl_Product tbl_Product { get; set; }
        public virtual tbl_Branch tbl_Branch { get; set; }
    }
}