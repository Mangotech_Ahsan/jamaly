﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class StockMoving
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        public int StockBatchID { get; set; }
        public int FromBranchID { get; set; }
        public int ToBranchID { get; set; }
        public Nullable<decimal> StockMoved { get; set; }
        public Nullable<decimal> StockReceived { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Location { get; set; }
        public string Detail { get; set; }
        public string UnitCode { get; set; }
        public Nullable<bool> IsPack { get; set; }
        public Nullable<bool> IsOpen { get; set; }
        public Nullable<bool> IsMinor { get; set; }
        public Nullable<int> UnitID { get; set; }
        public Nullable<int> LevelID { get; set; }
        public Nullable<decimal> MinorDivisor { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> AddBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> isReceived { get; set; }
        public Nullable<int> ReceivedBy { get; set; }
        public Nullable<System.DateTime> ReceivedOn { get; set; }
        public Nullable<decimal> CostPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public string UserID { get; set; }
        public Nullable<int> BranchMovingID { get; set; }
        public Nullable<int> BranchID { get; set; }
    }
}