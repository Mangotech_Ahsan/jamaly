﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class AccountBalances
    {
        public string AccountName { get; set; }
        public Nullable<decimal> Balance { get; set; }
    }
}