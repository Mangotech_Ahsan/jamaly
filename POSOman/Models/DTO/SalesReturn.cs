﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class SalesReturn
    {
        public int SalesReturnID { get; set; }
        public int BranchID { get; set; }
        [Required]
        public int AccountID { get; set; }
        public string Barcode { get; set; }
        public Nullable<int> SaleOrderID { get; set; }
        public string VehicleNo { get; set; }
        public string InvoiceNo { get; set; }
        public Nullable<decimal> COGS { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ReturnDate { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<int> PaymentTypeID { get; set; }
        [Required]
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<decimal> FinalAmount { get; set; }
        public Nullable<decimal> AmountPaid { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<decimal> DiscountPercent { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> AddBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<System.DateTime> UpdateBy { get; set; }
        public Nullable<bool> IsReturned { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsPaid { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public string Reason { get; set; }
        public string UserID { get; set; }

        public virtual ICollection<SOReturnDetails> SOReturnDetails { get; set; }
    }
    public partial class SOReturnDetails
    {
        public int DetailID { get; set; }
        public int ReturnID { get; set; }
        [Required]
        public int ProductID { get; set; }
        [Required]
        public decimal Qty { get; set; }
        public Nullable<int> UnitPerCarton { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public string UnitCode { get; set; }
        public Nullable<bool> IsPack { get; set; }
        public Nullable<int> LevelID { get; set; }
        public Nullable<int> UnitID { get; set; }
        public Nullable<bool> IsOpen { get; set; }
        public Nullable<bool> IsMinor { get; set; }
        public Nullable<decimal> MinorDivisor { get; set; }
        [Required]
        public Nullable<decimal> SalePrice { get; set; }
        [Required]
        public Nullable<decimal> Total { get; set; }
        public Nullable<decimal> DiscountPercent { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<int> ReturnedQty { get; set; }
        public Nullable<bool> IsReturned { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string Reason { get; set; }        
    }
}
