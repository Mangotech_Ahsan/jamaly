﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class JournalEntry
    {
        public int JEntryId { get; set; }
        public Nullable<int> BranchID { get; set; }
        public string VoucherName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> ThroughGJ { get; set; }

        public Nullable<long> CodeInt { get; set; }
        public string CodeString { get; set; }
        public string UserName { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public Nullable<int> RefID { get; set; }

        public Nullable<System.DateTime> VoucherDate { get; set; }
        public Nullable<bool> IsMultiDateEntry { get; set; }
        [NotMapped]
        public Nullable<System.DateTime> EntryDate { get; set; }
        public string ReferenceDetail { get; set; }
        public string PaymentType { get; set; }
        public string BankName { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public string ChequeNumber { get; set; }
        public Nullable<int> AddBy { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> DeleteOn { get; set; }
        public Nullable<int> DeleteBy { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string UserID { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        public virtual ICollection<JDetail> JDetail { get; set; }

    }
    public partial class JDetail
    {
        public int JDetailID { get; set; }
        public int JEntryID { get; set; }

        public Nullable<bool> IsMultiDateEntry { get; set; }
        public Nullable<System.DateTime> MultiEntryDate { get; set; }
        public Nullable<bool> ThorughGJ { get; set; }
        public Nullable<int> AccountID { get; set; }
        public Nullable<decimal> Dr { get; set; }
        public Nullable<decimal> Cr { get; set; }
        public Nullable<long> CodeInt { get; set; }
        public string CodeString { get; set; }
        public string UserName { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public string Memo { get; set; }
        public Nullable<int> RefAccountID { get; set; }
        public string Detail { get; set; }
        public Nullable<int> EntryTypeID { get; set; }
        public string UserReferenceID { get; set; }
        public Nullable<int> OrderID { get; set; }
        public Nullable<bool> isVendorPaid { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }

        public virtual tbl_JEntry tbl_JEntry { get; set; }
        public virtual tbl_JEntryType tbl_JEntryType { get; set; }
        public virtual tbl_AccountDetails tbl_AccountDetails { get; set; }
    }
}