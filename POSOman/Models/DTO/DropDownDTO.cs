﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class DropDownDTO
    {
        public string Name { get; set; }
        public int? Value { get; set; }
    }
}