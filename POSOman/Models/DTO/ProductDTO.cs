﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class ProductDTO
    {
        public ProductDTO()
        {
            this.tbl_DODetails = new HashSet<tbl_DODetails>();
            this.tbl_DOReturnDetails = new HashSet<tbl_DOReturnDetails>();
            this.tbl_PrevSOReturnDetails = new HashSet<tbl_PrevSOReturnDetails>();
            this.tbl_QuoteDetails = new HashSet<tbl_QuoteDetails>();
            this.tbl_SOReturnDetails = new HashSet<tbl_SOReturnDetails>();
            this.tbl_Stock = new HashSet<tbl_Stock>();
            this.tbl_StockBatch = new HashSet<tbl_StockBatch>();
            this.tbl_StockLog = new HashSet<tbl_StockLog>();
            this.tbl_StockMoving = new HashSet<tbl_StockMoving>();
            this.tbl_PODetails = new HashSet<tbl_PODetails>();
            this.tbl_POReturnDetails = new HashSet<tbl_POReturnDetails>();
            this.tbl_SaleDetails = new HashSet<tbl_SaleDetails>();
            this.tmp_OrderDetails = new HashSet<tmp_OrderDetails>();
            this.tbl_OnlineOrderDetail = new HashSet<tbl_OnlineOrderDetail>();
            this.tbl_ProductDetail = new HashSet<tbl_ProductDetail>();
        }

        public int ProductID { get; set; }
        public int? DepartmentID { get; set; }
        [NotMapped]
        public int? SubCategoryID { get; set; }
        [NotMapped]
        public int? SectionID { get; set; }
        public string PartNo { get; set; }
        public string ProductNameUr { get; set; }
        public string BarCode { get; set; }
        public string Description { get; set; }
        public Nullable<int> Code { get; set; }
        public string Product_Code { get; set; }
        public Nullable<int> UnitCodeID { get; set; }
        public Nullable<int> VehicleCodeID { get; set; }
        public Nullable<int> QtyPerUnit { get; set; }
        public Nullable<int> UnitPerCarton { get; set; }
        public string UnitCode { get; set; }
        public string OpenUnitCode { get; set; }
        public string LeastUnitCode { get; set; }
        public bool IsPacket { get; set; }
        public bool IsGeneralItem { get; set; }
        public bool IsOffsetItem { get; set; }
        public Nullable<int> LevelID { get; set; }
        public Nullable<bool> IsMinor { get; set; }
        public Nullable<decimal> MinorDivisor { get; set; }
        public Nullable<decimal> SaleRate { get; set; }
        public string Location { get; set; }
        public Nullable<int> isActive { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> Addby { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> DeleteOn { get; set; }
        public Nullable<int> DeleteBy { get; set; }
        public Nullable<int> BranchID { get; set; }
        public string UserID { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }
        public Nullable<int> OpeningStock { get; set; }
        public Nullable<decimal> CostPrice { get; set; }
        public string Country { get; set; }
        public string Expiry { get; set; }
        public Nullable<int> BrandID { get; set; }
        public Nullable<decimal> OnlinePrice { get; set; }
        public Nullable<decimal> DiscountPercent { get; set; }
        public Nullable<bool> IsAvailable { get; set; }
        public string ImageUrl { get; set; }
        public int? ProductTypeID { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual ICollection<tbl_DODetails> tbl_DODetails { get; set; }
        public virtual ICollection<tbl_DOReturnDetails> tbl_DOReturnDetails { get; set; }
        public virtual ICollection<tbl_PrevSOReturnDetails> tbl_PrevSOReturnDetails { get; set; }
        public virtual ICollection<tbl_QuoteDetails> tbl_QuoteDetails { get; set; }
        public virtual ICollection<tbl_SOReturnDetails> tbl_SOReturnDetails { get; set; }
        public virtual ICollection<tbl_Stock> tbl_Stock { get; set; }
        public virtual ICollection<tbl_StockBatch> tbl_StockBatch { get; set; }
        public virtual ICollection<tbl_StockLog> tbl_StockLog { get; set; }
        public virtual ICollection<tbl_StockMoving> tbl_StockMoving { get; set; }
        public virtual ICollection<tbl_PODetails> tbl_PODetails { get; set; }
         public virtual ICollection<tbl_POReturnDetails> tbl_POReturnDetails { get; set; }
        public virtual ICollection<tbl_SaleDetails> tbl_SaleDetails { get; set; }
        public virtual ICollection<tmp_OrderDetails> tmp_OrderDetails { get; set; }
        public virtual ICollection<tbl_OnlineOrderDetail> tbl_OnlineOrderDetail { get; set; }
         public virtual ICollection<tbl_ProductDetail> tbl_ProductDetail { get; set; }
        public virtual tbl_Brand tbl_Brand { get; set; }
        public virtual tbl_VehicleCode tbl_VehicleCode { get; set; }
    }
}