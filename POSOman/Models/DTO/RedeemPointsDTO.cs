﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class RedeemPointsDTO
    {
        public int LoyaltyCardID { get; set; }
        public string LoyaltyCardNo { get; set; }
        public string BarCode { get; set; }
        public Nullable<decimal> TotalPoints { get; set; }
        public Nullable<decimal> UsedPoints { get; set; }
        public Nullable<decimal> RedeemPoints { get; set; }
    }
}