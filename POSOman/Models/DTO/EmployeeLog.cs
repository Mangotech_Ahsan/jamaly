﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class EmployeeLog
    {
        public int EmployeeLogID { get; set; }
        public int EmployeeID { get; set; }
        public Nullable<int> JEntryID { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> Dr { get; set; }
        public Nullable<decimal> Cr { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> JentryTypeID { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int? PayType { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> Addby { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> DeleteOn { get; set; }
        public Nullable<int> DeleteBy { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }
        public virtual tbl_Branch tbl_Branch { get; set; }
        public virtual tbl_JEntry tbl_JEntry { get; set; }
        public virtual tbl_Employee tbl_Employee { get; set; }
    }
}