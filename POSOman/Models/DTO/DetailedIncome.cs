﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class DetailedIncome
    {
        public Nullable<decimal> Sales { get; set; }
        public Nullable<decimal> COGS { get; set; }
        public Nullable<decimal> GrossProfit { get; set; }
        public Nullable<decimal> NetProfit { get; set; }
        public string AccountName { get; set; }
        public Nullable<decimal> TotalExpense { get; set; }
        public Nullable<decimal> AllExpenses { get; set; }
        public Nullable<int> AccountID { get; set; }
        public Nullable<decimal> AdjustmentBalance { get; set; }
    }
}