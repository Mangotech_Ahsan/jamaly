﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class StkIssuance
    {
        public int ID { get; set; }
        public int RawProductID { get; set; }
        public string RawProduct { get; set; }
        public string RawProductList { get; set; }
        public string RawProductUnitCodeList { get; set; }
        public string Department { get; set; }
        public string Note { get; set; }
        public decimal RawProductPrice { get; set; }
        public decimal EstimatedPrice { get; set; }
        public decimal ActualPrice { get; set; }
        public int ItemID { get; set; }
        public decimal? RawProductQty { get; set; }
        public int OrderID { get; set; }
        public string ItemName { get; set; }
        public DateTime? StkIssuanceDate { get; set; }
        public string RawStkIssuanceDate { get; set; }
        public string Machine { get; set; }
        public string Category { get; set; }
        public string UnitCode { get; set; }
        public decimal Qty { get; set; }
        public decimal CostPrice { get; set; }
        public decimal TotalCostPrice { get; set; }
    }
}