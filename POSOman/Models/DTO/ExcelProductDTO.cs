﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class ExcelProductDTO
    {
        public string Current_Name { get; set; }
        public string Description { get; set; }
        public decimal Qty { get; set; }
        public int? RowID { get; set; }
        public string Id { get; set; }

    }

    public class ExcelProductNameDTO
    {
        public string Id { get; set; }
        public string Prev_Name { get; set; }
        public string Current_Name { get; set; }
        public int? RowID { get; set; }


    }
}