﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class ItemWisePackageDTO
    {
        public int RowID { get; set; }
        public int ItemID { get; set; }
        public int OrderID { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public int DeliverQty { get; set; }
        public int DeliveredQty { get; set; }
        public string PackageId { get; set; }
    }
}