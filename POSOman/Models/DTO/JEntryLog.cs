﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class JEntryLog
    {
        public int JLogID { get; set; }
        public int JEntryID { get; set; }
        public List<int> JEntryIDs { get; set; }
        public int OrderID { get; set; }
        public Nullable<int> EntryTypeID { get; set; }
        public decimal Amount { get; set; }
        public int OrderTypeID { get; set; }
        public int BranchID { get; set; }
        public Nullable<bool> isReversed { get; set; }
        public string MultiInvoiceOrderIDs { get; set; }

        public virtual tbl_Branch tbl_Branch { get; set; }
        public virtual tbl_JEntry tbl_JEntry { get; set; }
        public virtual tbl_OrderType tbl_OrderType { get; set; }
    }
}