﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class IncomeStatement
    {
        public Revenue Revenue { get; set; }
        public IEnumerable<ExpensesTrialBalance> ExpensesTrialBalance { get; set; }
    }
    public class Revenue
    {
        public Nullable<decimal> Sales { get; set; }
        public Nullable<decimal> COGS { get; set; }
        public Nullable<decimal> NetProfit { get; set; }
        public Nullable<decimal> Expenses { get; set; }
        public Nullable<decimal> GrossProfit { get; set; }
    }
    public class ExpensesTrialBalance
    {
        public int AccountTypeID { get; set; }
        public Nullable<int> AccountID { get; set; }
        public string AccountName { get; set; }
        public Nullable<decimal> Balance { get; set; }
        public Nullable<decimal> TotalDebit { get; set; }
        public Nullable<decimal> TotalCredit { get; set; }
        public Nullable<int> HeadID { get; set; }
    }
}