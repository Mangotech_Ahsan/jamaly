﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    using System;
    using System.Collections.Generic;
    public partial class StockLogDetails
    {

        public string Name { get; set; }
        public string PartNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> StockIN { get; set; }
        public Nullable<decimal> StockOut { get; set; }
        public string QtyBalance { get; set; }
        public Nullable<decimal> CostPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public string UnitCode { get; set; }
        public Nullable<bool> IsPack { get; set; }
        public Nullable<bool> IsOpen { get; set; }
        public Nullable<bool> IsMinor { get; set; }
        public Nullable<int> UnitID { get; set; }
        public Nullable<int> LevelID { get; set; }
        public Nullable<decimal> MinorDivisor { get; set; }
        public string Location { get; set; }
        public string OrderType { get; set; }
        public Nullable<int> OrderID { get; set; }
        public Nullable<int> OrderTypeID { get; set; }
        public Nullable<int> UnitPerCarton { get; set; }

    }
}