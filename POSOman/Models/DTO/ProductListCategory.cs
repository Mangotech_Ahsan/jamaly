﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class ProductListCategory
    {
        public int ProductID { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public string BarCode { get; set; }
        public string Brand { get; set; }
        public string Head { get; set; }
        public string SubCategory { get; set; }
        public string Section { get; set; }
        public Nullable<int> VehicleCodeID { get; set; }
        public string UnitCode { get; set; }
        public Nullable<decimal> SaleRate { get; set; }
        public Nullable<int> isActive { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> Addby { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> DeleteOn { get; set; }
        public Nullable<int> DeleteBy { get; set; }
        public Nullable<int> BranchID { get; set; }
        public string UserID { get; set; }
        public string ImageUrl { get; set; }
        public virtual tbl_VehicleCode tbl_VehicleCode { get; set; }
    }
}