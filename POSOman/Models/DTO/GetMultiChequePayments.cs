﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class GetMultiChequePayments
    {
        public int ID { get; set; }
        public int? PayTypeID { get; set; }
        public int? BankID { get; set; }
        public Nullable<DateTime> chqDate { get; set; }
        public string chqNumber { get; set; }
        public Nullable<decimal> Amount { get; set; }
    }
}