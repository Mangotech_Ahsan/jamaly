﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class BankTransfer
    {
        public int BranchID { get; set; }
        public string VoucherName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public string UserReferenceID { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime VoucherDate { get; set; }                
        public int FromBankID { get; set; }
        public int ToBankID { get; set; }
        public int AddBy { get; set; }
        public string UserID { get; set; }
    }
}