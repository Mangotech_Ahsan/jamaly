﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class VehicleModel
    {
        [Required]
        public string VehicleName { get; set; }
        public string Description { get; set; }
        public int BranchID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> Addby { get; set; }
        public string UserID { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }
    }
}