﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class Customer
    {
        public int CustomerID { get; set; }
        public Nullable<int> AccountID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Cell { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Company { get; set; }
        public Nullable<decimal> CreditLimit { get; set; }
        public string CRNo { get; set; }
        public Nullable<decimal> OpeningBalance { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> Addby { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> DeleteOn { get; set; }
        public Nullable<int> DeleteBy { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> BranchID { get; set; }
        public string UserID { get; set; }       
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual tbl_AccountDetails tbl_AccountDetails { get; set; }
        
        public Nullable<System.Guid> rowguid { get; set; }
    }
}