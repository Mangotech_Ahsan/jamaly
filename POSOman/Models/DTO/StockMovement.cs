﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class StockMovement
    {                            
        public string VehicleCode { get; set; }        
        public string Group { get; set; }
        public string VehicleModel { get; set; }
        public string PartNo { get; set; }       
        public string Description { get; set; }
        public string UnitCode { get; set; }
        public string Branch { get; set; }
        public Nullable<int> Qty { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<int> MoveQty { get; set; }
        public Nullable<decimal> CostPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public string Location { get; set; }
        public string UserID { get; set; }
        
    }
}