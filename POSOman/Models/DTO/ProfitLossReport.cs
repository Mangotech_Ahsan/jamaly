﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class ProfitLossReport
    {
        public List<PLSData> plsReport;
    }
    public class PLSData
    {
        public string HeadAccountType { get; set; }
        public string AccountType { get; set; }
        public string Particulars { get; set; }
        public decimal Total { get; set; }
        public string TotalInString { get; set; }
    }
}