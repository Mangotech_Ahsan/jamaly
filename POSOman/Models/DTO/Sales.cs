﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POSOman.Models;
using System.ComponentModel.DataAnnotations;

namespace POSOman.Models.DTO
{
    public class Sales
    {
        public int OrderID { get; set; }
        public int CompanyIDLogo { get; set; }
       // [Required]
        public int SOID { get; set; }
        public int? SalePersonAccID { get; set; }
        public string Barcode { get; set; }
        public string TaxName { get; set; }
        public bool IsOffsetOrder { get; set; }
        public int? OffsetTypeID { get; set; }

        public int BranchID { get; set; }
        [Required]
        public int AccountID { get; set; }
        public string QuoteID { get; set; }
        public int OnlineOrderID { get; set; }
        public int SaleTypeID { get; set; }
        public string HoldID { get; set; }
        public string TmpID { get; set; }
        public string CustomerCode { get; set; }
        public string PONo { get; set; }
        public string VehicleNo { get; set; }
        public Nullable<int> CreditDays { get; set; }
        public string InvoiceNo { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> SalesDate { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<int> PaymentTypeID { get; set; }
        public string SalesCode { get; set; }
        public string Currency { get; set; }
        [Required]
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<decimal> TAX { get; set; }
        public string Comment { get; set; }

        public Nullable<decimal> ReturnAmount { get; set; }
        public Nullable<decimal> AmountPaid { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public Nullable<decimal> DiscountPercent { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<decimal> CashReceived { get; set; }
        public Nullable<decimal> CashReturned { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> AddBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<bool> IsReturned { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsPaid { get; set; }
        public string Status { get; set; }
        public string BankName { get; set; }
        public Nullable<decimal> COGS { get; set; }
        public Nullable<decimal> LoyaltyDiscount { get; set; }
        public string ChequeNo { get; set; }        
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public string UserID { get; set; }
        public Nullable<int> ChallanID { get; set; }
        public decimal MarkupRate { get; set; }
        public decimal MarkedupPrice { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }

        public virtual ICollection<SaleDetails> SaleDetails { get; set; }
    }
    public partial class SaleDetails
    {
        public int DetailID { get; set; }
        public int OrderID { get; set; }
        public string PartNo { get; set; }
        [Required]
        public int ProductID { get; set; }
        [Required]
        public decimal Qty { get; set; }
        public string ItemDescription { get; set; }
        public Nullable<int> UnitPerCarton { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        [Required]
        public Nullable<decimal> SalePrice { get; set; }
        [Required]
        public Nullable<decimal> Total { get; set; }
        public string UnitCode { get; set; }
        public Nullable<bool> IsPack { get; set; }
        public Nullable<int> LevelID { get; set; }
        public Nullable<int> UnitID { get; set; }
        public Nullable<int> Packet { get; set; }
        public Nullable<bool> IsOpen { get; set; }
        public Nullable<bool> IsMinor { get; set; }
        public Nullable<decimal> MinorDivisor { get; set; }
        public Nullable<decimal> ExchangeRate { get; set; }
        public Nullable<decimal> DiscountPercent { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<decimal> ReturnedQty { get; set; }
        public Nullable<bool> IsReturned { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<int> CardQuality { get; set; }
        public decimal Length { get; set; }
        public decimal Width { get; set; }
        public decimal CardLength { get; set; }
        public decimal CardWidth { get; set; }
        public decimal CutLength { get; set; }
        public decimal CutWidth { get; set; }
        public decimal Upping { get; set; }
        public decimal RatePerKG { get; set; }
        public decimal CardGSM { get; set; }
        public int NoOfSheets { get; set; }
        public decimal NOOfImpressions { get; set; }
        public int Wastage { get; set; }
        public int TotalSheets { get; set; }
        public decimal PerSheetCost { get; set; }
        public decimal TotalSheetCost { get; set; }
        public int? RollID { get; set; }
        public decimal RollLength { get; set; }
        public decimal Split { get; set; }
        public int? SplinterID { get; set; }
        public decimal HSheetSize { get; set; }
        public decimal WSheetSize { get; set; }
        public decimal SplinterCount { get; set; }
        public decimal TotalWinInches { get; set; }
        public decimal TotalMeters { get; set; }
        public decimal MaterialCostPerMeter { get; set; }
        public decimal TotalMaterialCost { get; set; }
        public Nullable<int> MachineID { get; set; }
        public decimal GroundColor { get; set; }
        public decimal GroundCost { get; set; }
        public decimal TextColor { get; set; }
        public decimal TextCost { get; set; }
        public decimal NoOfPlates { get; set; }
        public decimal PlateCost { get; set; }
        public decimal PrintingCost { get; set; }
        public Nullable<int> LaminationTypeID { get; set; }
        public decimal LaminationCost { get; set; }
        public decimal DieCost { get; set; }
        public decimal DieCuttingCost { get; set; }
        public decimal UVCost { get; set; }
        public decimal SlittingCost { get; set; }
        public decimal DesignCost { get; set; }
        public decimal FoilPrintingCost { get; set; }
        public decimal FinalCostPerPc { get; set; }
    }
}
