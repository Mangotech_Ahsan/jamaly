﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class Quotation
    {
        public int OrderID { get; set; }
        public string QuoteID { get; set; }
        public string HoldID { get; set; }
        public string TmpID { get; set; }
        public int BranchID { get; set; }
        public int AccountID { get; set; }
        public int? CreditDays { get; set; }
        public string CustomerCode { get; set; }
        public string TaxName { get; set; }

        public string PONo { get; set; }
        public string VehicleNo { get; set; }
        public string InvoiceNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> SalesDate { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<int> PaymentTypeID { get; set; }
        public string SalesCode { get; set; }
        public string Currency { get; set; }
        public string Comment { get; set; }

        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<decimal> FinalAmount { get; set; }
        public Nullable<decimal> AmountPaid { get; set; }
        public Nullable<decimal> DiscountPercent { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
      public Nullable<decimal> Tax { get; set; }
         
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> AddBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<System.DateTime> UpdateBy { get; set; }
        public Nullable<bool> IsReturned { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsPaid { get; set; }
        public string Status { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<bool> IsQuote { get; set; }
        public Nullable<bool> IsUnsaved { get; set; }
        public Nullable<bool> IsHold { get; set; }
        public Nullable<bool> IsSubmit { get; set; }
        public string UserID { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }
        public virtual ICollection<QuoteDetails> QuoteDetails { get; set; }
    }
    public partial class QuoteDetails
    {
        public int DetailID { get; set; }
        public int OrderID { get; set; }
        public int BranchID { get; set; }
        public string PartNo { get; set; }
        public int ProductID { get; set; }
        public int Qty { get; set; }
        public Nullable<int> UnitPerCarton { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<decimal> ExchangeRate { get; set; }
        public Nullable<decimal> DiscountPercent { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<int> ReturnedQty { get; set; }
        public Nullable<bool> IsReturned { get; set; }
        public string IsDeleted { get; set; }
    }
}
