﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class StatementDTO
    {
        public int JEntryId { get; set; }
        public string Date { get; set; }
        public string Detail { get; set; }
        public string Description { get; set; }
        public string Dr { get; set; }
        public string Cr { get; set; }
        public string Balance { get; set; }
        public string OpeningBalance { get; set; }
        public string ClosingBalance { get; set; }
        public string AccName { get; set; }
        public string PayStatus { get; set; }
        public string Aging { get; set; }
        public List<Details> detailList { get; set; }
    }
    public class Details
    {
        public string Name { get; set; }
        public decimal Qty { get; set; }
        public decimal CostPrice { get; set; }
        public decimal Total { get; set; }
        public int? PID { get; set; }
        public int? OrderId { get; set; }
        public string InvoiceId { get; set; }
    }
}