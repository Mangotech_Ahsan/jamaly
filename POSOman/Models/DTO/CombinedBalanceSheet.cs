﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class CombinedBalanceSheet
    {
        public Nullable<decimal> CashSales { get; set; }
        public Nullable<decimal> CashSalesReturn { get; set; }
        public Nullable<decimal> SpecialDiscount { get; set; }
        public Nullable<decimal> SalesReceiving { get; set; }
        public Nullable<decimal> LoanPaid { get; set; }
        public Nullable<decimal> LoanReceiving { get; set; }
        public string AccountName { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public Nullable<decimal> Amount { get; set; }
    }
}
