﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class EmployeePurchaseDetail
    {
        public int DetailID { get; set; }
        public int OrderID { get; set; }
        public string Description { get; set; }
        public Nullable<int> ProductID { get; set; }
        public decimal Qty { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Total { get; set; }
        public bool IsProduct { get; set; }
        public int? ExpenseID { get; set; }
        public System.DateTime AddedOn { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> DeletedOn { get; set; }
    }
}