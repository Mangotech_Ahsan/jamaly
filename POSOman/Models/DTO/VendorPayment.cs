﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class VendorPayment
    {
        dbPOS db = new dbPOS();
        public List<GetVendorPayment_Result> GetVendorPayments(int AccountID,int? BranchID)
        {
            return db.GetVendorPayment(AccountID,BranchID).ToList();
        }
        public List<GetVendorPaymentDateWise_Result> GetVendorPaymentsDateWise(int? AccountID,DateTime? fromDate,DateTime? toDate,int? BranchId)
        {
            return db.GetVendorPaymentDateWise(AccountID,fromDate,toDate,BranchId).ToList();
        }
    }
}