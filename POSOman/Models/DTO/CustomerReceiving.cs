﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class CustomerReceiving
    {
        public string VoucherName { get; set; }
        public string Description { get; set; }
        public int? CustomerRefAccID { get; set; }
        public int? JEntryID { get; set; }

        public Nullable<decimal> TotalBalanceSettled { get; set; }
        public bool IsSettleEntry { get; set; }

        public bool IsActive { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public int RefId { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime VoucherDate { get; set; }
        public int PaymentTypeID { get; set; }
        public string BankName { get; set; }
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ChequeDate { get; set; }
        public string ChequeNumber { get; set; }
        public int AccountID { get; set; }
        public List<PaymentDetails> Details { get; set; }
        public string UserID { get; set; }
        public Nullable<int> BranchID { get; set; }
        public int OrderID { get; set; }
    }

    public class PaymentDetails
    {
        public int OrderID { get; set; }
        public decimal Receiving { get; set; }
        public decimal Balance { get; set; }
        public decimal Tax { get; set; }
        public decimal WHT { get; set; }
        public decimal TaxPercent { get; set; }
        public string TaxType { get; set; }
        public decimal Discount { get; set; }
        public decimal Adjustment { get; set; }
        public string AdjustmentDescription { get; set; }
    }
}
