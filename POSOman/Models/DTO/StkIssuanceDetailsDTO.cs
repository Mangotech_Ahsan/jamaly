﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class StkIssuanceDetailsDTO
    {
        public int OrderID { get; set; }
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public string CustomerName { get; set; }
        public decimal? ItemQty { get; set; }
        public decimal? ItemSalePrice { get; set; }
        public DateTime? IssueDate { get; set; }
        public List<StkIssuance> stkIssuedProducts { get; set; }

        public List<StkIssuance> rawProducts { get; set; }
    }
}