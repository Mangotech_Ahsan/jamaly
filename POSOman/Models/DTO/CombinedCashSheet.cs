﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class CombinedCashSheet
    {
        public DailyBalances DailyBalances { get; set; }
        public IEnumerable<DailyExpenses> DailyExpenses { get; set; }
        public IEnumerable<DailyReceiving> DailyReceiving { get; set; }

    }
    public class DailyBalances
    {
        public Nullable<decimal> PreviousCashSalesReturn { get; set; }
        public Nullable<decimal> CashSales { get; set; }
        public Nullable<decimal> CashSalesReturn { get; set; }
        public Nullable<decimal> SpecialDiscount { get; set; }
        public Nullable<decimal> SalesReceiving { get; set; }
        public Nullable<decimal> LoanPaid { get; set; }
        public Nullable<decimal> LoanReceiving { get; set; }
        public Nullable<decimal> Excess { get; set; }
        public Nullable<decimal> Short { get; set; }
        public Nullable<decimal> CashDeposit { get; set; }
        public Nullable<decimal> PreviousCash { get; set; }
        public Nullable<decimal> CashPurchases { get; set; }
        public Nullable<decimal> PurchaseExpensesPaid { get; set; }
        public Nullable<decimal> CashPurchasePayment { get; set; }
        public Nullable<decimal> CashSalaries { get; set; }
        public Nullable<decimal> CashLoansEmp { get; set; }
        public Nullable<decimal> TotalExpenses { get; set; }
        public Nullable<decimal> TotalReceivings { get; set; }
        public Nullable<decimal> TotalDepositToBank { get; set; }
        public Nullable<decimal> TotalCash { get; set; }
        

    }    
    public class DailyExpenses
    {
        public string AccountName { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public Nullable<decimal> Amount { get; set; }
    }
    public class DailyReceiving
    {
        public string AccountName { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public Nullable<decimal> Amount { get; set; }
    }
}