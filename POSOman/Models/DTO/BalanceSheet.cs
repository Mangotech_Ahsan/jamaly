﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    //public class BalanceSheet
    //{
    //    public Nullable<decimal> CashSales { get; set; }
    //    public Nullable<decimal> SalesReceiving { get; set; }
    //    public Nullable<decimal> GeneralExpenses { get; set; }
    //    public Nullable<decimal> UtilityBills { get; set; }
    //    public Nullable<decimal> GovtBills { get; set; }
    //    public Nullable<decimal> CustomBills { get; set; }
    //    public Nullable<decimal> FuelExpenses { get; set; }
    //    public Nullable<decimal> Discount { get; set; }
    //    public Nullable<decimal> TotalCash { get; set; }
    //    public Nullable<decimal> TotalExpenses { get; set; }
    //}

    public class BalanceSheet
    {
        public IEnumerable<AssetsBalance> AssetsBalance { get; set; }
        public IEnumerable<LiabilityBalance> LiabilityBalance { get; set; }
        public IEnumerable<EquityBalance> EquityBalance { get; set; }
    }
    public class AssetsBalance
    {
        public int AccountTypeID { get; set; }
        public Nullable<int> AccountID { get; set; }
        public string AccountName { get; set; }
        public Nullable<decimal> Balance { get; set; }
        public Nullable<decimal> TotalDebit { get; set; }
        public Nullable<decimal> TotalCredit { get; set; }
        public Nullable<int> HeadID { get; set; }
    }
    public class LiabilityBalance
    {
        public int AccountTypeID { get; set; }
        public Nullable<int> AccountID { get; set; }
        public string AccountName { get; set; }
        public Nullable<decimal> Balance { get; set; }
        public Nullable<decimal> TotalDebit { get; set; }
        public Nullable<decimal> TotalCredit { get; set; }
        public Nullable<int> HeadID { get; set; }
    }
    public class EquityBalance
    {
        public int AccountTypeID { get; set; }
        public Nullable<int> AccountID { get; set; }
        public string AccountName { get; set; }
        public Nullable<decimal> Balance { get; set; }
        public Nullable<decimal> TotalDebit { get; set; }
        public Nullable<decimal> TotalCredit { get; set; }
        public Nullable<int> HeadID { get; set; }
    }


    ///////////////////

    public class Asset
    {
        public int HeadID { get; set; }
        public int Id { get; set; }
        public string number { get; set; }
        public string name { get; set; }
        public string balance { get; set; }
        public List<Asset> assets { get; set; }
    }

    public class BalanceSheetFormat
    {
        public string number { get; set; }
        public string name { get; set; }
        public string balance { get; set; }
        public List<Asset> assets { get; set; }
    }


    ///

}