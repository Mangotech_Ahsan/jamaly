﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class EmployeePayment
    {
        dbPOS db = new dbPOS();
        
        public List<GetEmployeePaymentDateWise_Result> GetEmployeePaymentDateWise(int? AccountID, DateTime? fromDate, DateTime? toDate, int? BranchId)
        {
            return db.GetEmployeePaymentDateWise(AccountID, fromDate, toDate, BranchId).ToList();
        }
    }
}