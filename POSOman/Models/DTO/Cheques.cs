﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class Cheques
    {
        public string VoucherName { get; set; }
        public string BankName { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public string ChequeNumber { get; set; }
        public decimal Amount { get; set; }
    }
}