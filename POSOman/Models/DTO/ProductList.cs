﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class ProductList
    {
        public int ProductID { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public string BarCode { get; set; }
        public string VehicleCode { get; set; }
        public Nullable<int> VehicleCodeID { get; set; }
        public string UnitCode { get; set; }
        public Nullable<decimal> SaleRate { get; set; }
        public Nullable<int> isActive { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> Addby { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> DeleteOn { get; set; }
        public Nullable<int> DeleteBy { get; set; }
        public Nullable<int> BranchID { get; set; }
        public string UserID { get; set; }
        public string ImageUrl { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }

       
        public virtual tbl_VehicleCode tbl_VehicleCode { get; set; }
    }
}