﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class StockLog
    {
        // Used for New Sales 
        public int LogID { get; set; }
        public int ProductID { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<decimal> StockIN { get; set; }
        public Nullable<decimal> StockOut { get; set; }
        public Nullable<int> UnitPerCarton { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        public Nullable<decimal> CostPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public string Location { get; set; }
        public Nullable<int> UnitID { get; set; }
        public Nullable<int> OrderID { get; set; }
        public Nullable<int> OrderTypeID { get; set; }
        public string UserReferenceID { get; set; }
        public string InReference { get; set; }
        public Nullable<int> InReferenceID { get; set; }
        public string OutReference { get; set; }
        public Nullable<int> OutReferenceID { get; set; }
        public string Detail { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public Nullable<int> AddBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> AccountID { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }
        public string UserId { get; set; }
        public string UnitCode { get; set; }
        public Nullable<bool> IsPack { get; set; }
        public Nullable<bool> IsOpen { get; set; }
        public Nullable<bool> IsMinor { get; set; }
        public Nullable<int> LevelID { get; set; }
        public Nullable<decimal> MinorDivisor { get; set; }

        public virtual tbl_Branch tbl_Branch { get; set; }
        public virtual tbl_OrderType tbl_OrderType { get; set; }
        public virtual tbl_Product tbl_Product { get; set; }
    }
}