﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class StockPriceDTO
    {
        public int StockID { get; set; }
        public int ProductID { get; set; }      
        public string Location { get; set; }
        public string Detail { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }

        public Nullable<decimal> NewSalePrice { get; set; }
    }
}