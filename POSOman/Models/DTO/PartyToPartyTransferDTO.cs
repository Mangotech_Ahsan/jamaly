﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class PartyToPartyTransferDTO
    {
        public int BranchID { get; set; }
        public string VoucherName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public string UserReferenceID { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime VoucherDate { get; set; }
        public int FromBankID { get; set; }
        public int ToBankID { get; set; }
        public int AddBy { get; set; }
        public string UserID { get; set; }

        public string BankDetail { get; set; }
      
        public string SlipNumber { get; set; }
        public int RefId { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
       
        public int PaymentTypeID { get; set; }
        public string BankName { get; set; }
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ChequeDate { get; set; }
        public string ChequeNumber { get; set; }
        public int AccountID { get; set; }
        public List<PaymentDetails> Details { get; set; }
        
        public Nullable<decimal> MasterAmount { get; set; }
        public Nullable<decimal> MasterBalanceAmount { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
    }
}