﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.DTO
{
    public class JEntryLogModel
    {
        public string AccountName { get; set; }
        public int InvoiceNo { get; set; }
        public decimal Amount { get; set; }
    }
    
}