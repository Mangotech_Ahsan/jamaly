﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace POSOman.Models.BLL
{
    public class UpdateCostingModelBLL
    {
        dbPOS db = new dbPOS();
        public bool updateCosting(tbl_Product model) {

            try
            {
                using(var t = db.Database.BeginTransaction())
                {
                    //tbl_Product newModel = new tbl_Product();
                    var prevData = db.tbl_Product.Where(x => x.ProductID == model.ProductID).FirstOrDefault();
                    if (prevData != null)
                    {
                        model.PartNo = string.IsNullOrWhiteSpace(model.PartNo) == true ? prevData.PartNo : model.PartNo;
                        model.IsOffsetItem = model.IsOffsetItem == false ? prevData.IsOffsetItem : model.IsOffsetItem;
                        model.IsGeneralItem = model.IsGeneralItem == false ? prevData.IsGeneralItem : model.IsGeneralItem;
                        model.RawProductID = model.RawProductID == null ? prevData.RawProductID : model.RawProductID;
                        model.DepartmentID = model.DepartmentID == null ? prevData.DepartmentID : model.DepartmentID;
                        model.BarCode = string.IsNullOrWhiteSpace(model.BarCode) == true ? prevData.BarCode : model.BarCode;
                        model.UnitCodeID = model.UnitCodeID == null ? prevData.UnitCodeID : model.UnitCodeID;
                        model.VehicleCodeID = model.VehicleCodeID == null ? prevData.VehicleCodeID : model.VehicleCodeID;
                        model.QtyPerUnit = model.QtyPerUnit == null ? prevData.QtyPerUnit : model.QtyPerUnit;
                        model.UnitPerCarton = model.UnitPerCarton == null ? prevData.UnitPerCarton : model.UnitPerCarton;
                        model.UnitCode = string.IsNullOrWhiteSpace(model.UnitCode) == true ? prevData.UnitCode : model.UnitCode;
                        model.IsPacket = model.IsPacket == false ? prevData.IsPacket : model.IsPacket;
                        model.LevelID = model.LevelID == null ? prevData.LevelID : model.LevelID;
                        model.IsMinor = model.IsMinor == null ? prevData.IsMinor : model.IsMinor;
                        model.MinorDivisor = model.MinorDivisor == null ? prevData.MinorDivisor : model.MinorDivisor;
                        model.SaleRate = model.SaleRate == null ? prevData.SaleRate : model.SaleRate;
                        model.isActive = model.isActive == null ? prevData.isActive : model.isActive;
                        model.AddOn = model.AddOn == null ? prevData.AddOn : model.AddOn;
                        model.Addby = model.Addby == null ? prevData.Addby : model.Addby;
                        model.UpdateOn = model.UpdateOn == null ? prevData.UpdateOn : model.UpdateOn;
                        model.BranchID = model.BranchID == null ? prevData.BranchID : model.BranchID;
                        model.UserID = model.UserID == null ? prevData.UserID : model.UserID;
                        model.BrandID = model.BrandID == null ? prevData.BrandID : model.BrandID;
                        model.MarkupRate = model.MarkupRate <= 0 ? prevData.MarkupRate : model.MarkupRate;
                        model.MarkedupPrice = model.MarkedupPrice <= 0 ? prevData.MarkedupPrice : model.MarkedupPrice;
                        model.IsOffsetOrder = prevData.IsOffsetOrder;
                        model.OffsetTypeID = model.OffsetTypeID == null ? prevData.OffsetTypeID : model.OffsetTypeID;
                        model.CardQuality = model.CardQuality == null ? prevData.CardQuality : model.CardQuality;
                        model.Length = model.Length <= 0 ? prevData.Length : model.Length;
                        model.Width = model.Width <= 0 ? prevData.Width : model.Width;
                        model.CardLength = model.CardLength <= 0 ? prevData.CardLength : model.CardLength;
                        model.CardWidth = model.CardWidth <= 0 ? prevData.CardWidth : model.CardWidth;
                        model.CutLength = model.CutLength <= 0 ? prevData.CutLength : model.CutLength;
                        model.CutWidth = model.CutWidth <= 0 ? prevData.CutWidth : model.CutWidth;
                        model.Upping = model.Upping <= 0 ? prevData.Upping : model.Upping;
                        model.RatePerKG = model.RatePerKG <= 0 ? prevData.RatePerKG : model.RatePerKG;
                        model.CardGSM = model.CardGSM <= 0 ? prevData.CardGSM : model.CardGSM;
                        model.NoOfSheets = model.NoOfSheets <= 0 ? prevData.NoOfSheets : model.NoOfSheets;
                        model.NOOfImpressions = model.NOOfImpressions <= 0 ? prevData.NOOfImpressions : model.NOOfImpressions;
                        model.Wastage = model.Wastage <= 0 ? prevData.Wastage : model.Wastage;
                        model.RawSizeSheets = model.RawSizeSheets <= 0 ? prevData.RawSizeSheets : model.RawSizeSheets;
                        model.TotalSheets = model.TotalSheets <= 0 ? prevData.TotalSheets : model.TotalSheets;
                        model.PerSheetCost = model.PerSheetCost <= 0 ? prevData.PerSheetCost : model.PerSheetCost;
                        model.TotalSheetCost = model.TotalSheetCost <= 0 ? prevData.TotalSheetCost : model.TotalSheetCost;
                        model.MachineID = model.MachineID == null ? prevData.MachineID : model.MachineID;
                        model.GroundColor = model.GroundColor <= 0 ? prevData.GroundColor : model.GroundColor;
                        model.GroundCost = model.GroundCost <= 0 ? prevData.GroundCost : model.GroundCost;
                        model.TextColor = model.TextColor <= 0 ? prevData.TextColor : model.TextColor;
                        model.TextCost = model.TextCost <= 0 ? prevData.TextCost : model.TextCost;
                        model.NoOfPlates = model.NoOfPlates <= 0 ? prevData.NoOfPlates : model.NoOfPlates;
                        model.PlateCost = model.PlateCost <= 0 ? prevData.PlateCost : model.PlateCost;
                        model.PrintingCost = model.PrintingCost <= 0 ? prevData.PrintingCost : model.PrintingCost;
                        model.LaminationCost = model.LaminationCost <= 0 ? prevData.LaminationCost : model.LaminationCost;
                        model.DieCost = model.DieCost <= 0 ? prevData.DieCost : model.DieCost;
                        model.DieCuttingCost = model.DieCuttingCost <= 0 ? prevData.DieCuttingCost : model.DieCuttingCost;
                        model.UVCost = model.UVCost <= 0 ? prevData.UVCost : model.UVCost;
                        model.SlittingCost = model.SlittingCost <= 0 ? prevData.SlittingCost : model.SlittingCost;
                        model.DesignCost = model.DesignCost <= 0 ? prevData.DesignCost : model.DesignCost;
                        model.FoilPrintingCost = model.FoilPrintingCost <= 0 ? prevData.FoilPrintingCost : model.FoilPrintingCost;
                        model.EmbosingCost = model.EmbosingCost <= 0 ? prevData.EmbosingCost : model.EmbosingCost;
                        model.FinalCostPerPc = model.FinalCostPerPc <= 0 ? prevData.FinalCostPerPc : model.FinalCostPerPc;
                        model.RollID = model.RollID == null ? prevData.RollID : model.RollID;
                        model.RollLength = model.RollLength <= 0 ? prevData.RollLength : model.RollLength;
                        model.RollCost = model.RollCost <= 0 ? prevData.RollCost : model.RollCost;
                        model.Split = model.Split <= 0 ? prevData.Split : model.Split;
                        model.SplinterID = model.SplinterID == null ? prevData.SplinterID : model.SplinterID;
                        model.HSheetSize = model.HSheetSize <= 0 ? prevData.HSheetSize : model.HSheetSize;
                        model.WSheetSize = model.WSheetSize <= 0 ? prevData.WSheetSize : model.WSheetSize;
                        model.SplinterCount = model.SplinterCount <= 0 ? prevData.SplinterCount : model.SplinterCount;
                        model.TotalWinInches = model.TotalWinInches <= 0 ? prevData.TotalWinInches : model.TotalWinInches;
                        model.TotalMeters = model.TotalMeters <= 0 ? prevData.TotalMeters : model.TotalMeters;
                        model.MaterialCostPerMeter = model.MaterialCostPerMeter <= 0 ? prevData.MaterialCostPerMeter : model.MaterialCostPerMeter;
                        model.TotalMaterialCost = model.TotalMaterialCost <= 0 ? prevData.TotalMaterialCost : model.TotalMaterialCost;
                        model.CylinderID = model.CylinderID == null ? prevData.CylinderID : model.CylinderID;
                        model.PrintingMachineID = model.PrintingMachineID == null ? prevData.PrintingMachineID : model.PrintingMachineID;
                        model.PrintingRatePerRoll = model.PrintingRatePerRoll <= 0 ? prevData.PrintingRatePerRoll : model.PrintingRatePerRoll;
                        model.CylinderLength = model.CylinderLength <= 0 ? prevData.CylinderLength : model.CylinderLength;
                        model.AroundUps = model.AroundUps <= 0 ? prevData.AroundUps : model.AroundUps;
                        model.NoOfPrintingColors = model.NoOfPrintingColors <= 0 ? prevData.NoOfPrintingColors : model.NoOfPrintingColors;
                        model.TotalPcsInRoll = model.TotalPcsInRoll <= 0 ? prevData.TotalPcsInRoll : model.TotalPcsInRoll;
                        model.WastagePcsInRoll = model.WastagePcsInRoll <= 0 ? prevData.WastagePcsInRoll : model.WastagePcsInRoll;
                        model.TotalRequiredRolls = model.TotalRequiredRolls <= 0 ? prevData.TotalRequiredRolls : model.TotalRequiredRolls;
                        model.FinishingTypeID = model.FinishingTypeID == null ? prevData.FinishingTypeID : model.FinishingTypeID;
                        model.FinishingCost = model.FinishingCost <= 0 ? prevData.FinishingCost : model.FinishingCost;
                        model.BlockTypeID = model.BlockTypeID == null ? prevData.BlockTypeID : model.BlockTypeID;
                        model.BlockCost = model.BlockCost <= 0 ? prevData.BlockCost : model.BlockCost;
                        model.StickerMachineID = model.StickerMachineID == null ? prevData.StickerMachineID : model.StickerMachineID;
                        model.LSize = model.LSize <= 0 ? prevData.LSize : model.LSize;
                        model.WSize = model.WSize <= 0 ? prevData.WSize : model.WSize;
                        model.StickerCylinderID = model.StickerCylinderID == null ? prevData.StickerCylinderID : model.StickerCylinderID;
                        model.StickerMachineTypeID = model.StickerMachineTypeID == null ? prevData.StickerMachineTypeID : model.StickerMachineTypeID;
                        model.RepeatLength = model.RepeatLength <= 0 ? prevData.RepeatLength : model.RepeatLength;
                        model.ReelSize = model.ReelSize <= 0 ? prevData.ReelSize : model.ReelSize;
                        model.Across = model.Across <= 0 ? prevData.Across : model.Across;
                        model.RunningMeter = model.RunningMeter <= 0 ? prevData.RunningMeter : model.RunningMeter;
                        model.RequiredM2 = model.RequiredM2 <= 0 ? prevData.RequiredM2 : model.RequiredM2;
                        model.StickerQualityID = model.StickerQualityID == null ? prevData.StickerQualityID : model.StickerQualityID;
                        model.KGRequired = model.KGRequired <= 0 ? prevData.KGRequired : model.KGRequired;
                        model.StickerPrintingBlockCost = model.StickerPrintingBlockCost <= 0 ? prevData.StickerPrintingBlockCost : model.StickerPrintingBlockCost;
                        model.StickerDieCuttingCost = model.StickerDieCuttingCost <= 0 ? prevData.StickerDieCuttingCost : model.StickerDieCuttingCost;
                        model.StickerDieCutiingID = model.StickerDieCutiingID == null ? prevData.StickerDieCutiingID : model.StickerDieCutiingID;
                        model.DieBlock = model.DieBlock <= 0 ? prevData.DieBlock : model.DieBlock;
                        model.StickerRibbonID_T = model.StickerRibbonID_T == null ? prevData.StickerRibbonID_T : model.StickerRibbonID_T;
                        model.StickerRibbonTCost = model.StickerRibbonTCost <= 0 ? prevData.StickerRibbonTCost : model.StickerRibbonTCost;
                        model.StickerRibbonID_F = model.StickerRibbonID_F == null ? prevData.StickerRibbonID_F : model.StickerRibbonID_F;
                        model.StickerRibbonFCost = model.StickerRibbonFCost <= 0 ? prevData.StickerRibbonFCost : model.StickerRibbonFCost;
                        model.StickerRibbonID_S = model.StickerRibbonID_S == null ? prevData.StickerRibbonID_S : model.StickerRibbonID_S;
                        model.StickerRibbonSCost = model.StickerRibbonSCost <= 0 ? prevData.StickerRibbonSCost : model.StickerRibbonSCost;
                        model.StickerRibbonID_RFID = model.StickerRibbonID_RFID == null ? prevData.StickerRibbonID_RFID : model.StickerRibbonID_RFID;
                        model.StickerRibbonRFIDCost = model.StickerRibbonRFIDCost <= 0 ? prevData.StickerRibbonRFIDCost : model.StickerRibbonRFIDCost;
                        model.tr2PlateCost = model.tr2PlateCost <= 0 ? prevData.tr2PlateCost : model.tr2PlateCost;
                        model.EmbosingBlock = model.EmbosingBlock <= 0 ? prevData.EmbosingBlock : model.EmbosingBlock;
                        model.UVMattVarnish = model.UVMattVarnish <= 0 ? prevData.UVMattVarnish : model.UVMattVarnish;
                        model.UVGlossVarnish = model.UVGlossVarnish <= 0 ? prevData.UVGlossVarnish : model.UVGlossVarnish;
                        model.AQMattVarnish = model.AQMattVarnish <= 0 ? prevData.AQMattVarnish : model.AQMattVarnish;
                        model.AQGlossVarnish = model.AQGlossVarnish <= 0 ? prevData.AQGlossVarnish : model.AQGlossVarnish;
                        model.TotalM2ReqIncludingWastage = model.TotalM2ReqIncludingWastage <= 0 ? prevData.TotalM2ReqIncludingWastage : model.TotalM2ReqIncludingWastage;
                        model.TotalM2Cost = model.TotalM2Cost <= 0 ? prevData.TotalM2Cost : model.TotalM2Cost;
                        model.TotalQtyIncludingWastage = model.TotalQtyIncludingWastage <= 0 ? prevData.TotalQtyIncludingWastage : model.TotalQtyIncludingWastage;
                        model.RunningMeterIncludingWastage = model.RunningMeterIncludingWastage <= 0 ? prevData.RunningMeterIncludingWastage : model.RunningMeterIncludingWastage;
                        model.SumOfBlockCost = model.SumOfBlockCost <= 0 ? prevData.SumOfBlockCost : model.SumOfBlockCost;
                        model.SlittingOMachineID = model.SlittingOMachineID == null ? prevData.SlittingOMachineID : model.SlittingOMachineID;
                        model.DieCuttingOMachineID = model.DieCuttingOMachineID == null ? prevData.DieCuttingOMachineID : model.DieCuttingOMachineID;
                        model.UVOMachineID = model.UVOMachineID == null ? prevData.UVOMachineID : model.UVOMachineID;
                        model.LaminationOMachineID = model.LaminationOMachineID == null ? prevData.LaminationOMachineID : model.LaminationOMachineID;
                        model.FoilOMachineID = model.FoilOMachineID == null ? prevData.FoilOMachineID : model.FoilOMachineID;
                        model.EmbosingOMachineID = model.EmbosingOMachineID == null ? prevData.EmbosingOMachineID : model.EmbosingOMachineID;
                        model.GeneralExpense = model.GeneralExpense <= 0 ? prevData.GeneralExpense : model.GeneralExpense;
                        model.CostPerPc = model.CostPerPc <= 0 ? prevData.CostPerPc : model.CostPerPc;

                        if (prevData != null)
                        {
                            db.Entry(prevData).State = EntityState.Detached;
                        }
                        db.Entry(model).State = EntityState.Modified;
                        db.SaveChanges();

                        t.Commit();
                        return true;

                    }
                    
                }

                return false;
            }
            catch(Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                return false;
            }
            

           
        } 
    }
}