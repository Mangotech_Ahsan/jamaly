﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace POSOman.Models.BLL
{
    public static class Helper
    {
        public static DTO.HostNameAndIpAddressDTO GetHostNameAndIPAddress()
        {
            POSOman.Models.DTO.HostNameAndIpAddressDTO data = null;
            try
            {
                data = new DTO.HostNameAndIpAddressDTO();

                data.HostName = Dns.GetHostEntry(string.Empty).HostName;
                data.IpAddress = Dns.GetHostEntry(string.Empty).AddressList[2].ToString();
            }
            catch(Exception ex)
            {

            }

            return data;
        }
        public static DateTime PST()
        {
            TimeZoneInfo PakTimeZone;
            PakTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Pakistan Standard Time");
            return TimeZoneInfo.ConvertTime(DateTime.Now, PakTimeZone);
        }

        public static bool DateInRange(this DateTime dateToCheck)
        {
            DateTime startDate = new DateTime(1970, 01, 01);
            DateTime endDate = PST().AddYears(-19);

            return dateToCheck >= startDate && dateToCheck < endDate;
        }

        public static string DigitsWithComma(decimal? number)
        {
            try
            {
                double typeConvertedNumber = Convert.ToDouble(number);
                return Decimal.Round(Convert.ToDecimal(typeConvertedNumber)).ToString("#,##0");
            }
            catch(Exception e) { }
            return SystemConstants.Default_Value_For_Comma_Seperated_Digits;
        }

        public static async Task<bool> CompleteJoIfDelivered()
        {
            try
            {
                using (var d = new dbPOS())
                {
                    using (var t = d.Database.BeginTransaction())
                    {
                        try
                        {
                            var joDetailsCompletionTask = await Task.FromResult(d.MarkJoDetailsAsCompleted_SP());
                            var joCompletionTask = await Task.FromResult(d.MarkJoAsCompleted_SP());

                            t.Commit();
                            return true;
                        }
                        catch (Exception e)
                        {
                            t.Rollback();
                            return false;
                        }
                    }
                }

            }
            catch (Exception e) { }
            return false;
        }

        public static async Task<bool> CompleteJoIfDelivered_UnUsed()
        {
            try
            {
                using (var d = new dbPOS())
                {
                    using (var t = d.Database.BeginTransaction())
                    {
                        try
                        {
                            int c = 0;
                            var data = await Task.FromResult(d.GetTotalCompletedIncompletedJOrdersIdSP(null).Select(x => x.OrderID).ToList());

                            if (data?.Count > 0)
                            {
                                foreach (var id in data)
                                {


                                    var order = d.tbl_SalesOrder.Where(x => x.OrderID == id).FirstOrDefault();
                                    if (order != null)
                                    {
                                        order.isCompleted = true;

                                        d.Entry(order).State = EntityState.Modified;
                                        await d.SaveChangesAsync();

                                        c++;

                                    }
                                };

                                if (c == data.Count)
                                {
                                    t.Commit();

                                    return true;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            t.Rollback();
                            return false;
                        }
                    }
                }

            }
            catch (Exception e) { }
            return false;
        }

        public static string RemoveCharactersFromProductName (string Name)
        {
            if (!string.IsNullOrWhiteSpace(Name))
            {
                return Regex.Replace(Name, @"^[A-Za-z0-9 ._]+$", "");
            }
            return string.Empty;
        }

        public static string GetNumberOfDays(DateTime? startDate,DateTime? endDate)
        {

            try
            {
                if(startDate!=null && endDate != null)
                {
                    int NumberOfDays = Convert.ToInt32((endDate.Value.Date - startDate.Value.Date).TotalDays);

                    if (NumberOfDays > 1)
                    {
                        return NumberOfDays.ToString() + " days ago";
                    }
                    else
                    {
                        switch (NumberOfDays)
                        {
                            case 0:
                                return "Today";
                            case 1:
                                return "Yesterday";
                            default:
                                return string.Empty;
                        }
                    }
                    
                }
               

            }
            catch(Exception e) { }

            return string.Empty;
        }  
    }
}