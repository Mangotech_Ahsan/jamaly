﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.BLL
{
    public class ConversionClass
    {

        public static double SafeConvertToDouble(object objToConvert, double defaultValue)
        {
            if (objToConvert == null)
                return defaultValue;
            double value = 0;
            if (double.TryParse(objToConvert.ToString(), out value))
                return value;
            else
                return defaultValue;
        }
        public static decimal SafeConvertToDecimal(object objToConvert, decimal defaultValue)
        {
            if (objToConvert == null)
                return defaultValue;
            decimal value = 0;
            if (decimal.TryParse(objToConvert.ToString(), out value))
                return value;
            else
                return defaultValue;
        }


        public static int SafeConvertToInt32(object objToConvert, int defaultValue)
        {
            if (objToConvert == null)
                return defaultValue;
            int value = 0;
            if (Int32.TryParse(objToConvert.ToString(), out value))
                return value;
            else
                return defaultValue;
        }

        public static string SafeConvertToString(object objToConvert, string defaultValue)
        {
            if (objToConvert == null)
                return defaultValue;
            else
                return objToConvert.ToString();
        }
        // Convert string to bool if 1 or 0
        public static bool SafeConvertStringToBool(object objToConvert, bool defaultValue)
        {
            if (objToConvert == null)
                return defaultValue;
            if (objToConvert.ToString() == "1")
                return true;
            else if (objToConvert.ToString() == "0")
                return false;
            else
                return defaultValue;
        }
    }
}