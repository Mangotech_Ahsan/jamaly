﻿using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Web.Configuration;
using System.Text;

namespace POSOman.Models.BLL
{
    public class Reports
    {
        public object Save(Expenses model, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            int? jEntryTypeID = 0;
            int creditAccountID = 0;    //
            int debitAccountID = 0;     // 
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    StringBuilder Memo = new StringBuilder();
                    db.Configuration.ValidateOnSaveEnabled = false;
                    model.VoucherDate = DateTime.UtcNow.AddHours(5);
                    jEntryTypeID = 19; // Cash Deposit 
                    Memo.Append("Deposit Entry");
                    debitAccountID = model.AccountID; // CAsh Account id
                    creditAccountID = 1; //1; // Cash account id                                             
                    jEntryID = jEntryID = db.insertDepositEntry(model.Amount, debitAccountID, model.VoucherDate, AddBy, creditAccountID, Memo.ToString(), branchId, model.Description, jEntryTypeID, model.UserID).FirstOrDefault();
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

        public static List<AgingSummaryDTO> agingSummaryData(int range, int interval, int? AccountID, int? SPAccID, int? OID, DateTime? fromDate, DateTime? toDate, int? BranchID, int? InvoiceStatusID, string PaymentStatusDD, int? InvoiceTypeID, decimal? taxPercent)
        {

            try
            {
                using (var db = new dbPOS())
                {
                    List<AgingSummaryDTO> list = new List<AgingSummaryDTO>();
                    if (range > 0 && interval > 0) // r=30, i =120
                    {
                        int startDay = 1;
                        int endDay = 1;

                        int sequence = interval / range; //s = 4

                        if (sequence > 0)
                        {
                            for (int i = 1; i <= sequence+1; i++)
                            {
                                if(i<sequence + 1)
                                {
                                    startDay = endDay == 1 ? endDay : endDay + 1;

                                    endDay = (range * i);
                                }
                                else
                                {
                                    startDay = endDay == 1 ? endDay : endDay + 1;

                                    endDay = (range * i) * 800;
                                }
                                

                                List<GetCustomerAgingReportFilterWise_Result> data = db.GetCustomerAgingReportFilterWise(BranchID, AccountID, SPAccID, OID, fromDate, toDate, null, InvoiceStatusID, PaymentStatusDD, InvoiceTypeID, startDay, endDay, taxPercent).ToList();
                                
                                if (data!=null && data.Count > 0)
                                {

                                    foreach(var d in data)
                                    {
                                        list.Add(new AgingSummaryDTO
                                        {
                                            customerName = d.AccountName,
                                            totalAmount = d.TotalAmount ?? 0,
                                            rangeAndInterval = (i < sequence + 1)? startDay.ToString() + "-" + endDay.ToString():"> "+(startDay-1).ToString()
                                        });
                                    }
                                }
                                //

                            }
                        }

                        return list;

                    }


                }

            }
            catch (Exception e)
            {

            }
            return null;
        }
    }
}