﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.BLL
{
    public class UserActionsPerformed
    {
        dbPOS db = new dbPOS();
        public void MapActions(int? LoginUserID,string ActionPerformed)
        {
            try
            {
                string userid = HttpContext.Current.User.Identity.GetUserId();

                if (!string.IsNullOrWhiteSpace(userid))
                {
                    var loginUser = db.Users_Log.Where(x => x.UserId.Equals(userid)).OrderByDescending(x => x.ID).FirstOrDefault();
                    if (loginUser != null)
                    {
                        UserAction_Log UserAction = new UserAction_Log();
                        UserAction.UserLogId = loginUser.UserId == null ? 0 : Convert.ToInt32(loginUser.ID);
                        UserAction.ActionDate = Helper.PST();
                        UserAction.UserAction = ActionPerformed.ToString();
                        db.UserAction_Log.Add(UserAction);
                        //if (LoginUserID > 0)
                        db.SaveChanges();
                        return;
                    }

                }

                UserAction_Log UserActions = new UserAction_Log();
                UserActions.ActionDate = Helper.PST();
                UserActions.UserAction = ActionPerformed.ToString();
                db.UserAction_Log.Add(UserActions);
                //if (LoginUserID > 0)
                db.SaveChanges();
            }
            catch(Exception e)
            {

            }
            
        }
    }
}
