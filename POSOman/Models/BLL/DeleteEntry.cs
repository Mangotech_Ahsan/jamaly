﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSOman.Models;
using System.Data.Entity.Validation;
using System.Data.Entity;
using System.Text;
using System.Net;
using System.Data.Entity.Core.Objects;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace POSOman.Models.BLL
{
    public class DeleteEntry
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();
        public bool DeletePO(int? OrderID,int? LogID,int? UserID)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
            try
            {                
                    var POrder = db.tbl_PurchaseOrder.Where(x => x.OrderID == OrderID).FirstOrDefault();
                    POrder.IsDeleted = true;
                    POrder.DeletedBy = UserID;
                    db.Entry(POrder).State = EntityState.Modified;

                    var PRs = db.tbl_PurchaseOrder.Where(x => x.OrderID == POrder.PurchaseRequestID && x.IsDeleted!=true).ToList();
                    if(PRs!=null && PRs.Count > 0)
                    {
                        Parallel.ForEach(PRs, pr =>
                         {
                             pr.Can_Modify = null; db.Entry(pr).State = EntityState.Modified;
                         });
                        db.SaveChanges();
                    }
                    db.SaveChanges();
                    List<int> entryTypeIDs = new List<int>() { 8,30};
                    
                    var PODetails = db.tbl_PODetails.Where(x => x.OrderID == OrderID);
                    var StockLog = db.tbl_StockLog.Where(x => x.OrderID == OrderID && x.OrderTypeID == 1);
                    var JEntryID = db.tbl_JDetail.Where(x => x.OrderID == OrderID && entryTypeIDs.Contains(x.EntryTypeID ??0)).FirstOrDefault();
                    var JEntry = db.tbl_JEntry.Where(je => je.JEntryId == JEntryID.JEntryID ).ToList();               

                    foreach (var i in PODetails)
                    {
                        string BatchID = OrderID + "-" + i.ProductID;
                        var stockBatch = db.tbl_StockBatch.Where(sb => sb.BatchID == BatchID).FirstOrDefault();
                        if(stockBatch != null)
                        {
                            db.tbl_StockBatch.Remove(stockBatch);
                        }
                        else
                        {
                            BatchID = "OldStock-1001";
                            stockBatch = db.tbl_StockBatch.Where(sb => sb.BatchID == BatchID && sb.ProductID == i.ProductID).FirstOrDefault();
                            if (stockBatch != null)
                            {
                                stockBatch.Qty -= (i.Qty - i.ReturnedQty ??0);
                                db.Entry(stockBatch).State = EntityState.Modified;
                            }
                        }                     
                        i.IsDeleted = true;
                        i.DeletedBy = UserID;
                        db.Entry(i).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    
                    foreach (var i in StockLog)
                    {
                        i.IsActive = false;                        
                        db.Entry(i).State = EntityState.Modified;
                        var Stock = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && x.BranchID == i.BranchID).FirstOrDefault();
                        Stock.Qty = Stock.Qty - Convert.ToDecimal(i.StockIN);
                                          
                        db.Entry(Stock).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    
                    foreach (var i in JEntry)
                    {
                        //i.IsActive = false;
                        //i.DeleteBy = UserID;
                        //i.IsDeleted = true;
                        //db.Entry(i).State = EntityState.Modified;
                        //db.SaveChanges();
                        var JDetails = db.tbl_JDetail.Where(x => x.JEntryID == i.JEntryId).ToList();
                        if (JDetails.Count > 0)
                        {
                            db.tbl_JDetail.RemoveRange(JDetails);
                            db.SaveChanges();
                        }
                        //foreach(var o in JDetails)
                        //{
                        //    o.IsDeleted = true;
                        //    o.DeleteBy = UserID;
                        //    db.Entry(o).State = EntityState.Modified;
                        //    db.SaveChanges();
                        //}
                        var JEntryLog = db.tbl_JEntryLog.Where(x => x.JEntryID == i.JEntryId).ToList();
                        if (JEntryLog.Count > 0)
                        {
                            db.tbl_JEntryLog.RemoveRange(JEntryLog);
                            db.SaveChanges();
                        }
                        db.tbl_JEntry.RemoveRange(JEntry);
                        db.SaveChanges();

                        //foreach (var o in JEntryLog)
                        //{
                        //    o.isReversed = true;
                        //    db.Entry(o).State = EntityState.Modified;
                        //    db.SaveChanges();
                        //}
                    }

                    UserActions.MapActions(Convert.ToInt32(LogID), "Delete Purchase Entry".ToString());
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public bool DeletePurchaseRequest(int? OrderID, int? LogID, int? UserID)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var POrder = db.tbl_PurchaseOrder.Where(x => x.OrderID == OrderID).FirstOrDefault();
                    POrder.IsDeleted = true;
                    if (POrder.tbl_PODetails != null)
                    {
                        POrder.tbl_PODetails.ToList().ForEach(x => { x.IsDeleted = true; });
                       
                    }
                    POrder.DeletedBy = UserID;
                    db.Entry(POrder).State = EntityState.Modified;
                    db.SaveChanges();
                   

                    UserActions.MapActions(Convert.ToInt32(LogID), "Delete Purchase Request Entry".ToString());
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return false;
                }
            }
        }



        ///////////Delete Sales Entry

        public bool DeleteSO(int? OrderID, int? LogID,int? UserID)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    List<int> entryTypeIDs = new List<int>() { 9, 31 };
                    var SOrder = db.tbl_SalesOrder.Where(x => x.OrderID == OrderID).FirstOrDefault();
                    SOrder.IsDeleted = true;
                    SOrder.UpdateBy = UserID;
                    db.Entry(SOrder).State = EntityState.Modified;
                    db.SaveChanges();

                    var SODetails = db.tbl_SaleDetails.Where(x => x.OrderID == OrderID);
                    var StockLog = db.tbl_StockLog.Where(x => x.OrderID == OrderID && x.OrderTypeID == 3 && x.IsActive != false).ToList();
                    var JEntryID = db.tbl_JDetail.Where(x => x.OrderID == OrderID && entryTypeIDs.Contains(x.EntryTypeID ?? 0)).FirstOrDefault();
                    var JEntry = db.tbl_JEntry.Where(je => je.JEntryId == JEntryID.JEntryID);
                    foreach (var i in SODetails)
                    {
                        i.IsDeleted = true;
                        i.UpdateBy = UserID;
                        db.Entry(i).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    foreach (var i in StockLog)
                    {
                        var stockBatchID = i.StockBatchID;
                        var oldQty = i.StockOut;
                        var stockBatch = db.tbl_StockBatch.Where(x => x.StockBatchID == stockBatchID && x.ProductID == i.ProductID).FirstOrDefault();
                        if (stockBatch != null)
                        {
                            stockBatch.BranchID = i.BranchID ?? 9001;
                            stockBatch.Qty += i.StockOut ?? 0;
                            db.Entry(stockBatch).State = EntityState.Modified;
                        }
                        i.IsActive = false;                        
                        db.Entry(i).State = EntityState.Modified;
                        var Stock = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && x.BranchID == i.BranchID).FirstOrDefault();
                        Stock.Qty = Stock.Qty + Convert.ToDecimal(i.StockOut);
                       
                        db.Entry(Stock).State = EntityState.Modified;
                        db.SaveChanges();

                    }
                    foreach (var i in JEntry)
                    {
                        i.IsActive = false;
                        i.IsDeleted = true;
                        i.DeleteBy = UserID;
                        db.Entry(i).State = EntityState.Modified;
                        db.SaveChanges();
                        var JDetails = db.tbl_JDetail.Where(x => x.JEntryID == i.JEntryId).ToList();
                        foreach (var o in JDetails)
                        {
                            o.IsDeleted = true;
                            o.DeleteBy = UserID;
                            db.Entry(o).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        var JEntryLog = db.tbl_JEntryLog.Where(x => x.JEntryID == i.JEntryId).ToList();
                        foreach (var o in JEntryLog)
                        {
                            o.isReversed = true;
                            db.Entry(o).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                    }
                    UserActions.MapActions(Convert.ToInt32(LogID), "Delete Sales Entry".ToString());
                    transaction.Commit();
                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return false;
                }

            }


        }
    }
}
