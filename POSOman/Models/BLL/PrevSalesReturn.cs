﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace POSOman.Models.BLL
{
    public class PrevSalesReturn
    {
        public object Save(Models.tbl_PrevSalesReturn model, List<Models.DTO.StockLog> modelStockLog, int? bankAccId, int branchId)
        {
            dbPOS db = new dbPOS();            
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentStatus == "UnPaid")
                    {
                        model.AmountPaid = 0;
                        model.IsPaid = false;
                    }
                    else if (model.PaymentStatus == "Partial Paid")
                    {
                        model.IsPaid = false;
                    }
                    else if (model.PaymentStatus == "Paid")
                    {
                        model.IsPaid = true;
                    }
                    db.Configuration.ValidateOnSaveEnabled = false;
                    model.AddOn = DateTime.UtcNow.AddHours(5);
                    model.AddBy = 1;
                    model.BranchID = branchId;
                    model.UserID = HttpContext.Current.User.Identity.GetUserId();                    
                    string sPaymentStatus = model.PaymentStatus;
                    int? iPaymentAccountID = null;
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                        iPaymentAccountID = (int)(model.PaymentTypeID);//  set it from Payment Type ddl 
                    }
                    else if (model.PaymentTypeID > 1)
                    {
                        iPaymentAccountID = bankAccId;
                    }                    
                    db.tbl_PrevSalesReturn.Add(model);
                    int poId = db.SaveChanges();
                    int iReturnID = model.ReturnID;
                    decimal dAmountPaid = 0;
                    if (model.AmountPaid != null || model.AmountPaid > 0)
                        dAmountPaid = (decimal)(model.AmountPaid);
                    if (poId >= 1 && modelStockLog != null)
                    {
                        // get all product ids 
                        List<int> lstProdIds = model.tbl_PrevSOReturnDetails.Select(p => p.ProductID).ToList();
                        // get all stock where productid contains lstProdIds
                        var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == model.BranchID).ToList();
                        var lstSODetail = db.tbl_PrevSOReturnDetails.Where(s => lstProdIds.Contains(s.ProductID) && (s.ReturnID == model.ReturnID)).ToList();
                        List<tbl_PrevSOReturnDetails> returningOrders = lstSODetail.ToList();
                        List<tbl_Stock> existingStock = lstStock.ToList();
                        // 
                        foreach (var item in modelStockLog)
                        {
                            item.InvoiceDate = DateTime.UtcNow.AddHours(5).Date;
                            item.OrderID = iReturnID;
                            item.OrderTypeID = 10;
                            item.AddOn = DateTime.UtcNow.AddHours(5);
                            item.AddBy = 1;
                            item.CostPrice = 0;
                            item.BranchID = branchId;
                            lstProdIds.Add(item.ProductID);
                            //  if already exists stock
                            var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
                            if (row != null)
                            {
                                row.Qty += Convert.ToInt32(item.StockIN);
                                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                db.Entry(row).State = EntityState.Modified;
                            }
                            else
                            {
                                // if new item is not In Stock and User is Selling (As Discussed with Boss) 
                                tbl_Stock newStock = new tbl_Stock();
                                newStock.ProductID = item.ProductID;
                                newStock.Qty = Convert.ToInt32(item.StockIN);                                
                                newStock.Addon = DateTime.UtcNow.AddHours(5);
                                newStock.SalePrice = item.SalePrice;
                                newStock.CostPrice = 0;
                                newStock.BranchID = item.BranchID;                                
                                newStock.OnMove = 0;
                                db.tbl_Stock.Add(newStock);
                            }
                        }
                        Mapper.CreateMap<Models.DTO.StockLog, tbl_StockLog>();
                        var stockLog = Mapper.Map<ICollection<Models.DTO.StockLog>, ICollection<tbl_StockLog>>(modelStockLog);
                        stockLog.ToList<tbl_StockLog>().ForEach(s => db.Entry(s).State = EntityState.Added);
                        var returnOrder = db.tbl_SalesOrder.Where(s => s.SOID < 1000 && s.AccountID == model.AccountID && s.isOpening == true ).FirstOrDefault();
                        if (returnOrder != null && sPaymentStatus == "UnPaid")
                        {
                            if (returnOrder.ReturnAmount == null)
                            { returnOrder.ReturnAmount = model.TotalAmount; }
                            else
                            { returnOrder.ReturnAmount += model.TotalAmount; }
                            returnOrder.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(returnOrder).State = EntityState.Modified;
                        }
                        if (returnOrder != null && sPaymentStatus == "Partial Paid")
                        {
                            if (returnOrder.ReturnAmount == null)
                            { returnOrder.ReturnAmount = ( model.TotalAmount - dAmountPaid); }
                            else
                            { returnOrder.ReturnAmount += (model.TotalAmount - dAmountPaid); }
                            returnOrder.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(returnOrder).State = EntityState.Modified;
                        }
                        if (sPaymentStatus == "Paid")
                        {
                            db.insertPreviousSOReturnGJEntryPaid(iPaymentAccountID, iReturnID);
                        }
                        else if (sPaymentStatus == "Partial Paid")
                        {
                            db.insertPreviousSOReturnGJEntryPartiallyPaid(iPaymentAccountID, dAmountPaid, iReturnID);
                        }
                        else if (sPaymentStatus == "UnPaid")
                        {
                            db.insertPreviousSOReturnGJEntry(iReturnID);
                        }
                       
                        db.SaveChanges();
                    }
                    transaction.Commit();                   
                    return iReturnID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
    }
}