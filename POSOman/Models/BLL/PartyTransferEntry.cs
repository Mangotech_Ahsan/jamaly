﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace POSOman.Models.BLL
{
    public class PartyTransferEntry
    {

        public object Save(Models.DTO.PartyToPartyTransferDTO model, int? bankAccId, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            int? jEntryTypeID = 0;
            int? PartyAccountIDCredit = 0;
            int? CustomerAccountIDDebit = 0;
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    CustomerAccountIDDebit = model.FromBankID;
                    PartyAccountIDCredit = model.ToBankID;
                    jEntryTypeID = 41;
                    model.BranchID = branchId;
                    db.Configuration.ValidateOnSaveEnabled = false;

                    if (model.ChequeDate > model.VoucherDate)
                    {
                        jEntryID = db.insertPartytoPartyTransferGJPDCEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, CustomerAccountIDDebit, PartyAccountIDCredit, model.BankName, model.ChequeDate, model.ChequeNumber, model.BankName, null, model.Description, bankAccId, branchId, model.SlipNumber, model.BankDetail).FirstOrDefault();
                    }
                    else
                    {
                        jEntryID = db.insertPartytoPartyCustomerGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, CustomerAccountIDDebit, PartyAccountIDCredit, model.BankName, model.ChequeDate, model.ChequeNumber, null, model.Description, bankAccId, branchId, model.SlipNumber, model.BankDetail).FirstOrDefault();
                    }

                    db.SaveChanges();

                    transaction.Commit();
                    return jEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
    }
}