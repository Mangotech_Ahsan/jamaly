﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace POSOman.Models.BLL
{
    public class VendorPayment
    {
        //without invoices
        public object SaveSalePersonSinglePayment(Payment model, List<Models.DTO.JEntryLog> jEntryLog, int? bankAccId, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            model.BranchID = branchId;
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                        model.ChequeDate = null;
                    }
                    if (model.PaymentTypeID == 2)
                    {
                        model.ChequeDate = null;
                    }
                    StringBuilder VendorMemo = new StringBuilder();
                    VendorMemo.Append(model.Description);
                    db.Configuration.ValidateOnSaveEnabled = false;

                    jEntryID = db.insertSalePersonGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, VendorMemo.ToString(), model.Description, bankAccId, branchId).FirstOrDefault();
                    foreach (var item in jEntryLog)
                    {
                        item.JEntryID = jEntryID ?? 0; // get it from SP                                               
                        item.BranchID = branchId;
                    }
                    Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                    var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jEntryLog);
                    foreach (var item in jLog)
                    {
                        db.tbl_JEntryLog.Add(item);
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

        public object SaveSinglePayment(Payment model, List<Models.DTO.JEntryLog> jEntryLog, int? bankAccId, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            model.BranchID = branchId;
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    model.ChequeDate = model.VoucherDate;
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                        model.ChequeDate = null;
                    }
                    if (model.PaymentTypeID == 2)
                    {
                        model.ChequeDate = null;
                    }
                    StringBuilder VendorMemo = new StringBuilder();
                    VendorMemo.Append(model.Description);
                    db.Configuration.ValidateOnSaveEnabled = false;

                    jEntryID = db.insertVendorGJEntryWithOutInvoices(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, VendorMemo.ToString(), model.Description, bankAccId, branchId).FirstOrDefault();
                    foreach (var item in jEntryLog)
                    {
                        item.JEntryID = jEntryID ?? 0; // get it from SP                                               
                        item.BranchID = branchId;
                    }
                    Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                    var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jEntryLog);
                    foreach (var item in jLog)
                    {
                        db.tbl_JEntryLog.Add(item);
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        //  SettleAmount for customer advance
        public object SettleAmount(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId, int branchId, int CustJEntryID)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                        model.ChequeDate = null;
                    }
                    if (model.PaymentTypeID == 2)
                    {
                        model.ChequeDate = null;
                    }

                    var PrevInv = db.tbl_JEntry.Where(x => x.JEntryId == CustJEntryID).FirstOrDefault();

                    if (PrevInv != null)
                    {
                        PrevInv.TotalBalanceSettled += model.Amount;
                        db.Entry(PrevInv).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    var orderID = jentryLog[0].OrderID;
                    model.OrderID = Convert.ToInt32(orderID);
                    StringBuilder CustomerMemo = new StringBuilder();
                    StringBuilder PaymentMemo = new StringBuilder();
                    db.Configuration.ValidateOnSaveEnabled = false;
                    // get all Order ids 
                    List<int> lstOrderIds = model.Details.Select(p => p.OrderID).ToList();
                    // get all SaleeOrders where OrderID contains lstOrderIds
                    var lstOrders = db.tbl_PurchaseOrder.Where(s => lstOrderIds.Contains(s.OrderID)).ToList();
                    List<tbl_PurchaseOrder> existingOrders = lstOrders.ToList();
                    //
                    if (lstOrderIds.Count > 0)
                    {
                        if (model.ChequeDate > model.VoucherDate)
                        {
                            CustomerMemo.Append("PDC Received for Invoice No: ");   // change po no by system gen id here 
                        }
                        else
                        {
                            CustomerMemo.Append("Received for Invoice No: ");   // change po no by system gen id here 
                        }
                    }
                    foreach (var item in model.Details)
                    {
                        lstOrderIds.Add(item.OrderID);
                        //  if already exists 
                        var row = existingOrders.FirstOrDefault(s => s.OrderID == item.OrderID);
                        if (row != null)
                        {
                            CustomerMemo.Append(row.POID + ",");
                            decimal? totalPaid = row.TotalPaid + item.Receiving;
                            row.TotalPaid += item.Receiving;// enter totalPaid = 0 at time of order
                            decimal? totalPayable =(row.TotalAmount + Convert.ToDecimal(row.VAT )) - Convert.ToDecimal(row.ReturnAmount);
                            if (totalPaid == totalPayable)
                            {
                                row.PaymentStatus = "Paid";
                                row.IsPaid = true;

                            }
                            else if (totalPaid < totalPayable)
                            {
                                row.PaymentStatus = "Partial Paid";
                            }
                            row.Can_Modify = false;
                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(row).State = EntityState.Modified;
                        }
                    }
                    //if (model.ChequeDate > model.VoucherDate && model.PaymentTypeID > 2)
                    //{
                    //    jEntryID = db.insertCustomerGJPDCEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, null, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID).FirstOrDefault();
                    //}
                    //else
                    //{
                    //    jEntryID = db.insertCustomerGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID).FirstOrDefault();
                    //}
                    foreach (var item in jentryLog)
                    {
                        item.JEntryID = CustJEntryID;// jEntryID ?? 0; // get it from SP                                                
                        item.BranchID = branchId;
                    }
                    Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                    var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jentryLog);
                    foreach (var item in jLog)
                    {
                        db.tbl_JEntryLog.Add(item);
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return CustJEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

        //with invoices
        public object Save(Payment model, List<Models.DTO.JEntryLog> jEntryLog, int? bankAccId, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;            
            model.BranchID = branchId;
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    model.ChequeDate = model.VoucherDate;
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                        model.ChequeDate = null;
                    }
                    if (model.PaymentTypeID == 2)
                    {
                        model.ChequeDate = null;
                    }
                    StringBuilder VendorMemo = new StringBuilder();
                    StringBuilder PaymentMemo = new StringBuilder();
                    StringBuilder DiscountMemo = new StringBuilder();
                    StringBuilder TaxMemo = new StringBuilder();
                    StringBuilder AdjustmentMemo = new StringBuilder();
                   
                    db.Configuration.ValidateOnSaveEnabled = false;
                    // get all Order ids 
                    List<int> lstOrderIds = model.Details.Select(p => p.OrderID).ToList();
                    // get all PurchaseOrders where OrderID contains lstOrderIds
                    var lstOrders = db.tbl_PurchaseOrder.Where(s => lstOrderIds.Contains(s.OrderID)).ToList();
                    List<tbl_PurchaseOrder> existingOrders = lstOrders.ToList();
                    //
                    if (lstOrderIds.Count > 0)
                    {
                        VendorMemo.Append("Paid for Invoice No: ");
                        DiscountMemo.Append("Discounts for Invoice No: ");
                        TaxMemo.Append("Taxes for Invoice No: ");
                        AdjustmentMemo.Append("Adjustments for Invoice No: ");
                    }
                    decimal totalDiscount = 0;
                    decimal totalTax = 0;
                    decimal totalAdjustment = 0;

                    foreach (var item in model.Details)
                    {                        
                        lstOrderIds.Add(item.OrderID);
                        //  if already exists 
                        var row = existingOrders.FirstOrDefault(s => s.OrderID == item.OrderID);
                        if (row != null)
                        {
                            VendorMemo.Append(row.POID + ",");
                            DiscountMemo.Append(row.POID + ",");
                            TaxMemo.Append(row.POID + ",");
                            AdjustmentMemo.Append(row.POID + ",");

                            decimal? totalPaid = row.TotalPaid + item.Paying + (row.Expenses ?? 0) + (row.ReturnAmount ?? 0);                            
                            if (totalPaid >= row.TotalAmount)
                            {
                                
                                row.TotalPaid = row.TotalPaid + item.Paying;
                                row.PaymentStatus = "Paid";
                                row.IsPaid = true;
                                row.Can_Modify = false;
                            }
                            else if (row.AmountPaid < row.TotalAmount)
                            {
                                row.TotalPaid = row.TotalPaid + item.Paying;
                                row.PaymentStatus = "Partial Paid";
                                row.Can_Modify = false;                               
                            }
                            if (item.Discount > 0)
                            {
                                totalDiscount += (row.DiscountAmount == 0 || row.DiscountAmount == null) ? item.Discount : 0;
                                row.DiscountAmount = (row.DiscountAmount == 0 || row.DiscountAmount == null )?item.Discount:0;
                            }
                            if (item.Adjustment > 0)
                            {
                                totalAdjustment += (row.Adjustment == 0 || row.Adjustment == null) ? item.Adjustment : 0;
                                row.Adjustment = (row.Adjustment == 0 || row.Adjustment == null) ? item.Adjustment : 0;
                                row.AdjustmentDescription = (row.Adjustment == 0 || row.Adjustment == null) ? item.AdjustmentDescription : "-";
                            }
                            if (item.Tax > 0)
                            {
                                totalTax += (row.VAT == 0 || row.VAT == null) ? item.Tax : 0;
                                row.VAT = (row.VAT == 0 || row.VAT == null) ? item.Tax : 0;
                                row.VATPercent = (row.VATPercent == 0 || row.VATPercent == null) ? item.TaxPercent : 0;
                                row.VATType = (row.VAT == 0 || row.VAT == null) ? item.TaxType : null;
                            }
                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(row).State = EntityState.Modified;
                        }
                    }
                    if (model.ChequeDate > model.VoucherDate && model.PaymentTypeID > 2)
                    {
                        jEntryID = db.insertVendorPDCGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, VendorMemo.ToString(), model.Description, bankAccId, branchId).FirstOrDefault();
                    }
                    else
                    {
                        jEntryID = db.insertVendorGJEntryWithTaxAdjustmentAndDiscount(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, VendorMemo.ToString(), model.Description, bankAccId, branchId, totalDiscount, totalAdjustment, AdjustmentMemo.ToString(), totalTax, TaxMemo.ToString(), DiscountMemo.ToString()).FirstOrDefault();

                        //jEntryID = db.insertVendorGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, VendorMemo.ToString(), model.Description, bankAccId, branchId).FirstOrDefault();
                    }
                    foreach (var item in jEntryLog)
                    {
                        item.JEntryID = jEntryID ?? 0; // get it from SP                                               
                        item.BranchID = branchId;
                    }
                    Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                    var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jEntryLog);
                    foreach (var item in jLog)
                    {
                        db.tbl_JEntryLog.Add(item);
                    }
                    db.SaveChanges();                    
                    transaction.Commit();                    
                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }        
        public object DeleteEntry(int JEntryID)
        {
            dbPOS db = new dbPOS();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    ICollection<tbl_JEntryLog> jEntryLog = db.tbl_JEntryLog.Where(j => j.JEntryID == JEntryID).ToList();
                    Mapper.CreateMap<tbl_JEntryLog, Models.DTO.JEntryLog>();
                    var jLog = Mapper.Map<ICollection<tbl_JEntryLog>, ICollection<Models.DTO.JEntryLog>>(jEntryLog);
                    db.Configuration.ValidateOnSaveEnabled = false;
                    #region Update Purchase Order 
                    foreach (var item in jLog)
                    {
                        var row = db.tbl_PurchaseOrder.FirstOrDefault(so => so.OrderID == item.OrderID && so.BranchID == item.BranchID);
                        if (row != null)
                        {
                            row.TotalPaid -= Convert.ToDecimal(item.Amount);
                            if (row.TotalPaid == 0)
                            {
                                row.PaymentStatus = "UnPaid";
                                row.IsPaid = false;
                            }
                            else if (row.TotalPaid < row.TotalAmount)
                            {
                                row.PaymentStatus = "Partial Paid";
                                row.IsPaid = false;
                            }
                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(row).State = EntityState.Modified;
                        }
                    }
                    #endregion
                    #region Reverse Journal Entry 
                    db.DeleteVendorPayment(JEntryID);
                    #endregion
                    db.SaveChanges();
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }


        //with multi payments invoices
        public object SaveMulti(Payment model, List<Models.DTO.JEntryLog> jEntryLog, int? bankAccId, int branchId, string UserName, List<Models.DTO.GetMultiChequePayments> MultiPayments)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            //model.BranchID = branchId;
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                        model.ChequeDate = null;
                    }
                    if (model.PaymentTypeID == 2)
                    {
                        model.ChequeDate = null;
                    }

                   

                    StringBuilder VendorMemo = new StringBuilder();
                    StringBuilder PaymentMemo = new StringBuilder();
                    db.Configuration.ValidateOnSaveEnabled = false;
                    // get all Order ids 
                    List<int> lstOrderIds = model.Details.Select(p => p.OrderID).ToList();
                    // get all PurchaseOrders where OrderID contains lstOrderIds
                    var lstOrders = db.tbl_PurchaseOrder.Where(s => lstOrderIds.Contains(s.OrderID)).ToList();
                    List<tbl_PurchaseOrder> existingOrders = lstOrders.ToList();
                    //
                    if (lstOrderIds.Count > 0)
                    {
                        VendorMemo.Append("Paid for purchase Invoice No: ");
                    }
                    foreach (var item in model.Details)
                    {
                        lstOrderIds.Add(item.OrderID);
                        //  if already exists 
                        var row = existingOrders.FirstOrDefault(s => s.OrderID == item.OrderID);
                        if (row != null)
                        {
                            VendorMemo.Append(row.POID + ",");
                            decimal? totalPaid = row.TotalPaid + item.Paying;
                            row.TotalPaid = totalPaid;
                            if (totalPaid == row.TotalAmount)
                            {
                                row.PaymentStatus = "Paid";
                                row.IsPaid = true;
                            }
                            else if (row.AmountPaid < row.TotalAmount)
                            {
                                row.PaymentStatus = "Partial Paid";
                                // row.PaymentTypeID = model.PaymentTypeID;
                            }
                            row.UpdateOn = Helper.PST();
                            db.Entry(row).State = EntityState.Modified;
                        }
                    }
                    //////////////////////////////////////////
                    if (MultiPayments.Count > 0 && model.Amount == MultiPayments.Sum(x => x.Amount))
                    {

                        string BnkName = "";
                        int? BankAccountID = 0;

                        #region for JEntry entries

                        tbl_JEntry jEntry = new tbl_JEntry();

                        jEntry.VoucherName = "Vendor/Vendor PDC Payment";
                        jEntry.Description = model.Description;
                        jEntry.AddOn = Helper.PST();
                        jEntry.IsActive = true;
                        jEntry.RefID = null;// model.AccountID;
                        jEntry.AddBy = AddBy;
                        jEntry.Amount = model.Amount;
                        jEntry.VoucherDate = model.VoucherDate;
                        jEntry.BankName = null;
                        jEntry.ChequeDate = null;
                        jEntry.ChequeNumber = null;
                        jEntry.BranchID = model.BranchID;
                     
                        jEntry.UserName = UserName;
                        jEntry.IsVoucher = "VendorVoucher";

                        db.tbl_JEntry.Add(jEntry);
                        int ID = db.SaveChanges();

                        jEntryID = jEntry.JEntryId;

                        #endregion

                        #region for vendor Account JDetail multi entries

                        tbl_JDetail jDetailCr = new tbl_JDetail();
                        jDetailCr.AccountID = model.AccountID;
                        jDetailCr.JEntryID = jEntry.JEntryId;
                        jDetailCr.Dr = model.Amount;
                        jDetailCr.Cr = 0;
                        jDetailCr.AddOn = Helper.PST();
                        jDetailCr.Detail = VendorMemo.ToString();
                        jDetailCr.EntryTypeID = 2;
                        jDetailCr.BranchID = Convert.ToInt32(model.BranchID);
                        // jDetailCr.OrderID = model.OrderID;
                        jDetailCr.RefAccountID = null;// model.AccountID;
                     
                        jDetailCr.UserName = UserName;

                        db.tbl_JDetail.Add(jDetailCr);
                        db.SaveChanges();




                        #endregion


                        foreach (var i in MultiPayments)
                        {


                            if (i.PayTypeID == 1)
                            {
                                i.BankID = i.PayTypeID;
                            }
                            var BankName = db.tbl_AccountDetails.Where(x => x.AccountID == i.BankID && x.AccountTypeID == 27).FirstOrDefault();
                            if (BankName == null)
                            {
                                BnkName = "";
                            }
                            else
                            {
                                BnkName = BankName.AccountName;
                                BankAccountID = BankName.AccountID;
                            }
                            if (i.chqDate > model.VoucherDate && i.PayTypeID > 2)
                            {

                                #region for with pdc multi entries


                                tbl_JDetail jDetail = new tbl_JDetail();
                                if (i.PayTypeID >= 2 && i.PayTypeID != 4)
                                {
                                    jDetail.AccountID = Convert.ToInt32(24);//act as PDC account pay
                                    jDetail.BankName = BnkName;
                                    jDetail.ChequeDate = i.chqDate;
                                    jDetail.ChequeNumber = i.chqNumber;
                                }
                                else if (i.PayTypeID == 4)
                                {
                                    jDetail.AccountID = Convert.ToInt32(24);//act as PDC account pay
                                    jDetail.BankName = "";
                                    jDetail.ChequeDate = i.chqDate;
                                    jDetail.ChequeNumber = i.chqNumber;

                                }
                                else
                                {
                                    jDetail.AccountID = Convert.ToInt32(i.PayTypeID);
                                    jDetail.BankName = "";
                                    jDetail.ChequeDate = null;
                                    jDetail.ChequeNumber = "";
                                }

                                jDetail.JEntryID = jEntry.JEntryId;
                                jDetail.Dr = 0;
                                jDetail.Cr = i.Amount??0;
                                jDetail.AddOn = Helper.PST();

                                jDetail.Detail = "PDC purchase Payment-" + VendorMemo.ToString();
                                jDetail.EntryTypeID = 1;
                                jDetail.BranchID = Convert.ToInt32(model.BranchID);
                                // jDetail.OrderID = model.OrderID;
                                jDetail.PayTypeID = i.PayTypeID;
                                jDetail.RefAccountID = model.AccountID;
                            
                                jDetail.UserName = UserName;

                                db.tbl_JDetail.Add(jDetail);
                                db.SaveChanges();


                                if (i.PayTypeID == 3)
                                {
                                    //                         ([JEntryId],[ChequeTypeID],[Amount],[BankName]
                                    //,[ChequeDate],[ChequeNumber],[ChequeBank],[DepositBankAccountID]
                                    //,[Description]
                                    //,[IsCleared],[IsBounced],[IsPDC], ReferenceDetail
                                    //,[AddBy],[AddOn], BranchID)

                                    tbl_Cheques chq = new tbl_Cheques();
                                    chq.JEntryId = jEntry.JEntryId;
                                    chq.ChequeTypeID = 2;
                                    chq.Amount = i.Amount;
                                    chq.BankName = BnkName;
                                    chq.ChequeDate = i.chqDate;
                                    chq.ChequeNumber = i.chqNumber;
                                    chq.ChequeBank = BnkName;
                                    chq.DepositBankAccountID = BankAccountID;
                                    chq.Description = model.Description;
                                    chq.IsCleared = false;
                                    chq.IsBounced = false;
                                    chq.IsPDC = true;
                                    chq.ReferenceDetail = "PDC purchase Payment-" + VendorMemo.ToString();
                                    chq.AddBy = AddBy;
                                    chq.AddOn = Helper.PST();
                                    chq.BranchID = model.BranchID;

                                    db.tbl_Cheques.Add(chq);
                                    db.SaveChanges();
                                }
                                else if (i.PayTypeID == 4)
                                {
                                    tbl_Cheques chq = new tbl_Cheques();
                                    chq.JEntryId = jEntry.JEntryId;
                                    chq.ChequeTypeID = 3;
                                    chq.Amount = i.Amount;
                                    chq.BankName = "";
                                    chq.ChequeDate = i.chqDate;
                                    chq.ChequeNumber = i.chqNumber;
                                    // chq.ChequeBank = BnkName;
                                    // chq.DepositBankAccountID = BankAccountID;
                                    chq.Description = model.Description;
                                    chq.IsCleared = false;
                                    chq.IsBounced = false;
                                    chq.IsPDC = true;

                                    jDetail.PayTypeID = i.PayTypeID;
                                    chq.ReferenceDetail = "PDC purchase Payment-" + VendorMemo.ToString();
                                    chq.AddBy = AddBy;
                                    chq.AddOn = Helper.PST();
                                    chq.BranchID = model.BranchID;

                                    db.tbl_Cheques.Add(chq);
                                    db.SaveChanges();
                                }
                                else if (i.PayTypeID == 5)
                                {
                                    tbl_Cheques chq = new tbl_Cheques();
                                    chq.JEntryId = jEntry.JEntryId;
                                    chq.ChequeTypeID = 2;
                                    chq.Amount = i.Amount;
                                    chq.BankName = "";
                                    chq.ChequeDate = i.chqDate;
                                    chq.ChequeNumber = i.chqNumber;
                                    // chq.ChequeBank = BnkName;
                                    // chq.DepositBankAccountID = BankAccountID;
                                    chq.Description = model.Description;
                                    chq.IsCleared = false;
                                    chq.IsBounced = false;
                                    chq.IsPDC = true;
                                    chq.isOther = true;
                                    jDetail.PayTypeID = i.PayTypeID;
                                    chq.ReferenceDetail = "PDC purchase Payment-" + VendorMemo.ToString();
                                    chq.AddBy = AddBy;
                                    chq.AddOn = Helper.PST();
                                    chq.BranchID = model.BranchID;

                                    db.tbl_Cheques.Add(chq);
                                    db.SaveChanges();
                                }



                                #endregion
                            }
                            else
                            {

                                #region for without pdc multi entries


                                tbl_JDetail jDetail = new tbl_JDetail();
                                if (i.PayTypeID >= 2 && i.PayTypeID != 4)
                                {
                                    jDetail.AccountID = Convert.ToInt32(i.BankID);
                                    jDetail.BankName = BnkName;
                                    jDetail.ChequeDate = i.chqDate;
                                    jDetail.ChequeNumber = i.chqNumber;
                                }
                                else if (i.PayTypeID == 4)
                                {
                                    jDetail.AccountID = Convert.ToInt32(24);//act as PDC account pay
                                    jDetail.BankName = "";
                                    jDetail.ChequeDate = i.chqDate;
                                    jDetail.ChequeNumber = i.chqNumber;

                                }
                                else
                                {
                                    jDetail.AccountID = Convert.ToInt32(i.PayTypeID);
                                    jDetail.BankName = "";
                                    jDetail.ChequeDate = null;
                                    jDetail.ChequeNumber = "";
                                }

                                jDetail.JEntryID = jEntry.JEntryId;
                                jDetail.Dr = 0;
                                jDetail.Cr = i.Amount?? 0;
                                jDetail.AddOn = Helper.PST();
                                jDetail.Detail = VendorMemo.ToString();
                                jDetail.EntryTypeID = 1;
                                jDetail.BranchID = Convert.ToInt32(model.BranchID);
                                // jDetail.OrderID = model.OrderID;
                                jDetail.PayTypeID = i.PayTypeID;
                                jDetail.RefAccountID = model.AccountID;
                             
                                jDetail.UserName = UserName;

                                db.tbl_JDetail.Add(jDetail);
                                db.SaveChanges();


                                if (i.PayTypeID == 3)
                                {
                                    //                         ([JEntryId],[ChequeTypeID],[Amount],[BankName]
                                    //,[ChequeDate],[ChequeNumber],[Description]
                                    //,[IsCleared],[IsBounced], ReferenceDetail
                                    //,[AddBy],[AddOn], BranchID)

                                    tbl_Cheques chq = new tbl_Cheques();
                                    chq.JEntryId = jEntry.JEntryId;
                                    chq.ChequeTypeID = 2;
                                    chq.Amount = i.Amount;
                                    chq.BankName = BnkName;
                                    chq.ChequeDate = i.chqDate;
                                    chq.ChequeNumber = i.chqNumber;
                                    chq.Description = model.Description;
                                    chq.IsCleared = false;
                                    chq.IsBounced = false;
                                    chq.IsPDC = false;
                                    chq.ReferenceDetail = VendorMemo.ToString();
                                    chq.AddBy = AddBy;
                                    chq.AddOn = Helper.PST();
                                    chq.BranchID = model.BranchID;

                                    db.tbl_Cheques.Add(chq);
                                    db.SaveChanges();
                                }

                                else if (i.PayTypeID == 4)
                                {
                                    tbl_Cheques chq = new tbl_Cheques();
                                    chq.JEntryId = jEntry.JEntryId;
                                    chq.ChequeTypeID = 3;
                                    chq.Amount = i.Amount;
                                    chq.BankName = "";
                                    chq.ChequeDate = i.chqDate;
                                    chq.ChequeNumber = i.chqNumber;
                                    // chq.ChequeBank = BnkName;
                                    // chq.DepositBankAccountID = BankAccountID;
                                    chq.Description = "chq in hand pay method-" + model.Description;
                                    chq.IsCleared = false;
                                    chq.IsBounced = false;
                                    chq.IsPDC = false;

                                    jDetail.PayTypeID = i.PayTypeID;
                                    chq.ReferenceDetail = "PDC Payment-" + VendorMemo.ToString();
                                    chq.AddBy = AddBy;
                                    chq.AddOn = Helper.PST();
                                    chq.BranchID = model.BranchID;

                                    db.tbl_Cheques.Add(chq);
                                    db.SaveChanges();
                                }
                                else if (i.PayTypeID == 5)
                                {
                                    //                         ([JEntryId],[ChequeTypeID],[Amount],[BankName]
                                    //,[ChequeDate],[ChequeNumber],[Description]
                                    //,[IsCleared],[IsBounced], ReferenceDetail
                                    //,[AddBy],[AddOn], BranchID)

                                    tbl_Cheques chq = new tbl_Cheques();
                                    chq.JEntryId = jEntry.JEntryId;
                                    chq.ChequeTypeID = 2;
                                    chq.Amount = i.Amount;
                                    chq.BankName = BnkName;
                                    chq.ChequeDate = i.chqDate;
                                    chq.ChequeNumber = i.chqNumber;
                                    chq.Description = "other pay method-" + model.Description;
                                    chq.IsCleared = false;
                                    chq.IsBounced = false;
                                    chq.IsPDC = false;
                                    chq.isOther = true;
                                    chq.ReferenceDetail = VendorMemo.ToString();
                                    chq.AddBy = AddBy;
                                    chq.AddOn = Helper.PST();
                                    chq.BranchID = model.BranchID;

                                    db.tbl_Cheques.Add(chq);
                                    db.SaveChanges();
                                }




                                #endregion

                            }


                        }






                        foreach (var item in jEntryLog)
                        {
                            item.JEntryID = jEntryID ?? 0; // get it from SP                                                
                            item.BranchID = branchId;
                        }
                        Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                        var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jEntryLog);
                        foreach (var item in jLog)
                        {
                            db.tbl_JEntryLog.Add(item);
                        }
                    }
                    else
                    {
                        transaction.Rollback();
                        return "false";
                    }

                    //////////////////////////////////////////


                    //if (model.ChequeDate > model.VoucherDate && model.PaymentTypeID > 2)
                    //{
                    //  //  jEntryID = db.insertVendorPDCGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, VendorMemo.ToString(), model.Description, bankAccId, model.BranchID, NewCode, NewCodeSt, UserName).FirstOrDefault();
                    //}
                    //else
                    //{
                    //  //  jEntryID = db.insertVendorGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, VendorMemo.ToString(), model.Description, bankAccId, model.BranchID, NewCode, NewCodeSt, UserName).FirstOrDefault();
                    //}
                    //foreach (var item in jEntryLog)
                    //{
                    //    item.JEntryID = jEntryID ?? 0; // get it from SP                                               
                    //    item.BranchID = branchId;
                    //}
                    //Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                    //var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jEntryLog);
                    //foreach (var item in jLog)
                    //{
                    //    db.tbl_JEntryLog.Add(item);
                    //}
                    db.SaveChanges();
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }




    }
}
