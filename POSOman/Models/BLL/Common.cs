﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace POSOman.Models.BLL
{
    public class Common
    {
        private static dbPOS db = new dbPOS();

        public static int GetInvoiceNoEmployeePurchase()
        {
            int newSOID = 20001;

            
                var data = db.tbl_EmployeePurchases.Where(so => so.IsDeleted !=true ).OrderByDescending(v => v.OrderID).Select(x => x.EPOID).FirstOrDefault();
                if (data<=0)
                {
                    return newSOID;
                }
                else
                {
                    newSOID = Convert.ToInt32(data) + 1;
                }

                  return (newSOID);

        }

        public int GetBranchID()
        {
            int branchId = Convert.ToInt32(HttpContext.Current.Session["BranchID"]);

            if(branchId > 0)
            {
                return branchId;
            }
            else
            {
                string userid = HttpContext.Current.User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == userid);
                branchId = currentUser.BranchID;
            }

            return branchId;
        }

        public int GetUserID()
        {
            int userID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);

            if (userID > 0)
            {
                return userID;
            }
            else
            {
                string userid = HttpContext.Current.User.Identity.GetUserId();
                var currentUser = db.AspNetUsers.FirstOrDefault(x => x.Id == userid);
                userID = currentUser.UserId;
            }

            return userID;
        }

        public static string GetAccBalance(int? AccID)
        {
            //dbPOS db = new dbPOS();
            //List<GetAccountBalances_Result> data = db.GetAccountBalances(null, null, null, null, AccID).ToList();
            List<GetAccountBalancesIDWise_Result> data = db.GetAccountBalancesIDWise(null, null, null, null, AccID).ToList();

            if (data != null && data.Count>0)
            {
                decimal total = 0;
                foreach(var i in data)
                {
                    decimal IndTotal = 0;
                    if (i.Balance.Contains("("))
                    {
                       
                        string bal = i.Balance.Substring(1, i.Balance.Length - 2);
                        if(Decimal.TryParse(bal,out IndTotal))
                        {
                            total += IndTotal;
                        }
                    }
                    else
                    {
                        if (Decimal.TryParse(i.Balance, out IndTotal))
                        {
                            total += IndTotal;
                        }

                    }
                   
                }
                return total.ToString();
            }
            else
            {
                return "0";
            }
            
        }

        public static decimal GetAccTypeWiseBalance(int? AccTypeID)
        {
            
            //if(AccTypeID == 1 || AccTypeID == 25 || AccTypeID == 26)
            //{

            //}
            //List<GetAccountBalances_Result> data = db.GetAccountBalances(null, null, null, AccTypeID, null).ToList();
            //decimal bal = 0;
            //if (data.Count > 0)
            //{
            //    bal = data.Sum(a => a.Balance);
            //}

            //return bal;
            decimal Total = 0;
            Stack<int> AllAccTypesIDs = new Stack<int>();
            AllAccTypesIDs.Push(AccTypeID??0);
           
            while (AllAccTypesIDs.Count > 0)
            {
                dynamic data = null;
                int AccountTypeID = AllAccTypesIDs.Pop();
                string bal = string.Empty;
                 data = db.GetAccountBalancesIDWise(null, null, null, AccountTypeID, null).Select(x => x.Balance).ToList();
                if(data!=null && data.Count > 0)
                {
                    foreach(var i in data)
                    {
                        if (!string.IsNullOrWhiteSpace(i))
                        {
                            bal = i.ToString();
                            decimal indTotal = 0;
                            if (bal.Contains("("))
                            {
                                bal = bal.Substring(1, bal.Length - 2);

                                if (decimal.TryParse(bal.ToString(), out indTotal))
                                {
                                    Total += indTotal;
                                }
                            }
                            else
                            {
                                if (decimal.TryParse(bal.ToString(), out indTotal))
                                {
                                    Total += indTotal;
                                }
                            }


                        }
                    }
                }
                //else
                //{
                //    data = db.GetAccountBalancesIDWiseForAccountTypesOtherThanAssetsAndLiabilitiesUsedInCOA(null, null, null, AccountTypeID, null).Select(x => x.Balance).ToList() ;

                //    if (data != null && data.Count > 0)
                //    {
                //        foreach (var i in data)
                //        {
                //            if (!string.IsNullOrWhiteSpace(i))
                //            {
                //                bal = i.ToString();
                //                decimal indTotal = 0;
                //                if (bal.Contains("("))
                //                {
                //                    bal = bal.Substring(1, bal.Length - 2);

                //                    if (decimal.TryParse(bal.ToString(), out indTotal))
                //                    {
                //                        Total += indTotal;
                //                    }
                //                }
                //                else
                //                {
                //                    if (decimal.TryParse(bal.ToString(), out indTotal))
                //                    {
                //                        Total += indTotal;
                //                    }
                //                }


                //            }
                //        }
                //    }
                //}
                
                var ChildAccTypesIDs = db.tbl_AccountType.Where(x => x.HeadID == AccountTypeID).Select(x => x.AccountTypeID).ToList();
               
                if(ChildAccTypesIDs != null && ChildAccTypesIDs.Count > 0)
                {
                    foreach(var i in ChildAccTypesIDs)
                    {
                        AllAccTypesIDs.Push(i);
                    }
                    
                }
            }

            return Total;
           
        }

        public async Task<object> RegUserAPI(string UserName,string Name,  string Cell, string Pass)
        {
            try
            {

                string Msg = "0";
               
                IEnumerable<KeyValuePair<string, string>> queries = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("UserName",UserName),
                new KeyValuePair<string, string>("UserName",Name),
                new KeyValuePair<string, string>("Email",UserName.ToString()+"@gmail.com"),
                new KeyValuePair<string, string>("Password",Pass),
                new KeyValuePair<string, string>("ConfirmPassword",Pass),
                new KeyValuePair<string, string>("Cell",Cell.ToString()),
                new KeyValuePair<string, string>("UserRoles","SalePerson")

            };

                HttpContent q = new FormUrlEncodedContent(queries);
                using (HttpClient client = new HttpClient())
                {
                    string BaseURL = WebConfigurationManager.AppSettings["Url"];

                    var url = BaseURL + "/api/Account/RegisterUser";
                    using (HttpResponseMessage response = await client.PostAsync(url, q))
                    {
                        using (HttpContent content = response.Content)
                        {
                            string myContent = await content.ReadAsStringAsync();
                            HttpContentHeaders headers = content.Headers;

                            Msg = myContent.ToString();

                            ErrorTracer err = new ErrorTracer();
                            err.vErrorMsg = "Register User-->" + myContent.ToString();
                            err.vErrorLine = 508;
                            err.dErrorDate = Helper.PST();
                            db.ErrorTracers.Add(err);
                            db.SaveChanges();
                            return Msg;

                        }
                    }

                }

            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return e.Message;
            }

        }

    }
}