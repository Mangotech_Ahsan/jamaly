﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace POSOman.Models.BLL
{
    public class CustomerPayment
    {
        //without invoices
        public object SaveSinglePayment(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    model.ChequeDate = model.VoucherDate;
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                        model.ChequeDate = null;
                        model.ChequeNumber = null;
                    }
                    if (model.PaymentTypeID == 2)
                    {
                        model.ChequeDate = null;
                        model.ChequeNumber = null;
                    }
                    //model.IsSettleEntry = true;
                    model.TotalBalanceSettled = 0;
                    model.CustomerRefAccID = model.AccountID;

                    StringBuilder CustomerMemo = new StringBuilder();
                    StringBuilder PaymentMemo = new StringBuilder();
                    CustomerMemo.Append(model.Description);
                    db.Configuration.ValidateOnSaveEnabled = false;


                    //if (model.ChequeDate > model.VoucherDate)
                    //{
                    //    jEntryID = db.insertCustomerGJPDCEntryWithOutInvoices(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, null, CustomerMemo.ToString(), model.Description, bankAccId, branchId, 0).FirstOrDefault();
                    //}
                    //else
                    //{
                    if (model.IsSettleEntry == true)
                    {
                        jEntryID = db.insertCustomerGJEntryWithOutInvoices_IsSettleLater(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, 0).FirstOrDefault();
                    }
                    else
                    {
                        jEntryID = db.insertCustomerGJEntryWithOutInvoices(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, 0).FirstOrDefault();
                    }
                    //}


                    foreach (var item in jentryLog)
                    {
                        item.JEntryID = jEntryID ?? 0; // get it from SP                                                
                        item.BranchID = branchId;
                    }
                    Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                    var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jentryLog);
                    foreach (var item in jLog)
                    {
                        db.tbl_JEntryLog.Add(item);
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return jEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        //  SettleAmount for customer advance
        public object SettleAmount(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId, int branchId, int CustJEntryID)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                        model.ChequeDate = null;
                    }
                    if (model.PaymentTypeID == 2)
                    {
                        model.ChequeDate = null;
                    }

                    var PrevInv = db.tbl_JEntry.Where(x => x.JEntryId == CustJEntryID).FirstOrDefault();

                    if (PrevInv != null)
                    {
                        PrevInv.TotalBalanceSettled += model.Amount;
                        db.Entry(PrevInv).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    var orderID = jentryLog[0].OrderID;
                    model.OrderID = Convert.ToInt32(orderID);
                    StringBuilder CustomerMemo = new StringBuilder();
                    StringBuilder PaymentMemo = new StringBuilder();
                    db.Configuration.ValidateOnSaveEnabled = false;
                    // get all Order ids 
                    List<int> lstOrderIds = model.Details.Select(p => p.OrderID).ToList();
                    // get all SaleeOrders where OrderID contains lstOrderIds
                    var lstOrders = db.tbl_SalesOrder.Where(s => lstOrderIds.Contains(s.OrderID)).ToList();
                    List<tbl_SalesOrder> existingOrders = lstOrders.ToList();
                    //
                    if (lstOrderIds.Count > 0)
                    {
                        //if (model.ChequeDate > model.VoucherDate)
                        //{
                        //    CustomerMemo.Append("PDC Received for Invoice No: ");   // change po no by system gen id here 
                        //}
                        //else
                        //{
                        CustomerMemo.Append("Received for Invoice No: ");   // change po no by system gen id here 
                        //}
                    }
                    foreach (var item in model.Details)
                    {
                        lstOrderIds.Add(item.OrderID);
                        //  if already exists 
                        var row = existingOrders.FirstOrDefault(s => s.OrderID == item.OrderID);
                        if (row != null)
                        {
                            CustomerMemo.Append(row.SOID + ",");
                            decimal? totalPaid = row.TotalPaid + item.Receiving;
                            row.TotalPaid += item.Receiving;// enter totalPaid = 0 at time of order
                            decimal totalPayable = (row.TotalAmount + (row.VAT)) - row.ReturnAmount ?? 0;
                            if (totalPaid == totalPayable)
                            {
                                row.PaymentStatus = "Paid";
                                row.IsPaid = true;
                            }
                            else if (row.AmountPaid < totalPayable)
                            {
                                row.PaymentStatus = "Partial Paid";
                            }
                            //row.Can_Modify = false;
                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(row).State = EntityState.Modified;
                        }
                    }
                    //if (model.ChequeDate > model.VoucherDate && model.PaymentTypeID > 2)
                    //{
                    //    jEntryID = db.insertCustomerGJPDCEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, null, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID).FirstOrDefault();
                    //}
                    //else
                    //{
                    //    jEntryID = db.insertCustomerGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID).FirstOrDefault();
                    //}
                    foreach (var item in jentryLog)
                    {
                        item.JEntryID = CustJEntryID;// jEntryID ?? 0; // get it from SP                                                
                        item.BranchID = branchId;
                    }
                    Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                    var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jentryLog);
                    foreach (var item in jLog)
                    {
                        db.tbl_JEntryLog.Add(item);
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return CustJEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        public object SaveChallanPayment(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                        model.ChequeDate = null;
                    }
                    if (model.PaymentTypeID == 2)
                    {
                        model.ChequeDate = null;
                    }
                    var orderID = jentryLog[0].OrderID;
                    model.OrderID = Convert.ToInt32(orderID);
                    StringBuilder CustomerMemo = new StringBuilder();
                    StringBuilder PaymentMemo = new StringBuilder();
                    db.Configuration.ValidateOnSaveEnabled = false;
                    // get all Order ids 
                    List<int> lstOrderIds = model.Details.Select(p => p.OrderID).ToList();
                    // get all SaleeOrders where OrderID contains lstOrderIds
                    var lstOrders = db.tbl_Challan.Where(s => lstOrderIds.Contains(s.ChallanID)).ToList();
                    List<tbl_Challan> existingOrders = lstOrders.ToList();
                    //
                    if (lstOrderIds.Count > 0)
                    {
                        if (model.ChequeDate > model.VoucherDate)
                        {
                            CustomerMemo.Append("PDC Received for Challan Invoice No: ");   // change po no by system gen id here 
                        }
                        else
                        {
                            CustomerMemo.Append("Received for Challan Invoice No: ");   // change po no by system gen id here 
                        }
                    }
                    foreach (var item in model.Details)
                    {
                        lstOrderIds.Add(item.OrderID);
                        //  if already exists 
                        var row = existingOrders.FirstOrDefault(s => s.ChallanID == item.OrderID);
                        if (row != null)
                        {
                            CustomerMemo.Append(row.SOID + ",");
                            decimal? totalPaid = row.TotalPaid + item.Receiving;
                            row.TotalPaid += item.Receiving;// enter totalPaid = 0 at time of order
                            decimal totalPayable = (row.TotalAmount + (row.TaxAmount)) ?? 0;
                            if (totalPaid == totalPayable)
                            {
                                row.PaymentStatus = "Paid";
                                row.IsPaid = true;
                            }
                            else if (row.AmountPaid < totalPayable)
                            {
                                row.PaymentStatus = "Partial Paid";
                            }
                            // row.Can_Modify = false;
                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(row).State = EntityState.Modified;
                        }
                    }
                    if (model.ChequeDate > model.VoucherDate && model.PaymentTypeID > 2)
                    {
                        //jEntryID = db.insertCustomerGJPDCEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, null, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID).FirstOrDefault();
                        jEntryID = db.insertCustomerGJPDCEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID, null, null, null, string.Empty, null, string.Empty, string.Empty, string.Empty, 8).FirstOrDefault();
                    }
                    else
                    {
                        jEntryID = db.insertCustomerGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID).FirstOrDefault();
                    }
                    foreach (var item in jentryLog)
                    {
                        item.JEntryID = jEntryID ?? 0; // get it from SP                                                
                        item.BranchID = branchId;
                    }
                    Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                    var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jentryLog);
                    foreach (var item in jLog)
                    {
                        db.tbl_JEntryLog.Add(item);
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return jEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

        //with invoices
        public object Save(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog, int? bankAccId, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {   // with settle entries

                    if (model.JEntryID.HasValue && model.JEntryID > 0)
                    {
                        var jentry = db.tbl_JEntry.Where(x => x.JEntryId == model.JEntryID).FirstOrDefault();

                        if (jentry != null && !string.IsNullOrWhiteSpace(jentry.PaymentType))
                        {
                            model.PaymentTypeID = Convert.ToInt32(jentry.PaymentType);
                            model.VoucherDate = jentry.VoucherDate.Value;

                            if (model.PaymentTypeID != 1)
                            {
                                //bankAccId = jentry.tbl_JDetail.Where(x => x.RefAccountID == jentry.CustomerRefAccID).Select(x => x.AccountID).FirstOrDefault();
                                bankAccId = jentry.BankAccID;
                                model.ChequeNumber = jentry.ChequeNumber;
                                model.BankName = jentry.BankName;
                                model.ChequeDate = model.VoucherDate;// jentry.ChequeDate;
                            }

                            if (model.PaymentTypeID == 1)
                            {
                                model.BankName = "";
                                model.ChequeDate = null;
                            }
                            if (model.PaymentTypeID == 2)
                            {
                                model.ChequeDate = null;
                            }
                            var orderID = jentryLog[0].OrderID;
                            model.OrderID = Convert.ToInt32(orderID);

                            StringBuilder CustomerMemo = new StringBuilder();
                            StringBuilder InvoiceIds = new StringBuilder();
                            StringBuilder PaymentMemo = new StringBuilder();
                            StringBuilder DiscountMemo = new StringBuilder();
                            StringBuilder TaxMemo = new StringBuilder();
                            StringBuilder WHTMemo = new StringBuilder();
                            StringBuilder AdjustmentMemo = new StringBuilder();

                            db.Configuration.ValidateOnSaveEnabled = false;
                            // get all Order ids 
                            List<int> lstOrderIds = model.Details.Select(p => p.OrderID).ToList();
                            // get all SaleeOrders where OrderID contains lstOrderIds
                            var lstOrders = db.tbl_SalesOrder.Where(s => lstOrderIds.Contains(s.OrderID)).ToList();
                            List<tbl_SalesOrder> existingOrders = lstOrders.ToList();
                            //
                            if (lstOrderIds.Count > 0)
                            {
                                //if (model.ChequeDate > model.VoucherDate)
                                //{
                                //    CustomerMemo.Append("PDC Received for Invoice No: ");   // change po no by system gen id here 
                                //    DiscountMemo.Append("Discounts for Invoice No: ");
                                //    TaxMemo.Append("Taxes for Invoice No: ");
                                //    WHTMemo.Append("WHT for Invoice No: ");
                                //    AdjustmentMemo.Append("Adjustments for Invoice No: ");
                                //}
                                //else
                                //{
                                CustomerMemo.Append("Received for Invoice No: ");   // change po no by system gen id here 
                                DiscountMemo.Append("Discounts for Invoice No: ");
                                TaxMemo.Append("Taxes for Invoice No: ");
                                WHTMemo.Append("WHT for Invoice No: ");
                                AdjustmentMemo.Append("Adjustments for Invoice No: ");
                                //}
                            }

                            decimal totalDiscount = 0;
                            decimal totalTax = 0;
                            decimal totalWHT = 0;
                            decimal totalAdjustment = 0;
                            decimal GrandTotal = 0;

                            foreach (var item in model.Details)
                            {
                                //lstOrderIds.Add(item.OrderID);
                                //  if already exists 
                                var row = existingOrders.FirstOrDefault(s => s.OrderID == item.OrderID);
                                if (row != null)
                                {
                                    InvoiceIds.Append(row.OrderID + "|");
                                    CustomerMemo.Append(row.SOID + ",");
                                    DiscountMemo.Append(row.SOID + ",");
                                    TaxMemo.Append(row.SOID + ",");
                                    WHTMemo.Append(row.SOID + ",");
                                    AdjustmentMemo.Append(row.SOID + ",");

                                    decimal ReceivingWithWHT = item.Receiving + item.WHT;
                                    decimal totalPaid = Convert.ToDecimal(row.TotalPaid) + item.Receiving;
                                    row.TotalPaid += totalPaid;// enter totalPaid = 0 at time of order
                                                               //decimal totalPayable = (row.TotalAmount + (row.VAT)) - row.ReturnAmount??0;
                                    decimal totalPayable = ((row.TotalAmount - row.ReturnAmount) - item.WHT ?? 0) - item.Discount + item.Adjustment; // vat already in total amount
                                    decimal totalReceiving = totalPaid - item.Discount + item.Adjustment; // vat already in total amount

                                    GrandTotal += totalReceiving;

                                    if (totalPaid == totalPayable)
                                    {
                                        row.PaymentStatus = "Paid";
                                        row.IsPaid = true;
                                        //row.Can_Modify = false;
                                    }
                                    else if (row.AmountPaid < totalPayable)
                                    {
                                        row.PaymentStatus = "Partial Paid";
                                    }
                                    //row.Can_Modify = false;

                                    if (item.Discount > 0)
                                    {
                                        totalDiscount += (row.DiscountAmount == 0) ? item.Discount : 0;
                                        row.DiscountAmount += (row.DiscountAmount == 0) ? item.Discount : 0;
                                    }
                                    if (item.WHT > 0)
                                    {
                                        //totalWHT += (row.Tax == 0 ) ? item.WHT : 0;
                                        totalWHT += item.WHT;
                                        row.Tax = row.Tax.HasValue ? row.Tax += item.WHT : row.Tax = item.WHT;
                                    }
                                    if (item.Adjustment > 0)
                                    {
                                        totalAdjustment += (row.Adjustment == 0 || row.Adjustment == null) ? item.Adjustment : 0;
                                        //row.Adjustment += (row.Adjustment == 0 || row.Adjustment == null) ? item.Adjustment : 0;
                                        row.Adjustment = row.Adjustment.HasValue ? row.Adjustment += item.Adjustment : row.Adjustment = item.Adjustment;
                                        row.AdjustmentDescription = (row.Adjustment == 0 || row.Adjustment == null) ? item.AdjustmentDescription : "-";
                                    }
                                    if (item.Tax > 0)
                                    {
                                        totalTax += (row.VAT == 0) ? item.Tax : 0;
                                        row.VAT += (row.VAT == 0) ? item.Tax : 0;
                                        row.VATPercent = (row.VATPercent == 0 || row.VATPercent == null) ? item.TaxPercent : 0;
                                        row.VATType = (row.VAT == 0) ? item.TaxType : null;
                                    }
                                    row.UpdateOn = Helper.PST();
                                    db.Entry(row).State = EntityState.Modified;
                                }
                            }


                            foreach (var i in jentry.tbl_JDetail.Where(x => x.AccountID == model.AccountID).ToList())
                            {
                                i.Detail += (model.PaymentTypeID == 2 ? " | Bank: " + model.BankName : model.PaymentTypeID == 3 ? " | Chq#: " + jentry.ChequeNumber : "Cash Payment") + " | " + CustomerMemo.ToString();
                                db.Entry(i).State = EntityState.Modified;
                            }

                            foreach (var log in jentry.tbl_JEntryLog.Where(x => x.OrderID == 0).ToList())
                            {
                                log.MultiInvoiceOrderIDs += InvoiceIds.ToString();
                                db.Entry(log).State = EntityState.Modified;
                            }

                            jEntryID = db.insertCustomerGJEntryWithTaxAdjustmentAndDiscount_ForSettleEntryAsExpense(GrandTotal, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID, totalDiscount, totalWHT, totalAdjustment, AdjustmentMemo.ToString(), totalTax, TaxMemo.ToString(), DiscountMemo.ToString(), WHTMemo.ToString(), 8, null).FirstOrDefault();

                            //int? settlementJentryID = db.insertCustomerGJEntryWithSettlementEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, jentry.JEntryId).FirstOrDefault();

                            //if (model.ChequeDate > model.VoucherDate && model.PaymentTypeID > 2)
                            //{
                            //    jEntryID = db.insertCustomerGJPDCEntry(GrandTotal, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID, totalDiscount, totalWHT, totalAdjustment, AdjustmentMemo.ToString(), totalTax, TaxMemo.ToString(), DiscountMemo.ToString(), WHTMemo.ToString(), 8).FirstOrDefault();
                            //}
                            //else
                            //{
                            //    jEntryID = db.insertCustomerGJEntryWithTaxAdjustmentAndDiscount(GrandTotal, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID, totalDiscount, totalWHT, totalAdjustment, AdjustmentMemo.ToString(), totalTax, TaxMemo.ToString(), DiscountMemo.ToString(), WHTMemo.ToString(), 8, null).FirstOrDefault();
                            //}

                            ////////////////
                            foreach (var item in jentryLog)
                            {
                                item.JEntryID = jEntryID ?? 0; // get it from SP                                                
                                item.BranchID = branchId;
                            }
                            Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                            var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jentryLog);
                            foreach (var item in jLog)
                            {
                                db.tbl_JEntryLog.Add(item);
                            }
                            ////////////////
                            //if(jentry.Amount <= GrandTotal)
                            //{
                            //    jentry.UpdateOn = Helper.PST();

                            //    jentry.IsSettleEntry = false;
                            //    if (!string.IsNullOrWhiteSpace(jentry.ReferenceDetail))
                            //    {
                            //        jentry.ReferenceDetail = jentry.ReferenceDetail+ " | Fully Settled Against EntryID: " + jEntryID.ToString();

                            //    }
                            //    else
                            //    {
                            //        jentry.ReferenceDetail = "Fully Settled Against EntryID: " + jEntryID.ToString();

                            //    }
                            //    db.Entry(jentry).State = EntityState.Modified;
                            //    db.SaveChanges();
                            //}
                            //else if(jentry.Amount > GrandTotal)
                            //{
                            //    jentry.TotalBalanceSettled += GrandTotal;
                            //    jentry.Amount -= GrandTotal;
                            //    jentry.UpdateOn = Helper.PST();

                            //    jentry.IsSettleEntry = true;
                            //    if (!string.IsNullOrWhiteSpace(jentry.ReferenceDetail))
                            //    {
                            //        jentry.ReferenceDetail = jentry.ReferenceDetail + " | Partial Settled Against EntryID: " + jEntryID.ToString();

                            //    }
                            //    else
                            //    {
                            //        jentry.ReferenceDetail = "Partial Settled Against EntryID: " + jEntryID.ToString();

                            //    }

                            //    db.Entry(jentry).State = EntityState.Modified;
                            //    db.SaveChanges();
                            //}
                            jentry.SettleEntryID = jEntryID;
                            jentry.UpdateOn = Helper.PST();
                            db.Entry(jentry).State = EntityState.Modified;
                            db.SaveChanges();
                            transaction.Commit();

                            return jEntryID;


                        }



                    }

                    // without settle entries
                    else
                    {
                        if (model.PaymentTypeID == 1)
                        {
                            model.BankName = "";
                            model.ChequeDate = null;
                        }
                        if (model.PaymentTypeID == 2)
                        {
                            model.ChequeDate = null;
                        }
                        var orderID = jentryLog[0].OrderID;
                        model.OrderID = Convert.ToInt32(orderID);

                        StringBuilder CustomerMemo = new StringBuilder();
                        StringBuilder PaymentMemo = new StringBuilder();
                        StringBuilder DiscountMemo = new StringBuilder();
                        StringBuilder TaxMemo = new StringBuilder();
                        StringBuilder WHTMemo = new StringBuilder();
                        StringBuilder AdjustmentMemo = new StringBuilder();

                        db.Configuration.ValidateOnSaveEnabled = false;
                        // get all Order ids 
                        List<int> lstOrderIds = model.Details.Select(p => p.OrderID).ToList();
                        // get all SaleeOrders where OrderID contains lstOrderIds
                        var lstOrders = db.tbl_SalesOrder.Where(s => lstOrderIds.Contains(s.OrderID)).ToList();
                        List<tbl_SalesOrder> existingOrders = lstOrders.ToList();
                        //
                        if (lstOrderIds.Count > 0)
                        {
                            if (model.ChequeDate > model.VoucherDate)
                            {
                                CustomerMemo.Append("PDC Received for Invoice No: ");   // change po no by system gen id here 
                                DiscountMemo.Append("Discounts for Invoice No: ");
                                TaxMemo.Append("Taxes for Invoice No: ");
                                WHTMemo.Append("WHT for Invoice No: ");
                                AdjustmentMemo.Append("Adjustments for Invoice No: ");
                            }
                            else
                            {
                                CustomerMemo.Append("Received for Invoice No: ");   // change po no by system gen id here 
                                DiscountMemo.Append("Discounts for Invoice No: ");
                                TaxMemo.Append("Taxes for Invoice No: ");
                                WHTMemo.Append("WHT for Invoice No: ");
                                AdjustmentMemo.Append("Adjustments for Invoice No: ");
                            }
                        }

                        decimal totalDiscount = 0;
                        decimal totalTax = 0;
                        decimal totalWHT = 0;
                        decimal totalAdjustment = 0;

                        foreach (var item in model.Details)
                        {
                            //lstOrderIds.Add(item.OrderID);
                            //  if already exists 
                            var row = existingOrders.FirstOrDefault(s => s.OrderID == item.OrderID);
                            if (row != null)
                            {
                                CustomerMemo.Append(row.SOID + ",");
                                DiscountMemo.Append(row.SOID + ",");
                                TaxMemo.Append(row.SOID + ",");
                                WHTMemo.Append(row.SOID + ",");
                                AdjustmentMemo.Append(row.SOID + ",");

                                //decimal ReceivingWithWHT = item.Receiving + item.WHT;
                                //decimal? totalPaid = row.TotalPaid + ReceivingWithWHT;
                                //row.TotalPaid += ReceivingWithWHT;// enter totalPaid = 0 at time of order
                                //                                  //decimal totalPayable = (row.TotalAmount + (row.VAT)) - row.ReturnAmount??0;
                                //decimal totalPayable = ((row.TotalAmount - row.ReturnAmount) ?? 0) - item.Discount + item.Adjustment; // vat already in total amount

                                decimal ReceivingWithWHT = item.Receiving + item.WHT;
                                decimal totalPaid = Convert.ToDecimal(row.TotalPaid) + item.Receiving;
                                row.TotalPaid += totalPaid;// enter totalPaid = 0 at time of order
                                                           //decimal totalPayable = (row.TotalAmount + (row.VAT)) - row.ReturnAmount??0;
                                decimal totalPayable = ((row.TotalAmount - row.ReturnAmount) - item.WHT ?? 0) - item.Discount + item.Adjustment; // vat already in total amount
                                decimal totalReceiving = totalPaid - item.Discount + item.Adjustment; // vat already in total amount

                                if (totalPaid == totalPayable)
                                {
                                    row.PaymentStatus = "Paid";
                                    row.IsPaid = true;
                                    //row.Can_Modify = false;
                                }
                                else if (row.AmountPaid < totalPayable)
                                {
                                    row.PaymentStatus = "Partial Paid";
                                }
                                //row.Can_Modify = false;

                                if (item.Discount > 0)
                                {
                                    totalDiscount += (row.DiscountAmount == 0) ? item.Discount : 0;
                                    row.DiscountAmount += (row.DiscountAmount == 0) ? item.Discount : 0;
                                }
                                if (item.WHT > 0)
                                {
                                    //totalWHT += (row.Tax == 0 ) ? item.WHT : 0;
                                    totalWHT += item.WHT;
                                    row.Tax = row.Tax.HasValue ? row.Tax += item.WHT : row.Tax = item.WHT;
                                }
                                if (item.Adjustment > 0)
                                {
                                    totalAdjustment += (row.Adjustment == 0 || row.Adjustment == null) ? item.Adjustment : 0;
                                    //row.Adjustment += (row.Adjustment == 0 || row.Adjustment == null) ? item.Adjustment : 0;
                                    row.Adjustment = row.Adjustment.HasValue ? row.Adjustment += item.Adjustment : row.Adjustment = item.Adjustment;
                                    row.AdjustmentDescription = (row.Adjustment == 0 || row.Adjustment == null) ? item.AdjustmentDescription : "-";
                                }
                                if (item.Tax > 0)
                                {
                                    totalTax += (row.VAT == 0) ? item.Tax : 0;
                                    row.VAT += (row.VAT == 0) ? item.Tax : 0;
                                    row.VATPercent = (row.VATPercent == 0 || row.VATPercent == null) ? item.TaxPercent : 0;
                                    row.VATType = (row.VAT == 0) ? item.TaxType : null;
                                }
                                row.UpdateOn = Helper.PST();
                                db.Entry(row).State = EntityState.Modified;
                            }
                        }

                        if (model.ChequeDate > model.VoucherDate && model.PaymentTypeID > 2)
                        {
                            //jEntryID = db.insertCustomerGJPDCEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, null, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID).FirstOrDefault();
                            jEntryID = db.insertCustomerGJPDCEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID, totalDiscount, totalWHT, totalAdjustment, AdjustmentMemo.ToString(), totalTax, TaxMemo.ToString(), DiscountMemo.ToString(), WHTMemo.ToString(), 8).FirstOrDefault();
                        }
                        else
                        {
                            jEntryID = db.insertCustomerGJEntryWithTaxAdjustmentAndDiscount(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, CustomerMemo.ToString(), model.Description, bankAccId, branchId, model.OrderID, totalDiscount, totalWHT, totalAdjustment, AdjustmentMemo.ToString(), totalTax, TaxMemo.ToString(), DiscountMemo.ToString(), WHTMemo.ToString(), 8, null).FirstOrDefault();
                        }
                        foreach (var item in jentryLog)
                        {
                            item.JEntryID = jEntryID ?? 0; // get it from SP                                                
                            item.BranchID = branchId;
                        }
                        Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                        var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jentryLog);
                        foreach (var item in jLog)
                        {
                            db.tbl_JEntryLog.Add(item);
                        }
                        db.SaveChanges();
                        transaction.Commit();
                        return jEntryID;
                    }
                    return "Jentry ID not found";

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        // Delete Entry(s) of Customer Payments 
        public object DeleteEntry(int JEntryID)
        {
            dbPOS db = new dbPOS();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    ICollection<tbl_JEntryLog> jEntryLog = db.tbl_JEntryLog.Where(j => j.JEntryID == JEntryID).ToList();
                    Mapper.CreateMap<tbl_JEntryLog, Models.DTO.JEntryLog>();
                    var jLog = Mapper.Map<ICollection<tbl_JEntryLog>, ICollection<Models.DTO.JEntryLog>>(jEntryLog);
                    StringBuilder CustomerMemo = new StringBuilder();
                    StringBuilder PaymentMemo = new StringBuilder();
                    db.Configuration.ValidateOnSaveEnabled = false;
                    #region Update SalesOrder 
                    foreach (var item in jLog)
                    {
                        var row = db.tbl_SalesOrder.FirstOrDefault(so => so.OrderID == item.OrderID);//&& so.BranchID == item.BranchID);
                        if (row != null)
                        {
                            row.TotalPaid -= Convert.ToDecimal(item.Amount);
                            if (row.TotalPaid == 0)
                            {
                                row.PaymentStatus = "UnPaid";
                                row.IsPaid = false;
                            }
                            else if (row.TotalPaid < row.TotalAmount)
                            {
                                row.PaymentStatus = "Partial Paid";
                                row.IsPaid = false;
                            }
                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(row).State = EntityState.Modified;
                        }
                    }
                    #endregion
                    #region Reverse Journal Entry 
                    db.DeleteCustomerPayment(JEntryID);
                    #endregion
                    db.SaveChanges();
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        public object DeleteSpecialDiscount(int JEntryID)
        {
            dbPOS db = new dbPOS();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    ICollection<tbl_JEntryLog> jEntryLog = db.tbl_JEntryLog.Where(j => j.JEntryID == JEntryID).ToList();
                    Mapper.CreateMap<tbl_JEntryLog, Models.DTO.JEntryLog>();
                    var jLog = Mapper.Map<ICollection<tbl_JEntryLog>, ICollection<Models.DTO.JEntryLog>>(jEntryLog);
                    db.Configuration.ValidateOnSaveEnabled = false;
                    #region Update SalesOrder 
                    foreach (var item in jLog)
                    {
                        var row = db.tbl_SalesOrder.FirstOrDefault(so => so.OrderID == item.OrderID);//&& so.BranchID == item.BranchID);
                        if (row != null)
                        {
                            row.TotalPaid -= Convert.ToDecimal(item.Amount);
                            row.SpecialDiscount -= Convert.ToDecimal(item.Amount);
                            if (row.TotalPaid == 0)
                            {
                                row.PaymentStatus = "UnPaid";
                                row.IsPaid = false;
                            }
                            else if (row.TotalPaid < row.TotalAmount)
                            {
                                row.PaymentStatus = "Partial Paid";
                                row.IsPaid = false;
                            }
                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(row).State = EntityState.Modified;
                        }
                    }
                    #endregion
                    #region Reverse Journal Entry 
                    db.DeleteCustomerPayment(JEntryID);
                    #endregion
                    db.SaveChanges();
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

    }
}
