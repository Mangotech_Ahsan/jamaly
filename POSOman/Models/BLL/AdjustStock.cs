﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity;

namespace POSOman.Models.BLL
{
    public class AdjustStock
    {        
        public object Save(List<Models.DTO.StockMoveOut> modelStockLogOut,int UserID)
        {
            dbPOS db = new dbPOS();

            var manager = ((IObjectContextAdapter)db).ObjectContext.ObjectStateManager;
            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (modelStockLogOut != null)
                    {
                        //tbl_StockLog stockLog;
                        //foreach (var item in modelStockLogOut)
                        //{
                        //    if (item.StockOut > 0 || item.StockIN > 0)
                        //    {
                        //        item.UserId = HttpContext.Current.User.Identity.GetUserId();
                        //        stockLog = MaptoModel(item);
                        //        stockLog.AddBy = UserID;
                        //        db.tbl_StockLog.Add(stockLog);
                        //    }
                        //}                        
                        #region Change Stock                         
                        foreach (var item in modelStockLogOut)
                        {                            
                            //  if already exists stock
                            var row = db.tbl_Stock.FirstOrDefault(s => s.ProductID == item.ProductID && s.BranchID == item.BranchID);
                            if (row != null)
                            {
                                row.Location = item.Location;
                                if(item.StockOut > 0)
                                {
                                    var rStockOut = item.StockOut ?? 0;
                                    var rowCount = 0;
                                    var stockBatchID = 0;
                                    var CostPrice = 0m;
                                    decimal stLogQty = 0;
                                    #region FIFO 
                                    var stockBatch = db.GetProductFIFO(item.ProductID, item.StockOut, item.BranchID).ToList();
                                    while (rStockOut > 0)
                                    {
                                        if (stockBatch.Count > 0 && rowCount < stockBatch.Count)
                                        {
                                            var StockBatch = stockBatch[rowCount];
                                            var sbRow = db.tbl_StockBatch.Where(b => b.StockBatchID == StockBatch.StockBatchID).FirstOrDefault();
                                            var Order = db.tbl_StockLog.Where(sl => sl.OrderTypeID == 1 && sl.StockBatchID == sbRow.StockBatchID).FirstOrDefault();

                                            if (rStockOut > StockBatch.Qty)
                                            {
                                                stLogQty = sbRow.Qty;
                                                rStockOut -= sbRow.Qty;
                                                sbRow.Qty = 0;
                                                db.Entry(sbRow).State = EntityState.Modified;

                                            }
                                            else if (rStockOut < StockBatch.Qty)
                                            {
                                                stLogQty = rStockOut;
                                                sbRow.Qty -= rStockOut;
                                                db.Entry(sbRow).State = EntityState.Modified;
                                                rStockOut = 0;
                                            }
                                            else if (rStockOut == StockBatch.Qty)
                                            {
                                                stLogQty = rStockOut;
                                                sbRow.Qty = 0;
                                                db.Entry(sbRow).State = EntityState.Modified;
                                                rStockOut = 0;
                                            }
                                            stockBatchID = sbRow.StockBatchID;
                                            CostPrice = sbRow.CostPrice;
                                            if (Order != null)
                                            {
                                                var PurchaseOrder = db.tbl_PurchaseOrder.Where(po => po.OrderID == Order.OrderID).FirstOrDefault();
                                                PurchaseOrder.Can_Modify = false;
                                                db.Entry(PurchaseOrder).State = EntityState.Modified;
                                            }

                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            stLogQty = rStockOut;
                                            rStockOut = 0;
                                        }
                                        if (stLogQty > 0)
                                        {
                                            tbl_StockLog stLog = new tbl_StockLog();
                                            stLog.StockBatchID = stockBatchID;
                                            stLog.AddBy = UserID;
                                            stLog.AddOn = DateTime.UtcNow.AddHours(5);
                                            stLog.BranchID = item.BranchID;
                                            stLog.CostPrice = CostPrice;
                                            stLog.InvoiceDate = DateTime.UtcNow.AddHours(5);
                                            stLog.IsActive = true;
                                            stLog.OrderTypeID = item.OrderTypeID;
                                            stLog.OutReference = item.OutReference;
                                            stLog.OutReferenceID = item.OutReferenceID;
                                            stLog.ProductID = item.ProductID;
                                            stLog.SalePrice = item.SalePrice;
                                            stLog.StockIN = 0;
                                            stLog.ReturnedQty = 0;
                                            stLog.StockOut = stLogQty;
                                            stLog.UnitCode = item.UnitCode;
                                            stLog.UserReferenceID = item.UserReferenceID;
                                            db.tbl_StockLog.Add(stLog);
                                            rowCount += 1;
                                        }
                                    }

                                    #endregion
                                    row.Qty -= Convert.ToInt32(item.StockOut);
                                    #region Update CostPrice in Stock 
                                    decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == item.BranchID).Sum(p => (decimal?)p.CostPrice * p.Qty) ?? 0m);
                                    int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == item.BranchID).Sum(p => (decimal?)p.Qty) ?? 0m);

                                    decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;

                                    #endregion
                                }
                                else if (item.StockIN > 0)
                                {
                                    #region Stock Batch
                                    tbl_StockBatch batch = new tbl_StockBatch();
                                    batch.BatchID = Guid.NewGuid().ToString().Substring(0,5) + "-" + item.ProductID;
                                    batch.ProductID = item.ProductID;
                                    batch.Qty = item.StockIN ?? 0;
                                    batch.CostPrice = item.CostPrice ?? db.tbl_Stock.Where(st => st.ProductID == item.ProductID).Select(st =>st.CostPrice).FirstOrDefault() ?? 0;                                    
                                    batch.BranchID = item.BranchID ?? 9001;
                                    db.tbl_StockBatch.Add(batch);
                                    db.SaveChanges();
                                    tbl_StockLog stLog = new tbl_StockLog();
                                    stLog.AddBy = UserID;
                                    stLog.AddOn = DateTime.UtcNow.AddHours(5);
                                    stLog.BranchID = item.BranchID;
                                    stLog.CostPrice = batch.CostPrice;
                                    stLog.InReference = item.InReference;
                                    stLog.InvoiceDate = DateTime.UtcNow.AddHours(5);
                                    stLog.IsActive = true;
                                    stLog.IsSOReturned = false;
                                    stLog.OrderTypeID = item.OrderTypeID;
                                    stLog.ProductID = item.ProductID;
                                    stLog.ReturnedQty = 0;
                                    stLog.SalePrice = item.SalePrice;
                                    stLog.StockBatchID = batch.StockBatchID;
                                    stLog.StockIN = item.StockIN;
                                    stLog.StockOut = 0;
                                    db.tbl_StockLog.Add(stLog);
                                    db.SaveChanges();
                                    #endregion
                                    row.Qty += Convert.ToInt32(item.StockIN);
                                    #region Update CostPrice in Stock 
                                    decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == item.BranchID).Sum(p => (decimal?)p.CostPrice * p.Qty) ?? 0m);
                                    int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == item.BranchID).Sum(p => (decimal?)p.Qty) ?? 0m);

                                    decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;

                                    #endregion
                                }
                                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                db.Entry(row).State = EntityState.Modified;
                            }
                        }
                        #endregion                        
                        db.SaveChanges();
                    }
                    transaction.Commit();
                    result = "success";
                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
       private tbl_StockLog MaptoModel(Models.DTO.StockMoveOut stockOut)
        {
            tbl_StockLog stock = new tbl_StockLog()
            {
                ProductID = stockOut.ProductID,
                SalePrice = stockOut.SalePrice,
                CostPrice = stockOut.CostPrice,
                BranchID = stockOut.BranchID,
                Detail = stockOut.Detail,
                AddBy = 1,
                AddOn = DateTime.UtcNow.AddHours(5),
                InvoiceDate = DateTime.UtcNow.AddHours(5),
                OrderTypeID = stockOut.OrderTypeID,
                StockOut = stockOut.StockOut,
                OutReference = stockOut.OutReference,
                StockIN = stockOut.StockIN,
                InReference = stockOut.InReference,
                UserId = stockOut.UserId
            };

            return stock;
        }
    }
}