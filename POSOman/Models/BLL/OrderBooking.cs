﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace POSOman.Models.BLL
{
    public class OrderBooking
    {
        private POSOman.Controllers.PurchaseController poc = new Controllers.PurchaseController(); 
        public tbl_PurchaseOrder newOrder { get; set; }
        public tbl_Stock Stock { get; set; }
        public tbl_StockLog StockLog { get; set; }
        private void insertAccounts(int? iPaymentAccountID,decimal dAmountPaid,int iOrderID,string sPaymentStatus,dbPOS db)
        {
            try
            {
                 
                
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        //save purchase
        public object Save(tbl_PurchaseOrder model, List<tbl_StockLog> modelStockLog, int? bankAccId, int branchId)
        {                        
            dbPOS db = new dbPOS();                        
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            branchId = model.BranchID ?? 9001;
            model.BranchID = branchId;
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    model.ChequeDate = model.PurchaseDate;
                    db.Configuration.ValidateOnSaveEnabled = false;
                    if (model.PurchaseRequestID != null)
                    {
                        var pr = db.tbl_PurchaseOrder.Where(x => x.OrderID == model.PurchaseRequestID).FirstOrDefault();
                        if (pr != null)
                        {
                            pr.Can_Modify = false;
                            db.Entry(pr).State = EntityState.Modified;
                           // db.SaveChanges();
                        }
                    }
                   

                    if (model.VAT == null )
                    {
                        model.VAT = 0;
                    }

                    if (model.DiscountAmount == null)
                    {
                        model.DiscountAmount = 0;
                    }

                    if (model.PaymentStatus == "UnPaid")
                    {                              
                        model.AmountPaid = 0;                     
                        model.IsPaid = false;
                        model.PaymentTypeID = null;
                    }
                    else if (model.PaymentStatus == "Partial Paid")
                    {                        
                        model.IsPaid = false;                        
                    }
                    else if (model.PaymentStatus == "Paid")
                    {                        
                        model.IsPaid = true;
                        model.AmountPaid = model.AmountPaid;
                    }
                    
                    model.TotalPaid = model.AmountPaid;
                    string sPaymentStatus = model.PaymentStatus;
                    int? iPaymentAccountID = null;
                    model.AddBy = 1;
                    model.AddOn = DateTime.UtcNow.AddHours(5);        
                    model.IsReturned = false;
                    if (model.PaymentTypeID == 2) { model.ChequeDate = null; }
                    if (model.PaymentTypeID == 1)
                    {
                        model.ChequeDate = null;
                        model.BankName = "";
                        iPaymentAccountID = (int)(model.PaymentTypeID);//  set it from Payment Type ddl 
                    }
                    else if (model.PaymentTypeID > 1)
                    {
                        iPaymentAccountID = bankAccId;
                    }
                    foreach (var item in model.tbl_PODetails)
                    {
                        item.BranchID = branchId;
                    }
                    int newPOID = 0;
                    var tmp = db.tbl_PurchaseOrder.Where(po => po.isOpening != true && po.IsDeleted !=true).OrderByDescending(v => v.POID).FirstOrDefault();
                    if (tmp != null)
                    {
                        newPOID = tmp.POID + 1;
                    }
                    else
                    {
                        newPOID = 10001;
                    }
                    model.POID = newPOID;
                    model.Barcode = "PO-" + branchId + "-"+newPOID;
                    db.tbl_PurchaseOrder.Add(model);  
                                      
                    int poId = db.SaveChanges();
                    int iOrderID = model.OrderID;                    
                    decimal dAmountPaid = 0;
                    if (model.AmountPaid != null || model.AmountPaid > 0)
                        dAmountPaid = (decimal)(model.AmountPaid);
                    if (poId >= 1 && modelStockLog != null)
                    {
                        // get all product ids 
                        List<int> lstProdIds = model.tbl_PODetails.Select(p => p.ProductID).ToList();
                        // get all stock where productid contains lstProdIds
                        var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == model.BranchID).ToList();
                        List<tbl_Stock> existingStock = lstStock.ToList();
                        // 
                        foreach (var item in modelStockLog)
                        {
                            var VehID = db.tbl_Product.Where(x => x.ProductID == item.ProductID).Select(x => x.VehicleCodeID).FirstOrDefault();
                            
                            #region Stock Batch
                            tbl_StockBatch batch = new tbl_StockBatch();
                            batch.BatchID = iOrderID + "-" + item.ProductID;
                            batch.ProductID = item.ProductID;
                            batch.Qty = item.StockIN ?? 0;
                            batch.CostPrice = item.CostPrice ?? 0;
                            batch.ExpiryDate = model.tbl_PODetails.Where(p => p.ProductID == item.ProductID).FirstOrDefault().ExpiryDate;
                            batch.BranchID = branchId;
                            db.tbl_StockBatch.Add(batch);
                            db.SaveChanges();
                            #endregion
                            item.StockBatchID = batch.StockBatchID;
                           
                            item.InvoiceDate = model.PurchaseDate;
                            item.OrderID = iOrderID;
                            item.AddOn = Helper.PST();
                            item.AddBy = 1;
                            item.BranchID = branchId;
                            lstProdIds.Add(item.ProductID);

                            if(VehID  != 1)
                            {
                                //  if already exists stock
                                var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
                                if (row != null)
                                {
                                    //decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.CostPrice * p.Qty) ?? 0m);
                                    //int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.Qty) ?? 0);

                                    //decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;
                                    // if (avgCost == 0)

                                    #region stock batch
                                    //decimal? cost = row.Qty <= 0 ?item.CostPrice: row.CostPrice + item.CostPrice;
                                    //row.CostPrice = row.Qty <=0 ?item.CostPrice: cost/2;
                                    #endregion
                                    decimal? cost =   row.CostPrice + item.CostPrice;
                                    row.CostPrice = row.CostPrice <=0 ? cost :  cost / 2;
                                    //else
                                    //{ row.CostPrice = avgCost; }

                                    row.SalePrice = item.SalePrice;
                                    row.Qty += Convert.ToDecimal(item.StockIN);
                                    row.UpdateOn = Helper.PST();
                                    row.Location = item.Location;
                                    row.BranchID = item.BranchID;
                                    db.Entry(row).State = EntityState.Modified;
                                }
                                else
                                {
                                    // if new item is added 
                                    tbl_Stock newStock = new tbl_Stock();
                                    newStock.ProductID = item.ProductID;
                                    newStock.Qty = Convert.ToDecimal(item.StockIN);
                                    newStock.CostPrice = item.CostPrice;
                                    newStock.SalePrice = item.SalePrice;
                                    newStock.Addon = Helper.PST();
                                    newStock.Location = item.Location;
                                    newStock.BranchID = item.BranchID;
                                    newStock.OnMove = 0;
                                    db.tbl_Stock.Add(newStock);
                                }
                            }
                           
                        }
                        //db.insertPOGJEntry(iPaymentAccountID, dAmountPaid, iOrderID);
                        if (sPaymentStatus == "Paid")
                        {
                            //if (model.PurchaseDate < model.ChequeDate && iPaymentAccountID > 2)
                            //{                   //pdc Entry
                            //    //db.insertPOGJEntryPDCPaid(iPaymentAccountID, iOrderID);
                            //    db.insertPOGJEntryPDCPaid_New(iPaymentAccountID, iOrderID);
                            //}
                            //else
                            //{
                                //db.insertPOGJEntryPaid(iPaymentAccountID, iOrderID);
                                db.insertPOGJEntryPaid_New(iPaymentAccountID, iOrderID);
                            //}                            
                        }
                        else if (sPaymentStatus == "Partial Paid")
                        {
                            //if (model.PurchaseDate < model.ChequeDate && iPaymentAccountID > 2)
                            //{//pdc Entry
                            //    db.insertPOGJEntryPDCPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID);
                            //}
                            //else
                            //{
                                db.insertPOGJEntryPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID);
                            //}                            
                        }
                        else if (sPaymentStatus == "UnPaid")
                        {
                            //db.insertPOGJEntryUnpaid(iOrderID);
                            db.insertPOGJEntryUnpaid_New(iOrderID);
                        }
                        modelStockLog.ToList<tbl_StockLog>().ForEach(s => db.Entry(s).State = EntityState.Added);
                        db.SaveChanges();                                          
                    }       
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    string message = "";
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    db.InsertError(1, 1, 1, 1, "dbError", message, model.UserID, "Clover", DateTime.UtcNow.AddHours(5));
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    db.InsertError(1, 1, 1, 1, "catch:"+ex.Message, ex.InnerException.ToString(), model.UserID, "Clover", DateTime.UtcNow.AddHours(5));
                    return ex.Message;
                }
            }
        }

        public object SaveRequest(tbl_PurchaseOrder model, List<tbl_StockLog> modelStockLog, int? bankAccId, int branchId)
        {
            dbPOS db = new dbPOS();
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            branchId = model.BranchID ?? 9001;
            model.BranchID = branchId;
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;

                    if (model.VAT == null)
                    {
                        model.VAT = 0;
                    }

                    if (model.DiscountAmount == null)
                    {
                        model.DiscountAmount = 0;
                    }

                    if (model.PaymentStatus == "UnPaid")
                    {
                        model.AmountPaid = 0;
                        model.IsPaid = false;
                        model.PaymentTypeID = null;
                    }
                    

                    model.TotalPaid = model.AmountPaid;
                    string sPaymentStatus = model.PaymentStatus;
                    int? iPaymentAccountID = null;
                    model.AddBy = 1;
                    model.AddOn = DateTime.UtcNow.AddHours(5);
                    model.IsReturned = false;
                    if (model.PaymentTypeID == 2) { model.ChequeDate = null; }
                    if (model.PaymentTypeID == 1)
                    {
                        model.ChequeDate = null;
                        model.BankName = "";
                        iPaymentAccountID = (int)(model.PaymentTypeID);//  set it from Payment Type ddl 
                    }
                    else if (model.PaymentTypeID > 1)
                    {
                        iPaymentAccountID = bankAccId;
                    }
                    foreach (var item in model.tbl_PODetails)
                    {
                        item.BranchID = branchId;
                    }
                    int newPOID = 0;
                    var tmp = db.tbl_PurchaseOrder.Where(po => po.isOpening != true && po.IsDeleted != true).OrderByDescending(v => v.POID).FirstOrDefault();
                    if (tmp != null)
                    {
                        newPOID = tmp.POID + 1;
                    }
                    else
                    {
                        newPOID = 10001;
                    }
                    model.POID = newPOID;
                    model.PurchaseTypeID = 1;
                    model.Barcode = "PR-" + branchId + "-" + newPOID;
                    db.tbl_PurchaseOrder.Add(model);

                    int poId = db.SaveChanges();
                    int iOrderID = model.OrderID;
                    decimal dAmountPaid = 0;
                    if (model.AmountPaid != null || model.AmountPaid > 0)
                        dAmountPaid = (decimal)(model.AmountPaid);
                    if (poId >= 1 )
                    {
                        db.SaveChanges();
                        transaction.Commit();
                        return "success";
                    }
                    else
                    {
                        transaction.Rollback();
                        return "fail";
                    }
                    
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    string message = "";
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);

                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    db.InsertError(1, 1, 1, 1, "dbError", message, model.UserID, "Clover", DateTime.UtcNow.AddHours(5));
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    db.InsertError(1, 1, 1, 1, "catch:" + ex.Message, ex.InnerException.ToString(), model.UserID, "Clover", DateTime.UtcNow.AddHours(5));
                    return ex.Message;
                }
            }
        }

        // Save Edit Purchase Order
        public object EditPurchase(int OrderID, tbl_PurchaseOrder model, List<tbl_StockLog> modelStockLog, int? bankAccId, int branchId,int UserID)
        {
            branchId = model.BranchID ?? 9001;
            dbPOS db = new dbPOS();
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            model.BranchID = branchId;
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {

                    if (model.VAT == null)
                    {
                        model.VAT = 0;
                    }

                    if (model.DiscountAmount == null)
                    {
                        model.DiscountAmount = 0;
                    }
                    var PurchaseOrder = db.tbl_PurchaseOrder.Where(x => x.OrderID == OrderID).FirstOrDefault();
                    PurchaseOrder.InvoiceNo = model.InvoiceNo;
                    PurchaseOrder.PaymentStatus = model.PaymentStatus;
                    PurchaseOrder.PaymentTypeID = model.PaymentTypeID;
                    PurchaseOrder.VAT = model.VAT;
                    PurchaseOrder.DiscountAmount = model.DiscountAmount;
                    PurchaseOrder.AmountPaid = model.AmountPaid;
                    PurchaseOrder.TotalPaid = model.AmountPaid;
                    PurchaseOrder.TotalAmount = model.TotalAmount;
                    PurchaseOrder.UserID = model.UserID;
                    PurchaseOrder.AccountID = model.AccountID;
                    PurchaseOrder.Expenses = model.Expenses;
                    PurchaseOrder.Description = model.Description;
                    //int i = Convert.ToInt32(model.IsTransit);
                    db.Configuration.ValidateOnSaveEnabled = false;
                    if (model.PaymentStatus == "UnPaid")
                    {
                        PurchaseOrder.AmountPaid = 0;
                        PurchaseOrder.TotalPaid = 0;
                        PurchaseOrder.PaymentTypeID = null;
                        PurchaseOrder.IsPaid = false;
                    }
                    else if (model.PaymentStatus == "Partial Paid")
                    {
                        PurchaseOrder.IsPaid = false; 
                    }
                    else if (model.PaymentStatus == "Paid")
                    {
                        PurchaseOrder.IsPaid = true;
                        PurchaseOrder.AmountPaid = model.TotalAmount;// + model.VAT - model.DiscountAmount;
                    }

                    string sPaymentStatus = model.PaymentStatus;
                    int? iPaymentAccountID = null;
                    // assign login user id 
                    PurchaseOrder.UpdateBy = UserID;
                    PurchaseOrder.UpdateOn = DateTime.UtcNow.AddHours(5);
                    PurchaseOrder.IsReturned = false;

                    if (model.PaymentTypeID == 2)
                    {
                        PurchaseOrder.ChequeDate = null;
                        PurchaseOrder.BankName = model.BankName;
                        PurchaseOrder.ChequeNo = null;
                        iPaymentAccountID = bankAccId;
                    }
                    else if (model.PaymentTypeID == 1)
                    {
                        PurchaseOrder.ChequeDate = null;
                        PurchaseOrder.BankName = "";
                        PurchaseOrder.ChequeNo = null;
                        iPaymentAccountID = (int)(model.PaymentTypeID);//  set it from Payment Type ddl 
                    }
                    else if (model.PaymentTypeID == 3)
                    {
                        PurchaseOrder.BankName = model.BankName;
                        PurchaseOrder.ChequeNo = model.ChequeNo;
                        PurchaseOrder.ChequeDate = model.ChequeDate;
                        iPaymentAccountID = bankAccId;
                    }
                    foreach (var item in model.tbl_PODetails)
                    {
                        item.BranchID = branchId;
                    }
                    
                    db.Entry(PurchaseOrder).State = EntityState.Modified;
                    //db.SaveChanges();

                    var PDetails = db.tbl_PODetails.Where(x => x.OrderID == PurchaseOrder.OrderID && x.BranchID == PurchaseOrder.BranchID).ToList();
                    var ModelDetails = model.tbl_PODetails.ToList();

                    foreach (var i in ModelDetails)
                    {
                        i.OrderID = OrderID;
                    }

                    foreach (var i in PDetails)
                    {
                        string BatchID = PurchaseOrder.OrderID + "-" + i.ProductID;
                        //var PD = db.tbl_PODetails.Where(x => x.OrderID == PurchaseOrder.OrderID && x.ProductID == i.ProductID).FirstOrDefault();
                        //var MDetail = model.tbl_PODetails.Where(x => x.OrderID == OrderID && x.ProductID == i.ProductID).FirstOrDefault();

                        // int Qty = MDetail.Qty - PD.Qty  ;
                        decimal PDQty = i.Qty;

                        var stockBatch = db.tbl_StockBatch.Where(x => x.BatchID == BatchID && x.ProductID == i.ProductID).FirstOrDefault();
                        if (stockBatch != null)
                        {

                            db.Entry(stockBatch).State = EntityState.Deleted;
                            db.SaveChanges();
                        }

                        decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == i.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.CostPrice * p.Qty) ?? 0m);
                        int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == i.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.Qty) ?? 0m);

                        decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;

                        var Stock = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && x.BranchID == i.BranchID).FirstOrDefault();
                        if (Stock != null)
                        {
                            if (avgCost == 0)
                            { Stock.CostPrice = i.UnitPrice; }
                            else
                            { Stock.CostPrice = avgCost; }

                            Stock.Qty -= i.Qty;
                            db.Entry(Stock).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        var StockLog = db.tbl_StockLog.Where(x => x.OrderID == OrderID && x.ProductID == i.ProductID && x.OrderTypeID == 1 && x.IsActive != false).FirstOrDefault();

                        if (StockLog != null)
                        {

                            StockLog.IsActive = false;
                            db.Entry(StockLog).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        i.IsDeleted = true;
                        db.Entry(i).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    foreach (var MDetail in ModelDetails)
                    {
                        var PD = db.tbl_PODetails.Where(x => x.OrderID == OrderID && x.ProductID == MDetail.ProductID).FirstOrDefault();
                        string BatchID = PurchaseOrder.OrderID + "-" + PD.ProductID;
                        if (MDetail != null)
                        {
                            var stockBatch = db.tbl_StockBatch.Where(x => x.BatchID == BatchID && x.ProductID == MDetail.ProductID).FirstOrDefault();
                            if (stockBatch != null)
                            {
                                stockBatch.BranchID = branchId;
                                stockBatch.CostPrice = MDetail.UnitPrice ?? 0;
                                stockBatch.ProductID = MDetail.ProductID;
                                stockBatch.Qty = MDetail.Qty;
                                db.Entry(stockBatch).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                BatchID = "OldStock-1001";
                                stockBatch = db.tbl_StockBatch.Where(x => x.BatchID == BatchID && x.ProductID == MDetail.ProductID).FirstOrDefault();
                                if (stockBatch != null)
                                {
                                    stockBatch.Qty -= PD.Qty;
                                    stockBatch.Qty += MDetail.Qty;
                                    db.Entry(stockBatch).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    #region Stock Batch
                                    tbl_StockBatch batch = new tbl_StockBatch();
                                    batch.BatchID = BatchID;
                                    batch.ProductID = MDetail.ProductID;
                                    batch.Qty = MDetail.Qty;
                                    batch.CostPrice = MDetail.UnitPrice ?? 0;
                                    batch.ExpiryDate = model.tbl_PODetails.Where(p => p.ProductID == MDetail.ProductID).FirstOrDefault().ExpiryDate;
                                    batch.BranchID = branchId;
                                    db.tbl_StockBatch.Add(batch);
                                    db.SaveChanges();
                                    #endregion
                                }

                            }

                            if (PD != null)
                            {
                                PD.DiscountAmount = MDetail.DiscountAmount;
                                PD.DiscountPercent = MDetail.DiscountPercent;
                                PD.GSTAmount = MDetail.GSTAmount;
                                PD.GSTPercent = MDetail.GSTPercent;
                                PD.IsDeleted = MDetail.IsDeleted;
                                PD.IsReturned = MDetail.IsReturned;
                                PD.LevelID = MDetail.LevelID;
                                PD.IsMinor = MDetail.IsMinor;
                                PD.IsPack = MDetail.IsPack;
                                PD.Qty = MDetail.Qty;
                                PD.SalePrice = MDetail.SalePrice;
                                PD.Total = MDetail.Total;
                                PD.UnitPrice = MDetail.UnitPrice;
                                PD.UpdateOn = DateTime.UtcNow.AddHours(5);
                                PD.BranchID = MDetail.BranchID;
                                PD.UnitPrice = MDetail.UnitPrice;
                                db.Entry(PD).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                tbl_PODetails sd = new tbl_PODetails();

                                MDetail.UpdateOn = DateTime.UtcNow.AddHours(5);

                                sd = MDetail;

                                db.tbl_PODetails.Add(sd);
                                db.SaveChanges();

                            }

                            //decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == MDetail.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.CostPrice * p.Qty) ?? 0m);
                            //int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == MDetail.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.Qty) ?? 0m);

                            //decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;

                            //var Stock = db.tbl_Stock.Where(x => x.ProductID == MDetail.ProductID && x.BranchID == MDetail.BranchID).FirstOrDefault();
                            //if (Stock != null)
                            //{
                            //    if (avgCost == 0)
                            //    { Stock.CostPrice = MDetail.UnitPrice; }
                            //    else
                            //    { Stock.CostPrice = avgCost; }
                            //    Stock.Qty = Stock.Qty == 0 ? PD.Qty : Stock.Qty - PD.Qty;
                            //    Stock.Qty += MDetail.Qty;
                            //    db.Entry(Stock).State = EntityState.Modified;
                            //    db.SaveChanges();
                            //}

                            ////////////
                            
                            var Stock = db.tbl_Stock.Where(x => x.ProductID == MDetail.ProductID && x.BranchID == MDetail.BranchID).FirstOrDefault();
                            if (Stock != null)
                            {
                                //if (avgCost == 0)
                                //{ Stock.CostPrice = MDetail.UnitPrice; }
                                //else
                                Stock.CostPrice = (Convert.ToDecimal(Stock.CostPrice) + Convert.ToDecimal(MDetail.UnitPrice)) /2; 
                                //Stock.Qty = Stock.Qty == 0 ? PD.Qty : Stock.Qty - PD.Qty;
                                Stock.Qty += MDetail.Qty;
                                db.Entry(Stock).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                tbl_Stock newStock = new tbl_Stock();
                                newStock.ProductID = MDetail.ProductID;
                                newStock.Qty = Convert.ToDecimal(MDetail.Qty);
                                newStock.CostPrice = MDetail.UnitPrice ?? 0;
                                newStock.SalePrice = MDetail.SalePrice;
                                newStock.Addon = DateTime.UtcNow.AddHours(5);
                                newStock.Location = "";
                                newStock.BranchID = MDetail.BranchID;
                                newStock.OnMove = 0;
                                db.tbl_Stock.Add(newStock);
                                db.SaveChanges();
                            }
                            var StockLog = db.tbl_StockLog.Where(x => x.OrderID == OrderID && x.ProductID == MDetail.ProductID && x.OrderTypeID == 1 && x.IsActive != false).FirstOrDefault();

                            if (StockLog != null)
                            {
                                StockLog.StockIN = MDetail.Qty;
                                StockLog.CostPrice = MDetail.UnitPrice;
                                StockLog.SalePrice = MDetail.SalePrice;
                                StockLog.AccountID = model.AccountID;
                                db.Entry(StockLog).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                if (modelStockLog.Count > 0 && modelStockLog != null)
                                {
                                    List<tbl_StockLog> stLog = new List<tbl_StockLog>();
                                    db.tbl_StockLog.AddRange(modelStockLog);
                                    db.SaveChanges();
                                }

                            }


                        }
                        else
                        {
                            var stockBatch = db.tbl_StockBatch.Where(x => x.BatchID == BatchID && x.ProductID == MDetail.ProductID).FirstOrDefault();
                            if (stockBatch != null)
                            {
                                stockBatch.BranchID = branchId;
                                stockBatch.CostPrice = PD.UnitPrice ?? 0;
                                stockBatch.Qty -= PD.Qty;
                                db.Entry(stockBatch).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            PD.DiscountAmount = 0;
                            PD.DiscountPercent = 0;
                            PD.GSTPercent = 0;
                            PD.GSTAmount = 0;
                            PD.IsDeleted = true;
                            PD.IsReturned = true;
                            PD.Qty = 0;
                            PD.Total = 0;
                            PD.UpdateOn = DateTime.UtcNow.AddHours(5);
                            PD.DeletedBy = UserID;
                            db.Entry(PD).State = EntityState.Modified;
                            db.SaveChanges();
                            var Stock = db.tbl_Stock.Where(x => x.ProductID == MDetail.ProductID && x.BranchID == MDetail.BranchID).FirstOrDefault();
                            var StockLog = db.tbl_StockLog.Where(x => x.OrderID == OrderID && x.ProductID == MDetail.ProductID && x.OrderTypeID == 1).FirstOrDefault();
                            StockLog.StockIN = 0;

                            StockLog.CostPrice = 0;
                            StockLog.SalePrice = 0;
                            StockLog.AccountID = model.AccountID;
                            Stock.Qty = Stock.Qty == 0 ? PD.Qty : Stock.Qty - PD.Qty;

                            db.Entry(StockLog).State = EntityState.Modified;
                            db.Entry(Stock).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                    }


                    var Jentry = db.tbl_JEntry.Where(x => x.RefID == OrderID && x.VoucherName == "Purchase").ToList();
                    foreach (var j in Jentry)
                    {
                        var Jdetail = db.tbl_JDetail.Where(x => x.JEntryID == j.JEntryId).ToList();
                        foreach (var io in Jdetail)
                        {
                            db.Entry(io).State = EntityState.Deleted;
                            db.SaveChanges();
                        }
                        var JentryLog = db.tbl_JEntryLog.Where(x => x.JEntryID == j.JEntryId).FirstOrDefault();
                        if (JentryLog != null)
                        {
                            db.Entry(JentryLog).State = EntityState.Deleted;
                            db.SaveChanges();
                        }
                    }
                    foreach (var k in Jentry)
                    {
                        var Chq = db.tbl_Cheques.Where(x => x.JEntryId == k.JEntryId).FirstOrDefault();
                        if (Chq != null)
                        {
                            db.Entry(Chq).State = EntityState.Deleted;
                            db.SaveChanges();
                        }
                        db.Entry(k).State = EntityState.Deleted;
                        db.SaveChanges();
                    }

                    // int poId = db.SaveChanges();
                    int iOrderID = OrderID;
                    decimal dAmountPaid = 0;
                    if (model.AmountPaid != null || model.AmountPaid > 0)
                        dAmountPaid = (decimal)(model.AmountPaid);
                   

                    if (sPaymentStatus == "Paid")
                    {
                        db.insertPOGJEntryPaid(iPaymentAccountID, iOrderID);
                    }
                    else if (sPaymentStatus == "Partial Paid")
                    {
                        db.insertPOGJEntryPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID);
                    }
                    else if (sPaymentStatus == "UnPaid")
                    {
                        db.insertPOGJEntryUnpaid(iOrderID);
                    }
                    // modelStockLog.ToList<tbl_StockLog>().ForEach(s => db.Entry(s).State = EntityState.Added);
                    db.SaveChanges();
                    //}
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

       
    }
}
