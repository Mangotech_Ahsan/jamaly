﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AutoMapper;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity;

namespace POSOman.Models.BLL
{
    public class MoveStock
    {
        public object SaveStockMovement(List<Models.DTO.StockMoving> modelStockMoving)
        {
            dbPOS db = new dbPOS();
            var manager = ((IObjectContextAdapter)db).ObjectContext.ObjectStateManager;
            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (modelStockMoving != null)
                    {
                        tbl_StockMoving stockLog;
                        foreach (var item in modelStockMoving)
                        {
                            item.UserID = HttpContext.Current.User.Identity.GetUserId();               
                            stockLog = MaptoModel(item);
                            db.tbl_StockMoving.Add(stockLog);
                        }                        
                        // Less Stock Qty From Branch Moving fro
                        foreach (var item in modelStockMoving)
                        {
                            //  if already exists stock
                            var row = db.tbl_Stock.FirstOrDefault(s => s.ProductID == item.ProductID && s.BranchID == item.FromBranchID);
                            if (row != null)
                            {
                                if (row.OnMove == null)
                                { row.OnMove = item.StockMoved; }
                                else
                                { row.OnMove += Convert.ToDecimal(item.StockMoved); }
                                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                db.Entry(row).State = EntityState.Modified;
                            }
                        }                                                                  
                        db.SaveChanges();
                    }                    
                    transaction.Commit();
                    result = "success";
                    return result;   
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        public object SaveStockReceiving(List<Models.DTO.StockMoveIn> modelStockLogIn, List<Models.DTO.StockMoving> modelStockMoving)
        {
            dbPOS db = new dbPOS();
            var manager = ((IObjectContextAdapter)db).ObjectContext.ObjectStateManager;
            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (modelStockLogIn != null)
                    {
                        foreach (var item in modelStockLogIn)
                        {
                            if (item.StockOut > 0 && item.StockIN > 0)
                            {
                                var rStockOut = item.StockOut ?? 0;
                                var rowCount = 0;
                                decimal stLogQty = 0;
                                #region FIFO 
                                var stockBatch = db.GetProductFIFO(item.ProductID, item.StockOut, item.FromBranchID).ToList();
                                while (rStockOut > 0)
                                {
                                    tbl_StockBatch newStockBatch = new tbl_StockBatch();
                                    var StockBatch = stockBatch[rowCount];
                                    var sbRow = db.tbl_StockBatch.Where(b => b.StockBatchID == StockBatch.StockBatchID).FirstOrDefault();
                                    var Order = db.tbl_StockLog.Where(sl => sl.OrderTypeID == 1 && sl.StockBatchID == sbRow.StockBatchID).FirstOrDefault();
                                    
                                    if (rStockOut > StockBatch.Qty)
                                    {
                                        stLogQty = sbRow.Qty;
                                        rStockOut -= sbRow.Qty;
                                        sbRow.Qty = 0;
                                        db.Entry(sbRow).State = EntityState.Modified;                                      
                                    }
                                    else if (rStockOut < StockBatch.Qty)
                                    {
                                        stLogQty = rStockOut;
                                        sbRow.Qty -= rStockOut;
                                        db.Entry(sbRow).State = EntityState.Modified;
                                        rStockOut = 0;
                                    }
                                    else if (rStockOut == StockBatch.Qty)
                                    {
                                        stLogQty = rStockOut;
                                        sbRow.Qty = 0;
                                        db.Entry(sbRow).State = EntityState.Modified;
                                        rStockOut = 0;
                                    }                                    
                                    #region New Batch   // check logic either it will new batch or update old one
                                    newStockBatch.BatchID = sbRow.BatchID;
                                    newStockBatch.BranchID = item.ToBranchID ?? 0;
                                    newStockBatch.ProductID = item.ProductID;
                                    
                                    newStockBatch.CostPrice = sbRow.CostPrice;                                    
                                    newStockBatch.Qty = stLogQty;
                                    db.tbl_StockBatch.Add(newStockBatch);
                                    db.SaveChanges();
                                    #endregion
                                    if(Order != null)
                                    {
                                        var PurchaseOrder = db.tbl_PurchaseOrder.Where(po => po.OrderID == Order.OrderID).FirstOrDefault();
                                        PurchaseOrder.Can_Modify = false;
                                        db.Entry(PurchaseOrder).State = EntityState.Modified;
                                    }
                                    
                                    #region StockLog for Stock Out
                                    tbl_StockLog stLogOut = new tbl_StockLog();
                                    stLogOut.StockBatchID = sbRow.StockBatchID;
                                    stLogOut.AddBy = item.AddBy;
                                    stLogOut.AddOn = DateTime.UtcNow.AddHours(5);
                                    stLogOut.BranchID = item.FromBranchID;
                                    stLogOut.CostPrice = sbRow.CostPrice;
                                    stLogOut.InvoiceDate = DateTime.UtcNow.AddHours(5);
                                    stLogOut.IsActive = true;
                                    stLogOut.IsMinor = item.IsMinor;
                                    stLogOut.IsOpen = item.IsOpen;
                                    stLogOut.IsPack = item.IsPack;
                                    stLogOut.LevelID = item.LevelID;
                                    stLogOut.MinorDivisor = item.MinorDivisor;
                                    stLogOut.OrderTypeID = 6;
                                    stLogOut.OrderID = item.OrderID;
                                    stLogOut.OutReference = "Stock Moving";
                                    stLogOut.ProductID = item.ProductID;
                                    stLogOut.SalePrice = item.SalePrice;
                                    stLogOut.StockIN = 0;
                                    stLogOut.ReturnedQty = 0;
                                    stLogOut.StockOut = stLogQty;
                                    stLogOut.UnitCode = item.UnitCode;
                                    stLogOut.UnitID = item.UnitID;
                                    stLogOut.UnitPerCarton = item.UnitPerCarton;
                                    stLogOut.UserReferenceID = item.UserReferenceID;
                                    stLogOut.UserId = HttpContext.Current.User.Identity.GetUserId();
                                    db.tbl_StockLog.Add(stLogOut);
                                    #endregion
                                    #region StockLog for Stock IN
                                    tbl_StockLog stLogIn = new tbl_StockLog();
                                    stLogIn.StockBatchID = sbRow.StockBatchID;
                                    stLogIn.AddBy = item.AddBy;
                                    stLogIn.AddOn = DateTime.UtcNow.AddHours(5);
                                    stLogIn.BranchID = item.ToBranchID;
                                    stLogIn.CostPrice = sbRow.CostPrice;
                                    stLogIn.InvoiceDate = DateTime.UtcNow.AddHours(5);
                                    stLogIn.IsActive = true;
                                    stLogIn.IsMinor = item.IsMinor;
                                    stLogIn.IsOpen = item.IsOpen;
                                    stLogIn.IsPack = item.IsPack;
                                    stLogIn.LevelID = item.LevelID;
                                    stLogIn.MinorDivisor = item.MinorDivisor;
                                    stLogIn.OrderTypeID = 5;
                                    stLogIn.OrderID = item.OrderID;
                                    stLogIn.OutReference = "Stock Moving";
                                    stLogIn.ProductID = item.ProductID;
                                    stLogIn.SalePrice = item.SalePrice;
                                    stLogIn.StockIN = stLogQty;
                                    stLogIn.ReturnedQty = 0;
                                    stLogIn.StockOut = 0;
                                    stLogIn.UnitCode = item.UnitCode;
                                    stLogIn.UnitID = item.UnitID;
                                    stLogIn.UnitPerCarton = item.UnitPerCarton;
                                    stLogIn.UserReferenceID = item.UserReferenceID;
                                    stLogIn.UserId = HttpContext.Current.User.Identity.GetUserId();
                                    db.tbl_StockLog.Add(stLogIn);
                                    #endregion

                                    rowCount += 1;
                                }
                            }
                            #endregion
                        }
                        #region Less Qty from  Moved Branch
                        // Less Stock Qty From Branch Moving fro
                        foreach (var item in modelStockMoving)
                        {
                            #region Calculate CostPrice from Stock Batch 
                            decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == item.FromBranchID).Sum(p => (decimal?) p.CostPrice * p.Qty) ?? 0m);
                            int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == item.FromBranchID).Sum(p => (decimal?)p.Qty) ?? 0m);
                            decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;
                            //decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;

                            #endregion
                            //  if already exists stock
                            var row = db.tbl_Stock.FirstOrDefault(s => s.ProductID == item.ProductID && s.BranchID == item.FromBranchID);
                            if (row != null)
                            {                                
                                row.Qty -= Convert.ToDecimal(item.StockReceived);
                                if(item.StockReceived > 0)
                                {
                                    row.OnMove -= Convert.ToDecimal(item.StockReceived);
                                }
                                else
                                {
                                    row.OnMove -= Convert.ToDecimal(item.StockMoved);
                                }                                
                                row.CostPrice = (avgCost > 0) ? avgCost : item.CostPrice;
                                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                db.Entry(row).State = EntityState.Modified;
                            }
                        }
                        #endregion  

                        // Increment moving qty or Add New Qty if not exists in branch Moving To
                        // Update Stock Cost Price
                        foreach (var item in modelStockLogIn)
                        {
                            if (item.StockIN > 0)
                            {
                                #region Calculate CostPrice from Stock Batch 
                                decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == item.ToBranchID).Sum( p => (decimal?) p.CostPrice * p.Qty) ?? 0m);
                                int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == item.ToBranchID).Sum(p => (decimal?) p.Qty) ?? 0);

                                decimal avgCost = ((costPOTotal / qtyPO) > 0) ? (costPOTotal / qtyPO) : 0m;

                                #endregion
                                //  if already exists stock
                                var row = db.tbl_Stock.FirstOrDefault(s => s.ProductID == item.ProductID && s.BranchID == item.ToBranchID);
                                if (row != null)
                                {
                                    row.Qty += Convert.ToDecimal(item.StockIN);
                                    row.Location = item.Location;
                                    row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                    row.CostPrice = (avgCost > 0) ? avgCost : item.CostPrice;
                                    row.SalePrice = item.SalePrice;
                                    db.Entry(row).State = EntityState.Modified;
                                }
                                else
                                {
                                    var stockInserting = manager.GetObjectStateEntries(EntityState.Added).Where(e => !e.IsRelationship).Select(e => e.Entity).OfType<tbl_Stock>();
                                    var newRow = stockInserting.FirstOrDefault(s => s.ProductID == item.ProductID && s.BranchID == item.ToBranchID);
                                    if (newRow != null)
                                    {
                                        newRow.Qty += Convert.ToDecimal(item.StockIN);
                                        newRow.CostPrice = (avgCost > 0) ? avgCost : item.CostPrice;
                                    }
                                    else
                                    {
                                        // if new item is added 
                                        tbl_Stock newStock = new tbl_Stock();
                                        newStock.ProductID = item.ProductID;
                                        newStock.Qty = Convert.ToInt32(item.StockIN);
                                        newStock.CostPrice = (avgCost > 0) ? avgCost : item.CostPrice;
                                        newStock.SalePrice = item.SalePrice;
                                        newStock.Addon = DateTime.UtcNow.AddHours(5);
                                        newStock.BranchID = item.ToBranchID;
                                        newStock.Location = item.Location;
                                        db.tbl_Stock.Add(newStock);
                                    }
                                }
                            }
                        }
                        int? recdBy = modelStockMoving.FirstOrDefault().ReceivedBy;
                        foreach (var item in modelStockMoving)
                        {                            
                            var row = db.tbl_StockMoving.FirstOrDefault(s => s.ProductID == item.ProductID && s.ID == item.ID);
                            if (row != null)
                            {
                                row.StockReceived = Convert.ToInt32(item.StockReceived);
                                row.Detail = item.Detail;
                                row.isReceived = true;
                                row.ReceivedBy = recdBy;     
                                row.ReceivedOn = DateTime.UtcNow.AddHours(5); // Through it from front end if user requirement                                
                                db.Entry(row).State = EntityState.Modified;
                            }
                        }
                            db.SaveChanges();
                    }
                    transaction.Commit();
                    result = "success";
                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        // Save NEw Sale Price 
        public object UpdateSalePrice(List<Models.DTO.StockPriceDTO> modelStockPrice,int userID)
        {
            dbPOS db = new dbPOS();
            var manager = ((IObjectContextAdapter)db).ObjectContext.ObjectStateManager;
            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (modelStockPrice != null)
                    {                        
                        // Update NEw Price 
                        foreach (var item in modelStockPrice)
                        {
                            //  if already exists stock
                            var row = db.tbl_Stock.FirstOrDefault(s => s.ProductID == item.ProductID);
                            if (row != null && item.NewSalePrice > 0)
                            {
                                row.SalePrice = item.NewSalePrice;
                                row.UpdateBy = userID;
                                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                db.Entry(row).State = EntityState.Modified;
                            }
                        }
                        db.SaveChanges();
                    }
                    transaction.Commit();
                    result = "success";
                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        private tbl_StockMoving MaptoModel(Models.DTO.StockMoving stockMoving)
        {
            tbl_StockMoving stock = new tbl_StockMoving()
            {
                Location = stockMoving.Location,
                ProductID = stockMoving.ProductID,
                FromBranchID = stockMoving.FromBranchID,
                ToBranchID = stockMoving.ToBranchID,
                StockMoved = stockMoving.StockMoved,
                StockReceived = 0,
                IsPack = stockMoving.IsPack,
                LevelID = stockMoving.LevelID,
                CostPrice = stockMoving.CostPrice,
                SalePrice = stockMoving.SalePrice,
                Date = DateTime.UtcNow.AddHours(5), // Through from front if required
                UserID = stockMoving.UserID,
                AddBy = 1,
                isReceived = false,
                AddOn = DateTime.UtcNow.AddHours(5)                
            };

            return stock;
        }
        private tbl_StockLog MaptoModel(Models.DTO.StockMoveOut stockOut)
        {
            tbl_StockLog stock = new tbl_StockLog()
            {
                ProductID = stockOut.ProductID,
                SalePrice = stockOut.SalePrice,
                CostPrice = stockOut.CostPrice,
                BranchID = stockOut.BranchID,
                AddBy = 1,
                AddOn = DateTime.UtcNow.AddHours(5),
                InvoiceDate = DateTime.UtcNow.AddHours(5),
                OrderTypeID = stockOut.OrderTypeID,
                StockOut = stockOut.StockOut,
                OutReference = stockOut.OutReference,
                UserId = stockOut.UserId
            };

            return stock;
        }
        //private tbl_StockLog MaptoModel(Models.DTO.StockMoveIn stockIn)
        //{
        //    tbl_StockLog stock = new tbl_StockLog()
        //    {
        //        //UserId  = 
        //        ProductID = stockIn.ProductID,
        //        SalePrice = stockIn.SalePrice,
        //        CostPrice = stockIn.CostPrice,
        //        BranchID = stockIn.BranchID,
        //        AddBy = 1,
        //        AddOn = DateTime.UtcNow.AddHours(5),
        //        InvoiceDate = DateTime.UtcNow.AddHours(5),
        //        OrderTypeID = stockIn.OrderTypeID,
        //        StockIN = stockIn.StockIN,
        //        InReference = stockIn.InReference,
        //        UserId = stockIn.UserId
        //    };

        //    return stock;
        //}
    }
}