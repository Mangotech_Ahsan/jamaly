﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace POSOman.Models.BLL
{
    public class PurchaseReturn
    {
        public tbl_PurchaseOrder newOrder { get; set; }
        public tbl_Stock Stock { get; set; }
        public tbl_StockLog StockLog { get; set; }
        public object Save(tbl_PurchaseReturn model, List<tbl_StockLog> modelStockLog, int branchId)
        {
            dbPOS db = new dbPOS();            
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    decimal? GST = 0;
                    decimal? Disc = 0;

                    foreach(var i in model.tbl_POReturnDetails)
                    {
                        Disc += i.DiscountAmount * i.Qty;
                        GST += i.GSTAmount * i.Qty;
                    }

                    model.DiscountAmount = Disc;
                    model.VAT = GST;
                    db.Configuration.ValidateOnSaveEnabled = false;                    
                    // Use  using 
                    model.AddOn = DateTime.UtcNow.AddHours(5);
                   // model.BranchID = branchId;
                    db.tbl_PurchaseReturn.Add(model);
                    int poId = db.SaveChanges();
                    int iReturnID = model.PurchaseReturnID;                                      
                    if (poId >= 1 && modelStockLog != null)
                    {
                        // get all product ids 
                        List<int> lstProdIds = model.tbl_POReturnDetails.Select(p => p.ProductID).ToList();
                        // get all stock where productid contains lstProdIds
                        var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID==model.BranchID).ToList();
                        var lstPODetail = db.tbl_PODetails.Where(s => lstProdIds.Contains(s.ProductID) && (s.OrderID == model.PurchaseID)).ToList();
                        List<tbl_PODetails> returningOrders = lstPODetail.ToList();
                        List<tbl_Stock> existingStock = lstStock.ToList();
                        // 
                        foreach (var item in modelStockLog)
                        {
                            item.InvoiceDate = DateTime.UtcNow.AddHours(5).Date;
                            item.OrderID = iReturnID;
                            item.OrderTypeID = 2;
                            item.AddOn = DateTime.UtcNow.AddHours(5);
                            item.AddBy = model.AddBy;                           
                            lstProdIds.Add(item.ProductID);
                            //  if already exists stock
                            var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
                            var poProduct = returningOrders.FirstOrDefault(s => s.ProductID == item.ProductID);
                            #region
                            // Updating PODetails
                            if (poProduct != null)
                            {
                                poProduct.ReturnedQty += Convert.ToDecimal(item.StockOut);
                                poProduct.UpdateOn = DateTime.UtcNow.AddHours(5);
                                if (poProduct.Qty == poProduct.ReturnedQty)
                                {
                                    poProduct.IsReturned = true;
                                }
                                db.Entry(poProduct).State = EntityState.Modified;
                            }
                            #endregion
                            #region StockBAtch
                            var batchID = model.PurchaseID + "-" + item.ProductID;
                            var batchProduct = db.tbl_StockBatch.Where(sb => sb.BatchID == batchID && sb.ProductID == item.ProductID).FirstOrDefault() ;
                            if (batchProduct != null)
                            {
                                batchProduct.Qty -= Convert.ToDecimal(item.StockOut);                                
                                db.Entry(batchProduct).State = EntityState.Modified;
                                item.StockBatchID = batchProduct.StockBatchID;
                            }
                            else
                            {
                                batchID = "OldStock-1001";
                                batchProduct = db.tbl_StockBatch.Where(sb => sb.BatchID == batchID && sb.ProductID == item.ProductID).FirstOrDefault();
                                if (batchProduct != null)
                                {
                                    batchProduct.Qty -= Convert.ToDecimal(item.StockOut);
                                    db.Entry(batchProduct).State = EntityState.Modified;                                    
                                }
                            }
                            #endregion
                            if (row != null)
                            {
                                row.Qty -= Convert.ToInt32(item.StockOut);
                                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                db.Entry(row).State = EntityState.Modified;
                            }                            
                        }
                        var returnOrder = db.tbl_PurchaseOrder.Where(p => p.OrderID== model.PurchaseID).FirstOrDefault();
                        if (returnOrder != null)
                        {
                            if (returnOrder.ReturnAmount == null)
                            { returnOrder.ReturnAmount = model.TotalAmount; }
                            else
                            {returnOrder.ReturnAmount += model.TotalAmount; }
                            returnOrder.UpdateOn = DateTime.UtcNow.AddHours(5);
                            returnOrder.Can_Modify = false;
                            decimal? totalPaid = returnOrder.TotalPaid  + (returnOrder.Expenses ?? 0) + (returnOrder.ReturnAmount ?? 0);
                            if (totalPaid >= returnOrder.TotalAmount && returnOrder.IsPaid != true)
                            {
                                returnOrder.IsPaid = true;
                            }
                            db.Entry(returnOrder).State = EntityState.Modified;
                        }
                        // if all items are full returned then make PurchaseOrder isReturned  = true
                        db.insertPOReturnGJEntry(iReturnID);
                        modelStockLog.ToList<tbl_StockLog>().ForEach(s => db.Entry(s).State = EntityState.Added);        
                        db.SaveChanges();
                    }                    
                    transaction.Commit();
                    db.UpdatePurchaseTable(model.PurchaseID);
                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        //public object Save(tbl_PurchaseOrder model, List<tbl_StockLog> modelStockLog)
        //{
        //    dbPOS db = new dbPOS();

        //    string result = "";
        //    using (
        //    var transaction = db.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            db.Configuration.ValidateOnSaveEnabled = false;
        //            if (model.PaymentStatus == "UnPaid")
        //            {
        //                model.AmountPaid = 0;
        //                model.IsPaid = false;
        //            }
        //            else if (model.PaymentStatus == "Paid")
        //            {
        //                model.IsPaid = true;
        //            }
        //            int? iPaymentAccountID = null;
        //            // Use  using 
        //            model.AddOn = DateTime.UtcNow.AddHours(5);
        //            model.IsReturned = false;
        //            if (model.PaymentTypeID > 0)
        //            {
        //                iPaymentAccountID = (int)(model.PaymentTypeID);//  set it from Payment Type ddl 
        //            }
        //            db.tbl_PurchaseOrder.Add(model);
        //            int poId = db.SaveChanges();
        //            int iOrderID = model.OrderID;
        //            decimal dAmountPaid = 0;
        //            if (model.AmountPaid != null || model.AmountPaid > 0)
        //                dAmountPaid = (decimal)(model.AmountPaid);
        //            if (poId >= 1 && modelStockLog != null)
        //            {
        //                // get all product ids 
        //                List<int> lstProdIds = model.tbl_PODetails.Select(p => p.ProductID).ToList();
        //                // get all stock where productid contains lstProdIds
        //                var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID)).ToList();
        //                List<tbl_Stock> existingStock = lstStock.ToList();
        //                // 
        //                foreach (var item in modelStockLog)
        //                {
        //                    item.InvoiceDate = model.PurchaseDate;
        //                    item.InReferenceID = iOrderID;
        //                    item.AddOn = DateTime.UtcNow.AddHours(5);
        //                    item.AddBy = 1;
        //                    lstProdIds.Add(item.ProductID);
        //                    //  if already exists stock
        //                    var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
        //                    if (row != null)
        //                    {
        //                        row.Qty += Convert.ToInt32(item.StockIN);
        //                        row.UpdateOn = DateTime.UtcNow.AddHours(5);
        //                        db.Entry(row).State = EntityState.Modified;
        //                    }
        //                    else
        //                    {
        //                        // add Location in tbl_StockLog to get here
        //                        // if new item is added 
        //                        tbl_Stock newStock = new tbl_Stock();
        //                        newStock.ProductID = item.ProductID;
        //                        newStock.Qty = Convert.ToInt32(item.StockIN);
        //                        newStock.CostPrice = item.CostPrice;
        //                        newStock.SalePrice = item.SalePrice;
        //                        newStock.Addon = DateTime.UtcNow.AddHours(5);
        //                        newStock.Location = item.Location;
        //                        db.tbl_Stock.Add(newStock);
        //                    }
        //                }
        //                db.insertPOGJEntry(iPaymentAccountID, dAmountPaid, iOrderID);
        //                modelStockLog.ToList<tbl_StockLog>().ForEach(s => db.Entry(s).State = EntityState.Added);
        //                db.SaveChanges();
        //            }
        //            //}
        //            transaction.Commit();
        //            return result;
        //        }
        //        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
        //        {
        //            Exception raise = dbEx;
        //            foreach (var validationErrors in dbEx.EntityValidationErrors)
        //            {
        //                foreach (var validationError in validationErrors.ValidationErrors)
        //                {
        //                    string message = string.Format("{0}:{1}",
        //                        validationErrors.Entry.Entity.ToString(),
        //                        validationError.ErrorMessage);
        //                    // raise a new exception nesting
        //                    // the current instance as InnerException
        //                    raise = new InvalidOperationException(message, raise);
        //                }
        //            }
        //            transaction.Rollback();
        //            return "";
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            return ex.Message;
        //        }
        //    }
        //}
    }
}
