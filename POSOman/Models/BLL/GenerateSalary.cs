﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POSOman.Models;
using System.Data.Entity;
using AutoMapper;
using System.Text;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace POSOman.Models.BLL
{
    public class GenerateSalary
    {
        public object SaveSalary(DTO.GenerateSalary model, List<DTO.EmployeeLog> modelEmployeeLog, int? bankAccId)
        {
            dbPOS db = new dbPOS();
            int? jEntryID = 0;
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            model.BranchID = branchId;
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentAccountID == 1 || model.PaymentAccountID == 2)
                    {
                        model.ChequeDate = null;
                    }
                    if (model.PaymentAccountID == 1)
                    {
                        model.BankName = "";
                    }



                   
                    //model.Addby = 1;
                    //if (model.TotalOvertime == null)
                    //    model.TotalOvertime = 0;
                    //if (model.TotalDeduction == null)
                    //    model.TotalDeduction = 0;

                    //tbl_EmployeeSettlement set = new tbl_EmployeeSettlement();
                    //set.AddedOn = Helper.PST();
                    //set.Date = Helper.PST();
                    //set.Description = "Employee Monthly Salary";
                    //set.EmployeeID = model.EmployeeID ?? 0;
                    //set.IsDeleted = false;
                    //set.TotalAmount = Convert.ToDecimal(modelEmployeeLog.Sum(x => x.Dr));

                    //db.tbl_EmployeeSettlement.Add(set);
                    //int iD = db.SaveChanges();
                    if(model.TotalOvertime==0 || model.TotalSalary <= 0)
                    {
                        transaction.Rollback();
                        return -1;
                    }
                    else
                    {
                        db.Configuration.ValidateOnSaveEnabled = false;

                        if(model.Addby ==null)
                        {
                            var add = db.AspNetUsers.Where(p => p.Id == model.UserID).Select(p => p.UserId).FirstOrDefault(); 
                         model.Addby = add;

                        }
                        jEntryID = db.insertIndividualSalaryGJEntry(model.PaymentAccountID, model.TotalOvertime, null, model.VoucherDate, model.Addby, model.BranchID, model.BankName, model.ChequeNo, model.ChequeDate, bankAccId, model.UserID).FirstOrDefault();

                        if ( jEntryID > 0)
                        {

                            tbl_EmployeeLog log1 = new tbl_EmployeeLog();
                            log1.AddOn = Helper.PST();
                            log1.PayType = model.PaymentAccountID;
                            log1.Addby = 1;
                            log1.BranchID = 9001;
                            log1.Date = model.VoucherDate;
                            log1.Description = "Monthly Salary";
                            log1.EmployeeID = model.EmployeeID ?? 0;
                            log1.IsActive = true;
                            log1.IsDeleted = false;
                            log1.JEntryID = jEntryID;
                            if (model.TotalSalary > 0)
                            {
                                log1.JentryTypeID = 5;
                                log1.Dr = model.TotalSalary;
                                log1.Cr = 0;

                            }
                            else
                            {
                                return -1;
                            }

                            db.tbl_EmployeeLog.Add(log1);
                            db.SaveChanges();

                            tbl_SalaryLog salLog = new tbl_SalaryLog();
                            salLog.AddedOn = Helper.PST();
                            salLog.JEntryID = jEntryID;
                            salLog.EmployeeID = model.EmployeeID ?? 0;
                            salLog.IsActive = true;
                            salLog.SalaryMonth = Convert.ToDateTime(model.SalaryMonth);
                            salLog.NumberOfDays = model.NoOfDays ?? 0;
                            salLog.SalaryDate = Convert.ToDateTime(model.VoucherDate);
                            salLog.TotalSalary = Convert.ToDecimal(model.TotalOvertime);
                            db.tbl_SalaryLog.Add(salLog);
                            db.SaveChanges();

                           
                            if (modelEmployeeLog!=null) { 
                                foreach (var item in modelEmployeeLog)
                                {
                                    tbl_EmployeeLog log = new tbl_EmployeeLog();
                                    log.AddOn = Helper.PST();
                                    log.Addby = 1;
                                    log.PayType = model.PaymentAccountID;
                                    log.BranchID = 9001;
                                    log.Date = model.VoucherDate;
                                    log.Description = item.Description;
                                    log.EmployeeID = model.EmployeeID ?? 0;
                                    log.IsActive = true;
                                    log.IsDeleted = false;
                                    log.JEntryID = jEntryID;
                                    if (item.Dr > 0)
                                    {
                                        log.JentryTypeID = 39;
                                        log.Dr = item.Dr;
                                        log.Cr = 0;

                                    }
                                    else if (item.Dr < 0)
                                    {
                                        log.JentryTypeID = 40;
                                        log.Cr = item.Dr;
                                        log.Dr = 0;
                                    }
                                    else
                                    {
                                        continue;
                                    }

                                    db.tbl_EmployeeLog.Add(log);

                                    //tbl_EmployeeSettlementDetails det = new tbl_EmployeeSettlementDetails();
                                    //det.IsActive = true;
                                    //det.Amount = Convert.ToDecimal(item.Dr);
                                    //det.AddedOn = Helper.PST();
                                    //det.Description = item.Description;
                                    //det.EmpSettleID = set.ID;
                                    //db.tbl_EmployeeSettlementDetails.Add(det);
                                    db.SaveChanges();

                                    //item.JEntryID = jEntryID;
                                    //item.AddOn = DateTime.UtcNow.AddHours(5);
                                    //item.Addby = 1;
                                    //item.BranchID = branchId;
                                }
                            }
                           
                            // Mapper.CreateMap<Models.DTO.EmployeeLog, tbl_EmployeeLog>();
                            //  var employeeLog = Mapper.Map<ICollection<Models.DTO.EmployeeLog>, ICollection<tbl_EmployeeLog>>(modelEmployeeLog);
                            //  employeeLog.ToList<tbl_EmployeeLog>().ForEach(s => db.Entry(s).State = EntityState.Added);
                            //  db.SaveChanges();
                            transaction.Commit();
                            return jEntryID;
                        }
                        else
                        {
                            transaction.Rollback();
                            return -1;
                        }
                    }
                  
                   
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

        public object SaveSettlement(DTO.GenerateSalary model, List<DTO.EmployeeLog> modelEmployeeLog, int? bankAccId)
        {
            dbPOS db = new dbPOS();
            int? jEntryID = 0;
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            model.BranchID = branchId;
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    //if (model.PaymentAccountID == 1 || model.PaymentAccountID == 2)
                    //{
                    //    model.ChequeDate = null;
                    //}
                    //if (model.PaymentAccountID == 1)
                    //{
                    //    model.BankName = "";
                    //}
                    //model.Addby = 1;
                    //if (model.TotalOvertime == null)
                    //    model.TotalOvertime = 0;
                    //if (model.TotalDeduction == null)
                    //    model.TotalDeduction = 0;

                    tbl_EmployeeSettlement set = new tbl_EmployeeSettlement();
                    set.AddedOn = Helper.PST();
                    set.Date = Helper.PST();
                    set.Description = "Employee Settlement";
                    set.EmployeeID = model.EmployeeID??0;
                    set.IsDeleted = false;
                    set.TotalAmount =Convert.ToDecimal( modelEmployeeLog.Sum(x => x.Dr));

                    db.tbl_EmployeeSettlement.Add(set);
                    int iD = db.SaveChanges();

                    db.Configuration.ValidateOnSaveEnabled = false;
                   // jEntryID = db.insertSalaryGJEntry(model.PaymentAccountID, model.TotalDeduction, model.TotalSalary, model.TotalOvertime, model.VoucherDate, model.Addby, model.BranchID, model.BankName, model.ChequeNo, model.ChequeDate, bankAccId).FirstOrDefault();

                    if (iD > 0 && modelEmployeeLog != null)
                    {
                        foreach (var item in modelEmployeeLog)
                        {
                            tbl_EmployeeSettlementDetails det = new tbl_EmployeeSettlementDetails();
                            det.IsActive = true;
                            det.Amount = Convert.ToDecimal(item.Dr);
                            det.AddedOn = Helper.PST();
                            det.Description = item.Description;
                            det.EmpSettleID = set.ID;
                            db.tbl_EmployeeSettlementDetails.Add(det);
                            db.SaveChanges();

                            //item.JEntryID = jEntryID;
                            //item.AddOn = DateTime.UtcNow.AddHours(5);
                            //item.Addby = 1;
                            //item.BranchID = branchId;
                        }
                       // Mapper.CreateMap<Models.DTO.EmployeeLog, tbl_EmployeeLog>();
                      //  var employeeLog = Mapper.Map<ICollection<Models.DTO.EmployeeLog>, ICollection<tbl_EmployeeLog>>(modelEmployeeLog);
                      //  employeeLog.ToList<tbl_EmployeeLog>().ForEach(s => db.Entry(s).State = EntityState.Added);
                      //  db.SaveChanges();
                    }
                    transaction.Commit();
                    return set.ID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

        public object Save(DTO.GenerateSalary model, List<DTO.EmployeeLog> modelEmployeeLog, int? bankAccId)
        {
            dbPOS db = new dbPOS();
            int? jEntryID = 0;
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            model.BranchID = branchId;
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentAccountID == 1 || model.PaymentAccountID == 2)
                    {
                        model.ChequeDate = null;                        
                    }
                    if (model.PaymentAccountID == 1)
                    {
                        model.BankName = "";
                    }
                    model.Addby = 1;
                    if (model.TotalOvertime == null)
                        model.TotalOvertime = 0;
                    if (model.TotalDeduction == null)
                        model.TotalDeduction = 0;
                    db.Configuration.ValidateOnSaveEnabled = false;
                    jEntryID = db.insertSalaryGJEntry(model.PaymentAccountID, model.TotalDeduction, model.TotalSalary, model.TotalOvertime, model.VoucherDate, model.Addby, model.BranchID, model.BankName, model.ChequeNo, model.ChequeDate,bankAccId).FirstOrDefault();                    
                    
                    if (jEntryID  > 0 && modelEmployeeLog != null)
                    {    
                        foreach (var item in modelEmployeeLog)
                        {
                            item.JEntryID = jEntryID;                           
                            item.AddOn = DateTime.UtcNow.AddHours(5);
                            item.Addby = 1;
                            item.BranchID = branchId;                    
                        }
                        Mapper.CreateMap<Models.DTO.EmployeeLog, tbl_EmployeeLog>();
                        var employeeLog = Mapper.Map<ICollection<Models.DTO.EmployeeLog>, ICollection<tbl_EmployeeLog>>(modelEmployeeLog);
                        employeeLog.ToList<tbl_EmployeeLog>().ForEach(s => db.Entry(s).State = EntityState.Added);
                        db.SaveChanges();
                    }
                    transaction.Commit();                    
                    return jEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }


        public object SaveAdvance(DTO.Expenses model, DTO.EmployeeLog modelEmployeeLog, int? bankAccId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 0;
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            int? jEntryID = 0;            
            model.BranchID = branchId;
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            int empLogID = 0;
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {                    
                    if (model.PaymentTypeID == 1 || model.PaymentTypeID == 2)
                    {
                        model.ChequeDate = null;
                    }
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                    }
                    if (AddBy == 0)
                    {
                        var add = db.AspNetUsers.Where(p => p.Id == model.UserID).Select(p => p.UserId).FirstOrDefault();
                        AddBy = add;

                    }
                    db.Configuration.ValidateOnSaveEnabled = false;
                    jEntryID = db.insertAdvanceGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.BankName, model.ChequeDate, model.ChequeNumber,  model.BranchID, model.Description,bankAccId).FirstOrDefault();

                    if (jEntryID > 0)
                    {
                        modelEmployeeLog.JEntryID = jEntryID; // get it from SP                        
                        modelEmployeeLog.AddOn = DateTime.UtcNow.AddHours(5);
                        modelEmployeeLog.Addby = 1;

                        Mapper.CreateMap<Models.DTO.EmployeeLog, tbl_EmployeeLog>();
                        var employeeLog = Mapper.Map<Models.DTO.EmployeeLog, tbl_EmployeeLog>(modelEmployeeLog);
                        db.tbl_EmployeeLog.Add(employeeLog);
                        db.SaveChanges();
                        empLogID = employeeLog.EmployeeLogID;
                    }                    
                    
                    transaction.Commit();
                    return jEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
    }
}
