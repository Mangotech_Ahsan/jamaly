﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using AutoMapper;
using System.Data.Entity;

namespace POSOman.Models.BLL
{
    public class SaveJournalEntry
    {
        UserActionsPerformed UserActions = new UserActionsPerformed();
        public object SaveEntry(Models.DTO.JournalEntry modelJournalEntry, string UserName)
        {

            dbPOS db = new dbPOS();
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            string result = "";
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (modelJournalEntry.BranchID == null || modelJournalEntry.JDetail.Any(x => x.BranchID == null))
                    {
                        return false;
                    }
                    else
                    {
                        db.Configuration.ValidateOnSaveEnabled = false;

                        long? PrevCode = db.tbl_JEntry.OrderByDescending(x => x.CodeInt).Select(x => x.CodeInt).FirstOrDefault();
                        long NewCode = 0;
                        string NewCodeSt;
                        if (PrevCode != null)
                        {
                            NewCode = Convert.ToInt64(PrevCode) + 1;
                            NewCodeSt = ("GJ-" + NewCode).ToString();
                        }
                        else
                        {
                            NewCode = 950001;
                            NewCodeSt = ("GJ-" + NewCode).ToString();
                        }
                        int AccID = 0;
                        modelJournalEntry.IsActive = true;
                        if (modelJournalEntry.JDetail.All(x => x.MultiEntryDate != null))
                        {
                            foreach (var i in modelJournalEntry.JDetail)
                            {
                                i.IsMultiDateEntry = true;
                            }
                            modelJournalEntry.IsMultiDateEntry = true;
                        }


                        foreach (var i in modelJournalEntry.JDetail)
                        {
                            //if (db.tbl_Vendor.Any(x => x.AccountID == i.AccountID) || db.tbl_Customer.Any(x => x.AccountID == i.AccountID))
                            //{
                                i.RefAccountID = Convert.ToInt32(i.AccountID);

                            //}
                            i.CodeInt = NewCode;
                            i.CodeString = NewCodeSt;
                            i.UserName = UserName;
                            i.ThorughGJ = true;
                            i.EntryTypeID = 36;//Manual Entry
                            i.AddOn = Helper.PST();
                            i.Detail = string.IsNullOrWhiteSpace(i.Detail) ?"Manual Entry": "Manual Entry: " + i.Detail.ToString();
                        }

                        // pass login user id
                        modelJournalEntry.AddBy = 1;
                        modelJournalEntry.AddOn = Helper.PST();
                        modelJournalEntry.CodeInt = NewCode;
                        modelJournalEntry.CodeString = NewCodeSt;
                        modelJournalEntry.ThroughGJ = true;
                        modelJournalEntry.UserName = UserName;
                        
                        modelJournalEntry.UserID = HttpContext.Current.User.Identity.GetUserId();

                        Mapper.CreateMap<Models.DTO.JournalEntry, tbl_JEntry>();
                        Mapper.CreateMap<Models.DTO.JDetail, tbl_JDetail>();
                        var details = Mapper.Map<ICollection<Models.DTO.JDetail>, ICollection<tbl_JDetail>>(modelJournalEntry.JDetail);
                        var master = Mapper.Map<Models.DTO.JournalEntry, tbl_JEntry>(modelJournalEntry);
                        master.tbl_JDetail = details;
                        db.tbl_JEntry.Add(master);
                        db.SaveChanges();

                        transaction.Commit();
                        result = master.JEntryId.ToString();
                        return result;
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(),validationError.ErrorMessage);

                            raise = new InvalidOperationException(message, raise);
                            UserActions.MapActions(0, UserName + "(ErrorGJ Entry(" + message.ToString() + "))".ToString());
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    UserActions.MapActions(0, UserName + "(ErrorGJ Entry(" + ex.ToString() + "))".ToString());
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

        public object SaveEditGJEntry(Models.DTO.JournalEntry modelJournalEntry, string UserName)
        {
            dbPOS db = new dbPOS();
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;

                    if (modelJournalEntry.BranchID == null || modelJournalEntry.JDetail.Any(x => x.BranchID == null))
                    {
                        return false;
                    }
                    else
                    {
                        var PrevJEntry = db.tbl_JEntry.Where(x => x.JEntryId == modelJournalEntry.JEntryId).FirstOrDefault();
                        var PrevJDetail = db.tbl_JDetail.Where(x => x.JEntryID == modelJournalEntry.JEntryId).ToList();
                        if (PrevJDetail != null)
                        {
                            foreach (var r in PrevJDetail)
                            {
                                db.Entry(r).State = EntityState.Deleted;
                                db.SaveChanges();
                            }

                        }
                        db.Entry(PrevJEntry).State = EntityState.Deleted;
                        db.SaveChanges();

                        modelJournalEntry.JEntryId = 0;
                        int AccID = 0;

                        long? PrevCode = db.tbl_JEntry.OrderByDescending(x => x.CodeInt).Select(x => x.CodeInt).FirstOrDefault();
                        long NewCode = 0;
                        string NewCodeSt;
                        if (PrevCode != null)
                        {
                            NewCode = Convert.ToInt64(PrevCode) + 1;
                            NewCodeSt = ("GJ-" + NewCode).ToString();
                        }
                        else
                        {
                            NewCode = 950001;
                            NewCodeSt = ("GJ-" + NewCode).ToString();
                        }
                        modelJournalEntry.IsActive = true;
                        if (modelJournalEntry.JDetail.All(x => x.MultiEntryDate != null))
                        {
                            foreach (var i in modelJournalEntry.JDetail)
                            {
                                i.IsMultiDateEntry = true;
                            }
                            modelJournalEntry.IsMultiDateEntry = true;
                        }
                        foreach (var i in modelJournalEntry.JDetail)
                        {
                            //if (db.tbl_Vendor.Any(x => x.AccountID == i.AccountID) || db.tbl_Customer.Any(x => x.AccountID == i.AccountID))
                            //{
                                // AccID = Convert.ToInt32(i.AccountID);
                                i.RefAccountID = Convert.ToInt32(i.AccountID);

                            //}
                            // i.RefAccountID = AccID;
                            i.CodeInt = NewCode;
                            i.CodeString = NewCodeSt;
                            i.UserName = UserName;
                            i.ThorughGJ = true;
                            i.EntryTypeID = 36;//Manual Entry
                            i.AddOn = Helper.PST();
                            i.Detail = "Manual Entry";
                        }
                        #region (inactive)-previous jdetail entry saving scenario without multi entry 

                        //foreach (var i in modelJournalEntry.JDetail)
                        //{
                        //    if (db.tbl_Vendor.Any(x => x.AccountID == i.AccountID) || db.tbl_Customer.Any(x => x.AccountID == i.AccountID))
                        //    {
                        //        AccID = Convert.ToInt32(i.AccountID);


                        //    }
                        //    // i.RefAccountID = AccID;
                        //    i.CodeInt = NewCode;
                        //    i.CodeString = NewCodeSt;
                        //    i.UserName = UserName;
                        //    i.ThorughGJ = true;
                        //    i.EntryTypeID = 36;//Manual Entry
                        //    i.AddOn = Helper.PST();
                        //    i.Detail = "Manual Entry";
                        //}
                        //foreach (var i in modelJournalEntry.JDetail)
                        //{
                        //    //if (db.tbl_Vendor.Any(x => x.AccountID == i.AccountID) || db.tbl_Customer.Any(x => x.AccountID == i.AccountID))
                        //    //{
                        //    //    continue;
                        //    //}
                        //    //else
                        //    //{
                        //    //    i.RefAccountID = AccID;
                        //    //}
                        //    i.RefAccountID = AccID;

                        //}

                        #endregion
                        // pass login user id
                        modelJournalEntry.CodeInt = NewCode;
                        modelJournalEntry.CodeString = NewCodeSt;
                        modelJournalEntry.UserName = UserName;
                        modelJournalEntry.AddBy = 1;
                        modelJournalEntry.ThroughGJ = true;
                        modelJournalEntry.AddOn = Helper.PST();
                        modelJournalEntry.UpdateOn = Helper.PST();
                        modelJournalEntry.UserID = HttpContext.Current.User.Identity.GetUserId();

                        Mapper.CreateMap<Models.DTO.JournalEntry, tbl_JEntry>();
                        Mapper.CreateMap<Models.DTO.JDetail, tbl_JDetail>();
                        var details = Mapper.Map<ICollection<Models.DTO.JDetail>, ICollection<tbl_JDetail>>(modelJournalEntry.JDetail);
                        var master = Mapper.Map<Models.DTO.JournalEntry, tbl_JEntry>(modelJournalEntry);
                        master.tbl_JDetail = details;
                        db.tbl_JEntry.Add(master);
                        db.SaveChanges();

                        transaction.Commit();
                        result = master.JEntryId.ToString();
                        return result;
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                            UserActions.MapActions(0, UserName + "(Erro E-GJ(" + message.ToString() + "))".ToString());
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    UserActions.MapActions(0, UserName + "(ErrorE-GJ(" + ex.ToString() + "))".ToString());
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }


        public object DeleteGJEntry(int JEntryID)
        {
            dbPOS db = new dbPOS();
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            // string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;

                    var PrevJEntry = db.tbl_JEntry.Where(x => x.JEntryId == JEntryID).FirstOrDefault();
                    var PrevJDetail = db.tbl_JDetail.Where(x => x.JEntryID == JEntryID).ToList();
                    if (PrevJDetail != null)
                    {
                        foreach (var r in PrevJDetail)
                        {
                            db.Entry(r).State = EntityState.Deleted;
                            db.SaveChanges();
                        }

                    }
                    db.Entry(PrevJEntry).State = EntityState.Deleted;
                    db.SaveChanges();


                    transaction.Commit();
                    //result = master.JEntryId.ToString();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
    }
}