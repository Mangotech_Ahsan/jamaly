﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POSOman.Models;
using System.Data.Entity;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using System.Text;

namespace POSOman.Models.BLL
{
    public class tmpOrder
    {
        dbPOS db = new dbPOS();
        public void Save(tmp_PO model, int branchId)
        {
           
            try
            {
                decimal? GST = 0;
                decimal? Discount = 0;

                if(model.DiscountAmount == null || model.DiscountAmount<=0)
                {
                    foreach(var i in model.tmp_OrderDetails)
                    {
                      
                        Discount += i.DiscountAmount ?? 0;
                    }
                }

                if(model.VAT == null || model.VAT <= 0)
                {
                    foreach (var i in model.tmp_OrderDetails)
                    {
                        GST += i.GSTAmount ?? 0;
                       
                    }

                }


                if (GST > 0)
                {
                    model.VAT = GST;
                }

                if (Discount > 0)
                {
                    model.DiscountAmount = Discount;
                }

                if (model.PaymentStatus == "UnPaid")
                {
                    model.AmountPaid = 0;
                    model.IsPaid = false;
                }
                else if (model.PaymentStatus == "Paid")
                {
                    model.IsPaid = true;
                    model.AmountPaid = model.TotalAmount;// + GST - Discount;
                }
                int? iPaymentAccountID = null;
                int iOrderID = 0;
                // Use  using 
                model.AddOn = DateTime.UtcNow.AddHours(5);
                model.IsReturned = false;
                model.IsPO = false;
                model.BranchID = branchId;                
                model.UserID = HttpContext.Current.User.Identity.GetUserId();
                bool isChanged = false;
                if (model.PaymentTypeID > 0)
                {
                    iPaymentAccountID = (int)(model.PaymentTypeID);
                }
                // handle if tble_PO is empty
               if (model.tmpID == getTmpOrderID())
                {
                    var row = db.tmp_PO.FirstOrDefault(t => t.tmpID == model.tmpID);
                    if (row != null)
                    {                        
                        row.AccountID = model.AccountID;
                        row.AmountPaid = model.AmountPaid;
                        row.BranchID = branchId;
                        row.Currency = model.Currency;
                        row.Expenses = model.Expenses;
                        row.InvoiceNo = model.InvoiceNo;
                        row.PaymentTypeID = model.PaymentTypeID;
                        row.PaymentStatus = model.PaymentStatus;
                        row.TotalAmount = model.TotalAmount;
                        db.Entry(row).State = EntityState.Modified;
                    }
                    foreach (var item in model.tmp_OrderDetails)
                    {
                        item.BranchID = branchId;
                        if (db.tmp_OrderDetails.Any(o=> o.tmpID == item.tmpID && o.ProductID == item.ProductID && o.tmp_PO.AddBy == model.AddBy))
                        {
                            iOrderID = db.tmp_OrderDetails.Where(o => o.tmpID == item.tmpID && o.ProductID == item.ProductID && o.BranchID == branchId).FirstOrDefault().OrderID;
                        }
                        else
                        {
                            isChanged = true;
                            tmp_OrderDetails poDet = new tmp_OrderDetails();
                            poDet.OrderID = iOrderID;
                            poDet.PartNo = item.PartNo;
                            poDet.ProductID = item.ProductID;
                            poDet.GSTAmount = item.GSTAmount;
                            poDet.GSTPercent = item.GSTPercent;
                            poDet.DiscountAmount = item.DiscountAmount;
                            poDet.DiscountPercent = item.DiscountPercent;
                            poDet.BranchID = branchId;
                            poDet.Qty = item.Qty;
                            poDet.tmpID = item.tmpID;
                            poDet.UnitPrice = item.UnitPrice;
                            poDet.SalePrice = item.SalePrice;
                            poDet.ExchangeRate = item.ExchangeRate;
                            poDet.ExpiryDate = item.ExpiryDate;
                            poDet.Total = item.Total;
                            db.tmp_OrderDetails.Add(poDet);
                        }
                    }
                    if (isChanged)
                    {
                        db.SaveChanges();
                    }                    
                }
                else
                {
                    db.tmp_PO.Add(model);
                    int poId = db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                db.InsertError(1, 1, 1, 1, "Temp Order Save", ex.Message, model.UserID, "Clover", DateTime.UtcNow.AddHours(5));
            }
        }
            public int getTmpOrderID()
        {
            int tempID = 0;
            int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);
            var tmp = db.tmp_PO.Where(tpo => tpo.BranchID == branchId).OrderByDescending(v => v.OrderID).FirstOrDefault();
            if (tmp == null)
            {
                tempID = 0;
            }
            else {
                tempID = tmp.tmpID;
            }
            return tempID;
        }
        public void SavePurchaseOrder(tmp_PO model,int branchId)
        {
            try
            {
                //int branchId = Convert.ToInt32(WebConfigurationManager.AppSettings["BranchId"]);                
                model.AddOn = DateTime.UtcNow.AddHours(5);
                model.IsPO = true;
                string newPOID = "";
                var tmp = db.tmp_PO.Where(v => v.IsPO == true && v.BranchID == branchId).OrderByDescending(v => v.OrderID).FirstOrDefault();
                if (tmp != null)
                {
                    newPOID = tmp.PurchaseCode;
                    if (newPOID == null)
                    {
                        newPOID = "PO-10000";
                    }
                    else
                    {
                        string[] id = newPOID.Split('-');
                        var poID = Convert.ToInt32(id[1]) + 1;
                        StringBuilder sbCode = new StringBuilder();
                        sbCode.Append("PO-");
                        sbCode.Append(poID);
                        newPOID = sbCode.ToString();
                    }
                }
                else
                {
                    newPOID = "PO-10000";
                }
                model.PurchaseCode = newPOID;
                model.UserID = HttpContext.Current.User.Identity.GetUserId();
                // handle if tble_PO is empty
                model.BranchID = branchId;                                
                db.tmp_PO.Add(model);
                foreach (var item in model.tmp_OrderDetails)
                {
                    item.BranchID = branchId;
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                //ex.Message;
            }
        }
    }

}
