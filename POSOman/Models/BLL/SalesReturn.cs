﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace POSOman.Models.BLL
{
    public class SalesReturn
    {
        public object Save(Models.DTO.SalesReturn model,List<Models.DTO.StockLog> modelStockLog,int? bankAccId, int branchId)
        {
            dbPOS db = new dbPOS();                        
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentStatus == "UnPaid")
                    {
                        model.AmountPaid = 0;
                        model.IsPaid = false;
                    }
                    else if (model.PaymentStatus == "Partial Paid")
                    {
                        model.IsPaid = false;
                    }
                    else if (model.PaymentStatus == "Paid")
                    {
                        model.IsPaid = true;
                    }
                    db.Configuration.ValidateOnSaveEnabled = false;                    
                    model.AddOn = DateTime.UtcNow.AddHours(5);
                    model.BranchID = branchId;
                    model.UserID = HttpContext.Current.User.Identity.GetUserId();
                    
                    string sPaymentStatus = model.PaymentStatus;
                    int? iPaymentAccountID = null;
                    decimal COGS = 0;
                    //if (model.PaymentTypeID == 1)
                    //{
                    //    model.BankName = "";
                    //    iPaymentAccountID = (int)(model.PaymentTypeID);//  set it from Payment Type ddl 
                    //}
                    //else if (model.PaymentTypeID > 1)
                    //{
                    //    iPaymentAccountID = bankAccId;
                    //}
                    #region cc
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                        iPaymentAccountID = (int)(model.PaymentTypeID);//  set it from Payment Type ddl 
                    }
                    else if (model.PaymentTypeID > 1)
                    {
                        iPaymentAccountID = bankAccId;
                    }
                    if (model.PaymentTypeID == 4)
                    {
                        model.BankName = "";
                        iPaymentAccountID = 109;//  set it from Payment Type ddl 
                    }
                    if (model.PaymentTypeID == 5)
                    {
                        //modelSales.BankName = "CreditCard";
                        iPaymentAccountID = 110;//  set it from Payment Type ddl 
                    }
                    #endregion
                    Mapper.CreateMap<Models.DTO.SalesReturn, tbl_SalesReturn>();
                    Mapper.CreateMap<Models.DTO.SOReturnDetails, tbl_SOReturnDetails>();
                    var ReturnDetails = Mapper.Map<ICollection<Models.DTO.SOReturnDetails>, ICollection<tbl_SOReturnDetails>>(model.SOReturnDetails);
                    var master = Mapper.Map<Models.DTO.SalesReturn, tbl_SalesReturn>(model);
                    master.tbl_SOReturnDetails = ReturnDetails;
                    db.tbl_SalesReturn.Add(master);
                    int poId = db.SaveChanges();
                    int iReturnID = master.SalesReturnID;
                    decimal dAmountPaid = 0;
                    if (master.AmountPaid != null || master.AmountPaid > 0)
                        dAmountPaid = (decimal)(master.AmountPaid);
                    if (poId >= 1 && modelStockLog != null)
                    {
                        // get all product ids 
                        List<int> lstProdIds = master.tbl_SOReturnDetails.Select(p => p.ProductID).ToList();
                        // get all stock where productid contains lstProdIds
                        var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == master.BranchID).ToList();
                        var lstSODetail = db.tbl_SaleDetails.Where(s => lstProdIds.Contains(s.ProductID) && (s.OrderID == master.SaleOrderID)).ToList();
                        List<tbl_SaleDetails> returningOrders = lstSODetail.ToList();
                        List<tbl_Stock> existingStock = lstStock.ToList();
                        // 
                        foreach (var item in modelStockLog)
                        {
                            //item.InvoiceDate = DateTime.UtcNow.AddHours(5).Date;
                            //item.OrderID = iReturnID;
                            //item.OrderTypeID = 4;
                            //item.AddOn = DateTime.UtcNow.AddHours(5);
                            //item.AddBy = master.AddBy;
                            //item.BranchID = branchId;
                            //COGS += Convert.ToDecimal(item.CostPrice) * Convert.ToDecimal(item.StockIN);
                            lstProdIds.Add(item.ProductID);
                            //  if already exists stock
                            var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
                            var soProduct = returningOrders.FirstOrDefault(s => s.ProductID == item.ProductID);
                            #region
                            // Updating SODetails
                            if (soProduct != null)
                            {
                                soProduct.ReturnedQty += Convert.ToInt32(item.StockIN);
                                soProduct.UpdateOn = DateTime.UtcNow.AddHours(5);
                                if (soProduct.Qty == soProduct.ReturnedQty)
                                {
                                    soProduct.IsReturned = true;
                                }
                                db.Entry(soProduct).State = EntityState.Modified;
                            }
                            #endregion

                            #region FIFO 
                            var rStockIn = item.StockIN ?? 0;
                            var rowCount = 0;
                            decimal stLogQty = 0;
                            var stockLogFIFO = db.tbl_StockLog.Where(sl => sl.OrderTypeID == 3 && sl.OrderID == model.SaleOrderID && sl.IsSOReturned != true && sl.ProductID == item.ProductID && sl.IsActive != false).OrderByDescending(sl => sl.LogID).ToList();
                            while (rStockIn > 0)
                            {
                                var StockLogFIFO = stockLogFIFO[rowCount];
                                var sbRow = db.tbl_StockBatch.Where(b => b.StockBatchID == StockLogFIFO.StockBatchID).FirstOrDefault();
                                var stLogMatchQty = (StockLogFIFO.StockOut - StockLogFIFO.ReturnedQty);
                                if (rStockIn > stLogMatchQty)
                                {
                                    //StockLogFIFO.ReturnedQty += rStockIn;
                                    StockLogFIFO.ReturnedQty += stLogMatchQty;
                                    StockLogFIFO.IsSOReturned = true;
                                    COGS += (stLogMatchQty * StockLogFIFO.CostPrice) ?? 0;
                                    rStockIn -= stLogMatchQty ?? 0;
                                    if (sbRow != null)
                                    {
                                        sbRow.Qty += stLogMatchQty ?? 0;
                                        db.Entry(sbRow).State = EntityState.Modified;
                                    }    
                                                                    
                                    db.Entry(StockLogFIFO).State = EntityState.Modified;
                                    stLogQty = stLogMatchQty ?? 0;

                                }
                                else if (rStockIn < stLogMatchQty)
                                {
                                    StockLogFIFO.ReturnedQty += rStockIn;
                                    COGS += (rStockIn * StockLogFIFO.CostPrice) ?? 0;
                                    if (sbRow != null)
                                    {
                                        sbRow.Qty += rStockIn;
                                        db.Entry(sbRow).State = EntityState.Modified;
                                    }                                    
                                    db.Entry(StockLogFIFO).State = EntityState.Modified;
                                    stLogQty = rStockIn;                                    
                                    rStockIn = 0;
                                }
                                else if (rStockIn == stLogMatchQty)
                                {
                                    StockLogFIFO.ReturnedQty += rStockIn;
                                    StockLogFIFO.IsSOReturned = true;
                                    COGS += (rStockIn * StockLogFIFO.CostPrice) ?? 0;
                                    if (sbRow != null)
                                    {
                                        sbRow.Qty += rStockIn;
                                        db.Entry(sbRow).State = EntityState.Modified;
                                    }                                    
                                    db.Entry(StockLogFIFO).State = EntityState.Modified;
                                    stLogQty = rStockIn;                                    
                                    rStockIn = 0;
                                }
                                
                                    tbl_StockLog stLog = new tbl_StockLog();
                                stLog.AccountID = item.AccountID;
                                stLog.StockBatchID = (sbRow != null) ? sbRow.StockBatchID : 0;
                                stLog.AddBy = master.AddBy;
                                stLog.AddOn = DateTime.UtcNow.AddHours(5);
                                stLog.BranchID = branchId;
                                stLog.CostPrice = StockLogFIFO.CostPrice;
                                stLog.InvoiceDate = master.ReturnDate;
                                stLog.IsActive = true;
                                stLog.IsMinor = item.IsMinor;
                                stLog.IsOpen = item.IsOpen;
                                stLog.IsPack = item.IsPack;
                                stLog.LevelID = item.LevelID;
                                stLog.MinorDivisor = item.MinorDivisor;
                                stLog.OrderID = iReturnID;
                                stLog.OrderTypeID = 4;
                                stLog.OutReference = "Sales Return";
                                stLog.OutReferenceID = item.OutReferenceID;
                                stLog.ProductID = item.ProductID;
                                stLog.SalePrice = item.SalePrice;
                                stLog.StockIN = stLogQty;
                                stLog.StockOut = 0;
                                stLog.UnitCode = item.UnitCode;
                                stLog.UnitID = item.UnitID;
                                stLog.UnitPerCarton = item.UnitPerCarton;
                                stLog.UserReferenceID = item.UserReferenceID;
                                db.tbl_StockLog.Add(stLog);
                                rowCount += 1;

                            }
                            #endregion
                            if (row != null)
                            {
                                row.Qty += Convert.ToDecimal(item.StockIN);
                                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                db.Entry(row).State = EntityState.Modified;
                            }
                        }   
                        //Mapper.CreateMap<Models.DTO.StockLog, tbl_StockLog>();
                        //var stockLog = Mapper.Map<ICollection<Models.DTO.StockLog>, ICollection<tbl_StockLog>>(modelStockLog);
                        //stockLog.ToList<tbl_StockLog>().ForEach(s => db.Entry(s).State = EntityState.Added);
                        var returnOrder = db.tbl_SalesOrder.Where(s => s.OrderID == model.SaleOrderID).FirstOrDefault();
                        if (returnOrder != null)
                        {
                            if (returnOrder.ReturnAmount == null)
                            {
                                returnOrder.ReturnAmount = model.TotalAmount;                                
                            }
                            else
                            { returnOrder.ReturnAmount += model.TotalAmount; }
                            returnOrder.UpdateOn = DateTime.UtcNow.AddHours(5);
                            returnOrder.Can_Modify = false;
                            db.Entry(returnOrder).State = EntityState.Modified;
                        }
                        if (sPaymentStatus == "Paid")
                        {
                            db.insertSOReturnGJEntryPaid(COGS, iPaymentAccountID, iReturnID);
                        }
                        else if (sPaymentStatus == "Partial Paid")
                        {
                            db.insertSOReturnGJEntryPartiallyPaid(COGS, iPaymentAccountID, dAmountPaid, iReturnID);
                        }
                        else if (sPaymentStatus == "UnPaid")
                        {
                            db.insertSOReturnGJEntry(COGS, iReturnID);
                        }
                       
                        db.SaveChanges();
                    }                
                    transaction.Commit();
                    db.UpdateSalesTable(model.SaleOrderID);
                    return iReturnID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
    }
}
