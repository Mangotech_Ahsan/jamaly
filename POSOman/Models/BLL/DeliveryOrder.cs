﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity;
using System.Web;
using System.Web.Configuration;
using System.Text;

namespace POSOman.Models.BLL
{
    public class DeliveryOrder
    {
        public object createSales(Models.DTO.DeliveryOrder modelDO, List<Models.DTO.StockLog> modelStockLog, int branchId)
        {
            dbPOS db = new dbPOS();

            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {                    
                    db.Configuration.ValidateOnSaveEnabled = false;
                    var user = HttpContext.Current.User.Identity;
                    modelDO.UserID = user.GetUserId();
                    modelDO.AddOn = DateTime.UtcNow.AddHours(5);
                    modelDO.IsReturned = false;
                    modelDO.BranchID = branchId;
                    string newDOID = "";
                    var tmp = db.tbl_DeliveryOrder.Where(d => d.BranchID == branchId).OrderByDescending(v => v.OrderID).FirstOrDefault();
                    if (tmp != null)
                    {
                        newDOID = tmp.DOID;
                        if (newDOID == null)
                        {
                            newDOID = "DO-100";
                        }
                        else
                        {
                            string[] id = newDOID.Split('-');
                            var poID = Convert.ToInt32(id[1]) + 1;
                            StringBuilder sbCode = new StringBuilder();
                            sbCode.Append("DO-");
                            sbCode.Append(poID);
                            newDOID = sbCode.ToString();
                        }
                    }
                    else
                    {
                        newDOID = "DO-100";
                    }
                    modelDO.DOID = newDOID;
                    Mapper.CreateMap<Models.DTO.DeliveryOrder, tbl_DeliveryOrder>();
                    Mapper.CreateMap<Models.DTO.DODetails, tbl_DODetails>();
                    var details = Mapper.Map<ICollection<Models.DTO.DODetails>, ICollection<tbl_DODetails>>(modelDO.DODetails);
                    var master = Mapper.Map<Models.DTO.DeliveryOrder, tbl_DeliveryOrder>(modelDO);
                    master.tbl_DODetails = details;                    
                    db.tbl_DeliveryOrder.Add(master);
                    int soId = db.SaveChanges();
                    int iOrderID = master.OrderID;                    
                    if (soId >= 1 && modelStockLog != null)
                    {
                        // get all product ids 
                        List<int> lstProdIds = master.tbl_DODetails.Select(p => p.ProductID).ToList();
                        // get all stock where productid contains lstProdIds
                        var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == master.BranchID).ToList();
                        List<tbl_Stock> existingStock = lstStock.ToList();
                        // 
                        foreach (var item in modelStockLog)
                        {
                            item.InvoiceDate = master.SalesDate;
                            item.OrderID = iOrderID;
                            item.AddOn = DateTime.UtcNow.AddHours(5);
                            item.OrderTypeID = 7;
                            item.AddBy = 1;
                            item.BranchID = branchId;
                            lstProdIds.Add(item.ProductID);
                            //  if already exists stock
                            var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
                            if (row != null)
                            {
                                row.SalePrice = item.SalePrice;                                
                                row.Qty -= Convert.ToInt32(item.StockOut);
                                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                row.BranchID = item.BranchID;
                                row.Location = item.Location;
                                db.Entry(row).State = EntityState.Modified;
                            }
                            else
                            {
                                // if new item is not In Stock and User is Selling (As Discussed with Boss) 
                                tbl_Stock newStock = new tbl_Stock();
                                newStock.ProductID = item.ProductID;
                                newStock.Qty = Convert.ToInt32(item.StockOut * (-1));
                                newStock.SalePrice = item.SalePrice;
                                newStock.Addon = DateTime.UtcNow.AddHours(5);
                                newStock.Location = item.Location;
                                newStock.BranchID = item.BranchID;
                                newStock.CostPrice = item.CostPrice;
                                newStock.OnMove = 0;
                                db.tbl_Stock.Add(newStock);
                            }
                        }                       
                        Mapper.CreateMap<Models.DTO.StockLog, tbl_StockLog>();
                        var stockLogModel = Mapper.Map<ICollection<Models.DTO.StockLog>, ICollection<tbl_StockLog>>(modelStockLog);

                        stockLogModel.ToList<tbl_StockLog>().ForEach(s => db.Entry(s).State = EntityState.Added);
                        
                        db.SaveChanges();
                    }
                    transaction.Commit();
                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        // Create Invoice of Delivery All Orders selected by user Just Accounts will hit other data will be just insert to SO
        public object createInvoice(Models.DTO.Sales modelSales,int[] doIDs, int? bankAccId)
        {            
            dbPOS db = new dbPOS();
            string result = "";
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    if (modelSales.DiscountAmount > 0)
                    {
                        modelSales.TotalAmount = modelSales.TotalAmount - modelSales.DiscountAmount;
                    }
                    if (modelSales.PaymentStatus == "UnPaid")
                    {
                        modelSales.AmountPaid = 0;
                        modelSales.IsPaid = false;
                    }
                    else if (modelSales.PaymentStatus == "Partial Paid")
                    {
                        modelSales.IsPaid = false;
                    }
                    else if (modelSales.PaymentStatus == "Paid")
                    {
                        modelSales.IsPaid = true;
                    }
                    if (!modelSales.DiscountAmount.HasValue)
                    {
                        modelSales.DiscountAmount = 0;
                    }
                    modelSales.TotalPaid = modelSales.AmountPaid;
                    string sPaymentStatus = modelSales.PaymentStatus;
                    int? iPaymentAccountID = null;
                    decimal COGS = Convert.ToDecimal(modelSales.COGS);
                    // Use  using 
                    modelSales.AddOn = DateTime.UtcNow.AddHours(5);
                    modelSales.IsReturned = false;
                    if (modelSales.PaymentTypeID == 1 )
                    {
                        modelSales.BankName = "";
                        iPaymentAccountID = (int)(modelSales.PaymentTypeID);
                    }
                    else if (modelSales.PaymentTypeID > 1)
                    {
                        iPaymentAccountID = bankAccId;
                    }
                    Mapper.CreateMap<Models.DTO.Sales, tbl_SalesOrder>();
                    Mapper.CreateMap<Models.DTO.SaleDetails, tbl_SaleDetails>();
                    var details = Mapper.Map<ICollection<Models.DTO.SaleDetails>, ICollection<tbl_SaleDetails>>(modelSales.SaleDetails);
                    var master = Mapper.Map<Models.DTO.Sales, tbl_SalesOrder>(modelSales);
                    master.tbl_SaleDetails = details;                   
                    db.tbl_SalesOrder.Add(master);
                    int soId = db.SaveChanges();
                    int iOrderID = master.OrderID;
                    decimal dAmountPaid = 0;
                    if (master.AmountPaid != null || master.AmountPaid > 0)
                        dAmountPaid = (decimal)(master.AmountPaid);
                    if (soId >= 1)
                    {                       
                        if (sPaymentStatus == "Paid")
                        {
                            db.insertSOGJEntryPaid(iPaymentAccountID, iOrderID,COGS);
                        }
                        else if (sPaymentStatus == "Partial Paid")
                        {
                            db.insertSOGJEntryPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID,COGS);
                        }
                        else if (sPaymentStatus == "UnPaid")
                        {
                            db.insertSOGJEntryUnpaid(iOrderID,COGS);
                        }
                        foreach (var item in doIDs)
                        {
                            var status = db.tbl_DeliveryOrder.SingleOrDefault(p => p.OrderID == item);
                            if (status != null)
                            {
                                status.IsPaid = true;
                                status.SaleOrderID = iOrderID;
                            }
                        }                       
                        db.SaveChanges();
                    }
                    transaction.Commit();
                    result = iOrderID.ToString();
                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

    }
}
