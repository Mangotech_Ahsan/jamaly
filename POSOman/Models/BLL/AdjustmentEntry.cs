﻿using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using AutoMapper;
using System.Data.Entity;
using System.Threading.Tasks;

namespace POSOman.Models.BLL
{
    public class AdjustmentEntry
    {

        public object SaveCustomerAdjustment(Models.DTO.CustomerReceiving model, List<Models.DTO.JEntryLog> jentryLog, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            int? jEntryTypeID = 0;
            int creditAccountID = 0;
            int debitAccountID = 0;
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    jEntryTypeID = 14; // Customer Adjustment                    
                    debitAccountID = 18; // special discount from accountdetails table
                    creditAccountID = model.AccountID;//  customer Account less 
                    var orderID = jentryLog[0].OrderID;
                    model.OrderID = Convert.ToInt32(orderID);
                    StringBuilder CustomerMemo = new StringBuilder();
                    StringBuilder PaymentMemo = new StringBuilder();
                    db.Configuration.ValidateOnSaveEnabled = false;
                    // get all Order ids 
                    List<int> lstOrderIds = model.Details.Select(p => p.OrderID).ToList();
                    // get all SaleeOrders where OrderID contains lstOrderIds
                    var lstOrders = db.tbl_SalesOrder.Where(s => lstOrderIds.Contains(s.OrderID)).ToList();
                    List<tbl_SalesOrder> existingOrders = lstOrders.ToList();
                    //
                    if (lstOrderIds.Count > 0)
                    {
                        CustomerMemo.Append("Adjustment for PO No: ");   // change po no by system gen id here 
                    }
                    foreach (var item in model.Details)
                    {
                        lstOrderIds.Add(item.OrderID);
                        //  if already exists 
                        var row = existingOrders.FirstOrDefault(s => s.OrderID == item.OrderID);
                        if (row != null)
                        {
                            CustomerMemo.Append(row.SOID + ",");
                            decimal? totalPaid = row.TotalPaid + item.Receiving;
                            row.TotalPaid += item.Receiving;// enter totalPaid = 0 at time of order
                            if (row.SpecialDiscount == null || row.SpecialDiscount == 0)
                            { row.SpecialDiscount = item.Receiving; }
                            else
                            { row.SpecialDiscount += item.Receiving; }
                            //
                            if (totalPaid == row.TotalAmount)
                            {
                                row.PaymentStatus = "Paid";
                                row.IsPaid = true;
                            }
                            else if (row.AmountPaid < row.TotalAmount)
                            {
                                row.PaymentStatus = "Partial Paid";
                            }
                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(row).State = EntityState.Modified;
                        }
                    }
                    jEntryID = db.insertGJEntry(model.Amount, debitAccountID, model.VoucherDate, AddBy, creditAccountID, CustomerMemo.ToString(), branchId, model.Description, jEntryTypeID, model.UserID).FirstOrDefault();
                    foreach (var item in jentryLog)
                    {
                        item.JEntryID = jEntryID ?? 0; // get it from SP                                                
                        item.BranchID = branchId;
                    }
                    Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                    var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jentryLog);
                    foreach (var item in jLog)
                    {
                        db.tbl_JEntryLog.Add(item);
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return jEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        public object SaveEntry(Expenses model, string CustomerName, bool? isVendor, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            int? jEntryTypeID = 0;
            int creditAccountID = 0;    //
            int debitAccountID = 0;     // 
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            StringBuilder Memo = new StringBuilder();
            if (isVendor == true)
            {
                jEntryTypeID = 13; // Vendor Adjustment
                Memo.Append("Adjustment Entry For Vendor ");
                debitAccountID = model.AccountID;
                creditAccountID = 18; //18; // AdustAccountId from accountdetails table
            }
            else
            {
                jEntryTypeID = 14; // Customer Adjustment
                Memo.Append("Adjustment Entry For Customer " + CustomerName);
                //debitAccountID = 19; //19; // special discount from accountdetails table
                //creditAccountID = 1;//  Cash Account less 
                debitAccountID = 18; // special discount from accountdetails table
                creditAccountID = model.AccountID;//  customer Account less 
            }
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    model.BranchID = branchId;

                    db.Configuration.ValidateOnSaveEnabled = false;

                    jEntryID = jEntryID = db.insertGJEntry(model.Amount, debitAccountID, model.VoucherDate, AddBy, creditAccountID, Memo.ToString(), branchId, model.Description, jEntryTypeID, model.UserID).FirstOrDefault();


                    //payLog.JEntryID = jEntryID; // get it from SP                        
                    //payLog.AddOn = DateTime.UtcNow.AddHours(5);
                    //payLog.AddBy = 1;
                    //payLog.BranchID = branchId;
                    //payLog.JEntryTypeID = jEntryTypeID;
                    //Mapper.CreateMap<Models.DTO.PaymentLog, tbl_PaymentLog>();
                    //var paymentLog = Mapper.Map<Models.DTO.PaymentLog, tbl_PaymentLog>(payLog);
                    //db.tbl_PaymentLog.Add(paymentLog);
                    //db.SaveChanges();
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

        // Vendor Adjustment Entry(s)
        public object SaveVendorAdjustment(Payment model, List<Models.DTO.JEntryLog> jEntryLog, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            model.BranchID = branchId;
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    int? jEntryTypeID = 0;
                    int creditAccountID = 0;    //
                    int debitAccountID = 0;     // 
                    model.UserID = HttpContext.Current.User.Identity.GetUserId();
                    jEntryTypeID = 13; // Vendor Adjustment                        
                    debitAccountID = model.AccountID;
                    creditAccountID = 18; //18; // AdustAccountId from accountdetails table

                    StringBuilder VendorMemo = new StringBuilder();
                    StringBuilder PaymentMemo = new StringBuilder();
                    db.Configuration.ValidateOnSaveEnabled = false;
                    // get all Order ids 
                    List<int> lstOrderIds = model.Details.Select(p => p.OrderID).ToList();
                    // get all PurchaseOrders where OrderID contains lstOrderIds
                    var lstOrders = db.tbl_PurchaseOrder.Where(s => lstOrderIds.Contains(s.OrderID)).ToList();
                    List<tbl_PurchaseOrder> existingOrders = lstOrders.ToList();
                    //
                    if (lstOrderIds.Count > 0)
                    {
                        VendorMemo.Append("Adjustment for Invoice No: ");
                    }
                    foreach (var item in model.Details)
                    {
                        lstOrderIds.Add(item.OrderID);
                        //  if already exists 
                        var row = existingOrders.FirstOrDefault(s => s.OrderID == item.OrderID);
                        if (row != null)
                        {
                            VendorMemo.Append(row.POID + ",");
                            decimal? totalPaid = row.TotalPaid + item.Paying;
                            row.TotalPaid = totalPaid;
                            if (row.Adjustment > 0)
                            { row.Adjustment += item.Paying; }
                            else
                            { row.Adjustment = item.Paying; }
                            if (totalPaid == row.TotalAmount)
                            {
                                row.PaymentStatus = "Paid";
                                row.IsPaid = true;
                            }
                            else if (row.AmountPaid < row.TotalAmount)
                            {
                                row.PaymentStatus = "Partial Paid";
                            }
                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(row).State = EntityState.Modified;
                        }

                    }
                    jEntryID = db.insertGJEntry(model.Amount, debitAccountID, model.VoucherDate, AddBy, creditAccountID, VendorMemo.ToString(), branchId, model.Description, jEntryTypeID, model.UserID).FirstOrDefault();
                    foreach (var item in jEntryLog)
                    {
                        item.JEntryID = jEntryID ?? 0; // get it from SP                                                
                        item.BranchID = branchId;
                    }
                    Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                    var jLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jEntryLog);
                    foreach (var item in jLog)
                    {
                        db.tbl_JEntryLog.Add(item);
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return jEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        public object DeleteVendorAdjustment(int JEntryID)
        {
            dbPOS db = new dbPOS();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    ICollection<tbl_JEntryLog> jEntryLog = db.tbl_JEntryLog.Where(j => j.JEntryID == JEntryID).ToList();
                    Mapper.CreateMap<tbl_JEntryLog, Models.DTO.JEntryLog>();
                    var jLog = Mapper.Map<ICollection<tbl_JEntryLog>, ICollection<Models.DTO.JEntryLog>>(jEntryLog);
                    db.Configuration.ValidateOnSaveEnabled = false;
                    #region Update Purchase Order 
                    foreach (var item in jLog)
                    {
                        var row = db.tbl_PurchaseOrder.FirstOrDefault(so => so.OrderID == item.OrderID && so.BranchID == item.BranchID);
                        if (row != null)
                        {
                            row.TotalPaid -= Convert.ToDecimal(item.Amount);
                            row.Adjustment -= Convert.ToDecimal(item.Amount);
                            if (row.TotalPaid == 0)
                            {
                                row.PaymentStatus = "UnPaid";
                                row.IsPaid = false;
                            }
                            else if (row.TotalPaid < row.TotalAmount)
                            {
                                row.PaymentStatus = "Partial Paid";
                                row.IsPaid = false;
                            }
                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(row).State = EntityState.Modified;
                        }
                    }
                    #endregion
                    #region Reverse Journal Entry 
                    db.DeleteVendorPayment(JEntryID);
                    #endregion
                    db.SaveChanges();
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        public static dynamic SaveCashPayment(Payment model, Models.DTO.JEntryLog jentryLog)
        {
            try
            {
                if (model.Amount <= 0)
                {
                    return ("Please enter valid amount!");
                }
                else if (model.AccountID <= 0)
                {
                    return ("Please select valid employee!");
                }


                using (var db = new dbPOS())
                {
                    int? id = db.insertEmployeeCashPayments(model.Amount, Helper.PST(), 1, model.AccountID, model.Description).FirstOrDefault();

                    if (id != null && id > 0)
                    {
                        UserActionsPerformed UserActions = new UserActionsPerformed();
                        Common com = new Common();
                        UserActions.MapActions(com.GetUserID(), "Performed Employee Cash Payments".ToString());

                        return id;
                    }
                }

            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return e.Message;
            }

            return "Something went wrong!";
        }

        public static dynamic EditCashPayment(Payment model, Models.DTO.JEntryLog jentryLog)
        {
            try
            {
                if (model.Amount <= 0)
                {
                    return ("Please enter valid amount!");
                }
                else if (model.AccountID <= 0)
                {
                    return ("Please select valid employee!");
                }
                else if (model.JEntryID <= 0 || model.JEntryID == null)
                {
                    return ("Please select valid entry!");
                }


                using (var db = new dbPOS())
                {
                    int? id = db.EditEmployeeCashPayments(model.Amount, model.VoucherDate, 1, model.AccountID, model.JEntryID, model.Description).FirstOrDefault();

                    if (id != null && id > 0)
                    {
                        UserActionsPerformed UserActions = new UserActionsPerformed();
                        Common com = new Common();
                        UserActions.MapActions(com.GetUserID(), "Performed Edit Employee Cash Payments".ToString());

                        return id;
                    }
                }

            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return e.Message;
            }

            return "Something went wrong!";
        }

        public static dynamic DeleteCashPayment(Payment model, Models.DTO.JEntryLog jentryLog)
        {
            try
            {
                if (model.JEntryID == null || model.JEntryID <= 0)
                {
                    return ("Please select valid entry!");
                }



                using (var db = new dbPOS())
                {
                    int? id = db.DeleteEmployeeCashPayments(model.AccountID, model.JEntryID).FirstOrDefault();

                    if (id != null && id > 0)
                    {
                        UserActionsPerformed UserActions = new UserActionsPerformed();
                        Common com = new Common();
                        UserActions.MapActions(com.GetUserID(), "Performed Delete Employee Cash Payments".ToString());

                        return id;
                    }
                }

            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return e.Message;
            }

            return "Something went wrong!";
        }

        //Employee Return Pety cash
        public static dynamic SaveSettleEmpolyeePayments(Payment model, Models.DTO.JEntryLog jentryLog)
        {
            try
            {
                if (model.Amount <= 0)
                {
                    return ("Please enter valid amount!");
                }
                else if (model.AccountID <= 0)
                {
                    return ("Please select valid employee!");
                }


                using (var db = new dbPOS())
                {
                    using (var t = db.Database.BeginTransaction())
                    {
                        var JentryIds = db.GetJEntryListEmployeeIDWise(model.AccountID).Select(x => x.JEntryId).ToList();

                        if (JentryIds != null && JentryIds.Count > 0)
                        {

                            //var GetJentryID = JentryIds.Select(x=>x.JEntryId).ToList();
                            decimal totalAmount = model.Amount;
                            int c = 0;


                            var jentryDetails = db.tbl_JEntry.Where(x => JentryIds.Contains(x.JEntryId)).OrderBy(y => y.Amount).ToList();
                            if (jentryDetails != null && jentryDetails.Count > 0)
                            {
                                foreach (var JEntryID in jentryDetails)
                                {
                                    var jentryData = db.tbl_JEntry.Where(x => x.JEntryId == JEntryID.JEntryId).FirstOrDefault();

                                    if (jentryData != null)
                                    {
                                        decimal jentryAmount = jentryData.Amount ?? 0;
                                        jentryData.IsClearedFromEmployee = totalAmount >= jentryData.Amount ? true : false;
                                        jentryData.Amount = totalAmount >= jentryData.Amount ? 0 : jentryData.Amount - totalAmount;
                                        jentryData.UpdateOn = Helper.PST();
                                        jentryData.ReferenceDetail = totalAmount >= jentryData.Amount ? "Cleared From Employee On : " + Helper.PST().ToLongDateString() + " with EmployeeID: " + model.AccountID.ToString() : "Deducted Amount From Employee EntryId: " + jentryData.JEntryId.ToString() + " On : " + Helper.PST().ToLongDateString() + " with EmployeeID: " + model.AccountID.ToString().ToString();
                                        db.Entry(jentryData).State = EntityState.Modified;
                                        c += db.SaveChanges();
                                        totalAmount -= jentryAmount;

                                        if (totalAmount <= 0) // if amount is lesser, than just go out without checking other settlement entries
                                        {
                                            break;
                                        }

                                    }
                                }
                            }

                            var empData = db.tbl_Employee.Where(x => x.AccountID == model.AccountID).FirstOrDefault();

                            if (empData != null)
                            {

                                if (empData.EmployeePettyCashBalance != model.Amount && empData.EmployeePettyCashBalance != 0) { t.Rollback(); return ("Employee balance must be equals to the amount returned!"); }
                                empData.EmployeePettyCashBalance -= model.Amount;
                                db.Entry(empData).State = EntityState.Modified;
                                c += db.SaveChanges();
                            }
                            var payJEntryId = db.insertEmployeeManualCashSettlements(model.Amount, Helper.PST(), 1, model.AccountID, model.Description).FirstOrDefault();

                            if (payJEntryId != null && payJEntryId > 0)
                            {
                                UserActionsPerformed UserActions = new UserActionsPerformed();
                                Common com = new Common();
                                UserActions.MapActions(com.GetUserID(), "Performed Employee Return Settlements".ToString());
                                t.Commit();
                                return payJEntryId;
                            }

                        }
                    }

                }

            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return e.Message;
            }

            return "Something went wrong!";
        }
        public static object SaveEmployeePurchase_Unused(Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, Models.DTO.JEntryLog jentryLog)
        {
            try
            {
                using (var db = new dbPOS())
                {
                    using (var t = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (model != null && detailList != null && detailList.Count > 0 && jentryLog != null && jentryLog.JEntryIDs != null && jentryLog.JEntryIDs.Count > 0)
                            {
                                int c = 0;
                                decimal totalAmountForSettltement = 0;

                                tbl_EmployeePurchases purchase = new tbl_EmployeePurchases();

                                purchase.Date = Helper.PST();
                                purchase.Description = model.Description;
                                purchase.EmployeeAccID = model.AccountID;
                                purchase.JEntryID = model.JEntryID;
                                purchase.StatusID = 2;//issued
                                purchase.TotalAmount = model.Amount;
                                purchase.EPOID = Common.GetInvoiceNoEmployeePurchase();
                                purchase.InvoiceNo = "E-PUR-" + purchase.EPOID.ToString();
                                purchase.IsApproved = true;
                                purchase.ApprovedOn = Helper.PST();
                                totalAmountForSettltement = purchase.TotalAmount;

                                db.tbl_EmployeePurchases.Add(purchase);
                                c += db.SaveChanges();

                                List<tbl_EmployeePurchaseDetails> details = new List<tbl_EmployeePurchaseDetails>();
                                List<tbl_StockLog> logList = new List<tbl_StockLog>();

                                foreach (var i in detailList)
                                {
                                    tbl_EmployeePurchaseDetails detail = new tbl_EmployeePurchaseDetails();
                                    detail.AddedOn = Helper.PST();
                                    detail.Description = i.Description.Trim();
                                    detail.IsProduct = i.IsProduct;
                                    if (i.ProductID != null && i.ProductID > 0)
                                    {
                                        detail.ProductID = i.ProductID;
                                    }
                                    detail.Qty = i.Qty;
                                    detail.Total = i.Total;
                                    detail.UnitPrice = i.UnitPrice;
                                    detail.OrderID = purchase.OrderID;
                                    detail.ExpenseID = i.ExpenseID;
                                    detail.Description = string.IsNullOrWhiteSpace(detail.Description) ? "-" : detail.Description;

                                    if (detail.ExpenseID != null && detail.ExpenseID > 0)
                                    {
                                        totalAmountForSettltement -= detail.Total;
                                        var id = db.insertEmployeeCashSettlementsForExpensesEntry(detail.Total, Helper.PST(), 1, detail.ExpenseID, "Expense Item: " + detail.Description.ToString() + " In Employee OID: " + purchase.OrderID.ToString(), detail.Description.ToString(), 9001, purchase.EmployeeAccID,purchase.OrderID).FirstOrDefault();
                                    }

                                    details.Add(detail);


                                    tbl_StockLog log = new tbl_StockLog();

                                    log.AccountID = model.AccountID;
                                    log.AddBy = 1;
                                    log.AddOn = Helper.PST();
                                    log.BranchID = 9001;
                                    log.CostPrice = i.UnitPrice;
                                    log.Detail = "Product Purchased: " + i.Description.Trim();
                                    log.InReference = "Employee Purchase";
                                    log.InvoiceDate = Helper.PST();
                                    log.IsActive = true;
                                    log.OrderID = purchase.OrderID;
                                    log.OrderTypeID = 15;//Employee Purchase
                                    log.ProductID = (i.ProductID != null && i.ProductID > 0) ? i.ProductID ?? 0 : 1;//General Product
                                    log.ReturnedQty = 0;
                                    log.SalePrice = 0;
                                    log.StockIN = 0;
                                    log.StockOut = 0;

                                    logList.Add(log);

                                    if (i.ProductID != null && i.ProductID > 0)
                                    {
                                        if (db.tbl_Stock.Any(x => x.ProductID == i.ProductID && x.IsActive == true))
                                        {
                                            var stk = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && x.IsActive == true).FirstOrDefault();
                                            if (stk != null)
                                            {
                                                stk.Qty += i.Qty;
                                                stk.CostPrice = stk.CostPrice > 0 ? (stk.CostPrice + i.UnitPrice) / 2 : i.UnitPrice;
                                                stk.UpdateOn = Helper.PST();
                                            }
                                        }
                                        else
                                        {
                                            tbl_Stock stock = new tbl_Stock();

                                            stock.Qty = i.Qty;
                                            stock.CostPrice = i.UnitPrice;
                                            stock.Addon = Helper.PST();
                                            stock.AddBy = 1;
                                            stock.BranchID = 9001;
                                            stock.SalePrice = 0;
                                            stock.ProductID = i.ProductID ?? 0;

                                            db.tbl_Stock.Add(stock);
                                            c += db.SaveChanges();

                                        }

                                    }

                                }
                                if (details.Count > 0 && logList.Count > 0)
                                {
                                    db.tbl_EmployeePurchaseDetails.AddRange(details);
                                    db.tbl_StockLog.AddRange(logList);
                                    c += db.SaveChanges();
                                }

                                var getEmployeePendings = db.GetJEntryListEmployeeIDWise(purchase.EmployeeAccID).ToList();

                                if (getEmployeePendings != null && getEmployeePendings.Count > 0)
                                {
                                    foreach (var e in getEmployeePendings)
                                    {
                                        var entry = db.tbl_JEntry.Where(x => x.JEntryId == e.JEntryId).FirstOrDefault();
                                        if (entry != null)
                                        {
                                            entry.IsActive = false;
                                            entry.IsDeleted = true;
                                            entry.DeleteOn = Helper.PST();
                                            if (entry.tbl_JDetail != null && entry.tbl_JDetail.Count > 0)
                                            {
                                                entry.tbl_JDetail.ToList().ForEach(x => x.IsDeleted = true);
                                            }
                                            db.Entry(entry).State = EntityState.Modified;
                                            c += db.SaveChanges();
                                        }
                                    }
                                }

                                #region Payment Entry
                                //decimal totalAmount = model.Amount;
                                //var payJEntryId = db.insertEmployeeCashSettlements(totalAmountForSettltement, Helper.PST(), 1, model.AccountID, purchase.OrderID, model.Description).FirstOrDefault();

                                //foreach (var JEntryID in jentryLog.JEntryIDs)
                                //{
                                //    var jentryData = db.tbl_JEntry.Where(x => x.JEntryId == JEntryID).FirstOrDefault();

                                //    if (jentryData != null)
                                //    {
                                //        //if (jentryData.Amount < model.Amount)
                                //        //{
                                //        //    var lessJEntryId = db.insertEmployeeCashPaymentsForGreaterAmount(model.Amount - jentryData.Amount, Helper.PST(), 1, model.AccountID, model.Description).FirstOrDefault();
                                //        //}
                                //        //else if (jentryData.Amount > model.Amount)
                                //        //{
                                //        //    var lessJEntryId = db.insertEmployeeCashPaymentsForLesserAmount(jentryData.Amount - model.Amount, Helper.PST(), 1, model.AccountID, model.Description).FirstOrDefault();
                                //        //}

                                //        //var payJEntryId = db.insertEmployeeCashSettlements(model.Amount, Helper.PST(), 1, model.AccountID, purchase.OrderID, model.Description).FirstOrDefault();

                                //        jentryData.IsClearedFromEmployee = totalAmount > jentryData.Amount? true:false;
                                //        jentryData.Amount = totalAmount < jentryData.Amount? jentryData.Amount - totalAmount : jentryData.Amount;
                                //        jentryData.UpdateOn = Helper.PST();
                                //        jentryData.ReferenceDetail = totalAmount > jentryData.Amount ? "Cleared From Employee On : " + Helper.PST().ToLongDateString() + " with EmployeeOrderID: " + purchase.OrderID.ToString(): "Deducted Amount From Employee EntryId: "+jentryData.JEntryId.ToString()+" On : " + Helper.PST().ToLongDateString() + " with EmployeeOrderID: " + purchase.OrderID.ToString();
                                //        db.Entry(jentryData).State = EntityState.Modified;
                                //        c += db.SaveChanges();
                                //        totalAmount -= jentryData.Amount??0;
                                //    }
                                //}
                                #endregion

                                if (c > 0)
                                {
                                    t.Commit();
                                    UserActionsPerformed UserActions = new UserActionsPerformed();
                                    Common com = new Common();
                                    UserActions.MapActions(com.GetUserID(), "Performed Employee Purchase Notes".ToString());
                                    return purchase.OrderID;
                                }
                                //}
                            }
                        }
                        catch (Exception e)
                        {
                            t.Rollback();
                            while (e.InnerException != null) { e = e.InnerException; }
                            return e.Message;
                        }

                    }

                }

                return "Something Went Wrong";
            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return e.Message;
            }

        }
        public static object SaveEmployeePurchase(Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, Models.DTO.JEntryLog jentryLog)
        {
            try
            {
                using (var db = new dbPOS())
                {
                    using (var t = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (model != null && detailList != null && model.AccountID > 0 && detailList.Count > 0 && jentryLog != null /*&& jentryLog.JEntryIDs != null && jentryLog.JEntryIDs.Count > 0*/)
                            {
                                int c = 0;

                                tbl_EmployeePurchases purchase = new tbl_EmployeePurchases();

                                purchase.Date = Helper.PST();
                                purchase.Description = model.Description;
                                purchase.EmployeeAccID = model.AccountID;
                                purchase.JEntryID = model.JEntryID;
                                purchase.StatusID = 2;//issued
                                purchase.TotalAmount = model.Amount;
                                purchase.EPOID = Common.GetInvoiceNoEmployeePurchase();
                                purchase.InvoiceNo = "E-PUR-" + purchase.EPOID.ToString();
                                purchase.IsApproved = true;
                                purchase.ApprovedOn = Helper.PST();
                                decimal totalAmountForSettlement = purchase.TotalAmount;

                                db.tbl_EmployeePurchases.Add(purchase);
                                c += db.SaveChanges();
                                StringBuilder itemDescription = new StringBuilder();
                                itemDescription.Append(model.Description + ": ");

                                db.UpdateEmployeeCashBalance(purchase.EmployeeAccID);
                                c += db.SaveChanges();
                                //var empData = db.tbl_Employee.Where(x => x.AccountID == model.AccountID).FirstOrDefault();
                                //if (empData != null)
                                //{
                                //    //empData.EmployeePettyCashBalance -= model.Amount;
                                //    empData.EmployeePettyCashBalance = 0;
                                //    db.Entry(empData).State = EntityState.Modified;
                                //    c += db.SaveChanges();
                                //}


                                List<tbl_EmployeePurchaseDetails> details = new List<tbl_EmployeePurchaseDetails>();
                                List<tbl_StockLog> logList = new List<tbl_StockLog>();

                                foreach (var i in detailList)
                                {

                                    tbl_EmployeePurchaseDetails detail = new tbl_EmployeePurchaseDetails();
                                    detail.AddedOn = Helper.PST();
                                    detail.Description = i.Description.Trim();
                                    detail.IsProduct = i.IsProduct;
                                    if (i.ProductID != null && i.ProductID > 0)
                                    {
                                        detail.ProductID = i.ProductID;
                                        itemDescription.Append(db.tbl_Product.Where(x => x.ProductID == i.ProductID).Select(x => x.PartNo).FirstOrDefault());
                                        itemDescription.Append(" | ");
                                    }
                                    detail.Qty = i.Qty;
                                    detail.Total = i.Total;
                                    detail.UnitPrice = i.UnitPrice;
                                    detail.OrderID = purchase.OrderID;
                                    detail.ExpenseID = i.ExpenseID;
                                    detail.Description = string.IsNullOrWhiteSpace(detail.Description) ? "-" : detail.Description;

                                    if (detail.ExpenseID != null && detail.ExpenseID > 0)
                                    {
                                        totalAmountForSettlement -= detail.Total;
                                        var id = db.insertEmployeeCashSettlementsForExpensesEntry(detail.Total, Helper.PST(), 1, detail.ExpenseID, "Expense Item: " + detail.Description.ToString() + " With Employee OID: " + purchase.OrderID.ToString(), detail.Description.ToString(), 9001, 1, purchase.OrderID).FirstOrDefault();

                                    }

                                    details.Add(detail);


                                    tbl_StockLog log = new tbl_StockLog();

                                    log.AccountID = model.AccountID;
                                    log.AddBy = 1;
                                    log.AddOn = Helper.PST();
                                    log.BranchID = 9001;
                                    log.CostPrice = i.UnitPrice;
                                    log.Detail = "Product Purchased: " + i.Description.Trim();
                                    log.InReference = "Employee Purchase";
                                    log.InvoiceDate = Helper.PST();
                                    log.IsActive = true;
                                    log.OrderID = purchase.OrderID;
                                    log.OrderTypeID = 15;//Employee Purchase
                                    log.ProductID = (i.ProductID != null && i.ProductID > 0) ? i.ProductID ?? 0 : 1;//General Product
                                    log.ReturnedQty = 0;
                                    log.SalePrice = 0;
                                    log.StockIN = 0;
                                    log.StockOut = 0;

                                    logList.Add(log);

                                    if (i.ProductID != null && i.ProductID > 0)
                                    {
                                        if (db.tbl_Stock.Any(x => x.ProductID == i.ProductID && x.IsActive == true))
                                        {
                                            var stk = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && x.IsActive == true).FirstOrDefault();
                                            if (stk != null)
                                            {
                                                stk.Qty += i.Qty;
                                                stk.CostPrice = stk.CostPrice > 0 ? (stk.CostPrice + i.UnitPrice) / 2 : i.UnitPrice;
                                                stk.UpdateOn = Helper.PST();
                                            }
                                        }
                                        else
                                        {
                                            tbl_Stock stock = new tbl_Stock();

                                            stock.Qty = i.Qty;
                                            stock.CostPrice = i.UnitPrice;
                                            stock.Addon = Helper.PST();
                                            stock.AddBy = 1;
                                            stock.BranchID = 9001;
                                            stock.SalePrice = 0;
                                            stock.ProductID = i.ProductID ?? 0;

                                            db.tbl_Stock.Add(stock);
                                            c += db.SaveChanges();

                                        }

                                    }

                                }
                                if (details.Count > 0 && logList.Count > 0)
                                {
                                    db.tbl_EmployeePurchaseDetails.AddRange(details);
                                    db.tbl_StockLog.AddRange(logList);
                                    c += db.SaveChanges();

                                    if (totalAmountForSettlement > 0) // if store items purchased
                                        db.insertEmployeeCashSettlements(totalAmountForSettlement, Helper.PST(), 1, model.AccountID, purchase.OrderID, itemDescription.ToString()).FirstOrDefault();

                                }

                                var getEmployeePendings = db.GetJEntryListEmployeeIDWise(purchase.EmployeeAccID).ToList();

                                if (getEmployeePendings != null && getEmployeePendings.Count > 0)
                                {
                                    foreach (var e in getEmployeePendings)
                                    {
                                        var entry = db.tbl_JEntry.Where(x => x.JEntryId == e.JEntryId).FirstOrDefault();
                                        if (entry != null)
                                        {
                                            entry.IsActive = false;
                                            entry.IsDeleted = true;
                                            entry.DeleteOn = Helper.PST();
                                            if (entry.tbl_JDetail != null && entry.tbl_JDetail.Count > 0)
                                            {
                                                entry.tbl_JDetail.ToList().ForEach(x => x.IsDeleted = true);
                                            }
                                            db.Entry(entry).State = EntityState.Modified;
                                            c += db.SaveChanges();
                                        }
                                    }
                                }


                                if (c > 0)
                                {
                                    t.Commit();
                                    UserActionsPerformed UserActions = new UserActionsPerformed();
                                    Common com = new Common();
                                    UserActions.MapActions(com.GetUserID(), "Performed Employee Purchase Notes".ToString());
                                    return purchase.OrderID;
                                }
                                //}
                            }
                        }
                        catch (Exception e)
                        {
                            t.Rollback();
                            while (e.InnerException != null) { e = e.InnerException; }
                            return e.Message;
                        }

                    }

                }

                return "Something Went Wrong";
            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return e.Message;
            }

        }


        public static object CreateEmployeePurchase(Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, List<Models.DTO.JEntryLog> jentryLog)
        {
            try
            {
                using (var db = new dbPOS())
                {
                    using (var t = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (model != null && detailList != null && detailList.Count > 0)
                            {
                                int c = 0;

                                var JentryIds = db.GetJEntryListEmployeeIDWise(model.AccountID).Select(x => new { x.JEntryId }).ToList();

                                if (JentryIds != null && JentryIds.Count > 0)
                                {

                                    tbl_EmployeePurchases purchase = new tbl_EmployeePurchases();

                                    purchase.Date = Helper.PST();
                                    purchase.Description = model.Description;
                                    purchase.EmployeeAccID = model.AccountID;
                                    //purchase.JEntryID = model.JEntryID;
                                    purchase.StatusID = 1;//pending
                                    purchase.TotalAmount = model.Amount;
                                    purchase.EPOID = Common.GetInvoiceNoEmployeePurchase();
                                    purchase.InvoiceNo = "E-PUR-" + purchase.EPOID.ToString();
                                    purchase.IsApproved = false;

                                    db.tbl_EmployeePurchases.Add(purchase);
                                    c += db.SaveChanges();

                                    List<tbl_EmployeePurchaseDetails> details = new List<tbl_EmployeePurchaseDetails>();
                                    //List<tbl_StockLog> logList = new List<tbl_StockLog>();

                                    foreach (var i in detailList)
                                    {
                                        tbl_EmployeePurchaseDetails detail = new tbl_EmployeePurchaseDetails();
                                        detail.AddedOn = Helper.PST();
                                        detail.Description = i.Description.Trim();
                                        detail.IsProduct = i.IsProduct;
                                        if (i.ProductID != null && i.ProductID > 0)
                                        {
                                            detail.ProductID = i.ProductID;
                                        }
                                        detail.Qty = i.Qty;
                                        detail.Total = i.Total;
                                        detail.UnitPrice = i.UnitPrice;
                                        detail.OrderID = purchase.OrderID;
                                        detail.ExpenseID = i.ExpenseID;

                                        //if (detail.ExpenseID != null && detail.ExpenseID > 0)
                                        //{
                                        //    totalAmountForSettltement -= detail.Total;
                                        //    var id = db.insertEmployeeCashSettlementsForExpensesEntry(detail.Total, Helper.PST(), 1, detail.ExpenseID, "Expense Item: " + detail.Description.ToString() + " In Employee OID: " + purchase.OrderID.ToString(), 9001, purchase.EmployeeAccID).FirstOrDefault();
                                        //}

                                        details.Add(detail);


                                        //tbl_StockLog log = new tbl_StockLog();

                                        //log.AccountID = model.AccountID;
                                        //log.AddBy = 1;
                                        //log.AddOn = Helper.PST();
                                        //log.BranchID = 9001;
                                        //log.CostPrice = i.UnitPrice;
                                        //log.Detail = "Product Purchased: " + i.Description.Trim();
                                        //log.InReference = "Employee Purchase";
                                        //log.InvoiceDate = Helper.PST();
                                        //log.IsActive = true;
                                        //log.OrderID = purchase.OrderID;
                                        //log.OrderTypeID = 15;//Employee Purchase
                                        //log.ProductID = (i.ProductID != null && i.ProductID > 0) ? i.ProductID ?? 0 : 1;//General Product
                                        //log.ReturnedQty = 0;
                                        //log.SalePrice = 0;
                                        //log.StockIN = 0;
                                        //log.StockOut = 0;

                                        //logList.Add(log);

                                        //if (i.ProductID != null && i.ProductID > 0)
                                        //{
                                        //    if (db.tbl_Stock.Any(x => x.ProductID == i.ProductID && x.IsActive == true))
                                        //    {
                                        //        var stk = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && x.IsActive == true).FirstOrDefault();
                                        //        if (stk != null)
                                        //        {
                                        //            stk.Qty += i.Qty;
                                        //            stk.CostPrice = stk.CostPrice > 0 ? (stk.CostPrice + i.UnitPrice) / 2 : i.UnitPrice;
                                        //            stk.UpdateOn = Helper.PST();
                                        //        }
                                        //    }
                                        //    else
                                        //    {
                                        //        tbl_Stock stock = new tbl_Stock();

                                        //        stock.Qty = i.Qty;
                                        //        stock.CostPrice = i.UnitPrice;
                                        //        stock.Addon = Helper.PST();
                                        //        stock.AddBy = 1;
                                        //        stock.BranchID = 9001;
                                        //        stock.SalePrice = 0;
                                        //        stock.ProductID = i.ProductID ?? 0;

                                        //        db.tbl_Stock.Add(stock);
                                        //        c += db.SaveChanges();

                                        //    }

                                        //}

                                    }
                                    if (details.Count > 0)
                                    {
                                        db.tbl_EmployeePurchaseDetails.AddRange(details);
                                        //db.tbl_StockLog.AddRange(logList);
                                        c += db.SaveChanges();

                                    }


                                    //if (jentryData.Amount < model.Amount)
                                    //{
                                    //    var lessJEntryId = db.insertEmployeeCashPaymentsForGreaterAmount(model.Amount - jentryData.Amount, Helper.PST(), 1, model.AccountID, model.Description).FirstOrDefault();
                                    //}
                                    //else if (jentryData.Amount > model.Amount)
                                    //{
                                    //    var lessJEntryId = db.insertEmployeeCashPaymentsForLesserAmount(jentryData.Amount - model.Amount, Helper.PST(), 1, model.AccountID, model.Description).FirstOrDefault();
                                    //}

                                    //var payJEntryId = db.insertEmployeeCashSettlements(totalAmountForSettltement, Helper.PST(), 1, model.AccountID, purchase.OrderID, model.Description).FirstOrDefault();


                                    //decimal totalAmount = model.Amount;
                                    //foreach (var i in JentryIds)
                                    //{
                                    //    var jentryData = db.tbl_JEntry.Where(x => x.JEntryId == i.JEntryId).FirstOrDefault();
                                    //    if (jentryData != null)
                                    //    {
                                    //        decimal jentryAmount = jentryData.Amount??0;
                                    //        jentryData.IsClearedFromEmployee = totalAmount >= jentryData.Amount ? true : false;
                                    //        jentryData.Amount = totalAmount < jentryData.Amount ? jentryData.Amount - totalAmount : jentryData.Amount;
                                    //        jentryData.UpdateOn = Helper.PST();
                                    //        jentryData.ReferenceDetail += totalAmount >= jentryData.Amount ? "To Be Cleared From Employee On : " + Helper.PST().ToLongDateString() + " with Employee Purchase Request OrderID: " + purchase.OrderID.ToString() : "Deducted Amount From Employee EntryId: " + jentryData.JEntryId.ToString() + " On : " + Helper.PST().ToLongDateString() + " with EmployeeID: " + model.AccountID.ToString().ToString()+" and Employee Purchase OrderID: " + purchase.OrderID.ToString();
                                    //        db.Entry(jentryData).State = EntityState.Modified;
                                    //        c += db.SaveChanges();
                                    //        totalAmount -= jentryAmount;

                                    //    }
                                    //}

                                    if (c > 0)
                                    {
                                        t.Commit();
                                        UserActionsPerformed UserActions = new UserActionsPerformed();
                                        Common com = new Common();
                                        UserActions.MapActions(com.GetUserID(), "Performed Employee Purchase Request Note. OrderID: " + purchase.OrderID.ToString());
                                        return purchase.OrderID;
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            t.Rollback();
                            while (e.InnerException != null) { e = e.InnerException; }
                            return e.Message;
                        }

                    }

                }

                return "Something Went Wrong";
            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return e.Message;
            }

        }

        #region not used for now 20/12/23
        //public static object ApproveEmployeePurchase(int EmployeePurchaseID, Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, Models.DTO.JEntryLog jentryLog)
        //{
        //    try
        //    {
        //        using (var db = new dbPOS())
        //        {
        //            using (var t = db.Database.BeginTransaction())
        //            {
        //                try
        //                {
        //                    if (model != null && detailList != null && detailList.Count > 0 && EmployeePurchaseID > 0 && jentryLog != null && jentryLog.JEntryIDs != null && jentryLog.JEntryIDs.Count > 0)
        //                    {
        //                        int c = 0;
        //                        decimal totalAmountForSettltement = 0;
        //                        decimal totalExpAmountForSettltement = 0;
        //                        //var jentryData = db.tbl_JEntry.Where(x => x.JEntryId == model.JEntryID).FirstOrDefault();
        //                        var purchase = db.tbl_EmployeePurchases.Where(x => x.OrderID == EmployeePurchaseID).FirstOrDefault();

        //                        if (/*jentryData != null &&*/ purchase != null)
        //                        {
        //                            StringBuilder desc = new StringBuilder();
        //                            desc.Append(model.Description + " | JentryIds: ");
        //                            totalAmountForSettltement = model.Amount;
        //                            //purchase.Date = Helper.PST();
        //                            Parallel.ForEach(jentryLog.JEntryIDs,id=>
        //                            {
        //                                desc.Append(id.ToString() + ",");
        //                            });
        //                            purchase.Description += " -|- "+desc.ToString();
        //                            purchase.EmployeeAccID = model.AccountID;
        //                            //purchase.JEntryID = model.JEntryID;
        //                            purchase.StatusID = 2;//issued
        //                            purchase.TotalAmount = model.Amount;

        //                            purchase.IsApproved = true;
        //                            purchase.ApprovedOn = Helper.PST();

        //                            //db.tbl_EmployeePurchases.Add(purchase);
        //                            //c += db.SaveChanges();

        //                            List<tbl_EmployeePurchaseDetails> details = new List<tbl_EmployeePurchaseDetails>();
        //                            List<tbl_StockLog> logList = new List<tbl_StockLog>();

        //                            if (purchase.tbl_EmployeePurchaseDetails != null && purchase.tbl_EmployeePurchaseDetails.Count > 0)
        //                            {
        //                                db.tbl_EmployeePurchaseDetails.RemoveRange(purchase.tbl_EmployeePurchaseDetails);
        //                                c += db.SaveChanges();

        //                                var empData = db.tbl_Employee.Where(x => x.AccountID == model.AccountID).FirstOrDefault();
        //                                if (empData != null)
        //                                {
        //                                    empData.EmployeePettyCashBalance -= model.Amount;
        //                                    db.Entry(empData).State = EntityState.Modified;
        //                                    c += db.SaveChanges();
        //                                }
        //                                //purchase.tbl_EmployeePurchaseDetails = new List<tbl_EmployeePurchaseDetails>();

        //                            }

        //                            foreach (var i in detailList)
        //                            {

        //                                tbl_EmployeePurchaseDetails detail = new tbl_EmployeePurchaseDetails();
        //                                detail.AddedOn = Helper.PST();
        //                                detail.Description = i.Description.Trim();
        //                                detail.IsProduct = i.IsProduct;
        //                                if (i.ProductID != null && i.ProductID > 0)
        //                                {
        //                                    detail.ProductID = i.ProductID;
        //                                }
        //                                detail.Qty = i.Qty;
        //                                detail.Total = i.Total;
        //                                detail.UnitPrice = i.UnitPrice;
        //                                detail.OrderID = purchase.OrderID;
        //                                detail.ExpenseID = i.ExpenseID;

        //                                if (detail.ExpenseID != null && detail.ExpenseID > 0)
        //                                {
        //                                    totalExpAmountForSettltement += detail.Total; 
        //                                    totalAmountForSettltement -= detail.Total;
        //                                    var id = db.insertEmployeeCashSettlementsForExpensesEntry(detail.Total, Helper.PST(), 1, detail.ExpenseID, "Expense Item: " + detail.Description.ToString() + " In Employee OID: " + purchase.OrderID.ToString(), 9001, 1).FirstOrDefault();
        //                                }

        //                                details.Add(detail);


        //                                tbl_StockLog log = new tbl_StockLog();

        //                                log.AccountID = model.AccountID;
        //                                log.AddBy = 1;
        //                                log.AddOn = Helper.PST();
        //                                log.BranchID = 9001;
        //                                log.CostPrice = i.UnitPrice;
        //                                log.Detail = "Product Purchased: " + i.Description.Trim();
        //                                log.InReference = "Employee Purchase";
        //                                log.InvoiceDate = Helper.PST();
        //                                log.IsActive = true;
        //                                log.OrderID = purchase.OrderID;
        //                                log.OrderTypeID = 15;//Employee Purchase
        //                                log.ProductID = (i.ProductID != null && i.ProductID > 0) ? i.ProductID ?? 0 : 1;//General Product
        //                                log.ReturnedQty = 0;
        //                                log.SalePrice = 0;
        //                                log.StockIN = i.Qty;
        //                                log.StockOut = 0;

        //                                logList.Add(log);

        //                                if (i.ProductID != null && i.ProductID > 0)
        //                                {
        //                                    if (db.tbl_Stock.Any(x => x.ProductID == i.ProductID && x.IsActive == true))
        //                                    {
        //                                        var stk = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && x.IsActive == true).FirstOrDefault();
        //                                        if (stk != null)
        //                                        {
        //                                            stk.Qty += i.Qty;
        //                                            stk.CostPrice = stk.CostPrice > 0 ? (stk.CostPrice + i.UnitPrice) / 2 : i.UnitPrice;
        //                                            stk.UpdateOn = Helper.PST();
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        tbl_Stock stock = new tbl_Stock();

        //                                        stock.Qty = i.Qty;
        //                                        stock.CostPrice = i.UnitPrice;
        //                                        stock.Addon = Helper.PST();
        //                                        stock.AddBy = 1;
        //                                        stock.BranchID = 9001;
        //                                        stock.SalePrice = 0;
        //                                        stock.ProductID = i.ProductID ?? 0;

        //                                        db.tbl_Stock.Add(stock);
        //                                        c += db.SaveChanges();

        //                                    }

        //                                }

        //                            }
        //                            if (details.Count > 0)
        //                            {
        //                                db.tbl_EmployeePurchaseDetails.AddRange(details);
        //                                //purchase.tbl_EmployeePurchaseDetails = details;
        //                                db.Entry(purchase).State = EntityState.Modified;
        //                                db.tbl_StockLog.AddRange(logList);
        //                                c += db.SaveChanges();
        //                            }


        //                            decimal totalAmount = model.Amount;

        //                            var jentryDetails = db.tbl_JEntry.Where(x => jentryLog.JEntryIDs.Contains(x.JEntryId)).OrderBy(y => y.Amount).ToList();
        //                            if(jentryDetails!=null && jentryDetails.Count > 0)
        //                            {
        //                                foreach(var JEntryID in jentryDetails)
        //                                {
        //                                    var jentryData = db.tbl_JEntry.Where(x => x.JEntryId == JEntryID.JEntryId).FirstOrDefault();

        //                                    if (jentryData != null)
        //                                    {
        //                                        //if (jentryData.Amount < model.Amount)
        //                                        //{
        //                                        //    var lessJEntryId = db.insertEmployeeCashPaymentsForGreaterAmount(model.Amount - jentryData.Amount, Helper.PST(), 1, model.AccountID, model.Description).FirstOrDefault();
        //                                        //}
        //                                        //else if (jentryData.Amount > model.Amount)
        //                                        //{
        //                                        //    var lessJEntryId = db.insertEmployeeCashPaymentsForLesserAmount(jentryData.Amount - model.Amount, Helper.PST(), 1, model.AccountID, model.Description).FirstOrDefault();
        //                                        //}

        //                                        //var payJEntryId = db.insertEmployeeCashSettlements(model.Amount, Helper.PST(), 1, model.AccountID, purchase.OrderID, model.Description).FirstOrDefault();
        //                                        decimal jentryAmount = jentryData.Amount ?? 0;
        //                                        //if (totalAmount >= jentryData.Amount) { totalAmount -= jentryData.Amount ?? 0; }
        //                                        jentryData.IsClearedFromEmployee = totalAmount >= jentryData.Amount ? true : false;
        //                                        jentryData.Amount = totalAmount >= jentryData.Amount ? 0 : jentryData.Amount - totalAmount;
        //                                        jentryData.UpdateOn = Helper.PST();
        //                                        jentryData.SaleOrderID = purchase.OrderID;
        //                                        jentryData.ReferenceDetail += totalAmount >= jentryAmount ? "-|- Cleared From Employee On : " + Helper.PST().ToLongDateString() + " with EmployeeOrderID: " + purchase.OrderID.ToString() : "-|-Deducted Amount From Employee EntryId: " + jentryData.JEntryId.ToString() + " On : " + Helper.PST().ToLongDateString() + " with EmployeeOrderID: " + purchase.OrderID.ToString();
        //                                        if(jentryData.tbl_JDetail!=null && jentryData.tbl_JDetail.Count > 0)
        //                                        {
        //                                            foreach(var i in jentryData.tbl_JDetail)
        //                                            {
        //                                                if (i?.Cr > 0)
        //                                                {
        //                                                    i.Cr = purchase.TotalAmount - totalExpAmountForSettltement;
        //                                                }
        //                                                else if (i?.Dr > 0)
        //                                                {
        //                                                    i.Dr = purchase.TotalAmount - totalExpAmountForSettltement;
        //                                                }
        //                                            }
        //                                        }

        //                                        db.Entry(jentryData).State = EntityState.Modified;
        //                                        c += db.SaveChanges();
        //                                        //totalAmount = jentryAmount > totalAmount ? totalAmount:totalAmount - jentryAmount;
        //                                        totalAmount -= jentryAmount;
        //                                        if (totalAmount <= 0) // if amount is lesser, than just go out without checking other settlement entries
        //                                        {
        //                                            break;
        //                                        }
        //                                        //if (totalAmount >= jentryAmount)
        //                                        //{
        //                                        //    totalAmount -= jentryAmount;
        //                                        //}
        //                                        //else
        //                                        //{
        //                                        //    totalAmount = jentryAmount - totalAmount;
        //                                        //}
        //                                    }
        //                                }
        //                            }
        //                            //foreach (var JEntryID in jentryLog.JEntryIDs)
        //                            //{

        //                            //}

        //                            //jentryData.IsClearedFromEmployee = true;
        //                            //jentryData.UpdateOn = Helper.PST();
        //                            //jentryData.ReferenceDetail = "Cleared From Employee On : " + Helper.PST().ToLongDateString() + " with Employee Purchase Request OrderID: " + purchase.OrderID.ToString();
        //                            //db.Entry(jentryData).State = EntityState.Modified;
        //                            //c += db.SaveChanges();




        //                            if (c > 0)
        //                            {
        //                                t.Commit();
        //                                UserActionsPerformed UserActions = new UserActionsPerformed();
        //                                Common com = new Common();
        //                                UserActions.MapActions(com.GetUserID(), "Performed Employee Purchase Note. OrderID: " + purchase.OrderID.ToString());
        //                                return purchase.OrderID;
        //                            }
        //                        }
        //                    }
        //                }
        //                catch (Exception e)
        //                {
        //                    t.Rollback();
        //                    while (e.InnerException != null) { e = e.InnerException; }
        //                    return e.Message;
        //                }

        //            }

        //        }

        //        return "Something Went Wrong";
        //    }
        //    catch (Exception e)
        //    {
        //        while (e.InnerException != null) { e = e.InnerException; }
        //        return e.Message;
        //    }

        //}

        #endregion

        public static object ApproveEmployeePurchase(int EmployeePurchaseID, Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, Models.DTO.JEntryLog jentryLog)
        {
            try
            {
                using (var db = new dbPOS())
                {
                    using (var t = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (model != null && detailList != null && detailList.Count > 0 && EmployeePurchaseID > 0 && jentryLog != null /*&& jentryLog.JEntryIDs != null && jentryLog.JEntryIDs.Count > 0*/)
                            {
                                int c = 0;
                                decimal totalAmountForSettlement = 0;
                                decimal totalExpAmountForSettlement = 0;
                                //var jentryData = db.tbl_JEntry.Where(x => x.JEntryId == model.JEntryID).FirstOrDefault();
                                var purchase = db.tbl_EmployeePurchases.Where(x => x.OrderID == EmployeePurchaseID).FirstOrDefault();

                                if (/*jentryData != null &&*/ purchase != null)
                                {
                                    model.VoucherDate = purchase.Date;
                                    //StringBuilder desc = new StringBuilder();
                                    //desc.Append(model.Description + " | JentryIds: ");
                                    totalAmountForSettlement = model.Amount;
                                    //purchase.Date = Helper.PST();
                                    //Parallel.ForEach(jentryLog.JEntryIDs, id =>
                                    //{
                                    //    desc.Append(id.ToString() + ",");
                                    //});
                                    //purchase.Description += " -|- " + desc.ToString();
                                    purchase.EmployeeAccID = model.AccountID;
                                    //purchase.JEntryID = model.JEntryID;
                                    purchase.StatusID = 2;//issued
                                    purchase.TotalAmount = model.Amount;

                                    purchase.IsApproved = true;
                                    purchase.ApprovedOn = Helper.PST();

                                    StringBuilder itemDescription = new StringBuilder();
                                    itemDescription.Append(model.Description + ": ");

                                    //db.tbl_EmployeePurchases.Add(purchase);
                                    //c += db.SaveChanges();

                                    List<tbl_EmployeePurchaseDetails> details = new List<tbl_EmployeePurchaseDetails>();
                                    List<tbl_StockLog> logList = new List<tbl_StockLog>();

                                    if (purchase.tbl_EmployeePurchaseDetails != null && purchase.tbl_EmployeePurchaseDetails.Count > 0)
                                    {
                                        db.tbl_EmployeePurchaseDetails.RemoveRange(purchase.tbl_EmployeePurchaseDetails);
                                        c += db.SaveChanges();

                                        db.UpdateEmployeeCashBalance(purchase.EmployeeAccID);
                                        c += db.SaveChanges();
                                        //var EmpId = db.tbl_Employee.Where(x => x.AccountID == model.AccountID).Select(x=>x.EmployeeID).FirstOrDefault();
                                        //tbl_Employee empData = db.tbl_Employee.Where(x => x.EmployeeID == EmpId).FirstOrDefault();
                                        //if (empData != null)
                                        //{
                                        //    //empData.EmployeePettyCashBalance -= model.Amount;
                                        //    empData.EmployeePettyCashBalance = 0;
                                        //    db.Entry(empData).State = EntityState.Modified;
                                        //    c += db.SaveChanges();
                                        //}
                                        //purchase.tbl_EmployeePurchaseDetails = new List<tbl_EmployeePurchaseDetails>();

                                    }

                                    foreach (var i in detailList)
                                    {

                                        tbl_EmployeePurchaseDetails detail = new tbl_EmployeePurchaseDetails();
                                        detail.AddedOn = Helper.PST();
                                        detail.Description = i.Description.Trim();
                                        detail.IsProduct = i.IsProduct;
                                        if (i.ProductID != null && i.ProductID > 0)
                                        {
                                            detail.ProductID = i.ProductID;
                                            itemDescription.Append(db.tbl_Product.Where(x => x.ProductID == i.ProductID).Select(x => x.PartNo).FirstOrDefault());
                                            itemDescription.Append(" | ");
                                        }
                                        detail.Qty = i.Qty;
                                        detail.Total = i.Total;
                                        detail.UnitPrice = i.UnitPrice;
                                        detail.OrderID = purchase.OrderID;
                                        detail.ExpenseID = i.ExpenseID;

                                        if (detail.ExpenseID != null && detail.ExpenseID > 0)
                                        {
                                            totalExpAmountForSettlement += detail.Total;
                                            totalAmountForSettlement -= detail.Total;
                                            detail.Description = string.IsNullOrWhiteSpace(detail.Description) ? "-" : detail.Description;
                                            var id = db.insertEmployeeCashSettlementsForExpensesEntry(detail.Total, model.VoucherDate, 1, detail.ExpenseID, "Expense Item: " + detail.Description.ToString() + " With Employee OID: " + purchase.OrderID.ToString(), detail.Description.ToString(), 9001, 1, purchase.OrderID).FirstOrDefault();
                                        }

                                        details.Add(detail);


                                        tbl_StockLog log = new tbl_StockLog();

                                        log.AccountID = model.AccountID;
                                        log.AddBy = 1;
                                        log.AddOn = Helper.PST();
                                        log.BranchID = 9001;
                                        log.CostPrice = i.UnitPrice;
                                        log.Detail = "Product Purchased: " + i.Description.Trim();
                                        log.InReference = "Employee Purchase";
                                        log.InvoiceDate = model.VoucherDate;
                                        log.IsActive = true;
                                        log.OrderID = purchase.OrderID;
                                        log.OrderTypeID = 15;//Employee Purchase
                                        log.ProductID = (i.ProductID != null && i.ProductID > 0) ? i.ProductID ?? 0 : 1;//General Product
                                        log.ReturnedQty = 0;
                                        log.SalePrice = 0;
                                        log.StockIN = i.Qty;
                                        log.StockOut = 0;

                                        logList.Add(log);

                                        if (i.ProductID != null && i.ProductID > 0)
                                        {
                                            if (db.tbl_Stock.Any(x => x.ProductID == i.ProductID && x.IsActive == true))
                                            {
                                                var stk = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && x.IsActive == true).FirstOrDefault();
                                                if (stk != null)
                                                {
                                                    stk.Qty += i.Qty;
                                                    stk.CostPrice = stk.CostPrice > 0 ? (stk.CostPrice + i.UnitPrice) / 2 : i.UnitPrice;
                                                    stk.UpdateOn = Helper.PST();
                                                }
                                            }
                                            else
                                            {
                                                tbl_Stock stock = new tbl_Stock();

                                                stock.Qty = i.Qty;
                                                stock.CostPrice = i.UnitPrice;
                                                stock.Addon = Helper.PST();
                                                stock.AddBy = 1;
                                                stock.BranchID = 9001;
                                                stock.SalePrice = 0;
                                                stock.ProductID = i.ProductID ?? 0;

                                                db.tbl_Stock.Add(stock);
                                                c += db.SaveChanges();

                                            }

                                        }

                                    }

                                    var getEmployeePendings = db.GetJEntryListEmployeeIDWise(purchase.EmployeeAccID).ToList();

                                    if (getEmployeePendings != null && getEmployeePendings.Count > 0)
                                    {
                                        foreach (var e in getEmployeePendings)
                                        {
                                            var entry = db.tbl_JEntry.Where(x => x.JEntryId == e.JEntryId).FirstOrDefault();
                                            if (entry != null)
                                            {
                                                entry.IsActive = false;
                                                entry.IsDeleted = true;
                                                entry.DeleteOn = Helper.PST();
                                                if (entry.tbl_JDetail != null && entry.tbl_JDetail.Count > 0)
                                                {
                                                    entry.tbl_JDetail.ToList().ForEach(x => x.IsDeleted = true);
                                                }
                                                db.Entry(entry).State = EntityState.Modified;
                                                c += db.SaveChanges();
                                            }
                                        }
                                    }

                                    if (details.Count > 0)
                                    {
                                        db.tbl_EmployeePurchaseDetails.AddRange(details);
                                        db.Entry(purchase).State = EntityState.Modified;
                                        db.tbl_StockLog.AddRange(logList);
                                        c += db.SaveChanges();

                                        if (totalAmountForSettlement > 0)
                                            db.insertEmployeeCashSettlements(totalAmountForSettlement, model.VoucherDate, 1, model.AccountID, purchase.OrderID, itemDescription.ToString()).FirstOrDefault();
                                    }

                                    #region not used for now 20/12/23
                                    //decimal totalAmount = model.Amount;

                                    //var jentryDetails = db.tbl_JEntry.Where(x => jentryLog.JEntryIDs.Contains(x.JEntryId)).OrderBy(y => y.Amount).ToList();
                                    //if (jentryDetails != null && jentryDetails.Count > 0)
                                    //{
                                    //    foreach (var JEntryID in jentryDetails)
                                    //    {
                                    //        var jentryData = db.tbl_JEntry.Where(x => x.JEntryId == JEntryID.JEntryId).FirstOrDefault();

                                    //        if (jentryData != null)
                                    //        {
                                    //            //if (jentryData.Amount < model.Amount)
                                    //            //{
                                    //            //    var lessJEntryId = db.insertEmployeeCashPaymentsForGreaterAmount(model.Amount - jentryData.Amount, Helper.PST(), 1, model.AccountID, model.Description).FirstOrDefault();
                                    //            //}
                                    //            //else if (jentryData.Amount > model.Amount)
                                    //            //{
                                    //            //    var lessJEntryId = db.insertEmployeeCashPaymentsForLesserAmount(jentryData.Amount - model.Amount, Helper.PST(), 1, model.AccountID, model.Description).FirstOrDefault();
                                    //            //}

                                    //            //var payJEntryId = db.insertEmployeeCashSettlements(model.Amount, Helper.PST(), 1, model.AccountID, purchase.OrderID, model.Description).FirstOrDefault();
                                    //            decimal jentryAmount = jentryData.Amount ?? 0;
                                    //            //if (totalAmount >= jentryData.Amount) { totalAmount -= jentryData.Amount ?? 0; }
                                    //            jentryData.IsClearedFromEmployee = totalAmount >= jentryData.Amount ? true : false;
                                    //            jentryData.Amount = totalAmount >= jentryData.Amount ? 0 : jentryData.Amount - totalAmount;
                                    //            jentryData.UpdateOn = Helper.PST();
                                    //            jentryData.SaleOrderID = purchase.OrderID;
                                    //            jentryData.ReferenceDetail += totalAmount >= jentryAmount ? "-|- Cleared From Employee On : " + Helper.PST().ToLongDateString() + " with EmployeeOrderID: " + purchase.OrderID.ToString() : "-|-Deducted Amount From Employee EntryId: " + jentryData.JEntryId.ToString() + " On : " + Helper.PST().ToLongDateString() + " with EmployeeOrderID: " + purchase.OrderID.ToString();
                                    //            if (jentryData.tbl_JDetail != null && jentryData.tbl_JDetail.Count > 0)
                                    //            {
                                    //                foreach (var i in jentryData.tbl_JDetail)
                                    //                {
                                    //                    if (i?.Cr > 0)
                                    //                    {
                                    //                        i.Cr = purchase.TotalAmount - totalExpAmountForSettltement;
                                    //                    }
                                    //                    else if (i?.Dr > 0)
                                    //                    {
                                    //                        i.Dr = purchase.TotalAmount - totalExpAmountForSettltement;
                                    //                    }
                                    //                }
                                    //            }

                                    //            db.Entry(jentryData).State = EntityState.Modified;
                                    //            c += db.SaveChanges();
                                    //            //totalAmount = jentryAmount > totalAmount ? totalAmount:totalAmount - jentryAmount;
                                    //            totalAmount -= jentryAmount;
                                    //            if (totalAmount <= 0) // if amount is lesser, than just go out without checking other settlement entries
                                    //            {
                                    //                break;
                                    //            }
                                    //            //if (totalAmount >= jentryAmount)
                                    //            //{
                                    //            //    totalAmount -= jentryAmount;
                                    //            //}
                                    //            //else
                                    //            //{
                                    //            //    totalAmount = jentryAmount - totalAmount;
                                    //            //}
                                    //        }
                                    //    }
                                    //}
                                    #endregion

                                    if (c > 0)
                                    {
                                        t.Commit();
                                        UserActionsPerformed UserActions = new UserActionsPerformed();
                                        Common com = new Common();
                                        UserActions.MapActions(com.GetUserID(), "Performed Employee Purchase Note. OrderID: " + purchase.OrderID.ToString());
                                        return purchase.OrderID;
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            t.Rollback();
                            while (e.InnerException != null) { e = e.InnerException; }
                            return e.Message;
                        }

                    }

                }

                return "Something Went Wrong";
            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return e.Message;
            }

        }

        public static object EditApprovedEmployeePurchase(int EmployeePurchaseID, Payment model, List<Models.DTO.EmployeePurchaseDetail> detailList, Models.DTO.JEntryLog jentryLog)
        {
            try
            {
                using (var db = new dbPOS())
                {
                    using (var t = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (model != null && detailList != null && detailList.Count > 0 && EmployeePurchaseID > 0 && jentryLog != null /*&& jentryLog.JEntryIDs != null && jentryLog.JEntryIDs.Count > 0*/)
                            {
                                int c = 0;
                                decimal totalAmountForSettlement = 0;
                                decimal totalExpAmountForSettlement = 0;
                                //var jentryData = db.tbl_JEntry.Where(x => x.JEntryId == model.JEntryID).FirstOrDefault();
                                var purchase = db.tbl_EmployeePurchases.Where(x => x.OrderID == EmployeePurchaseID).FirstOrDefault();


                                //decimal reSettleEmployeeBalance = purchase.TotalAmount;
                                if (/*jentryData != null &&*/ purchase != null)
                                {
                                    ReverseEmployeePurchaseAccountEntriesAndStock(EmployeePurchaseID, db);

                                    model.VoucherDate = purchase.Date;
                                    //StringBuilder desc = new StringBuilder();
                                    //desc.Append(model.Description + " | JentryIds: ");
                                    totalAmountForSettlement = model.Amount;
                                    //purchase.Date = Helper.PST();
                                    //Parallel.ForEach(jentryLog.JEntryIDs, id =>
                                    //{
                                    //    desc.Append(id.ToString() + ",");
                                    //});
                                    //purchase.Description += " -|- " + desc.ToString();
                                    purchase.EmployeeAccID = model.AccountID;
                                    //purchase.JEntryID = model.JEntryID;
                                    purchase.StatusID = 2;//Approved
                                    purchase.TotalAmount = model.Amount;

                                    purchase.IsApproved = true;
                                    //purchase.ApprovedOn = Helper.PST();
                                    purchase.UpdatedOn = Helper.PST();

                                    StringBuilder itemDescription = new StringBuilder();
                                    itemDescription.Append(model.Description + ": ");

                                    //db.tbl_EmployeePurchases.Add(purchase);
                                    //c += db.SaveChanges();

                                    List<tbl_EmployeePurchaseDetails> details = new List<tbl_EmployeePurchaseDetails>();
                                    List<tbl_StockLog> logList = new List<tbl_StockLog>();

                                    if (purchase.tbl_EmployeePurchaseDetails != null && purchase.tbl_EmployeePurchaseDetails.Count > 0)
                                    {
                                        db.tbl_EmployeePurchaseDetails.RemoveRange(purchase.tbl_EmployeePurchaseDetails);
                                        c += db.SaveChanges();
                                        db.UpdateEmployeeCashBalance(purchase.EmployeeAccID);
                                        c += db.SaveChanges();
                                        //var empData = db.tbl_Employee.Where(x => x.AccountID == model.AccountID).FirstOrDefault();
                                        //if (empData != null)
                                        //{
                                        //    //empData.EmployeePettyCashBalance -= model.Amount;
                                        //    empData.EmployeePettyCashBalance = 0;
                                        //    db.Entry(empData).State = EntityState.Modified;
                                        //    c += db.SaveChanges();
                                        //}
                                        //purchase.tbl_EmployeePurchaseDetails = new List<tbl_EmployeePurchaseDetails>();

                                    }

                                    foreach (var i in detailList)
                                    {

                                        tbl_EmployeePurchaseDetails detail = new tbl_EmployeePurchaseDetails();
                                        detail.AddedOn = Helper.PST();
                                        detail.Description = i.Description.Trim();
                                        detail.IsProduct = i.IsProduct;
                                        if (i.ProductID != null && i.ProductID > 0)
                                        {
                                            detail.ProductID = i.ProductID;
                                            itemDescription.Append(db.tbl_Product.Where(x => x.ProductID == i.ProductID).Select(x => x.PartNo).FirstOrDefault());
                                            itemDescription.Append(" | ");
                                        }
                                        detail.Qty = i.Qty;
                                        detail.Total = i.Total;
                                        detail.UnitPrice = i.UnitPrice;
                                        detail.OrderID = purchase.OrderID;
                                        detail.ExpenseID = i.ExpenseID;

                                        if (detail.ExpenseID != null && detail.ExpenseID > 0)
                                        {
                                            totalExpAmountForSettlement += detail.Total;
                                            totalAmountForSettlement -= detail.Total;
                                            detail.Description = string.IsNullOrWhiteSpace(detail.Description) ? "-" : detail.Description;
                                            var id = db.insertEmployeeCashSettlementsForExpensesEntry(detail.Total, model.VoucherDate, 1, detail.ExpenseID, "Expense Item: " + detail.Description.ToString() + " With Employee OID: " + purchase.OrderID.ToString(), detail.Description.ToString(), 9001, 1,purchase.OrderID).FirstOrDefault();
                                        }

                                        details.Add(detail);


                                        tbl_StockLog log = new tbl_StockLog();

                                        log.AccountID = model.AccountID;
                                        log.AddBy = 1;
                                        log.AddOn = Helper.PST();
                                        log.BranchID = 9001;
                                        log.CostPrice = i.UnitPrice;
                                        log.Detail = "Product Purchased: " + i.Description.Trim();
                                        log.InReference = "Employee Purchase";
                                        log.InvoiceDate = model.VoucherDate;
                                        log.IsActive = true;
                                        log.OrderID = purchase.OrderID;
                                        log.OrderTypeID = 15;//Employee Purchase
                                        log.ProductID = (i.ProductID != null && i.ProductID > 0) ? i.ProductID ?? 0 : 1;//General Product
                                        log.ReturnedQty = 0;
                                        log.SalePrice = 0;
                                        log.StockIN = i.Qty;
                                        log.StockOut = 0;

                                        logList.Add(log);

                                        if (i.ProductID != null && i.ProductID > 0)
                                        {
                                            if (db.tbl_Stock.Any(x => x.ProductID == i.ProductID && x.IsActive == true))
                                            {
                                                var stk = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && x.IsActive == true).FirstOrDefault();
                                                if (stk != null)
                                                {
                                                    stk.Qty += i.Qty;
                                                    stk.CostPrice = stk.CostPrice > 0 ? (stk.CostPrice + i.UnitPrice) / 2 : i.UnitPrice;
                                                    stk.UpdateOn = Helper.PST();
                                                }
                                            }
                                            else
                                            {
                                                tbl_Stock stock = new tbl_Stock();

                                                stock.Qty = i.Qty;
                                                stock.CostPrice = i.UnitPrice;
                                                stock.Addon = Helper.PST();
                                                stock.AddBy = 1;
                                                stock.BranchID = 9001;
                                                stock.SalePrice = 0;
                                                stock.ProductID = i.ProductID ?? 0;

                                                db.tbl_Stock.Add(stock);
                                                c += db.SaveChanges();

                                            }

                                        }

                                    }

                                    var getEmployeePendings = db.GetJEntryListEmployeeIDWise(purchase.EmployeeAccID).ToList();

                                    if (getEmployeePendings != null && getEmployeePendings.Count > 0)
                                    {
                                        foreach (var e in getEmployeePendings)
                                        {
                                            var entry = db.tbl_JEntry.Where(x => x.JEntryId == e.JEntryId).FirstOrDefault();
                                            if (entry != null)
                                            {
                                                entry.IsActive = false;
                                                entry.IsDeleted = true;
                                                entry.DeleteOn = Helper.PST();
                                                if (entry.tbl_JDetail != null && entry.tbl_JDetail.Count > 0)
                                                {
                                                    entry.tbl_JDetail.ToList().ForEach(x => x.IsDeleted = true);
                                                }
                                                db.Entry(entry).State = EntityState.Modified;
                                                c += db.SaveChanges();
                                            }
                                        }
                                    }

                                    if (details.Count > 0)
                                    {
                                        db.tbl_EmployeePurchaseDetails.AddRange(details);
                                        db.Entry(purchase).State = EntityState.Modified;
                                        db.tbl_StockLog.AddRange(logList);
                                        c += db.SaveChanges();

                                        if (totalAmountForSettlement > 0)
                                            db.insertEmployeeCashSettlements(totalAmountForSettlement, model.VoucherDate, 1, model.AccountID, purchase.OrderID, itemDescription.ToString()).FirstOrDefault();
                                    }


                                    if (c > 0)
                                    {
                                        t.Commit();
                                        UserActionsPerformed UserActions = new UserActionsPerformed();
                                        Common com = new Common();
                                        UserActions.MapActions(com.GetUserID(), "Performed Edit Approved Employee Purchase Note. OrderID: " + purchase.OrderID.ToString());
                                        return purchase.OrderID;
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            t.Rollback();
                            while (e.InnerException != null) { e = e.InnerException; }
                            return e.Message;
                        }

                    }

                }

                return "Something Went Wrong";
            }
            catch (Exception e)
            {
                while (e.InnerException != null) { e = e.InnerException; }
                return e.Message;
            }

        }

        private static bool ReverseEmployeePurchaseAccountEntriesAndStock(int EmployeePurchaseOrderID, dbPOS db)
        {
            try
            {
                int c = 0;
                var stkLog = db.tbl_StockLog.Where(x => x.OrderTypeID == 15 && x.OrderID == EmployeePurchaseOrderID).ToList();
                if (stkLog != null && stkLog.Count > 0)
                {
                    foreach (var s in stkLog)
                    {
                        var stk = db.tbl_Stock.Where(x => x.ProductID == s.ProductID && x.ProductID > 0).FirstOrDefault();
                        if (stk != null)
                        {
                            stk.Qty -= s.StockIN ?? 0;
                            stk.IsUpdated = true;
                            stk.UpdateOn = Helper.PST();
                            db.Entry(stk).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        db.Entry(s).State = EntityState.Deleted;
                        db.SaveChanges();

                    }
                }

                // Delete Expenses
                var jEntry = db.tbl_JEntry.Where(x => x.VoucherName.Equals("Employee Purchased External Expense") && x.RefID == EmployeePurchaseOrderID).ToList();

                if(jEntry!=null && jEntry.Count > 0)
                {
                    foreach(var j in jEntry)
                    {
                        j.IsActive = false;
                        j.IsDeleted = true;
                        j.DeleteOn = Helper.PST();
                        

                        if(j.tbl_JDetail!=null && j.tbl_JDetail.Count > 0)
                        {
                            foreach(var jd in j.tbl_JDetail)
                            {
                                jd.IsDeleted = true;
                                jd.DeleteOn = Helper.PST();
                                db.Entry(jd).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }

                        db.Entry(j).State = EntityState.Modified;
                        c += db.SaveChanges();
                    }
                }

                // Delete Purchase Total
                var jEntryForEmpPurchase = db.tbl_JEntry.Where(x => x.VoucherName.Equals("Employee Purchase") && x.RefID == EmployeePurchaseOrderID).ToList();

                if (jEntryForEmpPurchase != null && jEntryForEmpPurchase.Count > 0)
                {
                    foreach (var j in jEntryForEmpPurchase)
                    {
                        j.IsActive = false;
                        j.IsDeleted = true;
                        j.DeleteOn = Helper.PST();


                        if (j.tbl_JDetail != null && j.tbl_JDetail.Count > 0)
                        {
                            foreach (var jd in j.tbl_JDetail)
                            {
                                jd.IsDeleted = true;
                                jd.DeleteOn = Helper.PST();
                                db.Entry(jd).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }

                        db.Entry(j).State = EntityState.Modified;
                        c += db.SaveChanges();
                    }
                }

                if (c > 0)
                    return true;
            }
            catch (Exception e)
            {

            }
            return false;
        }

        public static bool DeleteEmployeePendingPurchase(int EmpOID)
        {
            try
            {
                using(var db = new dbPOS())
                {
                    using(var t = db.Database.BeginTransaction())
                    {
                        var purchase = db.tbl_EmployeePurchases.Where(x => x.OrderID == EmpOID).FirstOrDefault();
                        if (purchase != null)
                        {
                            //bool isReversed = ReverseEmployeePurchaseAccountEntriesAndStock(EmpOID, db);
                            
                                purchase.IsDeleted = true;
                                purchase.DeletedOn = Helper.PST();
                                db.Entry(purchase).State = EntityState.Modified;
                                db.SaveChanges();

                                t.Commit();
                                return true;
                            
                        }

                    }
                }
            }
            catch(Exception e) { }
            return false;
        }
    }
}
