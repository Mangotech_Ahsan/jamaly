﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using System.Text;
using POSOman.Models.DTO;
using Syncfusion.Linq;

namespace POSOman.Models.BLL
{
    public class SalesOrder
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();

        #region Update Satin Product Details 
        public object updateSatinOrderProduct(SaleProductDTO model)
        {
            dbPOS db = new dbPOS();
            string result = "";
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    var getData = db.tbl_SaleDetails.Where(x => x.OrderID == model.OrderID && x.ProductID == model.ProductID && x.tbl_Product.DepartmentID == model.DepartmentID).FirstOrDefault();
                    if (getData != null)
                    {
                        //getData.ReadyQty = Convert.ToInt32(model.ReadyQty);
                        getData.ExpectedItemDeliveryDate = model.ExpectedDeliveryDate;
                        //getData.SlittingOMachineID = model.SlittingOMachineID;
                        //getData.DieCuttingOMachineID = model.DieCuttingOMachineID;
                        //getData.UVOMachineID = model.UVOMachineID;
                        //getData.LaminationOMachineID = model.LaminationOMachineID;
                        //getData.FoilOMachineID = model.FoilOMachineID;
                        //getData.EmbosingOMachineID = model.EmbosingOMachineID;

                        if (model.MachineTypeID == 1 && model.AttachedMachineID > 0)
                        {
                            getData.SlittingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 3 && model.AttachedMachineID > 0)
                        {
                            getData.DieCuttingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 2 && model.AttachedMachineID > 0)
                        {
                            getData.PrintingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 4 && model.AttachedMachineID > 0)
                        {
                            getData.UVOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 5 && model.AttachedMachineID > 0)
                        {
                            getData.LaminationOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 6 && model.AttachedMachineID > 0)
                        {
                            getData.FoilOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 7 && model.AttachedMachineID > 0)
                        {
                            getData.EmbosingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 8 && model.AttachedMachineID > 0)
                        {
                            getData.PackagingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 9 && model.AttachedMachineID > 0)
                        {
                            getData.FoldingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 10 && model.AttachedMachineID > 0)
                        {
                            getData.PlanningOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 11 && model.AttachedMachineID > 0)
                        {
                            getData.OutSourceOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 12 && model.AttachedMachineID > 0)
                        {
                            getData.GeneralOMachineID = model.AttachedMachineID;
                        }

                        db.Entry(getData).State = EntityState.Modified;

                        //tbl_JobOrderMachinesLogs log = new tbl_JobOrderMachinesLogs();
                        //log.AddedOn = Helper.PST();
                        //log.ItemID = model.ProductID;
                        //log.MachineID = model.AttachedMachineID ?? 0;
                        //log.MachineTypeID = model.MachineTypeID ?? 0;
                        //log.OrderID = model.OrderID;
                        //db.tbl_JobOrderMachinesLogs.Add(log);

                        //getData.QtyBalanced = Convert.ToInt32(getData.Qty - Convert.ToDecimal(model.ReadyQty));

                        //var Order = db.tbl_SalesOrder.Where(x => x.OrderID == model.OrderID).FirstOrDefault();
                        //if (Order != null)
                        //{
                        //    Order.ExpectedDeliveryDate = model.ExpectedDeliveryDate;
                        //    db.Entry(Order).State = EntityState.Modified;
                        //}

                        db.SaveChanges();
                    }


                    transaction.Commit();

                    return model.OrderID;
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    while (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                    }

                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = ex.Message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return ex.Message;
                }
            }
        }
        public object updateStickerOrderProduct(SaleProductDTO model)
        {
            dbPOS db = new dbPOS();
            string result = "";
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    var getData = db.tbl_SaleDetails.Where(x => x.OrderID == model.OrderID && x.ProductID == model.ProductID && x.tbl_Product.DepartmentID == model.DepartmentID).FirstOrDefault();
                    if (getData != null)
                    {
                        //getData.ReadyQty = Convert.ToInt32(model.ReadyQty);
                        getData.ExpectedItemDeliveryDate = model.ExpectedDeliveryDate;
                        if (model.MachineTypeID == 1 && model.AttachedMachineID > 0)
                        {
                            getData.SlittingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 3 && model.AttachedMachineID > 0)
                        {
                            getData.DieCuttingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 2 && model.AttachedMachineID > 0)
                        {
                            getData.PrintingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 4 && model.AttachedMachineID > 0)
                        {
                            getData.UVOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 5 && model.AttachedMachineID > 0)
                        {
                            getData.LaminationOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 6 && model.AttachedMachineID > 0)
                        {
                            getData.FoilOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 7 && model.AttachedMachineID > 0)
                        {
                            getData.EmbosingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 8 && model.AttachedMachineID > 0)
                        {
                            getData.PackagingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 9 && model.AttachedMachineID > 0)
                        {
                            getData.FoldingOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 10 && model.AttachedMachineID > 0)
                        {
                            getData.PlanningOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 11 && model.AttachedMachineID > 0)
                        {
                            getData.OutSourceOMachineID = model.AttachedMachineID;
                        }
                        else if (model.MachineTypeID == 12 && model.AttachedMachineID > 0)
                        {
                            getData.GeneralOMachineID = model.AttachedMachineID;
                        }
                        db.Entry(getData).State = EntityState.Modified;

                        //tbl_JobOrderMachinesLogs log = new tbl_JobOrderMachinesLogs();
                        //log.AddedOn = Helper.PST();
                        //log.ItemID = model.ProductID;
                        //log.MachineID = model.AttachedMachineID??0;
                        //log.MachineTypeID = model.MachineTypeID??0;
                        //log.OrderID = model.OrderID;
                        //db.tbl_JobOrderMachinesLogs.Add(log);

                        //var Order = db.tbl_SalesOrder.Where(x => x.OrderID == model.OrderID).FirstOrDefault();
                        //if (Order != null)
                        //{
                        //    Order.ExpectedDeliveryDate = model.ExpectedDeliveryDate;
                        //    db.Entry(Order).State = EntityState.Modified;
                        //}

                        db.SaveChanges();
                    }


                    transaction.Commit();

                    return model.OrderID;
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    while (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                    }

                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = ex.Message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return ex.Message;
                }
            }
        }
        public object TrelloTransferMachineProcess(SaleProductDTO model)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        var getData = db.tbl_SaleDetails.Where(x => x.OrderID == model.OrderID && x.ProductID == model.ProductID).FirstOrDefault();
                        if (getData != null)
                        {
                            //var modifyData = db.tbl_JobOrderMachinesLogs.Where(x => x.OrderID == model.OrderID && x.ItemID == model.ProductID && x.IsDeleted != true).OrderBy(x => x.ID).ToList();
                            //if (modifyData != null && modifyData.Count > 0)
                            //{
                                tbl_JobOrderMachinesLogs nextMachine = new tbl_JobOrderMachinesLogs();
                                //foreach (var i in modifyData)
                                //{
                                //    if (i.IsCurrentMachine == false && i.IsMachineUsed == false)
                                //    {
                                //        nextMachine = i;
                                //        break;
                                //    }
                                //}
                                nextMachine.AddedOn = Helper.PST();
                                nextMachine.ItemRank = model.toIndex;
                                nextMachine.IsCurrentMachine = true;
                                nextMachine.IsManual = true;
                                nextMachine.IsMachineUsed = true;
                                nextMachine.ItemID = model.ProductID;
                                nextMachine.MachineID = model.MachineID??0;
                                nextMachine.OrderID = model.OrderID;
                                nextMachine.MachineTypeID = db.tbl_OrderMachine.Where(x => x.ID == model.MachineID).Select(x => x.MachineTypeID).FirstOrDefault()??0;
                                db.tbl_JobOrderMachinesLogs.Add(nextMachine);
                                db.SaveChanges();

                                if (nextMachine.ID > 0)
                                {
                                    //modifyData.ForEach(x => { if (x.MachineID == nextMachine.MachineID && x.ID == nextMachine.ID) { x.IsCurrentMachine = true; x.IsMachineUsed = true; } else { x.IsCurrentMachine = false; } db.Entry(x).State = EntityState.Modified; });
                                    
                                    if (nextMachine.MachineTypeID == 1 && nextMachine.MachineID > 0)
                                    {
                                        getData.SlittingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 2 && nextMachine.MachineID > 0)
                                    {
                                        getData.PrintingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 3 && nextMachine.MachineID > 0)
                                    {
                                        getData.DieCuttingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 4 && nextMachine.MachineID > 0)
                                    {
                                        getData.UVOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 5 && nextMachine.MachineID > 0)
                                    {
                                        getData.LaminationOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 6 && nextMachine.MachineID > 0)
                                    {
                                        getData.FoilOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 7 && nextMachine.MachineID > 0)
                                    {
                                        getData.EmbosingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 8 && nextMachine.MachineID > 0)
                                    {
                                        getData.PackagingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 9 && nextMachine.MachineID > 0)
                                    {
                                        getData.FoldingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 10 && nextMachine.MachineID > 0)
                                    {
                                        getData.PlanningOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 11 && nextMachine.MachineID > 0)
                                    {
                                        getData.OutSourceOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 12 && nextMachine.MachineID > 0)
                                    {
                                        getData.GeneralOMachineID = nextMachine.MachineID;
                                    }
                                    db.Entry(getData).State = EntityState.Modified;

                                    db.SaveChanges();
                                    t.Commit();
                                    return 1;
                                }
                                else
                                {
                                    t.Rollback();
                                    return -2;
                                }

                            //}



                        }
                        return 0;
                    }
                    catch (Exception d)
                    {
                        t.Rollback();
                        //while (d.InnerException != null)
                        //{
                        //    d = d.InnerException;
                        //}
                        return -1;
                    }
                }
            }
            catch (Exception e)
            {
                //while (e.InnerException != null)
                //{
                //    e = e.InnerException;
                //}
                return -2;
            }
        }
        public object TransferMachineProcess(SaleProductDTO model)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        var getData = db.tbl_SaleDetails.Where(x => x.OrderID == model.OrderID && x.ProductID == model.ProductID && x.tbl_Product.DepartmentID == model.DepartmentID).FirstOrDefault();
                        if (getData != null)
                        {
                            var modifyData = db.tbl_JobOrderMachinesLogs.Where(x => x.OrderID == model.OrderID && x.ItemID == model.ProductID && x.IsDeleted != true).OrderBy(x => x.ID).ToList();
                            if (modifyData != null && modifyData.Count > 0)
                            {
                                tbl_JobOrderMachinesLogs nextMachine = new tbl_JobOrderMachinesLogs();
                                foreach (var i in modifyData)
                                {
                                    if (i.IsCurrentMachine == false && i.IsMachineUsed == false)
                                    {
                                        nextMachine = i;
                                        break;
                                    }
                                }

                                if (nextMachine.ID > 0)
                                {
                                    modifyData.ForEach(x => { if (x.MachineID == nextMachine.MachineID && x.ID == nextMachine.ID) { x.IsCurrentMachine = true; x.IsMachineUsed = true; } else { x.IsCurrentMachine = false; } db.Entry(x).State = EntityState.Modified; });
                                    //var toModify = db.tbl_JobOrderMachinesLogs.Where(x=> modifyData.Contains(x.ID)).tol;
                                    //foreach(var item in modifyData)
                                    //{
                                    //    db.Entry(item).State = EntityState.Modified;

                                    //}


                                    if (nextMachine.MachineTypeID == 1 && nextMachine.MachineID > 0)
                                    {
                                        getData.SlittingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 2 && nextMachine.MachineID > 0)
                                    {
                                        getData.PrintingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 3 && nextMachine.MachineID > 0)
                                    {
                                        getData.DieCuttingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 4 && nextMachine.MachineID > 0)
                                    {
                                        getData.UVOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 5 && nextMachine.MachineID > 0)
                                    {
                                        getData.LaminationOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 6 && nextMachine.MachineID > 0)
                                    {
                                        getData.FoilOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 7 && nextMachine.MachineID > 0)
                                    {
                                        getData.EmbosingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 8 && nextMachine.MachineID > 0)
                                    {
                                        getData.PackagingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 9 && nextMachine.MachineID > 0)
                                    {
                                        getData.FoldingOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 10 && nextMachine.MachineID > 0)
                                    {
                                        getData.PlanningOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 11 && nextMachine.MachineID > 0)
                                    {
                                        getData.OutSourceOMachineID = nextMachine.MachineID;
                                    }
                                    else if (nextMachine.MachineTypeID == 12 && nextMachine.MachineID > 0)
                                    {
                                        getData.GeneralOMachineID = nextMachine.MachineID;
                                    }
                                    db.Entry(getData).State = EntityState.Modified;

                                    db.SaveChanges();
                                    t.Commit();
                                    return 1;
                                }
                                else
                                {
                                    t.Rollback();
                                    return -2;
                                }

                            }



                        }
                        return 0;
                    }
                    catch (Exception d)
                    {
                        t.Rollback();
                        //while (d.InnerException != null)
                        //{
                        //    d = d.InnerException;
                        //}
                        return -1;
                    }
                }
            }
            catch (Exception e)
            {
                //while (e.InnerException != null)
                //{
                //    e = e.InnerException;
                //}
                return -2;
            }
        }

        public object DiscardRemainingQty(SaleProductDTO model)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        var getData = db.tbl_SaleDetails.Where(x => x.OrderID == model.OrderID && x.ProductID == model.ProductID && x.tbl_Product.DepartmentID == model.DepartmentID).FirstOrDefault();
                        if (getData != null)
                        {
                            getData.ReturnedQty = getData.QtyBalanced;
                            getData.Qty = getData.QtyDelivered;
                            getData.QtyBalanced = 0;

                            db.Entry(getData).State = EntityState.Modified;

                            var pkg = db.tbl_SOItemWisePackageDetails.Where(x => x.ItemID == model.ProductID && x.OrderID == model.OrderID && x.IsDeleted != true).ToList();
                            if (pkg != null && pkg.Count > 0)
                            {
                                decimal returningQty = getData.ReturnedQty ?? 0;
                                foreach (var p in pkg)
                                {
                                    if (returningQty > 0 && p.ProductQty > 0 && p.ProductQty <= returningQty)
                                    {
                                        returningQty -= p.ProductQty;
                                        p.ProductQty = p.DeliveredQty;

                                        db.Entry(p).State = EntityState.Modified;

                                        var chPkg = db.tbl_DisplayChallanItemsDescription.Where(x => x.PackageId == p.PackageId).FirstOrDefault();
                                        if (chPkg != null)
                                        {
                                            chPkg.ReturnedQty = Convert.ToInt32(p.ProductQty);
                                            db.Entry(chPkg).State = EntityState.Modified;
                                        }
                                    }
                                    else if (returningQty > 0 && p.ProductQty > 0 && p.ProductQty > returningQty)
                                    {
                                        var chPkg = db.tbl_DisplayChallanItemsDescription.Where(x => x.PackageId == p.PackageId).FirstOrDefault();
                                        if (chPkg != null)
                                        {
                                            chPkg.ReturnedQty = Convert.ToInt32(p.ProductQty - returningQty);
                                            db.Entry(chPkg).State = EntityState.Modified;
                                        }

                                        returningQty -= returningQty;
                                        p.ProductQty = p.DeliveredQty;

                                        db.Entry(p).State = EntityState.Modified;

                                    }
                                }
                                db.SaveChanges();

                            }

                            tbl_WastageSOItemQty log = new tbl_WastageSOItemQty();
                            log.AddedOn = Helper.PST();
                            log.DepartmentID = model.DepartmentID ?? 0;
                            log.OrderID = model.OrderID;
                            log.ItemID = model.ProductID;
                            log.WastageQty = getData.ReturnedQty ?? 0;

                            db.tbl_WastageSOItemQty.Add(log);

                            db.SaveChanges();
                            t.Commit();
                            return 1;
                        }
                        t.Rollback();
                        return 0;
                    }
                    catch (Exception d)
                    {
                        t.Rollback();
                        //while (d.InnerException != null)
                        //{
                        //    d = d.InnerException;
                        //}
                        return -1;
                    }
                }
            }
            catch (Exception e)
            {
                //while (e.InnerException != null)
                //{
                //    e = e.InnerException;
                //}
                return -2;
            }
        }

        public object MoveQtyToStock(SaleProductDTO model)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        var getData = db.tbl_SaleDetails.Where(x => x.OrderID == model.OrderID && x.ProductID == model.ProductID && x.tbl_Product.DepartmentID == model.DepartmentID).FirstOrDefault();
                        if (getData != null)
                        {
                            int prevReadyQty = getData.ReadyQty;
                            //getData.QtyBalanced -= model.MovingQty;


                            #region packaging qty scenario from discard qty

                            var pkg = db.tbl_SOItemWisePackageDetails.Where(x => x.ItemID == model.ProductID && x.OrderID == model.OrderID && x.IsDeleted != true).ToList();
                            if (pkg != null && pkg.Count > 0)
                            {
                                decimal returningQty = Convert.ToInt32(model.MovingQty);
                                foreach (var p in pkg)
                                {
                                    if (returningQty > 0 && p.ProductQty > 0 && p.ProductQty == returningQty && p.IsDeleted != true)
                                    {
                                        returningQty -= p.ProductQty;
                                        p.DeliveredQty += Convert.ToInt32(returningQty);
                                        p.DeliverQty = 0;
                                        //p.IsDeleted = true;

                                        db.Entry(p).State = EntityState.Modified;

                                    }
                                    else if (returningQty > 0 && p.ProductQty > 0 && p.ProductQty < returningQty && p.IsDeleted != true)
                                    {
                                        returningQty -= p.ProductQty;
                                        p.DeliveredQty += Convert.ToInt32(p.ProductQty);
                                        p.DeliverQty = 0;

                                        db.Entry(p).State = EntityState.Modified;

                                    }
                                    else if (returningQty > 0 && p.ProductQty > 0 && p.ProductQty > returningQty && p.IsDeleted != true)
                                    {

                                        p.DeliverQty -= Convert.ToInt32(returningQty);
                                        p.DeliveredQty += Convert.ToInt32(returningQty);
                                        returningQty -= returningQty;

                                        db.Entry(p).State = EntityState.Modified;

                                    }
                                }
                                db.SaveChanges();

                            }
                            //tbl_WastageSOItemQty log = new tbl_WastageSOItemQty();
                            //log.AddedOn = Helper.PST();
                            //log.DepartmentID = model.DepartmentID ?? 0;
                            //log.OrderID = model.OrderID;
                            //log.ItemID = model.ProductID;
                            //log.WastageQty = getData.ReturnedQty ?? 0;

                            //db.tbl_WastageSOItemQty.Add(log);
                            #endregion

                            #region qty moved to stock
                            var stkQty = db.tbl_Stock.Where(x => x.ProductID == getData.ProductID && x.IsItem == true).FirstOrDefault();
                            if (stkQty != null)
                            {
                                var log = db.tbl_SOItemStockDetails.Where(x => x.ItemID == getData.ProductID && x.OrderID == getData.OrderID && x.IsDeleted != true).FirstOrDefault();
                                if (log != null)
                                {
                                    log.Qty += model.MovingQty;
                                    db.Entry(log).State = EntityState.Modified; db.SaveChanges();

                                }
                                else
                                {
                                    tbl_SOItemStockDetails logSO = new tbl_SOItemStockDetails();
                                    logSO.AddedOn = Helper.PST();
                                    logSO.DepartmentID = getData.tbl_Product.DepartmentID ?? 0;
                                    logSO.ItemID = getData.ProductID;
                                    logSO.OrderID = getData.OrderID;
                                    logSO.Qty = model.MovingQty;
                                    db.tbl_SOItemStockDetails.Add(logSO);
                                    db.SaveChanges();
                                }
                                stkQty.Qty += model.MovingQty;
                                db.Entry(stkQty).State = EntityState.Modified; db.SaveChanges();

                            }
                            else
                            {
                                tbl_Stock stk = new tbl_Stock();
                                stk.Addon = Helper.PST();
                                stk.BranchID = 9001;
                                stk.CostPrice = getData.tbl_Product.MarkedupPrice;
                                stk.IsItem = true;
                                stk.IsActive = true;
                                stk.ProductID = getData.ProductID;
                                stk.Qty = model.MovingQty;
                                db.tbl_Stock.Add(stk);
                                db.SaveChanges();

                                tbl_SOItemStockDetails logSO = new tbl_SOItemStockDetails();
                                logSO.AddedOn = Helper.PST();
                                logSO.DepartmentID = getData.tbl_Product.DepartmentID ?? 0;
                                logSO.ItemID = getData.ProductID;
                                logSO.OrderID = getData.OrderID;
                                logSO.Qty = model.MovingQty;
                                db.tbl_SOItemStockDetails.Add(logSO);
                                db.SaveChanges();

                            }

                            getData.ReadyQty = 0;
                            getData.QtyBalanced = 0;

                            db.Entry(getData).State = EntityState.Modified;
                            #endregion

                            db.SaveChanges();
                            t.Commit();
                            return 1;
                        }
                        t.Rollback();
                        return 0;
                    }
                    catch (Exception d)
                    {
                        t.Rollback();
                        //while (d.InnerException != null)
                        //{
                        //    d = d.InnerException;
                        //}
                        return -1;
                    }
                }
            }
            catch (Exception e)
            {
                //while (e.InnerException != null)
                //{
                //    e = e.InnerException;
                //}
                return -2;
            }
        }
        public object UpdateMachineProcess(SaleProductDTO model)
        {
            try
            {
                using (var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        bool setCurrentMachine = false;
                        var getData = db.tbl_SaleDetails.Where(x => x.OrderID == model.OrderID && x.ProductID == model.ProductID && x.tbl_Product.DepartmentID == model.DepartmentID).FirstOrDefault();
                        var getMachineData = model.AttachedMachinesList.Select(x => new { x.AttachedMachineID, x.MachineTypeID }).FirstOrDefault();
                        if (getData != null && getMachineData != null)
                        {
                            if (getMachineData.MachineTypeID == 1 && getMachineData.AttachedMachineID > 0)
                            {
                                getData.SlittingOMachineID = getMachineData.AttachedMachineID;
                            }
                            else if (getMachineData.MachineTypeID == 2 && getMachineData.AttachedMachineID > 0)
                            {
                                getData.PrintingOMachineID = getMachineData.AttachedMachineID;
                            }
                            else if (getMachineData.MachineTypeID == 3 && getMachineData.AttachedMachineID > 0)
                            {
                                getData.DieCuttingOMachineID = getMachineData.AttachedMachineID;
                            }
                            else if (getMachineData.MachineTypeID == 4 && getMachineData.AttachedMachineID > 0)
                            {
                                getData.UVOMachineID = getMachineData.AttachedMachineID;
                            }
                            else if (getMachineData.MachineTypeID == 5 && getMachineData.AttachedMachineID > 0)
                            {
                                getData.LaminationOMachineID = getMachineData.AttachedMachineID;
                            }
                            else if (getMachineData.MachineTypeID == 6 && getMachineData.AttachedMachineID > 0)
                            {
                                getData.FoilOMachineID = getMachineData.AttachedMachineID;
                            }
                            else if (getMachineData.MachineTypeID == 7 && getMachineData.AttachedMachineID > 0)
                            {
                                getData.EmbosingOMachineID = getMachineData.AttachedMachineID;
                            }
                            db.Entry(getData).State = EntityState.Modified;

                            List<tbl_JobOrderMachinesLogs> logs = new List<tbl_JobOrderMachinesLogs>();
                            foreach (var i in model.AttachedMachinesList)
                            {
                                tbl_JobOrderMachinesLogs log = new tbl_JobOrderMachinesLogs();
                                log.AddedOn = Helper.PST();
                                log.ItemID = model.ProductID;
                                log.MachineID = i.AttachedMachineID;
                                log.MachineTypeID = i.MachineTypeID;
                                log.OrderID = model.OrderID;
                                log.IsCurrentMachine = (i.AttachedMachineID == getMachineData.AttachedMachineID && setCurrentMachine == false) ? true : false;
                                if (log.IsCurrentMachine == true) { log.IsMachineUsed = true; }
                                logs.Add(log);
                                setCurrentMachine = true;
                            }


                            db.tbl_JobOrderMachinesLogs.AddRange(logs);

                            var prevData = db.tbl_JobOrderMachinesLogs.Where(x => x.OrderID == model.OrderID && x.ItemID == model.ProductID && x.IsDeleted != true).ToList();
                            if (prevData != null && prevData.Count > 0)
                            {
                                prevData.ForEach(x => x.IsDeleted = true);
                                foreach (var item in prevData)
                                {
                                    db.Entry(item).State = EntityState.Modified;
                                }

                            }
                            db.SaveChanges();
                            t.Commit();
                            return 1;
                        }
                        return 0;
                    }
                    catch (Exception d)
                    {
                        t.Rollback();
                        //while (d.InnerException != null)
                        //{
                        //    d = d.InnerException;
                        //}
                        return -1;
                    }
                }
            }
            catch (Exception e)
            {
                //while (e.InnerException != null)
                //{
                //    e = e.InnerException;
                //}
                return -2;
            }
        }
        #endregion
        private int GetInvoiceNo(int? InvNo, int BranchID)
        {
            int newSOID = 0;

            if (InvNo == 0 || InvNo == null)
            {
                var data = db.tbl_SalesOrder.Where(so => so.BranchID == BranchID && so.isOpening != true).OrderByDescending(v => v.SOID).FirstOrDefault();
                if (data != null)
                {
                    newSOID = data.SOID + 1;
                }
                else
                {
                    newSOID = 101;
                }

            }

            return newSOID;
        }
        private int GetCode()
        {
            int basecode = 1001;
            var data = db.tbl_Challan.OrderByDescending(a => a.SOID).FirstOrDefault();
            if (data != null)
            {
                return (data.SOID + 1);
            }

            return basecode;
        }

        private int GetInvoiceNoCustomerTypeWise(int BranchID, int CustomerTypeID)
        {
            int newSOID = 1201;

            if (CustomerTypeID == 1) // Jamaly
            {
                var data = db.tbl_SalesOrder.Where(so => so.BranchID == BranchID && so.isOpening != true).OrderByDescending(v => v.O_Serial).Select(x => x.O_Serial).FirstOrDefault();
                if (data == null)
                {
                    return newSOID;
                }
                else
                {
                    newSOID = Convert.ToInt32(data) + 1;
                    return (newSOID);
                }

            }
            else if (CustomerTypeID == 2) // Jemely
            {
                var data = db.tbl_SalesOrder.Where(so => so.BranchID == BranchID && so.isOpening != true).OrderByDescending(v => v.UO_Serial).Select(x => x.UO_Serial).FirstOrDefault();
                if (data == null)
                {
                    return newSOID;
                }
                else
                {
                    newSOID = Convert.ToInt32(data) + 1;
                    return (newSOID);
                }

            }
            else
            {
                return newSOID;
            }


        }
        public object createSales(tbl_SalesOrder modelSales, List<Models.DTO.StockLog> modelStockLog, bool? isQuote, int? QuoteOrderID, int? bankAccId, int branchId, int? OrderTypeID, List<int> challanIDs, bool fsc_Cert, bool grs_Cert, bool oeko_tex_Cert, int UserID)
        {
            dbPOS db = new dbPOS();
            string result = "";
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    List<int> PIDs = modelSales.tbl_SaleDetails.Select(x => x.ProductID).ToList();

                    //if (db.tbl_Stock.Any(x => PIDs.Contains(x.ProductID) && x.Qty <= 0))
                    //{
                    //    transaction.Rollback();
                    //    return -100;
                    //}
                    if (modelSales.tbl_SaleDetails.Any(x => x.Qty <= 0))
                    {
                        transaction.Rollback();
                        return -100;
                    }
                    db.Configuration.ValidateOnSaveEnabled = false;
                    long? PrevCode = db.tbl_JEntry.OrderByDescending(x => x.CodeInt).Select(x => x.CodeInt).FirstOrDefault();
                    long NewCode = 0;
                    string NewCodeSt;

                    modelSales.ChequeDate = Helper.PST();

                    if (PrevCode != null)
                    {
                        NewCode = Convert.ToInt64(PrevCode) + 1;
                        NewCodeSt = ("SL-" + NewCode).ToString();
                    }
                    else
                    {
                        NewCode = 9501;
                        NewCodeSt = ("SL-" + NewCode).ToString();
                    }

                    if (modelSales.PaymentStatus == "UnPaid")
                    {
                        modelSales.AmountPaid = 0;
                        modelSales.IsPaid = false;
                    }
                    else if (modelSales.PaymentStatus == "Partial Paid")
                    {
                        modelSales.IsPaid = false;
                    }
                    else if (modelSales.PaymentStatus == "Paid")
                    {
                        modelSales.IsPaid = true;
                    }

                    const char chkMark = (char)(0X2713);

                    if (fsc_Cert)
                    {
                        var cert = db.tbl_Certificates.Where(x => x.ID == 3).Select(x => new { Name = x.Certificate + " " + x.CertificateInfo }).FirstOrDefault();
                        if (cert != null && !string.IsNullOrWhiteSpace(cert.Name))
                        {
                            modelSales.FSC_Cert = chkMark + " " + cert.Name;
                        }
                    }

                    if (grs_Cert)
                    {
                        var cert = db.tbl_Certificates.Where(x => x.ID == 4).Select(x => new { Name = x.Certificate + " " + x.CertificateInfo }).FirstOrDefault();
                        if (cert != null && !string.IsNullOrWhiteSpace(cert.Name))
                        {
                            modelSales.GRS_Cert = chkMark + " " + cert.Name;
                        }
                    }

                    if (oeko_tex_Cert)
                    {
                        var cert = db.tbl_Certificates.Where(x => x.ID == 5).Select(x => new { Name = x.Certificate + " " + x.CertificateInfo }).FirstOrDefault();
                        if (cert != null && !string.IsNullOrWhiteSpace(cert.Name))
                        {
                            modelSales.OEKO_TEX_Cert = chkMark + " " + cert.Name;
                        }
                    }
                    //decimal taxExcAm = 0;
                    //if(modelSales.VAT> 0)
                    //{
                    //    taxExcAm = modelSales.TotalAmount??0 - modelSales.VAT;
                    //}
                    modelSales.ReturnAmount = 0;
                    modelSales.TotalPaid = modelSales.AmountPaid;
                    string sPaymentStatus = modelSales.PaymentStatus;
                    int? iPaymentAccountID = null;
                    //modelSales.Can_Modify = false;
                    //modelSales.TaxID = null;
                    modelSales.BranchID = branchId;

                    modelSales.AddOn = Helper.PST();
                    modelSales.UserID = HttpContext.Current.User.Identity.GetUserId();
                    modelSales.IsReturned = false;
                    foreach (var i in modelSales.tbl_SaleDetails)
                    {
                        i.PartNo = db.tbl_Product.Where(x => x.ProductID == i.ProductID).Select(x => x.PartNo).FirstOrDefault() ?? "-";
                        i.BranchID = modelSales.BranchID;
                        i.IsDeleted = false;
                    }
                    if (modelSales.PaymentTypeID == 1)
                    {
                        modelSales.BankName = "";
                        iPaymentAccountID = (int)(modelSales.PaymentTypeID);//  set it from Payment Type ddl 
                    }
                    else if (modelSales.PaymentTypeID > 1)
                    {
                        iPaymentAccountID = bankAccId;
                    }
                    if (modelSales.PaymentTypeID == 4)
                    {
                        modelSales.BankName = "";
                        iPaymentAccountID = 109;//  set it from Payment Type ddl 
                    }
                    if (modelSales.PaymentTypeID == 5)
                    {
                        //modelSales.BankName = "CreditCard";
                        iPaymentAccountID = 110;//  set it from Payment Type ddl 
                    }

                    modelSales.CompanyIDLogo = 1;
                    var getInvoiceType = db.tbl_Customer.Where(x => x.AccountID == modelSales.AccountID).Select(x => x.InvoiceType).FirstOrDefault();

                    modelSales.SOID = Convert.ToInt32(GetInvoiceNoCustomerTypeWise(9001, getInvoiceType ?? 0));
                    if (getInvoiceType == null)
                    {
                        modelSales.Barcode = "SOI-" + branchId + "-" + modelSales.SOID;
                        modelSales.InvoiceNo = "SOI-" + modelSales.SOID;
                    }
                    else
                    {
                        if (getInvoiceType == 1) // Jamaly
                        {
                            //modelSales.Barcode = "STI-" + branchId + "-" + modelSales.SOID;
                            //modelSales.InvoiceNo = "STI-" + modelSales.SOID;
                            modelSales.Barcode = "JI-23-0-" + branchId + "-" + modelSales.SOID;
                            modelSales.InvoiceNo = "JI-23-0" + modelSales.SOID;
                            modelSales.O_Serial = modelSales.SOID;
                        }
                        else if (getInvoiceType == 2) // Jemely
                        {
                            modelSales.Barcode = "JE-23-0-" + branchId + "-" + modelSales.SOID;
                            modelSales.InvoiceNo = "JE-23-0" + modelSales.SOID;
                            modelSales.UO_Serial = modelSales.SOID;
                        }

                    }

                    db.tbl_SalesOrder.Add(modelSales);
                    int soId = db.SaveChanges();

                    int iOrderID = modelSales.OrderID;
                    decimal dAmountPaid = 0;
                    decimal COGS = 0;
                    if (modelSales.AmountPaid != null || modelSales.AmountPaid > 0)
                    {
                        dAmountPaid = (decimal)(modelSales.AmountPaid);
                    }

                    if (soId >= 1 && modelStockLog != null)
                    {
                        /////////////Stock///////////////

                        #region Stock
                        //List<int> lstProdIds = modelSales.tbl_SaleDetails.Select(p => p.ProductID).ToList();
                        //var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == modelSales.BranchID).ToList();
                        //List<tbl_Stock> existingStock = lstStock.ToList();
                        //foreach (var item in modelStockLog)
                        //{
                        //    var rStockOut = item.StockOut ?? 0;
                        //    var rowCount = 0;
                        //    var stockBatchID = 0;
                        //    var CostPrice = 0m;
                        //    decimal stLogQty = 0;

                        //    var VehID = db.tbl_Product.Where(x => x.ProductID == item.ProductID).Select(x => x.VehicleCodeID).FirstOrDefault();

                        //    if (VehID != 1)
                        //    {
                        //        #region FIFO 
                        //        var stockBatch = db.GetProductFIFO(item.ProductID, item.StockOut, item.BranchID).ToList();

                        //        while (rStockOut > 0)
                        //        {
                        //            if (stockBatch.Count > 0 && rowCount < stockBatch.Count)
                        //            {
                        //                var StockBatch = stockBatch[rowCount];
                        //                var sbRow = db.tbl_StockBatch.Where(b => b.StockBatchID == StockBatch.StockBatchID).FirstOrDefault();
                        //                var Order = db.tbl_StockLog.Where(sl => sl.OrderTypeID == 1 && sl.StockBatchID == sbRow.StockBatchID).FirstOrDefault();

                        //                if (rStockOut > StockBatch.Qty)
                        //                {
                        //                    stLogQty = sbRow.Qty;
                        //                    COGS += sbRow.Qty * sbRow.CostPrice;
                        //                    rStockOut -= sbRow.Qty;
                        //                    sbRow.Qty = 0;
                        //                    db.Entry(sbRow).State = EntityState.Modified;

                        //                }
                        //                else if (rStockOut < StockBatch.Qty)
                        //                {
                        //                    stLogQty = rStockOut;
                        //                    COGS += rStockOut * sbRow.CostPrice;
                        //                    sbRow.Qty -= rStockOut;
                        //                    db.Entry(sbRow).State = EntityState.Modified;
                        //                    rStockOut = 0;
                        //                }
                        //                else if (rStockOut == StockBatch.Qty)
                        //                {
                        //                    stLogQty = rStockOut;
                        //                    COGS += rStockOut * sbRow.CostPrice;
                        //                    sbRow.Qty = 0;
                        //                    db.Entry(sbRow).State = EntityState.Modified;
                        //                    rStockOut = 0;
                        //                }
                        //                stockBatchID = sbRow.StockBatchID;
                        //                CostPrice = sbRow.CostPrice;
                        //                if (Order != null)
                        //                {
                        //                    var PurchaseOrder = db.tbl_PurchaseOrder.Where(po => po.OrderID == Order.OrderID).FirstOrDefault();
                        //                    PurchaseOrder.Can_Modify = false;
                        //                    db.Entry(PurchaseOrder).State = EntityState.Modified;
                        //                }

                        //                db.SaveChanges();
                        //            }
                        //            else
                        //            {
                        //                stLogQty = rStockOut;
                        //                rStockOut = 0;
                        //            }
                        //            if (stLogQty > 0)
                        //            {
                        //                tbl_StockLog stLog = new tbl_StockLog();
                        //                stLog.AccountID = item.AccountID;
                        //                stLog.StockBatchID = stockBatchID;
                        //                stLog.AddBy = modelSales.AddBy;
                        //                stLog.AddOn = DateTime.UtcNow.AddHours(5);
                        //                stLog.BranchID = branchId;
                        //                stLog.CostPrice = CostPrice;
                        //                stLog.InvoiceDate = modelSales.SalesDate;
                        //                stLog.IsActive = true;
                        //                stLog.IsMinor = item.IsMinor;
                        //                stLog.IsOpen = item.IsOpen;
                        //                stLog.IsPack = item.IsPack;
                        //                stLog.LevelID = item.LevelID;
                        //                stLog.MinorDivisor = item.MinorDivisor;
                        //                stLog.OrderID = iOrderID;
                        //                stLog.OrderTypeID = item.OrderTypeID;
                        //                stLog.OutReference = item.OutReference;
                        //                stLog.OutReferenceID = item.OutReferenceID;
                        //                stLog.ProductID = item.ProductID;
                        //                stLog.SalePrice = item.SalePrice;
                        //                stLog.StockIN = 0;
                        //                stLog.ReturnedQty = 0;
                        //                stLog.StockOut = stLogQty;
                        //                stLog.UnitCode = item.UnitCode;
                        //                stLog.UnitID = item.UnitID;
                        //                stLog.UnitPerCarton = item.UnitPerCarton;
                        //                stLog.UserReferenceID = item.UserReferenceID;
                        //                db.tbl_StockLog.Add(stLog);
                        //                rowCount += 1;
                        //            }
                        //        }

                        //        #endregion
                        //    }

                        //    item.InvoiceDate = modelSales.SalesDate;
                        //    item.OrderID = iOrderID;
                        //    item.AddOn = DateTime.UtcNow.AddHours(5);
                        //    item.AddBy = modelSales.AddBy;
                        //    item.BranchID = branchId;
                        //    lstProdIds.Add(item.ProductID);


                        //    #region Update CostPrice in Stock 

                        //    decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.CostPrice * p.Qty) ?? 0m);
                        //    int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.Qty) ?? 0m);
                        //    decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;

                        //    #endregion

                        //    if (VehID != 1)
                        //    {
                        //        //  if already exists stock
                        //        var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
                        //        if (row != null)
                        //        {
                        //            if (avgCost > 0)
                        //            { row.CostPrice = avgCost; }
                        //            row.Qty -= Convert.ToInt32(item.StockOut);
                        //            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                        //            row.BranchID = item.BranchID;
                        //            row.Location = item.Location;
                        //            db.Entry(row).State = EntityState.Modified;
                        //        }
                        //        else
                        //        {
                        //            // if new item is not In Stock and User is Selling (As Discussed with Boss) 
                        //            tbl_Stock newStock = new tbl_Stock();
                        //            newStock.ProductID = item.ProductID;
                        //            newStock.Qty = Convert.ToDecimal(item.StockOut * (-1));
                        //            newStock.SalePrice = item.SalePrice;
                        //            newStock.Addon = DateTime.UtcNow.AddHours(5);
                        //            newStock.Location = item.Location;
                        //            newStock.BranchID = item.BranchID;
                        //            newStock.CostPrice = item.CostPrice;
                        //            newStock.OnMove = 0;
                        //            db.tbl_Stock.Add(newStock);
                        //        }

                        //    }


                        //}

                        #endregion

                        ///////////////////////////

                        if (challanIDs != null)
                        {
                            foreach (var item in challanIDs)
                            {

                                var getCOGS = db.tbl_Challan.Where(x => x.ChallanID == item).Select(x => x.COGS).FirstOrDefault();
                                if (getCOGS > 0)
                                {
                                    COGS += getCOGS;
                                }

                                tbl_ChallanIDs ch = new tbl_ChallanIDs();

                                ch.OrderID = Convert.ToInt32(iOrderID); // Sale Invoice OrderID
                                ch.ChallanID = item;
                                ch.JOID = db.tbl_Challan.Where(x=>x.ChallanID == item).Select(x=>x.JOID).FirstOrDefault()??0;
                                ch.AddOn = Helper.PST();
                                ch.isDeleted = false;

                                db.tbl_ChallanIDs.Add(ch);
                            }

                            var chs = db.tbl_Challan.Where(x => challanIDs.Contains(x.ChallanID)).ToList();
                            if (chs.Count > 0)
                            {
                                chs.ForEach(x => { x.ChallanStatusID = 2; });
                            }

                            db.SaveChanges();
                        }

                        ///

                        #region Accounts Entry

                        if (sPaymentStatus == "Paid")
                        {
                            // Post dated Cheque                     
                            //if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
                            //{
                            //    //pdc Entry
                            //    db.insertSOGJPDCPaidEntry(iPaymentAccountID, iOrderID, COGS);
                            //}
                            //else
                            //{
                                db.insertSOGJEntryPaid(iPaymentAccountID, iOrderID, COGS);
                            //}
                        }
                        else if (sPaymentStatus == "Partial Paid")
                        {
                            //if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
                            //{
                            //    //pdc Entry
                            //    db.insertSOGJPDCPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
                            //}
                            //else
                            //{
                                db.insertSOGJEntryPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
                            //}
                        }
                        else if (sPaymentStatus == "UnPaid")
                        {
                            //db.insertSOGJEntryUnpaid(iOrderID, COGS);
                            db.insertSOGJEntryUnpaid_New(iOrderID, COGS);
                        }
                        if (modelSales.SalePersonAccID != null && modelSales.SalePersonCommissionAmount > 0)
                        {
                            db.insertSalePersonCommissionGJEntryUnpaid(modelSales.SalePersonAccID, modelSales.SalePersonCommissionAmount, iOrderID);

                        }
                        #endregion

                        db.SaveChanges();
                    }

                    transaction.Commit();
                    result = iOrderID.ToString();
                    UserActions.MapActions(Convert.ToInt32(UserID), "Created Invoice #: " + modelSales.InvoiceNo.ToString() + " with OrderID: " + iOrderID.ToString());

                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    string message = "";
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                            raise = new InvalidOperationException(message, raise);
                        }
                    }

                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = Helper.PST();
                    err.vErrorMsg = message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return "";
                }
                catch (Exception ex)
                {
                    while (ex.InnerException != null) { ex = ex.InnerException; }
                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = Helper.PST();
                    err.vErrorMsg = ex.Message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return ex.Message;
                }
            }
        }


        public object editSales(tbl_SalesOrder modelSales, List<Models.DTO.StockLog> modelStockLog, bool? isQuote, int? QuoteOrderID, int? bankAccId, int branchId, int? OrderTypeID, List<int> challanIDs, bool fsc_Cert, bool grs_Cert, bool oeko_tex_Cert, int UserID)
        {
            dbPOS db = new dbPOS();
            string result = "";
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {

                    var invoice = db.tbl_SalesOrder.Where(x => x.OrderID == modelSales.OrderID && x.IsDeleted != true).FirstOrDefault();

                    if (invoice != null)
                    {
                       // DeleteOrders deleteInvoice = new DeleteOrders();
                       // int deleteStatus = deleteInvoice.DeleteSaleInvoice(modelSales.OrderID, Convert.ToInt32(UserID));
                       
                        #region delete Sale Invoice 
                        int c = 0;
                        
                            if (invoice.tbl_SaleDetails != null && invoice.tbl_SaleDetails.Count > 0)
                            {
                                foreach (var sd in invoice.tbl_SaleDetails)
                                {
                                    sd.IsDeleted = true;
                                    db.Entry(sd).State = EntityState.Modified;
                                }
                                //Parallel.ForEach(invoice.tbl_SaleDetails, sd =>
                                // {
                                //     sd.IsDeleted = true;
                                //     db.Entry(sd).State = EntityState.Modified;
                                // });
                                c += db.SaveChanges();
                            }

                            var chIDs = db.tbl_ChallanIDs.Where(x => x.OrderID == modelSales.OrderID && x.isDeleted != true).ToList();
                            if (chIDs != null && chIDs.Count > 0)
                            {
                                var existingChallanIDs = chIDs.Select(x => x.ChallanID).ToList();
                                var challans = db.tbl_Challan.Where(x => existingChallanIDs.Contains(x.ChallanID) && x.IsDeleted != true).ToList();

                                if (challans != null && challans.Count > 0)
                                {
                                    foreach (var ch in challans)
                                    {
                                        ch.ChallanStatusID = 1;
                                        db.Entry(ch).State = EntityState.Modified;
                                    }
                                    //Parallel.ForEach(challans, ch =>
                                    // {
                                    //     ch.ChallanStatusID = 1; db.Entry(ch).State = EntityState.Modified;
                                    // });

                                    c += db.SaveChanges();
                                }


                                foreach (var chid in chIDs)
                                {
                                    chid.isDeleted = true;
                                    db.Entry(chid).State = EntityState.Modified;
                                }
                                //Parallel.ForEach(chIDs, chid =>
                                //{
                                //    chid.isDeleted = true; db.Entry(chid).State = EntityState.Modified;
                                //});

                                c += db.SaveChanges();
                            }

                            #region Delete AccountEntries
                            db.DeleteSaleInvoiceIDWise(modelSales.OrderID);
                            #endregion

                            invoice.IsDeleted = true;
                            invoice.DeletedOn = Helper.PST();
                            db.Entry(invoice).State = EntityState.Modified;
                            c += db.SaveChanges();
                            if (c > 0)
                            {
                                //transaction.Commit();
                                UserActions.MapActions(Convert.ToInt32(UserID), "Deleted Invoice #: " + invoice.InvoiceNo.ToString());
                                //return c;
                            }


                            #region Edit Invoice

                            List<int> PIDs = modelSales.tbl_SaleDetails.Select(x => x.ProductID).ToList();

                            if (modelSales.tbl_SaleDetails.Any(x => x.Qty <= 0))
                            {
                                transaction.Rollback();
                                return -100;
                            }
                            db.Configuration.ValidateOnSaveEnabled = false;
                            long? PrevCode = db.tbl_JEntry.OrderByDescending(x => x.CodeInt).Select(x => x.CodeInt).FirstOrDefault();
                            long NewCode = 0;
                            string NewCodeSt;


                            if (PrevCode != null)
                            {
                                NewCode = Convert.ToInt64(PrevCode) + 1;
                                NewCodeSt = ("SL-" + NewCode).ToString();
                            }
                            else
                            {
                                NewCode = 9501;
                                NewCodeSt = ("SL-" + NewCode).ToString();
                            }

                            if(modelSales.SalesDate == null)
                        {
                            modelSales.SalesDate = invoice.SalesDate;
                        }
                        if (modelSales.PaymentStatus == "UnPaid")
                            {
                                modelSales.AmountPaid = 0;
                                modelSales.IsPaid = false;
                            }
                            else if (modelSales.PaymentStatus == "Partial Paid")
                            {
                                modelSales.IsPaid = false;
                            }
                            else if (modelSales.PaymentStatus == "Paid")
                            {
                                modelSales.IsPaid = true;
                            }

                            const char chkMark = (char)(0X2713);

                            if (fsc_Cert)
                            {
                                var cert = db.tbl_Certificates.Where(x => x.ID == 3).Select(x => new { Name = x.Certificate + " " + x.CertificateInfo }).FirstOrDefault();
                                if (cert != null && !string.IsNullOrWhiteSpace(cert.Name))
                                {
                                    modelSales.FSC_Cert = chkMark + " " + cert.Name;
                                }
                            }

                            if (grs_Cert)
                            {
                                var cert = db.tbl_Certificates.Where(x => x.ID == 4).Select(x => new { Name = x.Certificate + " " + x.CertificateInfo }).FirstOrDefault();
                                if (cert != null && !string.IsNullOrWhiteSpace(cert.Name))
                                {
                                    modelSales.GRS_Cert = chkMark + " " + cert.Name;
                                }
                            }

                            if (oeko_tex_Cert)
                            {
                                var cert = db.tbl_Certificates.Where(x => x.ID == 5).Select(x => new { Name = x.Certificate + " " + x.CertificateInfo }).FirstOrDefault();
                                if (cert != null && !string.IsNullOrWhiteSpace(cert.Name))
                                {
                                    modelSales.OEKO_TEX_Cert = chkMark + " " + cert.Name;
                                }
                            }

                            modelSales.ReturnAmount = 0;
                            modelSales.TotalPaid = modelSales.AmountPaid;
                            string sPaymentStatus = modelSales.PaymentStatus;
                            int? iPaymentAccountID = null;

                        modelSales.ChequeDate = Helper.PST();

                            modelSales.BranchID = branchId;

                            modelSales.Comment = "edit invoice created OBO deleted OrderID:" + invoice.OrderID.ToString() + ", Inv #: " + invoice.InvoiceNo.ToString();
                            modelSales.AddOn = invoice.AddOn;
                            modelSales.UpdateOn = Helper.PST();
                            modelSales.UserID = HttpContext.Current.User.Identity.GetUserId();
                            modelSales.IsReturned = false;
                            foreach (var i in modelSales.tbl_SaleDetails)
                            {
                                i.PartNo = db.tbl_Product.Where(x => x.ProductID == i.ProductID).Select(x => x.PartNo).FirstOrDefault() ?? "-";
                                i.BranchID = modelSales.BranchID;
                                i.IsDeleted = false;
                                i.UpdateOn = Helper.PST();
                            }
                            if (modelSales.PaymentTypeID == 1)
                            {
                                modelSales.BankName = "";
                                iPaymentAccountID = (int)(modelSales.PaymentTypeID);//  set it from Payment Type ddl 
                            }
                            else if (modelSales.PaymentTypeID > 1)
                            {
                                iPaymentAccountID = bankAccId;
                            }
                            if (modelSales.PaymentTypeID == 4)
                            {
                                modelSales.BankName = "";
                                iPaymentAccountID = 109;//  set it from Payment Type ddl 
                            }
                            if (modelSales.PaymentTypeID == 5)
                            {
                                //modelSales.BankName = "CreditCard";
                                iPaymentAccountID = 110;//  set it from Payment Type ddl 
                            }

                            modelSales.CompanyIDLogo = 1;
                            var getInvoiceType = db.tbl_Customer.Where(x => x.AccountID == modelSales.AccountID).Select(x => x.InvoiceType).FirstOrDefault();

                            modelSales.SOID = invoice.SOID;// Convert.ToInt32(GetInvoiceNoCustomerTypeWise(9001, getInvoiceType ?? 0));
                            if (getInvoiceType == null)
                            {
                                modelSales.Barcode = invoice.Barcode;// "SOI-" + branchId + "-" + modelSales.SOID;
                                modelSales.InvoiceNo = invoice.InvoiceNo;// "SOI-" + modelSales.SOID;
                            }
                            else
                            {
                                if (getInvoiceType == 1) // Jamaly
                                {
                                    modelSales.Barcode = invoice.Barcode;// "SOI-" + branchId + "-" + modelSales.SOID;
                                    modelSales.InvoiceNo = invoice.InvoiceNo;// "SOI-" + modelSales.SOID;
                                    modelSales.O_Serial = modelSales.SOID;
                                }
                                else if (getInvoiceType == 2) // Jemely
                                {
                                    modelSales.Barcode = invoice.Barcode;// "SOI-" + branchId + "-" + modelSales.SOID;
                                    modelSales.InvoiceNo = invoice.InvoiceNo;// "SOI-" + modelSales.SOID;
                                    modelSales.UO_Serial = modelSales.SOID;
                                }

                            }



                            db.tbl_SalesOrder.Add(modelSales);
                            c += db.SaveChanges();

                            int iOrderID = modelSales.OrderID;
                            decimal dAmountPaid = 0;
                            decimal COGS = 0;
                            if (modelSales.AmountPaid != null || modelSales.AmountPaid > 0)
                            {
                                dAmountPaid = (decimal)(modelSales.AmountPaid);
                            }

                            if (c >= 1 && modelStockLog != null)
                            {

                                if (challanIDs != null)
                                {
                                    foreach (var item in challanIDs)
                                    {

                                        var getCOGS = db.tbl_Challan.Where(x => x.ChallanID == item).Select(x => x.COGS).FirstOrDefault();
                                        if (getCOGS > 0)
                                        {
                                            COGS += getCOGS;
                                        }

                                        tbl_ChallanIDs ch = new tbl_ChallanIDs();

                                        ch.OrderID = Convert.ToInt32(iOrderID); // Sale Invoice OrderID
                                        ch.ChallanID = item;
                                        ch.AddOn = Helper.PST();
                                        ch.isDeleted = false;

                                        db.tbl_ChallanIDs.Add(ch);
                                    }

                                    var chs = db.tbl_Challan.Where(x => challanIDs.Contains(x.ChallanID)).ToList();
                                    if (chs.Count > 0)
                                    {
                                        chs.ForEach(x => { x.ChallanStatusID = 2; });
                                    }

                                    c += db.SaveChanges();
                                }

                                ///

                                #region Accounts Entry

                                if (sPaymentStatus == "Paid")
                                {
                                    // Post dated Cheque                     
                                    //if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
                                    //{
                                    //    //pdc Entry
                                    //    db.insertSOGJPDCPaidEntry(iPaymentAccountID, iOrderID, COGS);
                                    //}
                                    //else
                                    //{
                                        db.insertSOGJEntryPaid(iPaymentAccountID, iOrderID, COGS);
                                    //}
                                }
                                else if (sPaymentStatus == "Partial Paid")
                                {
                                    //if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
                                    //{
                                    //    //pdc Entry
                                    //    db.insertSOGJPDCPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
                                    //}
                                    //else
                                    //{
                                        db.insertSOGJEntryPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
                                    //}
                                }
                                else if (sPaymentStatus == "UnPaid")
                                {
                                    db.insertSOGJEntryUnpaid(iOrderID, COGS);
                                }
                                if (modelSales.SalePersonAccID != null && modelSales.SalePersonCommissionAmount > 0)
                                {
                                    db.insertSalePersonCommissionGJEntryUnpaid(modelSales.SalePersonAccID, modelSales.SalePersonCommissionAmount, iOrderID);

                                }
                                #endregion

                                c+=db.SaveChanges();
                            }


                            if (c > 1)
                            {
                                transaction.Commit();
                                result = iOrderID.ToString();

                                UserActions.MapActions(Convert.ToInt32(UserID), "Edited Invoice #: " + invoice.InvoiceNo.ToString() + " with new OrderID: "+iOrderID.ToString());
                                //return result;
                            }
                           
                            #endregion

                       
                        #endregion
                    }

                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    string message = "";
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                            raise = new InvalidOperationException(message, raise);
                        }
                    }

                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = ex.Message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return ex.Message;
                }
            }
        }

        //public object createSales(tbl_SalesOrder modelSales, List<Models.DTO.StockLog> modelStockLog, bool? isQuote, int? QuoteOrderID, int? bankAccId, int branchId, int? OrderTypeID)
        //{
        //    dbPOS db = new dbPOS();
        //    string result = "";
        //    using (var transaction = db.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            db.Configuration.ValidateOnSaveEnabled = false;
        //            long? PrevCode = db.tbl_JEntry.OrderByDescending(x => x.CodeInt).Select(x => x.CodeInt).FirstOrDefault();
        //            long NewCode = 0;
        //            string NewCodeSt;
        //            if (PrevCode != null)
        //            {
        //                NewCode = Convert.ToInt64(PrevCode) + 1;
        //                NewCodeSt = ("SL-" + NewCode).ToString();
        //            }
        //            else
        //            {
        //                NewCode = 950001;
        //                NewCodeSt = ("SL-" + NewCode).ToString();
        //            }

        //            if (modelSales.PaymentStatus == "UnPaid")
        //            {
        //                modelSales.AmountPaid = 0;
        //                modelSales.IsPaid = false;
        //            }
        //            else if (modelSales.PaymentStatus == "Partial Paid")
        //            {
        //                modelSales.IsPaid = false;
        //            }
        //            else if (modelSales.PaymentStatus == "Paid")
        //            {
        //                modelSales.IsPaid = true;
        //            }

        //            modelSales.ReturnAmount = 0;
        //            modelSales.TotalPaid = modelSales.AmountPaid;
        //            string sPaymentStatus = modelSales.PaymentStatus;
        //            int? iPaymentAccountID = null;
        //            modelSales.BranchID = branchId;
        //            modelSales.AddOn = DateTime.UtcNow.AddHours(5);
        //            modelSales.UserID = HttpContext.Current.User.Identity.GetUserId();
        //            modelSales.IsReturned = false;

        //            if (modelSales.PaymentTypeID == 1)
        //            {
        //                modelSales.BankName = "";
        //                iPaymentAccountID = (int)(modelSales.PaymentTypeID);//  set it from Payment Type ddl 
        //            }
        //            else if (modelSales.PaymentTypeID > 1)
        //            {
        //                iPaymentAccountID = bankAccId;
        //            }
        //            if (modelSales.PaymentTypeID == 4)
        //            {
        //                modelSales.BankName = "";
        //                iPaymentAccountID = 109;//  set it from Payment Type ddl 
        //            }
        //            if (modelSales.PaymentTypeID == 5)
        //            {
        //                //modelSales.BankName = "CreditCard";
        //                iPaymentAccountID = 110;//  set it from Payment Type ddl 
        //            }

        //            modelSales.SOID = GetInvoiceNo(modelSales.SOID, modelSales.BranchID);
        //            modelSales.CompanyIDLogo = 1;
        //            modelSales.Barcode = "SO-" + branchId + "-" + modelSales.SOID;
        //            db.tbl_SalesOrder.Add(modelSales);
        //            //Mapper.CreateMap<Models.DTO.Sales, tbl_SalesOrder>();
        //            //Mapper.CreateMap<Models.DTO.SaleDetails, tbl_SaleDetails>();
        //            //var details = Mapper.Map<ICollection<Models.DTO.SaleDetails>, ICollection<tbl_SaleDetails>>(modelSales.SaleDetails);
        //            //var master = Mapper.Map<Models.DTO.Sales, tbl_SalesOrder>(modelSales);
        //            //master.tbl_SaleDetails = details;
        //            //db.tbl_SalesOrder.Add(master);
        //            int soId = db.SaveChanges();

        //            int iOrderID = modelSales.OrderID;
        //            decimal dAmountPaid = 0;
        //            decimal COGS = 0; ;
        //            if (modelSales.AmountPaid != null || modelSales.AmountPaid > 0)
        //            {
        //                dAmountPaid = (decimal)(modelSales.AmountPaid);
        //            }

        //            if (soId >= 1 && modelStockLog != null)
        //            {
        //                List<int> lstProdIds = modelSales.tbl_SaleDetails.Select(p => p.ProductID).ToList();
        //                var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == modelSales.BranchID).ToList();
        //                List<tbl_Stock> existingStock = lstStock.ToList();
        //                foreach (var item in modelStockLog)
        //                {
        //                    var rStockOut = item.StockOut ?? 0;
        //                    var rowCount = 0;
        //                    var stockBatchID = 0;
        //                    var CostPrice = 0m;
        //                    decimal stLogQty = 0;

        //                    var VehID = db.tbl_Product.Where(x => x.ProductID == item.ProductID).Select(x => x.VehicleCodeID).FirstOrDefault();

        //                    if (VehID != 1)
        //                    {
        //                        #region FIFO 
        //                        var stockBatch = db.GetProductFIFO(item.ProductID, item.StockOut, item.BranchID).ToList();

        //                        while (rStockOut > 0)
        //                        {
        //                            if (stockBatch.Count > 0 && rowCount < stockBatch.Count)
        //                            {
        //                                var StockBatch = stockBatch[rowCount];
        //                                var sbRow = db.tbl_StockBatch.Where(b => b.StockBatchID == StockBatch.StockBatchID).FirstOrDefault();
        //                                var Order = db.tbl_StockLog.Where(sl => sl.OrderTypeID == 1 && sl.StockBatchID == sbRow.StockBatchID).FirstOrDefault();

        //                                if (rStockOut > StockBatch.Qty)
        //                                {
        //                                    stLogQty = sbRow.Qty;
        //                                    COGS += sbRow.Qty * sbRow.CostPrice;
        //                                    rStockOut -= sbRow.Qty;
        //                                    sbRow.Qty = 0;
        //                                    db.Entry(sbRow).State = EntityState.Modified;

        //                                }
        //                                else if (rStockOut < StockBatch.Qty)
        //                                {
        //                                    stLogQty = rStockOut;
        //                                    COGS += rStockOut * sbRow.CostPrice;
        //                                    sbRow.Qty -= rStockOut;
        //                                    db.Entry(sbRow).State = EntityState.Modified;
        //                                    rStockOut = 0;
        //                                }
        //                                else if (rStockOut == StockBatch.Qty)
        //                                {
        //                                    stLogQty = rStockOut;
        //                                    COGS += rStockOut * sbRow.CostPrice;
        //                                    sbRow.Qty = 0;
        //                                    db.Entry(sbRow).State = EntityState.Modified;
        //                                    rStockOut = 0;
        //                                }
        //                                stockBatchID = sbRow.StockBatchID;
        //                                CostPrice = sbRow.CostPrice;
        //                                if (Order != null)
        //                                {
        //                                    var PurchaseOrder = db.tbl_PurchaseOrder.Where(po => po.OrderID == Order.OrderID).FirstOrDefault();
        //                                    PurchaseOrder.Can_Modify = false;
        //                                    db.Entry(PurchaseOrder).State = EntityState.Modified;
        //                                }

        //                                db.SaveChanges();
        //                            }
        //                            else
        //                            {
        //                                stLogQty = rStockOut;
        //                                rStockOut = 0;
        //                            }
        //                            if (stLogQty > 0)
        //                            {
        //                                tbl_StockLog stLog = new tbl_StockLog();
        //                                stLog.AccountID = item.AccountID;
        //                                stLog.StockBatchID = stockBatchID;
        //                                stLog.AddBy = modelSales.AddBy;
        //                                stLog.AddOn = DateTime.UtcNow.AddHours(5);
        //                                stLog.BranchID = branchId;
        //                                stLog.CostPrice = CostPrice;
        //                                stLog.InvoiceDate = modelSales.SalesDate;
        //                                stLog.IsActive = true;
        //                                stLog.IsMinor = item.IsMinor;
        //                                stLog.IsOpen = item.IsOpen;
        //                                stLog.IsPack = item.IsPack;
        //                                stLog.LevelID = item.LevelID;
        //                                stLog.MinorDivisor = item.MinorDivisor;
        //                                stLog.OrderID = iOrderID;
        //                                stLog.OrderTypeID = item.OrderTypeID;
        //                                stLog.OutReference = item.OutReference;
        //                                stLog.OutReferenceID = item.OutReferenceID;
        //                                stLog.ProductID = item.ProductID;
        //                                stLog.SalePrice = item.SalePrice;
        //                                stLog.StockIN = 0;
        //                                stLog.ReturnedQty = 0;
        //                                stLog.StockOut = stLogQty;
        //                                stLog.UnitCode = item.UnitCode;
        //                                stLog.UnitID = item.UnitID;
        //                                stLog.UnitPerCarton = item.UnitPerCarton;
        //                                stLog.UserReferenceID = item.UserReferenceID;
        //                                db.tbl_StockLog.Add(stLog);
        //                                rowCount += 1;
        //                            }
        //                        }

        //                        #endregion
        //                    }




        //                    item.InvoiceDate = modelSales.SalesDate;
        //                    item.OrderID = iOrderID;
        //                    item.AddOn = DateTime.UtcNow.AddHours(5);
        //                    item.AddBy = modelSales.AddBy;
        //                    item.BranchID = branchId;
        //                    lstProdIds.Add(item.ProductID);


        //                    #region Update CostPrice in Stock 

        //                    decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.CostPrice * p.Qty) ?? 0m);
        //                    int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.Qty) ?? 0m);
        //                    decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;

        //                    #endregion

        //                    if (VehID != 1)
        //                    {
        //                        //  if already exists stock
        //                        var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
        //                        if (row != null)
        //                        {
        //                            if (avgCost > 0)
        //                            { row.CostPrice = avgCost; }
        //                            row.Qty -= Convert.ToInt32(item.StockOut);
        //                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
        //                            row.BranchID = item.BranchID;
        //                            row.Location = item.Location;
        //                            db.Entry(row).State = EntityState.Modified;
        //                        }
        //                        else
        //                        {
        //                            // if new item is not In Stock and User is Selling (As Discussed with Boss) 
        //                            tbl_Stock newStock = new tbl_Stock();
        //                            newStock.ProductID = item.ProductID;
        //                            newStock.Qty = Convert.ToDecimal(item.StockOut * (-1));
        //                            newStock.SalePrice = item.SalePrice;
        //                            newStock.Addon = DateTime.UtcNow.AddHours(5);
        //                            newStock.Location = item.Location;
        //                            newStock.BranchID = item.BranchID;
        //                            newStock.CostPrice = item.CostPrice;
        //                            newStock.OnMove = 0;
        //                            db.tbl_Stock.Add(newStock);
        //                        }

        //                    }


        //                }

        //                #region Accounts Entry

        //                if (sPaymentStatus == "Paid")
        //                {
        //                    // Post dated Cheque                     
        //                    if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
        //                    {
        //                        //pdc Entry
        //                        db.insertSOGJPDCPaidEntry(iPaymentAccountID, iOrderID, COGS);
        //                    }
        //                    else
        //                    {
        //                        db.insertSOGJEntryPaid(iPaymentAccountID, iOrderID, COGS);
        //                    }
        //                }
        //                else if (sPaymentStatus == "Partial Paid")
        //                {
        //                    if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
        //                    {
        //                        //pdc Entry
        //                        db.insertSOGJPDCPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
        //                    }
        //                    else
        //                    {
        //                        db.insertSOGJEntryPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
        //                    }
        //                }
        //                else if (sPaymentStatus == "UnPaid")
        //                {
        //                    db.insertSOGJEntryUnpaid(iOrderID, COGS);
        //                }

        //                #endregion

        //                db.SaveChanges();
        //            }

        //            transaction.Commit();
        //            result = iOrderID.ToString();
        //            return result;
        //        }
        //        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
        //        {
        //            string message = "";
        //            Exception raise = dbEx;
        //            foreach (var validationErrors in dbEx.EntityValidationErrors)
        //            {
        //                foreach (var validationError in validationErrors.ValidationErrors)
        //                {
        //                    message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
        //                    raise = new InvalidOperationException(message, raise);
        //                }
        //            }

        //            transaction.Rollback();
        //            ErrorTracer err = new ErrorTracer();
        //            err.dErrorDate = DateTime.UtcNow.AddHours(5);
        //            err.vErrorMsg = message;
        //            db.ErrorTracers.Add(err);
        //            db.SaveChanges();
        //            return "";
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            ErrorTracer err = new ErrorTracer();
        //            err.dErrorDate = DateTime.UtcNow.AddHours(5);
        //            err.vErrorMsg = ex.Message;
        //            db.ErrorTracers.Add(err);
        //            db.SaveChanges();
        //            return ex.Message;
        //        }
        //    }
        //}

        public object SaveOffsetSales(Sales modelSales, List<Models.DTO.StockLog> modelStockLog, bool? isQuote, int? QuoteOrderID, int? bankAccId, int branchId, int? OrderTypeID)
        {
            dbPOS db = new dbPOS();
            string result = "";
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    long? PrevCode = db.tbl_JEntry.OrderByDescending(x => x.CodeInt).Select(x => x.CodeInt).FirstOrDefault();
                    long NewCode = 0;
                    string NewCodeSt;
                    string itemDescription = modelSales.SaleDetails.Select(u => u.ItemDescription).FirstOrDefault();
                    if (db.tbl_SaleDetails.Any(x => x.ItemDescription.Equals(itemDescription)) && !string.IsNullOrWhiteSpace(itemDescription))
                    {
                        return -6;
                    }
                    if (PrevCode != null)
                    {
                        NewCode = Convert.ToInt64(PrevCode) + 1;
                        NewCodeSt = ("SO-" + NewCode).ToString();
                    }
                    else
                    {
                        NewCode = 901;
                        NewCodeSt = ("SO-" + NewCode).ToString();
                    }

                    if (modelSales.PaymentStatus == "UnPaid")
                    {
                        modelSales.AmountPaid = 0;
                        modelSales.IsPaid = false;
                    }
                    else if (modelSales.PaymentStatus == "Partial Paid")
                    {
                        modelSales.IsPaid = false;
                    }
                    else if (modelSales.PaymentStatus == "Paid")
                    {
                        modelSales.IsPaid = true;
                    }

                    modelSales.SalesDate = Helper.PST();
                    modelSales.IsOffsetOrder = true;
                    modelSales.ReturnAmount = 0;
                    modelSales.TotalPaid = modelSales.AmountPaid;
                    string sPaymentStatus = modelSales.PaymentStatus;
                    int? iPaymentAccountID = null;
                    modelSales.BranchID = branchId;
                    modelSales.AddOn = Helper.PST();
                    modelSales.UserID = HttpContext.Current.User.Identity.GetUserId();
                    modelSales.IsReturned = false;

                    if (modelSales.PaymentTypeID == 1)
                    {
                        modelSales.BankName = "";
                        iPaymentAccountID = (int)(modelSales.PaymentTypeID);//  set it from Payment Type ddl 
                    }
                    else if (modelSales.PaymentTypeID > 1)
                    {
                        iPaymentAccountID = bankAccId;
                    }
                    if (modelSales.PaymentTypeID == 4)
                    {
                        modelSales.BankName = "";
                        iPaymentAccountID = 109;//  set it from Payment Type ddl 
                    }
                    if (modelSales.PaymentTypeID == 5)
                    {
                        //modelSales.BankName = "CreditCard";
                        iPaymentAccountID = 110;//  set it from Payment Type ddl 
                    }

                    modelSales.SOID = GetInvoiceNo(modelSales.SOID, modelSales.BranchID);
                    modelSales.CompanyIDLogo = 1;
                    modelSales.Barcode = "SO-" + branchId + "-" + modelSales.SOID;
                    // db.tbl_SalesOrder.Add(modelSales);
                    Mapper.CreateMap<Models.DTO.Sales, tbl_SalesOrder>();
                    Mapper.CreateMap<Models.DTO.SaleDetails, tbl_SaleDetails>();
                    var details = Mapper.Map<ICollection<Models.DTO.SaleDetails>, ICollection<tbl_SaleDetails>>(modelSales.SaleDetails);
                    var master = Mapper.Map<Models.DTO.Sales, tbl_SalesOrder>(modelSales);
                    master.tbl_SaleDetails = details;
                    db.tbl_SalesOrder.Add(master);
                    int soId = db.SaveChanges();

                    int iOrderID = master.OrderID;
                    decimal dAmountPaid = 0;
                    decimal COGS = 0; ;
                    if (modelSales.AmountPaid != null || modelSales.AmountPaid > 0)
                    {
                        dAmountPaid = (decimal)(modelSales.AmountPaid);
                    }

                    //if (soId >= 1 && modelStockLog != null)
                    //{
                    //    List<int> lstProdIds = modelSales.SaleDetails.Select(p => p.ProductID).ToList();
                    //    var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == modelSales.BranchID).ToList();
                    //    List<tbl_Stock> existingStock = lstStock.ToList();
                    //    foreach (var item in modelStockLog)
                    //    {
                    //        var rStockOut = item.StockOut ?? 0;
                    //        var rowCount = 0;
                    //        var stockBatchID = 0;
                    //        var CostPrice = 0m;
                    //        decimal stLogQty = 0;

                    //        var VehID = db.tbl_Product.Where(x => x.ProductID == item.ProductID).Select(x => x.VehicleCodeID).FirstOrDefault();

                    //        if (VehID != 1)
                    //        {
                    //            #region FIFO 
                    //            var stockBatch = db.GetProductFIFO(item.ProductID, item.StockOut, item.BranchID).ToList();

                    //            while (rStockOut > 0)
                    //            {
                    //                if (stockBatch.Count > 0 && rowCount < stockBatch.Count)
                    //                {
                    //                    var StockBatch = stockBatch[rowCount];
                    //                    var sbRow = db.tbl_StockBatch.Where(b => b.StockBatchID == StockBatch.StockBatchID).FirstOrDefault();
                    //                    var Order = db.tbl_StockLog.Where(sl => sl.OrderTypeID == 1 && sl.StockBatchID == sbRow.StockBatchID).FirstOrDefault();

                    //                    if (rStockOut > StockBatch.Qty)
                    //                    {
                    //                        stLogQty = sbRow.Qty;
                    //                        COGS += sbRow.Qty * sbRow.CostPrice;
                    //                        rStockOut -= sbRow.Qty;
                    //                        sbRow.Qty = 0;
                    //                        db.Entry(sbRow).State = EntityState.Modified;

                    //                    }
                    //                    else if (rStockOut < StockBatch.Qty)
                    //                    {
                    //                        stLogQty = rStockOut;
                    //                        COGS += rStockOut * sbRow.CostPrice;
                    //                        sbRow.Qty -= rStockOut;
                    //                        db.Entry(sbRow).State = EntityState.Modified;
                    //                        rStockOut = 0;
                    //                    }
                    //                    else if (rStockOut == StockBatch.Qty)
                    //                    {
                    //                        stLogQty = rStockOut;
                    //                        COGS += rStockOut * sbRow.CostPrice;
                    //                        sbRow.Qty = 0;
                    //                        db.Entry(sbRow).State = EntityState.Modified;
                    //                        rStockOut = 0;
                    //                    }
                    //                    stockBatchID = sbRow.StockBatchID;
                    //                    CostPrice = sbRow.CostPrice;
                    //                    if (Order != null)
                    //                    {
                    //                        var PurchaseOrder = db.tbl_PurchaseOrder.Where(po => po.OrderID == Order.OrderID).FirstOrDefault();
                    //                        PurchaseOrder.Can_Modify = false;
                    //                        db.Entry(PurchaseOrder).State = EntityState.Modified;
                    //                    }

                    //                    db.SaveChanges();
                    //                }
                    //                else
                    //                {
                    //                    stLogQty = rStockOut;
                    //                    rStockOut = 0;
                    //                }
                    //                if (stLogQty > 0)
                    //                {
                    //                    tbl_StockLog stLog = new tbl_StockLog();
                    //                    stLog.AccountID = item.AccountID;
                    //                    stLog.StockBatchID = stockBatchID;
                    //                    stLog.AddBy = modelSales.AddBy;
                    //                    stLog.AddOn = DateTime.UtcNow.AddHours(5);
                    //                    stLog.BranchID = branchId;
                    //                    stLog.CostPrice = CostPrice;
                    //                    stLog.InvoiceDate = modelSales.SalesDate;
                    //                    stLog.IsActive = true;
                    //                    stLog.IsMinor = item.IsMinor;
                    //                    stLog.IsOpen = item.IsOpen;
                    //                    stLog.IsPack = item.IsPack;
                    //                    stLog.LevelID = item.LevelID;
                    //                    stLog.MinorDivisor = item.MinorDivisor;
                    //                    stLog.OrderID = iOrderID;
                    //                    stLog.OrderTypeID = item.OrderTypeID;
                    //                    stLog.OutReference = item.OutReference;
                    //                    stLog.OutReferenceID = item.OutReferenceID;
                    //                    stLog.ProductID = item.ProductID;
                    //                    stLog.SalePrice = item.SalePrice;
                    //                    stLog.StockIN = 0;
                    //                    stLog.ReturnedQty = 0;
                    //                    stLog.StockOut = stLogQty;
                    //                    stLog.UnitCode = item.UnitCode;
                    //                    stLog.UnitID = item.UnitID;
                    //                    stLog.UnitPerCarton = item.UnitPerCarton;
                    //                    stLog.UserReferenceID = item.UserReferenceID;
                    //                    db.tbl_StockLog.Add(stLog);
                    //                    rowCount += 1;
                    //                }
                    //            }

                    //            #endregion
                    //        }




                    //        item.InvoiceDate = modelSales.SalesDate;
                    //        item.OrderID = iOrderID;
                    //        item.AddOn = DateTime.UtcNow.AddHours(5);
                    //        item.AddBy = modelSales.AddBy;
                    //        item.BranchID = branchId;
                    //        lstProdIds.Add(item.ProductID);


                    //        #region Update CostPrice in Stock 

                    //        decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.CostPrice * p.Qty) ?? 0m);
                    //        int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.Qty) ?? 0m);
                    //        decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;

                    //        #endregion

                    //        if (VehID != 1)
                    //        {
                    //            //  if already exists stock
                    //            var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
                    //            if (row != null)
                    //            {
                    //                if (avgCost > 0)
                    //                { row.CostPrice = avgCost; }
                    //                row.Qty -= Convert.ToInt32(item.StockOut);
                    //                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                    //                row.BranchID = item.BranchID;
                    //                row.Location = item.Location;
                    //                db.Entry(row).State = EntityState.Modified;
                    //            }
                    //            else
                    //            {
                    //                // if new item is not In Stock and User is Selling (As Discussed with Boss) 
                    //                tbl_Stock newStock = new tbl_Stock();
                    //                newStock.ProductID = item.ProductID;
                    //                newStock.Qty = Convert.ToDecimal(item.StockOut * (-1));
                    //                newStock.SalePrice = item.SalePrice;
                    //                newStock.Addon = DateTime.UtcNow.AddHours(5);
                    //                newStock.Location = item.Location;
                    //                newStock.BranchID = item.BranchID;
                    //                newStock.CostPrice = item.CostPrice;
                    //                newStock.OnMove = 0;
                    //                db.tbl_Stock.Add(newStock);
                    //            }

                    //        }


                    //    }

                    //    #region Accounts Entry

                    //    if (sPaymentStatus == "Paid")
                    //    {
                    //        // Post dated Cheque                     
                    //        if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
                    //        {
                    //            //pdc Entry
                    //            db.insertSOGJPDCPaidEntry(iPaymentAccountID, iOrderID, COGS);
                    //        }
                    //        else
                    //        {
                    //            db.insertSOGJEntryPaid(iPaymentAccountID, iOrderID, COGS);
                    //        }
                    //    }
                    //    else if (sPaymentStatus == "Partial Paid")
                    //    {
                    //        if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
                    //        {
                    //            //pdc Entry
                    //            db.insertSOGJPDCPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
                    //        }
                    //        else
                    //        {
                    //            db.insertSOGJEntryPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
                    //        }
                    //    }
                    //    else if (sPaymentStatus == "UnPaid")
                    //    {
                    //        db.insertSOGJEntryUnpaid(iOrderID, COGS);
                    //    }

                    //    #endregion

                    //    db.SaveChanges();
                    //}

                    transaction.Commit();
                    result = iOrderID.ToString();
                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    string message = "";
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                            raise = new InvalidOperationException(message, raise);
                        }
                    }

                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = ex.Message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return ex.Message;
                }
            }
        }


        public object SaveOffsetClothSales(Sales modelSales, List<Models.DTO.StockLog> modelStockLog, bool? isQuote, int? QuoteOrderID, int? bankAccId, int branchId, int? OrderTypeID)
        {
            dbPOS db = new dbPOS();
            string result = "";
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    long? PrevCode = db.tbl_JEntry.OrderByDescending(x => x.CodeInt).Select(x => x.CodeInt).FirstOrDefault();
                    long NewCode = 0;
                    string NewCodeSt;
                    if (PrevCode != null)
                    {
                        NewCode = Convert.ToInt64(PrevCode) + 1;
                        NewCodeSt = ("SO-" + NewCode).ToString();
                    }
                    else
                    {
                        NewCode = 901;
                        NewCodeSt = ("SO-" + NewCode).ToString();
                    }

                    if (modelSales.PaymentStatus == "UnPaid")
                    {
                        modelSales.AmountPaid = 0;
                        modelSales.IsPaid = false;
                    }
                    else if (modelSales.PaymentStatus == "Partial Paid")
                    {
                        modelSales.IsPaid = false;
                    }
                    else if (modelSales.PaymentStatus == "Paid")
                    {
                        modelSales.IsPaid = true;
                    }

                    modelSales.SalesDate = Helper.PST(); 
                    modelSales.IsOffsetOrder = true;
                    modelSales.ReturnAmount = 0;
                    modelSales.TotalPaid = modelSales.AmountPaid;
                    string sPaymentStatus = modelSales.PaymentStatus;
                    int? iPaymentAccountID = null;
                    modelSales.BranchID = branchId;
                    modelSales.AddOn = Helper.PST();
                    modelSales.UserID = HttpContext.Current.User.Identity.GetUserId();
                    modelSales.IsReturned = false;

                    if (modelSales.PaymentTypeID == 1)
                    {
                        modelSales.BankName = "";
                        iPaymentAccountID = (int)(modelSales.PaymentTypeID);//  set it from Payment Type ddl 
                    }
                    else if (modelSales.PaymentTypeID > 1)
                    {
                        iPaymentAccountID = bankAccId;
                    }
                    if (modelSales.PaymentTypeID == 4)
                    {
                        modelSales.BankName = "";
                        iPaymentAccountID = 109;//  set it from Payment Type ddl 
                    }
                    if (modelSales.PaymentTypeID == 5)
                    {
                        //modelSales.BankName = "CreditCard";
                        iPaymentAccountID = 110;//  set it from Payment Type ddl 
                    }

                    modelSales.SOID = GetInvoiceNo(modelSales.SOID, modelSales.BranchID);
                    modelSales.CompanyIDLogo = 1;
                    modelSales.Barcode = "SO-" + branchId + "-" + modelSales.SOID;
                    // db.tbl_SalesOrder.Add(modelSales);
                    Mapper.CreateMap<Models.DTO.Sales, tbl_SalesOrder>();
                    Mapper.CreateMap<Models.DTO.SaleDetails, tbl_SaleDetails>();
                    var details = Mapper.Map<ICollection<Models.DTO.SaleDetails>, ICollection<tbl_SaleDetails>>(modelSales.SaleDetails);
                    var master = Mapper.Map<Models.DTO.Sales, tbl_SalesOrder>(modelSales);
                    master.tbl_SaleDetails = details;
                    db.tbl_SalesOrder.Add(master);
                    int soId = db.SaveChanges();

                    int iOrderID = master.OrderID;
                    decimal dAmountPaid = 0;
                    decimal COGS = 0; ;
                    if (modelSales.AmountPaid != null || modelSales.AmountPaid > 0)
                    {
                        dAmountPaid = (decimal)(modelSales.AmountPaid);
                    }


                    transaction.Commit();
                    result = iOrderID.ToString();
                    return result;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    string message = "";
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                            raise = new InvalidOperationException(message, raise);
                        }
                    }

                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = ex.Message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return ex.Message;
                }
            }
        }

        //for creating challan
        public object createOffsetSales(tbl_SalesOrder modelSales, List<Models.DTO.StockLog> modelStockLog, int? bankAccId, int? OrderTypeID, int? SaleOrderID)
        {
            dbPOS db = new dbPOS();
            string result = "";
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (modelSales.tbl_SaleDetails.All(x => x.Qty <= 0))
                    {
                        transaction.Rollback();
                        return -1;
                    }
                    if (modelSales.OrderID > 0)
                    {
                        int? iPaymentAccountID = null;
                        int branchId = 9001;
                        decimal TotalAmount = 0;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        var data = db.tbl_SaleDetails.Where(x => x.OrderID == modelSales.OrderID).ToList();
                        if (data != null && data.Count > 0 && modelSales.tbl_SaleDetails.Count > 0)
                        {

                            foreach (var i in modelSales.tbl_SaleDetails)
                            {
                                var getProd = data.Where(x => x.ProductID == i.ProductID && x.ReadyQty > 0).FirstOrDefault();
                                if (getProd != null)
                                {
                                    getProd.QtyDelivered += Convert.ToInt32(i.Qty);
                                    getProd.ReadyQty -= Convert.ToInt32(i.Qty);
                                    getProd.QtyBalanced = Convert.ToInt32(getProd.Qty - getProd.QtyDelivered);
                                    //TotalAmount += Convert.ToDecimal(i.Qty * i.SalePrice);
                                    db.Entry(getProd).State = EntityState.Modified;

                                    var pkgData = db.tbl_SOItemWisePackageDetails.Where(x => x.OrderID == modelSales.OrderID && x.ItemID == i.ProductID).ToList();
                                    if (pkgData != null)
                                    {
                                        foreach (var p in pkgData)
                                        {
                                            var update = db.tbl_SOItemWisePackageDetails.Where(x => x.ID == p.ID).FirstOrDefault();
                                            if (update != null)
                                            {
                                                update.DeliveredQty += update.DeliverQty;
                                                update.DeliverQty = 0;
                                                db.Entry(update).State = EntityState.Modified;
                                            }
                                        }
                                        db.SaveChanges();
                                    }
                                }

                            }

                            var order = db.tbl_SalesOrder.Where(x => x.OrderID == modelSales.OrderID).FirstOrDefault();

                            if (modelSales.PaymentTypeID == 1)
                            {
                                modelSales.BankName = "";
                                iPaymentAccountID = (int)(modelSales.PaymentTypeID);//  set it from Payment Type ddl 

                            }
                            else if (modelSales.PaymentTypeID > 1)
                            {
                                iPaymentAccountID = bankAccId ?? 0;
                            }
                            if (modelSales.PaymentTypeID == 4)
                            {
                                modelSales.BankName = "";
                                iPaymentAccountID = 109;//  set it from Payment Type ddl 
                            }
                            if (modelSales.PaymentTypeID == 5)
                            {
                                //modelSales.BankName = "CreditCard";
                                iPaymentAccountID = 110;//  set it from Payment Type ddl 
                            }
                            order.BankName = modelSales.BankName;
                            order.PaymentTypeID = iPaymentAccountID;
                            long? PrevCode = db.tbl_JEntry.OrderByDescending(x => x.CodeInt).Select(x => x.CodeInt).FirstOrDefault();
                            long NewCode = 0;

                            string NewCodeSt;
                            if (PrevCode != null)
                            {
                                NewCode = Convert.ToInt64(PrevCode) + 1;
                                NewCodeSt = ("SL-" + NewCode).ToString();
                            }
                            else
                            {
                                NewCode = 9501;
                                NewCodeSt = ("SL-" + NewCode).ToString();
                            }
                            order.PaymentStatus = modelSales.PaymentStatus;
                            //order.SOID = GetInvoiceNo(modelSales.SOID, modelSales.BranchID);
                            order.CompanyIDLogo = 1;
                            // order.Barcode = "SO-" + branchId + "-" + order.SOID;
                            order.BranchID = branchId;
                            order.UpdateOn = DateTime.UtcNow.AddHours(5);
                            order.UserID = HttpContext.Current.User.Identity.GetUserId();
                            order.IsReturned = false;
                            order.ReturnAmount = 0;
                            //order.TotalAmount = TotalAmount;
                            //order.AmountPaid = modelSales.AmountPaid;
                            //order.TotalPaid += modelSales.AmountPaid;
                            string sPaymentStatus = order.PaymentStatus;
                            db.Entry(order).State = EntityState.Modified;

                            int soId = db.SaveChanges();

                            int iOrderID = order.OrderID;
                            decimal dAmountPaid = 0;
                            decimal COGS = 0;

                            if (modelSales.AmountPaid != null || modelSales.AmountPaid > 0)
                            {
                                dAmountPaid = (decimal)(modelSales.AmountPaid);
                            }

                            if (soId >= 1 && modelStockLog != null)
                            {
                                List<int> lstProdIds = modelSales.tbl_SaleDetails.Select(p => p.ProductID).ToList();
                                var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == modelSales.BranchID).ToList();
                                List<tbl_Stock> existingStock = lstStock.ToList();
                                foreach (var item in modelStockLog)
                                {
                                    var rStockOut = item.StockOut ?? 0;
                                    var rowCount = 0;
                                    var stockBatchID = 0;
                                    var CostPrice = 0m;
                                    decimal stLogQty = 0;

                                    var VehID = db.tbl_Product.Where(x => x.ProductID == item.ProductID).Select(x => x.VehicleCodeID).FirstOrDefault();

                                    if (VehID != 1)
                                    {
                                        #region FIFO 
                                        var stockBatch = db.GetProductFIFO(item.ProductID, item.StockOut, item.BranchID).ToList();

                                        while (rStockOut > 0)
                                        {
                                            if (stockBatch.Count > 0 && rowCount < stockBatch.Count)
                                            {
                                                var StockBatch = stockBatch[rowCount];
                                                var sbRow = db.tbl_StockBatch.Where(b => b.StockBatchID == StockBatch.StockBatchID).FirstOrDefault();
                                                var Order = db.tbl_StockLog.Where(sl => sl.OrderTypeID == 1 && sl.StockBatchID == sbRow.StockBatchID).FirstOrDefault();

                                                if (rStockOut > StockBatch.Qty)
                                                {
                                                    stLogQty = sbRow.Qty;
                                                    COGS += sbRow.Qty * sbRow.CostPrice;
                                                    rStockOut -= sbRow.Qty;
                                                    sbRow.Qty = 0;
                                                    db.Entry(sbRow).State = EntityState.Modified;

                                                }
                                                else if (rStockOut < StockBatch.Qty)
                                                {
                                                    stLogQty = rStockOut;
                                                    COGS += rStockOut * sbRow.CostPrice;
                                                    sbRow.Qty -= rStockOut;
                                                    db.Entry(sbRow).State = EntityState.Modified;
                                                    rStockOut = 0;
                                                }
                                                else if (rStockOut == StockBatch.Qty)
                                                {
                                                    stLogQty = rStockOut;
                                                    COGS += rStockOut * sbRow.CostPrice;
                                                    sbRow.Qty = 0;
                                                    db.Entry(sbRow).State = EntityState.Modified;
                                                    rStockOut = 0;
                                                }
                                                stockBatchID = sbRow.StockBatchID;
                                                CostPrice = sbRow.CostPrice;
                                                if (Order != null)
                                                {
                                                    var PurchaseOrder = db.tbl_PurchaseOrder.Where(po => po.OrderID == Order.OrderID).FirstOrDefault();
                                                    PurchaseOrder.Can_Modify = false;
                                                    db.Entry(PurchaseOrder).State = EntityState.Modified;
                                                }

                                                db.SaveChanges();
                                            }
                                            else
                                            {
                                                stLogQty = rStockOut;
                                                rStockOut = 0;
                                            }
                                            if (stLogQty > 0)
                                            {
                                                tbl_StockLog stLog = new tbl_StockLog();
                                                stLog.AccountID = item.AccountID;
                                                stLog.StockBatchID = stockBatchID;
                                                stLog.AddBy = modelSales.AddBy;
                                                stLog.AddOn = Helper.PST();
                                                stLog.BranchID = branchId;
                                                stLog.CostPrice = CostPrice;
                                                stLog.InvoiceDate = modelSales.SalesDate;
                                                stLog.IsActive = true;
                                                stLog.IsMinor = item.IsMinor;
                                                stLog.IsOpen = item.IsOpen;
                                                stLog.IsPack = item.IsPack;
                                                stLog.LevelID = item.LevelID;
                                                stLog.MinorDivisor = item.MinorDivisor;
                                                stLog.OrderID = iOrderID;
                                                stLog.OrderTypeID = item.OrderTypeID;
                                                stLog.OutReference = item.OutReference;
                                                stLog.OutReferenceID = item.OutReferenceID;
                                                stLog.ProductID = item.ProductID;
                                                stLog.SalePrice = item.SalePrice;
                                                stLog.StockIN = 0;
                                                stLog.ReturnedQty = 0;
                                                stLog.StockOut = stLogQty;
                                                stLog.UnitCode = item.UnitCode;
                                                stLog.UnitID = item.UnitID;
                                                stLog.UnitPerCarton = item.UnitPerCarton;
                                                stLog.UserReferenceID = item.UserReferenceID;
                                                db.tbl_StockLog.Add(stLog);
                                                rowCount += 1;
                                            }
                                        }

                                        #endregion
                                    }




                                    item.InvoiceDate = modelSales.SalesDate;
                                    item.OrderID = iOrderID;
                                    item.AddOn = Helper.PST();
                                    item.AddBy = modelSales.AddBy;
                                    item.BranchID = branchId;
                                    lstProdIds.Add(item.ProductID);


                                    #region Update CostPrice in Stock 

                                    decimal costPOTotal = Convert.ToDecimal(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.CostPrice * p.Qty) ?? 0m);
                                    int qtyPO = Convert.ToInt32(db.tbl_StockBatch.Where(s => s.ProductID == item.ProductID && s.Qty > 0 && s.BranchID == branchId).Sum(p => (decimal?)p.Qty) ?? 0m);
                                    decimal avgCost = (costPOTotal > 0) && (qtyPO > 0) ? (costPOTotal / qtyPO) : 0m;

                                    #endregion

                                    if (VehID != 1)
                                    {
                                        //  if already exists stock
                                        var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
                                        if (row != null && (row.Qty - Convert.ToDecimal(item.StockOut) >= 0))
                                        {
                                            if (avgCost > 0)
                                            { row.CostPrice = avgCost; }
                                            row.Qty -= Convert.ToInt32(item.StockOut);
                                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                            row.BranchID = item.BranchID;
                                            row.Location = item.Location;
                                            db.Entry(row).State = EntityState.Modified;
                                        }
                                        else
                                        {
                                            transaction.Rollback();
                                            return -1;
                                            // if new item is not In Stock and User is Selling (As Discussed with Boss) 
                                            tbl_Stock newStock = new tbl_Stock();
                                            newStock.ProductID = item.ProductID;
                                            newStock.Qty = Convert.ToDecimal(item.StockOut * (-1));
                                            newStock.SalePrice = item.SalePrice;
                                            newStock.Addon = DateTime.UtcNow.AddHours(5);
                                            newStock.Location = item.Location;
                                            newStock.BranchID = item.BranchID;
                                            newStock.CostPrice = item.CostPrice;
                                            newStock.OnMove = 0;
                                            db.tbl_Stock.Add(newStock);
                                        }

                                    }


                                }


                                tbl_Challan ch = new tbl_Challan();
                                ch.AccountID = modelSales.AccountID;
                                ch.AddOn = Helper.PST();
                                ch.StrCode = "DC-" + GetCode().ToString();
                                ch.AmountPaid = modelSales.AmountPaid;
                                ch.BranchID = 9001;// modelSales.BranchID;
                                ch.DiscountAmount = modelSales.DiscountAmount;
                                ch.DiscountPercent = modelSales.DiscountPercent;
                                ch.InvoiceNo = modelSales.SOID.ToString();
                                ch.IsPaid = false;
                                ch.IsDeleted = false;
                                ch.IsActive = true;
                                //ch.SaleOrderID = SaleOrderID;
                                ch.JOID = SaleOrderID;
                                ch.SalesDate = modelSales.SalesDate;
                                ch.SOID = GetCode();
                                ch.TaxAmount = modelSales.TaxAmount;
                                ch.TaxPercent = modelSales.TaxPercent;
                                ch.TotalAmount = modelSales.TotalAmount;
                                ch.TotalPaid = modelSales.TotalPaid;
                                db.tbl_Challan.Add(ch);
                                db.SaveChanges();

                                List<tbl_ChallanDetails> chList = new List<tbl_ChallanDetails>();
                                foreach (var i in modelSales.tbl_SaleDetails)
                                {
                                    if (i.Qty > 0)
                                    {
                                        tbl_ChallanDetails cd = new tbl_ChallanDetails();
                                        cd.ChallanID = ch.ChallanID;
                                        cd.ProductID = i.ProductID;
                                        cd.Qty = i.Qty;
                                        cd.SalePrice = i.SalePrice;
                                        cd.Total = i.Total;
                                        cd.UnitCode = i.UnitCode;
                                        cd.UnitPrice = i.UnitPrice;
                                        chList.Add(cd);
                                    }

                                }

                                if (chList.Count > 0)
                                {
                                    db.tbl_ChallanDetails.AddRange(chList);

                                }
                                #region Accounts Entry

                                //if (sPaymentStatus == "Paid")
                                //{
                                //    // Post dated Cheque                     
                                //    if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
                                //    {
                                //        //pdc Entry
                                //        db.insertSOGJPDCPaidEntry(iPaymentAccountID, iOrderID, COGS);
                                //    }
                                //    else
                                //    {
                                //        db.insertSOGJEntryPaid(iPaymentAccountID, iOrderID, COGS);
                                //    }
                                //}
                                //else if (sPaymentStatus == "Partial Paid")
                                //{
                                //    if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
                                //    {
                                //        //pdc Entry
                                //        db.insertSOGJPDCPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
                                //    }
                                //    else
                                //    {
                                //        db.insertSOGJEntryPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
                                //    }
                                //}
                                //else if (sPaymentStatus == "UnPaid")
                                //{
                                //    db.insertSOGJEntryUnpaid(iOrderID, COGS);
                                //}

                                #endregion

                                db.SaveChanges();
                            }

                            transaction.Commit();
                            result = iOrderID.ToString();
                            return result;
                        }



                    }
                    transaction.Rollback();
                    return -1;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    string message = "";
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                            raise = new InvalidOperationException(message, raise);
                        }
                    }

                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = ex.Message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return ex.Message;
                }
            }
        }


        // in working
        public object createOffsetChallan(tbl_SalesOrder modelSales, List<ItemWisePackageDTO> pkgList, List<Models.DTO.StockLog> modelStockLog, int? bankAccId, int? OrderTypeID, int? SaleOrderID, string Driver, string Transport, string DriverVehicleNo, string GrossWeight, string NetWeight, bool fsc_Cert, bool grs_Cert, bool oeko_tex_Cert)
        {
            dbPOS db = new dbPOS();
            string result = "";
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {

                    int chID = 0;
                    if (modelSales.tbl_SaleDetails.All(x => x.Qty <= 0))
                    {
                        transaction.Rollback();
                        return -1;
                    }
                    var newPkgList = pkgList.Select(x => new { x.RowID, x.DeliveredQty, x.DeliverQty, x.Description, x.ItemID, x.OrderID, x.ProductName, x.Qty, x.PackageId }).Distinct().ToList();
                    if (newPkgList.Count <= 0)
                    {
                        transaction.Rollback();
                        return -1;
                    }

                    if (modelSales.OrderID > 0)
                    {
                        int? iPaymentAccountID = null;
                        int branchId = 9001;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        var data = db.tbl_SaleDetails.Where(x => x.OrderID == modelSales.OrderID).ToList();
                        if (data != null && data.Count > 0 && modelSales.tbl_SaleDetails.Count > 0)
                        {
                            List<tbl_SOItemWisePackageDetails> prevList = new List<tbl_SOItemWisePackageDetails>();
                            foreach (var i in modelSales.tbl_SaleDetails)
                            {
                                var getProd = data.Where(x => x.ProductID == i.ProductID && x.ReadyQty > 0).FirstOrDefault();
                                if (getProd != null)
                                {
                                    getProd.QtyDelivered += Convert.ToInt32(i.Qty);
                                    getProd.ReadyQty -= Convert.ToInt32(i.Qty);
                                    getProd.QtyBalanced = Convert.ToInt32(getProd.Qty - getProd.QtyDelivered);
                                    db.Entry(getProd).State = EntityState.Modified;

                                    //Current Logic
                                    var pkgData = db.tbl_SOItemWisePackageDetails.Where(x => x.OrderID == modelSales.OrderID && x.ItemID == i.ProductID).ToList();
                                    if (pkgData != null)
                                    {
                                        prevList.AddRange(pkgData);
                                        //db.tbl_SOItemWisePackageDetails.RemoveRange(pkgData); // Commented Out As Old Logic
                                       
                                        //db.SaveChanges();
                                    }


                                }

                            }

                            var order = db.tbl_SalesOrder.Where(x => x.OrderID == modelSales.OrderID).FirstOrDefault();

                            if (modelSales.PaymentTypeID == 1)
                            {
                                modelSales.BankName = "";
                                iPaymentAccountID = (int)(modelSales.PaymentTypeID);//  set it from Payment Type ddl 

                            }
                            else if (modelSales.PaymentTypeID > 1)
                            {
                                iPaymentAccountID = bankAccId ?? 0;
                            }
                            if (modelSales.PaymentTypeID == 4)
                            {
                                modelSales.BankName = "";
                                iPaymentAccountID = 109;//  set it from Payment Type ddl 
                            }
                            if (modelSales.PaymentTypeID == 5)
                            {
                                //modelSales.BankName = "CreditCard";
                                iPaymentAccountID = 110;//  set it from Payment Type ddl 
                            }
                            order.BankName = modelSales.BankName;
                            order.PaymentTypeID = iPaymentAccountID;
                            long? PrevCode = db.tbl_JEntry.OrderByDescending(x => x.CodeInt).Select(x => x.CodeInt).FirstOrDefault();
                            long NewCode = 0;

                            string NewCodeSt;
                            if (PrevCode != null)
                            {
                                NewCode = Convert.ToInt64(PrevCode) + 1;
                                NewCodeSt = ("SL-" + NewCode).ToString();
                            }
                            else
                            {
                                NewCode = 9501;
                                NewCodeSt = ("SL-" + NewCode).ToString();
                            }
                            order.PaymentStatus = modelSales.PaymentStatus;
                            order.CompanyIDLogo = 1;
                            order.BranchID = branchId;
                            order.UpdateOn = DateTime.UtcNow.AddHours(5);
                            order.UserID = HttpContext.Current.User.Identity.GetUserId();
                            order.IsReturned = false;
                            order.ReturnAmount = 0;
                          
                            string sPaymentStatus = order.PaymentStatus;
                            db.Entry(order).State = EntityState.Modified;

                            int soId = db.SaveChanges();

                            int iOrderID = order.OrderID;
                            decimal dAmountPaid = 0;

                            if (modelSales.AmountPaid != null || modelSales.AmountPaid > 0)
                            {
                                dAmountPaid = (decimal)(modelSales.AmountPaid);
                            }

                            if (soId >= 1 && modelStockLog != null)
                            {
                                List<int> lstProdIds = modelSales.tbl_SaleDetails.Select(p => p.ProductID).ToList();
                                var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.IsItem == true).ToList();
                                List<tbl_Stock> existingStock = lstStock.ToList();
                                foreach (var item in modelStockLog)
                                {
                                    var rStockOut = item.StockOut ?? 0;
                                                                       
                                    item.InvoiceDate = modelSales.SalesDate;
                                    item.OrderID = iOrderID;
                                    item.AddOn = Helper.PST();
                                    item.AddBy = modelSales.AddBy;
                                    item.BranchID = branchId;
                                    item.OutReference = item.StockOut.ToString() + "| out qty for challan";
                                    item.StockOut = 0;
                                    lstProdIds.Add(item.ProductID);

                                }

                                db.SaveChanges();
                                tbl_Challan ch = new tbl_Challan();

                                const char chkMark = (char)(0X2713);

                                if (fsc_Cert)
                                {
                                    var cert = db.tbl_Certificates.Where(x => x.ID == 3).Select(x => new { Name = x.Certificate + " " + x.CertificateInfo }).FirstOrDefault();
                                    if (cert != null && !string.IsNullOrWhiteSpace(cert.Name))
                                    {
                                        ch.FSC_Cert = chkMark + " " + cert.Name;
                                    }
                                }

                                if (grs_Cert)
                                {
                                    var cert = db.tbl_Certificates.Where(x => x.ID == 4).Select(x => new { Name = x.Certificate + " " + x.CertificateInfo }).FirstOrDefault();
                                    if (cert != null && !string.IsNullOrWhiteSpace(cert.Name))
                                    {
                                        ch.GRS_Cert = chkMark + " " + cert.Name;
                                    }
                                }

                                if (oeko_tex_Cert)
                                {
                                    var cert = db.tbl_Certificates.Where(x => x.ID == 5).Select(x => new { Name = x.Certificate + " " + x.CertificateInfo }).FirstOrDefault();
                                    if (cert != null && !string.IsNullOrWhiteSpace(cert.Name))
                                    {
                                        ch.OEKO_TEX_Cert = chkMark + " " + cert.Name;
                                    }
                                }
                                ch.AccountID = modelSales.AccountID;
                                ch.AddOn = Helper.PST();
                                ch.ChallanStatusID = 1;//Pending
                                ch.StrCode = "DC-" + GetCode().ToString();
                                ch.AmountPaid = modelSales.AmountPaid;
                                ch.BranchID = 9001;// modelSales.BranchID;
                                ch.DiscountAmount = modelSales.DiscountAmount;
                                ch.DiscountPercent = modelSales.DiscountPercent;
                                ch.InvoiceNo = modelSales.SOID.ToString();
                                ch.IsPaid = false;
                                ch.IsDeleted = false;
                                ch.IsActive = true;
                                ch.CustomerPO = modelSales.CustomerPO;
                                ch.Remarks = modelSales.Remarks;
                                //ch.SaleOrderID = SaleOrderID;
                                ch.JOID = SaleOrderID; // same as modelSales.OrderID
                                ch.SalesDate = Helper.PST();// modelSales.SalesDate;
                                ch.SOID = GetCode();
                                ch.TaxAmount = modelSales.TaxAmount;
                                ch.TaxPercent = modelSales.TaxPercent;
                                ch.TotalAmount = modelSales.TotalAmount;
                                ch.TotalPaid = modelSales.TotalPaid;
                                if (!string.IsNullOrWhiteSpace(Driver))
                                {
                                    ch.Driver = Driver;
                                }
                                if (!string.IsNullOrWhiteSpace(Transport))
                                {
                                    ch.Transport = Transport;
                                }
                                if (!string.IsNullOrWhiteSpace(DriverVehicleNo))
                                {
                                    ch.DriverVehicleNo = DriverVehicleNo;
                                }
                                if (!string.IsNullOrWhiteSpace(GrossWeight))
                                {
                                    ch.GrossWeight = GrossWeight;
                                }
                                if (!string.IsNullOrWhiteSpace(NetWeight))
                                {
                                    ch.NetWeight = NetWeight;
                                }
                                db.tbl_Challan.Add(ch);
                                db.SaveChanges();

                                #region Old Logic for Maintaining Package Qty
                                //if (newPkgList != null && newPkgList.Count > 0)
                                //{
                                //    foreach (var i in newPkgList)
                                //    {
                                //        if (prevList.Any(x => x.ItemID == i.ItemID && x.OrderID == i.OrderID && x.ID == i.RowID))
                                //        {


                                //            tbl_SOItemWisePackageDetails item = new tbl_SOItemWisePackageDetails();
                                //            item.AddedOn = Helper.PST();
                                //            item.IsDeleted = false;
                                //            item.DeliveredQty = i.DeliveredQty + i.DeliverQty;
                                //            item.DeliverQty = 0;
                                //            item.Description = i.Description.Trim();
                                //            item.PackageId = i.PackageId.Trim().ToString();
                                //            item.OrderID = modelSales.OrderID;
                                //            item.ItemID = i.ItemID;
                                //            item.ProductQty = i.Qty;
                                //            item.ChallanID = ch.ChallanID;


                                //            prevList.RemoveAll(x => x.ItemID == i.ItemID && x.OrderID == i.OrderID && x.ID == i.RowID && x.ChallanID == null);

                                //            if (item.ProductQty > (item.DeliveredQty + item.DeliverQty))
                                //            {
                                //                tbl_SOItemWisePackageDetails duplicateItemForSO = new tbl_SOItemWisePackageDetails();
                                //                duplicateItemForSO.AddedOn = Helper.PST();
                                //                duplicateItemForSO.IsDeleted = false;
                                //                duplicateItemForSO.DeliveredQty = i.DeliveredQty + i.DeliverQty;
                                //                duplicateItemForSO.DeliverQty = 0;
                                //                duplicateItemForSO.Description = i.Description.Trim();
                                //                duplicateItemForSO.PackageId = i.PackageId.Trim().ToString();
                                //                duplicateItemForSO.OrderID = modelSales.OrderID;
                                //                duplicateItemForSO.ItemID = i.ItemID;
                                //                duplicateItemForSO.ProductQty = i.Qty;
                                //                prevList.Add(duplicateItemForSO);
                                //            }

                                //            prevList.Add(item);

                                //            tbl_DisplayChallanItemsDescription d = new tbl_DisplayChallanItemsDescription();
                                //            d.AddedOn = Helper.PST();
                                //            d.Qty = i.DeliverQty;
                                //            d.Description = string.IsNullOrWhiteSpace(i.Description) ? "-" : i.Description.ToString().Trim();
                                //            d.PackageId = i.PackageId.Trim().ToString();
                                //            d.OrderID = modelSales.OrderID;
                                //            d.ItemID = i.ItemID;
                                //            d.ChallanID = ch.ChallanID;
                                //            db.tbl_DisplayChallanItemsDescription.Add(d);
                                //        }

                                //    }

                                //    prevList.ForEach(x => x.ID = 0);
                                //    db.tbl_SOItemWisePackageDetails.AddRange(prevList);
                                //    db.SaveChanges();

                                   
                                //}
                                #endregion

                                if (newPkgList != null && newPkgList.Count > 0)
                                {
                                    foreach (var i in newPkgList)
                                    {
                                        //if (prevList.Any(x => x.ItemID == i.ItemID && x.OrderID == i.OrderID && x.ID == i.RowID))
                                        //{
                                        if(i.ItemID > 0 && i.OrderID > 0 && !string.IsNullOrWhiteSpace(i.PackageId))
                                        {
                                            var soItemPackage = db.tbl_SOItemWisePackageDetails.Where(x => x.ItemID == i.ItemID && x.OrderID == i.OrderID && x.PackageId.Trim().Equals(i.PackageId.Trim())).FirstOrDefault();

                                            if (soItemPackage != null)
                                            {
                                                tbl_SOItemWisePackageChallanWiseDetails detail = new tbl_SOItemWisePackageChallanWiseDetails();
                                                detail.ItemID = i.ItemID;
                                                detail.PackageDescription = string.IsNullOrWhiteSpace(i.Description) ? "-" : i.Description.ToString().Trim();
                                                detail.PackageId = i.PackageId.Trim();
                                                detail.ChallanID = ch.ChallanID;
                                                detail.Qty = i.DeliverQty;
                                                detail.AddedOn = Helper.PST();
                                                detail.SOItemPackageDetailID = soItemPackage.ID;
                                                db.tbl_SOItemWisePackageChallanWiseDetails.Add(detail);
                                                db.SaveChanges();

                                                soItemPackage.DeliveredQty = i.DeliveredQty + i.DeliverQty;
                                                soItemPackage.DeliverQty = 0;
                                                soItemPackage.Description = i.Description.Trim();
                                                soItemPackage.PackageId = i.PackageId.Trim().ToString();
                                                soItemPackage.OrderID = modelSales.OrderID;
                                                soItemPackage.ItemID = i.ItemID;
                                                soItemPackage.ProductQty = i.Qty;
                                                db.Entry(soItemPackage).State = EntityState.Modified;
                                                db.SaveChanges();

                                                //prevList.RemoveAll(x => x.ItemID == i.ItemID && x.OrderID == i.OrderID && x.ID == i.RowID && x.ChallanID == null);

                                                //if (item.ProductQty > (item.DeliveredQty + item.DeliverQty))
                                                //{
                                                //    tbl_SOItemWisePackageDetails duplicateItemForSO = new tbl_SOItemWisePackageDetails();
                                                //    duplicateItemForSO.AddedOn = Helper.PST();
                                                //    duplicateItemForSO.IsDeleted = false;
                                                //    duplicateItemForSO.DeliveredQty = i.DeliveredQty + i.DeliverQty;
                                                //    duplicateItemForSO.DeliverQty = 0;
                                                //    duplicateItemForSO.Description = i.Description.Trim();
                                                //    duplicateItemForSO.PackageId = i.PackageId.Trim().ToString();
                                                //    duplicateItemForSO.OrderID = modelSales.OrderID;
                                                //    duplicateItemForSO.ItemID = i.ItemID;
                                                //    duplicateItemForSO.ProductQty = i.Qty;
                                                //    prevList.Add(duplicateItemForSO);
                                                //}

                                                //prevList.Add(item);

                                                tbl_DisplayChallanItemsDescription d = new tbl_DisplayChallanItemsDescription();
                                                d.AddedOn = Helper.PST();
                                                d.Qty = i.DeliverQty;
                                                d.Description = string.IsNullOrWhiteSpace(i.Description) ? "-" : i.Description.ToString().Trim();
                                                d.PackageId = i.PackageId.Trim().ToString();
                                                d.OrderID = modelSales.OrderID;
                                                d.ItemID = i.ItemID;
                                                d.ChallanID = ch.ChallanID;
                                                db.tbl_DisplayChallanItemsDescription.Add(d);
                                                db.SaveChanges();

                                                //}


                                            }
                                        }
                                        
                                    }

                                }

                                List<tbl_ChallanDetails> chList = new List<tbl_ChallanDetails>();
                                foreach (var i in modelSales.tbl_SaleDetails)
                                {
                                    if (i.Qty > 0)
                                    {
                                        tbl_ChallanDetails cd = new tbl_ChallanDetails();
                                        cd.ChallanID = ch.ChallanID;
                                        cd.ProductID = i.ProductID;
                                        cd.Qty = i.Qty;
                                        cd.SalePrice = i.SalePrice;
                                        cd.Total = i.Total;
                                        cd.UnitCode = i.UnitCode;
                                        cd.UnitPrice = i.UnitPrice;
                                        chList.Add(cd);
                                        chID = ch.ChallanID;
                                    }

                                }

                                if (chList.Count > 0)
                                {
                                    db.tbl_ChallanDetails.AddRange(chList);

                                }
                                #region Accounts Entry (Not used in challan)

                                //if (sPaymentStatus == "Paid")
                                //{
                                //    // Post dated Cheque                     
                                //    if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
                                //    {
                                //        //pdc Entry
                                //        db.insertSOGJPDCPaidEntry(iPaymentAccountID, iOrderID, COGS);
                                //    }
                                //    else
                                //    {
                                //        db.insertSOGJEntryPaid(iPaymentAccountID, iOrderID, COGS);
                                //    }
                                //}
                                //else if (sPaymentStatus == "Partial Paid")
                                //{
                                //    if (modelSales.SalesDate < modelSales.ChequeDate && iPaymentAccountID > 2)
                                //    {
                                //        //pdc Entry
                                //        db.insertSOGJPDCPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
                                //    }
                                //    else
                                //    {
                                //        db.insertSOGJEntryPartiallyPaid(iPaymentAccountID, dAmountPaid, iOrderID, COGS);
                                //    }
                                //}
                                //else if (sPaymentStatus == "UnPaid")
                                //{
                                //    db.insertSOGJEntryUnpaid(iOrderID, COGS);
                                //}

                                #endregion

                                db.SaveChanges();
                            }

                            transaction.Commit();
                            result = chID.ToString();// iOrderID.ToString();
                            return result;
                        }



                    }
                    transaction.Rollback();
                    return -1;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    string message = "";
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                            raise = new InvalidOperationException(message, raise);
                        }
                    }

                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = ex.Message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return ex.Message;
                }
            }
        }

        public object EditChallan(int ChallanID, string Driver, string Transport, string DriverVehicleNo, string custPONumber, string GrossWeight, string NetWeight,DateTime? ChallanDate)
        {
            dbPOS db = new dbPOS();
            string result = "";
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {

                    if (ChallanID > 0)
                    {
                        var challan = db.tbl_Challan.Where(x => x.ChallanID == ChallanID && x.IsDeleted != true).FirstOrDefault();
                        if (challan != null)
                        {
                            challan.Driver = string.IsNullOrWhiteSpace(Driver) ? challan.Driver : Driver;
                            challan.Transport = string.IsNullOrWhiteSpace(Transport) ? challan.Transport : Transport;
                            challan.DriverVehicleNo = string.IsNullOrWhiteSpace(DriverVehicleNo) ? challan.DriverVehicleNo : DriverVehicleNo;
                            challan.CustomerPO = string.IsNullOrWhiteSpace(custPONumber) ? challan.CustomerPO : custPONumber;
                            challan.NetWeight = string.IsNullOrWhiteSpace(NetWeight) ? challan.NetWeight : NetWeight;
                            challan.GrossWeight = string.IsNullOrWhiteSpace(GrossWeight) ? challan.GrossWeight : GrossWeight;
                            challan.SalesDate = ChallanDate !=null ? ChallanDate: challan.SalesDate;// string.IsNullOrWhiteSpace(GrossWeight) ? challan.GrossWeight : GrossWeight;
                            challan.UpdateOn = Helper.PST();
                            db.Entry(challan).State = EntityState.Modified;
                            db.SaveChanges();
                            transaction.Commit();
                            return 1;
                        }
                    }
                    transaction.Rollback();
                    return -1;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    string message = "";
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                            raise = new InvalidOperationException(message, raise);
                        }
                    }

                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ErrorTracer err = new ErrorTracer();
                    err.dErrorDate = DateTime.UtcNow.AddHours(5);
                    err.vErrorMsg = ex.Message;
                    db.ErrorTracers.Add(err);
                    db.SaveChanges();
                    return ex.Message;
                }
            }
        }

    }
}
