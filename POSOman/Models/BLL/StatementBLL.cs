﻿using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace POSOman.Models.BLL
{
    public class StatementBLL
    {
        
        public static List<Details> GetItemDetailsForStatementJenTryIDWise(int jEntryId)
        {
            try
            {
                
                List<Details> detailList = null;
               
                dbPOS db = new dbPOS();
                if (jEntryId > 0)
                {
                    var OrderIDs = db.tbl_JEntryLog.Where(x => x.JEntryID == jEntryId && (x.OrderTypeID == 3 || x.OrderTypeID == 1)).Select(x => new { x.OrderID, x.OrderTypeID }).ToList();
                    //List<Details> detList = new List<Details>();

                    if (OrderIDs != null && OrderIDs.Count > 0)
                    {
                        //Parallel.ForEach(OrderIDs, id =>
                        //{
                        foreach (var id in OrderIDs)
                        {
                            var data = db.GetItemDataForStatment(null, null, null, id.OrderID, null, id.OrderTypeID).Select(x => new { Id = id.OrderID, Name = x.ProductName, Qty = x.Qty, CostPrice = Convert.ToDecimal(id.OrderTypeID == 3 ? x.SalePrice : x.UnitPrice), Total = Convert.ToDecimal(x.Total), TotalPaid = Convert.ToDecimal(x.TotalPaid) }).ToList();
                            if (data != null && data.Count > 0)
                            {
                                detailList = new List<Details>();
                                Parallel.ForEach(data, item =>
                                 {

                                     Details detail = new Details();

                                     detail.Name = item.Name;
                                     detail.Qty = item.Qty;
                                     detail.CostPrice = item.CostPrice;
                                     detail.Total = item.Total;
                                     detail.InvoiceId = id.OrderID.ToString();
                                     detail.PID = item.Id;

                                     detailList.Add(detail);

                                 });
                                //var list = new
                                //{
                                //    data = data,
                                //    totalQty = data.Sum(x => x.Qty),
                                //    totalPrice = data.Sum(x => x.TotalPaid),
                                //    invoiceType = id.OrderTypeID == 1 ? "Purchase Invoice" : "Sales Invoice"
                                //};
                               
                            }
                            //});
                        }
                    }
                    else
                    {
                        var data = db.tbl_JEntry.Where(x => x.JEntryId == jEntryId && (x.IsDeleted != true || x.IsDeleted == null)).FirstOrDefault();
                        if (data != null && !string.IsNullOrWhiteSpace(data.VoucherName) && data.VoucherName.Equals("Sales") && data.RefID!=null && data.RefID>0)// Sales
                        {
                            var sale_Item_Data = db.GetItemDataForStatment(null, null, null, data.RefID, null, 3).Select(x => new { Id = data.RefID, Name = x.ProductName, Qty = x.Qty, CostPrice = Convert.ToDecimal( x.SalePrice ), Total = Convert.ToDecimal(x.Total), TotalPaid = Convert.ToDecimal(x.TotalPaid) }).ToList();
                            detailList = new List<Details>();
                            Parallel.ForEach(sale_Item_Data, item =>
                            {

                                Details detail = new Details();

                                detail.Name = item.Name;
                                detail.Qty = item.Qty;
                                detail.CostPrice = item.CostPrice;
                                detail.Total = item.Total;
                                detail.InvoiceId = data.RefID.ToString();
                                detail.PID = item.Id;

                                detailList.Add(detail);

                            });
                        } 
                        else if (data != null && !string.IsNullOrWhiteSpace(data.VoucherName) && data.VoucherName.Equals("Purchase") && data.RefID != null && data.RefID > 0)//Purchases
                        {
                            var sale_Item_Data = db.GetItemDataForStatment(null, null, null, data.RefID, null, 1).Select(x => new { Id = data.RefID, Name = x.ProductName, Qty = x.Qty, CostPrice = Convert.ToDecimal(x.UnitPrice), Total = Convert.ToDecimal(x.Total), TotalPaid = Convert.ToDecimal(x.TotalPaid) }).ToList();
                            detailList = new List<Details>();
                            Parallel.ForEach(sale_Item_Data, item =>
                            {

                                Details detail = new Details();

                                detail.Name = item.Name;
                                detail.Qty = item.Qty;
                                detail.CostPrice = item.CostPrice;
                                detail.Total = item.Total;
                                detail.InvoiceId = data.RefID.ToString();
                                detail.PID = item.Id;

                                detailList.Add(detail);

                            });
                        }
                        else if (data != null && !string.IsNullOrWhiteSpace(data.VoucherName) && !string.IsNullOrWhiteSpace(data.Description) && data.VoucherName.Equals("Direct Cash Payment To Employee")) // Employee Purchase
                        {

                            if(data.SaleOrderID == null)
                            {
                                if (data.Description.Contains("EmployeeOrderID"))
                                {
                                    string[] list = data.Description.Split(new string[] { "EmployeeOrderID" }, StringSplitOptions.None);

                                    if (list?.Length > 1)
                                    {
                                        string EPOID_String = list[1];
                                        if (!string.IsNullOrWhiteSpace(EPOID_String))
                                        {
                                            EPOID_String = Regex.Match(EPOID_String, @"\d+").Value;

                                            int EPOID;
                                            if (!string.IsNullOrWhiteSpace(EPOID_String))
                                            {
                                                if(Int32.TryParse(EPOID_String, out EPOID))
                                                {
                                                    data.SaleOrderID = EPOID;
                                                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                                                    db.SaveChanges();
                                                }
                                            }
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            

                            var sale_Item_Data = db.GetItemDataForStatment(null, null, null, data.SaleOrderID, null, 14).Select(x => new { Id = data.RefID, Name = x.ProductName, Qty = x.Qty, CostPrice = Convert.ToDecimal(x.UnitPrice), Total = Convert.ToDecimal(x.Total), TotalPaid = Convert.ToDecimal(x.TotalPaid) }).ToList();
                            if(sale_Item_Data!=null && sale_Item_Data.Count > 0)
                            {
                                detailList = new List<Details>();
                                Parallel.ForEach(sale_Item_Data, item =>
                                {

                                    Details detail = new Details();

                                    detail.Name = item.Name;
                                    detail.Qty = item.Qty;
                                    detail.CostPrice = item.CostPrice;
                                    detail.Total = item.Total;
                                    detail.InvoiceId = data.RefID.ToString();
                                    detail.PID = item.Id;

                                    detailList.Add(detail);

                                });

                            }
                        }
                    }

                }

                return detailList;
            }
            catch(Exception e)
            {
                
            }
            return null;
        }
    }
}