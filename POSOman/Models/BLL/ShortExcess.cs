﻿using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace POSOman.Models.BLL
{
    public class ShortExcess
    {
        public object Save(Expenses model, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            int? jEntryTypeID = 0;
            int creditAccountID = 0;    //
            int debitAccountID = 0;     // 
            model.UserID = HttpContext.Current.User.Identity.GetUserId();            
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    StringBuilder Memo = new StringBuilder();
                    db.Configuration.ValidateOnSaveEnabled = false;
                    if (model.AccountID == 1)   // Excess  
                    {
                        jEntryTypeID = 15; // Excess Entry
                        Memo.Append("Excess Entry");
                        debitAccountID = 1; // CAsh Account id
                        creditAccountID = 20; //20; // Excess account id                                             
                        jEntryID = jEntryID = db.insertGJEntry(model.Amount, debitAccountID, model.VoucherDate, AddBy, creditAccountID, Memo.ToString(), branchId, model.Description, jEntryTypeID, model.UserID).FirstOrDefault();
                    }
                    else if (model.AccountID == 2)   // Short 
                    {
                        Memo.Append("Short Entry.");
                        jEntryTypeID = 16; // Short Entry                        
                        debitAccountID = 21; //21; // Short Account id
                        creditAccountID = 1;  // CAsh Account id
                        jEntryID = jEntryID = db.insertGJEntry(model.Amount, debitAccountID, model.VoucherDate, AddBy, creditAccountID, Memo.ToString(), branchId, model.Description, jEntryTypeID, model.UserID).FirstOrDefault();
                    }                    
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
    }
}
