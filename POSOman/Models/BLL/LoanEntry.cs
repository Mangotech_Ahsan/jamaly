﻿using POSOman.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using System.Text;
using AutoMapper;

namespace POSOman.Models.BLL
{
    public class LoanEntry
    {
        public object Save(Expenses model, int? bankAccId, int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;
            int? jEntryTypeID = 0;
            int? creditAccountID = 0;    //
            int? debitAccountID = 0;     // 
            int? assetAccountId = 0;            
            model.UserID = HttpContext.Current.User.Identity.GetUserId();            
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentTypeID == 1 || model.PaymentTypeID == 2)
                    {                        
                        model.ChequeDate = null;
                    }
                    if (model.PaymentTypeID == 1)
                    {
                        assetAccountId = 1; // cash
                        model.BankName = "";
                    }
                    else
                    {
                        assetAccountId = bankAccId;
                    }
                    StringBuilder Memo = new StringBuilder();
                    if (model.AccountID == 1)   // Loan Payment
                    {
                        jEntryTypeID = 17; // Loan Payment
                        Memo.Append("Loan Payment");
                        debitAccountID = 22; //22; // Loan account id
                        creditAccountID = assetAccountId; // 
                    }
                    else if (model.AccountID == 2)   // Loan Receiving
                    {
                        Memo.Append("Loan Receiving.");
                        jEntryTypeID = 18; // Loan Receiving                        
                        debitAccountID = assetAccountId;   //
                        creditAccountID = 22;  //22    // Loan Account Id                        
                    }
                    
                    model.BranchID = branchId;                    
                    
                    db.Configuration.ValidateOnSaveEnabled = false;
                    if (model.PaymentTypeID == 2 || model.PaymentTypeID == 3)
                    {
                        jEntryID = db.insertLoanGJEntry(model.Amount, debitAccountID, model.VoucherDate, AddBy, creditAccountID, model.BankName,model.ChequeDate,model.ChequeNumber,Memo.ToString(), branchId, model.Description, bankAccId,jEntryTypeID, model.PaymentTypeID,model.UserID).FirstOrDefault();
                    }
                    else
                    {
                        jEntryID = jEntryID = db.insertGJEntry(model.Amount, debitAccountID, model.VoucherDate, AddBy, creditAccountID, Memo.ToString(), branchId, model.Description, jEntryTypeID, model.UserID).FirstOrDefault();
                    }
                //    if(model.AccountID == 1) // entry when paying
                //    { 
                //    payLog.JEntryID = jEntryID; // get it from SP                        
                //    payLog.AddOn = DateTime.UtcNow.AddHours(5);
                //    payLog.AddBy = 1;
                //    payLog.BranchID = branchId;
                //    Mapper.CreateMap<Models.DTO.PaymentLog, tbl_PaymentLog>();
                //    var paymentLog = Mapper.Map<Models.DTO.PaymentLog, tbl_PaymentLog>(payLog);
                //    db.tbl_PaymentLog.Add(paymentLog);
                //    db.SaveChanges();
                //}
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

    }
}
