﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.BLL
{
    public class POSCashDepositBLL
    {
       
        //Save Deposit
        public object SaveDeposit(Models.DTO.DepositDTO model, int branchId)
        {
            dbPOS db = new dbPOS();
            int? jEntryID = 0;
            int? jEntryTypeID = 0;
            int? creditAccountID = 0;   
            int? debitAccountID = 0;                 
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    debitAccountID = 109;
                    creditAccountID = 1;
                    //debitAccountID = model.ToBankID;
                    //creditAccountID = model.FromBankID;
                    jEntryTypeID = 37;
                    db.Configuration.ValidateOnSaveEnabled = false;
                    jEntryID = db.insertBankTransferEntry(model.Amount, debitAccountID, model.VoucherDate, model.AddBy, creditAccountID, model.ReferenceAccountID, model.UserReferenceID, model.BranchID, model.Description, "POSCash Deposit", jEntryTypeID, model.UserID, "POSCash Deposit Entry").FirstOrDefault();

                    transaction.Commit();
                    return jEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        //Save Drawing
        public object SaveDrawing(Models.DTO.DepositDTO model, int branchId)
        {
            dbPOS db = new dbPOS();
            int? jEntryID = 0;
            int? jEntryTypeID = 0;
            int? creditAccountID = 0;    //
            int? debitAccountID = 0;     //             
            model.UserID = HttpContext.Current.User.Identity.GetUserId();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    debitAccountID = model.ToBankID;//drawing id
                    creditAccountID = model.FromBankID;
                    jEntryTypeID = 38;
                    //model.BranchID = branchId;
                    db.Configuration.ValidateOnSaveEnabled = false;
                    jEntryID = jEntryID = db.insertBankTransferEntry(model.Amount, debitAccountID, model.VoucherDate, model.AddBy, creditAccountID, model.ReferenceAccountID, model.UserReferenceID, model.BranchID, model.Description, "POSCash Draw", jEntryTypeID, model.UserID, "POSCash Draw Entry").FirstOrDefault();

                    transaction.Commit();
                    return jEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

    }
}