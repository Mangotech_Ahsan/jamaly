﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace POSOman.Models.BLL
{
    public class DOReturn
    {
        public object Save(Models.DTO.DOReturn model, List<Models.DTO.StockLog> modelStockLog, int branchId)
        {
            dbPOS db = new dbPOS();            
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    model.AddOn = DateTime.UtcNow.AddHours(5);
                    model.BranchID = branchId;
                    Mapper.CreateMap<Models.DTO.DOReturn, tbl_DOReturn>();
                    Mapper.CreateMap<Models.DTO.DOReturnDetails, tbl_DOReturnDetails>();
                    var ReturnDetails = Mapper.Map<ICollection<Models.DTO.DOReturnDetails>, ICollection<tbl_DOReturnDetails>>(model.DOReturnDetails);
                    var master = Mapper.Map<Models.DTO.DOReturn, tbl_DOReturn>(model);
                    master.tbl_DOReturnDetails = ReturnDetails;
                    db.tbl_DOReturn.Add(master);
                    int poId = db.SaveChanges();
                    int iReturnID = master.DOReturnID;
                    if (poId >= 1 && modelStockLog != null)
                    {
                        // get all product ids 
                        List<int> lstProdIds = master.tbl_DOReturnDetails.Select(p => p.ProductID).ToList();
                        // get all stock where productid contains lstProdIds
                        var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == master.BranchID).ToList();
                        var lstSODetail = db.tbl_DODetails.Where(s => lstProdIds.Contains(s.ProductID) && (s.OrderID == master.DeliveryOrderID)).ToList();
                        List<tbl_DODetails> returningOrders = lstSODetail.ToList();
                        List<tbl_Stock> existingStock = lstStock.ToList();
                        // 
                        foreach (var item in modelStockLog)
                        {
                            item.InvoiceDate = DateTime.UtcNow.AddHours(5).Date;
                            item.OrderID = iReturnID;
                            item.OrderTypeID = 8;
                            item.AddOn = DateTime.UtcNow.AddHours(5);
                            item.AddBy = 1;
                            item.BranchID = branchId;
                            lstProdIds.Add(item.ProductID);
                            //  if already exists stock
                            var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
                            var soProduct = returningOrders.FirstOrDefault(s => s.ProductID == item.ProductID);
                            #region
                            // Updating SODetails
                            if (soProduct != null)
                            {
                                soProduct.ReturnedQty += Convert.ToInt32(item.StockIN);
                                soProduct.UpdateOn = DateTime.UtcNow.AddHours(5);
                                if (soProduct.Qty == soProduct.ReturnedQty)
                                {
                                    soProduct.IsReturned = true;
                                }
                                db.Entry(soProduct).State = EntityState.Modified;
                            }
                            #endregion
                            if (row != null)
                            {
                                row.Qty += Convert.ToInt32(item.StockIN);
                                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                db.Entry(row).State = EntityState.Modified;
                            }
                        }
                        var deliveryOrder = db.tbl_DeliveryOrder.Where(d => d.OrderID == master.DeliveryOrderID).FirstOrDefault();
                        if (deliveryOrder != null)
                        {
                            if (deliveryOrder.ReturnAmount == null)
                            {
                                deliveryOrder.ReturnAmount = master.TotalAmount;
                            }
                            else {
                                deliveryOrder.ReturnAmount += master.TotalAmount;
                            }
                            
                            deliveryOrder.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(deliveryOrder).State = EntityState.Modified;
                        }
                        Mapper.CreateMap<Models.DTO.StockLog, tbl_StockLog>();
                        var stockLog = Mapper.Map<ICollection<Models.DTO.StockLog>, ICollection<tbl_StockLog>>(modelStockLog);
                        stockLog.ToList<tbl_StockLog>().ForEach(s => db.Entry(s).State = EntityState.Added);                      
                        db.SaveChanges();
                    }
                    transaction.Commit();
                    db.UpdateDOTable(model.DeliveryOrderID);
                    return iReturnID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
    }
}