﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace POSOman.Models.BLL
{
    public class DeleteOrders
    {
        dbPOS db = new dbPOS();
        UserActionsPerformed UserActions = new UserActionsPerformed();

        public object DeleteDO(Models.DTO.DOReturn model)
        {
            dbPOS db = new dbPOS();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;         
                    
                    if (model.DOReturnDetails != null)
                    {
                        // get all product ids 
                        List<int> lstProdIds = model.DOReturnDetails.Select(p => p.ProductID).ToList();
                        // get all stock where productid contains lstProdIds
                        var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == model.BranchID).ToList();                        
                        List<tbl_Stock> existingStock = lstStock.ToList();
                        // 
                        foreach (var item in model.DOReturnDetails)
                        {                               
                            //  if already exists stock
                            var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);                                                       
                            if (row != null)
                            {
                                row.Qty += Convert.ToInt32(item.Qty);
                                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                db.Entry(row).State = EntityState.Modified;
                            }
                        }                          
                        db.SaveChanges();
                        if (model.DeliveryOrderID > 0)
                        {
                            db.DeleteDeliveryOrder(model.DeliveryOrderID, model.BranchID);
                        }
                    }
                    transaction.Commit();                    
                    return "1";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        public object DeleteSO(Models.DTO.SalesReturn model, int SalesOrderID, int SOID, int BranchID, int? ChequeID,int UPJEntryID,int? PaidJEntryID)
        {
            dbPOS db = new dbPOS();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;

                    if (model.SOReturnDetails != null)
                    {
                        // get all product ids 
                        List<int> lstProdIds = model.SOReturnDetails.Select(p => p.ProductID).ToList();
                        // get all stock where productid contains lstProdIds
                        var lstStock = db.tbl_Stock.Where(s => lstProdIds.Contains(s.ProductID) && s.BranchID == model.BranchID).ToList();
                        List<tbl_Stock> existingStock = lstStock.ToList();
                        // 
                        foreach (var item in model.SOReturnDetails)
                        {
                            //  if already exists stock
                            var row = existingStock.FirstOrDefault(s => s.ProductID == item.ProductID);
                            if (row != null)
                            {
                                row.Qty += Convert.ToInt32(item.Qty);
                                row.UpdateOn = DateTime.UtcNow.AddHours(5);
                                db.Entry(row).State = EntityState.Modified;
                            }
                        }                        
                        if (model.SaleOrderID > 0)
                        {
                            db.DeleteSalesOrder(SalesOrderID,SOID,BranchID,ChequeID,UPJEntryID,PaidJEntryID);
                        }
                        db.SaveChanges();
                    }
                    transaction.Commit();
                    return "1";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

        public int DeleteChallan(int ChallanID, int JOID)
        {
            try
            {

                int c = 0;
                using (var t = db.Database.BeginTransaction())
                {

                    var challan = db.tbl_Challan.Where(x => x.ChallanID == ChallanID && x.JOID == JOID && x.IsDeleted != true).FirstOrDefault();
                    if (challan != null)
                    {
                        var prodIDs = challan.tbl_ChallanDetails.Select(x => x.ProductID).ToList();

                        var pkgDetail = db.tbl_SOItemWisePackageDetails.Where(x => x.OrderID == JOID && prodIDs.Contains(x.ItemID) && x.IsDeleted != true).ToList(); // Job Order Items
                        if (pkgDetail != null && pkgDetail.Count > 0)
                        {

                            foreach(var p in pkgDetail)
                            {
                                //var displayItemsForChallan = db.tbl_DisplayChallanItemsDescription.Where(y => y.ItemID == p.ItemID && y.OrderID == JOID && y.ChallanID == ChallanID && y.PackageId == p.PackageId).ToList();
                                //if (displayItemsForChallan != null && displayItemsForChallan.Count > 0)
                                //{
                                //    foreach (var i in displayItemsForChallan)
                                //    {
                                //        p.DeliveredQty -= i.Qty;
                                //        p.DeliverQty += i.Qty;
                                //        //p.ChallanID = null;
                                //        db.Entry(p).State = EntityState.Modified;
                                //        //x.IsDeleted = true;

                                //        c += db.SaveChanges();
                                //    }
                                //}

                                var pkgItem = db.tbl_SOItemWisePackageChallanWiseDetails.Where(x => x.ItemID == p.ItemID && x.ChallanID == ChallanID && x.PackageId.Trim().Equals(p.PackageId.Trim())).FirstOrDefault();

                                if (pkgItem != null)
                                {
                                    pkgItem.IsDeleted = true;
                                    pkgItem.DeletedOn = Helper.PST();
                                    //p.ChallanID = null;
                                    db.Entry(pkgItem).State = EntityState.Modified;
                                    //x.IsDeleted = true;

                                    p.DeliveredQty -= pkgItem.Qty;
                                    p.DeliverQty += pkgItem.Qty;

                                    db.Entry(p).State = EntityState.Modified;

                                    c += db.SaveChanges();
                                }

                            }

                        }

                       
                        var jobOrder = db.tbl_SalesOrder.Where(x => x.OrderID == JOID && x.IsSaleOrder == true).FirstOrDefault();
                        if (jobOrder != null)
                        {

                            if (jobOrder.tbl_SaleDetails != null && jobOrder.tbl_SaleDetails.Count > 0)
                            {
                                foreach (var d in jobOrder.tbl_SaleDetails)
                                {
                                    var i = challan.tbl_ChallanDetails.Where(y => y.ProductID == d.ProductID).FirstOrDefault();
                                    if (i != null)
                                    {
                                        d.QtyDelivered -= Convert.ToInt32(i.Qty);
                                        d.ReadyQty += Convert.ToInt32(i.Qty);
                                        d.QtyBalanced = Convert.ToInt32(d.Qty - d.QtyDelivered);
                                        d.isCompleted = false;
                                        db.Entry(d).State = EntityState.Modified;
                                    }
                                    c += db.SaveChanges();

                                }
                                //Parallel.ForEach(jobOrder.tbl_SaleDetails.Where(x=>prodIDs.Contains(x.ProductID)), x =>
                                //{
                                //    var i = challan.tbl_ChallanDetails.Where(y => y.ProductID == x.ProductID).FirstOrDefault();
                                //    if (i != null)
                                //    {
                                //        x.QtyDelivered -= Convert.ToInt32(i.Qty);
                                //        x.ReadyQty += Convert.ToInt32(i.Qty);
                                //        x.QtyBalanced = Convert.ToInt32(x.Qty - x.QtyDelivered);
                                //        //TotalAmount += Convert.ToDecimal(i.Qty * i.SalePrice);
                                //        db.Entry(x).State = EntityState.Modified;
                                //    }

                                //});
                            }

                            jobOrder.isCompleted = false;
                            db.Entry(jobOrder).State = EntityState.Modified;
                            c += db.SaveChanges();
                        }


                        var stkLog = db.tbl_StockLog.Where(x => x.OrderID == JOID && prodIDs.Contains(x.ProductID)).ToList();
                        if (stkLog != null && stkLog.Count > 0)
                        {
                            if (prodIDs != null && prodIDs.Count > 0)
                            {
                                stkLog.ForEach(x => { x.IsActive = false; db.Entry(x).State = EntityState.Modified; });
                                c += db.SaveChanges();
                            }
                        }

                        //var challanDetails = challan.tbl_ChallanDetails.ToList();
                        if (challan.tbl_ChallanDetails != null && challan.tbl_ChallanDetails.Count > 0)
                        {

                            Parallel.ForEach(challan.tbl_ChallanDetails, i =>
                            {
                                var stock = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && (x.IsActive != false || x.IsActive == null)).FirstOrDefault();
                                if (stock != null)
                                {
                                    stock.Qty += i.Qty;
                                    db.Entry(stock).State = EntityState.Modified;
                                }
                            });

                            c += db.SaveChanges();
                        }
                        challan.IsActive = false;
                        challan.IsDeleted = true;
                        challan.DeletedOn = Helper.PST();
                        db.Entry(challan).State = EntityState.Modified;
                        c += db.SaveChanges();

                        if (c > 0)
                        {
                            t.Commit();
                            return 1;
                        }
                    }
                    t.Rollback();
                    return -1;
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return -1;
            }
        }
        #region Old Logic
        //public int DeleteChallan (int ChallanID,int JOID)
        //{
        //    try
        //    {

        //        int c = 0;
        //        using (var t = db.Database.BeginTransaction())
        //        {

        //            var challan = db.tbl_Challan.Where(x => x.ChallanID == ChallanID && x.JOID == JOID && x.IsDeleted!=true).FirstOrDefault();
        //            if (challan != null)
        //            {
        //                var prodIDs = challan.tbl_ChallanDetails.Select(x => x.ProductID).ToList();

        //                var pkgDetail = db.tbl_SOItemWisePackageDetails.Where(x => /*x.ChallanID == ChallanID &&*/ x.OrderID == JOID && x.IsDeleted!=true).ToList(); // Job Order Items
        //                if(pkgDetail!=null && pkgDetail.Count > 0)
        //                {
        //                    //foreach(var x in pkgDetail)
        //                    //{

        //                    //    var displayItemsForChallan = db.tbl_DisplayChallanItemsDescription.Where(y => y.ItemID == x.ItemID && y.ChallanID == ChallanID && y.OrderID == JOID).ToList();
        //                    //    if (displayItemsForChallan != null && displayItemsForChallan.Count > 0)
        //                    //    {
        //                    //        foreach (var i in displayItemsForChallan)
        //                    //        {
        //                    //            if (x.ItemID == i.ItemID && x.ChallanID == i.ChallanID && x.OrderID == i.OrderID)
        //                    //            {

        //                    //                //x.DeliveredQty -= i.Qty;
        //                    //                //x.DeliverQty += i.Qty;
        //                    //                //x.ChallanID = null;
        //                    //                //x.IsDeleted = true;
        //                    //                //db.Entry(x).State = EntityState.Modified;

        //                    //            }
        //                    //        }
        //                    //        //db.tbl_DisplayChallanItemsDescription.RemoveRange(displayItemsForChallan);
        //                    //        //c += db.SaveChanges();
        //                    //    }

        //                    //}

        //                    var getAllPkgDetails = db.tbl_SOItemWisePackageDetails.Where(x => /*x.ChallanID == null &&*/ x.OrderID == JOID && x.IsDeleted != true).ToList(); 
        //                    foreach (var p in getAllPkgDetails)
        //                    {
        //                        var displayItemsForChallan = db.tbl_DisplayChallanItemsDescription.Where(y => y.ItemID == p.ItemID && y.OrderID == JOID && y.ChallanID == ChallanID && y.PackageId == p.PackageId).ToList();
        //                        if (displayItemsForChallan != null && displayItemsForChallan.Count > 0)
        //                        {
        //                            foreach (var i in displayItemsForChallan)
        //                            {
        //                                p.DeliveredQty -= i.Qty;
        //                                p.DeliverQty += i.Qty;
        //                                //p.ChallanID = null;
        //                                db.Entry(p).State = EntityState.Modified;
        //                                //x.IsDeleted = true;
        //                            }
        //                            c += db.SaveChanges();

        //                        }
        //                    }

        //                    db.tbl_SOItemWisePackageDetails.RemoveRange(pkgDetail);

        //                    c += db.SaveChanges();
        //                }

        //                //var displayDetails = db.tbl_DisplayChallanItemsDescription.Where(x => x.ChallanID == ChallanID && x.OrderID == JOID).ToList();
        //                //if (displayDetails != null && displayDetails.Count > 0)
        //                //{
        //                //    db.tbl_DisplayChallanItemsDescription.RemoveRange(displayDetails);
        //                //    c += db.SaveChanges();
        //                //}

        //                var jobOrder = db.tbl_SalesOrder.Where(x => x.OrderID == JOID && x.IsSaleOrder == true).FirstOrDefault();
        //                if (jobOrder != null)
        //                {

        //                   if(jobOrder.tbl_SaleDetails != null && jobOrder.tbl_SaleDetails.Count > 0)
        //                    {
        //                        foreach(var d in jobOrder.tbl_SaleDetails)
        //                        {
        //                            var i = challan.tbl_ChallanDetails.Where(y => y.ProductID == d.ProductID).FirstOrDefault();
        //                            if (i != null)
        //                            {
        //                                d.QtyDelivered -= Convert.ToInt32(i.Qty);
        //                                d.ReadyQty += Convert.ToInt32(i.Qty);
        //                                d.QtyBalanced = Convert.ToInt32(d.Qty - d.QtyDelivered);
        //                                db.Entry(d).State = EntityState.Modified;
        //                            }
        //                            c += db.SaveChanges();

        //                        }
        //                        //Parallel.ForEach(jobOrder.tbl_SaleDetails.Where(x=>prodIDs.Contains(x.ProductID)), x =>
        //                        //{
        //                        //    var i = challan.tbl_ChallanDetails.Where(y => y.ProductID == x.ProductID).FirstOrDefault();
        //                        //    if (i != null)
        //                        //    {
        //                        //        x.QtyDelivered -= Convert.ToInt32(i.Qty);
        //                        //        x.ReadyQty += Convert.ToInt32(i.Qty);
        //                        //        x.QtyBalanced = Convert.ToInt32(x.Qty - x.QtyDelivered);
        //                        //        //TotalAmount += Convert.ToDecimal(i.Qty * i.SalePrice);
        //                        //        db.Entry(x).State = EntityState.Modified;
        //                        //    }

        //                        //});
        //                    }

        //                    jobOrder.isCompleted = false;
        //                    db.Entry(jobOrder).State = EntityState.Modified;
        //                    c += db.SaveChanges();
        //                }


        //                var stkLog = db.tbl_StockLog.Where(x => x.OrderID == JOID && prodIDs.Contains(x.ProductID)).ToList();
        //                if(stkLog!=null && stkLog.Count > 0)
        //                {
        //                    if(prodIDs!=null && prodIDs.Count > 0)
        //                    {
        //                        stkLog.ForEach(x => { x.IsActive = false; db.Entry(x).State = EntityState.Modified; });
        //                        c += db.SaveChanges();
        //                    }                         
        //                }

        //                //var challanDetails = challan.tbl_ChallanDetails.ToList();
        //                if(challan.tbl_ChallanDetails != null && challan.tbl_ChallanDetails.Count > 0)
        //                {

        //                        Parallel.ForEach(challan.tbl_ChallanDetails, i => 
        //                        {
        //                            var stock = db.tbl_Stock.Where(x => x.ProductID == i.ProductID && (x.IsActive != false || x.IsActive == null)).FirstOrDefault();
        //                            if (stock != null)
        //                            {
        //                                stock.Qty += i.Qty;
        //                                db.Entry(stock).State = EntityState.Modified;
        //                            }
        //                        });

        //                    c += db.SaveChanges();
        //                }
        //                challan.IsActive = false;
        //                challan.IsDeleted = true;
        //                challan.DeletedOn = Helper.PST();
        //                db.Entry(challan).State = EntityState.Modified;
        //                c += db.SaveChanges();

        //                if (c > 0)
        //                {
        //                    t.Commit();
        //                    return 1;
        //                }
        //            }
        //            t.Rollback();
        //            return -1;
        //        }
        //    }
        //    catch(Exception e)
        //    {
        //        while (e.InnerException != null)
        //        {
        //            e = e.InnerException;
        //        }
        //        return -1;
        //    }
        //}
        #endregion
        public int DeleteSaleInvoice(int OrderID,int userID)
        {
            try
            {
                using(var t = db.Database.BeginTransaction())
                {
                    try
                    {
                        int c = 0;
                        var invoice = db.tbl_SalesOrder.Where(x => x.OrderID == OrderID && x.IsDeleted != true).FirstOrDefault();
                        if (invoice != null)
                        {
                            if (invoice.tbl_SaleDetails != null && invoice.tbl_SaleDetails.Count > 0)
                            {
                                foreach (var sd in invoice.tbl_SaleDetails)
                                {
                                    sd.IsDeleted = true;
                                    db.Entry(sd).State = EntityState.Modified;
                                }
                                //Parallel.ForEach(invoice.tbl_SaleDetails, sd =>
                                // {
                                //     sd.IsDeleted = true;
                                //     db.Entry(sd).State = EntityState.Modified;
                                // });
                                c += db.SaveChanges();
                            }

                            var chIDs = db.tbl_ChallanIDs.Where(x => x.OrderID == OrderID && x.isDeleted != true).ToList();
                            if (chIDs != null && chIDs.Count > 0)
                            {
                                var existingChallanIDs = chIDs.Select(x => x.ChallanID).ToList();
                                var challans = db.tbl_Challan.Where(x => existingChallanIDs.Contains(x.ChallanID) && (x.IsDeleted != true || x.IsDeleted == null)).ToList();

                                if (challans != null && challans.Count > 0)
                                {
                                    foreach (var ch in challans)
                                    {
                                        ch.ChallanStatusID = 1;
                                        db.Entry(ch).State = EntityState.Modified;
                                    }
                                    //Parallel.ForEach(challans, ch =>
                                    // {
                                    //     ch.ChallanStatusID = 1; db.Entry(ch).State = EntityState.Modified;
                                    // });

                                    c += db.SaveChanges();
                                }


                                foreach (var chid in chIDs)
                                {
                                    chid.isDeleted = true;
                                    db.Entry(chid).State = EntityState.Modified;
                                }
                                //Parallel.ForEach(chIDs, chid =>
                                //{
                                //    chid.isDeleted = true; db.Entry(chid).State = EntityState.Modified;
                                //});

                                c += db.SaveChanges();
                            }

                            #region Delete AccountEntries
                            db.DeleteSaleInvoiceIDWise(OrderID);
                            #endregion

                            invoice.IsDeleted = true;
                            invoice.DeletedOn = Helper.PST();
                            db.Entry(invoice).State = EntityState.Modified;
                            c += db.SaveChanges();
                            if (c > 0)
                            {
                                t.Commit();
                                UserActions.MapActions(Convert.ToInt32(userID), "Deleted Invoice #: " + invoice.InvoiceNo.ToString());
                                return c;
                            }
                        }

                        t.Rollback();
                        return -1;
                    }
                    catch
                    {
                        t.Rollback();
                        return -2;
                    }
                   
                }
            }
            catch(Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                return -2;
            }

        }
    }
}