﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace POSOman.Models.BLL
{
    public class Cheques
    {
        // Vendor Cheque Bounce Entry 
        public object BounceVendorPDC(int JEntryID)
        {
            dbPOS db = new dbPOS();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    ICollection<tbl_JEntryLog> jEntryLog = db.tbl_JEntryLog.Where(j => j.JEntryID == JEntryID).ToList();
                    Mapper.CreateMap<tbl_JEntryLog, Models.DTO.JEntryLog>();
                    var jLog = Mapper.Map<ICollection<tbl_JEntryLog>, ICollection<Models.DTO.JEntryLog>>(jEntryLog);
                    db.Configuration.ValidateOnSaveEnabled = false;
                    #region Update Purchase Order 
                    foreach (var item in jLog)
                    {
                        var row = db.tbl_PurchaseOrder.FirstOrDefault(so => so.OrderID == item.OrderID && so.BranchID == item.BranchID);
                        if (row != null)
                        {
                            row.TotalPaid -= Convert.ToDecimal(item.Amount);
                            if (row.TotalPaid == 0)
                            {
                                row.PaymentStatus = "UnPaid";
                                row.IsPaid = false;
                            }
                            else if (row.TotalPaid < row.TotalAmount)
                            {
                                row.PaymentStatus = "Partial Paid";
                                row.IsPaid = false;
                            }
                            row.UpdateOn = DateTime.UtcNow.AddHours(5);
                            db.Entry(row).State = EntityState.Modified;
                        }
                    }
                    #endregion
                    #region Reverse Journal Entry 
                    int? newJEntryID = db.BounceVendorPDCCheque(JEntryID).FirstOrDefault();
                    #endregion
                    db.SaveChanges();
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        // Cheque Bounce ENtry 
        public object DeleteReceivedEntry(int JEntryID)
        {
            dbPOS db = new dbPOS();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    ICollection<tbl_JEntryLog> jEntryLog = db.tbl_JEntryLog.Where(j => j.JEntryID == JEntryID).ToList();
                    Mapper.CreateMap<tbl_JEntryLog, Models.DTO.JEntryLog>();
                    var jLog = Mapper.Map<ICollection<tbl_JEntryLog>, ICollection<Models.DTO.JEntryLog>>(jEntryLog);                    
                    db.Configuration.ValidateOnSaveEnabled = false;
                    #region Update SalesOrder 
                    foreach (var item in jLog)
                    {
                        // May be order branch and payment branches are different 
                        var row = db.tbl_SalesOrder.FirstOrDefault(so => so.OrderID == item.OrderID);// && so.BranchID == item.BranchID);
                        if (row != null)
                        {
                            decimal wht = 0;
                            decimal discount = 0;
                            decimal adjustment = 0;

                            var jentry = db.tbl_JDetail.Where(x => x.JEntryID == JEntryID).ToList();

                            Parallel.ForEach(jentry, x =>
                            {
                                if (!string.IsNullOrWhiteSpace(x.Memo) && x.Memo.Contains("WHTAccID:")) // WHT Acc ID
                                {
                                    string whtMemoForId = x.Memo.Split(':').ToArray().LastOrDefault().ToString();

                                    Decimal.TryParse(whtMemoForId, out wht);

                                    if (wht > 0) { wht = x.Cr; }
                                }
                                if (x.AccountID == 5) // Adj Acc ID
                                {
                                    adjustment = x.Cr;
                                }
                                if (x.AccountID == 31) // Discount Acc ID
                                {
                                    discount = x.Dr;
                                }
                            });

                            row.AdjustmentDescription = string.Empty;
                            row.Adjustment = row.Adjustment.HasValue ? row.Adjustment -= adjustment : row.Adjustment = 0;
                            row.Tax = row.Tax.HasValue ? row.Tax -= wht : row.Tax = 0; // wht
                            row.DiscountAmount -= discount;


                            row.TotalPaid -= Convert.ToDecimal(item.Amount) + wht;
                            if (row.TotalPaid == 0)
                            {
                                row.PaymentStatus = "UnPaid";
                                row.IsPaid = false;
                            }
                            else if (row.TotalPaid < row.TotalAmount)
                            {
                                row.PaymentStatus = "Partial Paid";
                                row.IsPaid = false;
                            }
                            row.UpdateOn = Helper.PST();
                            db.Entry(row).State = EntityState.Modified;
                        }

                        else // get OrderID from MultiInvoiceOrderIDs columns
                        {
                            if (!string.IsNullOrWhiteSpace(item.MultiInvoiceOrderIDs))
                            {
                                string[] OrderIDsStringArray = item.MultiInvoiceOrderIDs.Split('|');
                                var jEntry = db.tbl_JEntry.Where(x => x.JEntryId == JEntryID).FirstOrDefault();

                                if (OrderIDsStringArray.Length > 0)
                                {
                                    foreach(var id in OrderIDsStringArray)
                                    {
                                        int oid = 0;
                                        int.TryParse(id, out oid);
                                        
                                        var invoiceRow = db.tbl_SalesOrder.FirstOrDefault(so => so.OrderID == oid);// && so.BranchID == item.BranchID);
                                        if (invoiceRow != null)
                                        {
                                            decimal wht = 0;
                                            decimal discount = 0;
                                            decimal adjustment = 0;

                                            //var jentryDetails = db.tbl_JDetail.Where(x => x.JEntryID == JEntryID).ToList();
                                            var jentryDetails = db.tbl_JDetail.Where(x => x.JEntryID == jEntry.SettleEntryID).ToList();
                                            if(jentryDetails!=null && jentryDetails.Count > 0)
                                            {
                                                Parallel.ForEach(jentryDetails, x =>
                                                {
                                                    if (!string.IsNullOrWhiteSpace(x.Memo) && x.Memo.Contains("WHTAccID:")) // WHT Acc ID
                                                    {
                                                        string whtMemoForId = x.Memo.Split(':').ToArray().LastOrDefault().ToString();

                                                        Decimal.TryParse(whtMemoForId, out wht);

                                                        if (wht > 0) { wht = x.Cr; }
                                                    }
                                                    if (x.AccountID == 5) // Adj Acc ID
                                                    {
                                                        adjustment = x.Cr;
                                                    }
                                                    if (x.AccountID == 31) // Discount Acc ID
                                                    {
                                                        discount = x.Dr;
                                                    }
                                                });

                                            }

                                            invoiceRow.AdjustmentDescription = string.Empty;
                                            invoiceRow.Adjustment = invoiceRow.Adjustment.HasValue ? invoiceRow.Adjustment -= adjustment : invoiceRow.Adjustment = 0;
                                            invoiceRow.Tax = invoiceRow.Tax.HasValue ? invoiceRow.Tax -= wht : invoiceRow.Tax = 0; // wht
                                            invoiceRow.DiscountAmount -= discount;

                                            var jEntries = db.tbl_JEntryLog.Where(x => x.JEntryID == jEntry.SettleEntryID && x.OrderID == invoiceRow.OrderID ).FirstOrDefault();
                                            //invoiceRow.TotalPaid -= Convert.ToDecimal(item.Amount);// + wht;

                                            var removeSettledEntries = db.tbl_JEntry.Where(x => x.JEntryId == jEntry.SettleEntryID).FirstOrDefault();
                                            if (removeSettledEntries != null)
                                            {
                                                removeSettledEntries.IsActive = false;
                                                removeSettledEntries.IsDeleted = true;
                                                removeSettledEntries.DeleteOn = Helper.PST();
                                                db.Entry(removeSettledEntries).State = EntityState.Modified;
                                            }
                                            invoiceRow.TotalPaid -= jEntries?.Amount !=null ? Convert.ToDecimal(jEntries?.Amount):0;// + wht;
                                            if (invoiceRow.TotalPaid == 0)
                                            {
                                                invoiceRow.PaymentStatus = "UnPaid";
                                                invoiceRow.IsPaid = false;
                                            }
                                            else if (invoiceRow.TotalPaid < invoiceRow.TotalAmount)
                                            {
                                                invoiceRow.PaymentStatus = "Partial Paid";
                                                invoiceRow.IsPaid = false;
                                            }
                                            invoiceRow.UpdateOn = Helper.PST();
                                            db.Entry(invoiceRow).State = EntityState.Modified;

                                            //var jEntry = db.tbl_JEntry.Where(x => x.JEntryId == JEntryID).FirstOrDefault();
                                            if (jEntry != null)
                                            {
                                                jEntry.IsDeleted = true;
                                                jEntry.DeleteOn = Helper.PST();
                                                jEntry.IsActive = false;
                                                db.Entry(jEntry).State = EntityState.Modified;
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region Reverse Journal Entry 
                    //int? newJEntryID = db.BounceCheque(JEntryID).FirstOrDefault();
                    var cheqMode = db.tbl_Cheques.Where(j => j.JEntryId == JEntryID).FirstOrDefault();
                    int? newJEntryID = 0;
                    if (cheqMode.IsPDC == true)
                    {
                        newJEntryID = db.BouncePDCCheque(JEntryID).FirstOrDefault();
                    }
                    else
                    {
                        newJEntryID = db.BounceCheque(JEntryID).FirstOrDefault();
                    }
                    
                    #endregion
                    #region Reverse Entries Log
                    Mapper.CreateMap<Models.DTO.JEntryLog, tbl_JEntryLog>();
                    var jReverseLog = Mapper.Map<ICollection<Models.DTO.JEntryLog>, ICollection<tbl_JEntryLog>>(jLog);
                    tbl_JEntryLog jentryLog = new tbl_JEntryLog();
                    foreach (var item in jReverseLog)
                    {
                        jentryLog.OrderID = item.OrderID;
                        jentryLog.Amount = item.Amount;
                        jentryLog.BranchID = item.BranchID;
                        jentryLog.isReversed = true; // previously false on 29-09-2023
                        jentryLog.OrderTypeID = 3;                         
                        jentryLog.JEntryID = newJEntryID ?? 0;
                        jentryLog.ReversedOn = Helper.PST();
                        db.tbl_JEntryLog.Add(jentryLog);
                    }
                    #endregion                   
                    db.SaveChanges();
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }

        // Vendor Cheque Clearance Entry
        public object VendorChequeClearedEntry(int JEntryID)
        {
            dbPOS db = new dbPOS();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {                    
                    db.Configuration.ValidateOnSaveEnabled = false;
                    #region Journal Entry 
                    int? newJEntryID = db.insertVendorPDCClearence(JEntryID).FirstOrDefault();
                    #endregion                   
                    db.SaveChanges();
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        // Cheque Clearance Entry
        public object ChequeClearedEntry(int JEntryID,int? BankAccID)
        {
            dbPOS db = new dbPOS();
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    ICollection<tbl_JEntryLog> jEntryLog = db.tbl_JEntryLog.Where(j => j.JEntryID == JEntryID).ToList();
                    Mapper.CreateMap<tbl_JEntryLog, Models.DTO.JEntryLog>();
                    var jLog = Mapper.Map<ICollection<tbl_JEntryLog>, ICollection<Models.DTO.JEntryLog>>(jEntryLog);
                    db.Configuration.ValidateOnSaveEnabled = false;                   
                    #region Reverse Journal Entry 
                    int? newJEntryID = db.insertPDCClearence(JEntryID, BankAccID).FirstOrDefault();
                    #endregion                   
                    db.SaveChanges();
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
    }
}