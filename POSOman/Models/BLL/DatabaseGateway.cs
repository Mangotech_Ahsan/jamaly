﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
using System.Configuration;

namespace POSOman.Models.BLL
{
    public static class DatabaseGateway
    {
        public static string CONNECTION_STRING = ConfigurationManager.ConnectionStrings["DB_POSConnectionString"].ConnectionString;
        public static string COMMAND_TIMEOUT = ConfigurationManager.AppSettings["CommandTimeout"];
        
        /// <summary>
        /// Identify sql custom/user defined exceptions
        /// </summary>
        /// <param name="exception"></param>
        private static void ThrowSQLException(SqlException sqlException)
        {
            // If sql custom/user defined exception
            //if (sqlException.Number == 50000)
            //    throw new BusinessExceptions(sqlException.Message);
            //else
                throw sqlException;
        }
        
        /// <summary>
        /// Gets the data using the stored procedure and parameters provided
        /// </summary>
        /// <param name="procedure"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static DataSet GetDataSetUsingStoredProcedure(string procedure, List<SqlParameter> parameters)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(procedure, conn))
                    {
                        //command.CommandTimeout = ConversionHelper.SafeConvertToInt32(COMMAND_TIMEOUT);
                        command.CommandType = CommandType.StoredProcedure;

                        if (parameters != null)
                        {
                            foreach (SqlParameter parameter in parameters)
                            {
                                command.Parameters.Add(parameter);
                            }
                        }

                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataSet dataset = new DataSet();
                            adapter.Fill(dataset);
                            return dataset;
                        }
                    }
                }
            }
            catch (SqlException sqlException)
            {
                ThrowSQLException(sqlException);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procedure"></param>
        /// <param name="parameters"></param>
        /// <param name="conn"></param>
        /// <param name="sqlTrans"></param>
        /// <returns></returns>
        public static DataSet GetDataSetUsingStoredProcedure(string procedure, List<SqlParameter> parameters, SqlConnection conn, SqlTransaction sqlTrans)
        {
            try
            {
                using (SqlCommand command = new SqlCommand(procedure, conn))
                {
                    //command.CommandTimeout = ConversionHelper.SafeConvertToInt32(COMMAND_TIMEOUT);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Transaction = sqlTrans;

                    if (parameters != null)
                    {
                        foreach (SqlParameter parameter in parameters)
                        {
                            command.Parameters.Add(parameter);
                        }
                    }

                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        DataSet dataset = new DataSet();
                        adapter.Fill(dataset);
                        return dataset;
                    }
                }
            }
            catch (SqlException sqlException)
            {
                ThrowSQLException(sqlException);
            }

            return null;
        }

        /// <summary>
        /// Get scalar data using stored procedure and parameters provided. Casting should be done by the caller.
        /// </summary>
        /// <param name="procedure">Stored Procedure name</param>
        /// <param name="parameters">Stored Procedure parameters</param>
        /// <returns>An object of generic type. </returns>
        public static object GetScalarDataUsingStoredProcedure(string procedure, List<SqlParameter> parameters)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(procedure, conn))
                    {
                        //command.CommandTimeout = ConversionHelper.SafeConvertToInt32(COMMAND_TIMEOUT);
                        command.CommandType = CommandType.StoredProcedure;

                        if (parameters != null)
                        {
                            foreach (SqlParameter parameter in parameters)
                            {
                                command.Parameters.Add(parameter);
                            }
                        }

                        var result = command.ExecuteScalar();

                        return result;
                    }
                }
            }
            catch (SqlException sqlException)
            {
                ThrowSQLException(sqlException);
            }

            return null;
         }

        /// <summary>
        /// Execute stored procedure with given sql parameters
        /// </summary>
        /// <param name="procedure"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static void ExecuteStoredProcedure(string procedure, List<SqlParameter> parameters)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(procedure, conn))
                    {
                        //command.CommandTimeout = ConversionHelper.SafeConvertToInt32(COMMAND_TIMEOUT);
                        command.CommandType = CommandType.StoredProcedure;

                        foreach (SqlParameter parameter in parameters)
                        {
                            command.Parameters.Add(parameter);
                        }
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException sqlException)
            {
                ThrowSQLException(sqlException);
            }
        }

        internal static object GetDataSetUsingStoredProcedure(object uSP_ADD_USER_RATING_FEEDBACK, List<SqlParameter> parameters)
        {
            throw new NotImplementedException();
        }
    }
}
