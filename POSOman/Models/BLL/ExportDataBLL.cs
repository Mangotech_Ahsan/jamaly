﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace POSOman.Models.BLL
{
    public static class ExportDataBLL
    {
        public static void ExcelExport()
        {
            try
            {

            }
            catch(Exception e) { }
        }

        private static DataTable ConvertToDatatable(List<dynamic> list)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Name");
            dt.Columns.Add("Price");
            dt.Columns.Add("URL");
            foreach (var item in list)
            {
                var row = dt.NewRow();

                row["Name"] = item.Name;
                row["Price"] = Convert.ToString(item.Price);
                row["URL"] = item.URL;

                dt.Rows.Add(row);
            }

            return dt;
        }
    }
}