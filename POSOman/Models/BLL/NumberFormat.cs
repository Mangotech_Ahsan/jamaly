﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.BLL
{
    public class NumberFormat
    {
        public static string Convert(decimal num)
        {
            return num.ToString("#,##0.00");
        }
    }
}