﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POSOman.Models.DTO;
using System.Text;
using AutoMapper;
using System.Web.Configuration;

namespace POSOman.Models.BLL
{
    public class ExpensesEntry
    {
        public object Save(Expenses model, PaymentLog payLog, int? bankAccId,int branchId)
        {
            
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;            
            
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentTypeID == 1 || model.PaymentTypeID == 2)
                    {
                        model.ChequeDate = null;
                    }

                    if (model.PaymentTypeID == 1)
                    {
                        model.ChequeDate = null;
                        model.BankName = "";
                    }

                    model.BranchID = branchId;    
                    StringBuilder ExpenseMemo = new StringBuilder();
                    ExpenseMemo.Append("Paid For Expenses.");
                    db.Configuration.ValidateOnSaveEnabled = false;
                    if (model.PaymentTypeID == 2 || model.PaymentTypeID == 3)
                    {
                        jEntryID = db.insertExpenseGJEntryCredit(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, ExpenseMemo.ToString(), model.BranchID, model.Description,bankAccId, model.UserID, model.SaleOrderID,model.DepartmentID).FirstOrDefault();
                    }
                    else
                    {
                        jEntryID = db.insertExpenseGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, ExpenseMemo.ToString(), model.BranchID, model.Description, model.UserID, model.SaleOrderID,model.DepartmentID).FirstOrDefault();
                    }                   

                    transaction.Commit();
                    return jEntryID;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                            raise = new InvalidOperationException(message, raise);
                        }
                    }

                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
        public object PayPurchaseExpenses(Expenses model, int? bankAccId,int branchId)
        {
            dbPOS db = new dbPOS();
            int AddBy = 1;
            int? jEntryID = 0;                        
            using (
            var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.PaymentTypeID == 1 || model.PaymentTypeID == 2)
                    {
                        model.ChequeDate = null;
                    }
                    if (model.PaymentTypeID == 1)
                    {
                        model.BankName = "";
                    }
                    model.BranchID = branchId;
                    StringBuilder ExpenseMemo = new StringBuilder();
                    ExpenseMemo.Append("Paid For Purchase Expenses.");
                    db.Configuration.ValidateOnSaveEnabled = false;
                    if (model.PaymentTypeID == 2 || model.PaymentTypeID == 3)
                    {
                        jEntryID = db.insertExpenseGJEntryCredit(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, ExpenseMemo.ToString(), model.BranchID, model.Description, bankAccId, model.UserID, null,null).FirstOrDefault();
                    }
                    else
                    {
                        jEntryID = db.insertExpenseGJEntry(model.Amount, model.PaymentTypeID, model.VoucherDate, AddBy, model.AccountID, model.BankName, model.ChequeDate, model.ChequeNumber, ExpenseMemo.ToString(), model.BranchID, model.Description, model.UserID, null, null).FirstOrDefault();
                    }
                    
                    transaction.Commit();
                    return "success";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",validationErrors.Entry.Entity.ToString(),validationError.ErrorMessage);
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    transaction.Rollback();
                    return "failed";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
            }
        }
    }
}
