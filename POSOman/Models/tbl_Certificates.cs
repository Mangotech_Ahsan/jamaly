//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class tbl_Certificates
    {
        public int ID { get; set; }
        [RegularExpression(@"^\w{0,10}$", ErrorMessage = "String Length Exceed")]
        public string Certificate { get; set; }
        [RegularExpression(@"^\w{0,25}$", ErrorMessage = "String Length Exceed")]
        public string CertificateInfo { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> AddedOn { get; set; }
    }
}
