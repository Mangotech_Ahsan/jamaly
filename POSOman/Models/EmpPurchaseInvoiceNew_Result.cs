//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    
    public partial class EmpPurchaseInvoiceNew_Result
    {
        public string EmployeeName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string InvoiceNo { get; set; }
        public int EPOID { get; set; }
        public System.DateTime Date { get; set; }
        public decimal TotalAmount { get; set; }
        public string OrderStatus { get; set; }
        public string Product { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Qty { get; set; }
        public decimal Total { get; set; }
    }
}
