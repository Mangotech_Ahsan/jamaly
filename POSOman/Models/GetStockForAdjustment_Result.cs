//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    
    public partial class GetStockForAdjustment_Result
    {
        public int ProductID { get; set; }
        public string VehicleCode { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public Nullable<int> LevelID { get; set; }
        public bool IsPacket { get; set; }
        public string UnitCode { get; set; }
        public string OpenUnitCode { get; set; }
        public string LeastUnitCode { get; set; }
        public Nullable<int> UnitPerCarton { get; set; }
        public decimal Qty { get; set; }
        public Nullable<decimal> CostPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public string BranchName { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<decimal> OnMove { get; set; }
    }
}
