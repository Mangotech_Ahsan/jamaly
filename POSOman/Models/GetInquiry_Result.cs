//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    
    public partial class GetInquiry_Result
    {
        public int InquiryID { get; set; }
        public System.DateTime InquiryDate { get; set; }
        public string InquiryNumber { get; set; }
        public string CompanyName { get; set; }
        public string ContactPerson { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Sources { get; set; }
        public Nullable<System.DateTime> FollowUpDate { get; set; }
        public string FollowUp { get; set; }
        public Nullable<int> StatusID { get; set; }
        public string StatusTitle { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public Nullable<decimal> ChancesPercent { get; set; }
        public Nullable<int> SalePersonID { get; set; }
        public string AccountName { get; set; }
        public Nullable<decimal> SalesRevenue { get; set; }
    }
}
