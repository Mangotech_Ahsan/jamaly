//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_JEntryType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_JEntryType()
        {
            this.tbl_JDetail = new HashSet<tbl_JDetail>();
            this.tbl_PaymentLog = new HashSet<tbl_PaymentLog>();
        }
    
        public int EntryTypeID { get; set; }
        public string EntryType { get; set; }
        public string Detail { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }
        public Nullable<int> BranchID { get; set; }
    
        public virtual tbl_Branch tbl_Branch { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_JDetail> tbl_JDetail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_PaymentLog> tbl_PaymentLog { get; set; }
    }
}
