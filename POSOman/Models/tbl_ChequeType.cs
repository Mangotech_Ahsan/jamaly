//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_ChequeType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_ChequeType()
        {
            this.tbl_Cheques = new HashSet<tbl_Cheques>();
        }
    
        public int ChequeTypeID { get; set; }
        public string ChequeType { get; set; }
        public string Description { get; set; }
        public Nullable<System.Guid> rowguid { get; set; }
        public Nullable<int> BranchID { get; set; }
    
        public virtual tbl_Branch tbl_Branch { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Cheques> tbl_Cheques { get; set; }
    }
}
