//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_EmployeeSettlement
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_EmployeeSettlement()
        {
            this.tbl_EmployeeSettlementDetails = new HashSet<tbl_EmployeeSettlementDetails>();
        }
    
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public System.DateTime Date { get; set; }
        public bool IsDeleted { get; set; }
        public string Description { get; set; }
        public System.DateTime AddedOn { get; set; }
        public decimal TotalAmount { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_EmployeeSettlementDetails> tbl_EmployeeSettlementDetails { get; set; }
        public virtual tbl_Employee tbl_Employee { get; set; }
    }
}
