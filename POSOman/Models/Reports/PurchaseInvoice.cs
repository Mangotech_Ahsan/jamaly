﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSOman.Models.Reports
{
    public class PurchaseInvoices
    {
        public string VenderName { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int InvoiceNo { get; set; }
        public string GeneratedBy { get; set; }
        public string PaymentMode { get; set; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal FinalAmount { get; set; }
    }
}