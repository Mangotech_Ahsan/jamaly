//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSOman.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_InquiryQuotation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_InquiryQuotation()
        {
            this.tbl_InquiryQuotationDetails = new HashSet<tbl_InquiryQuotationDetails>();
            this.tbl_InquiryQuotationDetails1 = new HashSet<tbl_InquiryQuotationDetails>();
            this.tbl_SalesOrder1 = new HashSet<tbl_SalesOrder1>();
            this.tbl_SalesOrder11 = new HashSet<tbl_SalesOrder1>();
        }
    
        public int QuotationID { get; set; }
        public int Code { get; set; }
        public string StrCode { get; set; }
        public System.DateTime QuotationDate { get; set; }
        public Nullable<int> InquiryID { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> TaxPercnt { get; set; }
        public Nullable<decimal> TaxAmount { get; set; }
        public Nullable<decimal> DiscPercnt { get; set; }
        public Nullable<decimal> DiscAmount { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public string Description { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<bool> isDelete { get; set; }
        public Nullable<System.DateTime> AddOn { get; set; }
        public string AddBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<decimal> GSTPercnt { get; set; }
        public Nullable<decimal> GSTAmount { get; set; }
        public Nullable<decimal> AdvaneAmount { get; set; }
        public Nullable<decimal> MidWayAmount { get; set; }
        public Nullable<decimal> CompletionAmount { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public Nullable<System.DateTime> CompleteDate { get; set; }
        public string OfferFor { get; set; }
        public string QuotationType { get; set; }
        public string TaxType { get; set; }
        public string OnAdvance { get; set; }
        public string OnDelivery { get; set; }
        public string OnCompletion { get; set; }
        public string ValidUpto { get; set; }
    
        public virtual tbl_Inquiry tbl_Inquiry { get; set; }
        public virtual tbl_Inquiry tbl_Inquiry1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_InquiryQuotationDetails> tbl_InquiryQuotationDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_InquiryQuotationDetails> tbl_InquiryQuotationDetails1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_SalesOrder1> tbl_SalesOrder1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_SalesOrder1> tbl_SalesOrder11 { get; set; }
    }
}
